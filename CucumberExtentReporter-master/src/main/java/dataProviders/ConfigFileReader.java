package dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
    public Properties properties;
    public  String propertyFilePath= "src/test/resources/configuration.properties";
//  /Users/nisum/Documents/E_drive/CucumberFramework/src/test/resources/configs/configuration.properties
    public ConfigFileReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public  String getChromeDriverPath(){
        String chromeDriverPath = properties.getProperty("chromeDriverPath");
        if(chromeDriverPath!= null) {

            return chromeDriverPath;
        }
        else throw new RuntimeException("Driver Path not specified in the Configuration.properties file for the Key:driverPath");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if(implicitlyWait != null) {
            try{
                return Long.parseLong(implicitlyWait);
            }catch(NumberFormatException e) {
                throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
            }
        }
        return 30;
    }

    public String getApplicationUrl() {
        String url = properties.getProperty("url");
        if(url != null) return url;
        else throw new RuntimeException("Application Url not specified in the Configuration.properties file for the Key:url");
    }

    public String getBrowser() {
        String browserName = properties.getProperty("browser");
        if(browserName == null ) {
        	throw new RuntimeException("The browser is not specified properly in the configuration.properties : " + browserName);
        } else {
        	return browserName;
        }
    }

    public String getFirefoxPath() {
        String firefoxPath = properties.getProperty("firefoxDriverPath");
        if(firefoxPath != null) return firefoxPath;
        else throw new RuntimeException("The firefox driver path is not mentioned properly in the configuration.properties");
    }
    
    public String getEdgePath() {
        String edgePath = properties.getProperty("edgeDriverPath");
        if(edgePath != null) return edgePath;
        else throw new RuntimeException("The edge driver path is not mentioned properly in the configuration.properties");
    }
    
    /*public String getEnvironment() {
        String environmentName = properties.getProperty("environment");
        if(environmentName == null || environmentName.equalsIgnoreCase("local")) return EnvironmentType.LOCAL;
        else if(environmentName.equalsIgnoreCase("remote")) return EnvironmentType.REMOTE;
        else throw new RuntimeException("Environment Type Key value in Configuration.properties is not matched : " + environmentName);
    }*/

    public Boolean getBrowserWindowSize() {
        String windowSize = properties.getProperty("windowMaximize");
        if(windowSize != null  ) return Boolean.valueOf(windowSize);
        return true;

    }

    public String getReportConfigPath(){
        String reportConfigPath = properties.getProperty("reportConfigPath");
        if(reportConfigPath!= null) return reportConfigPath;
        else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
    }
    
    public String getnodeURL1() {
    	String node_url1 = properties.getProperty("node_url1");
    	if(node_url1!= null) return node_url1;
    	else throw new RuntimeException("Please specify the node URL properly");
    }
    public String getnodeURL2() {
    	String node_url2 = properties.getProperty("node_url2");
    	if(node_url2!= null) return node_url2;
    	else throw new RuntimeException("Please specify the node URL properly");
    }
    
    public boolean gridStatus() {
    	String gridStatus = properties.getProperty("grid_status");
    	if(gridStatus!= null) return Boolean.valueOf(gridStatus);
    	else throw new RuntimeException("Please specify the grid status properly");
    }
    public boolean Smoke_status() {
    	String smokestatus = properties.getProperty("Smoke_status");
    	if(smokestatus!= null) return Boolean.valueOf(smokestatus);
    	else throw new RuntimeException("Please specify the smoke test status properly");
    }
    
    
}