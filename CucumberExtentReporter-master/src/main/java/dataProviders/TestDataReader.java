package dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestDataReader {

	public Properties properties;
    public  String propertyFilePath= "src/test/resources/testdata.properties";
//  /Users/nisum/Documents/E_drive/CucumberFramework/src/test/resources/configs/configuration.properties
    public TestDataReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }
    
    public  String freeSpiritEmail(){
        String free_spirit_email = properties.getProperty("Free_spirit_email");
        if(free_spirit_email!= null || free_spirit_email.contains("@spirit.com")) {
            return free_spirit_email;
        }
        else throw new RuntimeException("The free spirit email mentioned is null/ not in the correct format");
    }
    public  String freeSpiritPassword(){
        String free_spirit_password = properties.getProperty("Free_Spirit_Password");
        if(free_spirit_password!= null) {
            return free_spirit_password;
        }
        else throw new RuntimeException("The free spirit password mentioned is null, please give the valid password");
    }
    public  String carryonBagprice(){
        String carryon_Bag = properties.getProperty("carryon_Bag");
        if(carryon_Bag!= null) {
            return carryon_Bag;
        }
        else throw new RuntimeException("The free spirit password mentioned is null, please give the valid password");
    }
    
    public String checkedBagprice(){
        String checked_Bag_prices = properties.getProperty("Checked_bag_prices");
        if(checked_Bag_prices!= null) {
            return checked_Bag_prices;
        }
        else throw new RuntimeException("The  checked bags prices are mentioned wrong, please give the valid prices");
    }
    
    public String checkedBagpriceMinus(){
        String checked_Bag_prices_Minus = properties.getProperty("Checked_bag_prices_Minus_Button");
        if(checked_Bag_prices_Minus!= null) {
            return checked_Bag_prices_Minus;
        }
        else throw new RuntimeException("The  checked bags prices are mentioned wrong, please give the valid prices");
    }
    
    public String Promocode(){
        String promo_code = properties.getProperty("Promo_code");
        if(promo_code!= null) {
            return promo_code;
        }
        else throw new RuntimeException("The promocode that are mentioned is wrong, please give the valid promocode");
    }
    
    public String InvalidPromocode(){
        String invalid_promo_code = properties.getProperty("Invalid_promo_code");
        if(invalid_promo_code!= null) {
            return invalid_promo_code;
        }
        else throw new RuntimeException("The promocode that are mentioned is wrong, please give the valid promocode");
    }
    
    public String Promocode_percent_off(){
        String percent_off_promo_code = properties.getProperty("Promo_code_Percent_off");
        if(percent_off_promo_code!= null) {
            return percent_off_promo_code;
        }
        else throw new RuntimeException("The promocode for percent off that are mentioned is wrong, please give the valid promocode");
    }
    
    public String Ninefcemail(){
        String valid_ninefc_email = properties.getProperty("NineFC_valid_emailaddress");
        if(valid_ninefc_email!= null) {
            return valid_ninefc_email;
        }
        else throw new RuntimeException("The 9FC email that are mentioned is wrong, please give the valid email address");
    }
    
    
    public String Ninepassword(){
        String valid_ninefc_password = properties.getProperty("NineFC_Valid_Password");
        if(valid_ninefc_password!= null) {
            return valid_ninefc_password;
        }
        else throw new RuntimeException("The 9FC password that are mentioned is wrong, please give the valid password");
    }
    
    public String NineFCnameoncard(){
        String valid_ninefc_name_on_card = properties.getProperty("NineFc_Name_on_card");
        if(valid_ninefc_name_on_card!= null) {
            return valid_ninefc_name_on_card;
        }
        else throw new RuntimeException("The 9FC Name on the card that are mentioned is wrong, please give the valid Name");
    }
    
    public String NineFCcardNumber(){
        String valid_ninefc_card_number = properties.getProperty("NineFC_card_Number");
        if(valid_ninefc_card_number!= null) {
            return valid_ninefc_card_number;
        }
        else throw new RuntimeException("The 9FC Number on the card that are mentioned is wrong, please give the valid card number");
    }
 
    public String NineFCExpirationdate(){
        String valid_ninefc_expiration_date = properties.getProperty("NineFC_Expiration_date");
        if(valid_ninefc_expiration_date!= null) {
            return valid_ninefc_expiration_date;
        }
        else throw new RuntimeException("The 9FC expiration date on the card that are mentioned is wrong, please give the valid card exipiration");
    }
 
    
    public  String UMNRninedfcemail(){
        String UMNR_ninedfc_email = properties.getProperty("UMNR_ninedfc_email");
        if(UMNR_ninedfc_email!= null || UMNR_ninedfc_email.contains("@spirit.com")) {
            return UMNR_ninedfc_email;
        }
        else throw new RuntimeException("The NineDFC member email mentioned is null/ not in the correct format");
    }
    public  String ninedfccarryonBagprice(){
        String ninedfc_carryon_Bag = properties.getProperty("ninedfc_carryon_Bag");
        if(ninedfc_carryon_Bag!= null) {
            return ninedfc_carryon_Bag;
        }
        else throw new RuntimeException("The carry on bag price is mentioned wrong, please give the valid pricesd");
    }
    
    public String ninedfccheckedBagprice(){
        String ninedfc_Checked_bag_prices = properties.getProperty("ninedfc_Checked_bag_prices");
        if(ninedfc_Checked_bag_prices!= null) {
            return ninedfc_Checked_bag_prices;
        }
        else throw new RuntimeException("The  checked bags prices are mentioned wrong, please give the valid prices");
    }
    
    public String ninedfccheckedBagpriceMinus(){
        String ninedfc_Checked_bag_prices_Minus_Button = properties.getProperty("ninedfc_Checked_bag_prices_Minus_Button");
        if(ninedfc_Checked_bag_prices_Minus_Button!= null) {
            return ninedfc_Checked_bag_prices_Minus_Button;
        }
        else throw new RuntimeException("The  checked bags prices are mentioned wrong, please give the valid prices");
    }
    
    public  String militaryninedfcemail(){
        String military_ninedfc_email = properties.getProperty("military_ninedfc_email");
        if(military_ninedfc_email!= null || military_ninedfc_email.contains("@spirit.com")) {
            return military_ninedfc_email;
        }
        else throw new RuntimeException("The NineDFC member email mentioned is null/ not in the correct format");
    }
    public  String discovercardnumber(){
        String discover_card_number = properties.getProperty("Discover_card_number");
        if(discover_card_number!= null) {
            return discover_card_number;
        }
        else throw new RuntimeException("The discover card number is not correct");
    }
    
    
    public  String visacardnumber(){
        String visa_card_number = properties.getProperty("Visa_card_number");
        if(visa_card_number!= null) {
            return visa_card_number;
        }
        else throw new RuntimeException("The visa card number is not correct");
    }
    
    public  String mastercardnumber(){
        String master_card_number = properties.getProperty("Master_card_number");
        if(master_card_number!= null) {
            return master_card_number;
        }
        else throw new RuntimeException("The master card number is not correct");
    }
    
    public  String americanexpresscardnumber(){
        String amex_card_number = properties.getProperty("American_Express_card_number");
        if(amex_card_number!= null) {
            return amex_card_number;
        }
        else throw new RuntimeException("The master card number is not correct");
    }
    
    public String childDOB(){
        String child_DOB = properties.getProperty("child_DOB");
        if(child_DOB!= null) {
            return child_DOB;
        }
        else throw new RuntimeException("The Child DOB is wrong, please give the valid DOB");
    }
    public  String uatpcardnumber(){
        String uatp_card_number = properties.getProperty("Uatp_card_number");
        if(uatp_card_number!= null) {
            return uatp_card_number;
        }
        else throw new RuntimeException("The UATP card number is not correct");
    }

    public  String egiftcard1number(){
        String egift_card1_number = properties.getProperty("Egift_card1_number");
        if(egift_card1_number!= null) {
            return egift_card1_number;
        }
        else throw new RuntimeException("The Egift card1 number is not correct");
    }

    public  String egiftcard2number(){
        String egift_card2_number = properties.getProperty("Egift_card2_number");
        if(egift_card2_number!= null) {
            return egift_card2_number;
        }
        else throw new RuntimeException("The Egift card2 number is not correct");
    }

    public  String egiftcard3number(){
        String egift_card3_number = properties.getProperty("Egift_card3_number");
        if(egift_card3_number!= null) {
            return egift_card3_number;
        }
        else throw new RuntimeException("The Egift card3 number is not correct");
    }

    public  String egiftcard4number(){
        String egift_card4_number = properties.getProperty("Egift_card4_number");
        if(egift_card4_number!= null) {
            return egift_card4_number;
        }
        else throw new RuntimeException("The Egift card4 number is not correct");
    }

    public  String egiftcard5number(){
        String egift_card5_number = properties.getProperty("Egift_card5_number");
        if(egift_card5_number!= null) {
            return egift_card5_number;
        }
        else throw new RuntimeException("The Egift card5 number is not correct");
    }
    
    public String NineFSNumber(){
        String valid_ninefc_fsnumber = properties.getProperty("NineFC_Valid_FS_Number");
        if(valid_ninefc_fsnumber!= null) {
            return valid_ninefc_fsnumber;
        }
        else throw new RuntimeException("The 9FC FS Number that are mentioned is wrong, please give the valid FS Number");
    }
    
}
