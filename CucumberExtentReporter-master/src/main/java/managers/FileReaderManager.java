package managers;

import dataProviders.ConfigFileReader;
import dataProviders.TestDataReader;

public class FileReaderManager {

    private static FileReaderManager fileReaderManager = new FileReaderManager();
    private static ConfigFileReader configFileReader;
    private static TestDataReader testDataReader;

    private FileReaderManager() {
    }

    public static FileReaderManager getInstance( ) {
        return fileReaderManager;
    }

    public ConfigFileReader getConfigReader() {
        return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
    }
    
    public TestDataReader getTestDataReader() {
        return (testDataReader == null) ? new TestDataReader() : testDataReader;
    }
}
