Feature: Boarding Pass

  @EPIC3700
  Scenario Outline: BoardingPass CP CI Email delivery option check both boxes
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user select the today as a departure date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user click on the checkin boarding pass switch to the pro trip popup and clicks on i do not need bags and clicks on get random seats popup
    Then user lands on the Extras page
    Then user clicks on the continue button on the extras page and switch to the popup and select the i dont want insurance and accept and print the boarding pass popup and switch to the boarding pass popup
    Then user selects the print and email the boarding pass
    Then user save the generated pdf

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3949
  Scenario Outline: BoardingPass CP CI Boarding Pass WireFrame
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user select the today as a departure date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user click on the checkin boarding pass switch to the pro trip popup and clicks on i do not need bags and clicks on get random seats popup
    Then user lands on the Extras page
    Then user clicks on the continue button on the extras page and switch to the popup and select the i dont want insurance and accept and print the boarding pass popup and switch to the boarding pass popup
    Then user selects the print and email the boarding pass

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13275
  Scenario Outline: BoardingPass_CP_CI_DOM_OW_Verify you get self tag airport message departing from a Self-Tagging Airports
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user select the today as a departure date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user click on the checkin boarding pass switch to the pro trip popup and clicks on i do not need bags and clicks on get random seats popup
    Then user lands on the Extras page
    Then user clicks on the continue button on the extras page and switch to the popup and select the i dont want insurance and accept and print the boarding pass popup and switch to the boarding pass popup
    Then user selects the print and email the boarding pass

    Examples: 
      | Required stops |
      | 1 Stop         |

  
