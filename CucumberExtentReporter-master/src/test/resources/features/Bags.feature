Feature: Bags stand alone module

  @Tag1
  Scenario Outline: add bags with plus or Minus end to end
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user clicks on the adding sporting equipment
    Then user click on the plus and minus buttons next to the bicycle and validate the prices
    Then user clicks on the plus and minus buttons next to the surfboard and validates the prices
    #Then user clicks on the plus and minus buttons next to the forty one to fifty lbs and validates the prices
    #Then user clicks on the plus and minus buttons next to the fifty one to seventy lbs and validates the prices
    #Then user clicks on the plus and minus buttons next to the seventy one to hundred lbs and validates the prices
    #Then user clicks on the plus and minus buttons next to the sixty eight one to eighty lbs and validates the prices
    #Then user clicks on the plus and minus buttons next to the special item and validates the prices
    Then user add the bags to the carryon and checkin and validates the total price and clicks on continue without adding bags
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2763
  Scenario Outline: add bags with plus or Minus next to carry-on bag and checked bag  [EPIC2763]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2765
  Scenario Outline: add sporting equipment with plus or minus for bicycle [EPIC2765]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the adding sporting equipment
    Then user click on the plus and minus buttons next to the bicycle and validate the prices
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @ID867
  Scenario Outline: add sporting equipment with plus or minus for Surfboard 
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the adding sporting equipment
    Then user clicks on the plus and minus buttons next to the surfboard and validates the prices
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #@EPIC2751
  #Scenario Outline: ADD Overweight Oversized BAGS forty one to fifty lbs eighteen to twenty three kg WITH PLUS  OR MINUS  [EPIC2751]
    #Given Spirit airlines application
    #When User Clicks on Book link
    #Then User clicks on onewaytrip
    #Then User choose the from city
    #Then User choose the To city
    #Then user selects departures date
    #Then User selects number of passengers
    #Then User clicks on Search button
    #When user lands on flight page
    #Then user clicks on the selected flight
    #Then user selects the what time to fly "<Required stops>"
    #Then user switch to the popup and clicks on close button
    #When user lands on passengers page
    #Then user enters the personal info and contact address
    #Then user clicks in the continue button
    #When user landing on the bags page
    #Then user clicks on the adding sporting equipment
    #Then user clicks on the plus and minus buttons next to the forty one to fifty lbs and validates the prices
    #Then User closes the browser
#
    #Examples: 
      #| Required stops |
      #| 1 Stop         |

  #@EPIC3099
  #Scenario Outline: ADD Overweight Oversized BAG fifty one to seventy lbs twenty three to thirty two kg WITH PLUS OR MINUS  [EPIC3099]
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user switch to the popup and clicks on close button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the plus and minus buttons next to the fifty one to seventy lbs and validates the prices
  #Then User closes the browser
  #
  #@EPIC3104
  #Scenario: ADD Overweight Oversized BAGS sixty eight to eighty linear inchces WITH PLUS  OR MINUS [EPIC3104]
  #Given Spirit airlines application
  #Then User clicks on onewaytrip
  #When User Clicks on Book link
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the plus and minus buttons next to the sixty eight one to eighty lbs and validates the prices
  #Then User closes the browser
  #
  #@EPIC3101
  #Scenario: ADD Overweight Oversized BAG seventy one to hundred lbs WITH PLUS OR MINUS [EPIC3101]
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the plus and minus buttons next to the seventy one to hundred lbs and validates the prices
  #Then User closes the browser
  #
  #@EPIC3106
  #Scenario: ADD Overweight Oversized BAG Special items over eighty linear inchces WITH PLUS OR MINUS
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the plus and minus buttons next to the special item and validates the prices
  #Then User closes the browser
  #@EPIC2770
  #Scenario: ADD BAG WITH TEXT INPUT FOR CARRYON BAGS
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks the text box next to the carryon bags and enter the number of bags and validate the price
  #Then User closes the browser
  #
  #@EPIC2767
  #Scenario: ADD BAG WITH TEXT INPUT FOR CHECKED BAGS
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks the text box next to the checked bags and enter the number of bags and validate the prices
  #Then User closes the browser
  #
  #@EPIC2773
  #Scenario: ADD SPORTING EQUIPMENT WITH TEXT INPUT FOR BICYCLE [EPIC2773]
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks the text box next to the carryon bags and enter the number of bags and validate the price
  #Then user clicks the text box next to the checked bags and enter the number of bags and validate the prices
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the bicycle and enter the number of bicycles and validate the prices
  #Then User closes the browser
  #
  #@EPIC2773
  #Scenario: ADD SPORTING EQUIPMENT WITH TEXT INPUT FOR SURFBOARD [EPIC2773]
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks the text box next to the carryon bags and enter the number of bags and validate the price
  #Then user clicks the text box next to the checked bags and enter the number of bags and validate the prices
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the surfboard and enter the number of surfboards and validate the prices
  #Then User closes the browser
  #
  #@EPIC2766
  #Scenario: ADD OVERWEIGHT OVERSIZED BAG forty one to fifty lbs WITH TEXT INPUT
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the bicycle and enter the number of bicycles and validate the prices
  #Then User closes the browser
  #
  #@EPIC2768
  #Scenario: ADD OVERWEIGHT OVERSIZED BAG fifty one to seventy lbs WITH TEXT INPUT COMBO
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the fifty one to seventy lbs enter the numbe of bags and validates the prices
  #Then User closes the browser
  #
  #@EPIC2771
  #Scenario: ADD OVERWEIGHT OVERSIZED BAG sixty eight to eighty  WITH TEXT INPUT
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the sixty eight to eighty lbs enters the numbe of bags and validates the prices
  #Then User closes the browser
  #
  #@EPIC2769
  #Scenario: ADD OVERWEIGHT OVERSIZED BAG seventy one to hundred lbs WITH TEXT INPUT
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the seventy one to hundred lbs enter the numbe of bags and validates the prices
  #Then User closes the browser
  #
  #@EPIC2772
  #Scenario: ADD Overweight Oversized BAG Special items over eighty linear inchces WITH TEXT INPUT Combo
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the special item enters the number of items and validate the prices
  #Then User closes the browser
  @EPIC2774
  Scenario Outline: CONTINUE WITHOUT BAGS BUTTON WITH BAGS [EPIC2774]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user add the bags to the carryon and checkin and validates the total price and clicks on continue without adding bags
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2775
  Scenario Outline: CONTINUE WITHOUT BAGS BUTTON WITHOUT BAGS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3015
  Scenario Outline: BAGS  ONE PAX VALIDATE BAG TOTAL BAR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2783
  Scenario Outline: user validates the moreinfo link on footer
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the more information link at the botom of the page and validate the information
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @ID907
  Scenario Outline: user validates the embargo restriction on footer
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the embargo restrictions link at the bottom of the page and valiadte the page and navigate back to bags page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #@tag2
  #Scenario: add bags with the text input for end to end
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User clicks on Search button
  #When user lands on flight page
  #Then user clicks on the selected flight
  #Then user selects the what time to fly "<Required stops>"
  #Then user clicks on the continue button
  #When user lands on passengers page
  #Then user enters the personal info and contact address
  #Then user clicks in the continue button
  #When user landing on the bags page
  #Then user clicks the text box next to the carryon bags and enter the number of bags and validate the price
  #Then user clicks the text box next to the checked bags and enter the number of bags and validate the prices
  #Then user clicks on the adding sporting equipment
  #Then user clicks on the text box next to the bicycle and enter the number of bicycles and validate the prices
  #Then user clicks on the text box next to the surfboard and enter the number of surfboards and validate the prices
  #Then user clicks on the text box next to the forty one to fifty lbs and enter the numbe of bags and validate the prices
  #Then user clicks on the text box next to the fifty one to seventy lbs enter the numbe of bags and validates the prices
  #Then user clicks on the text box next to the seventy one to hundred lbs enter the numbe of bags and validates the prices
  #Then user clicks on the text box next to the sixty eight to eighty lbs enters the numbe of bags and validates the prices
  #Then user clicks on the text box next to the special item enters the number of items and validate the prices
  #Then user clicks on the more information link at the botom of the page and validate the information
  #Then user clicks on the embargo restrictions link at the bottom of the page and valiadte the page and navigate back to bags page
  #Then User closes the browser
  @EPIC585
  Scenario Outline: verify PAX one carryon and five checked bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user adds the one carryon and five checked bags
    Then user validates the bags total with the shopping cart
    Then user validate the prices with the drop down pricing
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC614
  Scenario Outline: verify one carryon three checked bags one bike and two surfboards [EPIC614]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon three checked bags one bike and two surfboards
    Then user validates the bags total with the above shopping cart
    Then user validates the dropdown total prices
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC721
  Scenario Outline: Verify multipax add one carry on four checked bags and two surfboards
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user adds the one carry on four checked bags and two surfboards
    Then user validates the bag total with the above shopping cart
    Then user validates the dropdown total price

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC724
  Scenario Outline: Verify multipax add one carryon four checked bags and two surfboards for each pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then primary passenger add the one carryon four checked bags and two surfboards
    Then passenger two add the one carryon four checked bags and two surfboards
    Then passengers validates the price in the cart
    Then passengers validates  the total bags price
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #reviewed
  @EPIC733
  Scenario Outline: verify multi pax add one carryon three checked bags and two surfboards for each pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    #Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then primary passenger add the one carryon three checked bags and two surfboards
    Then passenger two add the one carryon three checked bags and two surfboards
    Then passengers validates the prices in the cart
    Then passengers validates  the total bags prices
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC826
  Scenario Outline: verify multipax add one carryon bag and three checked bags fro each pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    #Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then primary passenger add the one carryon three checked bags
    Then passenger two add the one carryon three checked bags
    Then passengers validate the prices in the cart
    Then passengers validate  the total bags prices
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC924
  Scenario Outline: verify multipax add one carryon bag for each pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then primary passneger selects one carryon bag
    Then passenger two selects the one carryon bag
    Then user validate the bags prices in the cart
    Then user validate the bags prices in the total bags dropdown
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC941
  Scenario Outline: verify multipax add one surfboard for each pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user validates the cities names
    Then primary passneger add the one surfboard
    Then passenger two add the one surfboard
    Then user validate the bag price in cart
    Then user validate the price in  total bags price
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC942
  Scenario Outline: verify five pax add sufrboards for pax one and four
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the five adult passnegers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the five passnegers information
    Then user selects the primary passenger contact checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user added one surfboard to the passneger one
    Then user added the one surfboard to the passenger four
    Then user validate the shopping cart bags price
    Then user validate the dropdown total bags price
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24546
  Scenario Outline: Bags customer portal Manage Travel continue without bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24616
  Scenario Outline: Bags customer portal manage travel add bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user validate the city pair names
    Then user validates the bags prices when increased and dicreased
    Then user validates the total bags price
    Then user clicks on the caret and validates the bags price
    Then user clicks on the standard pricing continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24619
  Scenario Outline: Bags customer portal manage travel  Nine FC add bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user validate the city pair names
    Then user validates the bags prices when increased and dicreased
    Then user validates the total bags price
    Then user clicks on the caret and validates the bags price
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  #Partially covered
  @EPIC26248
  Scenario Outline: Bags customer portal manage travel  Nine FC Enrollment signup
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC26249
  Scenario Outline: Bags customer portal manage travel  Nine FC Enrollment Login
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC26250
  Scenario Outline: Bags customer portal manage travel Nine FC Enrollment sign up Error
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup
    Then user clicks on the sigup button on the popup
    Then user enters the invalid password
    Then user clicks on the signup with the email
    Then user validates the error message on the popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26252
  Scenario Outline: Bags CP MT Nine FC Enrollment Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup
    Then user clicks on the continue with the standard fare button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26253
  Scenario Outline: Bags CP MT  Nine FC Enrollment Sign Up Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup
    Then user clicks on the sigup button on the popup
    Then user clicks on the continue with the standard fare button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26254
  Scenario Outline: Bags CP MT Nine FC Enrollment Log In Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the travel date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user selects the one carryon bag
    Then use clicks on the nine FC continue button and switch to the popup and validate the text on the popup
    Then user clicks on the login button on the popup
    Then user clicks on the continue with the standard fare button

    Examples: 
      | Required stops |
      | 1 Stop         |

  #-----------------------------------------------Sunny--------------------------------------------
  @EPIC24581
  Scenario Outline: Bags Customer portal Booking portal Nine dollar FC Single Pax One Way Bare Fare
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC24582
  Scenario Outline: Bags CP BP NineDFC Single Pax OW Thrills Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo button
    When user lands on passengers page
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon and One Checkin is selected
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC24583
  Scenario Outline: Bags CP BP  NineDFC Single Pax OW Bundles Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose this bundle button
    When user lands on passengers page
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24584
  Scenario Outline: Bags CP BP NineDFC Continue without bags
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24585
  Scenario Outline: Bags CP BP NineDFC Multi Pax OW Bare Fare
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner
    Then user Clicks on I dont need bags on pop up

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC24586
  Scenario Outline: Bags CP BP NineDFC Multipax Pax OW Thrills Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo button
    When user lands on passengers page
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon and One Checkin is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner
      | Required stops |
      | 1 Stop         |

  #partially completed
  @EPIC24587
  Scenario Outline: Bags CP BP  NineDFC Mulitipax Pax OW Bundles Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose this bundle button
    When user lands on passengers page
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24588
  Scenario Outline: Bags CP BP NineDFC Single Pax RT Bare Fare
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24589
  Scenario Outline: Bags CP BP NineDFC Multipax Pax RT Thrills Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo button
    When user lands on passengers page
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon and One Checkin is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24590
  Scenario Outline: Bags CP BP NineDFC Multi Pax RT Bundles Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on the continue button
    Then user clicks on choose this bundle button
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24591
  Scenario Outline: Bags CP BP NineDFC Multi Pax RT Bare Fare
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner
    Then user Clicks on I dont need bags on pop up

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24592
  Scenario Outline: Bags CP BP NineDFC Single Pax Pax RT Thrills Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo button
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon and One Checkin is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24593
  Scenario Outline: Bags CP BP NineDFC Single Pax RT Bundles Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on the continue button
    Then user clicks on choose this bundle button
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

#partially completed
  @EPIC24594
  Scenario Outline: Bags CP BP NineDFC Lap Child
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    Then user enters the personal information of Lap child
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner
    Then user Clicks on I dont need bags on pop up

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24595
  Scenario Outline: Bags CP BP $9FC Mutli Pax_Lima
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LIM
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags and validates he could only get one checkbag
    Then user check if element is enabled

    #	Then user Click continue underneath the NineDFC Banner
    #	Then user Clicks on I dont need bags on pop up
    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24596
  Scenario Outline: Bags CP BP $9FC Mutli Single Lima
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LIM
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag

    #   Then user Click on the plus button next to checked bags and validates he could only get one checkbag
    #	Then user Click continue underneath the NineDFC Banner
    #	Then user Clicks on I dont need bags on pop up
    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24597
  Scenario Outline: Bags CP BP $9FC Military Single Pax OW
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
       When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24598
  Scenario Outline: Bags CP BP $9FC Military Single Pax RT
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
      When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24599
  Scenario Outline: Bags CP BP $9FC Military Multi Pax OW
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
      When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24600
  Scenario Outline: Bags CP BP $9FC Military Multi Pax RT
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
   Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
     Then user switch to the popup and clicks on close button
       When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user enters the personal information for Second adult
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24601
  Scenario Outline: Bags CP BP $9FC Military Multi Military OW
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
      When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user enters the personal information for Second adult military memeber
    Then user clicks on Active Duty Military Personal Checkbox for passenger two
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24602
  Scenario Outline: Bags CP BP $9FC Military Multi Military RT
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
     Then user switch to the popup and clicks on close button
       When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user enters the personal information for Second adult military memeber
    Then user clicks on Active Duty Military Personal Checkbox for passenger two
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC26386
  Scenario Outline: Bags_CP_BP_$9FC_CONTINUE BAGS BUTTON (WITHOUT BAGS)
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
        When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Verify I dont need bags Pop up
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27667
  Scenario Outline: Bags CP BP $9FC Military Multi Pax OW Lap Child
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
     Then user enters the personal information of Lap child
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27668
  Scenario Outline: Bags CP BP $9FC Military Multi Pax RT Lap Child
    Given Spirit airlines application
    Then user login as Military_NineDFC member
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
        When user lands on passengers page
    Then user clicks on Active Duty Military Personal Checkbox
      Then user enters the personal information of Lap child
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user verifies military user get one free carry on
    Then user verifies military user gets two free check bags
    Then user Verify price increases for Check bag bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC27784
  Scenario Outline: Bags CP BP $9FC Multi Pax Lap Child
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
       When user lands on passengers page
     Then user enters the personal information of Lap child
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price for NineDFC
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags for NineDFC
    Then user Click continue underneath the NineDFC Banner
    #Then user Clicks on I dont need bags on pop up

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27785
  Scenario Outline: Bags CP BP $9FC Multi Pax Lap Child Bundle Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose this bundle button
      Then user enters the personal information of Lap child
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27788
  Scenario Outline: Bags CP BP $9FC Multi Pax Lap Child Thrills Combo
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo button
    Then user enters the personal information of passenger two
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on dynamic shopping cart carrot
    Then user Verify One carryon and One Checkin is selected
    Then user validates the caryon bag price for NineDFC
    Then user Verify price increases and decrease for each bag added
    Then user Click continue underneath the NineDFC Banner

    Examples: 
      | Required stops |
      | 1 Stop         |

  #################################~~~~~~~~~~~~ Consumer Portal[3957] ~~~~~~~~~~~~~~~~~~~~~~~~~##############################
  @EPIC24163
  Scenario Outline: Single Pax_OW_Bare Fare
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user switch to the popup and clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    Then user validates the bags total with the shopping cart
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24164
  Scenario Outline: Single Pax_OW_Thrills Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
      When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price when choose thrills combo
    #Then user Click on the minus button next to carryon bag
    #Then user adds the one carryon and one checked bags
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary with thrills combo
    Then user clicks on the continue with adding bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24165
  Scenario Outline: Single Pax_OW_Bundles Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose this bundle
    When user lands on passengers page
    Then user login as nine DFC member
    #Then user enters the personal info and contact address
    Then user clicks in the continue button
    #When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price with bundle
    Then user Click on the minus button next to carryon bag with bundle
    Then user adds the one carryon and one checked bags
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary with bundle
    Then user clicks on the continue with adding bags
    #When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24544
  Scenario Outline: Continue without bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user continue without adding bags with I do not need bags
    Then User navigates to Seats Page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24552
  Scenario Outline: Multi Pax_OW_Bare Fare
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of Lap child
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24553
  Scenario Outline: Multi Pax_OW_Thrills Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
     Then user enters the personal information of Lap child
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price when choose thrills combo
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    #Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24554
  Scenario Outline: Multi Pax_OW_Bundles Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose this bundle
    When user lands on passengers page
    Then user enters the primary passenger information
      Then user enters the personal information of Lap child
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price with bundle
    Then user Click on the minus button next to carryon bag with bundle
    Then user adds the one carryon and one checked bags
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary with bundle
    #Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user validates the bags total with the shopping cart for two passengers with bundle
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24557
  Scenario Outline: Single Pax_RT_Bare Fare
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    Then user validates the bags total with the shopping cart
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24563
  Scenario Outline: Multi Pax_Lap Child
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
   Then user enters the personal information of Lap child
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price
    Then user validates the caryon bag price
    Then user Click on the plus button next to checked bags
    #Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24564
  Scenario Outline: Mutli Pax_Lima
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To Lima
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user switch clicks on the search button and switch to the popup at lima city and switch to the child popup and enters the DOB
    #Then user enters the birthdate of the Lap child
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    #Then user validates the caryon bag price of passenger2
    Then user Click on plus button next to checked bags
    Then user Click on plus button next to checked bags for passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24565
  Scenario: Single_Lima
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To Lima
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button when you need POT
    Then User selects most recent available flights
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on plus button next to checked bags
    Then User validates save money of 9DFC member in itinerary
    Then user validates the bags total with the shopping cart
    Then user clicks on the continue with adding bags

  @EPIC24566
  Scenario Outline: Military_Single Pax_OW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the plus button next to checked bags for military member
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of military
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25067
  Scenario: Military_Lima
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To Lima
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button when you need POT
    Then User selects most recent available flights
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user clicks on the continue with adding bags

  @EPIC24568
  Scenario Outline: Military_Multi Pax_OW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of one child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
   Then user enters the personal information of Lap child
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    #Then user validates the caryon bag price of passenger2
    Then user Click on the plus button next to checked bags for military member
    #Then user Click on the plus button next to checked bags for passenger2
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of military multipax
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24570
  Scenario Outline: Military_Multi Military_OW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user enters the personal information of military passenger two
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user validates the caryon bag price of military passenger2
    Then user Click on the plus button next to checked bags for military member
    Then user Click on the plus button next to checked bags for military passenger2
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of multi military members
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24558
  Scenario Outline: Multi Pax_RT_Thrills Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal information of adult passenger two
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price when choose thrills combo
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user validates the caryon bag price of passenger2
    Then user Click on the plus button next to checked bags for passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    #Then user validates the bags total with the shopping cart with thrills combo
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC26166
  Scenario Outline: $9FC_Enrollment Sign up Error
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then user clicks on the continue with 9DFC conitnue button and signup

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26167
  Scenario Outline: $9FC_Enrollment Log in error
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then user clicks on the continue with 9DFC conitnue button and login

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26168
  Scenario Outline: $9FC_Enrollment Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then user clicks on the continue with 9DFC conitnue button and continue with std fare

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26169
  Scenario Outline: $9FC_Enrollment_Sign Up_Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then user clicks on the continue with 9DFC conitnue button and signup then continue with std fare

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26170
  Scenario Outline: $9FC_Enrollment_Sign Up_Continue with Standard Fares
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user adds the one carryon and one checked bags
    Then user clicks on the continue with 9DFC conitnue button and login then continue with std fare

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26385
  Scenario Outline: CONTINUE BAGS BUTTON (WITHOUT BAGS)
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user clicks on the continue without adding bags
    Then user continue without adding bags with I do not need bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24559
  Scenario Outline: Multi Pax_RT_Bundles Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on choose this bundle
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user enters the personal information of adult passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price with bundle
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user validates the caryon bag price of passenger2
    Then user Click on the plus button next to checked bags for passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    Then user validates the bags total with the shopping cart for two passengers with bundle
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24560
  Scenario Outline: Multi Pax_RT_Bare Fare
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24561
  Scenario Outline: Single Pax_RT_Thrills Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on choose thrills combo
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user validates the caryon bag price when choose thrills combo
    Then user Click on the minus button next to carryon bag
    Then user adds the one carryon and one checked bags
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary with thrills combo
    Then user clicks on the continue with adding bags
    #When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24562
  Scenario Outline: Single Pax_RT_Bundles Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on choose this bundle
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    #Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price with bundle
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user validates the caryon bag price of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary
    Then user validates the bags total with the shopping cart
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC26705
  Scenario Outline: Military_Multi Pax_OW_Lap Child
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of one child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
   Then user enters the personal information of Lap child
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the plus button next to checked bags for military member
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of military multipax
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26813
  Scenario Outline: Multi Pax_Lap Child_Bundle Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of one child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose this bundle
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
   Then user enters the personal information of Lap child
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    #Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price with bundle
    Then user Click on the plus button next to checked bags
    #Then user validates the caryon bag price of passenger2
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26814
  Scenario Outline: Multi Pax_Lap Child_Thrills Combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of one child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user enters the personal information of Lap child
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    #Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price when choose thrills combo
    Then user Click on the plus button next to checked bags
    #Then user validates the caryon bag price of passenger2
    #Then user Click on plus button next to checked bags for passenger2
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26706
  Scenario Outline: Military_Multi Pax_RT_Lap Child
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of one child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
      Then user enters the personal information of Lap child
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the minus button next to carryon bag
    Then user Click on the plus button next to checked bags
    Then user Click on the plus button next to checked bags of passenger2
    Then User validates save money of 9DFC member in itinerary
    Then user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers
    Then user validates the bags total with the shopping cart for two passengers
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24567
  Scenario Outline: Military_Single Pax_RT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user Click on the plus button next to checked bags for military member
    Then user Click on the plus button next to carryon bag for return trip
    Then user validates the caryon bag price for return trip
    Then user Click on the plus button next to checked bags for military member for return trip
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of military
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC24569
  Scenario Outline: Military_Multi Pax_RT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user enters the personal information of military passenger two
    Then user clicks in the continue button when log in as military
    When user landing on the bags page
    Then validate city pair of passenger
    Then user Click on the plus button next to carryon bag
    Then user validates the caryon bag price
    Then user validates the caryon bag price of military passenger2
    Then user Click on the plus button next to checked bags for military member
    Then user Click on the plus button next to checked bags for military passenger2
    Then User validates save money of 9DFC member in itinerary with bundle
    Then user validates the bags total with the shopping cart of multi military members
    Then user clicks on the continue with adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |
