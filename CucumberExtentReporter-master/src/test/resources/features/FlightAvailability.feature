Feature: Flight Availability

  #partially covered
  @EPIC3021
  Scenario: Flight Availability CP Flight Only Stops link populating the Flight Information popup Wireframe and Functionality
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the Nonstop button and switch to the popup validate the flight details
    Then user clicks on the onestop button and switch to the popup validate the flight details
    Then user clicks on the Twostop button and switch to the popup validate the flight details
    Then user clicks on the Threestop button and switch to the popup validate the flight details
    Then User closes the browser

  #Partially covered
  @EPIC2826
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate each item and its functionality on the Advanced Search Widget
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user verifies the Radio button options for the booking
    Then user selects the one way type of the booking
    Then user validate the dollar and miles are present
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the number of passengers By clikcing on the drop down
    Then user verify the promo code link is present
    Then user Mouse over on the tool tip and validate the text
    Then user clicks on the search flight button

  #Partially covered
  @EPIC3018
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the Young Travelers Popup displays properly for one ADT pax w multi CHD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the one adult and seven child passengers
    Then user clicks on the serach flight switch to the popup and enters the children date of birth

  #Partially covered
  @EPIC3020
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the Young Travelers Popup displays properly for one ADT pax one CHD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the one adlut and one child passenger
    Then user clicks on the serach flight switch to the popup and enters the child date of birth and click on continue button

  #Partially covered
  @EPIC3022
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the Young Travelers Popup displays properly for Single UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the one UMNR passneger
    Then user switch to the popup and enters the date of birth of the child and clicks on the continue button and again switch to the UMNR popup and clicks on accept

  #Partially covered
  @EPIC3023
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the Young Travelers Popup displays properly for Multi UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the seven child passengers
    Then user clicks on the serach flight switch to the popup and enters the children date of birth and agian switch to the unaccompained minor popup and clicks on accept button

  #Partially covered
  @EPIC3028
  Scenario: Flight Availability CP BP Advanced Search Widget Wireframe Validate the ADVANCED SEARCH LINK is present under SEARCH button
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page

  #Then user clicks on the advanced search button below the search button
  #Partially covered
  @EPIC3032
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate items related to the Promo Code
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user verifies the Radio button options for the booking
    Then user selects the one way type of the booking
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user selects the number of passengers By clikcing on the drop down
    Then user verify the promo code link is present
    Then user Mouse over on the tool tip and clicks on the signup now button and navigate to the and navigate to the account enrolment window and switch back to the flight availability page

  #Then user clicks on the add promocode link in flight availability page
  #Partially covered
  @EPIC3259
  Scenario: Flight Availability CP BP Advanced Search Widget Validate the passenger required popup appears when adult and child are set to zero simultaneously
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user verifies the Radio button options for the booking
    Then user selects the one way type of the booking
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user clicks on the passengers and selects zero passenger
    Then user clicks on the search flight and switch to the popup and close the popup window

  @EPIC5314
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate only North America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the From city and choose North America region and select the Departure city
    Then user clicks on the To city and chosse North America region and select the arrival city

  @EPIC5318
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate only Central America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the From city and choose Central America region and select the Departure city
    Then user clicks on the To city and chosse Central America region and select the arrival city

  @EPIC5321
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate only South America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the From city and choose South America region and select the Departure city
    Then user clicks on the To city and chosse South America region and select the arrival city

  @EPIC5324
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate only Caribbean Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the From city and choose caribbean region and select the Departure city
    Then user clicks on the To city and chosse caribbean region and select the arrival city

  #Partially covered
  @EPIC5327
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate Airport Code prediction in FROM-TO fields
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the From city text field
    Then user send the keys to the from city input box and select the departure city
    Then user clicks on the TO city text field

  #Then user enters the invalid text and validate the error message
  @EPIC13401
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the OW INT travel message pops up for DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    #Then user clicks on the advanced search button below the search button
    Then user selects the Domestic city as a from city
    Then user selects international city as a To city
    Then user clicks on the search flight button and switch to the popup and get the text on that popup

  @EPIC23664
  Scenario: Flight Availability CP BP Advanced Search Widget Functionality Validate the OW INT travel message pops up for INT-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user selects the international city as a from city
    Then user selects the domestic city as a To city
    Then user clicks on the search flight button and switch to the popup and get the text on that popup

  @EPIC26374
  Scenario Outline: Flight Availability CP BP Selecting Bare Fare
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clciks on the continue button and switch to the popup and selects the standard barefare
    When user lands on passengers page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26377
  Scenario Outline: Flight Availability CP BP Selecting The Bundle
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clciks on the continue button and switch to the popup and selects the choose bundle
    When user lands on passengers page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26378
  Scenario Outline: Flight Availability CP BP Selecting The Bundle
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clciks on the continue button and switch to the popup and selects the Thrills combo
    When user lands on passengers page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2825
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate each item and its functionality on the Basic Search Widget
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user clicks on the search flight button

  @EPIC3026
  Scenario: Flight Availability CP BP Basic Search Widget Wireframe Validate the NEW SEARCH action BUTTON presence and location
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user validates the New Search button is displayed

  @EPIC3027
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate a BASIC SEARCH WIDGET expands when the New Search button is clicked
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user validate the search widget fields on flight availability page

  @EPIC3173
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate only North America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the From city and choose North America region and select the Departure city
    Then user clicks on the To city and chosse North America region and select the arrival city

  @EPIC3174
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate only Central America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the From city and choose Central America region and select the Departure city
    Then user clicks on the To city and chosse Central America region and select the arrival city

  @EPIC3176
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate only South America Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the From city and choose South America region and select the Departure city
    Then user clicks on the To city and chosse South America region and select the arrival city

  @EPIC3178
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate only Caribbean Destinations are shown when region is selected
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the From city and choose caribbean region and select the Departure city
    Then user clicks on the To city and chosse caribbean region and select the arrival city

  @EPIC3235
  Scenario: Flight Availability CP BP Basic Search Widget Functionality Validate Airport Code prediction in FROM-TO fields
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the From city text field
    Then user send the keys to the from city input box and select the departure city
    Then user clicks on the TO city text field

  #partially covered
  @EPIC3017
  Scenario: Flight Availability CP BP Flight Only Validate the Only seats available message shows when there are Nine or less seats remaining
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user clicks on the search flight button

  @EPIC13328
  Scenario Outline: Customer Info_CP_BP_Have one PAX with pet in the cabin as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13346
  Scenario Outline: Customer Info_CP_BP_NEG_Have two PAX with the same name as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user enters the personal information for Second adult same name as first pax
    Then user clicks in the continue button

    #    Then User verifys error message for pax that have the same name
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21129
  Scenario Outline: Customer Info_CP_BP_PAX with a wheelchair and sevice animal for 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then User Selects Need Help ToFrom gate
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered Drycell Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21131
  Scenario Outline: Customer Info_CP_BP_ UMNR who has a service animal for 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR NineDFC member
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    #When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21169
  Scenario Outline: Customer Info_CP_BP_NEG_Have two PAX with the same name as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user enters the personal information for Second adult same name as first pax
    Then user clicks in the continue button

    #    Then User verifys error message for pax that have the same name
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21170
  Scenario Outline: Customer Info_CP_BP_9DFC member Have two PAX with the same name but with SR and JR suffix
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user enters the personal information for Second adult same name as first pax
    Then user adds SR and Jr suffix
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23575
  Scenario Outline: Customer Info_CP_BP_9DFC member logged in before customer info page verify suppressed content
    Given Spirit airlines application
    Then user login as NineDFC member
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then User Verify that the inline login block is suppressed
    Then User Verify Inline enrollment is not present
    Then user enters the personal information for Second adult
     Then user selects the primary passenger contact checkbox
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23578
  Scenario Outline: Customer Info_CP_BP_9DFC member with apostrophe in there name
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member with aspostrophe in there name
    Then user verify First middle and last name autopopulated with apostrophe
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23612
  Scenario Outline: Customer Info_CP_BP_NEG_UMNR logged in as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR NineDFC member
    Then user verify Umnr dob autopopulated
    Then user clicks in the continue button

    # 	Then user verify young traveler popup
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23655
  Scenario Outline: Customer Info_CP_BP _9DFC member Verify 15 years or older UMNR is treated as a adult
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR who is fifthteen years old
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR NineDFC member
    #	Then user verify Active Duty military personnel is suppressed
    Then user clicks in the continue button
   

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25257
  Scenario Outline: Customer Info_CP_BP_Have two PAX with the same name but with SR and JR suffix where the 9DFC member had the SR already
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a suffix attached
    Then user enters the personal information for Second adult same name as first pax
     Then user selects the primary passenger contact checkbox
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25258
  Scenario Outline: Customer Info_CP_BP_9DFC member Have two PAX with the same name but with SR and JR suffix with the same birthday
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a suffix attached
    Then user enters the personal information for Second adult same name as first pax
     Then user selects the primary passenger contact checkbox
    Then user inputs different DOB for second Pax
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25263
  Scenario Outline: Customer Info_CP_BP_9DFC member with a extra seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a suffix attached
    Then user enters the personal information for Second adult same name as first pax
     Then user selects the primary passenger contact checkbox
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27674
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair need help to from gate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom gate
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27675
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair wet cell option
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27676
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair help to seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom Seat
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23777
  Scenario Outline: Customer Info CP BP SinglePax NEG As a FS member Entering incorrect FS number on the FS text box
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information for Second adult
    Then user enters eight digits for the FS numbers for Second adult
    Then user clicks in the continue button
    # Then user validates FS error message
    Then user enters ten digits for the FS numbers for Second adult
    Then user clicks in the continue button

    #Then user validates FS error message
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25260
  Scenario Outline: Customer Info CP BP FS member who has two PAX with the same name but with SR and JR suffix make
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information for Second adult same name as first pax
    Then user selects suffix for passenger two
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25261
  Scenario Outline: Customer Info CP BP FS member Have two PAX with the same name but with SR and JR suffix with the same birthday
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information for Second adult same name as first pax
    Then user selects suffix for passenger two
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25265
  Scenario Outline: Customer Info CP BP FS member with a extra seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the adult name and DOB same as first adult
    Then user selects suffix for passenger two
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25273
  Scenario Outline: Customer Info CP BP Enter information for a FS Pax then login
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user login as FS member
    Then user verifies Member is logged in
    Then user verfies all information is correct
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    #Then user goes to Personal Information
    #Then user verfies Name and DOB
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

#partially completed
  @EPIC25638
  Scenario Outline: Customer Info CP BP As a FS member Car Booking with flight
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then Vacation User choose the from FLL City
    Then Vacation User choose the To LGA city
    Then user selects departure date and return date for Vacation
    Then user selects the two adult passengers
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the secondary passenger information
    Then User verifys Primary driver dropdown

    Examples: 
      | Required stops |
      | 1 Stop         |

#partially completed
  @EPIC25639
  Scenario Outline: Customer Info CP BP As a FS member Car Booking with flight
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then Vacation User choose the from FLL City
    Then Vacation User choose the To LGA city
    Then user selects departure date and return date for Vacation
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then User verifys Primary driver dropdown does not appear

    Examples: 
      | Required stops |
      | 1 Stop         |

#partially completed
  @EPIC27310
  Scenario Outline: MultiPax log in As a FS member Select one Wheelchair on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #################
  @EPIC25268
  Scenario Outline: Customer Info_CP_BP_Enter information for a 9DFC Pax then login
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user login as nine DFC member
    Then user verfies all information is correct for 9dfc
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user goes to Personal Information
    Then user verfies Name and DOB
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

#partially completed
  @EPIC25634
  Scenario Outline: Customer Info_CP_BP_As a 9DFC member Car Booking with flight
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then Vacation User choose the from FLL City
    Then Vacation User choose the To LGA city
    Then user selects departure date and return date for Vacation
    Then user selects the two adult passengers
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user enters the secondary passenger information
    Then User verifys Primary driver dropdown

    Examples: 
      | Required stops |
      | 1 Stop         |

#partially completed 
  @EPIC25635
  Scenario Outline: Customer Info_CP_BP_As a 9DFC member Car Booking with car only
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then Vacation User choose the from FLL City
    Then Vacation User choose the To LGA city
    Then user selects departure date and return date for Vacation
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then User verifys Primary driver dropdown does not appear

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27677
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair dry or gel cell battery
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered Drycell Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27678
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair manual
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27684
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair need help to from gate and manual wheelchair
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom gate
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27779
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair wet cell option with a pet in cabin
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    #Then user Clicks Pet In Cabin
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26952
  Scenario: Flight Availability CP BP Modify Basic Search POO POD Date
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the New Search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user clicks on the search flight button

  @EPIC26953
  Scenario: Flight Availability CP BP Modify Advanced Search Trip POO POD Date Pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    #Then user clicks on the advanced search button below the search button
    Then user clicks on the New Search button
    Then user clicks on the from down arrow and select the city
    Then user clicks on the To down arrow and select the city
    Then user select the date by clicking the down arrow
    Then user clicks on the search flight button
    
      @EPIC3255
  Scenario: Basic Search Widget_Functionality_Validate ALL Destinations are shown when no region is selected
  Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User counts number of origin cities
   Then User counts number of destination cities
