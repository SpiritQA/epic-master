Feature: Plane 319 model test

  @Epic23888
  Scenario Outline: Three Ninteen plane model seating configuration for CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user selects the has car seat checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the standard seat
    Then user trying to selects the Exit row and Big front seat for the child but it should be blocked
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23894
 
  Scenario Outline: Three Ninteen plane model seating configuration for BLND [Epic23894]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23893

  Scenario Outline: Three Ninteen plane model seating configuration for DEAF [EPIC23893]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on additional services and selects the Hearing disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23889
 
  Scenario Outline: Three Ninteen plane model seating configuration for ESAN  [EPIC23889]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the emotional psychiatric support animal
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23891
  Scenario Outline: Three Ninteen plane model seating configuration for INFT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
   Then user enters the infant passneger information 
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23892
  Scenario Outline: Three Ninteen plane model seating configuration for POCS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects clicks on the additional services and select the portable oxygen concentrators
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23890
  Scenario Outline: Three Ninteen plane model seating configuration for SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the service animal check box
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23895
  Scenario Outline: Three Ninteen plane model seating configuration for UNMR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the UMNR passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @Epic23916
  Scenario Outline: Three Ninteen plane model seating configuration for WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the battery powered dry
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23896
  Scenario Outline: Three Ninteen plane model seating configuration for WCBW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then primary passneger clicks on the additional services and selects the i have my own wheel chair
    Then user enters the secondary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23899
  Scenario Outline: Three Ninteen plane model seating configuration for WCHC
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    When user clicks on the additional services and select need help from seat
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23897
  Scenario Outline: Three Ninteen plane model seating configuration for WCHR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services button and selects the completely immobile
    Then user enters the secondary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23900
  Scenario Outline: Three Ninteen plane model seating configuration for WCMP
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services button and selects wheel chair manually powered
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23907
  Scenario Outline: Three Ninteen plane model seating configuration with two passnegers checkin path BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the secondary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled and select any random seat"<seat Count>"
    Then user clicks on the continue button on the seats page and switch to the popup and selects the i dont need bags
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | seat Count |
      |          2 |

  # -------------------> Manage Travel path <----------------------------------
  @EPIC23952
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23946
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user selects the has car seat checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23951
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path DEAF
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on additional services and selects the Hearing disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23947
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the emotional psychiatric support animal
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23949
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path INFT [EPIC23949]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the lap child
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the infant passneger information 
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23950
  
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path POCS [EPIC23950]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects clicks on the additional services and select the portable oxygen concentrators
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23948
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the service animal check box
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23953
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the UMNR passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23959
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the battery powered dry
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23954
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path WCBW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then primary passneger clicks on the additional services and selects the i have my own wheel chair
    Then user enters the secondary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23957
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path WCHC
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    When user clicks on the additional services and select need help from seat
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23956
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path WCHR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services button and selects the completely immobile
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23958
  Scenario Outline: Three ninteen plane model seating configuration with one adult passenger manage travel path WCMP
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To ATL city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services button and selects wheel chair manually powered
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

      
    #------------------------------> CHEKIN PATH <--------------------------------------- 
    
    
    