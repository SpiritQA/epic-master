Feature: verify user able to book onewaytrip ticket scenario

  @tag785
  Scenario Outline: verify user able to book the oneway tip ticket
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser
    Then user save the generated pdf

    Examples: 
      | Required stops |
      | Nonstop         |

  @EPIC2854
  Scenario: Search Widget CP BP One Way Multi PAX with ADT and Children International
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then user clicks on the deparure city
    Then user selects the Domestic city as a departure city
    Then user clicks on the To city
    Then user selects international city as a arriaval city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then User clicks on Search button and switch to the popup above the international field and clicks on OK button then switch to the child popup and enters the date of birth

  @EPIC2890
  Scenario: Search Widget CP BP one PAX with Lap child DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on the search flight button and switch to the popup enters the child date of birth less than seven days old
    Then user clicks on the search flight and switch to the popup enters the child date of birth under two years selects lap child

  @EPIC2911
  Scenario: Search Widget BP CP Multi PAX
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user clicks on the search flight button

  @EPIC2915
  Scenario: Search Widget CP BP one Pax and child with seat, DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on the search flight and switch to the popup enters the child date of birth under two years selects lap child

  @EPIC2923
  Scenario: Search Widget CP BP UMNR DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the child under five years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button
    Then user selects the child age between five to fourteen years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button

  @EPIC2968
  Scenario: Search Widget CP BP Muti Pax with children DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects two adult and children passenegers
    Then user clicks on the searchflight and switch to the popup and enters the DOB of the children and clicks on the continue button

  @EPIC3471
  Scenario: Search Widget CP BP one PAX OW-RT DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button

  @EPIC3589
  Scenario: Search Widget CP BP Miles
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user clicks on the Miles tab next to the dollar tab and login as free spirit member
    Then user selects departures date
    Then user selects the Nine adult passengers
    Then User clicks on Search button

  @EPIC2946
  Scenario: Search Widget CP BP RT UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then user verifies that round trip is selected by default
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects the child under five years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button
    Then user selects the child age between five to fourteen years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button

  @EPIC26807
  Scenario: Search Widget CP BP RT one PAX with Lap Child DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then user verifies that round trip is selected by default
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects the one child and one adult passengers
    Then user clicks on the search flight and switch to the popup enters the child date of birth under two years selects lap child

  @EPIC3491
  Scenario: Search Widget CP BP RT one PAX with Lap Child DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    Then user verifies that round trip is selected by default
    Then user clicks on the deparure city
    Then user selects the Domestic city as a departure city
    Then user clicks on the To city
    Then user selects international city as a arriaval city
    Then user selects departure date and return date
    Then user selects the one child and one adult passengers
    Then user clicks on the search flight and switch to the popup enters the child date of birth under two years selects lap child

  @EPIC27076
  Scenario: Search Widget CP BP Multi PAX with ADT and Children INT
    Given Spirit airlines application
    When User Clicks on Book link
    Then user verifies that round trip is selected by default
    Then user selects the departure city as a Aruba
    Then user select the arrival city as a canucun Mexico
    Then user selects departure date and return date
    Then user selects the one child and one adult passengers
    Then user clicks on the searchflight button and switch to the popup and enters the child DOB and clicks on continue button

  @EPIC23656
  Scenario Outline: Start booking with UNMR fifteenyears of age or older and validate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth for UNMR fifteen and older
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then User Clicks on the x on thrills combo pop up
    When user lands on passengers page
    Then user enters the UMNR passenger information and changes the DOB
    Then user validates that the Actvie duty Military check box is suppressed
    Then user enters the contact information
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2888
  Scenario: Search Widget BP CP OW Invalid Promo Code
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then user Mouse over on the tool tip and validate the text
    Then user clicks on the promocode link
    Then user send the invalid promocode
    Then user clicks on the search flight button and switch to the popup and validate the text then clicks on the edit promocode
    Then user clicks on the search flight button and switch to the popup and clicks on the close button
    Then user clicks on the serach flight button and switch to the popup and clicks on the continue without the coupon code
    When user lands on flight page

  #partially covered
  @EPIC23533
  Scenario Outline: Search Widget BP CP OW Valid Promo Code for dollers
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user select today's date as a departure date
    Then User selects number of passengers
    Then user clicks on the promocode link and enters the valid promocode
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    #Then user clicks on the flight caret and check the promo code price between the two flights
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    #Then user clciks on the flight caret button on the confirmation page and check the promocode price is applied
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially covered
  @EPIC26613
  Scenario Outline: Search Widget BP CP OW Valid Promo Code for Percent off
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then user clicks on the promocode link and enters the valid promocode for percent off
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    #Then user clicks on the flight caret and check the promo code price between the two flights
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    #Then user clciks on the flight caret button on the confirmation page and check the promocode price is applied
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26620
  Scenario: Search Widget BP CP RT Invalid Promo Code
    Given Spirit airlines application
    Then user clicks on the Multicity Radio button
    Then user clicks on the from city on first city pairs
    Then user selects the from city on first city pairs
    Then user clicks on the to city on first city pairs
    Then user selects the to city on first city pairs
    Then user enters the date of travel for first city pairs
    Then user clicks on the from city on second city pairs
    Then user selects the from city on second city pairs
    Then user clicks on the to city on second city pairs
    Then user selects the to city on second city pairs
    Then user enters the date of travel on second city pair
    Then User selects number of passengers
    Then user mouse over on the question mark and switch to the popup and get text
    Then user clicks on the promocode link and enters the invalid promocode
    Then user clicks on the search flight button and switch to the popup and validate the text then clicks on the edit promocode
    Then user clicks on the search flight button and switch to the popup and clicks on the close button
    Then user clicks on the serach flight button and switch to the popup and clicks on the continue without the coupon code
    When user lands on flight page

  #partially covered
  @EPIC26621
  Scenario Outline: Search Widget BP CP RT Valid Promo Code for dollers
    Given Spirit airlines application
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then user clicks on the promocode link and enters the valid promocode
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    #Then user clicks on the flight caret and check the promo code price between the two flights
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    #Then user clciks on the flight caret button on the confirmation page and check the promocode price is applied
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  @EPIC26622
  Scenario Outline: Widget BP CP Multi Valid Promo Code for Percent off
    Given Spirit airlines application
    Then user clicks on the Multicity Radio button
    Then user clicks on the from city on first city pairs
    Then user selects the from city on first city pairs
    Then user clicks on the to city on first city pairs
    Then user selects the to city on first city pairs
    Then user enters the date of travel for first city pairs
    Then user clicks on the from city on second city pairs
    Then user selects the from city on second city pairs
    Then user clicks on the to city on second city pairs
    Then user selects the to city on second city pairs
    Then user enters the date of travel on second city pair
    Then User selects number of passengers
    Then user mouse over on the question mark and switch to the popup and get text
    Then user clicks on the promocode link and enters the valid promocode for percent off
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    #Then user clicks on the flight caret and check the promo code price between the two flights
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    #Then user clciks on the flight caret button on the confirmation page and check the promocode price is applied
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops | Return flight stops |
      | 2 Stop         | 1 stop              |

  @EPIC3361
  Scenario Outline: Check the Reservation Summary and make sure customer info page info is correct
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date outside of twenty four hours
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then User Validates the Contact info in the reservation summary page
    Then click the EDIT link next to the contact info
    When User lands on my trips Passenger info Page
    Then User Validates all of the Contact Information Fields to make sure they are all correct
    Then User Clicks the Cancel Changes button and is redirected back to the Reservation Summary page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3377
  Scenario Outline: Check the Reservation Summary and attempt to make changes to reservation summry without saving the new info
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date outside of twenty four hours
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then User Validates the Contact info in the reservation summary page
    Then click the EDIT link next to the contact info
    When User lands on my trips Passenger info Page
    Then User Changes the contact information fields
    Then User Clicks the Cancel Changes button and is redirected back to the Reservation Summary page
    Then User Validates the Contact info in the reservation summary page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3377
  Scenario Outline: Check the Reservation Summary and make changes to reservation summry with saving the new info
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date outside of twenty four hours
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then User Validates the Contact info in the reservation summary page
    Then click the EDIT link next to the contact info
    When User lands on my trips Passenger info Page
    Then User Changes the contact information fields
    Then User Clicks the Save Changes button and is redirected back to the Reservation Summary page
    Then User Validates the Contact info was changed in the reservation summary page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3381
  Scenario Outline: Check the Reservation Summary and make changes to reservation by erasing info and attempting to save
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date outside of twenty four hours
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then User Validates the Contact info in the reservation summary page
    Then click the EDIT link next to the contact info
    When User lands on my trips Passenger info Page
    Then User Erases the first name and attempts to save changes and is promtped by a required field
    Then User Erases the last name and attempts to save the changes and is promtped by a required field
    Then User Erases the Address and attempts to save the changes and is promtped by a required field
    Then User Erases the City and attempts to save the changes and is promtped by a required field
    Then User Erases the State and Zip Code and attempts to save the changes and is promtped by a required field
    Then User Erases the Email and Email Confirmation and attempts to save the changes and is promtped by a required field
    Then User Enters an Invalid Email and invalid Email Confirmation and clicks the save button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3544
  Scenario: Verify the seat link on the options and extras page
    Given Spirit airlines application
    Then User clicks on the New Bag prices and Optional services link
    When User Lands on the Optional Services Page
    Then User Clicks on the Seats Link next to bags memberships and other
    Then User Validates the Verbiage Under the Seats Tab

  @EPIC3583
  Scenario: Verify the Memberships link on the options and extras page
    Given Spirit airlines application
    Then User clicks on the New Bag prices and Optional services link
    When User Lands on the Optional Services Page
    Then User Clicks on the Memberships Link next to bags seats and other

  @EPIC25798
  Scenario: Click on the learn more link on the home page and be redirected to the spirit101 page
    Given Spirit airlines application
    Then User clicks on the Learn more buttons towards the bottom of the page
    When User lands on the Spirit101 page
    Then User validates the Spirit Gives You More Go for less money
    Then User Validates the See how your bag sizes up link
    Then User validates the Check Out These Prices Link
    Then User Validates the See printing Details
    Then User validates the Get Check In APP
    Then Validate the Get More Go In Your Inbox Header
    Then Validate the Get More Go In Your Inbox Header Icon email and social media Links
    Then Validate the BECOME A FREE SPIRIT MEMBER Link
    Then Validate The GET MORE GO link
    Then Validate the SEE FREEQUENTLY ASKED QUESTIONS
    Then Validate the SEE OUR LATEST DEALS
    Then Validate the SHARE YOUR MORE GO STORY
    Then validate SEE WHERE WE FLY

  @EPIC26038
  Scenario: Click on the New Bag Prices and Optional Services link on the home page and be redirected to the Optional Services page and validate the Other Tab
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on the New Bag prices and Optional services link
    When User Lands on the Optional Services Page
    Then User clicks on the Other Tab Link
    Then User Validates the Verbiage Under The TAB MENU When The OTHER Tab Link Is Clicked
    Then User Clicks on the Onboard Snacks and Drinks Carrot and Validates all of the verbiage
    Then User Clicks on the Booking Related Carrot and Validates all of the verbiage
    Then User Clicks on the FREE SPIRIT AWARD BOOKING Carrot and Validates all of the verbiage
    Then User Clicks on the Modification Or Cancellation Carrot and Validates all of the verbiage
    Then User Clicks on the Extras Carrot and Validates all of the verbiage
    Then User Validates the Print Your Boarding Pass at Home Tool Tip
    Then User Validates the Boarding pass printed by Airport Kiosk tooltip
    Then User Validates the Boarding pass printed by Airport Agent tooltip
    Then User Validates the Unaccompanied Minors Price includes snack and beverage tooltip
    Then User clicks on the Other Tab Link
    Then User Validates Infant Lap Child tooltip
    Then User clicks on the Other Tab Link
    Then User Validates the Pet Transportation Limit 4 pets total in cabin tooltip
    Then User clicks on the Other Tab Link
    Then User Validates Travel Guard Insurance Domestic Itinerary tooltip
    Then User clicks on the Other Tab Link
    Then User Validates the Travel Guard Insurance International Itinerary tooltip
    Then User clicks on the Other Tab Link
    Then User Validates Travel Guard Insurance Vacation Package Itinerary tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Onboard Snacks and Drinks Carrot
    Then User Validates the Snacks tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Onboard Snacks and Drinks Carrot
    Then User Validates the Drinks tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Booking Related Carrot
    Then User Validates Reservation Center Booking Including All Packages tooltip
    Then User Vlaidates Group Booking tooltip
    Then User Validates Colombia Administrative Charge tooltip
    Then User Validates Stand By for Earlier Flight tooltip
    Then User Validates Passenger Usage Charge tooltip
    Then User Validates Regulatory Complaiance Charge tooltip
    Then User Validates Fuel Charge tooltip
    Then User Clicks on the FREE SPIRIT AWARD BOOKING Carrot
    Then User Validates Agent Transaction Reservation Center tooltip
    Then User Validates Award Redemption tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the FREE SPIRIT AWARD BOOKING Carrot
    Then User Validates Award Modification tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the FREE SPIRIT AWARD BOOKING Carrot
    Then User Validates Mileage Redeposit tooltip
    Then User Clicks on the Modification Or Cancellation Carrot
    Then User Validates Web Modification or Cancellation tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Modification Or Cancellation Carrot
    Then User Validates Reservations Airport Modification or Cancellation tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Modification Or Cancellation Carrot
    Then User Validates Group Booking Itinerary Modification or Cancellation tooltip
    Then User clicks on the Other Tab Link
    Then User Clicks on the Extras Carrot
    Then User Validates the Flight Flex tooltip
    Then User Validates the Shortcut Boarding tooltip
