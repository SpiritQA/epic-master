Feature: Book feature

  #@tag122
  #Scenario: verify user able to book the round trip ticket
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departure date and return date
  #Then User selects number of passengers
  #Then User enters promo code
  #Then User clicks on Search button
  #Then User closes the browser
  #
  #@tag123
  #Scenario: verify user able to book the oneway trip ticket
  #Given Spirit airlines application
  #When User Clicks on Book link
  #Then User clicks on onewaytrip
  #Then User choose the from city
  #Then User choose the To city
  #Then user selects departures date
  #Then User selects number of passengers
  #Then User enters promo code
  #Then User clicks on Search button
  #Then User closes the browser
  #
  #Scenario: verify user able to checkin
  #Given Spirit airlines application
  #When User Clicks on checkin link
  #Then User enters the passengers last name
  #Then User enters the conformation code
  #Then User clicks on submit button
  #
  #  Then User closes the browser
  #Scenario: verify user able to check Mytrips
  #Given Spirit airlines application
  #When User Clicks on Mytrips link
  #Then User enters the passengers lastname
  #Then User enters the passenger PNR
  #Then User clicks on submitbutton
  #Then User closes the browser
  #######################################STAND ALONE [4374] ###############################################
  @EPIC3035
  Scenario: Testing the Passengers section on the home page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects maximum number of adult passengers
    Then User selects maximum number of child passengers
    Then user clicks Search and passengers required popup prompts

  @EPIC21165
  Scenario: Lap child popup, Infants exceeds the number of adults(edit)
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the two child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child

  @EPIC26619
  Scenario: RT_Valid Promo Code for Percent off (EDIT)
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then user verify add a promo code link is present
    Then user Mouse over on the promo tool tip and validate the text

  @EPIC26617
  Scenario: RT Invalid Promo Code
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then user mouse over on the question mark and switch to the popup and get text
    Then user clicks on the promocode link and enters the invalid promocode
    Then user clicks on the search flight button and switch to the popup and validate the text then clicks on the edit promocode
    Then user clicks on the search flight button and switch to the popup and clicks on the close button
    Then user clicks on the serach flight button and switch to the popup and clicks on the continue without the coupon code
    When user lands on flight page

  @EPIC26618
  Scenario: RT_Valid Promo Code for dollers
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then user mouse over on the question mark and switch to the popup and get text
    Then user clicks on the promocode link and enters the valid promocode
    Then User clicks on Search button

  #When user lands on flight page
  @EPIC26456
  Scenario: No Car Model
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates within 48 hours
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button and validate no car popup

  @EPIC23561
  Scenario: Car Unavailable Modal
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button and validate no car popup

  @EPIC23545
  Scenario: Seasonal Service(Edit)
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city of Los Cabos
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then user clicks on search button and validate seasonal service popup

  @EPIC3593
  Scenario: RT_Interact with Miles login Popup
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    When user click miles tab on the right corner and expects login popup
    Then user navigates to home page and booking again
    When User Clicks on Book link
    Then User choose the from city
    Then User choose the To city
    Then user selects departure date and return date
    Then User selects number of passengers
    Then user clicks signup and redirects to account enrollment page
    Then user navigates to home page and booking again
    Then user click miles tab and validate email address and password from login in popup

  @EPIC21174
  Scenario Outline: NEG_FS member forgot password FS number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks reset password link under log in
    Then user enter incorrect FS number and validates retrieve password

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21175
  Scenario Outline: NEG_FS member forgot password FS number & email
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user scroll down to Account section and enetrs the information and member must have the apostrophein there first middle and last name
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks reset password link under log in
    Then user enter incorrect FS number and validates retrieve password

    Examples: 
      | Required stops |
      | 1 Stop         |

  #Partially completed
  @EPIC21176
  Scenario Outline: NEG_FS member forgot password email & FS number
    Given Spirit airlines application
    #    When user clicks on the signin button and switch to the popup and clicks on the signup button
    #    Then user scroll down to Account section and enetrs the information and member must have the apostrophein there first middle and last name
    #    Then user clicks on the continue to step two button
    #    Then user navigates to the contact section and enters the required information
    #    Then user clicks on the Term and conditions check box
    #    Then user clicks close signup close button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks reset password link under log in
    Then user enter incorrect FS email and validates retrieve password

    #Then user login to gmail and retrieve password
    Examples: 
      | Required stops |
      | 1 Stop         |

  #Partially completed
  @EPIC21177
  Scenario Outline: NEG_FS member forgot password email & FS number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks reset password link under log in
    Then user enter valid FS email and validates retrieve password

    Examples: 
      | Required stops |
      | 1 Stop         |

  #Partially completed
  @EPIC21178
  Scenario Outline: FS member forgot password FS number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks reset password link under log in
    Then user enter valid FS number and validates retrieve password

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23605
  Scenario Outline: 9DFC logging out using why cant you edit your name link
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as 9DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Click the Why can't you edit your name link and verify modal popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23608
  Scenario Outline: FS logging out using why cant you edit your name link
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user verifies whether Free Spirit member is logged in
    Then user Click the Why can't you edit your name link and verify modal popup

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23775
  Scenario Outline: NEG_As a 9DFC member Entering incorrect FS number on the FS text box
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks 9fc continue then switchto popup and continue
    When user lands on passengers page
    Then user enters the personal informations of passenger two of FS member

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23776
  Scenario Outline: NEG_As a 9DFC member Entering incorrect information for KTN and redress number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies nine DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then User validates FS number and known traveler number then redress number

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC27264
  Scenario: ADT-CHD pop-up modal ages from 0 to17
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the three child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child

  @EPIC27269
  Scenario Outline: Flight Only  1 CHD  14 years old on Domestic Connecting Flights
    Given Spirit airlines application
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then user selects the one child
    Then user clicks on search button and enters age 14 then accept UMNR popup
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info of UMNR under age of 14
    Then User clicks continue button and handle UMNR popup of connecting flights

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 2 stop              |

 @EPIC25702    
  Scenario Outline: Land on Request Milage Page and enters a PNR from a booking made with another account and recieves Error Message
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date outside of twenty four hours
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then User Clicks On The Spirit Logo To Return To The Home Page And Signs In As A Free Spirit Member Different Then Original Booking
    Then user clicks on the  login button on the popup
    Then User clicks on the signed in user located in the header
    Then user clicks on Request Mileage Credit in the Drop Down and Is redirected to the correct URL
    Then user enters the PNR from the original free spirit member generated at the begining of the test
    Then User Clicks the Go Button
    Then User receives error message from inputting a PNR that was made with a different account other than the one currently logged into
    
   Examples: 
      | Required stops |
      | 1 Stop         |
