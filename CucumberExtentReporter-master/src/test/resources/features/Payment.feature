
Feature: Payments Page

  @EPIC3019
  Scenario Outline: CP BP Validating Forms of Payment CreditCard
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the different credit card numbers and validate the card badge
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially completed
  @EPIC13232
  Scenario Outline: CP BP 9FC Saved Credit Card
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user selects the the do not purchase on travel guard content block
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    Then user selects the saved card from the dropdown

    #Then user validates the prechecked billing address
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21141
  Scenario Outline: CP_BP_Payment Page_AMEX
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Amex card information
    Then user clicks on the Booktrip and handle the travel more popup
 Examples: 
      | Required stops |
      | 1 Stop         |
      
  @EPIC21151
  Scenario Outline: CP BP Payment Page Visa
     Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Visa card information
    Then user clicks on the Booktrip and handle the travel more popup
 Examples: 
      | Required stops |
      | 1 Stop         |
      
    
  @EPIC26235
  Scenario Outline: CP BP Payment Page Master Card
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Master card information
    Then user clicks on the Booktrip and handle the travel more popup
 Examples: 
      | Required stops |
      | 1 Stop         |
    
    @EPIC26238
    Scenario Outline: CP BP Payment Page FREE SPIRIT Master Card
     Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Master card information
    Then user clicks on the Booktrip and handle the travel more popup
 Examples: 
      | Required stops |
      | 1 Stop         |
    
  @EPIC26369
  Scenario Outline: CP BP Payment Page Discover Terms and Conditions
  
   Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the discover card information 
    Then user do not agree the terms and conditions and validate the error message 
     Examples: 
      | Required stops |
      | 1 Stop         |
   
      
    ####################################################### MANAGE TRAVEL[1594] ###################################################
@EPIC26770
  Scenario Outline: Validating Forms of Payment_CreditCard
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on the bags page
   Then user clicks on the standard pricing continue button
   When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the different credit card numbers and validate the card badge
   Examples:
     | Required stops |
     | 2 Stop         |
     
@EPIC26771
  Scenario Outline: Validating Forms of Payment_Res Credit_Credit Card
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on payment page
   Then user clicks REDEEM A VOUCHER caret
   Then user enter PNR into Reservation Credit and click go
   Examples:
     | Required stops |
     | 2 Stop         |
     
 @EPIC26773
  Scenario Outline: 9FC Saved Credit Card
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   #Then user clicks on the continue button
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the continue with 9DFC conitnue button and login success
   When user landing on payment page
   Then user selects creditcard which are already saved onfile
   Then user verify billing address matches contact address
   Then user verify booking price
   
   Examples:
     | Required stops |
     | 1 Stop         |
     
@EPIC26775
  Scenario Outline: Payment Page_AMEX
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on payment page
   Then user enters the AMEX credit card number and validate the card badge
   Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
   
   Examples:
     | Required stops |
     | 2 Stop         |
     
@EPIC26776
  Scenario Outline: Payment Page_Visa
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on payment page
   Then user enters the Visa credit card number and validate the card badge
   Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
   
   Examples:
     | Required stops |
     | 2 Stop         |
     
@EPIC26777
  Scenario Outline: Payment Page_Master Card
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on payment page
   Then user enters the Master credit card number and validate the card badge
   Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
   
   Examples:
     | Required stops |
     | 2 Stop         |
     
@EPIC26780
  Scenario Outline: Payment Page_Master Card
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To city
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   Then user get pnr and navigate to checkin entry
   Then user change flights
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user clicks on the standard fare continue button
   When user landing on payment page
   Then user enters the Discover credit card number and validate the card badge
   Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
   
   Examples:
     | Required stops |
     | 2 Stop         |
    