Feature: Free Spirit Member

  @EPIC25659
  Scenario: Free spirit Memeber Email subscriptions and Subscribe to Email Deals [EPIC25659]
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the manage subscription link form the list
    Then user enetres the free spirit member customers email address
    Then user clicks on the continue button below the customers email address
    Then user complete the form and click on the subscribe button
    Then user can see that subscription is created

  #partially completed
  @EPIC21173
  Scenario Outline: Customer Info CP BP NEG FS member forgot password email
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user lands on the passengers page and clicks on the reset the password

    #Then user verifies Member is logged in
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23576
  Scenario Outline: Customer Information CP BP FS member logged in before customer info page verify suppressed content [EPIC23576]
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one  adult passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    #When user lands on the passenger page and validate the terms and conditions text
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  #partially completed
  @EPIC23579
  Scenario Outline: Customer Info Customer portaal Booking Portal FS member with apostrophe in there name
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user scroll down to Account section and enetrs the information and member must have the apostrophein there first middle and last name
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    Then user clicks on the Book now button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one  adult passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the  nine dollar fair club and switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC5865
  Scenario Outline: Customer Info CP BP while log in as a FS member you do not see banner on the bottom
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user clicks on the Book Trip button
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25701
  Scenario Outline: My Account CP FS Request Miles Credit Link Neg Booking not Found
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Request Mileage credit
    Then user enters the invalid conformation code "<inavalid conformation code>"
    Then user clicks on the GO button and switch to the error messge and validate it

    Examples: 
      | inavalid conformation code |
      | GHK23I                     |

  @EPIC13264
  Scenario: FS Enroll _CP_ FS Enroll general
    Given Spirit airlines application
    When User click on footer Free Spirit link
    Then User verify header verbiage on FS sign up page
    Then user verify Title error message
    Then user verify require First Name error message
    Then user verify invalid First Name error message
    Then user verify require Last Name error message
    Then user verify invalid Last Name error message
    Then user verify require Date of Birth error message
    Then user verify invalid Date of Birth error message
    Then user verify require Email error message
    Then user verify invalid Email error message
    Then user verify require Confirm Email error message
    Then user verify invalid Confirm Email error message
    Then user verify require Password error message
    Then user verify less than eight characters invalid Password error message
    Then user verify more than sixteen characters invalid Password error message
    Then user verify lower case eight characters invalid Password error message
    Then user verify lower case eight characters and upper case character invalid Password error message
    Then user verify lower case eight characters, upper case character and numbercial value invalid Password error message
    Then user verify lower case eight characters, upper case character and special character invalid Password error message
    Then user verify require Confirm Password error message
    Then user verify invalid Confirm Password error message
    Then user verify require Address error message
    Then user verify Symbol invalid Address error message
    Then user verify require City error message
    Then user verify Symbol invalid City error message
    Then user verify Number invalid City error message
    Then user verify require State error message
    Then user verify require Zip Code error message
    Then user verify less than four character Zip Code error message
    Then user verify Symbol invalid Zip Code error message
    Then user verify require Primary Mobile Number error message
    Then user verify invalid Primary Mobile Number error message
    Then user verify require Home Airport error message
    Then user verify Terms and Condition error for FS SignUp
    Then user verify FS signup successfully

  @EPIC13265
  Scenario: FS Enroll _CP_ FS Enroll general
    Given Spirit airlines application
    When User click on footer Free Spirit link
    Then User verify header verbiage on FS sign up page
    Then user verify Title error message
    Then user verify require First Name error message
    Then user verify invalid First Name error message
    Then user verify require Last Name error message
    Then user verify invalid Last Name error message
    Then user verify require Date of Birth error message
    Then user verify invalid Date of Birth error message
    Then user verify require Email error message
    Then user verify invalid Email error message
    Then user verify require Confirm Email error message
    Then user verify invalid Confirm Email error message
    Then user verify require Password error message
    Then user verify less than eight characters invalid Password error message
    Then user verify more than sixteen characters invalid Password error message
    Then user verify lower case eight characters invalid Password error message
    Then user verify lower case eight characters and upper case character invalid Password error message
    Then user verify lower case eight characters, upper case character and numbercial value invalid Password error message
    Then user verify lower case eight characters, upper case character and special character invalid Password error message
    Then user verify require Confirm Password error message
    Then user verify invalid Confirm Password error message

  @EPIC13351
  Scenario: FS Member Header_CP_FS_Wireframe and Links
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Your Miles link in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Free Spirit Number in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the My Reservations in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the My Account in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Manage Subscription in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Statements in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Request Mileage Credit in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Buy Miles in new browser and verify all fields
    When user clicks on the members name
    Then user clicks on the Sign Out and verify user is loggod out from application

  #Partial Completed:Incomplete data in Past Reservation. Unable to complete Test Case
  @EPIC25615
  Scenario: FS Member Header_CP_FS_Wireframe and Links
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the My Account link
    Then user verify all header fields under Current Reservation
    Then user verify reservation details under Current Reservation
    Then user click on end of reservation under Current Reservation link and verify all fields
    Then user verify all header fields under Past Resevations
    Then user verify reservation details under Past Resevations
    Then user click on end of reservation under Past Resevations link and verify all fields

  @EPIC25645
  Scenario: FS Enroll_ CP_NEG_Leaving required fields empty
    Given Spirit airlines application
    When User click on footer Free Spirit link
    Then User verify header verbiage on FS sign up page
    Then User click on CONTINUE TO STEP 2 button
    Then User verify all mandatory fields error message of Account Tab
    Then User fill all mandatory fields of Account Tab and click CONTINUE TO STEP 2
    Then User click on FREE SIGN UP button on Contact Tab
    Then User verify all mandatory fields error message of Contact Tab
    Then User verify Terms & Conditions message is in red colors

  @EPIC25647
  Scenario: FS Enroll_ CP_NEG_Inputing invalid information Contact Tab
    Given Spirit airlines application
    When User click on footer Free Spirit link
    Then User verify header verbiage on FS sign up page
    Then user verify Title error message
    Then user verify require First Name error message
    Then user verify invalid First Name error message
    Then user verify require Last Name error message
    Then user verify invalid Last Name error message
    Then user verify require Date of Birth error message
    Then user verify invalid Date of Birth error message
    Then user verify require Email error message
    Then user verify invalid Email error message
    Then user verify require Confirm Email error message
    Then user verify invalid Confirm Email error message
    Then user verify require Password error message
    Then user verify less than eight characters invalid Password error message
    Then user verify more than sixteen characters invalid Password error message
    Then user verify lower case eight characters invalid Password error message
    Then user verify lower case eight characters and upper case character invalid Password error message
    Then user verify lower case eight characters, upper case character and numbercial value invalid Password error message
    Then user verify lower case eight characters, upper case character and special character invalid Password error message
    Then user verify require Confirm Password error message
    Then user verify invalid Confirm Password error message
    Then user verify require Address error message
    Then user verify Symbol invalid Address error message
    Then user verify require City error message
    Then user verify Symbol invalid City error message
    Then user verify Number invalid City error message
    Then user verify require State error message
    Then user verify require Zip Code error message
    Then user verify less than four character Zip Code error message
    Then user verify Symbol invalid Zip Code error message
    Then user verify require Primary Mobile Number error message
    Then user verify invalid Primary Mobile Number error message
    Then user verify require Home Airport error message
    Then user verify Terms and Condition error for FS SignUp
    Then user verify FS signup successfully

  @EPIC25660
  Scenario: FS Email Subscription_CP_Unsubscribe Account
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the manage subscription link form the list
    Then user enetres the free spirit member customers email address
    Then user clicks on the continue button below the customers email address
    Then user click on Unsubscribe from Email Deals
    Then user verify Unsubscribe from Email Deals Popup

  @EPIC25661
  Scenario: FS Email Subscription_CP_Update Email Account
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the manage subscription link form the list
    Then user enetres the free spirit member customers email address
    Then user clicks on the continue button below the customers email address
    Then user complete the form and click on the subscribe button
    Then user can see that subscription is created

  #Salim(28-Sep-2018):1.User require a FS login which is having past booking history
  #                   2.Current FS login credentials do not have past booking.
  @EPIC25700
  Scenario: My Account_CP_FS_Request Miles Credit Link Neg_Already Received Milage
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Free Spirit Number link form the list
    Then user get booking Confirmation Code of last month rom past reservation
    Then user click on Request Mileage Credit link
    Then user enter Confirmation Code into for Request Mileage Credit
    Then user verify error message for past flown flight

  @EPIC25703
  Scenario: My Account_CP_FS_Request Miles Credit Link Neg_Already Received Milage
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Free Spirit Number link form the list
    Then user get booking Confirmation Code of last month rom current reservation
    Then user click on Request Mileage Credit link
    Then user enter Confirmation Code into for Request Mileage Credit
    Then user verify error message for future flight

  @EPIC25734
  Scenario: My Account_CP_FS_Request Miles Credit Link Neg_Already Received Milage
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Statement link form the list
    Then user verify Statement header text
    Then user verify Activity Period header text
    Then user verify Activity Period drop down
    Then user verify Transaction Types header text
    Then user verify Transaction Types drop down
    Then user verify Post Transactions header text
    Then user verify Post Transactions items
    Then user verify Post Transaction details
    Then user verify Statement footer verbiage

  @DryRun
  Scenario: My Account_CP_FS_Request Miles Credit Link Neg_Already Received Milage
    Given Spirit airlines application
    When user clicks on the sigin button and switch to the popup enters the free spirit memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the Free Spirit Number link form the list
    Then user click on Personal Information link
    Then user verify filled details of address section in Your Information
    Then user verify require Your Information Address error message
    Then user verify Symbol invalid Your Information Address error message
    Then user verify require Your Information City error message
    Then user verify Symbol invalid Your Information City error message
    Then user verify Number invalid Your Information City error message
    Then user verify require Your Information State error message
    Then user verify require Your Information Zip Code error message
    Then user verify less than four character Your Information Zip Code error message
    Then user verify Symbol invalid Your Information Zip Code error message
    Then user verify require Your Information Country error message
    Then user update Address Section and click on Save button
    Then user verify updated Address section of Your Information
