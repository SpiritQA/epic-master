Feature: Customer Log In

  @EPIC13396
  Scenario Outline: MultiPax NEG_FS verify login errors on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters a valid email address
    Then user enters a invalid password
    Then user validates login error message
    Then user enters invalid email address
    Then user enters a valid password
    Then user validates login error message
    Then user enters a invalid FS number
    Then user enters a valid password
    Then user validates login error message
    Then user enters valid FS number
    Then user enters a invalid password
    Then user validates login error message

    #Then user enters a valid password without email
    #Then user validates required Email error message
    #Then user enters a valid email without password
    #Then user validates required Password error message
    #Then user enters a valid FS number without password
    #Then user validates required Password error message
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23603
  Scenario Outline: MultiPax FS member entering incorrect information for FS number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the personal information for Second adult
    Then user enters eight digits for the FS numbers for Second adult
    Then user clicks in the continue button
    # Then user validates FS error message
    Then user enters ten digits for the FS numbers for Second adult
    Then user clicks in the continue button
    #Then user validates FS error message
    Then user enters letters for the FS numbers for Second adult
    Then user clicks in the continue button
    #Then user validates FS error message
    Then user enters symbols for the FS numbers for Second adult
    Then user clicks in the continue button
    # Then user validates FS error message
    Then user enters nine digits for the FS numbers for Second adult
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  #Then user validates FS error message
  @EPIC21134
  Scenario Outline: MultiPax log in as a FS member select a Hearing disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23650
  Scenario Outline: MultiPax log in as a NEG TEST FS member who is a UMNR on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    #then user verifies error popup
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13342
  Scenario Outline: MultiPax log in as a FS member add redress number and pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    #Then user clicks Add Redress Number
    #Then user enters Redress Number
    Then user enters the personal information of passenger two
    Then user Clicks on additional services for passenger two
    #Then user Clicks on PETC for passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13341
  Scenario Outline: MultiPax log in As a FS member Select one Wheelchair, and add Redress number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    #Then user clicks Add Redress Number
    #Then user enters Redress Number
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21136
  Scenario Outline: MultiPax log in as a FS member select a other disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects other disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21135
  Scenario Outline: MultiPax log in as a FS member select a Vision disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Vision disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3291
  Scenario Outline: MultiPax Generic booking Log in as FS member on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3289

  Scenario Outline: SinglePAX Generic booking Log in as FS member on customer information page [EPIC3289]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2971
  Scenario Outline: MultiPax log in As a FS member Select one Wheelchair Need Help ToFrom gate, one Service dog PAX on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks the Additional Services drop down and click the wheelchair and Service animal check box
    Then User Selects Need Help ToFrom gate
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21137
  Scenario Outline: MultiPax log in As a FS member Select one Manual Wheelchair, one Service dog PAX on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21138
  Scenario Outline: MultiPax log in As a FS member Select one Wheelchair, and POC on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3296
  Scenario Outline: MultiPax log in as a FS member select a random disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks on additional services and selects the hearing impaired
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3286
  Scenario Outline: MultiPax log in as a FS member who has a Known traveler number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member who has a known traveler number
    Then user verifies Member with known traveler is logged in
    Then user enters known traveler number
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13338
  Scenario Outline: MultiPax log in as a FS member who has a Known traveler number and select POC on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member who has a known traveler number
    Then user verifies Member with known traveler is logged in
    Then user enters known traveler number
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13339
  Scenario Outline: MultiPax log in as a FS member who has a Known traveler number and select Service Animal on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member who has a known traveler number
    Then user verifies Member with known traveler is logged in
    Then user enters known traveler number
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13347
  Scenario Outline: MultiPax log in As a FS member NEG test Have two PAX with the same name on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the same name for passenger two
    Then user clicks in the continue button
    #Then user verifies same name error message
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21167
  Scenario Outline: MultiPax As a FS Guest NEG test Have two PAX with the same name on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the same name for passenger two
    Then user clicks in the continue button
    #Then user verifies same name error message
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21168
  Scenario Outline: MultiPax log in As a FS member Have two PAX with the same name but with SR and JR suffix on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user selects SR suffix for passenger one
    Then user enters the same name for passenger two
    Then user selects JR suffix for passenger two
    Then user clicks in the continue button
    #Then user verifies same name error message
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2973
  Scenario Outline: MultiPax log in as a FS member with child on customer information page verify child DOB autopopulate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user enters the personal information of passenger two
    #Then User verfies Child DOB is autopopulated
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13340
  Scenario Outline: MultiPax log in as a FS member with child on customer information page verify child DOB autopopulate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user enters the personal information of Lap child
    #Then User verfies Child DOB is autopopulated
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3298
  Scenario Outline: MultiPax log in As a FS member Select one Wheelchair on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4983
  Scenario Outline: MultiPax log in as a FS member and enter wrong Known traveler number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters known traveler number
    #Then user enters invalid known traveler number
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    #must verify invalid ktn on confirmation page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2995
  Scenario Outline: MulitPAX booking Log in as  two UMNR FS member on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user clicks in the continue button
    #When user landing on the bags page
    #Then user log out
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3006
  Scenario Outline: SinglePAX booking Log in as FS member and Select voluntary provision of emergency services program on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects voluntary provision of emergency services program
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2981
  Scenario Outline: SinglePAX booking Log in as UMNR FS member on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user clicks in the continue button
    #When user landing on the bags page
    #Then user log out
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21139
  Scenario Outline: SinglePAX booking Log in as UMNR FS member and select a service animal on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    #When user landing on the bags page
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13345
  Scenario Outline: SinglePAX booking Log in as FS member and Select Pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks in the continue button
    #When user landing on the bags page
    #Then user log out
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13343
  Scenario Outline: SinglePAX booking Log in as FS member and Select hearing and vision disability on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user selects Vision disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3287
  Scenario Outline: SinglePAX booking Log in as FS member and Select ESAN on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks ESAN
    Then user clicks in the continue button
    When user landing on the bags page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC20952
  Scenario Outline: SinglePAX booking Log in as FS member and Select ESAN on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    When user landing on the bags page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21140
  Scenario Outline: SinglePAX booking Log in as FS member and Select wheelchair Completely Immobile on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects all Wheelchair checkboxes
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  #------------------------------nine DFC--------------------------------------
  @EPIC13370
  Scenario Outline: MultiPax NEG_nine dfc verify login errors on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    #Then user clicks on login
    Then user enters a valid email address
    Then user enters a invalid password
    Then user validates login error message
    Then user enters invalid email address
    Then user enters a valid password
    Then user validates login error message
    Then user enters a invalid FS number
    Then user enters a valid password
    Then user validates login error message
    Then user enters valid FS number
    Then user enters a invalid password
    Then user validates login error message

    #Then user enters a valid password without email
    #Then user validates required Email error message
    #Then user enters a valid email without password
    #Then user validates required Password error message
    #Then user enters a valid FS number without password
    #Then user validates required Password error message
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23602
  Scenario Outline: MultiPax nine dfc member entering incorrect information for FS number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information for Second adult
    Then user enters eight digits for the FS numbers for Second adult
    Then user clicks in the continue button
    # Then user validates FS error message
    Then user enters ten digits for the FS numbers for Second adult
    Then user clicks in the continue button
    #Then user validates FS error message
    Then user enters letters for the FS numbers for Second adult
    Then user clicks in the continue button
    #Then user validates FS error message
    Then user enters symbols for the FS numbers for Second adult
    Then user clicks in the continue button
    # Then user validates FS error message
    Then user enters nine digits for the FS numbers for Second adult
    Then user clicks in the continue button

    #Then user validates FS error message
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3294
  Scenario Outline: MultiPax Generic booking Log in as nine DFC member on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3293
  Scenario Outline: SinglePAX Generic booking Log in as nine DFC member on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    #Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3097
  Scenario Outline: MultiPax log in as a nine DFC member select a random disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks on additional services and selects the hearing impaired
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3282
  Scenario Outline: MultiPax log in as a nine DFC member who has a Known traveler number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies nine DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13318
  Scenario Outline: MultiPax log in as a FS member who has a Known traveler number and select POC on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies nine DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user Clicks Additional Services
    #Then user Clicks POC
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13319
  Scenario Outline: MultiPax log in as a nine dfc member who has a Known traveler number and select Service Animal on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies nine DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3036
  Scenario Outline: MultiPax log in as a nine DFC member with child on customer information page verify child DOB autopopulate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information of passenger two
    #Then User verfies Child DOB is autopopulated
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13322
  Scenario Outline: MultiPax log in as a nine DFC member add redress number and pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    #Then user clicks Add Redress Number
    #Then user enters Redress Number
    Then user enters the personal information of passenger two
    Then user Clicks on additional services for passenger two
    #Then user Clicks on PETC for passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13321
  Scenario Outline: MultiPax log in As a nine DFC member Select one Wheelchair, and add Redress number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    #Then user clicks Add Redress Number
    #Then user enters Redress Number
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3310
  Scenario Outline: MultiPax log in As a nine DFC member Select one Wheelchair on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks the Additional Services drop down and click the wheelchair and Service animal check box
    Then User Selects Manual Wheelchair
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21128
  Scenario Outline: MultiPax log in as a nine DFC member select a other disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects other disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21127
  Scenario Outline: MultiPax log in as a nine DFC member select a Vision disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects Vision disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC21126
  Scenario Outline: MultiPax log in as a nine DFC member select a Hearing disablity on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4978
  Scenario Outline: MultiPax log in as a nine DFC member and enter wrong Known traveler number on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters known traveler number
    #Then user enters invalid known traveler number
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    #must verify invalid ktn on confirmation page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3039
  Scenario Outline: SinglePAX booking Log in as nine DFC member and Select voluntary provision of emergency services program on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects voluntary provision of emergency services program
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3284
  Scenario Outline: SinglePAX booking Log in as nine DFC member and Select Pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2948
  Scenario Outline: As FS member 1 military PAX and 1 LapChild
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then verify FS member informations are read only
    Then user selects the usa military personnel checkbox
     Then user enters the personal information of Lap child
    Then User verifies LapChild DOB is autopopulated
    Then user verifies no Active duty military or Traveling with car seat check box next to lap child
    Then user clicks in the continue button

    #Then User closes the browser
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2886
  Scenario Outline: Single ADT PAX 1 LapChild 1 INFT seated and 1 child under 15
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the three child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user selects the usa military personnel checkbox
    Then user enters the personal information of the children
    Then User verifies LapChild DOB is autopopulated
    Then User verifies military PAX DOB is not populated
    Then user verifies no Active duty military or Traveling with car seat check box next to lap child
    Then user clicks in the continue button

    #Then User closes the browser
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2952
  Scenario Outline: 9DFC member 1 military PAX with a Vision Disability and service animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    Then user Clicks vision disability
    Then user Clicks service animal
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2953
  Scenario Outline: 9DFC member 1 military PAX with a wheelchair and POC
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    Then user Clicks Own wheelChair
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2961
  Scenario Outline: SinglePAX booking Log in as nine DFC member and Select Pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2989
  Scenario Outline: As 9DFC member 1 military PAX and 1 LapChild
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
      Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then User verifies military PAX DOB is not populated
    Then user selects the usa military personnel checkbox
    Then user enters the personal information of Lap child
    Then User verifies LapChild DOB is autopopulated
    #Then user verifies no Active duty military or Traveling with car seat check box next to lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3014
  Scenario Outline: 1 Military member with a known traveler number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS military member and known traveler_with same name as active military member
    Then user verifies FS Member of military with known traveler is logged in
    Then verify FS member informations are read only
    Then FS military member enters known traveler number
    #Then user selects the usa military personnel checkbox
    #Then user clicks in the continue button

    #Then User closes the browser
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3033
  Scenario Outline: As 9DFC member multi military
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3034
  Scenario Outline: As FS member multi military
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23657
  Scenario Outline: Military with thrills combo selected click keep military bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
   Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23658
  Scenario Outline: Military with thrills combo selected keep thrills combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue
    #Then user switch to the popup and clicks on choose thrills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23660
  Scenario Outline: Military with thrills combo able to close pop up with X
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X
   

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23661
  Scenario Outline: FS member who is a Military member with thrills combo selected click keep military bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23662
  Scenario Outline: FS member who is a Military member with thrills combo selected keep thrills combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user selects the usa military personnel checkbox
    Then user clicks in the continue button
    #Then user switch to military bags popup and choose thirills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23663
  Scenario Outline: FS member who is a Military member with thrills combo able to close pop up with X
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user selects the usa military personnel checkbox
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X
    #Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23665
  Scenario Outline: 9DFC member who is a Military member with thrills combo selected click keep military bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23666
  Scenario Outline: 9DFC member who is a Military member with thrills combo selected keep thrills combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and choose thirills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23667
  Scenario Outline: 9DFC member who is a Military member with thrills combo able to close pop up with X
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects multi ADT PAX
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X
    #Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23671
  Scenario Outline: 1 Military member who is Select voluntary provision of emergency services program
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    Then user selects voluntary provision of emergency services program
    Then user enters the contact information
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23688
  Scenario: MultiPax _AS a 9DFC create a booking with a car vacation under age
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects under age of driver
    Then User clicks on Search vacations button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X
    #Then user clicks in the continue button

  @EPIC23689
  Scenario: MultiPax _AS a 9DFC create a booking with a vacation and car
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button
    Then user lands on flights cars page
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user enter a DOB that is over the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user able to select a primary driver
    Then user clicks in the continue button

  @EPIC25694
  Scenario: AS a FS member who is active duty military create a booking with a car vacation under age
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects under age of driver
    Then User clicks on Search vacations button
    Then user lands on flights cars page
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user enter a DOB that is under the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax under the age of 21
    Then user unable to select a primary driver

  @EPIC25695
  Scenario: AS a FS member who is active duty military create a booking with a vacation and car
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button
    Then user lands on flights cars page
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user enter a DOB that is over the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user able to select a primary driver
    Then user clicks in the continue button

  @EPIC25696
  Scenario: MultiPax _AS a active duty military create a booking with a car vacation under age
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects under age of driver
    Then User clicks on Search vacations button
    Then user lands on flights cars page
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enter a DOB that is under the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax under the age of 21
    Then user unable to select a primary driver

#partially completed
  @EPIC25697
  Scenario: MultiPax _AS a active duty military create a booking with a vacation and car
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button
    Then user lands on flights cars page
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enter a DOB that is under the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user able to select a primary driver
    Then user clicks in the continue button

  ################################## ACTIVE DUTY MILITARY [1665] #######################################
  #####################	 Mobile Platform #####################
  @EPIC4582
  Scenario Outline: MultiPax ADT PAX with 1 active military add a service animal to the military member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user Clicks Additional Services
    Then user Clicks service animal
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4585
  Scenario Outline: Single PAX who is military with a service animal, and wheelchair
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    Then user Clicks service animal
    Then user Clicks Wheelchair with all three check box checked and a manual wheelchair
    Then user enters the contact information
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4589
  Scenario Outline: MultiPax with 1 actve military member with a Hearing Disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user Clicks Additional Services
    Then user Clicks service animal
    Then user Clicks hearing disablity
    Then user enters the contact information
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4593
  Scenario Outline: Single PAX who is military with a service animal, and wheelchair
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then user Clicks Additional Services
    Then user Clicks Portal Oxyzen Concentrators
    Then user enters the contact information
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  ################# ~~~~ Customer portal ~~~~ #######################
  @EPIC23745
  Scenario Outline: MultiPAX _ Military with thrills combo selected click keep military bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user enters the contact information
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23746
  Scenario Outline: MultiPAX _ Military with thrills combo selected keep thrills combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user enters the contact information
    Then user clicks in the continue button
    #Then user switch to military bags popup and choose thirills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23747
  Scenario Outline: MultiPAX _ Military with thrills combo able to close pop up with X
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user enters the contact information
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X
    #Then user clicks in the continue button
    #Then user switch to military bags popup and choose thirills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23748
  Scenario Outline: MultiPAX _FS member who is a Military member with thrills combo selected click keep military bags
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user enter a DOB that is over the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and continue

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23749
  Scenario Outline: MultiPAX _ FS member who is a Military member with thrills combo selected keep thrills combo
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user enter a DOB that is over the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and choose thirills combo

    Examples: 
      | Required stops |
      | 1 Stop         |

  #@EPIC23749
  Scenario Outline: MultiPAX _ FS member who is a Military member with thrills combo able to close pop up with X
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on choose thrills combo
    When user lands on passengers page
    Then user login as FS member with same name as active military member
    Then user verifies FS Member with military is logged in
    Then user enter a DOB that is over the age of 21
    Then user selects the usa military personnel checkbox
    Then use enters informations of second adt military pax
    Then user clicks in the continue button
    #Then user switch to military bags popup and close X

    Examples: 
      | Required stops |
      | 1 Stop         |

  ###################~~~~~~~~~ Flight + Car [1602]~~~~~~~~~~~~~~~~~~~~~##############################
  @EPIC3252
  Scenario: Flight+ Car Milti PAX (edit)
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then User selects number of adult passengers
    Then User selects over age of driver
    Then User clicks on Search vacations button
    #Then user lands on flights cars webpage with pre-selected flights

  @EPIC3262
  Scenario: Flight+Car 1 Pax with Lap Child
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then user selects the one adult passengers and one child
    Then User selects over age of driver
    Then User clicks on Search vacations button and enters the birthdate of the Lap child
    #Then user lands on flights cars webpage with pre-selected flights

  @EPIC3271
  Scenario: Flight+Car 1 Pax with INF with seat
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates
    Then user selects the one adult passengers and one child
    Then User selects over age of driver
    Then User clicks on Search vacations button and enters the birthdate of the INFT child
    #Then user lands on flights cars webpage with pre-selected flights

  @EPIC3275
  Scenario: Flight+Car Multi Pax with Multi Children DOM-DOM (Edit)
    Given Spirit airlines application
    When User Clicks on Vacation tab
    Then User enter from airport or city
    Then User enter to airport or city
    Then user selects vacation dates within 48 hours
    Then user selects the one adult passengers and one child
    Then User selects over age of driver
    Then User clicks on Search vacations button and enters the birthdate of the children
    #Then user lands on flights cars webpage with pre-selected flights

  ###################~~~~~~~~~ Booking [4399]~~~~~~~~~~~~~~~~~~~~~##############################
  #UI Based Testcases
  @EPIC2872
  Scenario Outline: Validate Selected Flight_OW Wireframe
    Given Spirit airlines application
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flight stops>"
    Then user validates verbiage of departure flight informations

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

  ###################~~~~~~~~~ Mobile - 9DFC Member[1662] ~~~~~~~~~~~~~~~~~~~~~##############################
  @EPIC4507
  Scenario Outline: MultiPax _As a 9DFC member with child (Lap or seated)
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then User verifies military PAX DOB is not populated
      Then user enters the personal information of Lap child
    Then User verifies LapChild DOB is autopopulated
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4511
  Scenario Outline: SinglePAX _As a 9DFC member Select voluntary provision of emergency services program
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as military nine DFC member
    Then user verifies Member with military nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects voluntary provision of emergency services program
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4515
  Scenario Outline: MultiPax _As a 9DFC member select a random disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks on additional services and selects the hearing impaired
    Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4518
  Scenario Outline: MultiPax _As a 9DFC member who has a Known traveler number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies nine DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4521
  Scenario Outline: SinglePAX _As a 9DFC create booking with pet in the cabin as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4527
  Scenario Outline: MultiPax_Generic booking as 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
  Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC4530
  Scenario Outline: MultiPax _As a 9DFC member with Wheelchair
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user Clicks on My Own WheelChair
    Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13348
  Scenario Outline: MultiPax _As a 9DFC 1 LapChild, 1 INFT seated, and 1 Child of age 14
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the three child and one adult passengers
    Then user clicks on search button and enters three birthdate of the children
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user enters the personal information of the children
    Then User verifies LapChild DOB is autopopulated
    Then User verifies military PAX DOB is not populated
    Then user clicks in the continue button

    #Then User closes the browser
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13350
  Scenario Outline: MultiPax _As a 9DFC 1 of your PAX has a Known Traveler Number with a POC
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies 9 DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13354
  Scenario Outline: MultiPax _As a 9DFC 1 of your PAX has a Known Traveler Number with a service animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member who has a known traveler number
    Then user verifies 9 DFC Member with known traveler is logged in
    Then user enters known traveler number for nine DFC Member
    Then user Clicks Additional Services
    Then user Clicks service animal
    Then user enters the personal information of Lap child
    Then user clicks in the continue button

    Examples: 
      | Required stops |
      | 1 Stop         |

  #--------------------------------------------------------------- Sunny-------------
  @EPIC3097
  Scenario Outline: Customer Info CP BP As a 9DFC member select a random disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user selects A random disability
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3284
  Scenario Outline: SinglePAX booking Log in as nine DFC member and Select Pet in cabin on customer information page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user Clicks Additional Services
    Then user Clicks Pet In Cabin
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13261
  Scenario Outline: Customer Info CP BP 2UMNR logged in as a 9DFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects TWO child
    Then user clicks on search button and enters the birthdate for TWO UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user login as UMNR NineDFC member
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13266
  Scenario Outline: Customer Info CP BP As a 9DFC member have a UMNR logged in
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR NineDFC member

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13320
  Scenario Outline: Customer Info_CP_BP_1 LapChild, 1 INFT seated, 1 Child of age 14 and a adult 9DFC PAx
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the Three child and one adult passengers
    Then user clicks on search button and enters the birthdate for three children
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user enters the personal information for all pax

    #    Then user clicks in the continue button
    Examples: 
      | Required stops |
      | 1 Stop         |
