Feature: Customer Information page

  @EPIC2885
  Scenario Outline: verify user able to select military member checkbox
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2996
  Scenario Outline: Verify MultiPax Login As a FS member select one military PAX
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    And user selects the usa military personnel checkbox
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2949
  Scenario Outline: verify multi pax log in as a FS member one military PAX with a emotional support animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects the emotional support animal check box
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2951
  Scenario Outline: verify As a FS member military PAX with a wheelchair and sevice animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    And user selects the usa military personnel checkbox
    Then user clicks the Additional Services drop down and click the wheelchair and Service animal check box
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2838
  Scenario Outline: verify MultiPax ADT PAX with one active military add a service animal to the military member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user Click the drop down under the contact information of a random PAX and add a service animal
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2933
  Scenario Outline: verify MultiPax with one active military one veteran and one military spouse add additional services
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user add a Wheelchair and click the Completely Immobile I Have My own wheelchair Need Help to From Gate and Need Help to From Seat check boxes
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2864
  Scenario Outline: verify MultiPax with one actve military member with a Hearing Disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects the hearing impaired
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2869
  Scenario Outline: MultiPax with actve military member with a Other disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects the other disability
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2866
  Scenario Outline: MultiPax with actve military member with a Vision Disability
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    #Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    #Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects the vision disability
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2884
  Scenario Outline: MultiPAX Multi Military members in the booking
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    #Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of passenger two
    Then user selects the us military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2855

  Scenario Outline: Single PAX who is military with a service animal, and wheelchair [EPIC2855]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects on the service animal and wheel chair
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2868
 
  Scenario Outline: SinglePax with actve military member with a Emotional support animal [EPIC2868]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects Emotional support animal
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2880
  
  Scenario Outline: SinglePax with actve military member with a Pet in cabin [EPIC2880]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    #Then user clicks on additional services and selects Pet in cabin
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2865
  
  Scenario Outline: SinglePax with actve military member with a POC [EPIC2865]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services and selects the portable oxygen
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2874
  Scenario Outline: SinglePax with actve military member with a wheelchair Completely Immobile [EPIC2874]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user clicks on additional services add a Wheelchair and click the Completely Immobile I Have My own wheelchair Need Help to From Gate and Need Help to From Seat check boxes
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |


  @EPIC3053
  Scenario Outline: verify Multi pax with a Heraing disability able to check in with the PNR 
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional info and selects the hearing disability
    Then user clicks on the Save button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3057
  Scenario Outline: verify Multi pax with a other disability able to check in with the PNR 
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    # Then user clicks on the Booktrip and handle the travel more popup
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the other disability
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3055
  Scenario Outline: verify multi pax with a vision disability able to checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the vision disability
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3065
  Scenario Outline: verify multi pax with a wheel chair and service animal check in with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the Wheel chair and service animal
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3063
  Scenario Outline: verify multi pax with a emotional support animal check in with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the emotional support animal
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3071
  Scenario Outline: verify multi pax with wheel chair and POC check in with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the wheel chair and POC
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3043
  Scenario Outline: verify multi pax select two pax with the disabilities check in with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the hearing disability and passenger two selects the vision disability
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3051
  Scenario Outline: verify ADT PAX with a service animal to the military member checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the service animal to a military member
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3095
  Scenario Outline: verify pax with a Known traveler number which is incorrect checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the add button and enter the incorrect known travel number
    #Then user clicks on the  known traveler Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3092
  Scenario Outline: Verify pax with the known traveler number checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the add button and enter the correct known travel number
    #Then user clicks on the  known traveler Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3257
  Scenario Outline: verify PAX with a Known traveler number with a random disablity checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional info and selects the vision disability
    #Then user clicks on the add button and enter the correct known travel number
    #Then user clicks on the  known traveler Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3068
  Scenario Outline: Multipax with vision disability and service animal checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the vision disability and service animal
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3082
  Scenario Outline: Testing that cancel button works correctly checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the vision disability and service animal
    #Then user clicks on the Cancel button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3084
  Scenario Outline: multi pax  Voluntary provision of emergency sevices program checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional information button
    #Then user selects the voluntry provision of emergency service program
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3169
  Scenario Outline: multi pax with the  disablity seating need a seat with a moveable aisle armrest
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional information button
    #Then user selects the diability seating and selects the  I need a seat with a moveable aisle armrest
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3161
  Scenario Outline: multi pax with disablity seating Emotional support animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on additional services and selects the emotional support animal check box
    #Then user selects the diability seating and selects the  I need a seat with a moveable aisle armrest
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3167
  Scenario Outline: multipax with disablity seating bend leg
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional information button
    #Then user selects the diability seating and selects the  I am unable to bend leg fused leg immobile leg
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3171
  Scenario Outline: multi pax with disablity seating someone traveling with me
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks on the additional information button
    #Then user selects the diability seating and selects the I need a seat for someone traveling with me
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3054
  Scenario Outline: single pax with a POC checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional information button
    #Then user selects the POC
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3044
  Scenario Outline: SinglePAX  UMNR who has a service animal checkin with the PNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one UMNR passenger
    Then user clicks on search button and enters the birthdate of the child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger child information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional information button
    #Then user selects the service animal
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3041
  Scenario Outline: SinglePAX Select  Hearing Disablity and Vision Disablity for random PAX
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional information button
    #Then user selects the hearing disability and vision disability
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3058
  Scenario Outline: Verify SinglePax with a wheelchair Completely Immobile
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    And user selects the usa military personnel checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional information button
    #Then user selects the wheel chair and completely immobile
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3056
  Scenario Outline: SinglePax with actve military member with a Emotional support animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    #Then user clicks on the additional info and selects the emotional support animal
    #Then user clicks on the Save button
    #Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2921
  Scenario Outline: MultiPax Have two PAX with the same name but with SR and JR suffix
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information with SR
    Then user enters the personal information of adult two with JR
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2934
  Scenario Outline: multipax and one of your PAX has a Known Traveler Number with a service animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger with known traveler number and service animal
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2935
 
  Scenario Outline: MultiPax  with one PAX who has a redress number with a Wheelchair  [EPIC2935]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the multi passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger with redress number and wheel chair
    Then user enters the personal information of adult two
    Then user enters the contact information
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2962
 
  Scenario Outline: SinglePax Generic booking  [EPIC2962]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #  Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2964
  Scenario Outline: multipax generic booking
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3012
  Scenario Outline: multipax booking with the known traveler number
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    #Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user enters the personal information of passenger two with know traveler number
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

      
    @EPIC23601
    Scenario: Check-In CP BP  Links CONFIRMATION CODE popup
     Given Spirit airlines application
     When user clicks on the check in tab 
     Then user clicks on the where to find your confirmation code link 
     Then user switch to the popup and clicks on the Spirit link
     Then user validate the text on the spirit link 
     Then user clicks on the Expedia link and validate the text 
     Then user clicks on the priceline link and validate the text 
     Then user clicks on the cheapoair link and validate the text
     Then user clicks on the orbitz  link and validate the text 
     Then user clicks on the close button on the popup
    
 


  @EPIC2910
  Scenario Outline: Customer Info_CP_BP_Select voluntary provision of emergency services program
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address
    Then user Clicks Additional Services
    Then user selects voluntary provision of emergency services program
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC2912
  Scenario Outline: Customer Info_CP_BP _ UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user verify DOB autopopulated for UMNR
	Then user enters the personal info and contact address for UMNR
    Then user clicks in the continue button
    When user landing on the bags page


    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC2914
  Scenario Outline: Customer Info_CP_BP_ UMNR who has a service animal
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user verify DOB autopopulated for UMNR
	Then user enters the personal info and contact address for UMNR
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    When user landing on the bags page


    Examples: 
      | Required stops |
      | 1 Stop         |
      
    @EPIC2922
  Scenario Outline: Customer Info_CP_BP_MultiPAX _with pet in the cabin
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address for two adults
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks in the continue button
    When user landing on the bags page


    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC2939
  Scenario Outline: Customer Info_CP_BP _ 1 LapChild, 1 INFT seated, or 1 Child of age 14
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the Three child and one adult passengers
    Then user clicks on search button and enters the birthdate for three children
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
     Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user enters the personal information for all pax
    Then user clicks in the continue button
    When user landing on the bags page
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC2984
  Scenario Outline: Customer Info_CP_BP_ 2 UMNR only
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects TWO child
    Then user clicks on search button and enters the birthdate for TWO UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address for UMNR
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    
   

    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC3011
  Scenario Outline: Customer Info_CP_BP_9DFC member with apostrophe in there name
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
	Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info that contains apostrophe and contact address
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
        @EPIC27301
  Scenario Outline: Customer Info_CP_BP_ PAX with a ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
    Then user Clicks ESAN
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC2944
  Scenario Outline: Customer Info_CP_BP_Have a booking with one PAX who has a redress number with a pet in cabin_With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
    #Then user Clicks Pet In Cabin
    Then user clicks and Add Redress Number 
    Then user clicks in the continue button
    When user landing on the bags page
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
# 9/6
      
  @EPIC2983
  Scenario Outline: Customer Info_CP_BP_Have a booking with a Infant with car seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
	Then user enters the personal info and contact address
    Then user enters the personal information of passenger two
    Then user selects carseat
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
      @EPIC23653
        Scenario Outline: Customer Info_CP_BP _Verify 15 years or older UMNR is treated as a adult
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR who is fifthteen years old
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	  Then user enters the personal info and contact address for passenger who is fifthteen years old
    Then user clicks in the continue button
    #When user landing on the bags page

        Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC25267
  Scenario Outline: Customer Info_CP_BP_PAX with a extra seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user enters the personal information for Second adult same name as first pax
    Then user adds SR and Jr suffix
    Then user clicks in the continue button
    When user landing on the bags page
    Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC26862
  Scenario Outline: Customer Info_CP_BP _ Multiple UMNR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects TWO child
    Then user clicks on search button and enters the birthdate for TWO UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address for UMNR
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    
        Examples: 
      | Required stops |
      | 1 Stop         |
      
       @EPIC27296
  Scenario Outline: Customer Info_CP_BP_Have one PAX with a Other disablity
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
    Then user selects other disability
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |
      
       @EPIC27166
  Scenario Outline:Customer Info_CP_BP_Select 1 Wheechair need help to from gate
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
	Then User Selects Need Help ToFrom gate
    Then user clicks in the continue button
    When user landing on the bags page
 
    Examples: 
      | Required stops |
      | 1 Stop         |
      
    @EPIC27306
  Scenario Outline:Customer Info_CP_BP_Select 1 Wheechair wet cell option
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
	Then user selects I have my own wheelchair
	Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
    @EPIC27336
  Scenario Outline:Customer Info_CP_BP_Select 1 Wheechair help to seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
	Then User Selects Need Help ToFrom Seat
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |
      
      @EPIC27308
  Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair wet cell option with a pet in cabin
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    #Then user Clicks Pet In Cabin
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      @EPIC27337
    Scenario Outline: Customer Info_CP_BP_Select 1 Wheechair dry or gel cell battery
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered Drycell Wheelchair
    Then user enters the personal information for Second adult
    Then user clicks in the continue button
    Examples: 
      | Required stops |
      | 1 Stop         |
      
        @EPIC27339
  Scenario Outline: Customer Info_CP_BP_Select vision impaired,  Service dog PAX
    Given Spirit airlines application
    When User Clicks on Book link
     Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user selects Vision disability
    Then user clicks in the continue button
    When user landing on the bags page

        Examples: 
      | Required stops |
      | 1 Stop         |
      
    @EPIC2909
  Scenario Outline: Customer Info_CP_BP_Select 2 PAX with disabilies
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address
	Then user Clicks Additional Services
	Then user selects Vision disability
    Then user enters the personal information for Second adult
    Then user Clicks on additional services for passenger two
	Then user selects Esan for pax two
    Then user clicks in the continue button
    
            Examples: 
      | Required stops |
      | 1 Stop         |
      
     @EPIC3293
  Scenario Outline: Customer Info_CP_BP_Generic booking
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
	Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user log out
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |
      
    @EPIC2879
  Scenario Outline: Validate the customer info page
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects ONE ADULT and TWO children
    Then User Clicks on SEARCH FLIGHTS and makes one child UNDER four years old and one child OVER four years old
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User enters Info for Primary Passenger Adult
    Then User Validates Primary Passenger KTN Tooltip
    Then User Validates Primary Passenger Free Spirit Tooltip
    Then User Validates Primary Passenger Active Duty Tooltip
    Then User Clicks on Primary Passenger Additional Services Caret
    Then User Validates Primary Passenger Emotional Support Animal Tooltip
    Then User Validates Primary Passenger Other CPAP nebulizer ventilator respirator cane walker etc
    Then User Validates Primary Passenger Voluntary Provision Tooltip
    Then User enters Info For Second Passenger Adult
    Then User Validates Passenger Two KTN Tooltip
    Then User Validates Passenger Two Free Spirit Tooltip
    Then User Clicks on Passenger Two Additional Services Caret
    Then User Validates Passenger Two Emotional Support Animal Tooltip
    Then User Validates Passenger Two Other CPAP nebulizer ventilator respirator cane walker etc
    Then User enters Info for Third Passenger Child
    Then User Validates Passenger Three KTN Tooltip
    Then User Validates Passenger Three Free Spirit Tooltip
    Then User Clicks on Passenger Three Additional Services Caret
    Then User Validates Passenger Three Emotional Support Animal Tooltip
    Then User Validates Passenger Three Other CPAP nebulizer ventilator respirator cane walker etc
    

    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC2916
  Scenario Outline: Make a booking with Two PAX both having the same exact Name and force Error Message to Display
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects two adult passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user enters the personal information for Second adult same name as first pax
    Then user clicks in the continue button
    
    
     Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC2942
  Scenario: Book A Car Only Booking Validate the Primary Driver Field and Try to Continue with A Driver Under the Age of Twenty One
  	Given Spirit airlines application
  
  @EPIC5355
  Scenario Outline: Negative Validations on the customer info page
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page   
    Then user clicks in the continue button
    Then User Validates The Enter Title Field Is Required And Enters A Title
    Then user enters the personal info and contact address
    Then User In The Booking Path Erases the First Name And Attempts To Continue And Is Promtped By A Required Field
    Then User In The Booking Path Erases the Last Name And Attempts To Continue And Is Promtped By A Required Field
    Then User In Booking Path Erases the Date Of Birth And Attempts To Continue And Is Promtped By A Required Field
    Then User In The Booking Path Enters the Contact First Name
    Then User In the Booking Path Enters The Contact Last Name
    Then User In The Booking Path Clicks The Primary Passenger Is The Contact Person Button
    Then User In The Booking Path Erases The Contact Address Field
    Then user clicks in the continue button
    Then User In The Booking Path Erases the Address And Attempts To Continue And Is Promtped By A Required Field
    Then User In The Booking Path Erases the City And Attempts To Continue And Is Promtped By A Required Field
    Then User In The Booking Path Erases the State and Zip Code And Attempts To Continue And Is Promtped By A Required Field
    Then User In The Booking Path Erases the Email and Email Confirmation And Attempts To Continue And Is Promtped By A Required Field
    Then User In Booking Path Enters An Invalid First Name
    Then User In Booking Path Enters An Invalid Last Name
    Then User In Booking Path Enters An Invalid Date Of Birth 
    Then User In Booking Path Enters An Invalid Redress Number
    Then User In The Booking Path Clicks The Primary Passenger Is The Contact Person Button
    Then User In Booking Path Enters An Invalid Contact Info First Name
    Then User In Booking Path Enters An Invalid Contact Info Last Name
        
    
    
    Examples: 
      | Required stops |
      | 1 Stop         | 
        
  @EPIC5856
  Scenario Outline: Validate Sign Up as A Free Spirit Member during the Booking Processs
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page   
    Then User Enters Personal Info and Contact Address with a Dynamic Email to Validate Free Spirit Sign Up 
    Then User Validates the Join Free Spirit Yelow Banner at the bottom 
    Then User Clicks Yes I want become a Free Spirit Member
    Then User Enters a Valid Password and a Confirm Valid Password
    Then user clicks in the continue button
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC5861
  Scenario Outline: Validate Sign Up as A $9FC Member during the Booking Processs On Passenger Info Page 
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page     
    Then User Enters Personal Info and Contact Address with an Existing 9DFC Member to trigger Email Already In Use 
    Then user clicks on the continue button
    
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |   
      
      
  @EPIC5871
  Scenario Outline: Validate Sign Up as A Free Spirit Member during the Booking Processs On Passenger Info Page
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page     
    Then User Enters Personal Info and Contact Address with an Existing Free Spirit Member to trigger Email Already In Use 
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC23580
  Scenario Outline: Validate Passenger Info Page Required Fields When Switching Countries
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User Validates That USA Is Selected By Default In Country Drop Down Menu  
    Then User Validates State and ZipCode Are Required Fields By Checking For The Red Asterisk 
    Then User Changes The Country To Canada  
    Then user clicks in the continue button
    Then user clicks in the continue button
    Then User Validates State and ZipCode Are Required Fields By Checking For The Red Asterisk
    Then User Validates State Is Required Field
    Then User Validates ZipCode Is Required Field 
    Then User Changes The Country To Afghanistan
    Then User Inputs Non Alphabetical Characters Into The State Box
    Then User Validates The Only Letters Are Allowed For The State Field When Using A Foreign Country
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
      
  @EPIC23604
  Scenario Outline: Make A Booking with Two Passengers and  Validate the FS Number Field On The Passenger Info Page 
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User Selects TWO Adults and ONE Child
    Then User Clicks on SEARCH FLIGHTS and makes One Child Over Four Years Old but Under Fifteen
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User enters Info for Primary Passenger Adult
    Then User enters Info For Adult PAX Number Two 
    Then User Enters Info For Child Over Four Under Fifteen PAX Number Three
    Then User Validates The Auto Populated Date Of Birth For Child Passenger Number Three 
    Then User Enters Less Then Nine Digits for Adult PAX TWO Free Spirit Field 
    Then User Clicks On The Continue Button On Passenger Information Page  
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters More Than Ten Digits Into The Free Spirit Field 
    Then User Clicks On The Continue Button On Passenger Information Page  
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters an Aplha Numeric Free Spirit Number And Attempts To Continue
    Then User Clicks On The Continue Button On Passenger Information Page  
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters A Free Spirit Number Containing Symbols
    Then User Clicks On The Continue Button On Passenger Information Page  
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters A Valid Free Spirit Number From Another Account
    
  	
  Examples: 
      | Required stops |
      | 1 Stop         |            
    
    
     @EPIC23651
  Scenario Outline: Make A Booking And Validate The Young Traveler Popup On The Passenger Information Page 
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User enters Info for Primary Passenger Adult
    Then User Changes The Date Of Birth To Make The Primary Passenger Under Fifteen and A UNMR
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Young Traveler Pop Up on Pax Info Page
    Then User Clicks Close On The Young Traveler Popup
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Young Traveler Pop Up on Pax Info Page
    Then User Clicks Return To Home Page Button On The Young Traveler Popup
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC23672
  Scenario Outline: Make A Booking And Validate The Password Fields In The Join Free Spirit Section Of The Passenger Information Page 
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page   
    Then User Enters Personal Info and Contact Address with a Dynamic Email to Validate Free Spirit Sign Up 
    Then User Validates the Join Free Spirit Yelow Banner at the bottom 
    Then User Clicks Yes I want become a Free Spirit Member
    Then User Enters A Password With Less Than Eight Characters
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A Password With More Than Sixteen Characters   
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A Password With Only Upper and Lower Case Letters
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A Password With Only Upper Case Letters and Special Characters
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A Passwrod With All Lowercase Letters And Special Characters
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A  Password With Only Lowercase Letters 
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A  Password With Only Special Characters 
    Then User Enters Two Different Passwords To Trigger Password Must Match Error
    Then User Validates The Join Free Spirit Invalid Password Error Message
    Then User Enters A  Password With Only Special Characters 
    
   
    
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC23779
  Scenario Outline: Make A Booking with Two Passengers and  Validate the FS Number Field On The Passenger Info Page 
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
	Then User selects number of passengers    
	Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page   
    Then User Enters Personal Info and Contact Address with a Dynamic Email to Validate Free Spirit Sign Up 
    Then User Enters A Correct Free Spirit Number From A Different Person For This Booking
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters An Incorrect Free Spirit Number To Trigger An Error
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters A Free Spirit Number With Less Than Nine Characters 
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    Then User Enters A Free Spirit Number With Less Than Ten Characters
    Then User Clicks On The Continue Button On Passenger Information Page
    Then User Validates The Error Message For Inputting An Improper Free Spirit Number
    
    
    

  Examples: 
      | Required stops |
      | 1 Stop         |   
      
      
  @EPIC23780 
  Scenario Outline: Validate The Known Traveler Number And Redress NUmber Fields
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers    
	Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page   
    Then User Enters Personal Info and Contact Address with a Dynamic Email to Validate Free Spirit Sign Up 
    Then User Enter An Incorrect Known Traveler Number For PAX One
    Then User Clicks On The Continue Button On Passenger Information Page
#    Then User Recieves Error Message For Incorrect Known Traveler Number
    Then User Clicks On Redress Number and Enters An Incorrect Redress Number
    Then User Clicks On The Continue Button On Passenger Information Page
#    Then User Receives Error Message For Incorret Redress Number
    #Then User Enters A Known Traveler Number With More Than Twenty Five Digits
#    Then User Recieves Error Message For Incorrect Known Traveler Number
    Then User Enters A Redress Number With More Than Sixteen Digits
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC25628
  Scenario: User Makes A Vacation Flight and Car Booking And Tries To Continue Passenger Info Page WithA Primary Driver Under Twenty One 
  	Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User Clicks On The Vacation From City
    Then User Clicks On The Vacation To City
    Then User Selects A Departure and Return Date Starting outside of 48Hours 
    Then User Selects Drivers Age Twenty One to Twenty Four Years Old
    Then User clicks on Search Vacations button
    Then User Clicks Book Car And Continues With Standard Bare Fare
    When user lands on passengers page 
    Then User enters Info for Primary Passenger Adult
    Then User Selects A Primary Driver 
    Then User Clears The Date Of Birth Of PAX One
    Then User Validates Primary Driver Is Required Text Under The Field
    Then User Attempts To Select A Primary Driver Again
    Then User Validates Primary Driver Is Required Text Under The Field
    Then User Enters A Date Of Birth Under Twenty One
    Then User Attempts To Select A Primary Driver Again
    Then User Validates Primary Driver Is Required Text Under The Field


  @EPIC25641
  Scenario Outline: Validate The Email Is Already Linked To A Free Spirit Member And Sign Up On the Passenger Info Page 
  Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User Eneters Info For Primary Passenger Along With A Pre Existing Free Spirit Email
    Then User Clicks On The Continue Button On Passenger Information Page
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC25644
  Scenario Outline: Validate The Email Is Already Linked To A Free Spirit Member Pop Up
  	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User Eneters Info For Primary Passenger Along With A Pre Existing Free Spirit Email
    Then User Clicks On The Continue Button On Passenger Information Page
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
      
  @EPIC27520
  Scenario Outline: Validate All Of The Fields On Passenger Information Page
    	Given Spirit airlines application
  	When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on continue with bare fare button
    When user lands on passengers page
    Then User enters Info for Primary Passenger Adult
    Then User Verifies That User Is On Passenger Info Page
    Then User Validates Contact Check Box 
    Then User Verifies All The Fields In Contact Box Are Auto Populated
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |
      
        
      