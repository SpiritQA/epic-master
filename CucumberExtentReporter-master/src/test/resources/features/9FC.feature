Feature: Nine DFC Member

  @EPIC26172
  Scenario: Nine FC Email Subscription CP Subscribe to Email Deals
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the manage subscription link form the list
    Then user enetres the customers email address
    Then user clicks on the continue button below the customers email address
    Then user complete the form and click on the subscribe button
    Then user can see that subscription is created

  @EPIC26174
  Scenario: Nine FC Email Subscription CP Update Email Account
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the free spirit link
    Then user clicks on the email subscription link on the left side of the list of links
    Then user enetres the customers email address
    Then user clicks on the continue button below the customers email address
    Then user complete the form and click on the subscribe button
    Then user can see that subscription is created

  @EPIC25434
  Scenario: Nine FC Email Subscription CP NEG Unable to Subscribe with existing email
    Given Spirit airlines application
    When user clicks on the nine dollar fair club
    Then user scrolldown to account section and enters the required information
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user click on the continue to step three button
    Then user navigates to the billing secetion and enters the all required information
    Then user clicks on terms and Conditions checkbox
    Then user clicks on the signup button

  #Then user validates the nine dollar fair club signup
  #Partially completed
  #@EPIC24581
  Scenario Outline: Bags Customer portal Booking portal Nine dollar FC Single Pax One Way Bare Fare
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one  adult passenger
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button for ninedfc
    Then user selects the primary passenger contact checkbox
    Then user clicks in the continue button
    When user landing on the bags page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25329
  Scenario: NineFC Booking CP BP Verify the add another card link takes you to the correct page
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the add another button
    Then user validates the navigates to add card page

  @EPIC25330
  Scenario: NineFC Booking Edit your primary card wireframe
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the edit link on the primary card table
    Then user verify the all billing information
    Then user uncheck the primary card check box
    Then user check the use same address check box
    Then user clicks on the save my changes button

  @EPIC25331
  Scenario: NineFC Booking CP BP Make sure the cancel button does not save your information on the edit primary card link
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the edit link on the primary card table
    Then user verify the all billing information
    Then user uncheck the primary card check box
    Then user check the use same address check box
    Then user clicks on the cancel button on the primary card
    Then user navigate back to the add  a card page and validate the text

  @EPIC25332
  Scenario: NineFC Booking CP BP Make sure the save button does save your information on the edit primary card link
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the edit link on the primary card table
    #Then user changes the all information for the primarycard
    Then user clicks on the save my changes button
    Then user clicks on the editbilling infromation link and validates the Billing address

#partially covered
  @EPIC25333
  Scenario: NineFC Booking NEG Enter Invalid information for the Edit link for primary card
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the edit link on the primary card table
    Then user enters the invalid characters for expiration date and validate the error messge
    Then user enters the invalid character for the address
    Then user enters the invalid characters in city field

  @EPIC25353
  Scenario: NineFC Booking CP BP Make sure the save button does save your information on the add a card page
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the add another button
    Then user validates the navigates to add card page
    Then user enters the valid card information to all fileds
    Then user clicks on the save my changes button

  #Then user go back to the account and validate the new card details
  @EPIC25358
  Scenario: NineFC Booking CP BP Delete a card that was added
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the add another button
    Then use selects the check box make this is my primary card
    Then user enters the valid card information to all fileds
    Then user clicks on the save my changes button
    Then user clicks on the delete button on additional card then switch to the popup and close the popup
    Then user clicsk on the delete button on additional card then switch to the popup and  clicks on delete the card

#  @EPIC26319
#  Scenario: NineFC Booking CP BP Cancel your NineFC Membership
#    Given Spirit airlines application
#    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
#    Then user clicks on the  login button on the popup
#    When user clicks on the members name
#    Then user clicks on the account link
#    Then user clisk on the cancel membership caret
#    Then user clicks on the cancel membership button and switch to the popup and clicks on keep membership
#    Then user selects the reason from the dropdown and clicks on the cancel membership button and switch to the popup close the popup button
#    Then user selects the reason from the dropdown and clicks on the cancel membership button and switch to the popup Clicks on the cancel mebership button

  @EPIC26331
  Scenario: NineFC Booking CP BP Bags Edit Billing information
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the edit button on the primary card section
    Then user verify that prmary card check box is selected or not in order to check the purcahse of NineFC

  @EPIC26332
  Scenario: NineFC Booking Add a additional card and verify it is updated on the edit card page
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user validates the primary card last four digits
    Then user validates the additional cards title
    Then user validates the additional card last four digits
    Then user validate that still user able to add another card

  @EPIC17113
  Scenario Outline: $9FC Sign up
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    #Then user get pnr and navigate to checkin entry
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and signup success

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC17114
  Scenario Outline: $9FC Sign in
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
     When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and login success

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC17119
  Scenario Outline: $9FC Sign in through sign up
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
     When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and FS login success

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC17986
  Scenario Outline: $9FC No, I dont want to sign up and save
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and continue with std fare

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC17988
  Scenario Outline: NEG_Validating Error message when Inputing Invalid password combinations
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
     When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and signup failure

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC17992
  Scenario Outline: Testing incorrect login
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and login failure

    Examples: 
      | Required stops |
      | 1 Stop         |

#  @EPIC18009
#  Scenario Outline: $9FC Dont know your password
#    Given Spirit airlines application
#    When User Clicks on Book link
#    Then User clicks on onewaytrip
#    Then User choose the from city FLL
#    Then User choose the To city LGA
#    Then user selects departures date
#    Then User selects number of passengers
#    Then User clicks on Search button
#    When user lands on flight page
#    Then user clicks on the selected flight
#    Then user selects the what time to fly "<Required stops>"
#    Then user switch to the popup and clicks on close button
#    When user lands on passengers page
#    Then user enters the personal info and contact address
#    Then user clicks in the continue button
#    When user landing on the bags page
#    Then user enters the one carry-on Bag
#    Then user enters the number of checked bag
#    Then user validates the bags total amount
#    Then user clicks on the standard pricing continue button
#    When user lands on seats page
#    Then user selects the continue without selecting the seats
#    When user lands on options page
#    Then user clicks on checkin
#    Then user clicks on the continue button on options page
#    When user landing on payment page
#    Then user enters the Payments information
#    Then user clicks on the Booktrip and handle the travel more popup
#    Then user get pnr and navigate to checkin entry
#    Then user change flights
#    Then user clicks on the selected flight
#    Then user selects the what time to fly "<Required stops>"
#   Then user clicks on the continue with 9DFC conitnue on FA page button and reset password

#    Examples: 
#      | Required stops |
#      | 1 Stop         |

  @EPIC18011
  Scenario Outline: $9FC No thanks button
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
 

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26284
  Scenario Outline: $9FC Enroll WireFrame
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and validate verbiages

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26278
  Scenario Outline: $9FC Sign up
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and signup success

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26279
  Scenario Outline: $9FC Sign in
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    #Then user clicks on the continue button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and login success

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26280
  Scenario Outline: $9FC Sign in through sign up
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and login success
    
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26281
  Scenario Outline: $9FC No, I dont want to sign up and save
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and continue with std fare

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26282
  Scenario Outline: NEG_Validating Error message when Inputing Invalid password combinations
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and signup failure

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26283
  Scenario Outline: Testing incorrect login
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue with 9DFC conitnue on FA page button and login failure

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26286
  Scenario Outline: $9FC No thanks button
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user change flights
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   

    Examples: 
      | Required stops |
      | 1 Stop         |

#  @EPIC26285
#  Scenario Outline: $9FC Dont know your password
#    Given Spirit airlines application
#    When User Clicks on Book link
#    Then User clicks on onewaytrip
#    Then User choose the from city FLL
#    Then User choose the To city LGA
#    Then user selects departures date
#    Then User selects number of passengers
#    Then User clicks on Search button
#    When user lands on flight page
#    Then user clicks on the selected flight
#    Then user selects the what time to fly "<Required stops>"
#    Then user switch to the popup and clicks on close button
#    When user lands on passengers page
#    Then user enters the personal info and contact address
#    Then user clicks in the continue button
#    When user landing on the bags page
#    Then user enters the one carry-on Bag
#    Then user enters the number of checked bag
#    Then user validates the bags total amount
#    Then user clicks on the standard pricing continue button
#    When user lands on seats page
#    Then user selects the continue without selecting the seats
#    When user lands on options page
#    Then user clicks on checkin
#    Then user clicks on the continue button on options page
#    When user landing on payment page
#    Then user enters the Payments information
#    Then user clicks on the Booktrip and handle the travel more popup
#    Then user get pnr and navigate to checkin entry
#    Then user change flights
#    Then user clicks on the selected flight
#    Then user selects the what time to fly "<Required stops>"
#     Then user clicks on the continue with 9DFC conitnue on FA page button and reset password
#
#    Examples: 
#      | Required stops |
#      | 1 Stop         |

  @EPIC5868
  Scenario Outline: Customer Info CP BP while log in as a 9DFC member you do not see banner on the bottom
    Given Spirit airlines application
    When user clicks on the sigin button and  switch to the popup and eneter the email address and password
    Then user clicks on the  login button on the popup
     When User Clicks on Book link
     Then User clicks on onewaytrip
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
     Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks in the confirmation button
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |
    

  @EPIC25434
  Scenario: Nine FC Email Subscription CP NEG Unable to Subscribe with existing email
    Given Spirit airlines application
    When user clicks on the nine dollar fair club
    Then user scrolldown to account section and enters the required information
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user click on the continue to step three button
    Then user navigates to the billing secetion and enters the all required information
    Then user clicks on the signup button

  @EPIC13501
  Scenario: $9FC Enrollment from $9FC Enrollment Page Links
    Given Spirit airlines application
    When user clicks on the nine dollar fair club
    Then user click on tool tip next to Get discounted fares and cheaper bags
    Then user verify popup verbiage appear after clicking tool tip next to Get discounted fares and cheaper bags
    Then user click on tool tip next to Covers everyone on your booking
    Then user verify popup verbiage appear after clicking tool tip next to Covers everyone on your booking
    Then user click on SIGN-UP NOW button
    Then user verify page is scroll down bottom of page
    Then user click on Terms and Conditions link
    Then user verify navigated URL after clicking Terms and Conditions link
    Then user click on F.A.Q link
    Then user verify navigated URL after clicking F.A.Q link
    Then user click on  Sign-up faster and easier link
    Then user verify popup details and close popup

  #Salim(17-Sep-2018):Assert are removed for below two steps as they are marked as BUG's
  #					1.Primary Phone Number field is excepting Alpha and Symbols
  #                   2.Billing Address field is excepting Alpha and Symbols
  #                   3.Country drop down is having invalid value "inserted by conversion"
  @EPIC25295
  Scenario: Nine FC Email Subscription CP NEG Unable to Subscribe with existing email
    Given Spirit airlines application
    When user clicks on the nine dollar fair club
    Then user verify Title error message
    Then user verify require First Name error message
    Then user verify invalid First Name error message
    Then user verify require Last Name error message
    Then user verify invalid Last Name error message
    Then user verify require Date of Birth error message
    Then user verify invalid Date of Birth error message
    Then user verify require Email error message
    Then user verify invalid Email error message
    Then user verify require Confirm Email error message
    Then user verify invalid Confirm Email error message
    Then user verify require Password error message
    Then user verify less than eight characters invalid Password error message
    Then user verify more than sixteen characters invalid Password error message
    Then user verify lower case eight characters invalid Password error message
    Then user verify lower case eight characters and upper case character invalid Password error message
    Then user verify lower case eight characters, upper case character and numbercial value invalid Password error message
    Then user verify lower case eight characters, upper case character and special character invalid Password error message
    Then user verify require Confirm Password error message
    Then user verify invalid Confirm Password error message
    Then user verify require Address error message
    Then user verify Symbol invalid Address error message
    Then user verify require City error message
    Then user verify Symbol invalid City error message
    Then user verify Number invalid City error message
    Then user verify require State error message
    Then user verify require Zip Code error message
    Then user verify less than four character Zip Code error message
    Then user verify Symbol invalid Zip Code error message
    Then user verify require Primary Mobile Number error message
    Then user verify invalid Primary Mobile Number error message
    Then user verify require Home Airport error message
    Then user verify require Name on Card error message
    Then user verify invalid Name on Card error message
    Then user verify require Card Number error message
    Then user verify invalid less than 15 numbers Card Number error message
    Then user verify require Expiration Date error message
    Then user verify invalid Expiration date error message
    Then user verify require CVV error message
    Then user verify invalid CVV error message
    Then user verify require Billing Address error message
    Then user verify invalid Billing Address error message
    Then user verify require Billing City error message
    Then user verify invalid Billing City error message
    Then user verify require Billing State error message
    Then user verify require Billing Zip/Postal Code error message
    Then user verify invalid Billing Zip/Postal Code error message
    Then user verify require Terms and Condition check box error message
