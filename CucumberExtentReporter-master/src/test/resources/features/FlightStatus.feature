Feature: verify user able to check flight status scenario

  	Scenario: verify user able to check flight status by destination 
    Given Spirit airlines application
    When User Clicks on Flight Status link
    Then User selects the check by destination
    Then User enters the departure city 
    Then user enters the arival city
    Then User selects the day of journey
    Then User clicks on check status
    Then User closes the browser
    
 
    Scenario: verify user able to check flight status by flight number 
    Given Spirit airlines application
    When User Clicks on Flight Status link
    Then User selects the check by flight number
    Then User enters the flight number 
    Then User selects the day of journey
    Then User clicks on check status
    Then User closes the browser
    
    #patially completed
    @EPIC2891
    Scenario: Home Page CP BP Flight Status by Number
    Given Spirit airlines application
    When User Clicks on Flight Status link
   Then User selects the check by destination
    Then user validates the text on the search widget
    Then user validates the default date 
    #Then user clicks on the checkstatus and validate the error icon at fromcity
    Then user clicks on the from city and select the city
    Then user clicks on the to city and select the city
    Then user clicks on the checkstatus button 
    Then user clicks on the get updates button 
    Then user clicks on the cancel button on flight status page 
    Then user enters the email and clicks on the get updates button and switch to the popup and close the popup
    Then user selects the new destination and date and check the flight status
    
    
   
    