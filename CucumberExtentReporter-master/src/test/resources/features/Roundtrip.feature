Feature: RoundTrip

  @Roundtrip
  Scenario Outline: Roundtrip
    Given Spirit airlines application
    Then User choose the from city FLL
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flght stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags

    Examples: 
      | Required stops | Return flight stops |
      | 1 Stop         | 1 stop              |

      