Feature: verify content and functionality of Header

  @EPIC25706
  Scenario Outline: Sign in to FS account with FS number
    Given Spirit airlines application
    When User clicks on sign in and then switch to the popup
    Then User logs in with fsnumber for english login "<FSnumber>" "<Password>"
    Then User closes the browser

    Examples: 
      | FSnumber   | Password  |
      | 1000216092 | Spirit11! |

  @EPIC2892
  Scenario Outline: Sign in to FS account with Email number
    Given Spirit airlines application
    When User clicks on sign in and then switch to the popup
   Then User logs in with fsnumber for english login "<FSnumber>" "<Password>"
    Then User closes the browser

    Examples: 
      | FSemail               | Password  |
      | joe.flyer2@spirit.com | Spirit11! |

  @EPIC2898
  Scenario: Negative Test links not displayed on Spanish header
    Given Spirit airlines application
    When User clicks on Espanol button
    Then Verfiy that the spanish home header does not contain links
    Then verify the spirit logo is on the left
    Then verify the correct spanish header links are displayed
    Then User closes the browser

  @EPIC25668
  Scenario: validate the Header on the FA page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
      Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then User closes the browser

  @EPIC25764
  Scenario: validate the Header on the 9DFC Enrollment Page
    Given Spirit airlines application
    Then user clicks on 9DFC enrollment header link
    When user lands NineDFC enrollment page
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed
    Then User closes the browser

#partially covered
  @EPIC25814
  Scenario: validate the Header on the Spanish Ayuda Page
    Given Spirit airlines application
    When User clicks on Espanol button
    Then Verfiy that the spanish home header does not contain links
    Then user clicks on the ayuda link
    When user lands on Ayuda page
    Then verify the spirit logo is on the left
    Then verify the correct spanish header links are displayed
    Then User closes the browser

#partially covered
  @EPIC25669
  Scenario: validate header in book myTrips checkIn and flightStatus links
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the book header link
    When user lands on the home page
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the myTrips header link
    When user lands on the myTrips home page
    Then User Clicks on Booking Tab
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
     Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the check in header link
    When user lands on the check in home page
    Then User Clicks on Booking Tab
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
     Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the flightStatus in header link
    When user lands on the flightstatus in home page
    Then User closes the browser

#partially covered 
  @EPIC25816
  Scenario: validate that header on the spanish contacts page
    Given Spirit airlines application
    When User clicks on Espanol button
    Then Verfiy that the spanish home header does not contain links
    Then user clicks on the contactanos link
    When user lands on contactanos page
    Then verify the spirit logo is on the leftside
    Then verify the correct spanish header links are displayed
    Then User closes the browser

  @EPIC25761
  Scenario: verify redirected to deals page from header
    Given Spirit airlines application
    When user clicks on the deals link in the header
    #page has not been completed
    Then user lands on the deals page
    Then User closes the browser
    
    
    @EPIC25806
	Scenario: verify header 9DFC link redireects correctly
    Given Spirit airlines application
	When User clicks on Espanol button
    Then User clicks on the Spanish 9DFC Header link
    Then User Lands on the Spanish Enrollment page
    
    @EPIC25725
    Scenario: Validate the Spanish header main links
    Given Spirit airlines application
	When User clicks on Espanol button
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date 
     Then User selects number of passengers
    Then User clicks on Buscar vuelo button
    When user lands on Spanish flight page
    Then user clicks on the book header link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date 
     Then User selects number of passengers
    Then User clicks on Buscar vuelo button
    When user lands on Spanish flight page
    Then user clicks on the myTrips header link    
    When user lands on the Sapnish Mis viajes home page
    Then User Clicks on Booking Tab
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date 
    Then User selects number of passengers
    Then User clicks on Buscar vuelo button
    When user lands on Spanish flight page   
    Then user clicks on the check in header link    
    When user lands on the registrarse in home page
    Then User Clicks on Booking Tab
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date 
     Then User selects number of passengers
    Then User clicks on Buscar vuelo button
    When user lands on Spanish flight page      
    Then user clicks on the flightStatus in header link
    When user lands on the Estado del Vuelo in home page    
    Then User closes the browser
    
    #@EPIC2898
    Scenario: Validate Spnaish Homepage does not have main header links
    Given Spirit airlines application
	When User clicks on Espanol button
    Then Verfiy that the spanish home header does not contain links
	Then verify the spirit logo is on the left
	Then verify the correct spanish header links are displayed
	Then User closes the browser
	
	@EPIC25807
	Scenario: Validate the spanish header 101 link redirects correctly
	Given Spirit airlines application
	When User clicks on Espanol button
	Then user Clicks on the Spanish spirit101 header link
	#Page is not translated to spanish
	Then user verifies the spanish spirit101 page
	Then User closes the browser
	
	@EPIC25804
	Scenario: Validate the spanish ofertas link redirects user correctly
	Given Spirit airlines application
	When User clicks on Espanol button
	Then user Clicks on the Spanish Ofertas header link
	Then user verifies the spanish Ofertas page
	Then User closes the browser
	
	@EPIC25792
	Scenario Outline: Log in throught the spanish header via FS Number
	Given Spirit airlines application
	When User clicks on sign in and then switch to the popup
 	Then User logs in with Spanish fsnumber "<FSnumber>" "<Password>"
    Then User closes the browser
    Examples:
    | FSnumber | Password | 
    | 1000216092|Spirit11!|	
	
	@EPIC25729
	Scenario Outline: Log in throught the spanish header via FS Email
	Given Spirit airlines application
	When User clicks on Espanol button
	When User clicks on sign in and then switch to the popup
    Then User logs in with Spanish email "<FSemail>" "<Password>"
    Then User closes the browser
    Examples:
    | FSemail | Password | 
    | joe.flyer2@spirit.com| Spirit11! |
    
    @EPIC25704
    Scenario Outline: Sign in to 9dfc account via Email     
    Given Spirit airlines application
    When User clicks on sign in and then switch to the popup
    Then User logs in with email "<FSemail>" "<Password>"
    Then User closes the browser
    Examples:
    | FSemail | Password | 
    | john.flyer9dfc@spirit.com| Spirit11! |
    
    @EPIC25752
    Scenario: Validate Links on the contact us page  
    Given Spirit airlines application
    Then user clicks on the contact us link
	When user lands on contact us page
	Then validate header left aligned
	Then user clicks the FAQ link
	Then User Navigates to the previous page
	When user lands on contact us page
	#Does not redirect correctly
	Then User clicks the spirit101 link
	Then User Navigates to the previous page
	When user lands on contact us page
	Then User closes the browser
	
	@EPIC25733
	Scenario: Validate Help Links on the header  
    Given Spirit airlines application
    Then user clicks on the header help link
    When User lands on the help page
    Then User closes the browser
    
    @EPIC2896
	Scenario: Validate spirit101 Links on the header  
    Given Spirit airlines application
    Then user clicks on the header spirit101 link
    When User lands on the spirit101 page
    Then User closes the browser
    
    @EPIC25766
    Scenario: Validate the Destinations page
    Given Spirit airlines application
    Then user clicks on the header destinations link
    When user lands on the destinations page
    Then verify the spirit logo is on the leftside
    Then Verfiy that the english header contain main links
    Then verify the correct english header links are displayed  
    Then User closes the browser
    
