Feature: AC Seat Test

  @EPIC3123
  Scenario Outline: Seats page continue without selecting seats
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    Then User closes the browser

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC3269
  Scenario Outline: Seats Page Exit Row Pop-Up Accept With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects "<Passenger count>" passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the five passnegers information
    Then user clicks on Primary passenger is the contact person
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    #Then user clicks on the exit row on seats page
    #Then user clicks on the exit row on seats page for plane two
    #Then user clicks on the continue button on seats page

    Examples: 
      | Required stops | Passenger count |
      | 1 Stop         |               5 |



#Partially covered 
  @EPIC3131
  Scenario Outline: Seats Page CP BP-CI-MT SinglePAX UMNR With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then user selects the one child
    Then user clicks on the search flight button and switch to the popup and enters the date of birth of the UMNR child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the passenger information 
    Then user clicks on Primary passenger is the contact person
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user attempts to selects the Exit row seat 
    Then user naviagte down to seats total content block and validate the price 
    Then user clciks on the itinerary caret and validate the bags amount 
    Then user selects the continue without selecting the seats
    
    
    Examples: 
      | Required stops |
      | 1 Stop         |

      
  @EPIC13107
  Scenario Outline: Wireframe Validation of the Seats Page  
  	Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then User validates the SEATS TOTAl content block has a Black background
    Then User Validates the SEATS TOTAL content block White Font
    Then User Validates the Continue Button has a Blue background
    Then User Validates the Continue Button has white font
    Then user validates the blue font on CONTINUE WITHOUT SELECTING SEATS
    
    
    Examples: 
      | Required stops |
      | 2 Stop         |
        
   
      