Feature: Nine_DFC_Member_Standalone

  #partial
  @EPIC13269
  Scenario Outline: $9FC Booking_CP_BP_Passenger_ Dynamic Shopping Cart  test Wireframe of 9DFC sign up
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user Creates FS Account
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    Then user clicks on the Book now button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city MSY
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user switch to the popup and clicks on close button for FS member
    When user lands on passengers page
    Then user verify shopping cart is displayed
    Then user Click the caret next to the Join NineFc and Save
    Then user Verify verbiage on the Join and Save drop down
    Then user click the Join Now and Save button
    Then user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS
    Then user Verify the NineFC membership due is added to the cart
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    When user clicks on the members name
    Then user clicks on the account link

    #Then user verifys Ninefc membership on account
    Examples: 
      | Required stops |
      | 1 Stop         |

  #partial
  @EPIC13270
  Scenario Outline: $9FC Booking_CP_BP_Bags_ Dynamic Shopping Cart test Wireframe of 9DFC sign up
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user Creates FS Account
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    Then user clicks on the Book now button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city MSY
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user switch to the popup and clicks on close button for FS member
    When user lands on passengers page
    Then user clicks in the continue button
    When user landing on the bags page
    Then user verify shopping cart is displayed
    Then user Click the caret next to the Join NineFc and Save
    Then user Verify verbiage on the Join and Save drop down
    Then user click the Join Now and Save button
    Then user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS
    Then user Verify the NineFC membership due is added to the cart
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    When user clicks on the members name
    Then user clicks on the account link

    #Then user verifys Ninefc membership on account
    Examples: 
      | Required stops |
      | 1 Stop         |

  #partial
  @EPIC13271
  Scenario Outline: $9FC Booking_CP_BP_Seats_ Dynamic Shopping Cart test Wireframe of 9DFC sign up
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user Creates FS Account
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    Then user clicks on the Book now button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city MSY
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user switch to the popup and clicks on close button for FS member
    When user lands on passengers page
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    Then user verify shopping cart is displayed
    Then user Click the caret next to the Join NineFc and Save
    Then user Verify verbiage on the Join and Save drop down
    Then user click the Join Now and Save button
    Then user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS
    Then user Verify the NineFC membership due is added to the cart
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    When user clicks on the members name
    Then user clicks on the account link

    #Then user verifys Ninefc membership on account
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13272
  Scenario Outline: $9FC Booking_CP_BP_Options_Dynamic Shopping Cart test Wireframe of 9DFC sign up
    Given Spirit airlines application
    When user clicks on the signin button and switch to the popup and clicks on the signup button
    Then user Creates FS Account
    Then user clicks on the continue to step two button
    Then user navigates to the contact section and enters the required information
    Then user clicks on the Term and conditions check box
    Then user clicks on the  Free signup button
    Then user clicks on the Book now button
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city MSY
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user switch to the popup and clicks on close button for FS member
    When user lands on passengers page
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user verify shopping cart is displayed
    Then user Click the caret next to the Join NineFc and Save
    Then user Verify verbiage on the Join and Save drop down
    Then user click the Join Now and Save button
    Then user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS
    Then user Verify the NineFC membership due is added to the cart
    Then user clciks on the checkin drop down and selects the i will check in at mobile for free
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    When user clicks on the members name
    Then user clicks on the account link

    #Then user verifys Ninefc membership on account
    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC13433
  Scenario: $9FC Non-Booking Enrollment from $9FC Enrollment Page Wireframe
    Given Spirit airlines application
    Then user Click on NineFC link under get to know us on home page
    When user lands on NineFC signup page
    Then user verifys the title of the page NineFC and NineFC fare club logo is displayed
    Then user verifys Join the club Verbiage
    Then user verifys SignUp Now button displayed
    Then user verifys Images displayed and Verbiage is correct
    Then user Verifys the Find More Information verbiage
    Then user clicks on Signup faster and easier link
    Then user verifys popup displays and validates verbiage

 

  @EPIC25321
  Scenario: $9FCBooking_Enrollment $9FC enrollment page information wireframe
    Given Spirit airlines application
    Then user Click on NineFC link under get to know us on home page
    When user lands on NineFC signup page
    Then user Verify the header of information table
    Then user Verify Title dropdown
    Then user Verify textbox Labeled First Name is displayed
    Then user Verify textbox Labeled Middle Name is displayed
    Then user Verify textbox Last Name is displayed
    Then user Verify Suffix dropdown
    Then user Verify textbox Labeled Date of Birth is displayed
    Then user Verify the Important Note is displayed
    Then user Verify textbox labeled Email is displayed
    Then user Verify textbox labeled Confirm Email is displayed
    Then user Verify textbox labeled Password is displayed
    Then user Verify textbox labeled Confirm Password is displayed
    Then user inputs all account information
    Then user clicks on the continue to step two button
    Then user Verify textbox labeled Address is displayed
    Then user Verify textbox labeled Address Continued is displayed
    Then user Verify textbox labeled City is displayed
    Then user Verify textbox labeled ZipCode is displayed
    Then user Verify textbox labeled Country is displayed
    Then user Verify textbox labeled Primary Phone Number is displayed
    Then user Verify textbox labeled Secondary Phone Number is displayed
    Then user Verify Dropdown labeled Home Airport is displayed
    Then user Verify Dropdown labeled Secondary Airport is displayed
    Then user inputs all Contact information
    Then user click on the continue to step three button
    Then user Verify textbox labeled Name on Card is displayed
    Then user Verify textbox labeled Card Number is displayed
    Then user Verify textbox labeled Expiration Date is displayed
    Then user Verify textbox labeled Security Code is displayed
    Then user Verify checkbox labeled Billing Address is displayed
    Then user Verify textbox labeled Address for billing is displayed
    Then user Verify textbox labeled City for billing is displayed
    Then user Verify Dropdown labeled State for billing is displayed
    Then user Verify textbox Zip Postal Code for billing is displayed
    Then user Verify Dropdown labeled Country for billing is displayed
    Then user Verify the terms and conditions
    Then user Verify Button labeled SignUP and Start Saving Today is displayed

 
      
         @EPIC25334
  Scenario: $9FCBooking_WireFrame for My Account page $9FC Member
  Given Spirit airlines application
  Then user clicks on the sigin button and switch to the popup enters the NineFC memeber and password
  Then user clicks on the login button on the popup
  When user clicks on the members name
  Then user clicks on the account link
  Then user Verify that the title is Your FREE SPIRIT Account
  Then user Verify the links on the left hand side of the Account page
  Then user Verify the welcome message
  Then user Verify that your current miles for the member are shown
  Then user Verify that your account number is correct
  Then user Verify that your milleage earning tier is the correct tier
  Then user Verify that there is a section with a marketing tile
  Then user Verify that there is a label that says Your Membership
  Then user Verify that there is a section that says Member Name
  Then user Verify the section that says Date Joined
  Then user Verify the section that says Paid Membership type
  Then user Verify the section that says Days Left in membership
  Then user Verify the section that says Cancel membership
  Then user Verify the section that says Your Reservations
  Then user Verify the section that says Locate a reservation
  Then user Verify the section with the confirmation code
  Then user Verify the section with Current Reservations
  Then user Verify the section with Past Reservations
  
    
      
      
 
  @EPIC25341
  Scenario: NineFCBooking_CP_BP_Add card link WireFrame
    Given Spirit airlines application
   Then user clicks on the sigin button and switch to the popup enters the NineFC memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the add another button
    Then user validates the navigates to add card page
    Then user Verify the headline of the page on add card page
    Then user Verify there is a name card text box on add card page
    Then user Verify there is a Card Number text box on add card page
    Then user Verify there is a Expiration Date text box on add card page
    Then user Verify there is no section for Security code on add card page
    Then user Verify the Please use for my billing information check box on add card page
    Then user Click the Please use your address for my billing information check box on add card page
    Then user Verify the Address text box on add card page
    Then user Verify the City text box on add card page
    Then user Verify the Zip Postal Code text box on add card page
    Then user Verify the State drop box on add card page
    Then user Verify the country drop box on add card page
    Then user Verify there is a Save and Cancel button on add card page
     
      
      @EPIC25354
  Scenario: $9FCBooking_NEG_Enter Invalid information for the add a card page
    Given Spirit airlines application
   Then user clicks on the sigin button and switch to the popup enters the NineFC memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user clicks on the edit billing information link
    Then user clicks on the add another button
    Then user validates the navigates to add card page
    Then user Verify the headline of the page on add card page
    Then user clicks on save button on add card page
    Then user Enter invalid characters for name and enter valid information for all other fields
	Then user Enter invalid characters for card numberand enter valid information for all other fields
	Then user Enter invalid charaters for expiration date and enter valid information for all other fields
    Then user Click the Please use your address for my billing information check box on add card page
	Then user clicks on save button on add card page
	Then user Enter invalid characters for Address and enter valid information for all other Fields Click the save button
	Then user Enter invalid characters for City and enter valid information for all other fields
	Then user Enter invalid characters for ZipPostal code and enter valid information for all other fields
	Then user Do not enter any information for State and country and enter valid information for all other fields
    
   
    
     @EPIC26173
  Scenario: $NineFC Email Subscription_CP_Unsubscribe Account
    Given Spirit airlines application
    Then user clicks on the sigin button and switch to the popup enters the NineFC memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user click the Email Subscriptions link
	Then user enters Email on Your Fs Account page
	Then user clicks continue on email notification signin page
	Then user clicks the checkbox Unsubscribe from Email Deals
	Then user clikcs Submit
	Then user verify Email deals popup

   
      
     @EPIC26175
  Scenario: $NineFC Email Subscription_CP_Unsubscribe Account
    Given Spirit airlines application
    Then user clicks on the sigin button and switch to the popup enters the NineFC memeber and password
    Then user clicks on the  login button on the popup
    When user clicks on the members name
    Then user clicks on the account link
    Then user click the Email Subscriptions link
	Then user enters Email on Your Fs Account page
	Then user clicks continue on email notification signin page
	Then user verify member is already subsribed

      
     @EPIC26337
  Scenario: NineFC Booking_CP_BP_Passenger_ Dynamic Shopping Cart Wireframe for NineDFC member
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To city MSY
    Then user selects departures date
    Then user selects the one passenger
    Then User clicks on Search button
    When user lands on flight page
      Then user clicks first available departing flight
    #Then User clicks on first available return flight
    #Then user clicks on the selected flight
    #Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user Click the caret next to the Join NineFc and Save
	Then user Verify verbiage on the Join and Save drop down
	Then user verify Enrollment at the bottom
	Then user login as nine DFC member using enter key
	Then user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS
	Then user verify Enrollment at the bottom is suppressed
	
           #Examples: 
      #| Required stops |
      #| 1 Stop         |
	  