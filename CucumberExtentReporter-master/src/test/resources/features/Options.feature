Feature: Options Page

  @EPIC17459
  Scenario Outline: Negative test, check in option RT Lima Peru
    Given Spirit airlines application
    Then User choose the from city FLL
    Then User choose the To city LIM
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks first available departing flight
    Then User clicks on first available return flight
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user selects prepaid check in RT to LIM
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then user validates that they are not charged incorrectly "<Correct>"
    Then User closes the browser

    Examples: 
      | Required stops | Return flght stops | Correct |
      | 1 stop         | 1 stop             | $10.00  |

  @EPIC1366
  Scenario Outline: UMNR does not have check in option
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    #Then User accepts UMNR FEE
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then Validate that the check in option is disabled

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC3308
  Scenario Outline: Check in for free
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user selects Free check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then user validates that they are not charged incorrectly "<Correct>"
    Then User closes the browser

    Examples: 
      | Required stops | Correct |
      | 0 stop         | $0.00   |

  @EPIC3315
  Scenario Outline: Check in option - Not Sure
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user selects not sure option
    #Then user validates the options total is correct "<Total>"
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then user validates that they are not charged incorrectly "<Correct>"
    Then User closes the browser

    Examples: 
      | Required stops | Correct |
      | 0 stop         | $0.00   |

  @EPIC3312
  Scenario Outline: Check in option - Prepaid
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then user validates that they are not charged incorrectly "<Correct>"
    Then User closes the browser

    Examples: 
      | Required stops | Correct |
      | 0 stop         | $10.00  |

  ############################################## Check in Options #############################
  @EPIC26670
  Scenario Outline: Flight Flex plus shortcut boarding
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add Flight Flex
    Then User selects to add shortcut boarding
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26671
  Scenario Outline: Flight Flex+Shortcut Security
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add Flight Flex
    Then User selects to add shortcut security
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26673
  Scenario Outline: Flight Flex plus shortcut security AND ShortCut boarding
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add Flight Flex
    Then User selects to add shortcut security
    Then User selects to add shortcut boarding
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26681
  Scenario Outline: Flight Flex plus shortcut security AND ShortCut boarding
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add Flight Flex
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26674
  Scenario Outline: User validates shortcut boarding already added with carry on
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user validates that the Shortcut boarding outline is green when carry on added
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26669
  Scenario Outline: User validates shortcut boarding boardering boarder turns green when added
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add shortcut boarding
    Then user validates that the Shortcut boarding outline is green
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26668
  Scenario Outline: User validates shortcut boarding boardering boarder turns green when added
    Given Spirit airlines application
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add shortcut security
    Then user validates that the Shortcut security outline is green
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  @EPIC26672
  Scenario Outline: Flight Flex plus shortcut security AND ShortCut boarding
    Given Spirit airlines application
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then User selects to add shortcut security
    Then User selects to add shortcut boarding
    Then user selects prepaid check in
    Then user clicks on the continue button on options page
    Then user landing on payment page
    Then User closes the browser

    Examples: 
      | Required stops |
      | 0 stop         |

  ########################### Stand-Alone #####################################
  @EPIC25799
  Scenario Outline: Validate that no activities for 9 pax on options page
    Given Spirit airlines application
    Then User choose from city FLL
    Then User choose To city LAS
    Then user selects "<Passenger count>" passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flight stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the nine passnegers information
    Then user clicks on Primary passenger is the contact person
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user validates that Car Rentals, Hotels, and Activities is not displayed
    Then user verifies all Extras are displayed
    Then user selects not sure option
    Then user validates that the check in div border is green
    Then User closes the browser

    Examples: 
      | Required stops | Return flight stops | Passenger count |
      | 2 Stop         | 1 Stop              |               9 |

  @EPIC25607
  Scenario Outline: Activities wire frame
    Given Spirit airlines application
    Then User choose from city FLL
    Then User choose To city LAS
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flight stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user validates the Activities on page
    Then user clicks on view all activities
    When user lands on activities page
    Then user validates the activities heading
    #Need to add remaining steps here
    #
    #
    Then User closes the browser

    Examples: 
      | Required stops | Return flight stops | Passenger count |
      | 2 Stop         | 1 Stop              |               9 |

  @EPIC25342
  Scenario Outline: Car wire frame
    Given Spirit airlines application
    Then User choose from city FLL
    Then User choose To city LAS
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the Return selected flight
    Then user selects the return flight "<Return flight stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the number of checked bag
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on view all cars
    When user lands on cars page
    Then user validates the cars heading
    Then user validates the search boxes
    Then user validates the first car box
    #Create code for this
    #Uplift currently not working
    Then user validates the Uplift
    #need to complete remaining steps
    Then User closes the browser

    Examples: 
      | Required stops | Return flight stops | Passenger count |
      | 2 Stop         | 1 Stop              |               9 |
