Feature: CheckIn
#partially completed
  @EPIC26008
  Scenario Outline: Flow CP CI Flow for flight only yes to all
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects one adult passenger for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    #    When user lands on flight page
    #    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user verify seats pop up does not appear
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26015
  Scenario Outline: Flow CP CI Flow for flight only yes to all except seats and car select seats
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks on Primary passenger is the contact person
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up appears
    Then user clicks Get Random Seats on choose your seats popup
    Then user verify cars pop up does appear
    Then user clicks no to cars pop up
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26055
  Scenario Outline: Flow CP CI Flow for flight only max bags no seats no car no travel no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up appears
    Then user clicks Get Random Seats on choose your seats popup
    Then user verify cars pop up does appear
    Then user clicks no to cars pop up
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26057
  Scenario Outline: Flow CP CI Flow for flight only max bags no seats car no travel no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
     Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects one adult passenger for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up appears
    Then user clicks Get Random Seats on choose your seats popup
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26059
  Scenario Outline: Flow CP CI Flow for flight only max bags no seats car travel no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
      Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects one adult passenger for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up appears
    Then user clicks Get Random Seats on choose your seats popup
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26060
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks no to cars pop up
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26062
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks no to cars pop up
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26069
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    #    When user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks no to cars pop up
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26073
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    #    When user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    #	Then user clicks yes to random car and continue
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26074
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    #    When user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    #	Then user clicks yes to random car and continue
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26077
  Scenario Outline: Flow_CP_CI_Flow for flight only non max bags, no seats, car, travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User inputs drivers age
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up appears
    Then user clicks Get Random Seats on choose your seats popup
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verify Travel Guard pop up does not appear
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26063
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, car, travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26064
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26066
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks Select car button
    Then user Selects a Car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26070
  Scenario Outline: Flow CP CI Flow for flight only max bags, no seats, car, travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26071
  Scenario Outline: Flow CP CI Flow for flight only max bags, no seats, car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26072
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, car, travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26075
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26078
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #    Then user selects the what time to fly "<Required stops>"
    #    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26080
  Scenario Outline: Flow CP CI Flow for flight only max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Click on the plus button next to carryon bag
    Then user Click on the plus button next to checked bags Max bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user clicks on Select Seats
    Then user selects the continue without selecting the seats
    Then user verify cars pop up does appear
    Then user clicks Select car button
    #	Then user continues without car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26082
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, no car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    #    Then User clicks on RoundTrip
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user clicks Nope im good to pro tip pop up
    Then user Verify seats pop up does not appears
    Then user selects the continue without selecting the seats
    Then user verify cars pop up does appear
    Then user clicks Select car button
    #	Then user continues without car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26083
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, car, travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26084
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, car, no travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26085
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, no car, travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26087
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, no car, travel, no ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26088
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks Select car button
    #	Then user continues without car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks no to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26089
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, no car, travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks Select car button
    #	Then user continues without car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26090
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, no car, travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User inputs drivers age
    Then User clicks on Search button
    Then User clicks on Search Vacations button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks Select car button
    #	Then user continues without car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26094
  Scenario Outline: Flow CP CI Flow for flight only non max bags, no seats, car,  travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    Then User clicks yes to travel Guard
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26103
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, car, travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    Then User clicks yes to travel Guard
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26104
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User selects over age of driver
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on Shortcut Boarding
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26105
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User clicks yes to travel Guard
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user clicks Select car button
    Then user Selects a Car
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user clicks yes to Travel Guard popup
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC26108
  Scenario Outline: Flow CP CI Flow for flight only non max bags, seats, no car, no travel, yes ancillary
    Given Spirit airlines application
    When User Clicks on Book link
    Then User choose the from FLL City
    Then User choose the To city LGA
    Then user selects departure date and return date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user selects the return flight "<Required stops>"
    Then user clicks book car button
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as nine DFC member
    Then user verifies Member with nine DFC is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user clicks on Standard Pricing
    When user lands on seats page
    Then user Selects a seat
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    Then User clicks yes to travel Guard
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
    Then user clicks Checkin and Print Boarding Pass
    Then user verify Bags pop up does not appear
    Then user Verify seats pop up does not appears
    Then user verify cars pop up does appear
    Then user verify user lands on extras page
    Then user clicks on the continue button on the extras page
    Then user verifys Travel Guard popup
    Then user verify Payment Page does not appear
    Then user verify Hazmat popup
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |
@EPIC3403
  Scenario Outline: Passport_Validate invalid characters are not accepted for passport number
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and validate
   Examples: 
      | Required stops |
      | 2 Stop         |
      
  @EPIC3522
  Scenario Outline: SinglePAX _ Edit-Add Passport_Verify Passport Information page
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and validate
   Examples: 
      | Required stops |
      | 2 Stop         |
      
 @EPIC3530
  Scenario Outline: SinglePAX _ Edit-Add Passport_cancel button
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and cancel changes
   Examples: 
      | Required stops |
      | 2 Stop         |
      
 @EPIC3531
  Scenario Outline: SinglePAX _ Edit-Add Passport_save button
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and save changes
   Examples: 
      | Required stops |
      | 2 Stop         |
      
 @EPIC3532
  Scenario Outline: SinglePAX _ Edit-Add Passport_Verify middle name pop box is working correctly
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and verify middle name popup
   Examples: 
      | Required stops |
      | 2 Stop         |
      
@EPIC3536
  Scenario Outline: SinglePAX _ Edit-Add Passport_Verify proceed without a middle name works correctly
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and proceed without middlename
   Examples: 
      | Required stops |
      | 2 Stop         |
 
 @EPIC3595
  Scenario Outline: SinglePAX _ Edit-Add Pasport_Validate passport expiration date one day after return day
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and validate passport expiration date one day after return day
   Examples: 
      | Required stops | 
      | 2 Stop         | 
      
@EPIC3598
  Scenario Outline: SinglePAX _ Edit-Add Passport_apostrophe in middle name
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To MDE
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and verify middle name with apostrophe
   Examples: 
      | Required stops |
      | 2 Stop         |
      
@EPIC25508
  Scenario Outline: Timatic_Passport Pop-up
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To SAP
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and verify timatic passport popup
   Examples: 
      | Required stops |
      | 2 Stop         |
      
@EPIC25545
  Scenario Outline: Timatic_Passport Pop-up_Multi
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To SAP
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button when you need POT
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks add or edit passport informations links and verify timatic passport popup multi
   Examples: 
      | Required stops |
      | 2 Stop         |
      
@EPIC25673
  Scenario Outline: Station Advisory
   Given Spirit airlines application
   When User Clicks on Book link
   Then User clicks on onewaytrip
   Then User choose the from city
   Then User choose the To DTW
   Then user selects departures date
   Then User selects number of passengers
   Then User clicks on Search button
   When user lands on flight page
   Then user clicks on the selected flight
   Then user selects the what time to fly "<Required stops>"
   Then user switch to the popup and clicks on close button
   When user lands on passengers page
   Then user enters the personal info and contact address
   Then user clicks in the continue button
   When user landing on the bags page
   Then user enters the one carry-on Bag
   Then user enters the number of checked bag
   Then user validates the bags total amount
   Then user clicks on the standard pricing continue button
   When user lands on seats page
   Then user selects the continue without selecting the seats
   When user lands on options page
   Then user clicks on checkin
   Then user clicks on the continue button on options page
   When user landing on payment page
   Then user enters the Payments information
   Then user clicks on the Booktrip and handle the travel more popup
   When user lands on confirmation page
   Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user validated station advisory at checkin page
   Examples: 
      | Required stops |
      | 1 Stop         |