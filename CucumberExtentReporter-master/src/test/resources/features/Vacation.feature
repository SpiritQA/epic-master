Feature: Vacation page

  @EPIC18076
  Scenario: Search Widget BP Flight plus Car one Pax
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects one adult passenger for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button

  @EPIC18077
  Scenario: Search Widget BP Flight Car Milti PAX DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects two adult passenger for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button

  @EPIC18078
  Scenario: Search Widget BP Flight Car one Pax with Lap Child DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    Then user clicks on the vacation link
    Then User selects the vacation from city
    Then User selects the vacation To city
    Then user selects vacation departure date and return date
    Then User selects one adult passenger with lap child for vacation
    Then User selects over age of driver
    Then User clicks on Search Vacations button and switch to the young travelers popup and enters the child DOB

  @EPIC3343
  Scenario: Search Widget_CP_BP_Flight+Hotel 1 Pax, DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then User Verify One room is selected by default
    Then user selects vacation dates
    Then User Selects One adult
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC3539
  Scenario: Search Widget_CP_BP_ Flight+Hotel Multi PAX DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LGA
    Then user selects vacation dates
    Then User Selects Three adults
    Then User Selects Two rooms
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC3385
  Scenario: Search Widget_CP_BP_Flight+Hotel 1 Pax with Lap child DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LGA
    Then user selects vacation dates
    Then user selects the one child and one adult passengers
    Then User Verify One room is selected by default
    Then User clicks on Search vacations button
    Then User Enters LapChild Dob
    When user lands on Select your Hotel page

  @EPIC3396
  Scenario: Search Widget_CP_BP_Flight+Hotel One Pax with INF with seat DOM-DOM
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then user selects vacation dates
    Then user selects the one child and one adult passengers
    Then User Verify One room is selected by default
    Then User clicks on Search vacations button
    Then User Enters Child Dob require seat
    When user lands on Select your Hotel page

  @EPIC3399
  Scenario: Search Widget_CP_BP_Fligth+Hotel Multi Pax with Multi Children, DOM- DOM
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then user selects vacation dates
    Then user selects the two child and two adult passengers
    Then User Selects Two rooms
    Then User clicks on Search vacations button
    Then User Enters Child Dob for two children on popup
    When user lands on Select your Hotel page

  @EPIC3539
  Scenario: Search Widget_CP_BP_ Flight+Hotel Multi PAX DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then user selects vacation dates
    Then User Selects Three adults
    Then User Selects Two rooms
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC3541
  Scenario: Search Widget_CP_BP_Flight+Hotel One Pax DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then User Verify One room is selected by default
    Then user selects vacation dates
    Then User Selects One adult
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC3396
  Scenario: Search Widget_MP_BP_Flight+Hotel One Pax with INF with seat DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then user selects vacation dates
    Then user selects the one child and one adult passengers
    Then User Verify One room is selected by default
    Then User clicks on Search vacations button
    Then User Enters Child Dob require seat
    When user lands on Select your Hotel page

  @EPIC3385
  Scenario: Search Widget_MP_BP_Flight+Hotel One Pax with Lap child DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then user selects vacation dates
    Then user selects the one child and one adult passengers
    Then User Verify One room is selected by default
    Then User clicks on Search vacations button
    Then User Enters LapChild Dob
    When user lands on Select your Hotel page

  
  @EPIC3399
  Scenario: Search Widget_CP_BP_Fligth+Hotel Multi Pax with Multi Children, DOM-INT
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then user selects vacation dates
    Then user selects the two child and two adult passengers
    Then User Selects Two rooms
    Then User clicks on Search vacations button
    Then User Enters Child Dob for two children on popup
    When user lands on Select your Hotel page

  @EPIC25241
  Scenario: Search Widget_BP_ Flight+Hotel_Testing the Passengers section on the home page
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation CUN
    Then user selects vacation dates
    Then verifies plus and minus buttons for adult and children
    Then User clicks on Search vacations button
    Then User Verifies passengers required popup and close the popup

  @EPIC26627
  Scenario: Search Widget_CP_BP_Flight+Hotel Valid Promo for dollars Code
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then User Verify One room is selected by default
    Then user selects vacation dates
    Then User Selects One adult
    Then User clicks on Add a Promo Code
    Then user add a valid promo code for dollars
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC26626
  Scenario: Search Widget_CP_BP_Flight+Hotel Invalid Promo Code
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then User Verify One room is selected by default
    Then user selects vacation dates
    Then User Selects One adult
    Then User clicks on Add a Promo Code
    Then user add invalid Promo Code
    Then User clicks on Search vacations button
    Then user verifies Promation code popup
    Then user clicks Edit Coupon
    Then user reenter invalid promo code

  @EPIC26628
  Scenario: Search Widget_CP_BP_Flight+Hotel Valid Promo for Percent off Code
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then User Verify One room is selected by default
    Then user selects vacation dates
    Then User Selects One adult
    Then User clicks on Add a Promo Code
    Then user add a valid promo code for percent
    Then User clicks on Search vacations button
    When user lands on Select your Hotel page

  @EPIC23549
  Scenario: Search Widget_BP_Flight+Hotel No Hotel Available
    Given Spirit airlines application
    When User Clicks on Book link
    When User Clicks on Vacation tab
    Then User selects Flight and Hotel
    Then User selects from city for vacation FLL
    Then User selects to city for vacation LAS
    Then User Verify One room is selected by default
    Then user selects vacation dates within fortyEight hour window
    Then User Selects One adult
    Then User clicks on Search vacations button
    Then User verifies No Hotel Available popup
