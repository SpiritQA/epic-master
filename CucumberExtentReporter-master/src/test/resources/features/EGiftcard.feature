Feature: E-Gift Card scenario 

#	@EPIC23812
  #Scenario: Validate E-Gift Card confirmation page
    #Given Spirit airlines application
    #When user clicks on the EGiftCard link in the footer
    #Then User creates a Egift Card
    #Then the user lands on the gift cards page
    #Then Validate the header is left aligned
    #Then user Validate verbiage and left aligned
    #Then verify the EGift card number is greyed out
    #Then user verifies the verbiage PIN number allows only 4 chars
    #Then user verifies purchase details
    #Then user verifies the reciient name field
    #Then user verifies the recipients email field
    #Then user validates the theme
    #Then user validates the contact field
    #Then user validates the purchasesrs first name last name are greyed
    #Then user validates the purchasesrs email greyed
    #Then user user validates the print confirmation button

    
    @EPIC23804
    Scenario Outline: Purchase $100 E-Gift Card with Visa card
    Given Spirit airlines application
    When user clicks on the EGiftCard link in the footer
    Then user selects the gift card amount "<cardamount>"
    Then user validates the carousel icon
    Then user selects happy birthday option
    Then user enters recipient name in TO text box
    Then user enters sender name in FROM text box
    Then user enters Senders email
    Then user enters Recipients email
    Then user REenters Recipients email
    Then user enters special message
    Then user clicks on the eGiftCard continue button  
    Then user inputs cc infomration "<cctype>"
    #partially covered
    Examples:
    | cardamount | cctype | 
    | 100 | Visa |  
    
    @EPIC23807
    Scenario Outline: Purchase a $25 E-Gift Card with master card
    Given Spirit airlines application
    When user clicks on the EGiftCard link in the footer
    Then user selects the gift card amount "<cardamount>"
    Then user validates the carousel icon
    Then user selects thinking of you option
    Then user enters recipient name in TO text box
    Then user enters sender name in FROM text box
    Then user enters Senders email
    Then user enters Recipients email
    Then user REenters Recipients email
    Then user enters special message
    Then user clicks on the eGiftCard continue button  
    Then user inputs cc infomration "<cctype>"
    #partially covered
    Examples:
    | cardamount | cctype | 
    | 25 | Mastercard |  
    
    @EPIC23808
    Scenario Outline: Purchase a $50 E-Gift Card with AMEX card
    Given Spirit airlines application
    When user clicks on the EGiftCard link in the footer
    Then user selects the gift card amount "<cardamount>"
    Then user validates the carousel icon
    Then user selects just married option
    Then user enters recipient name in TO text box
    Then user enters sender name in FROM text box
    Then user enters Senders email
    Then user enters Recipients email
    Then user REenters Recipients email
    Then user enters special message
    Then user clicks on the eGiftCard continue button  
    Then user inputs cc infomration "<cctype>"
    #partially covered
    Examples:
    | cardamount | cctype | 
    | 50 | AMEX| 
    
	@EPIC23809
    Scenario Outline: Purchase a editable amount E-Gift Card with Discover card
    Given Spirit airlines application
    When user clicks on the EGiftCard link in the footer
    Then user selects the gift card amount "<cardamount>"
    Then user validates the carousel icon
    Then user selects just married option
    Then user enters recipient name in TO text box
    Then user enters sender name in FROM text box
    Then user enters Senders email
    Then user enters Recipients email
    Then user REenters Recipients email
    Then user enters special message
    Then user clicks on the eGiftCard continue button  
    Then user inputs cc infomration "<cctype>"
    #partially covered
    Examples:
    | cardamount | cctype | 
    | 200 | Discover|   
    