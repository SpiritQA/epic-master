Feature: plane 321 model seat test 

  @EPIC23937
  Scenario Outline: 321 plane model seating configuration for BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user attempts to select the exit row even though it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23931
  Scenario Outline: 321 plane model seating configuration for CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user selects the has car seat checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the airbus321 standard seat
    Then user attempts to select the Exit row and Big front seat for the child but it should be blocked
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23936
  Scenario Outline: 321 plane model seating configuration for DEAF
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on additional services and selects the Hearing disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23932
  Scenario Outline: 321 plane model seating configuration for ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the emotional psychiatric support animal
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23934
  Scenario Outline: 321 plane model seating configuration for INFT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the lap child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
   Then user enters the infant passneger information 
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23935
  Scenario Outline: 321 plane model seating configuration for POCS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects clicks on the additional services and select the portable oxygen concentrators
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23933
  Scenario Outline: 321 plane model seating configuration for SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the service animal check box
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23938
  Scenario Outline: 321 plane model seating configuration for UNMR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the UMNR passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC23944
  Scenario Outline: 321 plane model seating configuration for WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the battery powered dry
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  #Check In Path---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  @EPIC24074
  Scenario Outline: 321 plane model seating configuration for BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
   Then user select the today as a departure date 
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user attempts to select the exit row even though it should be disabled

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24068
  Scenario Outline: 321 plane model seating configuration for CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user selects the has car seat checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user attempts to select the exit row even though it should be disabled

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24073
  Scenario Outline: 321 plane model seating configuration for DEAF
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on additional services and selects the Hearing disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24069
  Scenario Outline: 321 plane model seating configuration for ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the emotional psychiatric support animal
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24071
  Scenario Outline: 321 plane model seating configuration for INFT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the lap child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24072
  Scenario Outline: 321 plane model seating configuration for POCS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects clicks on the additional services and select the portable oxygen concentrators
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24070
  Scenario Outline: 321 plane model seating configuration for SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the service animal check box
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24075
  Scenario Outline: 321 plane model seating configuration for UNMR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the UMNR passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24081
  Scenario Outline: 321 plane model seating configuration for WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the battery powered dry
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 1 Stop         |

  #Manage TravelScenario Outline Paths------------------------------------------------------------------------------------------------------------------------------------
  @EPIC24060
  Scenario Outline: 321 model seating configuration Manage travel path for BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional info button and selects the vision disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then user clicks on the change seats button
    When user lands on seats page
    Then user attempts to select the exit row even though it should be disabled

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24054
  Scenario Outline: 321 plane model seating configuration Manage travel path for CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the personal information of passenger two
    Then user selects the has car seat checkbox
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then user clicks on the change seats button
    When user lands on seats page
    Then user attempts to select the exit row even though it should be disabled

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24059
  Scenario Outline: 321 Plane model seating configuration Manage travel path for DEAF
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on additional services and selects the Hearing disability
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
      Then user clicks on the change seats button
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24055
  Scenario Outline: 321 Plane model seating configuration Manage travel path for ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the emotional psychiatric support animal
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
     Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24057
  Scenario Outline: 321 plane model seating configuration Manage Travel for INFT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birth date of the lap child
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
   Then user enters the infant passneger information 
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
     Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24058
  Scenario Outline: 321 plane model seating configuration Manage Travel for POCS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user selects clicks on the additional services and select the portable oxygen concentrators
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
     Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24056
  Scenario Outline: 321 plane model seating configuration Manage Travel for SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the service animal check box
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
     Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to select the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24061
  Scenario Outline: 321 plane model seating configuration for UNMR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the Child passenger UNMR
    Then user selects clicks on continue button and eneter the date of birth
    Then user clicks on the popup continue button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the UMNR passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC24067
  Scenario Outline: 321 plane model seating configuration for WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks on the additional services and selects the battery powered dry
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then user clicks on the change seats button
    When user lands on seats page
    Then user trying to selects the Exit row and big front seat but it should be disabled
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------
  @EPIC3129
  Scenario Outline: seats page with basic seating and dynamic cart option
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC3278
  Scenario Outline: Exit Row Pop Up Change Seat
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then user clicks on the exit row seat and switch to the popup and selects the change seat

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC3982
  Scenario Outline: Seats Page CP MultiPAX Basic Seat Selection With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then User clicks on regular seat
    Then User verifies the price in the seats total content block

    Examples: 
      | Required stops |
      | 2 Stop         |


  @EPIC3987
  Scenario Outline: Seats Page CP SinglePAX Seat change and partial selection_With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user checks off active duty US military personnel
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then User clicks on regular seat
    Then User verifies the price in the seats total content block
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC3995
  Scenario Outline: Seats Page CP SinglePAX BFS Seat Selection With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then user clicks on a 321BFS
    Then User verifies the price in the seats total content block
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC3992
  Scenario Outline: Seats Page CP SinglePAX BFS Seat Selection With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the two adult passengers
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on the selected flight
    Then user selects the which time to fly
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the secondary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and both passenger names
    Then User clicks on regular seat
    Then user clicks on regular seat for passenger2
    Then User verifies the price in the seats total content block
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC3990
  Scenario Outline: Seats Page CP MultiPAX BFS Seat Selection With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the two adult passengers
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the which time to fly
    Then user clicks on the continue button
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the secondary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and both passenger names
    Then User clicks on regular seat
    Then user clicks on a 321BFS
    Then user clicks on regular seat for passenger2
    Then User verifies the price in the seats total content block
    Then user clicks on the continue button on seats page

    Examples: 
      | Required stops |
      | 2 Stop         |

   

  @EPIC4499
  Scenario Outline: Seats Page CP SinglePAX Basic Exit Row Seat Selection_With Dynamic Shopping Cart Validation
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then User selects an exit row seat on plane321
    Then User verifies the price in the seats total content block
    Then user clicks on the continue button on seats page
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then user clicks on the Edit seats button
    When user lands on seats page
    Then User validates city pair and passenger names
    Then User selects an exit row seat on plane321
    Then User verifies the price in the seats total content block

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC13106
  Scenario Outline: Thrills combo continue without selecting seats
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    #Then user selects the which time to fly
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    Then user clicks on choose thrills combo
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then user selects the continue without selecting the seats

    Examples: 
      | Required stops |
      | 2 Stop         |
      
      
      @EPIC4496
      Scenario Outline: Seats Page CP MultiPAX Basic Exit Row Seat Selection With Dynamic Shopping Cart Validation
      
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then user selects the two adult passengers
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the which time to fly
    Then user clicks on the continue button
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user clicks on the continue button
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the secondary passenger information
    Then user clicks in the continue button
    When user landing on the bags page
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates city pair and both passenger names
      Examples: 
      | Required stops |
      | 2 Stop         |
    