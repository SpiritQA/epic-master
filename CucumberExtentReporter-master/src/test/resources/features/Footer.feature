Feature: Footer Test Cases

  @EPIC2924
  Scenario Outline: validate the footer column headers and links on all booking path pages
    Given Spirit airlines application
    When User Clicks on Book link
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then User clicks on onewaytrip
    Then User choose the from FLL City
    Then User choose the To SAN city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then User Clicks on the x on thrills combo pop up
    When user lands on passengers page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    When user lands on passengers page
    Then user enters the primary passenger information
    Then user enters the contact information
    Then user clicks in the continue button
    When user landing on the bags page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then user clicks on the continue without adding bags button
    When user lands on seats page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page

    Examples: 
      | Required stops |
      | 2 Stop         |

  @EPIC2925
  Scenario: Click the Free Spirit link in Get to Know us footer and be redirected to the page
    Given Spirit airlines application
    Then user clicks on the free spirits link
    When User lands on the account enrollment page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us

  @EPIC2926
  Scenario: Click the $9FC link in Get to Know us footer and be redirected to the page
    Given Spirit airlines application
    Then User Clicks on the Nine Dollar Fare Club link
    When User land on the club enrollment page
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us

  @EPIC2927
  Scenario: Click the About Spirit link in Get to Know us footer and be redirected to the page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User Clicks on the About Spirit link
    When User land on the About Us Page
    Then Validate About Us header
    Then Validate the media note message in about us
    Then Validate the Spirit Standard Product Features header
    Then Validate Spirit Optional Services and Products
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us

  @EPIC2928
  Scenario: Click the Contact us under the Talk to Us footer and be redirected to the page
    Given Spirit airlines application
    Then User clicks on the Contact Us Link
    When User lands on the Contact Us Page
    Then User Validates the contact us header
    Then User Validates the frequently asked questions link
    Then User Validates the Spirit101 link
    Then User Validates the Blue Bar Manage A reservation link
    Then User Validates the Blue Bar Check in Link
    Then User Validates the Blue Bar Get Flight Status Link
    Then User Validates the Blue Bar Call Link
    Then User Validates the international numbers link under the call link
    Then User Validates the Blue Bar Email Help Link
    Then User Validates the Most Common Questions Header
    Then User Validates What are the size and weight limits for bags
    Then User Validates How much does Spirit charge for bags
    Then User Validates How can I check in and get my boarding pass
    Then User Validates Do I have to purchase a seat assignment
    Then User Validates General travel information Header
    Then User Validates Travel Insurance Link
    Then User Validates Cancelation and refund policies link
    Then User Validates Traveling With Pets link
    Then User Validates TSA and TSA PreCheck_Link
    Then User Validates Traveling with children or unaccompanied minors link
    Then User Validates Weather issues link
    Then User Validates What we do for the military link
    Then User Validates the 9dollar Fare Club Header in Contact Us Page
    Then User Validates Miles expiration policies Link
    Then User Validates Cancel or renew 9DFC Link
    Then User Validates Use Miles, Get Miles, Buy or Gift Miles Link
    Then User Validates Missing miles from a promotion, certificate or bonus offer Link
    Then User Validates Forgot your password Link
    Then User Validates Learn about FREE SPIRIT and 9dollar Fare Club Link
    Then User Validates Information about the SPIRIT MasterCard Credit Card

  @EPIC2930
  Scenario: validate all the footer links on the legal page
    Given Spirit airlines application
    Then User Clicks on the Legal Link in the Talk To Us Section of the Footer
    Then Validate the Text under the legal Header
    Then User validates column header Get to know Us
    Then User validates the column header Talk to us
    Then User validates the column header Fly with us
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us
    Then Validate the Download Adobe Acrobat Reader for free Link
    Then User Validates the Privacy Policy Link
    Then User Validates the Contract of Carriage Link
    Then User Validates th Free Spirit Terms and conditions Link
    Then User validates the nine dollar fare club terms and conditons link
    Then User validates the general terms and conditions link
    Then User clicks the Privacy Policy link and is taken to the correct page
    Then User clicks the contract of carriage page link and is taken to the correct page
    Then User clicks Free spirit terms and conditions link and is taken to the correct page
    Then User clicks nine dollar fare club terms and conditions link and is taken to the correct page
    Then User clicks the general terms and conditions page and is taken to the correct page
    Then User goes back to the home page
    Then User clicks on the Contract of Carriage link under Talk To Us
    Then User clicks on the Guest Service Plan Link under Talk To Us
    Then User clicks on the Tarmac Dekay Plan Link under Talk To Us

  @EPIC2931
  Scenario: Validate All the Social Media Links In the Footer
    Given Spirit airlines application
    Then User Validates the connect with us header inside the footer
    Then User clicks on the Facebook Icon Validates the site and returns to the home page
    Then User Clicks on the Twitter Icon Validates the site and returns to the home page
    Then User Clicks on the Instagram Icon Validates the site and returns to the home page
    Then User Clicks on the YouTube Icon Validates the site and returns to the home page
    Then User Click on the Tumblr Icon validates the site and returns to the home page

  @EPIC2932
  Scenario: Validate the app store link and download our Mobile App Header
    Given Spirit airlines application
    Then User Validates the Download Our Mobile App Header
    Then User Clicks the App Store Icon and returns to the home page
    Then User Clicks the Google Play Store App and returns to the home page

  @EPIC2966
  Scenario: Validate the app store link and download our Mobile App Header
    Given Spirit airlines application
    Then Validate the Spirit Mastercard
    Then User Validates the fifteen thousand bonus miles offer text in footer
    Then user validates the APPLY NOW link in the connect with us section of the footer

  @EPIC21199
  Scenario: Validate the help center page and all the links
    Given Spirit airlines application
    Then User clicks the Help Center Link and validates the customer support center page
    Then User validates the footer on the customer support page
    Then User validates the Spirit Airlines Support header on the Customer Support Page
    Then User inputs text into the search field and validates that it works
    Then user Validates the customer support page most common questions Header
    Then User validates the explore all support topics header
    Then user validates the What are the size and weight limits for the bags link
    Then user validates the how much does spirit charge for bags link
    Then user validates the how can i change or cancel my reservation link
    Then user validates the how can i check in and get my boarding pass link
    Then user validates the do i have to purchase a seat assignment link
    Then user validates the customer support bags link
    Then user validates the customer support Seats link
    Then user validates the customer support Online Check In Link
    Then user validates the customer support Travel info Link
    Then user validates the customer support Packages Link
    Then user validates the customer support Special Needs Link
    Then user validates the customer support Member Clubs Link
    Then user validates the customer support Frequent Flyer Link
    Then user validates the customer support General Link

  @EPIC23606
  Scenario: Validate the Get to know us Careers page
    Given Spirit airlines application
    Then user clicks on the careers link in the get to know us section of the footer
    Then Validate the Join Our Team Header
    Then Validate The Help Us Change The Way People Fly Header
    Then user clicks and validates the find a job link
    Then Validate the history and future of spirit

  #  	 Then User validates the FIND A JOB Link in the bottom right corner (Button was broken and not there, please fix when available in the future)
  @EPIC25438
  Scenario: Validate the Investor Relations Page
    Given Spirit airlines application
    Then user clicks on the investors relations link and is taken to the page
    Then User validates the footer on the Investore Relations page

  @EPIC25439
  Scenario: Validate the where we fly page
    Given Spirit airlines application
    Then User clicks on the Where We Fly Link under the the fly with us Title in the footer
    When User lands on the route maps page
    Then User validates the Upcoming Nonstop Services header
    Then User validates the Times Table Header
    Then User validates the Seasonal Service Header

  @EPIC25448
  Scenario: Validate the Media Center Page
    Given Spirit airlines application
    Then user clicks on Media Center Link in the get to know us footer section
    When User lands on the press release page
    Then User validates the media contact header
    Then User validates the text under the media contact
    Then User validates the press releases header
    Then User validates the Media Library header
    Then User validates the text under the media library
    Then user validates Aircraft photos title
    Then user validates videos title
    Then User validates logos title
    Then User validates route map title
    Then User validates column header Get to know Us in the press release page
    Then User validates the column header Talk to us in the press release page
    Then User validates the column header Fly with us in the press relase page
    Then User validates the all the links under Get to know us
    Then User validates all the links under talk to us
    Then User validates all the links under fly with us

  @EPIC25499
  Scenario: Spirit Logo and copy right
    Given Spirit airlines application
    Then user validates the spirit logo
    Then user validates the copyright text underneath the logo

  #partially completed
  @EPIC25501
  Scenario: Validate the spirit pencil banner in the footer
    Given Spirit airlines application
    Then user validates the spirit master card logo in thr pencil banner
    Then User validates the apply now button in the pencil banner
    Then User clicks the apply now button and is taken to the correct page

  @EPIC25505
  Scenario: Validate the Egift Card link in the footer
    Given Spirit airlines application
    Then user Validates the e gift card link
    Then User clicks on the Egift card clink and is taken to the right page

  @EPIC25511
  Scenario: Validate the travel agent link in the footer
    Given Spirit airlines application
    Then User clicks on the travel agent link in the footer and validates the URL
    Then User validates the footer on the Travel Agent Portal

  @EPIC25514
  Scenario: Validate Fly With Us Deals URL and Footer
    Given Spirit airlines application
    Then User clicks on the Deals link in the footer and validates the URL
    Then User validates the footer on the deals page

  @EPIC25990
  Scenario Outline: User Books A flight within twenty-four hours and validates footer in check in page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then  user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user clicks on check in tab and inputs last name and pnr and clicks check in
    Then User Validates Reservation Page Summary Footer

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC25991
  Scenario Outline: User Books A flight Outside of twenty-four hours and validates the footer in check in page
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user enters the personal info and contact address
    Then user clicks in the continue button
    When user landing on the bags page
    Then user enters the one carry-on Bag
    Then user enters the number of checked bag
    Then user validates the bags total amount
    Then user clicks on the standard pricing continue button
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code
    Then User Validates Reservation Page Summary Footer

    Examples: 
      | Required stops |
      | 1 Stop         |
