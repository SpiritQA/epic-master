Feature: AC Seat Config 320(32A)

  @EPIC23923
  Scenario Outline: AC ThreeTwenty Verify block seats for BLND ssr [EPIC23923]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Vision disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for BLND ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23922
  Scenario Outline: AC ThreeTwenty Verify block seats for DEAF ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for DEAF ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23918
  Scenario Outline: AC ThreeTwenty Verify block seats for ESAN ssr [EPIC23918]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks ESAN
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for ESAN ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23921
  Scenario Outline: AC ThreeTwenty Verify block seats for POCS ssr [EPIC23921]
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for POCS ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23921
  Scenario Outline: AC ThreeTwenty Verify block seats for SRVA ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for SRVA ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23924
  Scenario Outline: AC ThreeTwenty Verify block seats for UNMR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for UNMR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23930
  Scenario Outline: AC ThreeTwenty Verify block seats for WCBD ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered drygel cell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCBD ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23925
  Scenario Outline: AC ThreeTwenty Verify block seats for WCBW ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCBW ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23928
  Scenario Outline: AC ThreeTwenty Verify block seats for WCHC ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    # Then User Selects Need Help ToFrom seat
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCHC ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23927
  Scenario Outline: AC ThreeTwenty Verify block seats for WCHR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom gate
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCHR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23926
  Scenario Outline: AC ThreeTwenty Verify block seats for WCHS ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects completely immobile
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCHS ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23929
  Scenario Outline: AC ThreeTwenty Verify block seats for WCMP ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects manually powered Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for WCMP ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23917
  Scenario Outline: AC ThreeTwenty Verify block seats for CRSR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
      Then user enters the infant passneger information 
    #Then user clicks on the CarSeat
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for CRSR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23920
  Scenario Outline: AC ThreeTwenty Verify block seats for INFT ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the infant passneger information 
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    Then user verifies Blocked seats on ThirtyTwoA for INFT ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  #<--------------------Manage travel path--------------->
  @EPIC23966
  Scenario Outline: MT AC ThreeTwenty Verify block seats for BLND ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Vision disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for BLND ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
    Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for BLND ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23965
  Scenario Outline: MT AC ThreeTwenty Verify block seats for DEAF ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    # Then user verifies Blocked seats on ThirtyTwoA for DEAF ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
     Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for DEAF ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23961
  Scenario Outline: MT AC ThreeTwenty Verify block seats for ESAN ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks ESAN
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for ESAN ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for ESAN ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23964
  Scenario Outline: MT AC ThreeTwenty Verify block seats for POCS ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for POCS ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for POCS ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23962
  Scenario Outline: MT AC ThreeTwenty Verify block seats for SRVA ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for SRVA ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for SRVA ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23967
  Scenario Outline: MT AC ThreeTwenty Verify block seats for UNMR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    # Then user verifies Blocked seats on ThirtyTwoA for UNMR ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for UNMR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23973
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCBD ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered drygel cell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then  user verifies Blocked seats on ThirtyTwoA for WCBD ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCBD ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23968
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCBW ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    # Then user verifies Blocked seats on ThirtyTwoA for WCBW ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCBW ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23971
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCHC ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    # Then User Selects Need Help ToFrom seat
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    # Then user verifies Blocked seats on ThirtyTwoA for WCHC ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCHC ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23970
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCHR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom gate
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for WCHR ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCHR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23969
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCHS ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects completely immobile
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    # Then user verifies Blocked seats on ThirtyTwoA for WCHS ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCHS ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23972
  Scenario Outline: MT AC ThreeTwenty Verify block seats for WCMP ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects manually powered Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for WCMP ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for WCMP ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23960
  Scenario Outline: MT AC ThreeTwenty Verify block seats for CRSR ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
   Then user enters the infant passneger information 
    Then user clicks on the CarSeat
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for CRSR ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for CRSR ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC23963
  Scenario Outline: MT AC ThreeTwenty Verify block seats for INFT ssr
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LAS
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
  Then user enters the infant passneger information 
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    Then user verifies seat map is Airbus ThirtyTwoA
    #Then user verifies Blocked seats on ThirtyTwoA for INFT ssr
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    Then user verifies Blocked seats on ThirtyTwoA for INFT ssr

    Examples: 
      | Required stops |
      | 1 Stop         |

  #<--------------------Check in path--------------->
  @EPIC24046
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path BLND
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
     Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Vision disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for BLND ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 0 Stop         |

  @EPIC24045
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path DEAF
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects Hearing disability
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for DEAF ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24041
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path ESAN
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks ESAN
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for ESAN ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24044
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path POCS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Clicks POC
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for POCS ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24042
  Scenario Outline: ThreeTwenty plane model seating configuration with two passnegers checkin path SRVA
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user Selects Service Animal
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for SRVA ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24053
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCBD
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered drygel cell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    #Then  user verifies Blocked seats on ThirtyTwoA for WCBD ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24048
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCBW
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects Battery Powered wetcell Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for WCBW ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24051
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCHC
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    # Then User Selects Need Help ToFrom seat
    Then user verifies Blocked seats on ThirtyTwoA for WCHC ssr
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for WCHC ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24050
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCHR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects Need Help ToFrom gate
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for WCHR ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24049
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCHS
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then User Selects completely immobile
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for WCHS ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24052
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path WCMP
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects number of passengers
    Then User clicks on Search button
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user Clicks Additional Services
    Then user selects I have my own wheelchair
    Then User Selects manually powered Wheelchair
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for WCMP ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24040
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path CRSR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
  Then user enters the infant passneger information 
    Then user clicks on the CarSeat
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for CRSR ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24043
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path INFT
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then user selects the one child and one adult passengers
    Then user clicks on search button and enters the birthdate of the Lap child
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as FS member
    Then user verifies Member is logged in
    Then user enters the personal information of passenger two
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for INFT ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |

  @EPIC24047
  Scenario Outline: ThreeTwenty plane model seating configuration checkin path UNMR
    Given Spirit airlines application
    When User Clicks on Book link
    Then User clicks on onewaytrip
    Then User choose the from city
    Then User choose the To city LGA
    Then user selects departures date
    Then User selects One child
    Then user clicks on search button and enters the birthdate for UMNR
    When user lands on flight page
    Then user clicks on the selected flight
    Then user selects the what time to fly "<Required stops>"
    Then user switch to the popup and clicks on close button
    When user lands on passengers page
    Then user login as UMNR FS member
    Then user verifies UMNR Member is logged in
    Then user clicks in the continue button
    When user landing on the bags page
    Then user Clicks continue without bags
    Then user Clicks on I dont need bags on pop up
    When user lands on seats page
    Then user selects the continue without selecting the seats
    When user lands on options page
    Then user clicks on checkin
    Then user clicks on the continue button on options page
    When user landing on payment page
    Then user enters the Payments information
    Then user clicks on the Booktrip and handle the travel more popup
    When user lands on confirmation page
    Then user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR
   Then user clicks on the seats link on mytrips page
    When user lands on seats page
    Then user verifies Blocked seats on ThirtyTwoA for UNMR ssr
    Then user selects the continue without selecting the seats
    Then user clicks the i dont need bags
    Then user clicks Checkin and Print Boarding Pass
    Then user clicks Nope im good to pro tip pop up
    Then user clicks Get Random Seats on choose your seats popup
    Then user clicks No to Reserve a Car
    When user lands on Extras page
    Then user clicks on the continue button on the extras page
    Then user clicks no to Travel Guard
    Then user clicks Accept and Print Boarding Pass on Hazmat
    Then user clicks Finish Check in

    Examples: 
      | Required stops |
      | 1 Stop         |
