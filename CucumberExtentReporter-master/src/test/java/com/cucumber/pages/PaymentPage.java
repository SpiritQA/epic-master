package com.cucumber.pages;

public class PaymentPage {

	public final static String credit_card_name_validation_American_Express = "//app-payment-field-input[1]//img[1]";
	public final static String credit_card_name_validation_discover = "//app-payment-field-input[1]//img[2]";
	public final static String credit_card_name_validation_Master = "//app-payment-field-input[1]//img[3]";
	public final static String credit_card_name_validation_visa = "//app-payment-field-input[1]//img[4]";
	public final static String credit_card_name_validation_UTP = "//app-payment-field-input[1]//img[5]";

	public final static String No_insurance_radio_button = "//label[@for='radio-insuranceCoverage2']";

	public final static String Saved_card_dropdown = "//select[@id='cards']";

	public final static String payments_page_terms_and_conditions = "//label[contains(text(),' I agree to terms and conditions including the HAZMAT restrictions.* ')]";

	public final static String payments_page_redeem_a_voucher = "//p[@class='font-weight-bold mb-0 action-highlight ng-star-inserted']";
	public final static String payments_page_confirmation_code = "//input[@id='confirmationCode']";
	public final static String payments_page_voucher_number = "//input[@id='voucherNumber']";
	public final static String payments_page_Egiftcard_number = "//input[@id='giftCardNumber']";
	public final static String payments_page_pinnumber = "//input[@id='pinNumber']";
	public final static String payments_page_con_go = "//div[@class='col-12 col-md-4 align-items-center']//button[@type='button'][contains(text(),'Go')]";

	public final static String Trip_summary_Add_Edit_Passport_Information = "//button[@class='btn btn-link ng-star-inserted']";
	public final static String Trip_summary_travel_advisory_content = "//li[@class='ng-star-inserted']//p[@class='ng-star-inserted']";
	public final static String Trip_summary_travel_advisory_link = "//a[@routerlink='/travel-advisory']";
	public final static String Passport_information_pagetitle = "//app-travel-document-page[@class='ng-star-inserted']//h1[contains(text(),'Passport Information')]";
	public final static String Passport_information_title = "//select[@id='title']";
	public final static String Passport_information_passportnumber = "//input[@id='passportNumber']";
	public final static String Passport_information_firstname = "//input[@name='firstName']";
	public final static String Passport_information_middlename = "//input[@id='middleName']";
	public final static String Passport_information_lastname = "//input[@name='lastName']";
	public final static String Passport_information_passportnumber_err = "//div[@class='s-error-text ng-star-inserted' and contains(text(),'Passport Number must contain')]";
	public final static String Passport_information_expirationdate = "//input[@id='changeForBeforeDepartureDate0']";
	public final static String Passport_information_expirationdate_err = "//div[@class='s-error-text ng-star-inserted']";
	public final static String Passport_expirationdate_popup_content = "//p[contains(text(),'NOTE: Some countries require your passport to be valid six months')]";
	public final static String Passport_middlename_remainder_popup_updatebtn = "//input[@id='update']";
	public final static String Passport_middlename_remainder_popup_proceed_without = "//input[contains(text(),'Proceed without')]";
	public final static String Passport_expirationdate_popup_okbtn = "//button[@class='btn btn-primary text-uppercase']";
	public final static String Passport_information_countryof_residence = "//select[@id='nationality']";
	public final static String Passport_information_issuingcountry = "//select[@id='passportIssuingCountry']";
	public final static String Passport_information_savechanges = "//button[contains(text(),' Save Changes ')]";
	public final static String Passport_information_cancelchanges = "//a[@class='btn-link-sml']";
	public final static String Passport_expired_popup_title = "//h2[@class='modal-title']";
	public final static String Passport_expired_popup_okbtn = "//div[@class='modal-body']//button[contains(text(),'ok')]";
	
	
	
	
}
