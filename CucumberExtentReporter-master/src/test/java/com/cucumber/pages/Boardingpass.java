package com.cucumber.pages;

public class Boardingpass {

	
	public final static String checkin_borading_pass_nope_im_good="//button[@class='btn btn-secondary btn-block']";
	
	public final static String checkin_boarding_pass_get_random_seats="//button[contains(text(),'Get Random Seats')]";
	
	public final static String  checkin_boarding_pass_Extras_continue_button="//button[@class='btn btn-primary' and contains(text(),'Continue')]";
	
	public final static String checkin_boarding_pass_travel_gurad_insuarance="//input[@id='radio-no']";
	
	public final static String checkin_boarding_pass_accept_and_print="//button[@class='btn btn-primary' and contains(text(),'Accept & Print Boarding Pass')]";
	
	public final static String checkin_boardingpass_popup_close="//i[@class='icon-close']";
	
	public final static String checkin_boarding_pass_popup_print_checkbox="//label[@class='ng-star-inserted custom-control-label' and @for='print']";
	
	public final static String checkin_boarding_pass_popup_email_checkbox="//label[@class='ng-star-inserted custom-control-label' and @for='sendEmail']";
	
	public final static String checkin_boarding_pass_finish_checkin="//button[@class='btn btn-primary' and contains(text(),'finish Check-In')]";
	
	public final static String checkin_boarding_pass_extras="//p[@class='h2']";
	
}
 