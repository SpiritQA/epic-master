package com.cucumber.pages;

public class BookingSummaryPage {

	
public final static String departureDate = "//div[@class='col-12 flight-desktop-container position-relative ng-star-inserted'][1]//p[@class='d-inline mb-1']";
	
	public final static String originAirport = "//div[@class='col-12 flight-desktop-container position-relative ng-star-inserted'][1]//p[@class='custom-padding-summary mb-1'][1]";
			
	public final static String destinationAirport = "//div[@class='col-12 flight-desktop-container position-relative ng-star-inserted'][2]//p[@class='custom-padding-summary mb-1'][2]";
	
	public final static String confirmationCode = "//div[@class='col-md-4 d-flex align-items-center justify-content-end']//strong[contains(text(),'Confirmation Code')]/..";
}
