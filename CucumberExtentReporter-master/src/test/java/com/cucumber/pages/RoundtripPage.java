package com.cucumber.pages;

public class RoundtripPage {
	
	public final static String Roundtrip_radio_button="//label[@for='radio-roundTrip']";
	
	public final static String click_on_from_city="//div[@class='row form-group ng-tns-c15-0 ng-star-inserted']//input[@name='originStationCode']";
	
	public final static String click_on_to_city="//div[@class='row form-group ng-tns-c15-0 ng-star-inserted']//input[@name='destinationStationCode']";
	
	public final static String select_from_city ="//app-station-picker-dropdown[1]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),'Fort Lauderdale, FL / Miami, FL AREA (FLL)')]";
	
	 public final static String Aruba_city_From_city_INT="//app-station-picker-dropdown[1]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Aruba, Aruba (AUA) ')]";
	 
	 public final static String cancun_city_To_city_INT="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Cancun, Mexico (CUN)')]";
	
	public final static String Select_To_city="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' New York, NY - LaGuardia (LGA) ')]";
	
    public final static String departure_date_and_return_date="//input[@id='dates']";
    
    public final static String clickon_passengers="//div[@class='row form-group']/descendant::div[@class='col-sm-3'][3]";
	
	
	public final static String select_adults="//tab[@_ngcontent-c7][1]//app-count-picker[@name='adultCount']//input[@_ngcontent-c18]";
	
	
	public final static String select_child="//tab[@_ngcontent-c7][1]//app-count-picker[@name='childCount']//input[@_ngcontent-c18]";
	
	public final static String Search_button = "//button[text()='Search' and @class='btn btn-primary' and @_ngcontent-c12]";
    
    
	public final static String Return_flight_select_day_cost="//section[1]/app-fare-pickers[1]/div[2]/app-fare-picker[1]//app-low-fare-day[4]/button[1]/div[2]/div[1]/div[1]";
    
	public final static String Return_select_day="//section[1]//app-fare-pickers[1]//div[2]/app-fare-picker[1]//app-low-fare[1]//app-low-fare-day[4]";
	
	public final static String Return_next_week_click="//section[1]/app-fare-pickers[1]/div[2]/app-fare-picker[1]//app-low-fare[1]//div[1]/button[5]";
	
	public final static String Return_select_day_not_available="//section[@class='ng-star-inserted']//div[2]//app-low-fare-day[4]//span[contains(text(),' Not available ')]";
	
	
	public static String Return_not_available (int i, String Return_otherday_cost) { 
		
		Return_otherday_cost = "//section[@class='ng-star-inserted']//div[2]//app-low-fare-day["+i+"]//div[contains(text(),' Not available ')]";
			return Return_otherday_cost;
			
			}

		public static String Return_cost_of_next_day (int i, String Return_otherday_cost) { 
			
			Return_otherday_cost = "//section[@class='ng-star-inserted']//div[2]//app-low-fare-day["+i+"]//div[@class='ng-star-inserted']";
			return Return_otherday_cost;
			
			}
		
		public static String Return_next_day_Return (int i, String Return_otherday) { 
			
			Return_otherday = "//section[@class='ng-star-inserted']//div[2]//app-low-fare-day["+i+"]";
			return Return_otherday;
			
			}
		
		public static String Return_next_week_day_cost_Return (int j, String Return_next_week_day_cost) { 
			
			Return_next_week_day_cost = "//section[@class='ng-star-inserted']//div[2]//app-low-fare-day["+j+"]//div[@class='ng-star-inserted']";
			return Return_next_week_day_cost;
			
			}
		
		public static String Return_next_week_day_Return (int j, String Return_next_week_day) { 
			
			Return_next_week_day = "//section[@class='ng-star-inserted']//div[2]//app-low-fare-day["+j+"]";
			return Return_next_week_day;
			
			}
		
       public final static String Return_flight_count="//section/div[2]//following::div[@class='d-flex flex-column'][2]/div";
   
   
    
}
