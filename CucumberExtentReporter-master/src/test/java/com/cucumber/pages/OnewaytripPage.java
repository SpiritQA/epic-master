package com.cucumber.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.stepDefinitions.MySharedClass;


public class OnewaytripPage {
	static WebDriver driver = MySharedClass.getDriver();
	private static final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	public final static String Book_Tab = "//section[@class='tabset']/tabset[1]/ul[@class='nav nav-tabs']/descendant::span[text()='Book']";

	public final static String oneway_Trip = "//label[@for='radio-oneWay']";

	public final static String clickon_fromcity = "//input[@name='originStationCode']";

	public final static String Denever_city = "//app-station-picker-dropdown[1]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Denver, CO (DEN) ')]";

	public final static String clickon_Tocity = "//input[@name='destinationStationCode']";

	public final static String Sandeigo_city = "//div[@class='col-sm-3 d-flex align-items-center ng-tns-c17-2 ng-star-inserted']/button[contains(text(),'San Diego, CA (SAN)')]";

	public final static String Cancun_Mexico_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Cancun, Mexico (CUN) ')]";

	public final static String Date = "//tab[@class='active tab-pane']//input[@name='date']";

	public final static String clickon_passengers = "//div[@class='row form-group']/descendant::div[@class='col-sm-3'][3]";

	public final static String CustomerLoggedIn = "//ul[@class='text-uppercase navblock3D']//li[@class='nav3D'][1]";

	public final static String select_adults = "//div[@class='ng-tns-c22-9']//div[@class='d-flex pt-1'][1]//div[contains(text(),'Adults')][1]//following::i[@class='icon-add-circle'][1]";

	public final static String select_child = "//tab[@_ngcontent-c7][1]//app-count-picker[@name='childCount']//input[@_ngcontent-c18]";

	public final static String Search_button = "//button[contains(text(),' Search Flights')]";

	public final static String International_city_field_popup_ok_button = "//button[contains(text(),' ok')]";

	public final static String Learn_more = "//a[text()='learn more']";

	public final static String New_search_button = "//button[contains(text(),'New Search')]";

	public final static String selected_day_cost = "//app-low-fare-day[3]//div[@class='ng-star-inserted']";

	public final static String selected_day = "//app-low-fare-day[4]";

	public final static String next_week_click = "//section[@class='ng-star-inserted']/div[4]//app-low-fare//button[5]";

	public final static String select_day_not_available = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[1]//span[contains(text(),' Not available ')]";
	
	public final static String select_day_soldout ="//section[@class='ng-star-inserted']/div[4][1]//span[contains(text(),' Sold out ')]";

	public static String not_available(int i, String otherday_cost) {

		otherday_cost = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + i
				+ "]//div[contains(text(),' Not available ')]";
		return otherday_cost;

	}
	
	/*public static String sold_out(int i, String otherday_cost) {

		otherday_cost = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + i
				+ "]//div[contains(text(),' Sold out ')]";
		return otherday_cost;

	}

	public static String cost_of_next_day(int i, String otherday_cost) {

		otherday_cost = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + i
				+ "]//div[@class='ng-star-inserted']";
		return otherday_cost;

	}*/

	public static String next_day_oneway(int i, String otherday) {

		otherday = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + i + "]";
		return otherday;

	}

	public static String next_week_day_cost_oneway(int j, String next_week_day_cost) {

		next_week_day_cost = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + j
				+ "]//div[@class='ng-star-inserted']";
		return next_week_day_cost;

	}

	public static String next_week_day_oneway(int j, String next_week_day) {

		next_week_day = "//section[@class='ng-star-inserted']/div[4]//app-low-fare-day[" + j + "]";
		return next_week_day;

	}
	/*
	public static boolean NotAvailableSeatSelection() {
		 boolean available_status = false;
		 boolean flag = false;
		 boolean temp = false;
		 int i = 0;
		int j = 0;
		if (!available_status) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.selected_day_cost)));
		}
		if (!available_status) {
			driver.findElement(By.xpath(OnewaytripPage.selected_day)).click();
			flag = true;
		} else {

			for (i = 5; i <= 7; i++) {
				available_status = true;
				String otherday_status = null;
				String sold_out_otherday_status = null;
				String otherday = null;
				otherday_status = OnewaytripPage.not_available(i, otherday_status);
				sold_out_otherday_status = OnewaytripPage.sold_out(i, sold_out_otherday_status);
				try {
					
					temp = (driver.findElement(By.xpath(otherday_status)).isEnabled() || driver.findElement(By.xpath(sold_out_otherday_status)).isEnabled()) ;
				} catch (NoSuchElementException e) {
					
				}
				
				if (temp) {
					available_status = false;
				}
				
				otherday = OnewaytripPage.next_day_oneway(i, otherday);
				if (!available_status) {
					driver.findElement(By.xpath(otherday)).click();
					flag = true;
					break;
				}

			}
		}

		if (flag == false) {
			driver.findElement(By.xpath(OnewaytripPage.next_week_click)).click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
			}
			for (j = 1; j <= 7; j++) {
				available_status = true;
				String next_week_day_status = null;
				String next_week_day = null;
				try {
					next_week_day_status = OnewaytripPage.not_available(i, next_week_day_status);
					driver.findElement(By.xpath(next_week_day_status)).isEnabled();
				} catch (NoSuchElementException e) {
					available_status = false;
				}

				next_week_day = OnewaytripPage.next_week_day_oneway(j, next_week_day);
				if (!available_status) {
					driver.findElement(By.xpath(next_week_day)).click();
					flag = true;
					break;
				}
			}

		}
		return flag;
	}
	
	public static boolean SoldOutSeatSelection() {
		 boolean available_status = false;
		 boolean flag = false;
		 int i = 0;
		int j = 0;
		if (!available_status) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.selected_day_cost)));
		}
		if (!available_status) {
			driver.findElement(By.xpath(OnewaytripPage.selected_day)).click();
			flag = true;
		} else {

			for (i = 5; i <= 7; i++) {
				available_status = true;
				String otherday_status = null;
				String otherday = null;
				try {
					otherday_status = OnewaytripPage.not_available(i, otherday_status);
					driver.findElement(By.xpath(otherday_status)).isEnabled();
				} catch (NoSuchElementException e) {
					available_status = false;
				}
				otherday = OnewaytripPage.next_day_oneway(i, otherday);
				if (!available_status) {
					driver.findElement(By.xpath(otherday)).click();
					flag = true;
					break;
				}

			}
		}

		if (flag == false) {
			driver.findElement(By.xpath(OnewaytripPage.next_week_click)).click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
			}
			for (j = 1; j <= 7; j++) {
				available_status = true;
				String next_week_day_status = null;
				String next_week_day = null;
				try {
					next_week_day_status = OnewaytripPage.not_available(i, next_week_day_status);
					driver.findElement(By.xpath(next_week_day_status)).isEnabled();
				} catch (NoSuchElementException e) {
					available_status = false;
				}

				next_week_day = OnewaytripPage.next_week_day_oneway(j, next_week_day);
				if (!available_status) {
					driver.findElement(By.xpath(next_week_day)).click();
					flag = true;
					break;
				}
			}

		}
		return flag;
	}*/
	
	public final static String bare_fare = "//div[contains(text(),'5/20/18')]";

	public final static String flight_time_selection = "//div[@class='d-flex flex-column justify-content-between mb-1']/app-journey[1]//button[@class='btn btn-secondary']";

	public final static String Barefare_standard = "//app-journey[1]//div[@class='card']//div[@class='card-footer d-flex justify-content-between text-center p-0']/div[2]//input[@name='fareValue']";

	public final static String Continue_Button = "//p[contains(text(),'Standard Fare')]//following::button[contains(text(),'Continue')]";

	public final static String flight_count = "//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div";

	public final static String flight_availability_button = "/html/body/app-root/main/div[2]/app-book-path/div/app-flights-page/section/div[3]/app-fare-picker/div/div/div[2]/div[5]/div[1]/app-journey/div[1]/div[2]/div[4]/div[2]/div/app-input/div/label";

	// //div[@class='d-flex flex-column justify-content-between
	// mb-1']/app-journey[2]//button[@class='btn btn-secondary']
	// passengers info
	public final static String Treat_yourself_popup = "//button[@class='close']";

	public final static String Login_button = "//button[contains(text(),' Log In')]";

	public final static String Member_email = "//input[@id='username']";

	public final static String Member_Password = "//input[@id='password']";

	public final static String login_button_to_account = "//button[text()=' Log In']";

	public final static String Title = "//select[@name='title']";

	public final static String passengers_first_name = "//input[@id='firstName']";

	public final static String passengers_middle_name = "//input[@id='middleName']";

	public final static String passengers_last_name = "//input[@id='lastName']";

	public final static String passengers_dateof_birth = "//input[@id='dateOfBirth']";

	public final static String Special_services = "//a[contains(text(),' Special services')]";

	public final static String vision_disability = "//label[contains(text(),'Vision Disability')]";

	public final static String wheelchair_services = "//input[@name='hasWheelchairToFromGate']";

	public final static String passengers_address = "//input[@id='address']";

	public final static String passengers_city = "//input[@id='city']";

	public final static String passengers_state = "//select[@name='provinceState']";

	public final static String zip_code = "//input[@id='postalCode']";

	public final static String country = "//select[@id='countryCode']";

	public final static String passengers_email = "//input[@name='contactEmailPrimary']";

	public final static String confirm_email = "//input[@name='contactEmailConfirm']";

	public final static String Passengers_phone = "//input[@id='phoneNumber']";

	public final static String passenerspage_Continue_button = "//button[contains(text(),'continue')]";

	public final static String passengerpage_validation_passneger_information_text = "//h1[contains(text(),'Passenger Information')]";

	// bags page

	public final static String carry_on_bag = "//p[contains(text(),'Carry-On Bag')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String checked_bag = "//p[text()='Checked Bag']/following::input[@class='form-control custom-form-counter'][1]";

	public final static String bags_total = "//p[contains(text(),'Bags Total')]/following::p[1]";

	public final static String standard_picing_continue_button = "//div[@class='standard-pricing-container']//button[contains(text(),'Continue')]";

	public final static String continue_withou_bags = "//button[@class='btn btn-link btn-sm d-none d-md-inline-block']";

	public final static String add_bicycle = "//p[contains(text(),'Bicycle - Bring one non-motorized bicycle')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String add_surfboard = "//p[contains(text(),'Surfboard - Put up to two borads in the same package')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_41_50lb = "//p[contains(text(),'41 - 50lbs.(18-23 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_51_70lb = "//p[contains(text(),'51 - 70lbs.(23-32 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_71_100lb = "//p[contains(text(),'71 - 100lbs.(32-45 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_60_80_linear_iches = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String special_items_over80_inches = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::input[@class='form-control custom-form-counter'][1]";

	// Seats page
	public final static String denver_lasvegas = "//div[@class='seat-row-seat standard2 assigned']";

	public final static String lasvegas_sandeigo = "//div[@class='seat-row-seat standard2 assigned']";

	public final static String seatspage_continue_button = "//button[text()='Continue']";

	public final static String continue_without_selecting_seats = "//div[@class='row purchase-seating-container d-none d-sm-block']//button[contains(text(),'Continue without selecting seats')]";

	// options page
	public final static String options_page_total = "//p[text()='Options Total']";
	public final static String checkin_decision = "//select[@name='checkInSelect']";

	public final static String continue_button = "//button[contains(text(),'Continue')]";
	// Payment page

	public final static String Paymets_page = "//h1[contains(text(),'Payment')]";

	public final static String name_on_card = "//input[@id='accountHolderName']";

	public final static String card_number = "//input[@id='cardNumber']";

	public final static String Experation_date = "//input[@id='expMonthYear']";

	public final static String security_code = "//input[@id='securityCode']";
	public final static String same_billing_adress = "//input[@name='useSameAddress']";
	public final static String Terms_check = "//label[@for='termsCheck']";

	// billing address

	public final static String Billing_adress = "//input[@id='billingAddress']";

	public final static String billing_city = "//input[@id='billingCity']";

	public final static String Billing_state = "//select[@id='billingState']";

	public final static String billing_zip = "//input[@name='billingZipPostal']";

	public final static String billing_country = "//select[@name='billingCountry']";

	public final static String Book_trip = "//div[@class='col-12 d-flex justify-content-center']/button";

	public final static String Travel_more_popup = "//label[@for='radio-travel-guard-no']";

	public final static String fs_member_logout = "//ul[@id='aria-dropdown-split']//a[contains(text(),'Sign Out')]";
	// print conformation
	public final static String iframe_switch = "//img[@class='roktAppsBanner']";
	public final static String Number_of_frames = "//div[@id='ux_widget_inner']";

	public final static String iframe_popup = "//a[@aria-label='close']";

	public final static String iframe_text = "//p[@class='scope directive']";

	public final static String iframe_Nothanks_button = "//button[contains(text(),'No thanks')]";

	public final static String iframe_continue_button = "//button[contains(text(),'Continue')]";

	public final static String iframe_close_button = "//button[text()='Close']";

	public final static String conformation_page = "//h3[text()='Your Trip Confirmation']";

	public final static String print_conformation = "//button[@id='print-button']";

	public final static String Infant_acceptance_popup = "//button[contains(text(),'Close')]";

	public final static String lap_child_require_seat = "//label[@for='child1LapOption']";

	public final static String UMNR_popup_close_button = "//button[@class='btn btn-primary' and contains(text(),'close')]";

	public final static String BookPage_Miles_Tab = "//label[@class='btn btn-secondary-pil home-pill-btn']";

	public final static String add_promocode_Homepage = "//a[contains(text(),' Add a Promo Code')]";

	public final static String promo_code_input_homepage = "//input[@id='promoCode']";

	public final static String promo_code_popup_close_button = "//button[@class='close']";

	public final static String promo_code_popup_edit_coupon_code = "//button[contains(text(),'edit coupon code')]";

	public final static String promo_code_popup_continue_without_coupon_code = "//button[contains(text(),'continue without coupon code')]";

	public final static String mouse_over_popup_text = "//div[@class='popover-content popover-body']";

	public final static String promo_code_popup_text = "//div[@class='modal-body']//ul";

	public final static String UMNR_Popup_Accept_button = "//div[@class='modal-content']//button[contains(text(),'accept')]";
	public final static String Choose_thrills_combo = "//button[contains(text(),'Choose Thrills Combo')]";

	public final static String Three_free_military_bags = "//label[contains(text(),'3 Free Military Bags')]";
	public final static String Continue_military_bags = "//label[contains(text(),'Continue')]";

	public final static String Military_bags_popup_x = "//button[@class='close']";

	public final static String Thrills_combo_savings = "//label[contains(text(),'Thirills Combo Savings')]";

	public final static String Your_selected_departure_txt = "//div[@class='d-none d-lg-block ng-trigger ng-trigger-accordionAnimate']//div[@class='h3'][contains(text(),'Your Selected Departure')]";

	public final static String Selected_departure_flight1_headertxt = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-6'][1]";

	public final static String Selected_departure_date = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-6'][1]//div[@class='color-gunmetal']";

	public final static String Selected_departure_deptime = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-5 d-flex justify-content-center align-items-center'][1]//div[@class='h4 mb-0 flex-grow-1 flex-basis-0 text-right']";

	public final static String Selected_departure_arrtime = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-5 d-flex justify-content-center align-items-center'][1]//div[@class='h4 mb-0 flex-grow-1 flex-basis-0']";

	public final static String Selected_departure_stops = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[@class='p-3 mb-3 border-1-charcoal']//div[@class='col-2 d-flex align-items-center'][1]";

	public final static String Selected_departure_price = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[@class='p-3 mb-3 border-1-charcoal']//div[@class='col-3 d-flex justify-content-end align-items-center'][1]";

	public final static String Selected_departure_Plane_icon = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-5 d-flex justify-content-center align-items-center'][1]//div[@class='flex-grow-1 flex-basis-30 d-flex flex-column justify-content-center align-items-center fg-color-gray']";

	public final static String Selected_departure_border = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//div[@class='p-3 mb-3 border-1-charcoal']";

	public final static String Selected_departure_flight1_editlink = "//button[contains(text(),'Change Departing')]";

	public final static String Selected_departure_flight2_editlink = "//button[contains(text(),'Change Returning')]";

	public final static String Selected_departure_flight2_headertxt = "//div[@class='container']//div[@class='router-outlet']//section[@class='ng-star-inserted']//div[5][@class='mb-3 ng-star-inserted'][1]//following::div[@class='col-6'][2]";

	public final static String Continue_with_bare_fare = "//button[contains(text(),'Continue With Bare Fare')]";

	public final static String Choose_this_bundle = "//div[@class='row thrills-upsell-modal']//button[contains(text(),'Choose This Bundle')]";

	public final static String Choose_your_seat_txt = "//div[@class='col-12']//h1[contains(text(),'Choose Your Seat')]";

	public final static String first_standard_flight = "//div[@class='d-flex flex-column justify-content-center align-items-center ng-star-inserted']//label[@class='custom-control-label bold-label ng-star-inserted'][contains(text(),'$')]";

	public final static String std_fare_continue = "//button[contains(text(),'Continue')]";

	public final static String Term_check_error = "//div[@class='s-error-text ng-star-inserted' and contains(text(),'I agree')]";

	public final static String options_page_Header = "//h1";

	public final static String FlightPage_header = "//div[@class='col-12']//h1";

	public final static String Continue_Button_ninedfc = "//div[@class='fare-club-container']//button[contains(text(),'Continue')]";

	public final static String flights_9dfcContinue_button = "//div[@class='col-lg-12 ng-star-inserted']//button[contains(text(),'Continue')]";

	public final static String Umnr_popup_connecting_flight_content = "//div[@class='modal-content']//p[@class='font-weight-bold']";

	public final static String Confirmation_Page_Thrifty_Car_Rental_Link = "//a//img[@src='https://cms10dss.spirit.com/Shared/en-us/Confirmation/718015%20Thrifty%20Spirit%20Confirmation%20Page%20Banner%20%282%29.jpg']";

	// Manage Travel Path

	public final static String Your_Trip_Summary_Header = "//h1[@class='h1']";

	public final static String Contact_Customer_Name_Reservation_Summary = "//p[@class='mb-0 d-inline']";

	public final static String Contact_Customer_Email_Reservation_Summary = "//p[@class='mb-0'][contains(text(),'jack.flyer9dfc@spirit.com')]";

	public final static String Contact_Customer_Phone_Number_Reservation_Summary = "//p[contains(text(),'1230000000')]";

	public final static String Contact_Info_EDIT_Link = "//div[@class='col-12 col-md-9 col-xl-6']//button[@type='button'][contains(text(),'Edit')]";

	// Contact Info My Trips Passenger Info Page

	public final static String First_Name_field = "//input[@id='firstName']";

	public final static String Last_Name_field = "//input[@id='lastName']";

	public final static String Address_field = "//input[@id='address']";

	public final static String City_field = "//input[@id='city']";

	public final static String State_field = "//select[@name='provinceState']";

	public final static String ZipCode_field = "//input[@id='postalCode']";

	public final static String Country_field = "//select[@id='countryCode']";

	public final static String Email_field = "//input[@id='contactEmailPrimary']";

	public final static String ConfirmEmail_field = "//input[@id='contactEmailConfirm']";

	public final static String PhoneCountryCode_field = "//select[@id='phoneCountryType']";

	public final static String Phone_field = "//input[@id='phoneNumber']";

	public final static String Cancel_Changes_Button = "//button[@class='btn btn-link']";

	public final static String Save_Changes_Button = "//button[@type='submit']";

	public final static String Title_Required = "//div[contains(text(),'Title is required')]";

	public final static String First_Name_Required = "//div[contains(text(),'First Name is required')]";

	public final static String Last_Name_Required = "//div[contains(text(),'Last Name is required')]";

	public final static String Date_Of_Birth_is_Required = "//div[contains(text(),' Date of Birth is required')]";

	public final static String Address_Required = "//div[contains(text(),' Address (Number and Street, Optional Apartment/Unit/P.O. box/etc.) is required')]";

	public final static String City_Required = "//div[@class='s-error-text ng-star-inserted']";

	public final static String State_Required = "//div[contains(text(),'State is required')]";

	public final static String Zip_Code_Required = "//div[contains(text(),'Zip / Postal Code is required')]";

	public final static String Email_Required = "//section[@class='mb-2 ng-dirty ng-touched ng-invalid']//div[5]//div[1]//app-input[1]//div[contains(text(),'Email is required')]";

	public final static String Email_Confirmation_Code_Required = "//div[contains(text(),'Confirm Email is required')]";

	public final static String Confirm_Email_email_address_is_invalid = "//div[@class='row']//div[2]//app-input[1]//div[1]//div[1]//div[1]";

	public final static String Invalid_Name_Characters_Entered = "//div[contains(text(),' Invalid name characters entered. ')]";

	public final static String Incorrect_Date_Format = "//div[contains(text(),' Incorrect date format.')]";

	public final static String Your_Redress_Number_Contains_Invalid = "//div[contains(text(),' Your Redress Number contains invalid characters')]";

	// New Bag Prices And Optional Services
	// -------------------------------------------------------------------------------------------------------------------------------------------------------------
	public final static String New_Bag_Prices_And_Optional_Services = "//app-home-links[@class='ng-star-inserted']//div[@class='row']//a[@class='btn btn-link text-left']";

	public final static String Options_and_Extras_Page_header = "//div[@class='container']//div[@class='h2']";

	public final static String Options_and_Extras_Page_header_verbiage = "//p[contains(text(),'We believe in only paying for what you use, not wh')]";

	public final static String Options_and_Extras_Page_Bags_Link = "//li[@class='nav-item col-12 col-md-3 active']";

	public final static String Options_and_Extras_Page_Seats_Link = "//strong[contains(text(),'Seats')]";

	public final static String Options_and_Extras_Page_Memberships_Link = "//strong[contains(text(),'Memberships')]";

	public final static String Options_and_Extras_Page_Other_Link = "//strong[contains(text(),'Other')]";
	public final static String Verbiage_under_Other_Tab = "//p[contains(text(),'We have plenty of additional extras and services l')]";

	// Sections Under Other
	public final static String Boarding_pass_printed_at_home = "//div[contains(text(),'Boarding pass printed at home')]";
	public static final String Boarding_pass_printed_at_home_tooltip = "//div[@class='card-body']//div[1]//div[1]//i[1]";
	public static final String Boarding_pass_printed_at_home_tooltip_Popup = "//div[contains(text(),'Print your boarding pass at home to save.')]";
	public static final String Boarding_pass_printed_at_home_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String Free = "//div[1]/div[2]/div[1][contains(text(),'Free')]";

	public final static String Boarding_pass_printed_by_Airport_Kiosk = "//div[contains(text(),'Boarding pass printed by Airport Kiosk')]";
	public final static String Boarding_pass_printed_by_Airport_Kiosk_tooltip = "//div[@class='card-body']//div[2]//div[1]//i[1]";
	public final static String Boarding_pass_printed_by_Airport_Kiosk_tooltip_Popup = "//div[contains(text(),'Print your boarding pass at home to save.')]";
	public final static String Boarding_pass_printed_by_Airport_Kiosk_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $2_per_boarding_pass_printed = "//div[contains(text(),'$2 per boarding pass printed')]";

	public final static String Boarding_pass_printed_at_Airport_Agent = "//div[contains(text(),'Boarding pass printed at Airport Agent')]";
	public final static String Boarding_pass_printed_at_Airport_Agent_tooltip = "//div[@class='card-body']//div[3]//div[1]//i[1]";
	public final static String Boarding_pass_printed_at_Airport_Agent_tooltip_Popup = "//div[contains(text(),'Print your boarding pass at home to save.')]";
	public final static String Boarding_pass_printed_at_Airport_Agenttooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $10_per_boarding_pass_printed = "//div[contains(text(),'$10 per boarding pass printed')]";

	public final static String Unaccompanied_Minors_Price_includes_snack_and_beverage = "//div[contains(text(),'Unaccompanied Minors (Price includes snack and bev')]";
	public final static String Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip = "//div[@class='card-body']//div[4]//div[1]//i[1]";
	public final static String Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup = "//popover-container[1]/div[2]/div[1]/div[1]/div[1]/div[1]";
	public final static String Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $100_per_customer_each_way = "//div[contains(text(),'$100 per customer, each way')]";

	public final static String Infant_Lap_Child = "//div[contains(text(),'Infant (Lap Child)')]";
	public final static String Infant_Lap_Child_tooltip = "//div[@class='card-body']//div[5]//div[1]//i[1]";
	public final static String Infant_Lap_Child_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Infant_Lap_Child_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String Free_taxes_may_apply_for_certain_countries = "//div[contains(text(),'Free (taxes may apply for certain countries)')]";

	public final static String Pet_Transportation_Limit_4_pets_total_in_cabin = "//div[contains(text(),'Pet Transportation (Limit 4 pets total in cabin)')]";
	public final static String Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip = "//div[@class='card-body']//div[6]//div[1]//i[1]";
	public final static String Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $110_per_pet_container_each_way = "//div[contains(text(),'$110 per pet container, each way')]";

	public final static String Travel_Guard_Insurance_Domestic_Itinerary = "//div[contains(text(),'Travel Guard Insurance / Domestic Itinerary')]";
	public final static String Travel_Guard_Insurance_Domestic_Itinerary_tooltip = "//div[@class='card-body']//div[7]//div[1]//i[1]";
	public final static String Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup_Travel_Gaurd_Link = "//a[contains(text(),'Travel Guard')]";
	public final static String From_$14 = "//div[contains(text(),'From $14')]";

	public final static String Travel_Guard_Insurance_International_Itinerary = "//div[contains(text(),'Travel Guard Insurance / International Itinerary')]";
	public final static String Travel_Guard_Insurance_International_Itinerary_tooltip = "//div[@class='card-body']//div[8]//div[1]//i[1]";
	public final static String Travel_Guard_Insurance_International_Itinerary_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Travel_Guard_Insurance_International_Itinerary_tooltip_Popup_Travel_Gaurd_Link = "//a[contains(text(),'Travel Guard')]";
	public final static String From_$25 = "//div[contains(text(),'From $25')]";

	public final static String Travel_Guard_Insurance_Vacation_Package_Itinerary = "//div[contains(text(),'Travel Guard Insurance / Vacation Package Itinerar')]";
	public final static String Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip = "//div[@class='card-body']//div[9]//div[1]//i[1]";
	public final static String Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup_Travel_Gaurd = "//a[contains(text(),'Travel Guard')]";
	public final static String From_$28 = "//div[contains(text(),'From $28')]";

	// On board snacks and drinks
	public final static String Onboard_Snacks_and_Drinks = "//strong[contains(text(),'Onboard Snacks and Drinks')]";
	public final static String Onboard_Snacks_and_Drinks_Carrot = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/a[1]/app-chevron[1]/i[1]";

	public final static String Snacks = "//p[contains(text(),'Snacks')]";
	public final static String Snacks_tooltip = "//div[@class='card-body collapse in show']//div[2]//div[1]//i[1]";
	public final static String Snacks_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Snacks_tooltip_Popup_More_Ifo_link = "//a[contains(text(),'More Info')]";
	public final static String $1_to_$10 = "//div[contains(text(),'$1 to $10')]";

	public final static String Drinks = "//p[contains(text(),'Drinks')]";
	public final static String Drinks_tooltip = "//div[@class='card-body collapse in show']//div[3]//div[1]//i[1]";
	public final static String Drinks_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Drinks_tooltip_Popup_More_Info_link = "//a[contains(text(),'More Info')]";
	public final static String $1_to_$15 = "//div[contains(text(),'$1 to $15')]";

	// Booking Related
	public final static String Booking_Related = "//strong[contains(text(),'Booking Related')]";
	public static final String Booking_Related_Carrot = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[2]/a[1]/app-chevron[1]/i[1]";
	public final static String Per_Customer = "//div[@class='card-body collapse in show']//em[contains(text(),'Per Customer')]";

	public final static String Reservation_Center_Booking_including_packages = "//p[contains(text(),'Reservation Center Booking (including packages)')]";
	public final static String Reservation_Center_Booking_including_packages_tooltip = "//div[@class='card-body collapse in show']//div[2]//div[1]//i[1]";
	public final static String Reservation_Center_Booking_including_packages_tooltip_Popup = "//div[contains(text(),'This applies to bookings made via our reservation ')]";
	public final static String Reservation_Center_Booking_including_packages_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $35_per_booking = "//div[contains(text(),'$35 per booking')]";

	public final static String Group_Booking = "//div[@class='card-body collapse in show']//p[@class='d-inline'][contains(text(),'Group Booking')]";
	public final static String Group_Booking_tooltip = "//div[@class='card-body collapse in show']//div[3]//div[1]//i[1]";
	public final static String Group_Booking_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Group_Booking_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String Group_Booking_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $5_per_booking = "//div[contains(text(),'$5 per booking')]";

	public final static String Colombia_Administrative_Charge = "//p[contains(text(),'Colombia Administrative Charge')]";
	public final static String Colombia_Administrative_Charge_tooltip = "//div[@class='card-body collapse in show']//div[4]//div[1]//i[1]";
	public final static String Colombia_Administrative_Charge_tooltip_Popup = "//div[contains(text(),'This applies to bookings that are sold in or depar')]";
	public final static String Colombia_Administrative_Charge_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $15_to_$95_per_booking = "//div[contains(text(),'$15 to $95 per booking')]";

	public final static String Standby_for_Earlier_Flight = "//p[contains(text(),'Standby for Earlier Flight')]";
	public final static String Standby_for_Earlier_Flight_tooltip = "//div[@class='card-body collapse in show']//div[5]//div[1]//i[1]";
	public final static String Standby_for_Earlier_Flight_tooltip_Popup = "//div[contains(text(),'This applies to customers who arrive to the airpor')]";
	public final static String Standby_for_Earlier_Flight_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $99_each_way = "//div[contains(text(),'$99 each way')]";

	public final static String Passenger_Usage_Charge = "//p[contains(text(),'Passenger Usage Charge')]";
	public final static String Passenger_Usage_Charge_tooltip = "//div[@class='card-body collapse in show']//div[6]//div[1]//i[1]";
	public final static String Passenger_Usage_Charge_tooltip_Popup = "//div[contains(text(),'This applies to bookings created online or via res')]";
	public final static String Passenger_Usage_Charge_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $899_to_$1999_per_segment = "//div[contains(text(),'$8.99 to $19.99 per segment')]";

	public final static String Regulatory_Compliance_Charge = "//p[contains(text(),'Regulatory Compliance Charge')]";
	public final static String Regulatory_Compliance_Charge_tooltip = "//div[@class='card-body collapse in show']//div[7]//div[1]//i[1]";
	public final static String Regulatory_Compliance_Charge_tooltip_Popup = "//div[contains(text(),'This charge is to recover costs incurred due to De')]";
	public final static String Regulatory_Compliance_Charge_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $7_per_segment = "//div[contains(text(),'$7 per segment')]";

	public final static String Fuel_Charge = "//p[contains(text(),'Fuel Charge')]";
	public final static String Fuel_Charge_tooltip = "//div[@class='card-body collapse in show']//div[8]//div[1]//i[1]";
	public final static String Fuel_Charge_tooltip_Popup = "//div[contains(text(),'This applies to bookings created online or via res')]";
	public final static String Fuel_Charge_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $10_per_segment = "//div[contains(text(),'$10 per segment')]";

	// Free Spirit Award
	public final static String FREE_SPIRIT_Award_Booking = "//strong[contains(text(),'FREE SPIRIT® Award Booking')]";
	public final static String FREE_SPIRIT_Award_Booking_Carrot = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[1]/div[2]/a[1]/app-chevron[1]/i[1]";
	public final static String Per_Customer_Per_Booking = "//div[@class='card-body collapse in show']//em[contains(text(),'Per Customer, Per Booking')]";

	public final static String Agent_Transaction_Reservation_Center = "//p[contains(text(),'Agent Transaction - Reservation Center')]";
	public final static String Agent_Transaction_Reservation_Center_tooltip = "//app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[2]/div[2]/div[1]/i[@class='icon-info-circle']";
	public final static String Agent_Transaction_Reservation_Center_tooltip_Popup = "//div[contains(text(),'This applies to reservation center transactions fo')]";
	public final static String Agent_Transaction_Reservation_Center_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $35 = "//app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[2]/div[2]/div[2]/div[1][contains(text(),'$35')]";

	public final static String Award_Redemption = "//p[contains(text(),'Award Redemption')]";
	public final static String Award_Redemption_tooltip = "//app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[2]/div[3]/div[1]/i[@class='icon-info-circle']";
	public final static String Award_Redemption_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Award_Redemption_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String Free_to_$100 = "//div[contains(text(),'Free to $100')]";

	public final static String Award_Modification = "//p[contains(text(),'Award Modification')]";
	public final static String Award_Modification_tooltip = "//div[@class='card-body collapse in show']//div[4]//div[1]//i[1]";
	public final static String Award_Modification_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Award_Modification_tooltip_Popup_link = "//a[contains(text(),'More Info')]";
	public final static String Award_$110 = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[2]/div[4]/div[2]/div[1][contains(text(),'$110')]";

	public final static String Mileage_Redeposit = "//p[contains(text(),'Mileage Redeposit')]";
	public final static String Mileage_Redeposit_tooltip = "//div[@class='card-body collapse in show']//div[5]//div[1]//i[1]";
	public final static String Mileage_Redeposit_tooltip_Popup = "//div[contains(text(),'This applies to all award booking cancellations an')]";
	public final static String Mileage_Redeposit_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String Mileage_$110 = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[5]/div[1]/div[2]/div[5]/div[2]/div[contains(text(),'$110')]";

	// Modification or Cancelation
	public final static String Modification_Or_Cancellation = "//strong[contains(text(),'Modification Or Cancellation')]";
	public final static String Modification_Or_Cancellation_Carrot = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[6]/div[1]/div[1]/div[2]/a[1]/app-chevron[1]/i[1]";
	public final static String Modification_Per_Customer_Per_Booking = "//app-optional-services-page[1]/div[1]/div[2]/div[6]/div[1]/div[2]/div[1]/div[2]/em[contains(text(),'Per Customer, Per Booking')]";

	public final static String Web_Modification_or_Cancellation = "//p[contains(text(),'Web Modification or Cancellation')]";
	public final static String Web_Modification_or_Cancellation_tooltip = "//app-optional-services-page[1]/div[1]/div[2]/div[6]/div[1]/div[2]/div[2]/div[1]/i[@class='icon-info-circle']";
	public final static String Web_Modification_or_Cancellation_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Web_Modification_or_Cancellation_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $90 = "//div[contains(text(),'$90')]";

	public final static String Reservations_Airport_Modification_or_Cancellation = "//p[contains(text(),'Reservations / Airport Modification or Cancellatio')]";
	public final static String Reservations_Airport_Modification_or_Cancellation_tooltip = "//div[@class='card-body collapse in show']//div[3]//div[1]//i[1]";
	public final static String Reservations_Airport_Modification_or_Cancellation_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Reservations_Airport_Modification_or_Cancellation_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $100 = "//app-optional-services-page[1]/div[1]/div[2]/div[6]/div[1]/div[2]/div[3]/div[2]/div[contains(text(),'$100')]";

	public final static String Group_Booking_Itinerary_Modification_or_Cancellation = "//p[contains(text(),'Group Booking Itinerary Modification or Cancellati')]";
	public final static String Group_Booking_Itinerary_Modification_or_Cancellation_tooltip = "//div[@class='card-body collapse in show']//div[4]//div[1]//i[1]";
	public final static String Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup = "//div[@class='col-10']//div";
	public final static String Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup_more_info_link = "//a[contains(text(),'More Info')]";
	public final static String $50 = "//div[contains(text(),'$50')]";

	// Extras
	public final static String Extras = "//strong[contains(text(),'Extras')]";
	public final static String Extras_Carrot = "/html[1]/body[1]/app-root[1]/main[1]/div[2]/app-optional-services-page[1]/div[1]/div[2]/div[7]/div[1]/div[1]/div[2]/a[1]/app-chevron[1]/i[1]";
	public final static String Extras_Per_Customer = "//app-optional-services-page[1]/div[1]/div[2]/div[7]/div[1]/div[2]/div[1]/div[2]/em[contains(text(),'Per Customer')]";

	public final static String Flight_Flex = "//p[contains(text(),'Flight Flex')]";
	public final static String Flight_Flex_tooltip = "//div[@class='card-body collapse in show']//div[2]//div[1]//i[1]";
	public final static String Flight_Flex_tooltip_Popup = "//div[contains(text(),'Modify your itinerary once for free, online, up to')]";
	public final static String Flight_Flex_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $35_to_$45 = "//div[contains(text(),'$35 to $45')]";

	public final static String Shortcut_Boarding = "//p[contains(text(),'Shortcut Boarding')]";
	public final static String Shortcut_Boarding_tooltip = "//div[@class='card-body collapse in show']//div[3]//div[1]//i[1]";
	public final static String Shortcut_Boarding_tooltip_Popup = "//div[contains(text(),'Zone 2 Priority Boarding')]";
	public final static String Shortcut_Boarding_tooltip_Popup_X = "//a[@class='icon-close popover-circle']";
	public final static String $599_each_way = "//div[contains(text(),'$5.99+ each way')]";

	public final static String Shortcut_Security = "//p[contains(text(),'Shortcut Security')]";
	public final static String Up_to_$15 = "//div[contains(text(),'Up to $15')]";

	public final static String Verbiage_Under_Seats_Tab = "//p[contains(text(),'Every customer has the opportunity to select their')]";

	// Home Page Links
	public final static String Learn_More_Link = "//div[contains(text(),'Learn More')]";

	// Spirit101 Page
	public final static String Spirit101_Page_Header = "//h1[contains(text(),'Spirit gives you More Go for less money')]";

	public final static String SEE_HOW_YOUR_BAG_SIZES_UP = "//div[@class='col-4 ng-star-inserted']//strong//a[@href='https://www.spirit.com/OptionalServices'][contains(text(),'SEE HOW YOUR BAG SIZES UP')]";

	public final static String CHECK_OUT_THESE_PRICES = "//div[@class='col-4 ng-star-inserted'][2]//following::p[contains(text(),'Pay for your bags when you book')]//following::a[contains(text(),'CHECK OUT THESE PRICES')][1]";

	public final static String SEE_PRINTING_DETAILS = "//div[@class='col-4 ng-star-inserted']//p[contains(text(),'Check')]//following::a[contains(text(),'SEE PRINTING DETAILS')]";

	public final static String GET_CHECKIN_APP = "//div[@class='col-4 ng-star-inserted']//p[contains(text(),'Check')]//following::a[contains(text(),'GET CHECK-IN APP')]";

	public final static String Get_More_Go_In_Your_Inbox_Header = "//p[contains(text(),'Get More Go in your inbox')]";
	public final static String Get_More_Go_In_Your_Inbox_Email = "//img[@src='https://www.spirit.com/Spirit101/assets/images/symbols/emails.svg']";
	public final static String Get_More_Go_In_Your_Inbox_Facebook = "//img[@src='https://www.spirit.com/Spirit101/assets/images/symbols/facebook.svg']";
	public final static String Get_More_Go_In_Your_Inbox_Twitter = "//img[@src='https://www.spirit.com/Spirit101/assets/images/symbols/twitter.svg']";
	public final static String Get_More_Go_In_Your_Inbox_Instagram = "//img[@src='https://www.spirit.com/Spirit101/assets/images/symbols/instagram.svg']";

	public final static String BECOME_A_FREE_SPIRIT_MEMBER = "//a[contains(text(),'BECOME A FREE SPIRIT® MEMBER.')]";

	public final static String GET_MORE_GO = "//a[contains(text(),'GET MORE GO')]";

	public final static String SEE_FREQUENTLY_ASKED_QUESTIONS = "//a[contains(text(),'SEE FREQUENTLY ASKED QUESTIONS')]";

	public final static String SEE_OUR_LATEST_DEALS = "//a[contains(text(),'SEE OUR LATEST DEALS')]";

	public final static String SHARE_YOUR_MORE_GO_STORY = "//a[contains(text(),'SHARE YOUR \"MORE GO STORY\"')]";

	public final static String SEE_WHERE_WE_FLY = "//a[contains(text(),'SEE WHERE WE FLY')]";
	
public final static String Passenger_Contact_First_Name ="//div[@class='col-sm-4']//input[@name='firstName']";
    
    public final static String Passenger_Contact_Last_Name ="//div[@class='col-sm-4']//input[@name='lastName']";
}