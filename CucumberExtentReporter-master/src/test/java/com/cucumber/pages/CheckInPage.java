package com.cucumber.pages;

public class CheckInPage {
    
	
	public final static String Checkin_link ="//section[@class='tabset']/tabset[1]/ul[@class='nav nav-tabs']/descendant::span[text()='Check In']";
	
	public  final static String passengers_lastname ="//tab[@class='active tab-pane']//input[@id='lastName']";
	
    public final static String confirmation_code ="//tab[@class='active tab-pane']//input[@id='recordLocator']";
	
	public final static String checkin_button ="//button[contains(text(),' Check-In ')]";
	
	public final static String check_in_Tab_BookPage="//ul[@class='nav nav-tabs']/descendant::span[text()='Check-In']";
	
	public final static String Where_to_find_your_confirmation_code_link="//a[contains(text(),'Where to find your Confirmation Code .')]";
	
	public final static String confirmation_code_popup_spirit_link="//a[@href='javascript:void(0)'][contains(text(),'Spirit')]";
	public final static String confirmation_code_popup_Expedia_link="//a[contains(text(),'Expedia')]";
	
	public final static String confirmation_code_popup_Priceline_link="//a[contains(text(),'Priceline')]";
	
	public final static String confirmation_code_popup_Cheapoair_link="//a[contains(text(),'Cheapoair')]";
	
	public final static String confirmation_code_popup_orbitz_link="//a[contains(text(),'Orbitz')]";
	
	public final static String  confirmation_code_close_button="//button[contains(text(),'CLOSE WINDOW')]";
	
	public final static String confirmation_code_popup_spirit_link_text_validation="//p[contains(text(),\"You'll find your Confirmation Code clearly highlig\")]";
	public final static String confirmation_code_popup_Expedia_link_text_validation="//p[contains(text(),\"On Expedia's confirmation email, you'll find your \")]";
	
	public final static String confirmation_code_popup_Priceline_link_text_validation="//p[contains(text(),\"On Priceline's confirmation email, you'll find you\")]";
	
	public final static String confirmation_code_popup_Cheapoair_link_text_validation="//p[contains(text(),\"On CheapOair's confirmation email, you'll find you\")]";
	
	public final static String confirmation_code_popup_orbitz_link_text_validation="//p[contains(text(),\"On Orbitz's confirmation email, you'll find your 6\")]";
	
	
    public final static String BagsPopUp_ProTip ="//div[@class='modal-content']//div[contains(text(),' Save more when you buy Carry-On or Checked bags online. ')]";
	
	public final static String SeatsPopUp_X_Button ="//div[@class='modal-content']//i[@class='fas fa-times']";
	
	public final static String BagsPopUp_Yes_Button ="//div[@class='modal-content']//button[text()='Yes']";
	
	public final static String BagsPageTitle ="//h1[contains(text(),' Add Bags Now And Save')]";
	
	public final static String StandardPricing_button ="//div[@class='standard-pricing-container']//button[contains(text(),' Continue')]";
	
	public final static String SeatsPopUp ="//div[@class='modal-content']//h2[contains(text(),'Choose Your Seats')]";
	
	public final static String SeatsPopUp_Select_Seats_button ="//div[@class='modal-content']//button[contains(text(),'Select Seats')]";
	
	public final static String ExtrasPage_title ="//div[@class='col-12']//p[contains(text(),'Extras')]";
	
	public final static String ShortcutSecurity_Add_Button ="//div[@class='row d-flex align-items-stretch'][1]//div[@class='col-12 mb-4 ng-star-inserted'][1]//a[@class='text-uppercase ng-star-inserted']";
	
	public final static String ShortcutSecurity_CheckBox ="//input[@class='ng-valid ng-dirty ng-touched']";
	
	public final static String Add_Shortcut_Security_Button ="//button[contains(text(),'Add Shortcut Security')]";
	
	public final static String TravelGuard_Popup ="//div[@class='modal-header']//h2[contains(text(),'Travel Guard')] ";
	
	public final static String TravelGuard_Popup_no_button ="//div[@class='modal-content']//div[@class='form-check']//label[@for='radio-no']";
	
	public final static String TravelGuard_Popup_yes_button ="//div[@class='modal-content']//div[@class='form-check']//label[@for='radio-yes']";
	
	public final static String Hazmat_Header_Popup ="//div[@class='modal-content']//div[@class='modal-header']//h5";
	
	public final static String PaymentPage_Header ="//div[@class='col-12']//h1[contains(text(),'Payment')]";
	
	public final static String Spirit_logo ="//img[@class='logo']";
	
	public final static String Seat_Assignment_Seat_page ="//div[@class='row mt-3 ng-star-inserted']//div[@class='col-2 text-right']//p";
	
	public final static String Seat_price_Seats_page ="//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[@class='col-6 col-sm-5 d-flex justify-content-end']//p";
	
	public final static String Seat_price_Payment_page ="//app-breakdown-section[@header='SEATS']//div[@class='col-4 col-lg-3 col-xl-2 d-flex justify-content-end']";
	
	public final static String Passenger_info_carrot_PaymentPage ="//div[@class='card-body mt-3 mb-3']//div[@class='col-md-2 col-lg-1 d-flex justify-content-end ng-star-inserted']//i[@class='fas fa-chevron-down']";
	
	public final static String Total_Due_carrot_PaymentPage ="//div[@class='total-due-container']//i[@class='fas fa-chevron-up color-white ng-trigger ng-trigger-openState']";
	
	public final static String Seat_Assignment_Payment_page ="//div[@class='seats-section mt-3']//p";
	
	public final static String Online_Checkin_Header ="//div[@class='row mt-4 ng-star-inserted']//h1";
	
	public final static String Add_Seats_button ="//div[@class='col-12 col-md-5 d-flex justify-content-end']//button[contains(text(),'Add Seats')]";
	
	public final static String BagsTotal_Price ="//div[@class='row']//div[@class='col-12 col-sm-5 d-sm-flex justify-content-sm-end']//p";
	
	public final static String Standard_pricing ="//div[@class='col-lg-5 col-md-4 d-md-flex flex-md-column justify-content-end']//p[@class='h3 text-uppercase text-center text-md-right spacing-bottom']";
	
	public final static String SeatsPage_header ="//div[@class='col-12']//h1";
	
	public final static String TravelGuard_yes_radio ="//div[@class='custom-control custom-radio']//label[@for='radio-insuranceCoverage1']";
	
	public final static String Reserve_A_Car_Popup ="//div[@class='modal-header']//h2[contains(text(),\"Reserve A Car\")]";
	
	public final static String Reserve_A_Car_Popup_No_Button ="//button[contains(text(),'No')]";
	
	public final static String Vacation_link ="//li[@class='nav-item ng-star-inserted']//span[text()='Vacation']";
	
	public final static String Driver_Age_text_box="//div[@class='input-group ng-star-inserted']//input[@id='driverAge']";
	
	public final static String Search_Vacations_button = "//button[contains(text(),'Search Vacation')]";
	
	public final static String Primary_Passenger_is_The_Contact_Person_CheckBox = "//label[@for='check-usePrimaryAsContact']";
	
	public final static String Continue_with_Bare_fare_Button = "//button[contains(text(), 'Continue With Bare Fare')]";
	
	public final static String driver_age = "//div[@class='row form-group ng-star-inserted']//div[@class='input-group ng-star-inserted']//select[@name='driverAge' and @id='driverAge']";
	
	public final static String Book_Car_button = "//button[contains(text(),'Book Car')]";
	
	public final static String Select_Car_button = "//button[contains(text(),'SELECT CAR')]";
	
	public final static String Shortcut_Boarding_Add_button = "//div[@class='row d-flex align-items-stretch']//div[2]//app-extra-simple-col//a[1]";
	
	
	public final static String Change_flights="//button[contains(text(),'Change Flights')]";
	public final static String popup_editchkbtn="//div[@class='journeyContainer ng-star-inserted']//input[@type='checkbox']";
	
	public final static String To_destinationdrop="//select[@name='destinationStationCode']";
	   
    public final static String popup_continue="//button[contains(text(),'Continue')]";

    public final static String Primary_driver_dropdown = "//select[@id='primaryDriverIndex']";

}
