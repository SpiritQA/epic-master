package com.cucumber.pages;

public class SeatsPage319 {

	public final static String From_FLL_City = "//app-station-picker-dropdown[@_ngcontent-c18][1]//button[contains(text(),' Fort Lauderdale, FL (FLL) ')]";

	public final static String TO_ATL_City = "//app-station-picker-dropdown[@_ngcontent-c18][2]//button[contains(text(),' Atlanta, GA (ATL) ')]";

	public final static String lap_child_doesnt_required_seat = "//label[@for='child1LapOption']";

	public final static String child_car_seat = "//label[@for='hasCarSeat1']";

	public final static String first_cities_names_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//p[1][@class='font-weight-bold']";

	public final static String second_cities_names_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//p[1][@class='font-weight-bold']";

	public final static String passenger_name_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//div[@class='col-10 col-sm-7 col-lg-8 font-weight-bold'][1]//p[1]";

	public final static String Airbus_32_A = "//strong[text()='Airbus 32A']";

	public final static String Airbus_32_B = "//strong[text()='Airbus 32B']";

	public final static String Airbus_32_A_exitrow_six = "//div[@class='reanimate-seat-rows ng-star-inserted exit-row ng-star-inserted'][1]//app-unit[7]";

	public final static String Airbus_32_A_exitrow_popup = "//button[text()='Accept']";

	public final static String seats_page_continue_button = "//div[@class='row purchase-seating-container d-none d-sm-block']//button[text()='Continue']";

	public final static String Airbus32_B_exit_row_columnone_one = "//div[@class='reanimate-seat-rows ng-star-inserted exit-row ng-star-inserted'][1]/app-unit[1]";

	public final static String Airbus_Name = "//div[@class='row']//p[2]/strong";

	public final static String seats_page_cities_caret_two = "//div[@class='row mb-3 mt-3 ng-star-inserted']//following::div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//i";

	public final static String seats_page_seats_total_caret = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//i";

	public final static String first_city_total_price_validation = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[2]/div";

	public final static String second_city_total_price_validation = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[3]/div";

	public final static String second_flight_carrot = "//div[@class='row mb-3 mt-3 ng-star-inserted']/div[4]//i[@class='fa fa-chevron-down']";

	public final static String checkin_path_Edit_seats_passeneger_one = "//div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-valid'][1]//button[text()='Edit Seats']";

	public final static String chekin_path_seatspage_bags_popup = "//div[@class='modal-content']//div[@class='col-12 col-sm-6'][2]";

	public final static String checkin_path_extraspage_text_validation = "//p[text()='Extras']";

	public final static String checkin_path_extrapage_continue_button = "//button[text()=' Continue ']";

	public final static String Mytrips_button_confromationpage = "//div[@class='desktop']//a[@aria-label='My Trips']";
	
	public final static String mytrips_change_flights ="//button[contains(text(),'Change Flights')]";
	
	public final static String mytrips_edit_seats="//button[contains(text(),'Edit Seats')]";
	
	public final static String mytrips_select_seats_popup ="//button[contains(text(),'Select Seats')]";

	public final static String Airbus_319_Big_Front_Seat_1A = "//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[1]";
	public final static String Airbus_319_Big_Front_Seat_1B = "//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[3]";
	public final static String Airbus_319_Big_Front_Seat_1E = "//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[5]";
	public final static String Airbus_319_Big_Front_Seat_1F = "//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[7]";
	public final static String Airbus_319_Big_Front_Seat_2A = "//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[1]";
	public final static String Airbus_319_Big_Front_Seat_2B = "//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[3]";
	public final static String Airbus_319_Big_Front_Seat_2E = "//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[5]";
	public final static String Airbus_319_Big_Front_Seat_2F = "//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[7]";
	public final static String Airbus_319_Big_Front_Seat_3E = "//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[5]";
	public final static String Airbus_319_Big_Front_Seat_3F = "//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[7]";

	// Standard Seating part 1 (row 4 seems to have issues in placement. I believe
	// they are priority seating but the diagram has not been developed properly.
	public final static String Airbus_319_Standard_Seat_4A = "//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_4B = "//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_4C = "//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_5A = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_5B = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_5C = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_5D = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_5E = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_5F = "//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_6A = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_6B = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_6C = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_6D = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_6E = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_6F = "//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_7A = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_7B = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_7C = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_7D = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_7E = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_7F = "//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_8A = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_8B = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_8C = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_8D = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_8E = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_8F = "//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_9A = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_9B = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_9C = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_9D = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_9E = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_9F = "//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_10A = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_10B = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_10C = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_10D = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_10E = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_10F = "//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[7]";

	// Exit Row 11
	public final static String Airbus_319_Exit_Seat_11A = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[1]";
	public final static String Airbus_319_Exit_Seat_11B = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[2]";
	public final static String Airbus_319_Exit_Seat_11C = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[3]";
	public final static String Airbus_319_Exit_Seat_11D = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[5]";
	public final static String Airbus_319_Exit_Seat_11E = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[6]";
	public final static String Airbus_319_Exit_Seat_11F = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[7]";

	// Standard Seating Part 2

	public final static String Airbus_319_Standard_Seat_12A = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_12B = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_12C = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_12D = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_12E = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_12F = "//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_13A = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_13B = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_13C = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_13D = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_13E = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_13F = "//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_14A = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_14B = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_14C = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_14D = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_14E = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_14F = "//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_15A = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_15B = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_15C = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_15D = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_15E = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_15F = "//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_16A = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_16B = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_16C = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_16D = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_16E = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_16F = "//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_17A = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_17B = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_17C = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_17D = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_17E = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_17F = "//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_18A = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_18B = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_18C = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_18D = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_18E = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_18F = "//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_19A = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_19B = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_19C = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_19D = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_19E = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_19F = "//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_20A = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_20B = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_20C = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_20D = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_20E = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_20F = "//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_21A = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_21B = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_21C = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_21D = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_21E = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_21F = "//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_22A = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_22B = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_22C = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_22D = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_22E = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_22F = "//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_23A = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_23B = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_23C = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_23D = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_23E = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_23F = "//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_24A = "//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_24B = "//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_24C = "//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_24D = "//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_24E = "//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_25A = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_25B = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_25C = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_25D = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_25E = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_25F = "//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[7]";
	public final static String Airbus_319_Standard_Seat_26A = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[1]";
	public final static String Airbus_319_Standard_Seat_26B = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[2]";
	public final static String Airbus_319_Standard_Seat_26C = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[3]";
	public final static String Airbus_319_Standard_Seat_26D = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[5]";
	public final static String Airbus_319_Standard_Seat_26E = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[6]";
	public final static String Airbus_319_Standard_Seat_26F = "//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[7]";

}
