package com.cucumber.pages;

public class VacationPage {

	public final static String Book_link = "//section[@class='tabset']/tabset[1]/ul[@class='nav nav-tabs']/descendant::span[text()='Book']";

	public final static String vacation_link = "//ul[@class='nav nav-tabs']/descendant::span[text()='Vacation']";

	public final static String Flight_car = "//label[@class='custom-control-label' and @for='customRadio1']";

	public final static String Flight_Hotel = "//label[@class='ng-star-inserted custom-control-label' and @for='radio-flightHotel']";

	public final static String Flight_hotel_car = "//label[@class='custom-control-label' and @for='customRadio3']";

	public final static String From_city = "//div[@class='ng-tns-c20-6']//input[@id='originStationCode']";

	public final static String Number_of_cities = "//div[@class='row']/following::label[text()='From']/following::input[@name='destinationStationCode']";

	public final static String vacation_select_from_city = "//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-7 ng-star-inserted']//button[contains(text(),'Denver, CO (DEN)')]";

	public final static String vacation_select_to_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block']//div[5]//div[2]//button[1]";

	public final static String To_city = "//div[@class='ng-tns-c20-6']//input[@id='destinationStationCode']";

	public final static String passengers = "//input[@id='passengers']";

	public final static String Numbe_of_adults = "//div[contains(text(),'Adults')]//following::input[@class='form-control custom-form-counter'][1]";

	public final static String Numbe_of_child = "//div[contains(text(),'Children')]//following::input[@class='form-control custom-form-counter']";

	public final static String pomocode = "//div[@class='col-sm-3']/following::label[text()='Promo']/following::input[@class='form-control'][2]";

	public final static String Driver_age = "//select[@name='driverAge']";

	public final static String Rooms = "//select[@name='hotelRoomCount']";

	public final static String Search_Button = "//div[@class='row']/following::button[text()='Search'][2][@class='btn btn-primary']";

	public final static String Departure_and_Return_dates = "//tab[@class='active tab-pane']//div[@class='input-group ng-star-inserted']//input[@name='dates']";

	public final static String Select_Your_hotel_header = "//h1";

	public final static String Passengers_Required_header = "//div[@class='modal-content']//h2";

	public final static String Passengers_Required_popup_close_button = "//button[contains(text(),'Close Window')]";

	public final static String No_Hotel_header = "//div[@class='modal-content']//h2";

	public final static String No_Hotel_content = "//div[@class='modal-content']//div[@class='modal-body']//div[@class='col-12 text-center'][1]";

	public final static String No_Hotel_x_button = "//div[@class='modal-header']//i[@class='icon-close']";

	public final static String pomocode_textbox = "//input[@id='promoCode']";

	public final static String Promocode_Popup_Title = "//div[@class='modal-content']//h2";

	public final static String Edit_coupon_code_buttton = "//div[@class='modal-content']//button[contains(text(),'edit coupon code')]";

public final static String Book_Car_Button ="//button[@class='btn btn-primary btn-sm'][contains(text(),'Book Car')]";
	
	public final static String Primary_Driver_Field ="//select[@id='primaryDriverIndex']";
	
	public final static String Primary_Driver_Is_Required_Error_Message ="//div[contains(text(),'Primary driver is required')]";

}
