package com.cucumber.pages;

public class BagsPage {

	// add bags with the plus and minus buttons

	public final static String personal_item_text = "//p[contains(text(),'1 Personal Item')]";

	public final static String city_names_validation = "//div[@class='row ng-star-inserted']//div[@class='col-sm-10']";

	public final static String carry_on_bag_plus_button = "//p[text()='Carry-On Bag']//following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2'][1]//i[@class='icon-add-circle']";

	public final static String carry_on_bag_minus_button = "//p[text()='Carry-On Bag']//following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2'][1]//i[@class='icon-minus-circle']";

	public final static String checked_bag_plusbutton = "//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle']";

	public final static String checked_minus_plusbutton = "//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-minus-circle']";

	public final static String carry_on_bag_price_validation = "//p[text()='Carry-On Bag'][1]/following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2'][1]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String checked_bag_price_validation = "//p[text()='Checked Bag']/following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String add_sporting_equipment = "//p[contains(text(),' Add Sporting Equipment ')]//i[@class='icon-chevron-up color-action ng-trigger ng-trigger-openState']";
	
	public final static String adding_sporting_equipment_passenger_four="//div[@class='passenger-bag-container ng-star-inserted'][4]//p[contains(text(),' Add Sporting Equipment ')]//i[@class='icon-chevron_up color-action ng-trigger ng-trigger-openState']";

	public final static String bicycle_plus_buton = "//p[contains(text(),'Bicycle - Bring non-motorized bicycle ')]//following::div[@class='d-flex justify-content-center'][1]//i[@class='icon-add-circle']";

	public final static String bicycle_minus_buton = "//p[contains(text(),'Bicycle - Bring non-motorized bicycle')]/following::div[@class='d-flex justify-content-center'][1]//i[@class='icon-minus-circle']";

	public final static String bicycle_price_validation = "//p[contains(text(),'Bicycle - Bring non-motorized bicycle ')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String surfboard_plus_button = "//p[contains(text(),'Surfboard - Put up to two boards in the same package ')]//following::i[@class='icon-add-circle']";

	public final static String surfboard_minus_button = "//p[contains(text(),'Surfboard - Put up to two boards in the same package ')]//following::i[@class='icon-minus-circle']";

	public final static String surfboard_price_validation = "//p[contains(text(),'Surfboard - Put up to two boards in the same package ')]//following::p[@class='font-weight-bold no-margin bottom text-right']";

	public final static String weight_41_50_lbs_plus_button = "//p[contains(text(),'41 - 50lbs.(18-23 kg)')]/following::i[@class='icon-add-circle'][1]";

	public final static String weight_41_50_lbs_minus_button = "//p[contains(text(),'41 - 50lbs.(18-23 kg)')]/following::i[@class='icon-minus-circle'][1]";

	public final static String weight_41_50_lbs_price_validation = "//p[contains(text(),'41 - 50lbs.(18-23 kg)')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String weight_51_70_lbs_plus_button = "//p[contains(text(),'51 - 70lbs.(23-32 kg)')]/following::i[@class='icon-add-circle'][1]";

	public final static String weight_51_70_lbs_minus_button = "//p[contains(text(),'51 - 70lbs.(23-32 kg)')]/following::i[@class='icon-minus-circle'][1]";

	public final static String weight_51_70_price_validation = "//p[contains(text(),'51 - 70lbs.(23-32 kg)')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String weight_71_100_lbs_plus_button = "//p[contains(text(),'71 - 100lbs.(32-45 kg)')]/following::i[@class='icon-add-circle'][1]";

	public final static String weight_71_100_lbs_minus_button = "//p[contains(text(),'71 - 100lbs.(32-45 kg)')]/following::i[@class='icon-minus-circle'][1]";

	public final static String weight_71_100_price_validation = "//p[contains(text(),'71 - 100lbs.(32-45 kg)')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String weight_68_80_lbs_plus_button = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::i[@class='icon-add-circle'][1]";

	public final static String weight_68_80_lbs_minus_button = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::i[@class='icon-minus-circle'][1]";

	public final static String weight_68_80_price_validation = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String Special_item_plus_button = "//p[contains(text(),'Special items over 80 linear inchces.(203 cm)')]/following::i[@class='icon-add-circle'][1]";

	public final static String Special_item_minus_button = "//p[contains(text(),'Special items over 80 linear inchces.(203 cm)')]/following::i[@class='icon-minus-circle'][1]";

	public final static String special_item_price_validation = "//p[contains(text(),'Special items over 80 linear inchces.(203 cm)')]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String Bags_total_button = "//p[contains(text(),'Bags Total')]//following::i[1]";

	public final static String total_bags_price = "//p[@class='d-inline font-weight-bold text-uppercase mr-3 mb-0']";
	
	public final static String total_carry_on_bag_price ="//app-price-section-line[1]//div[@class='col-6 d-flex justify-content-end']//p[1]";
	
	public final static String total_checked_bag_price ="//app-price-section-line[2]//div[@class='col-6 d-flex justify-content-end']//p[1]";
	
	public final static String total_Bike_price ="//p[@class='d-inline font-weight-bold text-uppercase mr-3 mb-0']";
	
	public final static String total_surf_board_price ="//app-price-section-line[4]//div[@class='col-6 d-flex justify-content-end']//p[1]";

	public final static String your_itinerary_caret = "//div[contains(text(),'Your Itinerary')]/following::i[@class='icon-chevron-up color-action ng-trigger ng-trigger-openState'][1]";

	public final static String your_itinerary_bags_total = "//div[@class='col-7 headings-font-weight']/i[@class='icon-Checked-Bag mr-2']/following::div[1]";

	public final static String continue_withou_bags = "//button[@class='btn btn-link btn-sm d-none d-md-inline-block']";

	public final static String popup_i_need_bags = "//button[contains(text(),' buy bags now ')]";

	public final static String popup_idont_need_bags = "//div[@class='col-12 col-md-6 mb-2 mb-md-0']//button[@class='btn btn-secondary btn-block']";

	public final static String NineFC_Continue_button = "//p[contains(text(),'$9 Fare Club® Pricing')]// following::button[contains(text(),'Continue')][1]";

	public final static String NineFC_popup_text_validation = "//p[@class='mb-0']";

	public final static String NineFC_popup_sigup_button = "//button[contains(text(),'sign up')]";

	public final static String NineFC_popup_signup_password = "//input[@id='password']";

	public final static String NineFC_popup_signup_confirm_password = "//input[@id='confirmPassword']";

	public final static String NineFC_popup_signup_confirmwith_email = "//input[@id='confirmPassword']";

	public final static String NineFC_popup_signup_error_text = "//label[contains(text(),' Choose Password')]//following::div[@class='s-error-text ng-star-inserted'][1]";

	public final static String NineFC_popup_signup_continue_with_statndardfare = "//button[contains(text(),'continue with standard fares')]";

	public final static String NineFC_popup_login_button = "//button[contains(text(),'log in')]";

	// add bags with text input
	public final static String carry_on_bag_Text_input = "//p[contains(text(),'Carry-On Bag')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String checked_bag_Text_input = "//p[text()='Checked Bag']/following::input[@class='form-control custom-form-counter'][1]";

	public final static String add_bicycle_Text_input = "//p[contains(text(),'Bicycle - Bring one non-motorized bicycle')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String add_surfboard_Text_input = "//p[contains(text(),'Surfboard - Put up to two borads in the same package')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_41_50lb_Text_input = "//p[contains(text(),'41 - 50lbs.(18-23 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_51_70lb_Text_input = "//p[contains(text(),'51 - 70lbs.(23-32 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_71_100lb_Text_input = "//p[contains(text(),'71 - 100lbs.(32-45 kg)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String overweight_60_80_linear_iches_Text_input = "//p[contains(text(),'68 - 80 linear inchces.(158-203 cm)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String special_items_over80_inches_Text_input = "//p[contains(text(),'Special items over 80 linear inchces.(203 cm)')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String More_infomation_link = "//a[text()='More information']";

	public final static String New_tab_search_buttton = "//input[@name='commit']";

	public final static String embargo_restrictions = "//a[contains(text(),'embargo restrictions')]";

	// PASSENGER TWO

	public final static String passenger_two_carryonbag_plusbutton = "//div[@class='ng-star-inserted']//p[text()='Carry-On Bag']//following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2'][1]//i[@class='icon-add-circle']";

	public final static String passenger_checkedbag_plusbutton = "//div[@class='ng-star-inserted']//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle']";

	public final static String passneger_two_adding_sport_equipment = "//div[@class='ng-star-inserted']//p[contains(text(),' Add Sporting Equipment ')]//i[@class='icon-chevron_up color-action ng-trigger ng-trigger-openState']";

	public final static String passenger_two_surfboard_plusbutton = "//div[@class='ng-star-inserted']//div[@class='collapse in show']//div[3]//app-count-picker[1]//i[2]";
	public final static String passeneger_two_checkedbag_input_text = "//div[@class='ng-star-inserted']//p[text()='Checked Bag']/following::input[@class='form-control custom-form-counter'][1]";

	public final static String passenger_two_checkedbag_price_validation = "//div[@class='ng-star-inserted']//p[text()='Checked Bag']/following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String passnger_two_surfboard_text_input = "//div[@class='ng-star-inserted']//p[contains(text(),'Surfboard - Put up to two borads in the same package')]/following::input[@class='form-control custom-form-counter'][1]";

	public final static String passebger_two_surfboard_price_validation = "//app-bags-page[1]/section[1]/div[3]/div[2]/div[3]/div[3]/div[3]/p[1]";

	public final static String passneger_two_carryonbag_price_validation = "//div[@class='ng-star-inserted']//p[text()='Carry-On Bag'][1]/following::div[@class='col-5 col-md-3 col-lg-auto col-xl-2'][1]/following::p[@class='font-weight-bold no-margin bottom text-right'][1]";

	public final static String Continue_With_Bare_Fare_buttton = "//button[contains(text(),'Continue With Bare Fare')]";

	public final static String Choose_This_Bundle_buttton = "//button[contains(text(),'Choose This Bundle')]";

	public final static String Choose_Thrills_Combo_buttton = "//button[contains(text(),'Choose Thrills Combo')]";

	public final static String ShoppingCart_carrot = "//div[contains(text(),'Your Itinerary')]/following::i[@class='icon-chevron_up color-action ng-trigger ng-trigger-openState'][1]";

	public final static String BagsTotal = "//app-total-breakdown/div[1]//div[2]/p";

	public final static String BagsPopupH2 = "//div[@class='modal-content']//div[@class='modal-header']//h2[1]";

	public final static String BagsPopupBody = "//div[@class='modal-content']//div[@class='modal-body']";
	
	public final static String checked_bag_plusbutton_disabled = "//section[1]/div[3]/div[1]//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle disabled']";
	  public final static String Save_money_9dfc = "//div[@class='price-flag d-none d-md-flex']//p[@class='font-weight-bold text-uppercase']";
	  
	  public final static String nineDFC_continue="//p[contains(text(),' $9 Fare Club® Pricing ')][1]//following::button[contains(text(),'Continue')][1]";
	  
	  public final static String nineDFC_modal_content="//p[@class='p-small']//span";
	  
	    public final static String Standard_pricing_continue = "//div[@class='col-lg-5 col-md-4 d-md-flex flex-md-column justify-content-end']//button[@class='btn btn-primary btn-primary-mobile align-self-end'][contains(text(),'Continue')]";
	 
	   
	    public final static String popup_modal_fade_show="//bs-modal-backdrop[@class='modal-backdrop fade in show']";
	    
	    public final static String popup_ninedfc_modal="//div[@class='modal-content']";
	    
	    public final static String popup_closebtn="//div[@class='modal-header']//button[@type='button']//i[@class='icon-close']";
	    
	    public final static String popup_modal_content="//div[@class='modal-body']//div[1]//div[1]";
	    
	    public final static String popup_signup="//div[@class='modal-body']//button[contains(text(),'sign up')]";
	    
	    public final static String popup_login="//div[@class='modal-body']//button[contains(text(),'log in')]";
	    
	    public final static String popup_cont_std_farebtn="//button[contains(text(),'continue with standard fares')]";
	    
	    public final static String popup_emailaddress="//input[@id='username']";
	    
	    public final static String popup_password="//input[@id='password']";
	    
	    public final static String popup_choose_pwd="//div[@class='form-group ng-star-inserted']//input[@id='password']";
	    
	    public final static String popup_confirm_pwd="//div[@class='form-group ng-star-inserted']//input[@id='confirmPassword']";
	    
	    public final static String pop_err_must_match="//div[@class='s-error-text ng-star-inserted']";
	    
	    public final static String pop_alert_err="//div[@class='alert alert-danger mt-3 ng-star-inserted']";
	    
	    public final static String pop_user_err="//input[@id='username']//following::div[@class='s-error-text ng-star-inserted'][1]";
	    
//	    public final static String pop_pwd_err="//input[@id='password']//following::div[@class='s-error-text ng-star-inserted']";
	    
	    public final static String pop_reset_pwd="//a[@id='aria-emailHelp']";
	    
	    public final static String pop_err_invalid_emailorpwd="//div[@class='alert alert-danger mt-3 ng-star-inserted']";
	    
	    public final static String popup_signup_emailbtn="//button[@type='submit']";
	    
	    public final static String popup_loginbtn="//button[contains(text(),' Log In')]";
	    
	    public final static String popup_seats_i_dont_need_seats="//div[@class='col-12 col-sm-6']//button[@class='btn btn-secondary']";
	    
	    public final static String Popup_dont_purchase_bags="//div[@class='col-12 col-sm-6']//button[@class='btn btn-secondary']";
	    
	    public final static String Retrieve_pwd="//input[@id='emailOrNumberText']";
	    
	    public final static String send_btn="//button[@name='sendButton']";
	    
	    public final static String Pwd_reset_msg="//div[@class='row alert alert-success ng-star-inserted']";
	    
	    public final static String continue_with_bags = "//div[@class='row mt-4 text-center']//button[@class='btn btn-link btn-sm']";
	    
	    public final static String checked_bag_psgr2_plusbutton = "//div[@class='ng-star-inserted']//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle']";
	    
	    public final static String checked_bag_psgr2_plusbutton_return ="//section[@class='ng-star-inserted']//div[6]//div[2]//div[2]//div[3]//app-count-picker[1]//div[1]//i[2]";
	    
	    public final static String checked_bag_psgr2_plusbutton_disable = "//div[@class='ng-star-inserted']//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle disabled']";
	 
	    public final static String checked_bag_psgr2_minusbutton = "//div[@class='ng-star-inserted']//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-minus-circle']";
	    public final static String carry_on_bag_psgr2_price_validation = "//div[@class='ng-star-inserted']//p[@class='font-weight-bold no-margin bottom text-right'][contains(text(),'$')]";
	    public final static String checked_bag_psgr2_price_validation = "//section[1]/div[3]/div[2]/div[2]/div[4]/p[1]";

	    public final static String carry_on_bag_psgr2_price_validation_return = "//section[@class='ng-star-inserted']//div[6]//div[2]//div[1]//div[4]//p[1]";


	    public final static String checked_bag_psgr2_price_validation_return = "//div[@class='ng-star-inserted']//div[@class='col-3 col-sm-2 col-lg-auto order-md-12 col-xl-1']//p[@class='font-weight-bold no-margin bottom text-right'][contains(text(),'$')]";

	    public final static String carry_on_bag_psgr2_plus_button = "//div[@class='ng-star-inserted']//div[@class='row align-items-center spacing-bottom']//div[@class='col-5 col-md-3 col-lg-auto col-xl-2']//i[@class='icon-add-circle']";

	    public final static String checked_bag_plusbutton_disable = "//div[@class='col-5 col-md-3 col-lg-auto col-xl-2 d-flex justify-content-center']//i[@class='icon-add-circle disabled']";

	    public final static String Save_money_bundle = "//div[@class='price-flag d-none d-md-flex']//p[@class='font-weight-bold text-uppercase']";

	    public final static String  carry_on_bag_plus_button_return="//section[@class='ng-star-inserted']//div[6]//div[1]//div[1]//div[3]//app-count-picker[1]//div[1]//i[2]";

	    public final static String carry_on_bag_price_validation_return = "//section[@class='ng-star-inserted']//div[6]//div[1]//div[1]//div[4]//p[1]";
	    
	 
	    public final static String checked_bag_plusbutton_return ="//section[@class='ng-star-inserted']//div[6]//div[1]//div[2]//div[3]//app-count-picker[1]//div[1]//i[2]";
	    public final static String checked_bag_price_validation_return = "//div[@class='passenger-bag-container ng-star-inserted']//div[@class='col-3 col-sm-2 col-lg-auto order-md-12 col-xl-1']//p[@class='font-weight-bold no-margin bottom text-right'][contains(text(),'$')]";
	    public final static String carry_on_bag_psgr2_plus_button_return = "//section[@class='ng-star-inserted']//div[6]//div[2]//div[1]//div[3]//app-count-picker[1]//div[1]//i[2]";
	    
	    public final static String popup_modal_content_1="//div[@class='modal-body']//div[@class='row']//p[@class='mb-0'][1]";
	    public final static String popup_modal_content_2="//div[@class='modal-body']//div[@class='row']//p[@class='mb-0'][2]";
		
		 public final static String pop_pwd_err="//input[@id='password']//following::div[@class='alert alert-danger mt-3 ng-star-inserted']";
		    public final static String Continue_Button = "//div[@class='col-12 d-flex justify-content-center']//button[contains(text(),'Continue')]";
	
}
