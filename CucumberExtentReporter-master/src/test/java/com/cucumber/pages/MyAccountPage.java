package com.cucumber.pages;

public class MyAccountPage {

	
	
	 public final static String MyAccount_header="//h1";
	 
	 public final static String MyAccount_My_Account_link="//div[@class='row mt-3']//li//a[contains(text(),'My Account')]";
	 
	 public final static String MyAccount_Personal_Information_link="//div[@class='row mt-3']//li//a[contains(text(),'Personal Information')]";
	 
	 public final static String MyAccount_Billing_Information_link="//div[@class='row mt-3']//li//a[contains(text(),'Billing Information')]";
	 
	 public final static String MyAccount_Email_Subscriptions_link="//div[@class='row mt-3']//li//a[contains(text(),'Email Subscriptions')]";
	 
	 public final static String MyAccount_Statements_link="//div[@class='row mt-3']//li//a[contains(text(),'Statements')]";
	 
	 public final static String MyAccount_Redeem_miles_link="//div[@class='row mt-3']//li//a[contains(text(),'Redeem Miles')]";
	 
	 public final static String MyAccount_Buy_or_gift_miles_link="//div[@class='row mt-3']//li//a[contains(text(),'Buy or Gift Miles')]";
	 
	 public final static String MyAccount_Request_Mileage_Credit_link="//div[@class='row mt-3']//li//a[contains(text(),'Request Mileage Credit')]";
	 
	 public final static String MyAccount_Welcome_message="//h2";
	 
	 public final static String MyAccount_Your_Current_miles="//div[@class='card']//p";
	 
	 public final static String MyAccount_Free_Spirit_Account_label="//div[@class='row']//div[@class='col-md-6']//p[contains(text(),' Free Spirit® Account Number ')]";
	 
	 public final static String MyAccount_Free_Spirit_Account_Number="//div[@class='row']//div[@class='col-md-6'][1]//p[2]";
	 
	 public final static String FSDrpDwn_Free_Spirit_Account_Number="//ul[@id='aria-dropdown-split']/li[2]/a";
	 
	 public final static String MyAccount_Mileage_Earning_Tier_label="//div[@class='col-md-6']//p[contains(text(),' Mileage Earning Tier ')]";
	 
	 public final static String MyAccount_Mileage_Earning_Tier ="//div[@class='col-md-6'][2]//p[2]";
	 
	 public final static String MyAccount_Marketing_tile="//a//img[@class='bank-banner']";
	 
	 public final static String MyAccount_Your_membership_header="//h3[contains(text(),'Your Membership')]";
	 
	 public final static String MyAccount_Membership_Name_Label="//p[contains(text(),' Membership Name ')]";
	 
	 public final static String MyAccount_Membership_Name="//div[@class='row']//div[@class='col-sm-6'][1]/p[2]";
	 
	 public final static String MyAccount_Date_Joined_Label="//p[contains(text(),'Date Joined')]";
	 
	 public final static String MyAccount_Date_Joined="//div[@class='row']//div[@class='col-sm-6'][2]/p[2]";
	 
	 public final static String MyAccount_Paid_Membership_Type_Label="//p[contains(text(),'Paid Membership Type')]";
	 
	 public final static String MyAccount_Paid_Membership_Type="//div[@class='row ']//div[@class='col-sm-6'][1]/p[2]";
	 
	 public final static String MyAccount_days_left_in_Membership_label ="//p[contains(text(),' Days Left in Membership ')]";
	 
	 public final static String MyAccount_days_left_in_Membership="//div[@class='row ']//div[@class='col-sm-6'][2]/p[2]";
	 
	 public final static String MyAccount_Cancel_Membership_btn="//button[contains(text(),'Cancel Membership')]";
	 
	 public final static String MyAccount_Your_Reservation_Header="//h3[contains(text(),'Your Reservations')]";
	 
	 public final static String MyAccount_Locate_Reservation_content="//div[@class='col-12 col-md-6 ']/p";
	 
	 public final static String MyAccount_Confirmation_Code_Label="//label[@for='recordLocator']";
	 
	 public final static String MyAccount_Confirmation_Code_TextBox="//input[@id='recordLocator']";
	 
	 public final static String MyAccount_Confirmation_Code_Go_btn="//button[contains(text(),'Go')]";
	 
	 public final static String MyAccount_Current_Reservation_Header="//h3[contains(text(),'Current Reservations')]";
	 
	 public final static String MyAccount_Past_Reservations_Header="//h3[contains(text(),'Past Reservations')]";
	 
	 //add a card page
	 public final static String Add_Card_Name_label="//label[@for='accountHolderName']";
	 
	 public final static String Add_Card_Name_TextBox="//input[@id='accountHolderName']";
	 
	 public final static String Add_Card_Name_Error_Message="//div[@class='col-12 col-md-6 col-lg-4 ng-star-inserted']//div[@class='input-errors text-right ng-star-inserted']/div";
	 
	 public final static String Add_Card_Card_number_label="//label[@for='cardNumber']";
	 
	 public final static String Add_Card_Card_Number_TextBox="//input[@id='cardNumber']";
	 
	 public final static String Add_Card_Card_Number_Error_message="//div[@class='col-12 col-md-6 col-lg-5 ng-star-inserted']//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_card_Expiration_Date_label="//label[@for='expMonthYear']";
	 
	 public final static String Add_card_Expiration_Date_TextBox="//input[@id='expMonthYear']";
	 
	 public final static String Add_card_Expiration_Error_Message="//div[@class='col-12 col-md-4 col-lg-3']//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_card_Use_My_Billing_info_CheckBox="//label[@for='useSameAddress']";
	 
	 public final static String Add_Card_Address_label="//label[@for='billingAddress']";
	 
	 public final static String Add_Card_Adress_TextBox="//input[@id='billingAddress']";
	 
	 public final static String Add_Card_Adress_Error_message="//div[@class='col-12 col-md-8']//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_Card_City_label="//label[@for='billingCity']";
	 
	 public final static String Add_Card_City_TextBox="//input[@id='billingCity']";
	 
	 public final static String Add_Card_City_ErrorMessage="//div[@class='col-12 col-md-4'][1]//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_Card_State_label="//label[@for='billingState']";
	 
	 public final static String Add_Card_State_TextBox="//input[@id='billingState']";
	 
	 public final static String Add_Card_State_DropDown ="//select[@name='billingState']";
	 
	 public final static String Add_Card_State_ErrorMessage="//div[@class='col-12 col-md-4'][2]//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_Card_ZipPostalCode_label="//label[@for='billingZipPostal']";
	 
	 public final static String Add_Card_ZipPostalCode_TextBox="//input[@id='billingZipPostal']";
	 
	 public final static String Add_Card_ZipPostalCode_ErrorMessage="//div[@class='col-12 col-md-4'][3]//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_Card_Country_label="//label[@for='billingCountry']";
	 
	 public final static String Add_Card_Country_Drpdwn="//Select[@id='billingCountry']";
	 
	 public final static String Add_Card_Country_Drpdwn_error_message="//div[@class='col-12 col-md-4'][4]//div[@class='input-errors text-right ng-star-inserted']//div";
	 
	 public final static String Add_Card_Save_changes_Btn="//button[@name='saveButton']";
	 
	 public final static String Add_Card_Cancel_Btn="//a[@name='cancelButton']";
	 
	 public final static String Email_Deals_EmailAdress_txtbox="//input[@id='initialEmailAddress']";
	 
	 public final static String Email_Deals_Continue_btn="//button[contains(text(),'Continue')]";
	 
	 public final static String Unsubscribe_Checkbox="//label[@for='unsubscribe']";
	 
	 public final static String SubmitBtn="//button[contains(text(),'Submit')]";
	 
	 public final static String Email_Deals_Popup_header="//div[@class='modal-content']//div//h2";
	 
	 public final static String Email_Deals_FirstName_txtbox="//input[@name='firstName']";
	 
	 public final static String EnrollmentSection="//section[@ngmodelgroup='freeSpirit']";
	 
	
	
	
	
	
	
	
	
	
}
