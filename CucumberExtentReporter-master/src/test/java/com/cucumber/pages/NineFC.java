package com.cucumber.pages;

public class NineFC {

public final static String Homepage_sigin_button ="//a[contains(text(),'Sign-In')]";
	
	public final static String login_account_emailadress ="//input[@id='username']";
	
	public final static String Login_account_password ="//input[@id='password']";

	public final static String login_button_onpopup ="//button[contains(text(),' Log In')]";
	
	public final static String nine_dfc_member_button="//a[@id='aria-basic-link']";  
     
       public final static String ShoppingCart_Itinerary="//div[@class='itinerary ng-star-inserted']";
     
     public final static String ShoppingCart_JoinNineFC_Carrot="//section//div[2]//div[@class='row ng-star-inserted']//a//app-chevron/i";
     
     public final static String ShoppingCart_Ninefc_Content_Line_One="//div[@class='border-top-1 flight-club-savings row expanded']";
     
     public final static String ShoppingCart_Ninefc_Content_Line_Two="//div[@class='fare-club-save-container']//div[@class='col-12']//p[1]";
     
     public final static String ShoppingCart_Ninefc_Content_Line_Three="//div[@class='fare-club-save-container']//div[@class='col-12']//p[2]";
     
     public final static String ShoppingCart_Ninefc_Content_Line_Four="//div[@class='fare-club-save-container']//div[@class='col-12']//a";
     
     public final static String ShoppingCart_Join_Now_And_Save_button="//div[@class='fare-club-save-container']//button";
     
     public final static String ShoppingCart_Ninefc_Congrats="//div[@class='row ng-star-inserted']//div[@class='d-flex justify-content-between align-items-center py-2 border-top-1 flight-club-savings'][1]";
     
     public final static String ShoppingCart_Option_carrot="//div[@class='row d-flex justify-content-between align-items-center py-2 border-top-1 ng-star-inserted']//a";

     public final static String ShoppingCart_NineFc_Price_title="//div[@class='itinerary ng-star-inserted']//div[@class='row ng-star-inserted'][1]//div[@class='col-7']//p[@class='p-small mb-0 ng-star-inserted']";
     
     public final static String ShoppingCart_NineFc_Price="//div[@class='itinerary ng-star-inserted']//div[@class='row ng-star-inserted'][1]//div[@class='col-5 d-flex flex-column align-items-end']//p";
     
     
     public final static String FS_member_loggedin_Displaying_Name_header_Link ="//a[@id='aria-basic-link']";

     public final static String FS_Header_Menu_Dropdown_Request_Mileage_Credit ="//a[contains(text(),'Request Mileage Credit')]";

//      http://qaepic01.spirit.com/account/retro-credit-request-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

     public final static String Request_Mileage_Credit ="//h1[@class='h1']";
     public final static String Verbiage1_Under_Request_Mileage_Credit ="//p[contains(text(),'To request mileage credit you will need your confirmation code for your reservation.')]";
     public final static String Verbiage2_Under_Request_Mileage_Credit ="//p[contains(text(),'You can:')]";
     public final static String Verbiage3_Under_Request_Mileage_Credit ="//li[contains(text(),'Add your Free Spirit® number to an upcoming flight')]";
     public final static String Verbiage4_Under_Request_Mileage_Credit ="//li[contains(text(),'Request mileage credit for a flight that you have ')]";
     public final static String Verbiage5_Under_Request_Mileage_Credit ="//p[contains(text(),'Free Spirit® miles will be added after flights are')]";
     public final static String Verbiage6_Under_Request_Mileage_Credit ="//p[contains(text(),'Please enter your six character confirmation code ')]";

     public final static String Confirmation_Code_Block_Verbiage_Above ="//label[@class='ng-star-inserted'][contains(text(),'Confirmation code (6 characters)')]";
     public final static String Confirmation_Code_Block_Verbiage_Inside ="//input[@placeholder=' Confirmation code (6 characters)']";
     public final static String Confirmation_Code_Block ="//input[@id='confirmationCode']";
     public final static String Confirmation_Code_Block_Go_Button ="//button[@type='submit']";
     public final static String Confirmation_Code_Block_Error_Message ="//div[@class='toast-message ng-star-inserted']";
     
     
     public final static String FS_member_SignUp_button = "//div[@class='col-md-12 col-lg-8 mt-lg-4 footerDesktop']//a[contains(text(),'Free Spirit®')]";
 	
 	public final static String your_miles ="//a[@class='dropdown-item'][contains(text(),'Your Miles')]";
 	
 	public final static String free_spirit ="//a[@class='dropdown-item'][contains(text(),'Free Spirit® #')]";
 	
 	public final static String my_reservation ="//a[@class='dropdown-item'][contains(text(),'My Reservations')]";
 	
 	public final static String account_link="//a[@class='dropdown-item'][contains(text(),'My Account')]";
 	
 	public final static String manage_subscriptions ="//a[@class='dropdown-item'][contains(text(),'Manage Subscriptions')]";
 	
 	public final static String statements ="//a[@class='dropdown-item'][contains(text(),'Statements')]";
 	
 	public final static String request_mileage_credit ="//a[@class='dropdown-item'][contains(text(),'Request Mileage Credit')]";
 	
 	public final static String request_mileage_creditlink = "//div[@class='col-md-3 ml-auto']//a[contains(text(),'Request Mileage Credit')]";
 	
 	public final static String request_mileage_ErrorMessage = "//div[contains(@class,'toast-message ng-star-inserted')]";
 	
 	public final static String buy_miles ="//a[@class='dropdown-item'][contains(text(),'Buy Miles')]";
 	
 	public final static String sign_out ="//a[@class='dropdown-item'][contains(text(),'Sign Out')]";

 	public final static String Billing_information ="//a[contains(text(),'Billing Information')]";
 	
 	public final static String add_another_card="//button[contains(text(),' Add Another Card')]";
 	
 	public final static String Request_Mileage_credit ="//a[contains(text(),'Request Mileage Credit')]";
 	
 	public final static String conformation_code_request_mileage_credit ="//input[@id='confirmationCode']";
 	
 	public final static String Request_mileage_credit_GO_button ="//button[contains(text(),'Go')]";
 	
 	public final static String add_card_validate_text="//h3[@class='ng-star-inserted']";
 	
 	public final static String subscribe_email_address="//input[@id='initialEmailAddress']";
 	
 	public final static String subscribe_email_continue_button="//button[contains(text(),'Continue')]";
 	
 	public final static String subscribe_email_checkbox = "//input[@id='unsubscribe']";
 	
 	public final static String validate_email_address="//input[@id='validateEmailAddress']";
 	
 	public final static String validate_confirm_email_address="//input[@id='validateNewEmailAddress']";
 	
 	public final static String validate_new_email_address="//input[@id='newEmailAddress']";
 	
 	public final static String ninedfc_member_firstname="//input[@id='firstName']";
 	
 	public final static String ninedfc_member_lastname="//input[@id='lastName']";
 	
 	public final static String Home_airport="//select[@id='primaryAirport']";
 	
 	public final static String secoundary_aiport="//select[@id='secondaryAirport']";
 	
 	public final static String subscribe_button="//button[contains(text(),'Submit')]";
 	
 	public final static String subscibe_Popup_Header = "//h2[contains(text(),'EMAIL DEALS')]";
 	
 	public final static String subscibe_Popup_SubHeader1 = "//div[@class='modal-body']//div[@class='row ']";
 	
 	public final static String subscibe_Popup_SubHeader2 = "//div[@class='modal-body']//div[@class='row mt-3']";
 	
 	public final static String subscribe_popup_HomePageButton =  "//button[contains(text(),'Go To Homepage')]";
 	
 	public final static String subscribe_popup_CloseButton =  "//button[contains(text(),'Close')]";
 	
 	public final static String statements_HeaderText = "//h2[contains(text(),'Statements')]";
 	
 	public final static String statements_ActivityPeriod = "//label[contains(text(),'Activity Period')]";
 	
 	public final static String statements_ActivityPeriod_DropDown = "//select[@id='activityPeriods']";
 	
 	public final static String statements_TransactionTypes = "//label[contains(text(),'Transaction Types')]";
 	
 	public final static String statements_TransactionTypes_DropDown = "//select[@id='activityTypes']";
 	
 	public final static String statements_PostedTransactions = "//h3[contains(text(),'Posted Transactions')]";
 	
 	public final static String statements_PostedTransactions_Fields = "//table[@class='table table-striped']//th";
 	
 	public final static String statements_FooterVerbiage = "//p[@class='p-legal']";
 	
 	public final static String validate_the_subscription ="//div[@class='col-12 mt-2 ng-star-inserted']//strong";
 	
 	public final static String free_spirit_link="//ul[@id='aria-dropdown-split']/li[2]";
 	
 	public final static String email_subscription_link ="//a[contains(text(),'Email Subscriptions')]";
 	
    public final static String nine_fc_link="//li[@class='nav2D']//a[contains(text(),'$9 Fare Club')]";
    
    public final static String  nine_fc_signup_title ="//select[@id='title']";
    
    public final static String nine_fc_signup_firstname="//input[@id='firstName']";
    
    public final static String nine_fc_signup_lastname="//input[@id='lastName']";
    
    public final static String nine_fc_signup_dateofbirth="//input[@id='dateOfBirth']";
    
    public final static String nine_fc_signup_email_address="//input[@id='emailAddress']";
    
    public final static String nine_fc_signup_confirm_email_address= "//input[@id='confirmEmail']";
    
    public final static String nine_fc_signup_password="//input[@id='password']";
    
 	public final static String nine_fc_signup_confirm_password="//input[@id='confirmPassword']";
 	
 	public final static String nine_fc_signup_continue_step_button="//button[contains(text(),'continue to step 2')]";
 	
 	public final static String nine_fc_signup_adress ="//input[@id='address1']";
 	
 	public final static String nine_fc_sigup_address2="//input[@id='address2']";
 	
 	public final static String nine_fc_signup_city="//input[@id='city']";
 	
 	public final static String nine_fc_signup_state="//select[@name='state']";
 	
 	public final static String nine_fc_sigup_zipcode="//input[@id='postalCode']";
 	
 	public final static String nine_fc_signup_country="//select[@id='country']";
 	
 	public final static String nine_fc_signup_primaryphone="//input[@id='homePhone']";
 	
 	public final static String nine_fc_signup_secoundaryphone="//input[@id='workPhone']";
 	
 	public final static String nine_fc_signup_primary_airport="//select[@id='primaryAirport']";
 	
 	public final static String nine_fc_signup_secoundary_airport="//select[@id='secondaryAirport']";
 	
 	public final static String nine_fc_signup_contine3_button="//button[contains(text(),'continue to step 3') or contains(text(),'free sign up')]";
 	
 	public final static String nine_fc_signup_nameoncard="//input[@id='accountHolderName']";
 	
 	public final static String nine_fc_signup_cardnumber="//input[@id='cardNumber']";
 	
 	public final static String nine_fc_signup_expiration_date="//input[@id='expMonthYear']";
 	
 	public final static String nine_fc_signup_securitycode="//input[@id='securityCode']";
 	
 	public final static String nine_fc_signup_billing_address="//input[@id='billingAddress']";
 	
 	public final static String nine_fc_signup_billingcity="//input[@id='billingCity']";
 	
 	public final static String nine_fc_signup_billing_state="//select[@id='billingState']";
 	
 	public final static String nine_fc_signup_billingzipcode="//input[@id='billingZipPostal']";
 	
 	public final static String nine_fc_sigup_billingcountry="//select[@id='billingCountry']";
 	
 	public final static String nine_fc_signup_signup_button="//button[contains(text(),'sign-up and start saving today!')]";
 	
 	public final static String nine_fc_adult_passeneger_plusbutton="//div[@class='ng-tns-c22-9']//div[@class='d-flex pt-1'][1]//div[contains(text(),'Adults')][1]//following::i[@class='icon-add-circle'][1]";
 	
 	public final static String nine_fc_adult_minusbutton="//div[@class='ng-tns-c22-9']//div[@class='d-flex pt-1'][1]//div[contains(text(),'Adults')][1]//following::i[@class='icon-minus-circle'][1]";
 	
 	public final static String nine_fc_child_plusbutton="//div[@class='ng-tns-c22-9']//div[@class='d-flex'][1]//div[contains(text(),'Children')][1]//following::i[@class='icon-add-circle'][1]";
 	
 	public final static String nine_fc_flight_availability_contine_button="//div[@class='fare-club-container mb-3']//button[contains(text(),' Continue')]";
 	
 	public final static String FS_member_Reset_Password="//a[contains(text(),' Reset Password')]";
 	
 	public final static String Terms_condition_text_validation_passenger_page="//button[contains(text(),'Terms & Conditions')]";

      public final static String Fs_member_popup_signup="//div[@class='modal-body']//a[contains(text(),'Sign Up')]"; 

     public final static String nine_fc_signup_middle_name="//input[@id='middleName']";
     
     public final static String FS_member_termsand_conditions="//label[@for='termsAccepted']";
     
     public final static String FS_Member_free_signup_button="//button[contains(text(),'free sign up')]";

     public final static String FS_member_BookNow_button="//button[contains(text(),'book now')]";
     
     public final static String Nine_fc_make_this_primary_card_checkbox ="//label[contains(text(),'Make this my primary card')]";
     
     public final static String Nine_fc_use_same_address_checkbox="//label[@for='useSameAddress']";
     
     public final static String Nine_fc_save_changes_button ="//button[@name='saveButton']";
     
    public final static String Nine_fc_cancel_button_primary_card  ="//a[@name='cancelButton']";
    
    public final static String Nine_fc_billing_address_validation="//tr[@class='ng-star-inserted']//td[5]";
    
    public final static String Nine_fc_expiration_date_invalid_text_validation="//div[contains(text(),'Expiration Date is invalid')]";
    
    public final static String Nine_fc_invalid_address_text_validation ="//div[contains(text(),'Only letters and numbers are allowed')]";
   
    public final static String Nine_fc_validate_card_number="//app-account-billing[@class='ng-star-inserted']//div[2]//td[3]";
   
    public final static String Nine_fc_delete_button_billing_information="//h3[contains(text(),'Additional Cards')]//following:: button[contains(text(),'Delete')]";
   
    public final static String Nine_fc_delete_card_popup_close_button="//i[@class='icon-close']";
   
    public final static String Nine_fc_delete_card_button_popup ="//button[contains(text(),'Delete Card')]";
    
    public final static String Nine_fc_cancel_membership_caret ="//button[@class='btn btn-link btn-sm pl-0']";
    
    public final static String Nine_fc_cancel_membership_popup_close_button="//i[@class='icon-close']";
    
    public final static String Nine_fc_cancel_membership_popup_keep_membership="//button[contains(text(),'Keep My Membership')]";
    
    public final static String Nine_fc_cancel_membership_popup_cancel_membership ="//button[@class='btn btn-link']";
    
    public final static String Nine_fc_cancel_membership_button="//button[@type='submit'][contains(text(),'Cancel Membership')]";
    
    public final static String Nine_fc_cancel_membership_dropdown="//select[@id='cancelReason']";
    
    public final static String Nine_fc_primary_card_edit_button="//h3[text()='Primary Card']//following::a[text()='Edit'][1]";
    
      public final static String Nine_fc_additional_card_number_validation="//div[@class='row saved-cards-table ng-star-inserted'][2]//td[3]";
      
      public final static String Nine_fc_primary_card_number_validation ="//div[@class='row saved-cards-table ng-star-inserted'][1]//td[3]";
      
      public final static String Nine_fc_additional_card_text_validation="//h3[contains(text(),'Additional Cards')]";
      
      public final static String MyAccount_link="//li[@role='menuitem']//a[contains(text(),'My Account')]";
      
      public final static String PersonalInformation_link="//a[contains(text(),'Personal Information')]";

      public final static String MemberName_Account_Page="//div[@class='col-12 col-md-6']//p[2]";

      public final static String MemberDOB_Account_Page="//input[@id='dateOfBirth']";
      
      public final static String Page_Load_Complete = "//div[@class='overlay']";
      
      public final static String DiscountedFare_CheaperBags = "//p[contains(text(),'Get discounted fares and cheaper bags')]/i";
      
      public final static String CoverEveryone_OnBooking = "//p[contains(text(),'Covers everyone on your booking. ')]/i";
      
      public final static String ToolTip_Popup_Header = "//div[@class='fareclub-container ng-star-inserted']/div/div/p[1]";
      
      public final static String ToolTip_Popup_Body = "//div[@class='fareclub-container ng-star-inserted']/div/div/p[2]";
      
      public final static String SignUpNow_Button = "//a[contains(text(),'SIGN-UP NOW')]";
      
      public final static String Terms_And_Conditions = "//a[contains(text(),'Terms and Conditions')]";
      
      public final static String FAQ = "//a[contains(text(),'F.A.Q.')]";
      
      public final static String SignUp_Faster_Easier = "//a[contains(text(),'Sign-up faster and easier.')]";
      
      public final static String SignUp_Error_Message = "//div[@class='s-error-text ng-star-inserted']";
  	
      public final static String login_Popup = "//div[@class='modal-content']";
      
      public final static String login_button_Close = "//i[@class='icon-close']";
      
      public final static String login_Reset_Password = "//a[contains(text(),'Reset Password')]";
      
      public final static String login_SignUp_link = "//a[contains(text(),'Sign Up')]";
      
      public final static String login_Popup_Label = "//label[@class='ng-star-inserted asterisk']";
      
      public final static String login_Popup_Header = "//div[@class='modal-header']";
      
      public final static String FS_member_header = "//h1[@class='h1 ng-star-inserted']";
      
      public final static String FS_member_subheader = "//div[@class='ng-star-inserted']";
      
      public final static String FS_member_singup_verbiage = "//div[@class='col-12 col-lg-4 text-center ng-star-inserted']";
      
      public final static String member_profile_Header = "//h1[contains(text(),'Your Free Spirit® Account')]";
      
      public final static String member_profile_SubHeader = "//h2[contains(text(),'Welcome back')]";
      
      public final static String member_profile_LeftPanelinks = "//div[@class='col-md-3 ml-auto']//a";
      
      public final static String member_profile_CurrentMiles = "//strong[contains(text(),'Your Current Miles')]";
      
      public final static String member_profile_fsNumber = "//p[contains(text(),'Free Spirit® Account Number')]";
      
      public final static String member_profile_MileagEarning = "//p[contains(text(),'Mileage Earning Tier')]";
      
      public final static String member_profile_BannerImage = "//img[@class='bank-banner']";
      
      public final static String member_profile_YourMembership = "//h3[contains(text(),'Your Membership')]";
      
      public final static String member_profile_MembershipName = "//p[contains(text(),'Membership Name')]";
      
      public final static String member_profile_DateJoined = "//p[contains(text(),'Date Joined')]";
      
      public final static String member_profile_MembershipVerbiage = "//div[@class='col-md-12 mb-2 ng-star-inserted']";
      
      public final static String member_profile_YourReservation = "//h3[contains(text(),'Your Reservations')]";
      
      public final static String member_profile_LocateReservation = "//div[@class='col-12 col-md-6 '][1]";
      
      public final static String member_profile_ConfirmationVerbiage = "//label[contains(text(),'Confirmation Code (6 characters)')]";
      
      public final static String member_profile_ConfirmationBox = "//input[@id='recordLocator']";
      
      public final static String member_profile_ConfirmationButton = "//button[contains(text(),'Go')]";
      
      public final static String member_profile_CurrentReservation = "//h3[contains(text(),'Current Reservations')]";
      
      public final static String member_profile_CurrentReservationHeader = "//h3[contains(text(),'Current Reservations')]//following::thead[@class='reservation-background']//th";
      
      public final static String member_profile_CurrentReservationFlights ="//h3[contains(text(),'Current Reservations')]//following::thead[@class='reservation-background']/../tbody/tr[1]/td";
      
      public final static String member_profile_PastReservation = "//h3[contains(text(),'Past Reservations')]";
      
      public final static String member_profile_PastReservationHeader = "//h3[contains(text(),'Past Reservations')]//following::thead[@class='reservation-background']//th";
      
      public final static String member_profile_PastReservationFlights ="//h3[contains(text(),'Past Reservations')]//following::thead[@class='reservation-background']/../tbody/tr[1]/td";
      
      public final static String manage_subscriptions_Header = "//h1[contains(text(),'Email Deals')]";
      
      public final static String manage_subscriptions_SubHeader1 = "//p[contains(text(),'Subscribe to our email deals and other great spirit.com offers!')]";
      
      public final static String manage_subscriptions_SubHeader2 = "//p[contains(text(),'Already a subscriber? Enter your email address to update your account or to unsubscribe.')]";
      
      public final static String manage_subscriptions_EmailTitle = "//label[contains(text(),'Email Address')]";
      
      public final static String manage_subscriptions_EmailBox = "//input[@id='initialEmailAddress']";
      
      public final static String manage_subscriptions_EmailButton = "//button[contains(text(),'Continue')]";
      
      public final static String manage_subscriptions_FooterText = "//p[contains(text(),'You may unsubscribe from future emails at any time by clicking the unsubscribe link in our emails.')]";
      
      public final static String personal_information_Address = "//input[@id='lineOne']";
      
      public final static String personal_information_City = "//input[@id='city']";
      
      public final static String personal_information_State = "//select[@name='provinceState']";
      
      public final static String personal_information_ZIPCode = "//input[@id='postalCode']";
      
      public final static String personal_information_Country = "//select[@id='countryCode']";
      
      public final static String personal_information_SaveButton = "//button[contains(text(),'Save Changes')]";

}


