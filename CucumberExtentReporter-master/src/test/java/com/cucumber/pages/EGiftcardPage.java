package com.cucumber.pages;

public class EGiftcardPage {

	public final static String moreGoGiftCardHeader = "//h1[contains(text(),'More Go E-Gift Cards')]";
	public final static String checkYourBalanceButton = "//button[@class='btn btn-secondary']";

	public final static String addamountof25 = "//p[contains(text(),'$25')]";
	public final static String addamountof50 = "//p[contains(text(),'$50')]";
	public final static String addamountof100 = "//p[contains(text(),'$100')]";
	public final static String enterAmount = "//input[@id='textAmount']";

	public final static String happyBirthdayOption = "//p[contains(text(),'Happy Birthday')]";
	public final static String congratulationsOption = "//p[contains(text(),'Congratulations')]";
	public final static String goForItOption = "//p[contains(text(),'Go For It')]";
	public final static String justBecauseOption = "//p[contains(text(),'Just Because (No Reason)')]";
	public final static String thinkingOfYouOption = "//p[contains(text(),'Thinking of You')]";
	public final static String missYouOption = "//p[contains(text(),'Miss You')]";
	public final static String loveYouOption = "//p[contains(text(),'Love You')]";
	public final static String timeForAVacayOption = "//p[contains(text(),'Time for a Vacay')]";
	public final static String justMarriedOption = "//p[contains(text(),'Just Married')]";
	public final static String customMessageOption = "//p[contains(text(),'Custom Message')]";

	public final static String carauselLeftBttn = "//i[@class='icon-chevron-left']";
	public final static String carauselRightBttn = "//i[@class='icon-chevron_right']";

	public final static String toTextBox = "//input[@id='textRecipientName']";
	public final static String fromTextBox = "//input[@id='textSenderName']";
	public final static String senderEmailTextBox = "//input[@id='textSenderEmail']";
	public final static String recipientEmailTextBox = "//input[@id='textRecipientEmail']";
	public final static String reEnterEmail = "//input[@id='textRecipientEmailCopy']";
	public final static String messageTextBox = "//textarea[@id='textMessage']";
	public final static String continueBttn = "//button[contains(@name,'continue')]";

	public final static String nameOnCard = "//input[@id='accountHolderName']";
	public final static String cardNumber = "//input[@id='cardNumber']";
	public final static String cardExpiration = "//input[@id='expMonthYear']";
	public final static String securityCode = "//input[@id='securityCode']";
	public final static String billingAddress = "//input[@id='billingAddress']";
	public final static String billingCity = "//input[@id='billingCity']";
	public final static String state = "//select[@name='billingState']";
	public final static String zipCode = "//input[@id='billingZipPostal']";
	public final static String country = "//select[@id='billingCountry']";
	public final static String iAgreeTC = "//label[@for='termsCheck']";
	public final static String continueBttnPurchase = "//button[@name='continue']";

	public final static String visa = "//img[@class='credit-card-brand visa-card']";
	public final static String amex = "//img[@class='credit-card-brand american-express']";
	public final static String mastercard = "//img[@class='credit-card-brand master-card']";
	public final static String discover = "//img[@class='credit-card-brand discover-card']";

	public final static String Nindfc_signup_page_validation ="//a[contains(text(),'SIGN-UP NOW!')]";
	
	public final static String AYUDA_page_validation="//input[@name='commit']";
	
	public final static String flight_availability_page_spanish_validation =" //button[contains(text(),' búsqueda nueva')]";
	
	public final static String spanish_signin_button="//a[contains(text(),'Ingresar')]";
	
	public final static String spanish_login_button="//button[contains(text(),' Inicia Sesión')]";
}
