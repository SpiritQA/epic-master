
package com.cucumber.pages;

public class SeatsPage321 {

public final static String Validate_Pax_Names ="//div[@class='col-9 font-weight-bold']";
	
	public final static String Validate_City_Pairs_Seats_Page ="//div[@class='flight-destination']//div[@class='col-10 col-sm-7 col-lg-8']";

	public final static String From_FLL_City = "//app-station-picker-dropdown[@_ngcontent-c18][1]//button[contains(text(),' Fort Lauderdale, FL (FLL) ')]";

	public final static String TO_SAN_City = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' San Diego, CA (SAN) ')]";
	
	public final static String lap_child_doesnt_required_seat="//label[@for='child1LapOption']";

	public final static String child_car_seat = "//label[@for='hasCarSeat1']";

	public final static String first_cities_names_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//p[1][@class='font-weight-bold']";

	public final static String second_cities_names_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//p[1][@class='font-weight-bold']";

	public final static String passenger_name_validation = "//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//div[@class='col-10 col-sm-7 col-lg-8 font-weight-bold'][1]//p[1]";

	public final static String Airbus_32_A = "//strong[text()='Airbus 32A']";

	public final static String Airbus_321 = "//strong[text()='Airbus 32B']";

	public final static String Airbus_32_A_exitrow_six = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[7]";

	public final static String Airbus_32_A_exitrow_popup = "//button[text()='Accept']";

	public final static String seats_page_continue_button = "//div[@class='row purchase-seating-container d-none d-sm-block']//button[text()='Continue']";

	public final static String Airbus_321_exit_row_columnone_one = "//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[1]";

	public final static String Airbus_Name = "//div[@class='reanimate-seat-rows']/following::div[@class='col-4 text-center']//strong";

	public final static String seats_page_cities_caret_two = "//div[@class='row mb-3 mt-3 ng-star-inserted']//following::div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//i";

	public final static String seats_page_seats_total_caret = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//i";

	public final static String first_city_total_price_validation = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[2]/div";

	public final static String second_city_total_price_validation = "//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[3]/div";

	public final static String second_flight_carrot = "//div[@class='row mb-3 mt-3 ng-star-inserted']/div[4]//i[@class='fa fa-chevron-down']";

	public final static String mytrips_edit_seats="//button[contains(text(),'Edit Seats')]";
	
	public final static String change_seat="//button[contains(text(),'Change Seat')]";
	public final static String first_flight_carrot ="//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//h4[contains(text(),'FLL - IAH')]";
	public final static String Choose_Thrills_Combo ="//button[contains(text(),'Choose Thrills Combo')]";
// AIRBUS 32-B
	
	
    public final static String Airbus_321_Big_Front_Seat_1A ="//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[1]";
	public final static String Airbus_321_Big_Front_Seat_1B ="//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[3]";
	public final static String Airbus_321_Big_Front_Seat_1E ="//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[5]";
	public final static String Airbus_321_Big_Front_Seat_1F ="//div[@class='reanimate-seat-rows ng-star-inserted'][1]//app-unit[7]";
	public final static String Airbus_321_Big_Front_Seat_2A ="//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[1]";
	public final static String Airbus_321_Big_Front_Seat_2B ="//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[3]";
	public final static String Airbus_321_Big_Front_Seat_2E ="//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[5]";
	public final static String Airbus_321_Big_Front_Seat_2F ="//div[@class='reanimate-seat-rows ng-star-inserted'][2]//app-unit[7]";
	//STANDARD SEATING
	public final static String Airbus_321_Standard_Seat_3A ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_3B ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_3C ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_3D ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_3E ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_3F ="//div[@class='reanimate-seat-rows ng-star-inserted'][3]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_4A ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_4B ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_4C ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_4D ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_4E ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_4F ="//div[@class='reanimate-seat-rows ng-star-inserted'][4]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_5A ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_5B ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_5C ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_5D ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_5E ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_5F ="//div[@class='reanimate-seat-rows ng-star-inserted'][5]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_6A ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_6B ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_6C ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_6D ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_6E ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_6F ="//div[@class='reanimate-seat-rows ng-star-inserted'][6]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_7A ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_7B ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_7C ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_7D ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_7E ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_7F ="//div[@class='reanimate-seat-rows ng-star-inserted'][7]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_8A ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_8B ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_8C ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_8D ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_8E ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_8F ="//div[@class='reanimate-seat-rows ng-star-inserted'][8]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_9A ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_9B ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_9C ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_9D ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_9E ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_9F ="//div[@class='reanimate-seat-rows ng-star-inserted'][9]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_10B ="//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_10C ="//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_10D ="//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_10E ="//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_10F ="//div[@class='reanimate-seat-rows ng-star-inserted'][10]//app-unit[7]";
	//EXIT ROW SEATING 
	public final static String Airbus_321_Exit_Seat_11A ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[1]";
	public final static String Airbus_321_Exit_Seat_11B ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[2]";
	public final static String Airbus_321_Exit_Seat_11C ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[3]";
	public final static String Airbus_321_Exit_Seat_11D ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[5]";
	public final static String Airbus_321_Exit_Seat_11E ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[6]";
	public final static String Airbus_321_Exit_Seat_11F ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[7]";
	//Standard Seating Part 2
	public final static String Airbus_321_Standard_Seat_12A ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_12B ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_12C ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_12D ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_12E ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_12F ="//div[@class='reanimate-seat-rows ng-star-inserted'][12]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_13A ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_13B ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_13C ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_13D ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_13E ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_13F ="//div[@class='reanimate-seat-rows ng-star-inserted'][13]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_14A ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_14B ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_14C ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_14D ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_14E ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_14F ="//div[@class='reanimate-seat-rows ng-star-inserted'][14]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_15A ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_15B ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_15C ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_15D ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_15E ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_15F ="//div[@class='reanimate-seat-rows ng-star-inserted'][15]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_16A ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_16B ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_16C ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_16D ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_16E ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_16F ="//div[@class='reanimate-seat-rows ng-star-inserted'][16]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_17A ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_17B ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_17C ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_17D ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_17E ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_17F ="//div[@class='reanimate-seat-rows ng-star-inserted'][17]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_18A ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_18B ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_18C ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_18D ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_18E ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_18F ="//div[@class='reanimate-seat-rows ng-star-inserted'][18]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_19A ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_19B ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_19C ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_19D ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_19E ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_19F ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_20A ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_20B ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_20C ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_20D ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_20E ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_20F ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_21A ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_21B ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_21C ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_21D ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_21E ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_21F ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_22A ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_22B ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_22C ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_22D ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_22E ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_22F ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_23A ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_23B ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_23C ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_23D ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_23E ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_23F ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_24A ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_24B ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_24C ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_24D ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_24E ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[6]";
	//EXIT ROW SEATING 
	public final static String Airbus_321_Exit_Seat_25A ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[1]";
	public final static String Airbus_321_Exit_Seat_25B ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[2]";
	public final static String Airbus_321_Exit_Seat_25C ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[3]";
	public final static String Airbus_321_Exit_Seat_25D ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[5]";
	public final static String Airbus_321_Exit_Seat_25E ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[6]";
	public final static String Airbus_321_Exit_Seat_25F ="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]//app-unit[7]";
	//Standard Seating Part 3
	public final static String Airbus_321_Standard_Seat_26A ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_26B ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_26C ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_26D ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_26E ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_26F ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_27A ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_27B ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_27C ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_27D ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_27E ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_27F ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_28A ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_28B ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_28C ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_28D ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_28E ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_28F ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_29A ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_29B ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_29C ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_29D ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_29E ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_29F ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_30A ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_30B ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_30C ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_30D ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_30E ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_30F ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_31A ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_31B ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_31C ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_31D ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_31E ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_31F ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_32A ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_32B ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_32C ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_32D ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_32E ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_32F ="//div[@class='reanimate-seat-rows ng-star-inserted'][32]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_33A ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_33B ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_33C ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_33D ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_33E ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_33F ="//div[@class='reanimate-seat-rows ng-star-inserted'][33]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_34A ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_34B ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_34C ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_34D ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_34E ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_34F ="//div[@class='reanimate-seat-rows ng-star-inserted'][34]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_35A ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_35B ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_35C ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_35D ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_35E ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_35F ="//div[@class='reanimate-seat-rows ng-star-inserted'][35]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_36A ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_36B ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_36C ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_36D ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_36E ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_36F ="//div[@class='reanimate-seat-rows ng-star-inserted'][36]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_37A ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_37B ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_37C ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_37D ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_37E ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_37F ="//div[@class='reanimate-seat-rows ng-star-inserted'][37]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_38A ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_38B ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_38C ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[3]";
	public final static String Airbus_321_Standard_Seat_38D ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_38E ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_38F ="//div[@class='reanimate-seat-rows ng-star-inserted'][38]//app-unit[7]";
	public final static String Airbus_321_Standard_Seat_39A ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_39B ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[2]";
	public final static String Airbus_321_Standard_Seat_39C ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[1]";
	public final static String Airbus_321_Standard_Seat_39D ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[5]";
	public final static String Airbus_321_Standard_Seat_39E ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[6]";
	public final static String Airbus_321_Standard_Seat_39F ="//div[@class='reanimate-seat-rows ng-star-inserted'][39]//app-unit[7]";
	
}
