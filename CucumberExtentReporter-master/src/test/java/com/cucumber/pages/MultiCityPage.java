package com.cucumber.pages;

public class MultiCityPage {

	
	public final static String Multi_city_radio_button="//label[@for='radio-multiCity']";
	
	public final static String Multicity_clickson_fromcity_one="//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@id='originStationCode0']";
	
	public final static String Multicity_clickson_Tocity_one="//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@id='destinationStationCode0']";
	
	public final static String Multicity_clickson_fromcity_two="//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@id='originStationCode1']";
	
	public final static String Multicity_clickson_Tocity_two="//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@id='destinationStationCode1']";
	
	public final static String Multicity_date_one="//tab[@class='active tab-pane']//input[@id='date0']";
	
	public final static String Multicity_date_two="//tab[@class='active tab-pane']//input[@id='date1']";
	
	public final static String clickon_passengers = "//input[@name='passengers']";
	
	public final static String Multicicty_select_one_to_city="//app-flight-search-input[1]/div[1]/div[1]//app-station-picker-dropdown[2]//button[contains(text(),' Orlando, FL (MCO) ')]";
	
	public final static String Multicity_select_one__from_city="//app-flight-search-input[1]/div[1]/div[1]//app-station-picker-dropdown[1]//button[contains(text(),' Denver, CO (DEN) ')]";
	
	public final static String Multicity_select_two_TOcity="//app-flight-search-input[1]/div[1]/div[2]//app-station-picker-dropdown[2]//button[contains(text(),' New York, NY - LaGuardia (LGA) ')]";
	
	public final static String Multicity_select_two_fromcity="//app-flight-search-input[1]/div[1]/div[2]//app-station-picker-dropdown[1]//button[contains(text(),' Fort Lauderdale, FL / Miami, FL AREA (FLL) ')]";
	
	
	public final static String Multicity_mouse_over_text="//div[@class='popover-content popover-body']";
	
	
}
