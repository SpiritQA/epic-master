
	package com.cucumber.pages;

	public class NineFCSignUpPage {

	 public final static String NineFc_header="//h1";
	 
	 public final static String NineFc_Logo="//div[@class='row mb-md-5 ng-star-inserted']//img[@src='https://cms10dss.spirit.com/Shared/nine-fc-icon.png']";
	 
	 public final static String NineFc_Join_The_Club_Content1="//div[@class='row mt-3 mt-md-0 ng-star-inserted']//div[1]/p";
	 
	 public final static String NineFc_Join_The_Club_Content2="//div[@class='row mt-3 mt-md-0 ng-star-inserted']//div[2]/p";
	 
	 public final static String NineFc_Join_The_Club_Content3="//div[@class='row mt-3 mt-md-0 ng-star-inserted']//div[3]/p";
	 
	 public final static String NineFc_SignUp_now_Btn="//div[@class='row mb-5 ng-star-inserted']//a";
	 
	 public final static String NineFc_Fly_More_Img="//div[@class='three-up-sell-container ng-star-inserted']//img[@src='https://cms10dss.spirit.com/Shared/_save_more.png']";
	 
	 public final static String NineFc_Bring_More_Img="//div[@class='three-up-sell-container ng-star-inserted']//img[@src='https://cms10dss.spirit.com/Shared/bring_more.png']";
	 
	 public final static String NineFc_Save_More_Img="//div[@class='three-up-sell-container ng-star-inserted']//img[@src='https://cms10dss.spirit.com/Shared/fly_more.png']";
	 
	 public final static String NineFc_Fly_More_Content1="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][1]//h2";
	 
	 public final static String NineFc_Fly_More_Content2="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][1]//p";
	 
	 public final static String NineFc_Bring_More_Content1="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][2]//h2";
	 
	 public final static String NineFc_Bring_More_Content2="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][2]//p";
	 
	 public final static String NineFc_Save_More_Content1="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][3]//h2";
	 
	 public final static String NineFc_Save_More_Content2="//div[@class='three-up-sell-container ng-star-inserted']//div[@class='col-12 col-md-4 text-center'][3]//p";
	 
	 public final static String NineFc_Find_Information_Line1="//div[@class='row mt-5 ng-star-inserted']//p[@class='first_line']";
	 
	 public final static String NineFc_Find_Information_Line2="//div[@class='row mt-5 ng-star-inserted']//center//p[1]";
	 
	 public final static String NineFc_Find_Information_Line3="//div[@class='row mt-5 ng-star-inserted']//center//p[2]";
	 
	 public final static String NineFc_Find_Information_Line4 ="//div[@class='row mt-5 ng-star-inserted']//center//p[3]";
	 
	 public final static String NineFc_Find_Information_Line5="//div[@class='row mt-5 ng-star-inserted']//center//p[4]";
	 
	 public final static String NineFc_Find_Information_Line6="//app-account-enrollment-page[1]/div[@class='row mt-5 ng-star-inserted']/div[1]/p";
	 
	 public final static String NineFc_SignUp_fasterAndEasier_Link="//app-account-enrollment-page[1]/div[@class='row mt-5 ng-star-inserted']/div[1]/p//a";
	 
	 public final static String NineFc_SignUp_fasterAndEasier_PopUp_header="//div[@class='modal-content']//h2";
	 
	 public final static String NineFc_SignUp_Title_label="//div[@class='form-group ng-star-inserted']//label[@for='title']";
	 
	 public final static String NineFc_SignUp_Title_Drp_Mr="//select[@id='title']//option[2]";
	 
	 public final static String NineFc_SignUp_Title_Drp_Mrs="//select[@id='title']//option[3]";
	 
	 public final static String NineFc_SignUp_Title_Drp_Ms="//select[@id='title']//option[4]";
	 
	 public final static String NineFc_SignUp_Account_Tab="//ul[@class='nav nav-pills nav-fill']//li[1]";
	 
	 public final static String NineFc_SignUp_Contact_Tab="//ul[@class='nav nav-pills nav-fill']//li[2]";
	 
	 public final static String NineFc_SignUp_Billing_Tab="//ul[@class='nav nav-pills nav-fill']//li[3]";
	 
	 public final static String NineFc_SignUp_FirstName_label="//div[@class='form-group ng-star-inserted']//label[@for='firstName']";
	 
	 public final static String NineFc_SignUp_MiddleName_label="//div[@class='form-group ng-star-inserted']//label[@for='middleName']";
	 
	 public final static String NineFc_SignUp_LastName_label="//div[@class='form-group ng-star-inserted']//label[@for='lastName']";
	 
	 public final static String NineFc_SignUp_Suffix_label="//div[@class='form-group ng-star-inserted']//label[@for='suffix']";
	 
	 public final static String NineFc_SignUp_Suffix_DrpDown="//div[@class='form-group ng-star-inserted']//select[@id='suffix']";
	 
	 public final static String NineFc_SignUp_Suffix_Drp_Jr="//select[@id='suffix']//option[3]";
	 
	 public final static String NineFc_SignUp_Suffix_Drp_Sr="//select[@id='suffix']//option[4]";
	 
	 public final static String NineFc_SignUp_DateOfBirth_label="//div[@class='form-group ng-star-inserted']//label[@for='dateOfBirth']";
	 
	 public final static String NineFc_SignUp_Important_line1="//div[@class='col-md-6']//p[1]";
	 
	 public final static String NineFc_SignUp_Important_line2="//div[@class='col-md-6']//p[2]";
	 
	 public final static String NineFc_SignUp_Email_label="//div[@class='form-group ng-star-inserted']//label[@for='emailAddress']";
	 
	 public final static String NineFc_SignUp_Confirm_Email_label="//div[@class='form-group ng-star-inserted']//label[@for='confirmEmail']";
	 
	 public final static String NineFc_SignUp_Password_label="//div[@class='form-group ng-star-inserted']//label[@for='password']";
	 
	 public final static String NineFc_SignUp_Confirm_Password_label="//div[@class='form-group ng-star-inserted']//label[@for='confirmPassword']";
	 
	 public final static String NineFc_SignUp_Address_label="//div[@class='form-group ng-star-inserted']//label[@for='address1']";
	 
	 public final static String NineFc_SignUp_Address_Continued_label="//div[@class='form-group ng-star-inserted']//label[@for='address2']";
	 
	 public final static String NineFc_SignUp_Address_Continued_Textbox="//input[@name='address2']";
	 
	 public final static String NineFc_SignUp_City_label="//div[@class='form-group ng-star-inserted']//label[@for='city']";
	 
	 public final static String NineFc_SignUp_State_label="//div[@class='form-group ng-star-inserted']//label[@for='state']";
	 
	 public final static String NineFc_SignUp_Zip_PostalCode_label="//div[@class='form-group ng-star-inserted']//label[@for='postalCode']";
	 
	 public final static String NineFc_SignUp_Country_label="//div[@class='form-group ng-star-inserted']//label[@for='country']";
	 
	 public final static String NineFc_SignUp_PrimaryPhone_label="//div[@class='form-group ng-star-inserted']//label[@for='homePhone']";
	 
	 public final static String NineFc_SignUp_SecondaryPhone_label="//div[@class='form-group ng-star-inserted']//label[@for='workPhone']";
	 
	 public final static String NineFc_SignUp_HomeAirport_label="//div[@class='form-group ng-star-inserted']//label[@for='primaryAirport']";
	 
	 public final static String NineFc_SignUp_SecondaryAirport_label="//div[@class='form-group ng-star-inserted']//label[@for='secondaryAirport']";
	 
	 public final static String NineFc_SignUp_NameOnCard_label="//div[@class='form-group ng-star-inserted']//label[@for='accountHolderName']";
	 
	 public final static String NineFc_SignUp_CardNumber_label="//div[@class='form-group ng-star-inserted']//label[@for='cardNumber']";
	 
	 public final static String NineFc_SignUp_ExpirationDate_label="//div[@class='form-group ng-star-inserted']//label[@for='expMonthYear']";
	 
	 public final static String NineFc_SignUp_SecurityCode_label="//div[@class='form-group ng-star-inserted']//label[@for='securityCode']";

	 
	 public final static String NineFc_SignUp_BillingAddress_Checkbox="//div[@class='custom-control custom-checkbox ng-star-inserted']//label[@for='useSameAddress']";
	 
	 public final static String NineFc_SignUp_Billing_Address_label="//p[contains(text(),'Billing Address')]";
	 
	 public final static String NineFc_SignUp_CC_Address_label="//div[@class='form-group ng-star-inserted']//label[@for='billingAddress']";
	 
	 public final static String NineFc_SignUp_Billing_City_label="//div[@class='form-group ng-star-inserted']//label[@for='billingCity']";
	 
	 public final static String NineFc_SignUp_Billing_State_label="//div[@class='form-group ng-star-inserted']//label[@for='billingState']";
	 
	 public final static String NineFc_SignUp_Billing_Zip_label="//div[@class='form-group ng-star-inserted']//label[@for='billingZipPostal']";
	 
	 public final static String NineFc_SignUp_Billing_Country_label="//div[@class='form-group ng-star-inserted']//label[@for='billingCountry']";
	 
	 public final static String NineFc_SignUp_TermsAndConditions_label="//div[@class='col-9 col-sm-6']//p";
	 
	 public final static String NineFc_SignUp_TermsAndConditions_Content="//label[@for='termsAccepted']";
	 
	 public final static String NineFc_SignUp_TermsAndConditions_CheckBox="//input[@id='termsAccepted']";
	 
	 public final static String NineFc_SignUp_and_Start_Saving_btn="//button[contains(text(),'sign-up and start saving today!')]";
	 
	 
	 
	 
	 
	}



	
	
	
	
	
	
	

