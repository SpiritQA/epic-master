package com.cucumber.pages;

public class PassengerinfoPage {
	public final static String clickon_passengers = "//input[@name='passengers']";

	public final static String select_adults = "//div[@class='d-flex pt-1']//i[@class='icon-add-circle']";

	public final static String select_adult_minus_button = "//div[@class='d-flex pt-1']//i[@class='icon-minus-circle']";

	public final static String select_child = "//div[@class='d-flex']//i[@class='icon-add-circle']";

	public final static String child_month = "//select[@id='month']";

	public final static String child_day = "//select[@id='day']";

	public final static String child_year = "//select[@id='year']";

	public final static String child_popup_continue_button = "//div[@class='col-12 text-center']/button[contains(text(),'Continue')]";

	public final static String free_spirit_login = "//button[text()='Log In']";

	public final static String free_spirit_email = "//input[@name='username']";

	public final static String free_spirit_password = "//input[@name='password']";

	public final static String login_account_button = "//button[ text()=' Log In']";

	public final static String adullt_Title = "//div//section[1]//select[@id='title']";

	public final static String adult_passengers_first_name = "//div//section[1]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_middle_name = "//div//section[1]//input[@name='middleName']";

	public final static String adult_passengers_last_name = "//div//section[1]//input[@name='lastName']";

	public final static String adult_passeneger_suffix = "//div//section[1]//select[@name='suffix']";

	public final static String adult_passengers_dateof_birth = "//div//section[1]//input[@name='dateOfBirth']";

	public final static String primary_passenger_contact_checkbox = "//label[@for='check-usePrimaryAsContactName']";

	public final static String adult_known_traveler_number = "//div//section[1]//input[@name='knownTravelerNumber']";

	public final static String adult_Free_spirit_memeber = "//div//section[1]//input[@name='accountNumber']";

	public final static String adult_Additional_services = "//div//section[1]//a[contains(text(),' Additional Services')]";

	public final static String adult_Hearing_impaired_Checkbox = "//div//section[1]//label[@for='customCheckhearing-impaired0']";

	public final static String adult_vision_disability_Checkbox = "//div//section[1]//label[@for='customCheckvision-disability0']";

	public final static String adult_pet_in_cabin_Checkbox = "//div//section[1]//label[@for='customCheckpet-in-cabin0']";

	public final static String adult_ServiceAnimal_CheckBox = "//div//section[1]//label[@for='customCheckservice-animal0']";

	public final static String adult_Emotional_checkbox = "//div//section[1]//label[@for='customCheckemotional-support0']";

	public final static String adult_portable_oxygen = "//div//section[1]//label[@for='customCheckportable-oxygen0']";

	public final static String adult_other_checkbox = "//div//section[1]//label[@for='customCheckpage.other0']";

	public final static String adult_redress_number = "//div//section[1]//a[contains(text(),'Redress Number ')]";

	public final static String adult_redress_input_box = "//div//section[1]//input[@name='redressNumber']";

	public final static String adult_Voluntry_provision_caret = "//div//section[1]//a[contains(text(),'Voluntary Provision')]";

	public final static String adult_voluntry_provision_checkbox = "//div//section[1]//label[@for='customCheckpage.is-volunteer0']";

	public final static String adult_ActiveDutyMilitaryPersonnelCheckBox = "//div//section[1]//label[@for='isMilitary0']";

	public final static String adult_ompletely_immobile = "//div//section[1]//label[@for='customCheckhas-wheel-chair-completely-immobile0']";

	public final static String adult_own_wheel_chair = "//div//section[1]//label[@for='customCheckhas-wheel-chair-has-own0']";
	public final static String adult_wheel_chir_dropdown = "//div//section[1]//select[@name='wheelChairOwn']";

	public final static String adult_Need_help_to_from_gate = "//div//section[1]//label[@for='customCheckhas-wheel-chair0']";

	public final static String Invalid_Email_or_Password_alert = "//div[@class='alert alert-danger mt-3 ng-star-inserted']";

	public final static String Email_required_alert = "//div[@class='col-12'][1]//div[@class='s-error-text ng-star-inserted']";

	public final static String Password_required_alert = "//div[@class='col-12'][2]//div[@class='s-error-text ng-star-inserted']']";

	public final static String free_spirit_check_button = "//label[@for='fSEnrollCheckbox']";
	public final static String LapChild_Radio_Button = "//label[@for='child1LapOption']";
	// child info
	public final static String child_title = "//div//section[2]//select[@name='title']";

	public final static String infant_firstname = "//div//section[2]//input[@name='first']";
	public final static String infant_lastname = "//div//section[2]//input[@name='last']";

	public final static String child_firstname = "//div//section[2]//input[@name='firstName']";

	public final static String child_lastname = "//div//section[2]//input[@name='lastName']";

	public final static String child_knowntraveler_number = "//div//section[2]//input[@name='knownTravelerNumber']";

	public final static String child_middlename = "//div//section[2]//input[@name='middleName']";

	public final static String child_free_spirit = "//div//section[2]//input[@name='number']";

	public final static String child_active_duty_militery = "//div//section[2]//label[@for='hasCarSeat1']";

	public final static String child_passenger_suffix = "//div//section[2]//select[@name='suffix']";

	public final static String child_dateofbirth = "//div//section[2]//input[@name='dateOfBirth']";

	public final static String child_additional_services = "//div//section[2]//a[contains(text(),'Additional Services')]";

	public final static String child_hearig_imaired = "//div//section[2]//label[@for='customCheckhearing-impaired1']";

	public final static String child_vision_disability = "//div//section[2]//label[contains(text(),'Vision Disability')]";

	public final static String child_petin_cabin = "//div//section[2]//label[contains(text(),'Pet In Cabin')]";

	public final static String child_service_animal = "//div//section[2]//label[contains(text(),'Service Animal')]";

	public final static String child_psychiatric_support_animal = "//div//section[2]//label[contains(text(),'Emotional/Psychiatric Support Animal')]";

	public final static String child_portable_oxygen = "//div//section[2]//label[contains(text(),'Portable Oxygen')]";

	public final static String child_other = "//div//section[2]//label[contains(text(),'Other (CPAP, nebulizer, ventilator etc.)')]";

	public final static String child_needhelp_tofrom_gate = "//div//section[2]//label[@for='customCheckhas-wheel-chair1']";

	public final static String child_completely_immobile = "//div//section[2]//label[@for='customCheckhas-wheel-chair-completely-immobile1']";

	public final static String child_myown_wheel_chair = "//div//section[2]//label[@for='customCheckhas-wheel-chair-has-own1']";

	public final static String child_redress_number = "//div//section[2]//:a[contains(text(),'Add Redress Number')]";

	public final static String child_redress_input_field = "//div//section[2]//input[@name='redressNumber']";

	public final static String child_voluntry_provision = "//div//section[2]//a[contains(text(),'Voluntary Provision')]";

	public final static String child_voluntry_provision_checkbox = "//div//section[2]//label[@for='customCheckpage.is-volenteer1']";
	public final static String CarSeat = "//Label[@for='hasCarSeat1']";

	public final static String passengers_address = "//input[@id='address']";

	public final static String passengers_city = "//input[@id='city']";

	public final static String passengers_state = "//select[@name='provinceState']";

	public final static String zip_code = "//input[@id='postalCode']";

	public final static String PAX_Two_Adult_Title_Field = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[2]//div[2]//app-input[1]//div[1]//select[1]";
	public final static String PAX_Two_Adult_Last_Name_Field = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[2]//div[5]//app-input[1]//div[1]//input[1]";

	public final static String PAX_Three_Child_Title_Field = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[2]//div[2]//app-input[1]//div[1]//select[1]";

	public final static String PAX_Three_Child_First_Name_Field = "//body/app-root[@ng-version='6.1.4']/main/div[@class='container']/app-book-path[@class='ng-star-inserted']/div[@class='router-outlet']/app-passengers-page-information[@class='ng-star-inserted']/section[@class='ng-star-inserted']/form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']/section[3]/div[2]/div[3]/app-input[1]/div[1]/input[1]";

	public final static String PAX_Three_Child_First_Last_Field = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[2]//div[5]//app-input[1]//div[1]//input[1]";
	
	public final static String PAX_Three_Child_Date_Of_Birth_Field ="//div//section[3]//input[@id='dateOfBirth']";

	public final static String PAX_Two_Adult_Date_Of_Birth_Field = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[3]//div[1]//app-input[1]//div[1]//input[1]";

	public final static String PAX_One_Adult_Free_Spirit_Number_Field = "//div//section[@class='mb-2 ng-star-inserted'][1]//input[@id='number'][1]";
	public final static String PAX_Two_Adult_First_Name_Field = "//body/app-root[@ng-version='6.1.4']/main/div[@class='container']/app-book-path[@class='ng-star-inserted']/div[@class='router-outlet']/app-passengers-page-information[@class='ng-star-inserted']/section[@class='ng-star-inserted']/form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']/section[2]/div[2]/div[3]/app-input[1]/div[1]/input[1]";

	public final static String country = "//select[@id='countryCode']";

	public final static String passengers_email = "//input[@name='contactEmailPrimary']";

	public final static String Passengers_phone = "//input[@id='phoneNumber']";

	public final static String passenerspage_Continue_button = "//button[contains(text(),'continue')]";

	public final static String conformation_page_popup = "//a[@aria-label='close dialog']";
	// checking path
	public final static String PNR_CONFORMATION = "//div[@class='col-md-4 d-flex align-items-center justify-content-end']//p";

	public final static String checkin_mainmenu = "//div[@class='desktop']//a[@aria-label='Check In']";

	public final static String checkin_additional_info = "//div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-valid'][1]//a[contains(text(),' Additional Info ')]";

	public final static String checkin_additional_info_passenger_two = "//div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-valid'][2]//a[contains(text(),' Additional Info')]";

	public final static String checkin_Heraing_disability = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='isHearingImpaired']";

	public final static String checkin_heraing_disability_passenger2 = "//div[@class='pt-3 collapse ng-star-inserted ng-pristine ng-valid in show ng-touched']//input[@name='isHearingImpaired']";

	public final static String checkin_service_animal = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasServiceAnimal']";

	public final static String checkin_POC = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasPortableOxygen']";

	public final static String checkin_visiondisability = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasVisionDisability']";

	public final static String checkin_visiondisability_passenger_two = "//div[@ngmodelgroup='passengers']/div[2]//input[@name='hasVisionDisability']";

	public final static String checkin_emotional = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasEmotionalSupportAnimal']";

	public final static String checkin_primarypassenger_other = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasOther']";

	public final static String checkin_petin_cabin = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasPetInCabin']";

	public final static String checkinwheelchair_to_from_gate = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasWheelchairToFromGate']";

	public final static String wheelchair_need_help_from_gate = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasWheelchairCustomerAisleChair']";

	public final static String checkin_completely_immobile = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasWheelchairStraightBack']";

	public final static String checkin_has_own_wheelchair = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='hasWheelchair']";

	public final static String checkin_disability_seating = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='disabilitySeating']";

	public final static String checkin_wheelchair_selection = "//select[@name='wheelChairOwn']";

	public final static String chekin_primarypassenger_save = " //div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-valid'][1]/preceding::button[text()=' save '][1]";

	public final static String checkin_passenger_two_add_button1 = "//div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-invalid'][1]/following::button[text()='Add'][1]";

	public final static String checkin_passenger_two_add_button2 = "//div[@class='row mb-3 ng-star-inserted ng-untouched ng-pristine ng-invalid'][1]/following::button[text()='Add'][2]";

	public final static String checkin_passenger_two_knowntraveler_inputbox = "//div[@ngmodelgroup='passengers']/div[1]/following::input[@id='ktnNumber']";

	public final static String checkin_passenger_two_knowntraveler_errormessage = "//div[contains(text(),'A KTN value must be 1-26 characters long and alphanumeric only.')]";

	public final static String checkin_passenger_two_knowntraveler_save_button = "//div[@class='row collapse in show'][1]/following::button[contains(text(),'SAVE')][1]";

	public final static String checkin_primarypassenger_cancel = "//div[@class='pt-3 collapse ng-star-inserted ng-valid ng-dirty ng-touched in show']//button[contains(text(),'CANCEL')]";

	public final static String checkin_primarypassenger_voluntary_provision = "//div[@class='pt-3 collapse ng-star-inserted ng-valid ng-dirty ng-touched in show']//input[@name='isVolunteer']";

	public final static String checkin_primarypassenger_disabllity_seating = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='disabilitySeating']";

	public final static String checkin_primarypassenger_disablity_seating_aisel_arrest = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='disabilityMoveableAisleArmrest']";

	public final static String checkin_primarypassenger_disability_seating_bendleg = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='disabilityImmobileLeg']";

	public final static String checkin_primarypassenger_disability_seating_traveling_withme = "//div[@ngmodelgroup='passengers']/div[1]//input[@name='disabilityTravelingWithMeSeat']";

	public final static String adult_passeneger2_suffix = "//div//section[2]//select[@name='suffix']";

	public final static String mytrips_mainmenu = "//li[@class='nav1D']//a[@aria-label='My Trips'][contains(text(),'My Trips')]";

	// in-line enrollment
	public final static String join_banner_checkbox = "//label[contains(text(),'Yes, I want to become a FREE SPIRIT™ member.')]";

	// passenger 3
	public final static String adullt_three_Title = "//div//section[3]//select[@id='title']";
	public final static String adult_passengers_three_first_name = "//div//section[3]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_three_middle_name = "//div//section[3]//input[@name='middleName']";

	public final static String adult_passengers_three_last_name = "//div//section[3]//input[@name='lastName']";

	public final static String adult_passengers_three_dateof_birth = "//div//section[3]//input[@name='dateOfBirth']";

	// passenger four
	public final static String adullt_four_Title = "//div//section[4]//select[@id='title']";
	public final static String adult_passengers_four_first_name = "//div//section[4]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_four_middle_name = "//div//section[4]//input[@name='middleName']";

	public final static String adult_passengers_four_last_name = "//div//section[4]//input[@name='lastName']";

	public final static String adult_passengers_four_dateof_birth = "//div//section[4]//input[@name='dateOfBirth']";

	public final static String passneger_four_adding_sport_equipment = "//section//div[4]/div[2]//app-chevron/i";

	public final static String passenger_four_plus_surfboard = "//div[@class='flight-bag-container mb-4 ng-star-inserted']//div[4]//div[3]//div[3]//app-count-picker[1]//i[2]";

	// passenger five
	public final static String adullt_five_Title = "//div//section[5]//select[@id='title']";
	public final static String adult_passengers_five_first_name = "//div//section[5]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_five_middle_name = "//div//section[5]//input[@name='middleName']";

	public final static String adult_passengers_five_last_name = "//div//section[5]//input[@name='lastName']";

	// passenger six
	public final static String adullt_six_Title = "//section[@class='mb-2 ng-star-inserted'][6]//div//select[@name='title']";
	public final static String adult_passengers_six_first_name = "//section[@class='mb-2 ng-star-inserted'][6]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_six_middle_name = "//section[@class='mb-2 ng-star-inserted'][6]//input[@name='middleName']";

	public final static String adult_passengers_six_last_name = "//section[@class='mb-2 ng-star-inserted'][6]//input[@name='lastName']";

	public final static String adult_passengers_six_dateof_birth = "//section[@class='mb-2 ng-star-inserted'][6]//input[@name='dateOfBirth']";

	// passenger seven
	public final static String adullt_seven_Title = "//section[@class='mb-2 ng-star-inserted'][7]//div//select[@name='title']";
	public final static String adult_passengers_seven_first_name = "//section[@class='mb-2 ng-star-inserted'][7]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_seven_middle_name = "//section[@class='mb-2 ng-star-inserted'][7]//input[@name='middleName']";

	public final static String adult_passengers_seven_last_name = "//section[@class='mb-2 ng-star-inserted'][7]//input[@name='lastName']";

	public final static String adult_passengers_seven_dateof_birth = "//section[@class='mb-2 ng-star-inserted'][7]//input[@name='dateOfBirth']";

	// passenger eight
	public final static String adullt_eight_Title = "//section[@class='mb-2 ng-star-inserted'][8]//div//select[@name='title']";
	public final static String adult_passengers_eight_first_name = "//section[@class='mb-2 ng-star-inserted'][8]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_eight_middle_name = "//section[@class='mb-2 ng-star-inserted'][8]//input[@name='middleName']";

	public final static String adult_passengers_eight_last_name = "//section[@class='mb-2 ng-star-inserted'][8]//input[@name='lastName']";

	public final static String adult_passengers_eight_dateof_birth = "//section[@class='mb-2 ng-star-inserted'][8]//input[@name='dateOfBirth']";

	// passenger nine
	public final static String adullt_nine_Title = "//section[@class='mb-2 ng-star-inserted'][9]//div//select[@name='title']";
	public final static String adult_passengers_nine_first_name = "//section[@class='mb-2 ng-star-inserted'][9]//input[@id='firstName' and @name='firstName']";

	public final static String adult_passengers_nine_middle_name = "//section[@class='mb-2 ng-star-inserted'][9]//input[@name='middleName']";

	public final static String adult_passengers_nine_last_name = "//section[@class='mb-2 ng-star-inserted'][9]//input[@name='lastName']";

	public final static String adult_passengers_nine_dateof_birth = "//section[@class='mb-2 ng-star-inserted'][9]//input[@name='dateOfBirth']";
	public final static String adullt_Title1 = "//section[@class='mb-2 ng-star-inserted'][1]//div//select[@name='title']";

	public final static String adult_passengers_five_dateof_birth = "//div//section[5]//input[@name='dateOfBirth']";

	public final static String Primary_passenger_chk = "//div[@class='custom-control custom-checkbox ng-star-inserted']//label[contains(text(),'The primary passenger is the contact person')]";

	public final static String child2_title = "//section[3][@class='ng-star-inserted']//select[@name='title' and @id='title']";

	public final static String child3_title = "//section[4][@class='ng-star-inserted']//select[@name='title' and @id='title']";
	public final static String child2_firstname = "//section[3][@class='ng-star-inserted']//input[@name='firstName']";

	public final static String child3_firstname = "//section[4][@class='ng-star-inserted']//input[@name='firstName']";
	public final static String child2_lastname = "//section[3][@class='ng-star-inserted']//input[@name='lastName']";

	public final static String child3_lastname = "//section[3][@class='ng-star-inserted']//input[@name='lastName']";
	public final static String child2_dateofbirth = "//section[3][@class='ng-star-inserted']//input[@name='dateOfBirth']";

	public final static String child3_dateofbirth = "//section[4][@class='ng-star-inserted']//input[@name='dateOfBirth']";

	public final static String adullt2_Title = "//div//section[2]//select[@id='title']";
	public final static String adult2_passengers_first_name = "//div//section[2]//input[@id='firstName' and @name='firstName']";
	public final static String adult2_passengers_middle_name = "//div//section[2]//input[@name='middleName']";
	public final static String adult2_passengers_last_name = "//div//section[2]//input[@name='lastName']";
	public final static String adult2_passengers_dateof_birth = "//div//section[2]//input[@name='dateOfBirth']";
	public final static String adult2_ActiveDutyMilitaryPersonnelCheckBox = "//section[2]/div[4]/div[2]/app-input//label";

	public final static String Primary_driver = "//div/section[2]//select[@name='Primary driver']";

	public final static String adult_Need_help_to_or_from_gate = "//div//section[1]//label[@for='customCheckhas-wheel-chair-aisle-chair0']";
	public final static String Completely_immobile = "//div//section[1]//label[@for='customCheckhas-wheel-chair-completely-immobile0']";
	public final static String Vacations_Child_seat_option = "//label[contains(text(),'Will require a seat.')]";

	public final static String adult_Need_help_to_from_Seat = "//label[@for='customCheckhas-wheel-chair-aisle-chair0']";

	public final static String inline_login_block = "//div[@class='row alternate-login mb-2 ng-star-inserted']";

	public final static String inline_enrollment_block = "//section[@ngmodelgroup='freeSpirit']";

	public final static String Passenger_resetpassword_link = "//a[@id='aria-emailHelp']";
	public final static String RetrievePassword_FSNumber = "//input[@id='emailOrNumberText']";
	public final static String RetrievePassword_sendbtn = "//button[@name='sendButton']";
	public final static String RetrievePassword_FSNumber_err = "//div[@class='s-error-text ng-star-inserted']";
	public final static String RetrievePassword_success_msg = "//div[@class='row alert alert-success ng-star-inserted']";
	public final static String Password_Reset_popup_content = "//div[@class='modal-body']//p[contains(text(),'Your temporary password has been emailed to you')]";
	public final static String Password_Reset_popup_close = "//i[@class='icon-close']";
	public final static String Password_Reset_popup_returnlogin = "//button[contains(text(),'Go To Login Page')]";
	public final static String Password_Reset_popup_outsidefade = "//modal-container[@class='modal fade show']";
	public final static String Passenger_edit_name_link = "//button[@class='btn btn-link' and contains(text(),'Why can')]";
	public final static String Passenger_edit_popup_header = "//div[@class='modal-header']";
	public final static String Passengername_edit_popup_content = "//div[@class='row text-center']//p[1]";
	public final static String Passengername_edit_popup_logout = "//button[@class='btn btn-primary' and contains(text(),'Log Out')]";
	public final static String Passengername_edit_popup_close_link = "//div[@class='modal-body']//div[3]//a[contains(text(),'Close')]";
	public final static String Passengername_edit_popup_closebtn = "//i[@class='icon-close']";
	public final static String adult2_free_spirit_number = "//section[@class='mb-2 ng-star-inserted'][2]//input[@id='number']";
	public final static String passengerpage_alert = "//div[@class='toast-message ng-star-inserted']";

	public final static String Passengers_State_Foreign_Country_Field = "//input[@name='provinceState']";

	public final static String Passengers_State_Foreign_Country_Only_Letters_Are_Allowed_Text = "//div[contains(text(),'Only letters are allowed')]";

	public final static String PAX_Three_Child_Additonal_Services_Other_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_Three_Child_Additonal_Services_Other_Tooltip_Popup_More_Information_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_Three_Child_ActiveDutyMP_Tooltip = "Does Not Exist";

	public final static String PAX_Three_Child_Traveling_With_A_CarSeat_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[4]//div[2]//span[1]//i[1]";

	public final static String PAX_Three_Child_Traveling_With_A_CarSeat_Tooltip_Popup = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[3]//div[4]//div[2]//bs-tooltip-container[1]//div[2]//div[1]//div[1]//p[2]";

	public final static String PAX_One_Adult_KTN_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[1]//div[3]//div[2]//app-input[1]//div[1]//i[1]";

	public final static String PAX_One_Adult_KTN_Tooltip_Popup = "//p[@class='p-small']//strong";

	public final static String PAX_One_Adult_KTN_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More Information')]";

	public final static String PAX_One_Adult_FreeSpiritNumber_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[1]//div[3]//div[3]//app-input[1]//div[1]//i[1]";

	public final static String PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup = "//p[@class='p-small']//strong";

	public final static String PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link = "//a[contains(text(),'Sign up')]";

	public final static String PAX_One_Adult_ActiveDutyMP_Tooltip = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[1]//div[4]//div[2]//span[1]//i[1]";

	public final static String PAX_One_Adult_ActiveDutyMP_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_One_Adult_ActiveDutyMP_Tooltip_Popup_Sign_Up_Now_Link = "//a[@class='btn btn-link text-capitalize ng-star-inserted']";

	public final static String PAX_One_Adult_Additonal_Services_Caret = "//app-chevron[@class='ng-tns-c6-5']//i[@class='icon-chevron_up color-action ng-trigger ng-trigger-openState']";

	public final static String PAX_One_Adult_Additonal_Services_Emotional_Support_Animal = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//div[2]//div[2]//app-input[1]//div[1]//label[1]";

	public final static String PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip = "//app-special-assistance-ssrs-input[@class='collapse in show']//div[@class='row']//div[2]//span[1]//i[1]";

	public final static String PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_One_Adult_Additonal_Services_Other_ = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//label[@class='ng-star-inserted custom-control-label'][contains(text(),'Other (CPAP, nebulizer, ventilator, respirator, ca')]";

	public final static String PAX_One_Adult_Additonal_Services_Other_Tooltip = "//app-special-assistance-ssrs-input[@class='collapse in show']//div[@class='row']//div[3]//span[1]//i[1]";

	public final static String PAX_One_Adult_Additonal_Services_Other_Tooltip_Popup = "//span[@class='ng-star-inserted']";

	public final static String PAX_One_Adult_Additonal_Services_Other_Tooltip_Popup_More_Information_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_One_Adult_Additonal_Services_Voluntary = "//div[contains(text(),'Voluntary Provision of Emergency Services Program')]";

	public final static String PAX_One_Adult_Additonal_Services_Voluntary_Tooltip = "//div[@class='row pt-3 ng-star-inserted']//i[@class='icon-info-circle']";

	public final static String PAX_One_Adult_Additonal_Services_Voluntary_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_One_Adult_Additonal_Services_Voluntary_Tooltip_Popup_More_Information_Link = "//a[contains(text(),'More Information')]";

	public final static String PAX_Two_Adult_KTN_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[3]//div[2]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Two_Adult_KTN_Tooltip_Popup = "";

	public final static String PAX_Two_Adult_KTN_Tooltip_Popup_More_Info_Link = "";

	public final static String PAX_Two_Adult_FreeSpiritNumber_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[3]//div[3]//app-input[1]//div[1]//i[1]";

	public final static String Pax_Two_Adult_Additional_Services_Caret = "";

	public final static String PAX_Two_Adult_ActiveDutyMP_Tooltip = "";

	public final static String PAX_Two_Child_KTN_Tooltip = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[2]//div[3]//div[2]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Two_Child_KTN_Tooltip_Popup = "//div[@class='row ng-star-inserted']//div[@class='col-12']//p[@class='p-small']";

	public final static String PAX_Two_Child_KTN_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More Information')]";

	public final static String PAX_Two_Child_FreeSpiritNumber_Tooltip = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[2]//div[3]//div[3]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup = "//div[@class='row ng-star-inserted']//div[@class='col-12']//p[@class='p-small']";

	public final static String PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link = "//a[contains(text(),'Sign up')]";

	public final static String Pax_Two_Child_Additional_Services_Caret = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[3]//div[4]//div[1]//div[1]//div[1]//a[1]";

	public final static String PAX_Two_Child_Additonal_Services_Emotional_Support_Animal = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//div[2]//div[2]//app-input[1]//div[1]//label[1]";

	public final static String PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//div[@class='row']//div[2]//span[1]//i[1]";

	public final static String PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_Two_Child_Additonal_Services_Other_ = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//label[@class='ng-star-inserted custom-control-label'][contains(text(),'Other (CPAP, nebulizer, ventilator, respirator, ca')]";

	public final static String PAX_Two_Child_Additonal_Services_Other_Tooltip = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//label[@class='ng-star-inserted custom-control-label'][contains(text(),'Other (CPAP, nebulizer, ventilator, respirator, ca')]";

	public final static String PAX_Two_Child_Additonal_Services_Other_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_Two_Child_Additonal_Services_Other_Tooltip_Popup_More_Information_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_Two_Child_ActiveDutyMP_Tooltip = "Does Not Exist";

	public final static String PAX_Two_Child_Traveling_With_A_CarSeat_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[2]//div[4]//div[2]//span[1]//i[1]";

	public final static String PAX_Two_Child_Traveling_With_A_CarSeat_Tooltip_Popup = "//form[@class='ng-untouched ng-star-inserted ng-dirty ng-invalid']//section[2]//div[4]//div[2]//bs-tooltip-container[1]//div[2]//div[1]//div[1]//p[2]";

	public final static String PAX_Three_Adult_KTN_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[3]//div[2]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Three_Adult_FreeSpiritNumber = "";

	public final static String PAX_Three_Adult_FreeSpiritNumber_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[3]//div[3]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Three_Adult_ActiveDutyMP_Tooltip = "";

	public final static String PAX_Three_Child_KTN_Tooltip = "//form[@class='ng-star-inserted ng-dirty ng-invalid ng-touched']//section[3]//div[3]//div[2]//app-input[1]//div[1]//i[1]";

	public final static String PAX_Three_Child_KTN_Tooltip_Popup = "//div[@class='row ng-star-inserted']//div[@class='col-12']//p[@class='p-small']";

	public final static String PAX_Three_Child_KTN_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More Information')]";

	public final static String PAX_Three_Child_FreeSpiritNumber = "//div[@class='col-12 col-lg-5 extra-info-tip ng-pristine ng-valid ng-touched']//input[@id='number']";

	public final static String PAX_Three_Child_FreeSpiritNumber_Tooltip = "//div[@class='col-12 col-lg-5 extra-info-tip ng-pristine ng-valid ng-touched']//i[@class='icon-info-circle ng-star-inserted']";

	public final static String PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup = "//div[@class='row ng-star-inserted']//div[@class='col-12']//p[@class='p-small']";

	public final static String PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link = "//a[contains(text(),'Sign up')]";

	public final static String Pax_Three_Child_Additional_Services_Caret = "//app-chevron[@class='ng-tns-c6-22']//i[@class='icon-chevron_up color-action ng-trigger ng-trigger-openState']";

	public final static String PAX_TThree_Child_Additonal_Services_Emotional_Support_Animal = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//div[2]//div[2]//app-input[1]//div[1]//label[1]";

	public final static String PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip = "//app-special-assistance-ssrs-input[@class='collapse in show']//div[@class='row']//div[2]//span[1]//i[1]";

	public final static String PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup = "//div[@class='popover-content popover-body']";

	public final static String PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link = "//a[contains(text(),'More information')]";

	public final static String PAX_Three_Child_Additonal_Services_Other_ = "//section[@class='ng-star-inserted ng-valid ng-dirty ng-touched']//label[@class='ng-star-inserted custom-control-label'][contains(text(),'Other (CPAP, nebulizer, ventilator, respirator, ca')]";

	public final static String PAX_Three_Child_Additonal_Services_Other_Tooltip = "//app-special-assistance-ssrs-input[@class='collapse in show']//div[@class='row']//div[3]//span[1]//i[1]";

	public final static String Join_Free_Spirit_Yellow_Banner = "//section//div[@class='col-12'][contains(text(),\"Join \")]";
	public final static String Join_Free_Spirit_Yes_Check_Box = "//div[@class='row terms-passenger']//label[@class='ng-star-inserted custom-control-label']";

	public final static String Join_Free_Spirit_Select_A_Valid_Password = "//input[@id='passwordPrimary']";
	public final static String Join_Free_Spirit_Confirm_A_Valid_Password = "//input[@id='passwordConfirmed']";
	public final static String Passengers_State_Label_Title = "//label[contains(text(),'State')]";
	public final static String Zip_Code_Label_Title = "//label[contains(text(),'Zip / Postal Code')]";
	public final static String Free_Spirit_Wring_Number_Error_Message = "//div[@aria-live='polite']";

	public final static String PAX_Info_Page_Young_Traveler_Popup_Header ="//h2[contains(text(),'Young Traveler')]";
    public final static String PAX_Info_Page_Young_Traveler_Popup_Message ="//p[contains(text(),' It appears there is a child under the age of 15')]";
    public final static String PAX_Info_Page_Young_Traveler_Popup_Close ="//a[contains(text(),'Close')]";
    public final static String PAX_Info_Page_Young_Traveler_Popup_Return_To_Homepage_Button ="//button[contains(text(),'Return to Homepage ')]";
    
    public final static String Country_Code_United_States_of_America ="//select[@id='countryCode']//option[contains(text(),'United States of America')]";
    
	public final static String Phone_Country_Code ="//select[@name='phoneCountryType']";
	
	public final static String PAX_One_Adult_Date_Of_Birth ="//input[@id='dateOfBirth']";
	
	public final static String Join_Free_Spirit_Invalid_Password_Error_Message ="//div[@class='s-error-text ng-star-inserted'][contains(text(),'Password must be at least 8 characters')]";
	
	public final static String PAX_One_Adult_KTN_Field ="//input[@id='knownTravelerNumber']";
}
