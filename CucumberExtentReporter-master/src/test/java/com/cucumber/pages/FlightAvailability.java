package com.cucumber.pages;

public class FlightAvailability {

	public final static String Nonstop_button = "//div[@class='d-flex flex-column']//div[1]//app-journey[1]//div[2]//div[2]//button[contains(text(),'Nonstop')]";

	public final static String One_stop = "//div[@class='d-flex flex-column']//div[6]//app-journey[1]//div[1]//div[2]//div[2]//button[1]";

	public final static String two_stops = "//div[@class='d-flex flex-column']//div[8]//app-journey[1]//div[1]//div[2]//div[2]//button[1]";

	public final static String Three_stops = "//div[@class='col-2 d-flex align-items-center']//button[@type='button'][contains(text(),'3 stops')]";

	public final static String Flight_Number_popup = "//div[@class='triangle-count' and contains(text(),'1')]//following::div[@class='col-5 pt-2'][1]//p";

	public final static String Airbus_number_flight_one = "//div[@class='triangle-count' and contains(text(),'1')]//following::div[@class='col-5 pt-2'][2]//p";

	public final static String Flight_number_popup_onestop_flight_two = "//div[@class='triangle-count' and contains(text(),'2')]//following::div[@class='col-5 pt-2'][1]//p";

	public final static String Airbus_Number_one_stop_flight_two = "//div[@class='triangle-count' and contains(text(),'2')]//following::div[@class='col-5 pt-2'][2]//p";

	public final static String Flight_information_popup = "//h2[contains(text(),'Flight Information')]";

	public final static String flight_ON_Time = "//div[@class='triangle-count' and contains(text(),'1')]//following::div[@class='col-12'][1]//p[2]";

	public final static String flight_ON_Time_one_stop_flight_two = "//div[@class='triangle-count' and contains(text(),'2')]//following::div[@class='col-12'][1]//p[2]";

	public final static String Flight_late = "//div[@class='triangle-count' and contains(text(),'1')]//following::div[@class='col-12'][3]//p[2]";

	public final static String flight_late_one_stop_flight_two = "//div[@class='triangle-count' and contains(text(),'2')]//following::div[@class='col-12'][3]//p[2]";

	public final static String Flight_actual_time = "//div[@class='triangle-count' and contains(text(),'1')]//following::div[@class='col-12'][1]//p[2]";

	public final static String flight_actual_time_one_stop_flight_two = "//div[@class='triangle-count' and contains(text(),'2')]//following::div[@class='col-12'][1]//p[2]";

	public final static String flight_info_close_button = "//button[contains(text(),'Close')]";

	public final static String Flight_number_popup_two_stop_flight_three = "//div[@class='triangle-count' and contains(text(),'3')]//following::div[@class='col-5 pt-2'][1]//p";

	public final static String Airbus_Numner_two_stop_flight_three = "//div[@class='triangle-count' and contains(text(),'3')]//following::div[@class='col-5 pt-2'][2]//p";

	public final static String flight_on_time_two_stop_flight_three = "//div[@class='triangle-count' and contains(text(),'3')]//following::div[@class='col-12'][2]//p[2]";

	public final static String flight_late_two_stop_flight_three = "//div[@class='triangle-count' and contains(text(),'3')]//following::div[@class='col-12'][3]//p[2]";

	public final static String flight_actual_time_two_stop_flight_three = "//div[@class='triangle-count' and contains(text(),'3')]//following::div[@class='col-12'][1]//p[2]";

	public final static String Flight_number_popup_three_stop_flight_four = "//div[@class='triangle-count' and contains(text(),'4')]//following::div[@class='col-5 pt-2'][1]//p";

	public final static String Airbus_Numner_three_stop_flight_four = "//div[@class='triangle-count' and contains(text(),'4')]//following::div[@class='col-5 pt-2'][2]//p";

	public final static String flight_on_time_three_stop_flight_four = "//div[@class='triangle-count' and contains(text(),'4')]//following::div[@class='col-12'][2]//p[2]";

	public final static String flight_late_three_stop_flight_four = "//div[@class='triangle-count' and contains(text(),'4')]//following::div[@class='col-12'][3]//p[2]";

	public final static String flight_actual_time_three_stop_flight_four = "//div[@class='triangle-count' and contains(text(),'4')]//following::div[@class='col-12'][1]//p[2]";

	public final static String Flight_availanbility_New_search_button = "//button[contains(text(),' New Search ')]";

	public final static String flight_availability_Roundtrip_radio_button = "//label[@for='radio-roundTrip']";

	public final static String flight_availability_onewaytrip_radio_button = "//label[@for='radio-oneWay']";

	public final static String flight_availability_Multicity_radio_button = "//label[@for='radio-multiCity']";

	public final static String flight_availability_From_city_click = "//input[@name='originStationCode']";

	public final static String flight_availability_TO_city_click = "//input[@name='destinationStationCode']";

	public final static String Flight_availability_select_from_city = "//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),'New York, NY - LaGuardia (LGA)')]";

	public final static String Flight_availability_select_To_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),' San Diego, CA (SAN) ')]";

	public final static String Flight_availability_select_date = "//input[@id='date']";

	public final static String Flight_availability_click_passengers = "//input[@id='passengers']";

	public final static String Flight_availability_clicks_adlult_minus_button = "//div[@class='ng-tns-c22-15']//div[@class='d-flex pt-1'][1]//div[contains(text(),'Adults')][1]//following::i[@class='icon-minus-circle'][1]";

	public final static String Flight_availability_clicks_adlult_Plus_button = "//div[@class='ng-tns-c22-15']//div[@class='d-flex pt-1'][1]//div[contains(text(),'Adults')][1]//following::i[@class='icon-add-circle'][1]";

	public final static String Flight_availability_clicks_child_Plus_button = "//div[@class='ng-tns-c22-15']//div[@class='d-flex'][1]//div[contains(text(),'Children')][1]//following::i[@class='icon-add-circle'][1]";

	public final static String Flight_availability_promo_code = "//a[contains(text(),' Add a Promo Code ')]";

	public final static String Flight_availability_tip_tool = "//i[@class='icon-question-circle']";

	public final static String Flight_availability_search_flight_button = "//button[contains(text(),' Search Flights ')]";

	public final static String Flight_availability_dollars = "//div[@aria-label='Second Group']//label[1]";

	public final static String Flight_availability_Miles = "//div[@aria-label='Second Group']//label[2]";

	public final static String Flight_availability_child_input_text = "//div[@class='flex-basis-0 flex-grow-1 pl-3']//following::div[@class='d-flex justify-content-center'][2]//input[@class='form-control custom-form-counter']";

	public final static String Flight_availability_child_one_date_of_birth = "//input[@id='dateOfBirth1']";

	public final static String Flight_availability_child_two_date_of_birth = "//input[@id='dateOfBirth2']";

	public final static String Flight_availability_child_three_date_of_birth = "//input[@id='dateOfBirth3']";

	public final static String Flight_availability_child_four_date_of_birth = "//input[@id='dateOfBirth4']";

	public final static String Flight_availability_child_five_date_of_birth = "//input[@id='dateOfBirth5']";

	public final static String Flight_availability_child_six_date_of_birth = "//input[@id='dateOfBirth6']";

	public final static String Flight_availability_child_seven_date_of_birth = "//input[@id='dateOfBirth7']";

	public final static String Flight_availability_child_popup_continue_button = "//div[@class='modal-content']//button[contains(text(),'continue')]";

	public final static String Flight_availability_unaccompanied_minor_accept_button = "//button[contains(text(),'accept')]";

	public final static String Flight_availability_Tool_tip_signup_now_button = "//a[contains(text(),'Sign up now ')] ";

	public final static String Flight_availability_passenger_popup_button = "//button[contains(text(),'Close Window')] ";

	public final static String Flight_availability_region_North_America_tab = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-12']//button[@type='button'][contains(text(),'North America')]";

	public final static String Flight_availability_region_North_America_To_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-13']//button[contains(text(),'North America')]";

	public final static String Flight_availability_region_central_America_From_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-12']//button[contains(text(),'Central America')]";

	public final static String Flight_availability_region_central_America_To_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-13']//button[contains(text(),'Central America')]";

	public final static String Flight_Availability_region_cental_America_select_From_city = "//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),' Cancun, Mexico (CUN) ')]";

	public final static String Flight_Availability_region_central_America_select_To_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),' San Pedro Sula, Honduras (SAP) ')]";

	public final static String Flight_availability_region_south_America_From_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-12']//button[contains(text(),'South America')]";

	public final static String Flight_availability_region_south_America_To_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-13']//button[contains(text(),'South America')]";

	public final static String Flight_Availability_region_south_America_select_From_city = "//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),'Cartagena, Colombia (CTG)')]";

	public final static String Flight_Availability_region_south_America_select_To_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),' Lima, Peru (LIM)')]";

	public final static String Flight_availability_region_caribbean_From_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-12']//button[contains(text(),'Caribbean')]";

	public final static String Flight_availability_region_caribbean_To_city = "//app-station-picker-dropdown[@class='ng-tns-c20-11 ng-tns-c19-13']//button[contains(text(),'Caribbean')]";

	public final static String Flight_Availability_region_caribbean_select_From_city = "//div[@class='container pb-2 d-none d-md-block']//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-12 ng-star-inserted']//button[contains(text(),' Aruba, Aruba (AUA) ')]";

	public final static String Flight_Availability_region_caribbean_select_To_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block']//button[contains(text(),' San Juan, Puerto Rico (SJU) ')]";

	public final static String Flight_availability_travel_message_over_the_field = "//div[@class='popover-content popover-body']";

	public final static String Flight_availability_travel_message_over_the_field_Ok_button = "//button[contains(text(),' ok')]";

	public final static String Flight_avialbility_treat_yourself_continue_bare_fare = "//button[contains(text(),'Continue With Bare Fare')]";

	public final static String Flight_avialbility_treat_yourself_choose_bundle = "//button[contains(text(),'Choose This Bundle')]";

	public final static String Flight_avialbility_treat_yourself_Thrills_combo = "//button[contains(text(),'Choose Thrills Combo')]";

	public final static String popup_seat_purchase_myseats = "//button[@class='btn btn-primary']";
	public final static String popup_seat_dont_purchase_myseats = "//button[@class='btn btn-secondary']";
	public final static String popup_seat_close = "//i[@class='icon-close']";

	public final static String Origin_cities = "//app-station-picker-dropdown[@class='ng-tns-c20-3 ng-tns-c19-4']//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-4 ng-star-inserted']";
	public final static String Destination_cities = "//app-station-picker-dropdown[@class='ng-tns-c20-3 ng-tns-c19-5']//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-5 ng-star-inserted']";
}
