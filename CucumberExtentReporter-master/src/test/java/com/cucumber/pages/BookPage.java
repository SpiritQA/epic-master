package com.cucumber.pages;

public class BookPage {

	public final static String More_info = "//a[contains(text(),'more info')]";

	public final static String Book_Tab = "//a[@class='nav-link active'][1]/span[contains(text(),'Book')]";

	public final static String Round_Trip = "//label[@for='radio-roundTrip']";

	public final static String oneway_Trip = "//label[@for='radio-oneWay']";

	public final static String clickon_fromcity = "//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@name='originStationCode']";

	public final static String from_city = "//app-station-picker-dropdown[1]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Denver, CO (DEN) ')]";

	public final static String clickon_Tocity = "//app-flight-search//app-input//div[@class='input-group ng-star-inserted']//input[@name='destinationStationCode']";

	public final static String TO_city = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Fort Lauderdale, FL / Miami, FL AREA (FLL) ')]";

	public final static String TO_city_MSY = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' New Orleans, LA (MSY) ')]";

	public final static String Travel_departuredate_Returndate = "//tab[@class='active tab-pane']//div[@class='row form-group ng-tns-c20-3 ng-star-inserted']//input[@name='dates']";

	public final static String Date = "//tab[@class='active tab-pane']//input[@name='date']";

	public final static String clickon_passengers = "//input[@name='passengers']";

	public final static String select_adults = "//div[@class='d-flex pt-1']//i[@class='icon-add-circle']";

	public final static String select_adult_minus_button = "//div[@class='d-flex pt-1']//i[@class='icon-minus-circle']";

	public final static String select_child = "//div[@class='d-flex']//i[@class='icon-add-circle']";

	public final static String enter_child_date_of_birth = "//input[@id='dateOfBirth1']";

	public final static String child_popup_continue_button = "//div[@class='col-12 text-center']//button[contains(text(),'continue')]";

	public final static String promocode = "//div[@class='col-sm-3']/preceding::input[@class='form-control'][2]";

	public final static String Search_button = "//button[contains(text(),'Search Flights')]";

	public final static String Homepage_sigin_button = "//li[@class='nav3D'][1]";

	public final static String login_account_emailadress = "//input[@id='username']";

	public final static String Login_account_password = "//input[@id='password']";

	public final static String login_button_onpopup = "//button[contains(text(),' Log In')]";

	// English Header
	public final static String spiritLogo = "//img[@class='logo']";
	public final static String spiritLogo_ayuda_page = "//nav[@class='navbar navbar-spirit navbar-main container navspanish']//img[@class='ae-img']";
	public final static String spiritlogo_contactus_page = "//img[@class='logo']";
	public final static String signIn_Header = "//li[@class='nav3D'][1]";
	public final static String help_Header = "//li[@class='nav3D'][2]";
	public final static String contactUs_Header = "//li[@class='nav3D'][3]";
	public final static String deals_Header = "//li[@class='nav2D'][1]";
	public final static String nineFareClubEN_Header = "//li[@class='nav2D'][2]";
	public final static String spirit101EN_Header = "//li[@class='nav2D'][3]";
	public final static String destination_Header = "//li[@class='nav2D'][4]";
	public final static String languageButtonwhileEnglish = "//li[@class='Language']";
	// wont display on home page
	public final static String header1DnavigationBar = "//ul[@class='nav navbar-nav main-links']";
	public final static String book_Header = "//ul[@class='text-uppercase navblock1D header-ul ng-star-inserted']//li[1]";
	public final static String myTrips_Header = "//ul[@class='text-uppercase navblock1D header-ul ng-star-inserted']//li[2]";
	public final static String checkIn_Header = "//ul[@class='text-uppercase navblock1D header-ul ng-star-inserted']//li[3]";
	public final static String flightStatus_Header = "//ul[@class='text-uppercase navblock1D header-ul ng-star-inserted']//li[4]";

	// Spanish Header
	public final static String registrarme_Header = "//nav[@class='navbar navbar-spirit navbar-main container navspanish']//a[contains(text(),'Registrarse')]";
	public final static String ayuda_Header = "//ul[@class='text-uppercase navblock3D']//li[2]";
	public final static String contactanos_Header = "//li[@class='nav3D'][3]";
	public final static String ofertas_Header = "//li[@class='nav2D']//a[@aria-label='Deals'][contains(text(),'Ofertas')]";
	public final static String nineFareClubES_Header = "//li[@class='nav2D'][2]";
	public final static String spirit101ES_Header = "//li[@class='nav2D']//a[@aria-label='Spirit 101'][contains(text(),'Spirit 101')]";
	public final static String destinos_Header = "//li[@class='nav2D']//a[@aria-label='Destinations'][contains(text(),'Destinos')]";
	public final static String languageButtonwhileSpanish = "//li[@class='Language']//a[@aria-label='Language'][contains(text(),'English')]";
	// wont display on the homepage
	public final static String spanishMain_Header = "//ul[@class='text-uppercase navblock1D header-ul ng-star-inserted']";
	public final static String reservas_Header = "//li[@class='nav1D']//a[@aria-label='Book'][contains(text(),'Reservas')]";
	public final static String mis_viajes_Header = "//li[@class='nav1D']//a[@aria-label='My Trips'][contains(text(),'Mis Viajes')]";
	public final static String registrarse_Header = "//a[@aria-label='Check In']";
	public final static String estado_del_Vuelo_Header = "//li[@class='nav1D']//a[@aria-label='Flight Status'][contains(text(),'Estado Del Vuelo')]";
	// Booking Tab
	public final static String bookingtab = "//*[@id=\'scroll-home-search\']/div[2]/div/tabset/ul/li[1]/a";

	public final static String spiritAirlinesSupport = "//*[@id=\"wrap\"]/main/div/div[1]/h1";

	public final static String fareClubHeader = "//h1[@class='h1']";

	public final static String cantactanosbuttons = "//div[@class='row contact-btn-container']";

	public final static String login_button_onpopupES = "//button[contains(text(),' Log In')]";

	public final static String fareClubimg = "//div[@class='col-12 col-md-6 text-center mt-3 mt-md-0 d-md-flex justify-content-md-end']//img[@class='img-fluid']";

	public final static String Search_buttonES = "//button[contains(text(),'Buscar Vuelo')]";

	// My Trips
	public final static String manageTravelH3 = "//h3[contains(text(),'My Trips')]";
	public final static String manageTravelH3ES = "//h3[contains(text(),'Mis Viajes')]";

	// Check In
	public final static String checkInH3 = "//h3[contains(text(),'Check-In')]";
	public final static String checkInH3ES = "//h3[contains(text(),'Registrarse')]";

	// Flight Status
	public final static String flightstatusparagraph = "//h3[contains(text(),'Check Flight Status')]";
	public final static String flightstatusparagraphES = "//p[contains(text(),'¿A tiempo? ¿Retrasado? Encuentre aquí la info de llegadas y salidas.')]";

	public final static String eGiftCard = "//div[@class='col-xs-12 col-md-3 col-lg-4']//a[@class='text-capitalize'][contains(text(),'E-Gift Card')]";

	public final static String commonQuestions = "//h2[contains(text(),'Most Common Questions')]";

	public final static String header1 = "//h1[@class='h1']";

	public final static String cantactussbuttons = "//div[@class='row contact-btn-container']";

	public final static String FAQ = "//a[contains(text(),'Frequently Asked Questions')]";
	public final static String spirit101 = "//app-contact-us[1]//a[contains(text(),'Spirit 101')]";

	public final static String routMap = "//div[@class='container']/app-route-maps//iframe";

	public final static String routMapWelcome = "/html/body/div[5]/div/h3";
	public final static String upcomingNonstopServices = "//h4[contains(text(),'Upcoming Nonstop Services')]";
	public final static String timeTables = "//h4[contains(text(),'Time Tables')]";
	public final static String seasonalServices = "//h4[contains(text(),'Seasonal Service')]";

	///////////////////////////////////////////////////////////////////////////////////////////
	// Spanish

	public final static String destinosH1 = "/html/body/app-root/main/div[2]/app-route-maps/div[1]/div/h1";

	public final static String upcomingNonstopServicesES = "//h4[contains(text(),'Futuros Servicios Directos')]";
	public final static String timeTablesES = "//h4[contains(text(),'Horario de Vuelos')]";
	public final static String seasonalServiceES = "//h4[contains(text(),'Servicio de Temporada')]";

	public final static String enter_child2_date_of_birth = "//input[@id='dateOfBirth2']";

	public final static String Child1WillRequireSeat = "//label[@for='child1SeatOption']";

	public final static String Child1LapSeat = "//label[@for='child1LapOption']";

	public final static String Child2WillRequireSeat = "//label[@for='child2SeatOption']";

	public final static String Child2LapSeat = "//label[@for='child2SeatOption']";

	public final static String enter_child3_date_of_birth = "//input[@id='dateOfBirth3']";

	public final static String Vacation_Tab = "//a[@class='nav-link'][1]/span[contains(text(),'Vacation')]";

	public final static String Flight_car_rdbtn = "//label[@class='ng-star-inserted custom-control-label' and contains(text(),'Flight + Car')]";

	public final static String Vacation_from_airport_or_city = "//app-input[@class='ng-tns-c20-6']//input[@id='originStationCode']";

	public final static String Umnr_popup = "//button[contains(text(),'accept')]";

	public final static String Vacation_to_airport_or_city = "//app-input[@class='ng-tns-c20-6']//input[@id='destinationStationCode']";
	
	public final static String dates = "//input[@id='dates' and @name='dates']";
	
	public final static String driver_age = "//div[@class='col-sm-2']//div[@class='input-group ng-star-inserted']//select[@name='driverAge' and @id='driverAge']";
	
	public final static String Search_vacationsbtn = "//button[contains(text(),'Search Vacation') and @type='submit']";
	
	public final static String Vacation_clickon_passengers = "//app-input[@class='ng-tns-c20-6']//input[@id='passengers']";

	public final static String Book_car = "//button[@class='btn btn-primary' and contains(text(),'Book Car')]";

	public final static String preselected_flights = "//div[@class='d-none d-lg-block ng-trigger ng-trigger-accordionAnimate']//div[@class='h3'][contains(text(),'Your Selected')]";

	public final static String Proof_of_return_okbtn = "//div[@class='popover-content popover-body']//button[contains(text(),'ok')]";

	public final static String To_lim = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Lima, Peru (LIM) ')]";
	
	public final static String Selected_departure_citypair = "//div[@class='col-6']//div[@class='h3']";

	public final static String add_promo_tooltip = "//span[@class='m-1 align-middle']";
	
	public final static String add_promo_tooltip_signuplink = "//a[@class='btn btn-link text-capitalize ng-tns-c20-3 ng-star-inserted']";
	
	public final static String add_promo_code_link = "//a[@class='h-100 align-middle']";
	
	public final static String book_miles = "//div[@class='row']//div[@class='col radio-row-margin-hack']//label[@class='btn btn-secondary-pil home-pill-btn']";

	public final static String nocar_popup_title = "//h2[@class='modal-title']";
	
	public final static String nocar_popup_close = "//div[@class='modal-header']//button[@type='button' and @class='close']";
	
	
	public final static String nocar_popup_changedatesbtn = "//button[@class='btn btn-primary' and @type='button']";
	
	public final static String nocar_popup_text = "//p[contains(text(),'Unfortunately, There are no cars available for the dates selected.')]";
	
	public final static String nocar_popup_select_new_dates = "//modal-container[@class='modal fade show']//div[@class='row']//div[3]";
	
	public final static String seasonal_service_popup_close = "//button[@type='button' and @class='close']";
	
	public final static String login_popup_username = "//input[@id='username']";
	
	public final static String login_popup_password = "//input[@id='password']";
	
	public final static String login_popup_username_err = "//div[contains(text(),'email address is invalid')]";
	
	public final static String login_popup_pwd_err = "//div[@class='s-error-text ng-star-inserted']";
	
	public final static String login_popup_resetpwd_link = "//a[@id='aria-emailHelp']";
	
	public final static String login_popup_loginbtn = "//button[@class='btn btn-primary d-sm-inline']";
	
	public final static String login_popup_signupbtn = "//a[@routerlink='account-enrollment']";
	
	public final static String login_popup_closebtn = "//i[@class='icon-close']";
	
	public final static String login_popup_title = "//h2[@class='modal-title']";
	
	public final static String retrieve_pwd_email = "//input[@id='emailOrNumberText']";

	public final static String Adults_input_box = "//div[@class='d-flex pt-1']//div[@class='form-group']//input";

	public final static String Children_input_box = "//div[@class='d-flex']//div[@class='form-group']//input";

	public final static String select_adults_disable = "//app-count-picker[@class='ng-tns-c22-9 ng-untouched ng-valid ng-dirty']//i[@class='icon-add-circle disabled']";

	public final static String select_child_disable = "//i[@class='icon-minus-circle disabled']";
	
	public final static String select_child_minus_button = "//i[@class='icon-minus-circle']";
	
	public final static String from_sjd = "//app-station-picker-dropdown[1]//div[@class='container pb-2'][1]//button[contains(text(),' Los Cabos, Mexico (SJD) ')]";
	
	
	public final static String To_MDE = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Medellin, Colombia (MDE) ')]";
    public final static String To_SAP = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' San Pedro Sula, Honduras (SAP) ')]";
    public final static String To_DTW = "//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Detroit, MI (DTW) ')]";
    public final static String ADT_CHD_popup_Content="//div[@class='d-flex']//span[@class='color-gray']";
    public final static String From_FLL="//app-station-picker-dropdown[1]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Fort Lauderdale, FL / Miami, FL AREA (FLL) ')]";
    public final static String To_LAS="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Las Vegas, NV (LAS) ')]";
}
