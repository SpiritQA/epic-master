package com.cucumber.pages;

public class OptionsPage {

	
	//Cars
		public final static String carsCarausellRight = " //section[@class='ng-star-inserted']//app-car//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-right text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron_right']";
		public final static String carsCarausellLeft = " //section[@class='ng-star-inserted']//app-car//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-left text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron-left']";
		public final static String viewAllCarOptions = " //button[contains(text(),'View All Cars')] ";

		//hotels
		public final static String hotelsCarausellRight = " //section[@class='ng-star-inserted']//app-hotel//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-right text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron_right']";
		public final static String hotelsCarausellLeft = " //section[@class='ng-star-inserted']//app-hotel//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-left text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron-left'] ";
		public final static String viewAllHotelsOption = " //button[contains(text(),'View All Hotels')] ";
		
		//deals
		public final static String dealsHeader = "//div[@class='row mb-2 ng-star-inserted']//div[@class='col-12']//p[contains(text(),'Deals on Activites')]";
		public final static String dealsCarausellRight = "//section[@class='ng-star-inserted']//app-activity//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-left text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron-left']";
		public final static String dealsCarausellLeft = "//section[@class='ng-star-inserted']//app-activity//app-multi-carousel[@class='ng-star-inserted']//div[@class='row p-0 mb-5']//div[@class='col-1 carousel-right text-center d-flex flex-column justify-content-center pl-0 pr-0']//a[@href='javascript:void(0)']//i[@class='icon-chevron_right']"; 
		public final static String dealsCarausellViewAll = "//button[contains(text(),'View All Activities')]" ; 
		public final static String activity1 = " //section[@class='ng-star-inserted'][3]//div[@class='col-10 d-flex align-items-stretch']//app-ancillary-item[1]";  
		public final static String activity2 =	" //section[@class='ng-star-inserted'][3]//div[@class='col-10 d-flex align-items-stretch']//app-ancillary-item[2]"; 
		public final static String activity3 =" //section[@class='ng-star-inserted'][3]//div[@class='col-10 d-flex align-items-stretch']//app-ancillary-item[3]";
		public final static String activity4 =" //section[@class='ng-star-inserted'][3]//div[@class='col-10 d-flex align-items-stretch']//app-ancillary-item[4]";
		public final static String activity5 =" //section[@class='ng-star-inserted'][3]//div[@class='col-10 d-flex align-items-stretch']//app-ancillary-item[5]";
		
		//FlightFlex
		public final static String flightFlexPrice = "//app-extras-simple//div[1]//app-extra-simple-col[1]//div[1]//div[3]//div[1]//div[2]";
		public final static String flightFlexInfoBttn = "//div[@class='row d-flex align-items-stretch']//div[1]//app-extra-simple-col[1]//div[1]//div[2]//a[1]//i[1]";
		public final static String flightFlexAddBttn = "//app-extras-simple/div[2]/div[1]/app-extra-simple-col/div/div[3]/div[2]/a";

		//Shortcut Security
		public final static String checkinOptionDiv = "//app-extra-simple-row/div";	
		
		//public final static String shortCutSecuritydiv = "//app-extra-simple-col/div";	
		
		public final static String shortCutSecurityPrice = "//div[@class='col-12 col-lg-4 mb-4 ng-star-inserted'][2]//div[@class='font-weight-bold h3']";
		public final static String shortCutSecurityinfoBttn = " //div[@class='row d-flex align-items-stretch']//div[2]//app-extra-simple-col[1]//div[1]//div[2]//a[1]//i[1] ";
		public final static String shortCutSecurityAddBttn  = "//app-extras-simple/div[2]/div[2]/app-extra-simple-col/div/div[3]/div[2]/a ";
		public final static String shortCutSecurityborder =  "//div[@class='card card-body d-flex justify-content-between custom-card-height extra-selected']";
		
		public final static String shortCutSecuritychckBox =  "//div[@class='custom-control custom-checkbox ng-star-inserted']";
		public final static String shortCutSecurityPopUpBttn =  "//button[@class='btn btn-primary text-uppercase']";
		
		
		public final static String shortCutBoardingPrice = "//app-extras-simple//div[3]//app-extra-simple-col[1]//div[1]//div[3]//div[1]//div[2]";
		//public final static String shortCutBoardinginfoBttn = " ***** ";
		public final static String shortCutBoardingAddBttn  = "//app-options-page/section[4]/app-extras-simple/div[2]/div[3]/app-extra-simple-col/div/div[3]/div[2]/a";
		public final static String shortCutBoardingBorderWithCarryOn = "//div[@class='card card-body d-flex justify-content-between custom-card-height extra-selected extra-disabled']";
		public final static String shortCutBoardingBorder = "//div[@class='card card-body d-flex justify-content-between custom-card-height extra-selected']";
		
		//Continue
		public final static String checkinptiondiv ="//div[@class='card card-body extra-selected']";
		public final static String checkinptionDD = " //select[@name='checkInSelect'] ";
		public final static String checkinFree = " //option[@value='ONLINE'] ";
		public final static String checkinPrepaid = " //option[@value='PRE-PAY'] ";
		public final static String checkinNotSure = " //option[@value='NONE'] ";

		public final static String optionsTotal = " //div[@class='col-7 col-md-4 order-md-last d-md-flex justify-content-md-center flex-column']//p[@class='text-right h6 mb-0']";
		public final static String optionsTotalDD = " //app-chevron[@class='ng-tns-c6-3']//i[@class='icon-chevron_up color-white ng-trigger ng-trigger-openState'] ";
		
		//to get text of element in the TOTAL ul drop down
		public final static String optionsTotalDDelement1 = "//ul[@class='pr-4']/li[1]";
		public final static String optionsTotalDDelement2 = "//ul[@class='pr-4']/li[2]";//if displayed
		public final static String optionsTotalDDelement3 = "//ul[@class='pr-4']/li[3]";//if displayed
		public final static String optionsTotalDDelement4 = "//ul[@class='pr-4']/li[4]";//if displayed
		
		//to get price of elemet in the TOTAL ul dropdown
		public final static String optionsTotalDDelement1price = "//div[@class='col-3 d-md-flex flex-column align-items-end']//ul/li[1]";
		public final static String optionsTotalDDelement2price = "//div[@class='col-3 d-md-flex flex-column align-items-end']//ul/li[2]";//if displayed 
		public final static String optionsTotalDDelement3price = "//div[@class='col-3 d-md-flex flex-column align-items-end']//ul/li[3]";//if displayed
		public final static String optionsTotalDDelement4price = "//div[@class='col-3 d-md-flex flex-column align-items-end']//ul/li[4]";//if displayed
		
		public final static String continueBttn = " //button[@type='button'] ";	
		
		public final static String dynamicShoppingCart = "//div[2]/app-book-path/section/div/div[2]/div/div/div[1]/div/div/div[2]";
		

		//
		public final static String purchaseTotal = "//p[@class='text-right h6 mb-0']";
				
		public final static String purchaseTotalDD = "//app-chevron[@class='ng-tns-c6-40']";
				
		public final static String optionsDD =  "//app-breakdown-section[4]//i";
		
		
		public final static String checkinoptionPrice =  "//app-breakdown-section[4]/div[1]/div[3]/p ";
		//public final static String checkinoptionPrice =  "//app-price-section-line[@class='col-md-12 ng-star-inserted'][4]//p[@class='font-weight-bold pr-4 text-uppercase mb-0']";		
	
		public final static String activitiesHeading ="//div[@class='row ng-star-inserted']//h1";
		public final static String activitiesHeadingdiv ="//app-ancillary-list-page[@class='ng-star-inserted']//div[@class='row ng-star-inserted']//div[@class='col-12']";
		
		public final static String carsHeading ="//div[@class='row ng-star-inserted']//h1";
		public final static String carsHeadingdiv ="//app-ancillary-list-page[@class='ng-star-inserted']//div[@class='row ng-star-inserted']//div[@class='col-12']";
		
		public final static String carsCarNameSearch = "//input[@title='Filter By']";
		public final static String carsSortBy = "//select[@title='Sort By']";
		public final static String carsDisplaynum = "//div[@title='Display Number']//select";
		
		public final static String firstAvailableCar = "//div[@class='row mt-3 ng-star-inserted'][1]";
		public final static String firstCarCompanyIMG = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-12 col-md-4 mb-2']//img[1]";
		public final static String firstCarPictureIMG = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-12 col-md-4 mb-2']//img[2]";
		public final static String firstCarCompanyNameandcarSize = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-12 col-md-3 pt-md-3 ng-star-inserted']//p[1]";
		public final static String firstCarModel = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-12 col-md-3 pt-md-3 ng-star-inserted']//p[2]";
		public final static String firstCarMoreInfoLink = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-12 col-md-3 pt-md-3 ng-star-inserted']//a";
		public final static String firstCarPrice = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-6 col-md-3 d-lg-flex flex-lg-column justify-content-lg-center align-items-lg-end pr-lg-4']//p";
		public final static String firstCarExludeTaxes = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-6 col-md-3 d-lg-flex flex-lg-column justify-content-lg-center align-items-lg-end pr-lg-4']//a";
		public final static String firstCarBookBttn = "//div[@class='row mt-3 ng-star-inserted'][1]//div[@class='col-6 col-md-2 d-flex align-items-end align-items-md-center']//button";

	
	
	
	
	
	
	
	
	
	
	
	
}
