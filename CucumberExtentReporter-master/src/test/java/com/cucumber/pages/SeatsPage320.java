package com.cucumber.pages;

public class SeatsPage320 {
    public  final static String TO_city_LGA="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' New York, NY - LaGuardia (LGA)')]";
    
	public  final static String TO_city_las="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Las Vegas, NV (LAS) ')]";
	
public  final static String TO_city_LAS="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Las Vegas, NV (LAS) ')]";
    
    public  final static String TO_city_CUN="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Cancun, Mexico (CUN) ')]";
	
    public final static String first_cities_names_validation="//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//p[1][@class='font-weight-bold']"; 
	
	public final static String second_cities_names_validation="//div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//p[1][@class='font-weight-bold']";
	
	public final static String passenger_name_validation="//div[@class='col-12 d-none d-sm-block ng-star-inserted'][1]//div[@class='col-10 col-sm-7 col-lg-8 font-weight-bold'][1]//p[1]";
	
	public final static String Airbus_32_A="//strong[text()='Airbus 32A']";
	
	public final static String Airbus_32_B="//strong[text()='Airbus 32B']";
	
	public final static String Airbus_32_A_exitrow_six="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]//app-unit[7]";
	
	public final static String Airbus_32_A_exitrow_popup="//button[text()='Accept']";
	
	public final static String seats_page_continue_button="//div[@class='row purchase-seating-container d-none d-sm-block']//button[text()='Continue']";

	public final static String Airbus32_B_exit_row_columnone_one="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[1]";
	
	public final static String Airbus_Name ="//div[@class='reanimate-seat-rows ng-star-inserted']/following::div[@class='col-4 text-center'][1]//p[2]";
	
	public final static String seats_page_cities_caret_two="//div[@class='row mb-3 mt-3 ng-star-inserted']//following::div[@class='col-12 d-none d-sm-block ng-star-inserted'][2]//i";
	
	public final static String seats_page_seats_total_caret="//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//i";
	
	public final static String first_city_total_price_validation="//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[2]/div";
	
	public final static String second_city_total_price_validation="//div[@class='row spacing-bottom mb-5 d-none d-sm-block']//div[3]/div";
	
	public final static String second_flight_carrot = "//div[@class='row mb-3 mt-3 ng-star-inserted']/div[4]//i[@class='fa fa-chevron-down']";
	
    public final static String MyTrips="//div[@class='header container']//ul[@class='text-uppercase navblock1D header-ul']//li[2]";
    
    public final static String ConfirmationCode_carrot="//p[@class='confirm-code pt-2']//i[@class='icon-chevron_down']";
 
    public final static String Continue_button = "//button[contains(text(),' continue ')]";
    
    public final static String My_Trips ="//section[@class='tabset']/tabset[1]/ul[@class='nav nav-tabs']/descendant::span[text()='My Trips']";
    
    public final static String Passengers_Lastname="//tab[@class='tab-pane active']//input[@title='Enter passenger last name']";
   
    public final static String confromation_code ="//tab[@class='tab-pane active']//input[@title='Enter passenger PNR']";
   
    public final static String Search_button ="//button[contains(text(),'Search')][@class='btn btn-primary']/preceding::div[@class='col-sm-12 col-md-auto bottom-align']";
   
    public final static String EditSeats_button ="//button[contains(text(),'Edit Seats')]";
    
    public final static String chekin_path_seatspage_bags_popup_dont_purchase_bags="//div[@class='row text-center']//div[@class='col-12 col-sm-6'][2]";
    
    public final static String chekin_path_seatspage_bags_popup="//div[@class='modal-content']//div[@class='col-12 col-sm-6'][1]";
	
    public final static String checkin_path_extraspage_text_validation="//p[text()='Extras']";
    
    public final static String checkin_path_extrapage_continue_button="//button[text()=' Continue ']";
    
    public final static String Checkin_and_Print_Boarding_Pass_Button = "//button[contains(text(),' Check-In And Print Boarding Pass ')]";
    
    public final static String Choose_Your_seats_pop_up_random_button = "//div[@class='modal-content']//div[@class='col-12 col-sm-6'][1]";
    
    public final static String Hazmat_Popup_Accept_and_Print_button ="//div[@class='modal-content']//div[@class='col-12']//button[contains(text(),'Accept & Print Boarding Pass')]";
    
    public final static String Your_Boarding_Pass_Popup_Checkin_Button ="//div[@class='modal-content']//div[@class='col-12']//button[contains(text(),'FINISH CHECK-IN')]";
    
    public final static String Reserve_a_Car_No_Button = "//div[@class='modal-content']//div[@class='col-12 col-sm-6']//button[contains(text(),'No')]";
   
    public final static String TravelGuard_No_Button = "//div[@class='modal-content']//div[@class='form-check']//label[@for='radNo']";
    
    public final static String Mytrips_checkin_seats_link="//a[contains(text(),' seats')]";
//	AIRBUS 32A
	//BFS-Big front seat
//	standard seat
//	Exit seat
	public final static String Airbus_32_A_BFS_1A="//div[@class='reanimate-seat-rows ng-star-inserted'][1]/app-unit[1]";
	
    public final static String Airbus_32_A_BFS_1C="//div[@class='reanimate-seat-rows ng-star-inserted'][1]/app-unit[3]";
	
    public final static String Airbus_32_A_BFS_1D="//div[@class='reanimate-seat-rows ng-star-inserted'][1]/app-unit[5]";
	
    public final static String Airbus_32_A_BFS_1F="//div[@class='reanimate-seat-rows ng-star-inserted'][1]/app-unit[7]";
    
    public final static String Airbus_32_A_BFS_2A="//div[@class='reanimate-seat-rows ng-star-inserted'][2]/app-unit[1]";
    
    public final static String Airbus_32_A_BFS_2C="//div[@class='reanimate-seat-rows ng-star-inserted'][2]/app-unit[3]";
    
    public final static String Airbus_32_A_BFS_2D="//div[@class='reanimate-seat-rows ng-star-inserted'][2]/app-unit[5]";
    
    public final static String Airbus_32_A_BFS_2F="//div[@class='reanimate-seat-rows ng-star-inserted'][2]/app-unit[7]";

    public final  static String Airbus_32_A_3A="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[1]";
    
    public final  static String Airbus_32_A_3B="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[2]";
    
    public final  static String Airbus_32_A_3C="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[3]";
    
    public final  static String Airbus_32_A_3D="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[5]";
    
    public final  static String Airbus_32_A_3E="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[6]";
    
    public final  static String Airbus_32_A_3F="//div[@class='reanimate-seat-rows ng-star-inserted'][3]/app-unit[7]";
    
    public final  static String Airbus_32_A_4A="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[1]";
    
    public final  static String Airbus_32_A_4B="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[2]";
    
    
    public final  static String Airbus_32_A_4C="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[3]";
    
    public final  static String Airbus_32_A_4D="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[5]";
    
    public final  static String Airbus_32_A_4E="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[6]";
    
    public final  static String Airbus_32_A_4F="//div[@class='reanimate-seat-rows ng-star-inserted'][4]/app-unit[7]";
    
    public final static String Airbus_32_A_5A="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[1]";
    
    public final static String Airbus_32_A_5B="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[2]";

    public final static String Airbus_32_A_5C="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[3]";

    public final static String Airbus_32_A_5D="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[5]";

    public final static String Airbus_32_A_5E="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[6]";

    public final static String Airbus_32_A_5F="//div[@class='reanimate-seat-rows ng-star-inserted'][5]/app-unit[7]";

    public final static String Airbus_32_A_6A="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[1]";
    
    public final static String Airbus_32_A_6B="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[2]";

    public final static String Airbus_32_A_6C="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[3]";

    public final static String Airbus_32_A_6D="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[5]";

    public final static String Airbus_32_A_6E="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[6]";

    public final static String Airbus_32_A_6F="//div[@class='reanimate-seat-rows ng-star-inserted'][6]/app-unit[7]";

    public final static String Airbus_32_A_7A="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[1]";

    public final static String Airbus_32_A_7B="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[2]";
    
    public final static String Airbus_32_A_7C="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[3]";
    
    public final static String Airbus_32_A_7D="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[5]";
    
    public final static String Airbus_32_A_7E="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[6]";
    
    public final static String Airbus_32_A_7F="//div[@class='reanimate-seat-rows ng-star-inserted'][7]/app-unit[7]";
    
    public final static String Airbus_32_A_8A="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[1]";
    
    public final static String Airbus_32_A_8B="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[2]";

    public final static String Airbus_32_A_8C="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[3]";
    
    public final static String Airbus_32_A_8D="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[5]";

    public final static String Airbus_32_A_8E="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[6]";

    public final static String Airbus_32_A_8F="//div[@class='reanimate-seat-rows ng-star-inserted'][8]/app-unit[7]";

    public final static String Airbus_32_A_9A="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[1]";

    public final static String Airbus_32_A_9B="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[2]";
    
    public final static String Airbus_32_A_9C="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[3]";
    
    public final static String Airbus_32_A_9D="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[5]";
    
    public final static String Airbus_32_A_9E="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[6]";
    
    public final static String Airbus_32_A_9F="//div[@class='reanimate-seat-rows ng-star-inserted'][9]/app-unit[7]";
    
    public final static String Airbus_32_A_10A="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[1]";
    
    public final static String Airbus_32_A_10B="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[2]";
    
    public final static String Airbus_32_A_10C="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[3]";
    
    public final static String Airbus_32_A_10D="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[5]";
    
    public final static String Airbus_32_A_10E="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[6]";
    
    public final static String Airbus_32_A_10F="//div[@class='reanimate-seat-rows ng-star-inserted'][10]/app-unit[F]";
    
    
    public final static String Airbus_32_A_11A="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[1]";
    
    public final static String Airbus_32_A_11B="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[2]";

    public final static String Airbus_32_A_11C="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[3]";
    
    public final static String Airbus_32_A_11D="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[5]";

    public final static String Airbus_32_A_11E="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[6]";

    public final static String Airbus_32_A_11F="//div[@class='reanimate-seat-rows ng-star-inserted'][11]/app-unit[7]";

    public final static String Airtbus_32_A_Exit_12A="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[1]";
    
    public final static String Airtbus_32_A_Exit_12B="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[2]";

    public final static String Airtbus_32_A_Exit_12C="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[3]";

    public final static String Airtbus_32_A_Exit_12D="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[5]";

    public final static String Airtbus_32_A_Exit_12E="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[6]";

    public final static String Airtbus_32_A_Exit_12F="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][1]/app-unit[7]";

    public final static String Airtbus_32_A_Exit_13A="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[1]";

    public final static String Airtbus_32_A_Exit_13B="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[2]";

    public final static String Airtbus_32_A_Exit_13C="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[3]";

    public final static String Airtbus_32_A_Exit_13D="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[5]";

    public final static String Airtbus_32_A_Exit_13E="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[6]";

    public final static String Airtbus_32_A_Exit_13F="//div[@class='reanimate-seat-rows exit-row ng-star-inserted'][2]/app-unit[7]";

    public final static String Airbus_32_A_14A="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[1]";
    
    public final static String Airbus_32_A_14B="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[2]";
    
    public final static String Airbus_32_A_14C="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[3]";
    
    public final static String Airbus_32_A_14D="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[5]";
    
    public final static String Airbus_32_A_14E="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[6]";
    
    public final static String Airbus_32_A_14F="//div[@class='reanimate-seat-rows ng-star-inserted'][14]/app-unit[7]";
    
    public final static String Airbus_32_A_15A="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[1]";
    
    public final static String Airbus_32_A_15B="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[2]";
    
    public final static String Airbus_32_A_15C="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[3]";
    
    public final static String Airbus_32_A_15D="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[5]";
    
    public final static String Airbus_32_A_15E="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[6]";
    
    public final static String Airbus_32_A_15F="//div[@class='reanimate-seat-rows ng-star-inserted'][15]/app-unit[7]";
    
    public final static String Airbus_32_A_16A="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[1]";
    
    public final static String Airbus_32_A_16B="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[2]";
    
    public final static String Airbus_32_A_16C="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[3]";
    
    public final static String Airbus_32_A_16D="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[5]";
    
    public final static String Airbus_32_A_16E="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[6]";
    
    public final static String Airbus_32_A_16F="//div[@class='reanimate-seat-rows ng-star-inserted'][16]/app-unit[7]";
    
    public final static String Airbus_32_A_17A="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[1]";

    public final static String Airbus_32_A_17B="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[2]";
    
    public final static String Airbus_32_A_17C="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[3]";
    
    public final static String Airbus_32_A_17D="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[5]";
    
    public final static String Airbus_32_A_17E="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[6]";
    
    public final static String Airbus_32_A_17F="//div[@class='reanimate-seat-rows ng-star-inserted'][17]/app-unit[7]";
    
    public final static String Airbus_32_A_18A="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[1]";
    
    public final static String Airbus_32_A_18B="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[2]";

    public final static String Airbus_32_A_18C="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[3]";

    public final static String Airbus_32_A_18D="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[5]";

    public final static String Airbus_32_A_18E="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[6]";

    public final static String Airbus_32_A_18F="//div[@class='reanimate-seat-rows ng-star-inserted'][18]/app-unit[7]";

    public final static String Airbus_32_A_Standard_Seat_19A ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[1]";
    
	public final static String Airbus_32_A_Standard_Seat_19B ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_19C ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_19D ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_19E ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_19F ="//div[@class='reanimate-seat-rows ng-star-inserted'][19]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_20A ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_20B ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_20C ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_20D ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_20E ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_20F ="//div[@class='reanimate-seat-rows ng-star-inserted'][20]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_21A ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_21B ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_21C ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_21D ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_21E ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_21F ="//div[@class='reanimate-seat-rows ng-star-inserted'][21]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_22A ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_22B ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_22C ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_22D ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_22E ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_22F ="//div[@class='reanimate-seat-rows ng-star-inserted'][22]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_23A ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_23B ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_23C ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_23D ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_23E ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_23F ="//div[@class='reanimate-seat-rows ng-star-inserted'][23]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_24A ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_24B ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_24C ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_24D ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_24E ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_24F ="//div[@class='reanimate-seat-rows ng-star-inserted'][24]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_25A ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_25B ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_25C ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_25D ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_25E ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_25F ="//div[@class='reanimate-seat-rows ng-star-inserted'][25]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_26A ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_26B ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_26C ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_26D ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_26E ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_26F ="//div[@class='reanimate-seat-rows ng-star-inserted'][26]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_27A ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_27B ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_27C ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_27D ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_27E ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_27F ="//div[@class='reanimate-seat-rows ng-star-inserted'][27]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_28A ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_28B ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_28C ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_28D ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_28E ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_28F ="//div[@class='reanimate-seat-rows ng-star-inserted'][28]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_29A ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_29B ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_29C ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_29D ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_29E ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_29F ="//div[@class='reanimate-seat-rows ng-star-inserted'][29]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_30A ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_30B ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_30C ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_30D ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_30E ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_30F ="//div[@class='reanimate-seat-rows ng-star-inserted'][30]//app-unit[7]";
	
	public final static String Airbus_32_A_Standard_Seat_31A ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[1]";
	
	public final static String Airbus_32_A_Standard_Seat_31B ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[2]";
	
	public final static String Airbus_32_A_Standard_Seat_31C ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[3]";
	
	public final static String Airbus_32_A_Standard_Seat_31D ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[5]";
	
	public final static String Airbus_32_A_Standard_Seat_31E ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[6]";
	
	public final static String Airbus_32_A_Standard_Seat_31F ="//div[@class='reanimate-seat-rows ng-star-inserted'][31]//app-unit[7]";
	
	public  final static String TO_city_LIM="//app-station-picker-dropdown[2]//div[@class='container pb-2 d-none d-md-block'][1]//button[contains(text(),' Lima, Peru (LIM) ')]";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
