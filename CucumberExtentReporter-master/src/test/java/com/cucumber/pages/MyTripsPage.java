package com.cucumber.pages;

public class MyTripsPage {
 
	public final static String My_Trips ="//section[@class='tabset']/tabset[1]/ul[@class='nav nav-tabs']/descendant::span[text()='My Trips']";
	
	public final static String Passengers_Lastname="//tab[@class='active tab-pane']//input[@id='lastName']";
	
	public final static String confromation_code ="//tab[@class='active tab-pane']//input[@id='recordLocator']";
	
	public final static String Search_button ="//div[@class='col-12 ng-star-inserted']//button[contains(text(),' continue ')]";
	
	
}
 