package com.cucumber.pages;

public class FlightStatusPage {

	public final static String Flight_status  ="//span[text()='Flight Status']";
	
	public final static String checkby_destination="//label[@for='radio-fs1']";
	
	
	public final static String flight_status_clicks_Departure_city ="//app-home-page[1]/section[1]//tabset[1]//app-flight-status-home[1]//input[@name='originStationCode']";
	
	
	public final static String flight_statusclicks_Arrival_city ="//app-home-page[1]/section[1]//tabset[1]//app-flight-status-home[1]//input[@name='destinationStationCode']";
	
	public final static String flight_status_new_destination_departure_city_click="//input[@id='originStationCode']";
	
	public final static String flight_status_new_destination_arrival_city_click="//input[@id='destinationStationCode']";
	
	
	public final static String Flight_status_Select_day ="//select[@id='date']";
	
	
	public final static String checkby_Flight_number ="//label[@for='radio-fs2']";
	
	
	public final static String Flight_number ="//input[@id='flightNumber']";
	
	public final static String check_status ="//button[contains(text(),'Check Status')]";
	
	public final static String select_departure_city="//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-9 ng-star-inserted']//button[contains(text(),'Denver, CO (DEN)')]";
	
	public final static String select_arrival_city="//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-10 ng-star-inserted']//button[contains(text(),' Fort Lauderdale, FL / Miami, FL AREA (FLL) ')]";
	
	public final static String flight_status_get_updates="//app-flight-status-page[1]/div[4]//div[1]/button[1]";
	
	public final static String flight_status_cancel_button="//div[@class='card mb-2 collapse in show']//button[contains(text(),'Cancel')]";
	
	public final static String flight_status_getupdates_email="//div[@class='card mb-2 collapse in show']//button[contains(text(),'Get Updates')]";
	
	public final static String flight_status_input_email="//app-flight-status-page//div[4]//input";
	
	public final static String flight_status_text_validation="//p[contains(text(),'On-time? Delayed? Find arrival and departure info ')]";
	
	public final static String flight_status_from_city_text_validation="//div[contains(text(),'From is required')]";
	
	public final static String flight_status_to_city_text_validation="//div[contains(text(),'To is required')]";
	
	public final static String flight_status_popup_close_button="//i[@class='icon-close']";
	
	public final static String flight_status_from_cities_drop_down_close="//app-station-picker-dropdown[@class='ng-tns-c19-41']//i[@class='icon-close station-close-btn']";
	
	public final static String flight_status_to_cities_drop_down_close="//app-station-picker-dropdown[@class='ng-tns-c19-42']//i[@class='icon-close station-close-btn']";
	
	
	public final static String flight_status_new_destination_from_city="//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-11 ng-star-inserted']//button[contains(text(),' Newark, NJ (EWR) ')]";
	
	public final static String flight_status_new_destination_to_city="//div[@class='col-sm-3 d-flex align-items-center ng-tns-c19-12 ng-star-inserted']//button[contains(text(),' San Diego, CA (SAN)')]";
	
}
 