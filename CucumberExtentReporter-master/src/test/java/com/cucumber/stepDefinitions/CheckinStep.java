package com.cucumber.stepDefinitions;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage320;
import com.cucumber.pages.SeatsPage319;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckinStep {

	
	
	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();

	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	
	@Then("^user verifys Bags pop up appears$") 
	public void user_verifys_Bags_pop_up_appears() throws Throwable {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.BagsPopUp_ProTip)));
		Assert.assertTrue("BAGS POP UP DOES NOT APPEAR!!!",
				driver.findElement(By.xpath(CheckInPage.BagsPopUp_ProTip)).getText().equals("Save more when you buy Carry-On or Checked bags online."));
		
        log.info("User verified Bags popup");
	}
	
	@Then("^user verifys no Bags pop up appears$") 
	public void user_verifys_no_Bags_pop_up_appears() throws Throwable {
		{
		    try
		    {
		    	driver.findElement(By.xpath(CheckInPage.BagsPopUp_ProTip));
		    	log.info("Bags popup appears");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Bags popup Doesnt not appear verified");
		    }
		}
	}
	
	@Then("^user exits seats popup and restarts checkin path$")
    public void user_exits_seats_popup_and_restarts_checkin_path()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.SeatsPopUp_X_Button)));
        driver.findElement(By.xpath(CheckInPage.SeatsPopUp_X_Button)).click();
 
        log.info("User exits seats popup and restarts checkin path");
	}
	
	@Then("^user clicks yes to Bags popup$")
    public void Then_user_clicks_yes_to_Bags_popup()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.BagsPopUp_Yes_Button)));
        driver.findElement(By.xpath(CheckInPage.BagsPopUp_Yes_Button)).click();
 
        log.info("User Clicks on yes to Bags PopUp");
	}
	
	@Then("^user verifys user is taken to bags page$")
	public void user_verifys_user_is_taken_to_bags_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.BagsPageTitle)));
		Assert.assertTrue("User is not  navigated to options page",
				driver.findElement(By.xpath(CheckInPage.BagsPageTitle)).getText().equals("Add Bags Now And Save"));

		log.info("user navigated to Bags Page");
	}

	@Then("^user Click on the plus button next to checked bags Max bags$")
	public void user_click_on_the_plus_button_next_to_checked_bags_max_bags_checkin_path() throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			log.info("Now the bags count is increased to " + (i + 1));

			int BagsCost[] = new int[] { 30, 70, 155, 240, 325 };
			Assert.assertTrue(
					"After increasing the bags count to " + (i + 1) + " the cost is not increased to " + BagsCost[i],
					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
							.equals("$" + BagsCost[i]));
			log.info("The cost increased to " + BagsCost[i] + " for adding the " + (i + 1) + "th bag");
		}

	}
	
	@Then("^user clicks on Standard Pricing$") 
	public void user_clicks_on_Standard_Pricing() throws Throwable {
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.StandardPricing_button)));
		driver.findElement(By.xpath(CheckInPage.StandardPricing_button)).click();
		
        log.info("User Clicks on Standard Pricing Continue button");
	}
	
	@Then("^user Verify seats pop up appears$") 
	public void user_Verify_seats_pop_up_appears() throws Throwable {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.SeatsPopUp)));
		Assert.assertTrue("SEATS POP UP DOES NOT APPEAR!!!",
				driver.findElement(By.xpath(CheckInPage.SeatsPopUp)).getText().equals("Choose Your Seats"));
		
        log.info("User verified Seats popup");
	}
	
	@Then("^user verify user lands on extras page$") 
	public void user_verify_user_lands_on_extras_page() throws Throwable {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.ExtrasPage_title)));
		Assert.assertTrue("User is not on Extras Page!!!",
				driver.findElement(By.xpath(CheckInPage.ExtrasPage_title)).getText().equals("Extras"));
		
        log.info("User navigated to Extras page verfied");
	}
	
	@Then("^user adds Shortcut Security$") 
	public void user_adds_Shortcut_Security() throws Throwable {
        Thread.sleep(2000);
		driver.findElement(By.xpath(CheckInPage.ExtrasPage_title)).click();
		
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
		driver.findElement(By.xpath(CheckInPage.ShortcutSecurity_CheckBox)).click();
		driver.findElement(By.xpath(CheckInPage.Add_Shortcut_Security_Button)).click();	
        log.info("User Adds shortcut Security");
	}
	
	@Then("^user verifys Travel Guard popup$") 
	public void user_verifys_Travel_Guard_popup() throws Throwable {
        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.TravelGuard_Popup)));
		Assert.assertTrue("No Travel Guard PopUp!!!",
				driver.findElement(By.xpath(CheckInPage.TravelGuard_Popup)).getText().equals("Travel Guard Recommends Travel Insurance"));
		
        log.info("Travel Guard Popup verfied");
	}
	
	@Then("^user verify Hazmat popup$") 
	public void Then_user_verify_Hazmat_popup() throws Throwable {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.Hazmat_Header_Popup)));
		Assert.assertTrue("User Does not see hazmat Popup!!!",
				driver.findElement(By.xpath(CheckInPage.Hazmat_Header_Popup)).getText().equals("DON'T BRING HAZARDOUS MATERIAL"));
		
        log.info("Hazmat Popup verfied");
	}
	
	@Then("^user verify he is navigated to purchase page$") 
	public void user_verify_he_is_navigated_to_purchase_page() throws Throwable {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.PaymentPage_Header)));
		Assert.assertTrue("User Does not see payment page!!!",
				driver.findElement(By.xpath(CheckInPage.PaymentPage_Header)).getText().equals("Payment "));
		
        log.info("user verify he is navigated to purchase page");
	}
	
	@Then("^user clicks no to Travel Guard popup$") 
	public void Then_user_clicks_no_to_Travel_Guard_popup() throws Throwable {
        Thread.sleep(2000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
		driver.findElement(By.xpath(CheckInPage.TravelGuard_Popup_no_button)).click();
        log.info("User Clicks no to Travel Guard");
        
	}
	
	@Then("^user clicks yes to Travel Guard popup$") 
	public void Then_user_clicks_yes_to_Travel_Guard_popup() throws Throwable {
        Thread.sleep(2000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
		driver.findElement(By.xpath(CheckInPage.TravelGuard_Popup_yes_button)).click();
        log.info("User Clicks yes to Travel Guard");
        
	}
	
	@Then("^user clicks on Spirit Logo$") 
	public void user_clicks_on_Spirit_Logo() throws Throwable {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.Spirit_logo)));
		
		driver.findElement(By.xpath(CheckInPage.Spirit_logo)).click();
		
        log.info("User clicks on Spirit LOGO");
	}
	
	@Then("^user Proceeds to Seats pop up selects random and restarts checkin path$") 
	public void user_attemps_to_checkin_again() throws Throwable {
		Thread.sleep(5000);
		
		String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
		String pnr = tmp.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + pnr);

        Thread.sleep(10000);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)));
        driver.findElement(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)).click();
        log.info("user succesfully clicks on the checkin and print boarding pass on the checkin page");
        
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)));
        driver.findElement(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)).click();
 
        log.info("user succefully handled the popup and select the i dont need bags ");
        
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.SeatsPopUp)));
		Assert.assertTrue("SEATS POP UP DOES NOT APPEAR!!!",
				driver.findElement(By.xpath(CheckInPage.SeatsPopUp)).getText().equals("Choose Your Seats"));
		
        log.info("User verified Seats popup");
        
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Choose_Your_seats_pop_up_random_button)));
        driver.findElement(By.xpath(SeatsPage320.Choose_Your_seats_pop_up_random_button)).click();
 
        log.info("user succefully selects Random Seats ");
        
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.ExtrasPage_title)));
		Assert.assertTrue("User is not on Extras Page!!!",
				driver.findElement(By.xpath(CheckInPage.ExtrasPage_title)).getText().equals("Extras"));
		
        log.info("User navigated to Extras page verfied");
        
        driver.findElement(By.xpath(CheckInPage.Spirit_logo)).click();
        
        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_mainmenu)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_mainmenu)).click();
		log.info("Clicked on the checkin button present on top banner");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
		log.info("Passenger entering the last name as mentioned");
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
		log.info("Entered the confirmation code as : " + pnr);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.checkin_button)));
		driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
		log.info("Then user clicked on the search button to check the info of the booked ticket");
 
    }
	
	@Then("^user Selects a seat$") 
	public void user_Selects_a_seat() throws Throwable {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.Spirit_logo)));
		

        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='reanimate-seat-rows ng-star-inserted']//app-unit[@class='ng-star-inserted']"));
        java.util.Iterator<WebElement> i = elements.iterator();
        while(i.hasNext()) {
        	WebElement element = i.next();
        	if (element.isEnabled()) {
       element.click();
      	String Seat = driver.findElement(By.xpath(CheckInPage.Seat_Assignment_Seat_page)).getText();
    	String Seat_Price = driver.findElement(By.xpath(CheckInPage.Seat_price_Seats_page)).getText();
    	log.info("User Clicked on seat " + Seat);
    	log.info("the Price for the seat is " + Seat_Price);
       break;
        	}
        	Thread.sleep(2000);
        driver.findElement(By.xpath(SeatsPage319.seats_page_continue_button)).click();
        }  
		
	}
	
	
	@Then("^user Select seats and verifies the seat on payment page$") 
	public void user_Select_seats_and_verifies_the_seat_on_payment_page() throws Throwable {
        Thread.sleep(2000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
				driver.findElement(By.xpath(CheckInPage.SeatsPopUp_Select_Seats_button)).click();
		
        log.info("User Clicks on Select Seats");
	
        Thread.sleep(2000);

        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='reanimate-seat-rows ng-star-inserted']//app-unit[@class='ng-star-inserted']"));
        java.util.Iterator<WebElement> i = elements.iterator();
        while(i.hasNext()) {
        	WebElement element = i.next();
        	if (element.isEnabled()) {
       element.click();
       log.info("User Selects a seat");
       break;
        	}
        }
        	String Seat = driver.findElement(By.xpath(CheckInPage.Seat_Assignment_Seat_page)).getText();
        	String Seat_Price = driver.findElement(By.xpath(CheckInPage.Seat_price_Seats_page)).getText();
        	log.info("User Clicked on seat " + Seat);
        	log.info("the Price for the seat is " + Seat_Price);
        	
        	//User clicks on Continue button on seat page
        	Thread.sleep(2000);
        	driver.findElement(By.xpath(SeatsPage319.seats_page_continue_button)).click();
        	
        	//user clicks on Continue button on extras page
        	 Thread.sleep(2000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.ExtrasPage_title)));
    		Assert.assertTrue("User is not on Extras Page!!!",
    				driver.findElement(By.xpath(CheckInPage.ExtrasPage_title)).getText().equals("Extras"));
    		
            log.info("User navigated to Extras page verfied");
            
            wait.until(ExpectedConditions
    				.visibilityOfElementLocated(By.xpath(SeatsPage319.checkin_path_extrapage_continue_button)));
    		driver.findElement(By.xpath(SeatsPage319.checkin_path_extrapage_continue_button)).click();
    		log.info("user succesfully clicks on the continue button on the extras page");
        	
        	//User clicks no to travel Guard
    		
    		   for (String winHandle : driver.getWindowHandles()) {
    	            driver.switchTo().window(winHandle);
    	        }
    	        Thread.sleep(2000);
    			driver.findElement(By.xpath(CheckInPage.TravelGuard_Popup_no_button)).click();
    	        log.info("User Clicks no to Travel Guard");  
    	        
    	        //user lands on payment page and verify seat assignment and price
    	        Thread.sleep(2000);
    	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Paymets_page)));
    			Assert.assertTrue("User is not  navigated to payments page",
    					driver.findElement(By.xpath(OnewaytripPage.Paymets_page)).getText().equals("PAYMENT"));
    			log.info("user navigated to payments page");
    			
    			//verify seat assignment
    			
    			driver.findElement(By.xpath(CheckInPage.Passenger_info_carrot_PaymentPage)).click();
    			
            	String Seat_Assignment = driver.findElement(By.xpath(CheckInPage.Seat_Assignment_Payment_page)).getText();
            	String VSeat_Assignment = Seat_Assignment.substring(9);
            	log.info("Seat assignment on payment page is " + VSeat_Assignment);
            	  	        
    			Assert.assertTrue("Seat Assignment is Incorrect",
    					(VSeat_Assignment.equals(Seat)));
    			log.info("Seat Assignment verified on payments page");
            	
//            	if(VSeat_Assignment.equals(Seat))
//            		log.info("Seat verified on payments page");
//            	else
//            		log.info("Seat assignment is incorrect");
            	
            	//Verify Seat Price
            	Thread.sleep(2000);
            	driver.findElement(By.xpath(CheckInPage.Total_Due_carrot_PaymentPage)).click();
            	String Seat_Price_payment = driver.findElement(By.xpath(CheckInPage.Seat_price_Payment_page)).getText();
            	log.info("Seat price on payment page is " + Seat_Price_payment);
            	
    			Assert.assertTrue("Seat Price is Incorrect",
    					(Seat_Price_payment.equals(Seat_Price)));
    			log.info("Seat Price verified on payments page");
            	
//            	if(Seat_Price_payment.equals(Seat_Price))
//            		log.info("Seat Price Verified on payments page");
//            	else
//            		log.info("Seat Price is incorrect");
    			
    			//Inputs payment
    			driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("jacks flayer");
    			driver.findElement(By.xpath(OnewaytripPage.card_number)).sendKeys("6011212100000004");
    			driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
    			driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
    			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
    			driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
	}
	
    @When("^User lands on Online Checkin Page$")
    public void user_lands_on_online_checkin_page() throws Throwable {
    	Thread.sleep(3000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.Online_Checkin_Header)));

		Assert.assertTrue("User is not  navigated Check-In page",
				driver.findElement(By.xpath(CheckInPage.Online_Checkin_Header)).getText()
						.equals("Online Check-In"));
		log.info("user succesfully navigated to Check-In page");
    }

    @Then("^User verify Add Seats Button for passenger$")
    public void user_verify_add_seats_button_for_passenger() throws Throwable {
    	Assert.assertTrue("Button does not say ADD SEATS",
				driver.findElement(By.xpath(CheckInPage.Add_Seats_button)).getText()
						.equals("ADD SEATS"));
		log.info("Seats Button Verified");
    }

    @Then("^User Clicks on add Seats Button$")
    public void user_clicks_on_add_seats_button() throws Throwable {
    	driver.findElement(By.xpath(CheckInPage.Add_Seats_button)).click();
    	log.info("User clicks on Seats Button");
    }

    @Then("^User verify he is taken to Seats page$")
    public void user_verify_he_is_taken_to_Seats_page() throws Throwable {
    	Thread.sleep(3000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath((CheckInPage.SeatsPage_header))));

		Assert.assertTrue("User is not  navigated Seats page",
				driver.findElement(By.xpath(CheckInPage.SeatsPage_header)).getText()
						.equals("Choose Your Seat"));
		log.info("user succesfully navigated to Seats Page");
    }
	
	@Then("^user Verify seats pop up does not appears$") 
	public void user_Verify_seats_pop_up_does_not_appears() throws Throwable {
		{
		    try
		    {
		    	driver.findElement(By.xpath(CheckInPage.SeatsPopUp));
		    	log.info("Seats popup appears");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Seats popup Doesnt not appear verified");
		    }
		}
	}
	
	@Then("^user takes the new pnr code and clicks on the checkin button verifys the checkin Widet and enters the lastname and PNR$")
	public void user_takes_the_new_pnr_code_and_clicks_on_the_checkin_button_verifys_the_checkin_widet_and_enters_the_lastname_and_pnr() throws Throwable {
	 
//        driver.findElement(By.xpath(PassengerinfoPage.conformation_page_popup)).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
		String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
		String pnr = tmp.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + pnr);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_mainmenu)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_mainmenu)).click();
		log.info("Clicked on the checkin button present on top banner");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
		
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).getAttribute("placeholder").equals("Passenger's Last Name");
		log.info("Passenger Last Name textbox Verified");
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
		log.info("Passenger entering the last name");
		
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).getAttribute("placeholder").equals("Confirmation Code");
		log.info("Confirmation code txt box Verfied");
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
		log.info("Entered the confirmation code as : " + pnr);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.checkin_button)));
		driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
		log.info("Then user clicked on the search button to check the info of the booked ticket");
	}

	@Then("^user validates the caryon bag price for checkin path$")
	public void user_validates_the_caryon_bag_price() throws Throwable {
		
		Assert.assertTrue(" carryon bag price is incorrect",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals("$45"));
		log.info("user succesfully validated the  carryon bag price ");
	}


	@Then("^user Click on the plus button next to MAX checked bags on checkin path$")
	public void user_click_on_the_plus_button_next_to_checked_bags() throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			log.info("Now the bags count is increased to " + (i + 1));

			int BagsCost[] = new int[] { 30, 70, 155, 240, 325 };
			Assert.assertTrue(
					"After increasing the bags count to " + (i + 1) + " the cost is not increased to " + BagsCost[i],
					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
							.equals("$" + BagsCost[i]));
			log.info("The cost increased to " + BagsCost[i] + " for adding the " + (i + 1) + "th bag");
			
		}

	}

//	
//	
	@Then("^Checked bag test$")
	public void Checked_bag_test() throws Throwable {
	
		String BagsCost[] = new String[5];
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			BagsCost[i] = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			log.info("The cost increased to " + BagsCost[i] + " for adding the " + (i + 1) + "th bag");
			
			Assert.assertTrue(
					"Bag Price does not equal Bags total ",
					BagsCost[i].equals(driver.findElement(By.xpath(CheckInPage.BagsTotal_Price)).getText()));
			log.info("Bags " + BagsCost[i] + "price matching Bags Total");
			
			Assert.assertTrue(
					"Bag Price does not equal Standard Pricing total ",
					BagsCost[i].equals(driver.findElement(By.xpath(CheckInPage.Standard_pricing)).getText()));
			log.info("Bags " + BagsCost[i] + "price matching Standard Pricing Total");
		}
		    
}
		@Then("^carry on bag test$")
		public void carry_on_bag_test() throws Throwable {
			
			driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	    	log.info("User clicks carry on button");
	    	
		Assert.assertTrue(
				"Bag total does not match standard Pricing ",
				driver.findElement(By.xpath(CheckInPage.BagsTotal_Price)).getText().equals(driver.findElement(By.xpath(CheckInPage.Standard_pricing)).getText()));
		log.info("Bags total matching standard pricing");
    
	}
		
		@Then("^User clicks on RoundTrip$")
		public void User_clicks_on_RoundTrip() throws Throwable {
			
			Thread.sleep(2000);
			driver.findElement(By.xpath(BookPage.Round_Trip)).click();
		}
		
		@Then("^User clicks yes to travel Guard$")
		public void User_clicks_yes_to_travel_Guard() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.TravelGuard_yes_radio)));
			driver.findElement(By.xpath(CheckInPage.TravelGuard_yes_radio)).click();
		}
		
	    @Then("^user verify Bags pop up does not appear$")
	    public void user_verify_bags_pop_up_does_not_appear() throws Throwable {
	    	  try
			    {
			    	driver.findElement(By.xpath(CheckInPage.BagsPopUp_ProTip));
			    	log.info("Bags popup appears");
			    }
			    catch (NoSuchElementException e)
			    {
			    	log.info("Bags popup Doesnt not appear verified");
			    }
	    }

	    @Then("^user verify cars pop up does appear$")
	    public void user_verify_cars_pop_up_does_appear() throws Throwable {
	    	try
		    {
		    	driver.findElement(By.xpath(CheckInPage.Reserve_A_Car_Popup));
		    	log.info("Reserve a Car popup appears!!!");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Reserve a Car popup Doesnt not appear verified");
		    }
	    }
	    
		@Then("^user clicks no to cars pop up$")
		public void user_clicks_no_to_cars_pop_up() throws Throwable {
			
			Thread.sleep(2000);
			driver.findElement(By.xpath(CheckInPage.Reserve_A_Car_Popup_No_Button)).click();
		}

	    @Then("^user verify Travel Guard pop up does not appear$")
	    public void user_verify_travel_guard_pop_up_does_not_appear() throws Throwable {
	    	try
		    {
		    	driver.findElement(By.xpath(CheckInPage.TravelGuard_Popup));
		    	log.info("Travel guard popup appears");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Travel guard popup Doesnt not appear verified");
		    }
	    }

	    @Then("^user verify Payment Page does not appear$")
	    public void user_verify_payment_page_does_not_appear() throws Throwable {
	    	try
		    {
		    	driver.findElement(By.xpath(CheckInPage.PaymentPage_Header));
		    	log.info("Payment page appears");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Payment page not appear verified");
		    }
	    }
	    
		 @Then("^user clicks on the Booktrip$")
		    public void user_clicks_on_the_booktrip_and_handle_the_travel_more_popup() throws Throwable {
			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Book_trip)));
			 Thread.sleep(3000);
			
			  driver.findElement(By.xpath(OnewaytripPage.Book_trip)).click(); 


			  log.info("user clicks on book trip");
				}
		 
		 @Then("^user clicks on the vacation link$")
		    public void user_clicks_on_the_vacation_link() throws Throwable {
			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Vacation_link)));
			 Thread.sleep(3000);
			
			  driver.findElement(By.xpath(CheckInPage.Vacation_link)).click(); 

			  log.info("user clicks on Vacation Link");
				}
		 
		 
		    @Then("^User inputs drivers age$")
		    public void user_inputs_drivers_age() throws Throwable {
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Vacation_link)));
				 Thread.sleep(3000);
				
				  driver.findElement(By.xpath(CheckInPage.Driver_Age_text_box)).click(); 
				  driver.findElement(By.xpath(CheckInPage.Driver_Age_text_box)).clear(); 
				  driver.findElement(By.xpath(CheckInPage.Driver_Age_text_box)).sendKeys("25");

				  log.info("user inputs drivers age");
		    }
		    
		    @Then("^User clicks on Search Vacations button$")
		    public void User_clicks_on_Search_Vacations_button(){
		    	try {
		    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Search_Vacations_button)));
		        	driver.findElement(By.xpath(CheckInPage.Search_Vacations_button)).click();
		    	} catch (Exception e) {
		    		log.info("Unable to find the element "+e);
		    	}
		    }
		    
			 @Then("^user clicks on Primary passenger is the contact person$")
			    public void user_clicks_on_Primary_passenger_is_the_contact_person() throws Throwable {
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Primary_Passenger_is_The_Contact_Person_CheckBox)));
				 Thread.sleep(3000);
				
				  driver.findElement(By.xpath(CheckInPage.Primary_Passenger_is_The_Contact_Person_CheckBox)).click(); 

				  log.info("user clicks on The primary passenger is the contact person checkbox");
					}
			 
			 @Then("^User selects over age of driver$")
			    public void User_selects_age_of_driver() throws Throwable {
			        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.driver_age)));
			        Select driversage = new Select(driver.findElement(By.xpath(CheckInPage.driver_age)));
			        driversage.selectByIndex(1);
			        log.info("user selects age of 21-24 driver");
			    }
			 
			 
			 @Then("^user clicks book car button$")
			    public void user_clicks_book_car_button() throws Throwable {
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Book_Car_button)));
				 Thread.sleep(3000);
				
				  driver.findElement(By.xpath(CheckInPage.Book_Car_button)).click(); 

				  log.info("user clicks on Book a Car button");
					}
			 
			 @Then("^user clicks Select car button$")
			    public void user_clicks_Select_car_button() throws Throwable {
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Select_Car_button)));
				 Thread.sleep(3000);
				
				  driver.findElement(By.xpath(CheckInPage.Select_Car_button)).click(); 

				  log.info("user clicks Select car button");
					}
			 
			 
			 
				@Then("^user Selects a Car$") 
				public void user_Selects_a_car() throws Throwable {
			        Thread.sleep(2000);
			        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.Spirit_logo)));
					

			        List<WebElement> elements = driver.findElements(By.xpath("//button[@class='btn btn-primary']"));
			        java.util.Iterator<WebElement> i = elements.iterator();
			        while(i.hasNext()) {
			        	WebElement element = i.next();
			        	if (element.isEnabled()) {
			       element.click();
			    	log.info("User booked a car");

			       break;
			        	}
			        }  
					
				}
				
			@Then("^user clicks on Shortcut Boarding$")
		    public void user_clicks_on_Shortcut_Boarding() throws Throwable {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Shortcut_Boarding_Add_button)));
				Thread.sleep(3000);
					
				driver.findElement(By.xpath(CheckInPage.Shortcut_Boarding_Add_button)).click(); 

				log.info("user clicks on Shortcut Boarding");
			}
			
			@Then("^user clicks on Select Seats$")
		    public void user_clicks_on_Select_Seats() throws Throwable {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.SeatsPopUp_Select_Seats_button)));
				Thread.sleep(3000);
					
				driver.findElement(By.xpath(CheckInPage.SeatsPopUp_Select_Seats_button)).click(); 

				log.info("user clicks on Select Seats");
			}
			
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
