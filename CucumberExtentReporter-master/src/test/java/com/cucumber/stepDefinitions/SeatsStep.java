package com.cucumber.stepDefinitions;



import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;

import cucumber.api.java.en.Then;

public class SeatsStep {

	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	@Then("^user validates the city pair and pax names$")
	public void user_validates_the_city_pair_and_pax_names() throws Throwable {

		Assert.assertTrue("User is not  validate the city pair", driver
				.findElement(By.xpath(SeatsPage.first_cities_names_validation)).getText().equals("Denver - Las Vegas"));
		log.info("user succesfully validated the first cities names");

		Assert.assertTrue("User is not  validate the city pair",
				driver.findElement(By.xpath(SeatsPage.second_cities_names_validation)).getText()
						.equals("Las Vegas - San Diego"));
		log.info("user succesfully validated the second cities names");

		Assert.assertTrue("User is not  validate the passenger name",
				driver.findElement(By.xpath(SeatsPage.passenger_name_validation)).getText().equals("jack flayer"));
		log.info("user succesfully validated the passenger name");

	}

	@Then("^user clicks on the exit row on seats page$")
	public void user_clicks_on_the_exit_row_on_seats_page() throws Throwable {

		

		try {
			if(driver.findElement(By.xpath(SeatsPage.Airbus_Name)).isDisplayed()) {
				String flightName = driver.findElement(By.xpath(SeatsPage.Airbus_Name)).getText();
				
				char seatType = 's';
				Assert.assertTrue("Filled the flight seats in Airbus 32A",
						SeatsCommon.seatSelection(flightName, seatType, 5, driver));
			} else {
				Assert.assertTrue("The flight type is not the expected flight type the expected flight types are Airbus 32A, Airbus 32B and Airbus319 ",false);
			}
		} catch (NoSuchElementException e) {
			log.info("The exception while filling the seats : " + e);
		}

	
	}

	@Then("^user clicks on the exit row on seats page for plane two$")
	public void user_clicks_on_the_exit_row_on_seats_page_for_plane_two() throws InterruptedException {

		try {
			if(driver.findElement(By.xpath(SeatsPage.second_flight_carrot)).isDisplayed() && driver.findElement(By.xpath(SeatsPage.Airbus_Name)).isDisplayed() ) {
				String flightName = driver.findElement(By.xpath(SeatsPage.Airbus_Name)).getText();
				char seatType = 's';
				Assert.assertTrue("Filled the flight seats in Airbus 32A",
						SeatsCommon.seatSelection(flightName, seatType, 5, driver));
			} else {
				Assert.assertTrue("The flight type is not the expected flight type the expected flight types are Airbus 32A, Airbus 32B and Airbus319 ",false);
			}
		} catch (NoSuchElementException e) {
			log.info("The exception while filling the seats : " + e);
		}
	}

	@Then("^user clicks on the continue button on seats page$")
	public void user_clicks_on_the_continue_button_on_seats_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(SeatsPage.seats_page_continue_button)));

		driver.findElement(By.xpath(SeatsPage.seats_page_continue_button)).click();
		log.info("user succesfully clicks on the seats page continue button");
	}

	
	@Then("^user selects the exit row seat for palne one and accept the popup$")
	public void user_selects_the_exit_row_seat_for_palne_one_and_accept_the_popup() throws Throwable {

		String s = driver.findElement(By.xpath(SeatsPage.Airbus_Name)).getText();
		log.info("flight name " + s);

		if (s.equalsIgnoreCase("Airbus 32A")) {
			driver.findElement(By.xpath(SeatsPage.Airtbus_32_A_Exit_12A)).click();
			log.info("user succesfully clicks on the Airbus 32A exit seat");
		} else if (s.equalsIgnoreCase("Airbus 32B")) {

			driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Exit_Seat_11A)).click();
			log.info("user succesfully clciks on the Airbus 32B exit seat");
		} else if (s.equalsIgnoreCase("Airbus 319")) {
			driver.findElement(By.xpath(SeatsPage.Airbus_319_Exit_Seat_11A)).click();
			log.info("user succesfully clciks on the Airbus 319 exit seat");

		} else {
			Assert.assertFalse("The flight filling is failed with the exception", false);
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.Airbus_32_A_exitrow_popup)));
		driver.findElement(By.xpath(SeatsPage.Airbus_32_A_exitrow_popup)).click();
		log.info("user succefully handle the popup and accept it");
	}

	@Then("^user selects the exit row seat for plane two and accept the popup$")
	public void user_selects_the_exit_row_seat_for_plane_two_and_accept_the_popup() throws Throwable {

		Thread.sleep(3000);
		String s = driver.findElement(By.xpath(SeatsPage.Airbus_Name)).getText();
		log.info("flight name " + s);

		if (s.equalsIgnoreCase("Airbus 32A")) {
			driver.findElement(By.xpath(SeatsPage.Airtbus_32_A_Exit_12A)).click();
			log.info("user succesfully clicks on the Airbus 32A exit seat");
		} else if (s.equalsIgnoreCase("Airbus 32B")) {

			driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Exit_Seat_11A)).click();
			log.info("user succesfully clciks on the Airbus 32B exit seat");
		} else if (s.equalsIgnoreCase("Airbus 319")) {
			driver.findElement(By.xpath(SeatsPage.Airbus_319_Exit_Seat_11A)).click();
			log.info("user succesfully clciks on the Airbus 319 exit seat");

		} else {
			Assert.assertFalse("The flight filling is failed with the exception", false);
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.Airbus_32_A_exitrow_popup)));
		driver.findElement(By.xpath(SeatsPage.Airbus_32_A_exitrow_popup)).click();
		log.info("user succefully handle the popup and accept it");
		
	}

	@Then("^user validates the total price content block at seats total$")
	public void user_validates_the_total_price_content_block_at_seats_total() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.seats_page_seats_total_caret)));
		driver.findElement(By.xpath(SeatsPage.seats_page_seats_total_caret)).click();
		
		Assert.assertTrue("User not validates the first flight seat total price",
				driver.findElement(By.xpath(SeatsPage.first_city_total_price_validation)).getText().equals("Total: 17"));
		log.info("user succesfully validated the first seat total price ");
		
		Assert.assertTrue("User not validates the second flight seat total price",
				driver.findElement(By.xpath(SeatsPage.second_city_total_price_validation)).getText().equals("Total: 4"));
		log.info("user succesfully validated the first seat total price ");
		
	}
		
	
	@Then("^user selects the one child$")
    public void user_selects_the_one_child() throws Throwable {
		
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		log.info("user succesfully clicks on the adults minus button");
		driver.findElement(By.xpath(BookPage.select_child)).click();
		log.info("user successfully selects the one child");
		
		
    }

    @Then("^user clicks on the search flight button and switch to the popup and enters the date of birth of the UMNR child$")
    public void user_clicks_on_the_search_flight_button_and_switch_to_the_popup_and_enters_the_date_of_birth_of_the_umnr_child() throws Throwable {
    	String parentHandle = driver.getWindowHandle(); 
    	driver.findElement(By.xpath(BookPage.Search_button)).click(); 

    	for (String winHandle : driver.getWindowHandles()) {
    	    driver.switchTo().window(winHandle); 
    	}

    	driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2006");
        log.info("user succesfully enters the child date of birth");
    
        driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
    	driver.switchTo().window(parentHandle);

    	for (String winHandle : driver.getWindowHandles()) {
    	    driver.switchTo().window(winHandle); 
    	}
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.child_umnr_popup_accept)));
    	driver.findElement(By.xpath(SeatsPage.child_umnr_popup_accept)).click();
    	log.info("user succesfully clicks on the umnr popup accept button");
    	
    }
	 
    @Then("^user enters the passenger information$")
    public void user_enters_the_passenger_information() throws Throwable {
       
        Thread.sleep(3000);
 		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
// 		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).click();
// 		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.DOWN);
// 		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.ENTER);
 		
 		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
 		s.selectByIndex(1);

 		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("Anthony");
 		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("Cardona");
 		
 		
 		log.info("user succesfully enter the primary passengers information");
    	
    	
    }
    @Then("^user attempts to selects the Exit row seat$")
    public void user_attempts_to_selects_the_exit_row_seat() throws Throwable {
//    	Assert.assertTrue("User should not select the exit row seat",
//				driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Standard_Seat_26D)).isEnabled());
//    	log.info("user successfully validate the seat is enabled or not ");
//   
//    		driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Standard_Seat_12B)).click();
    	
    	for (int i = 0; i <= 10; i++) {
			String seatxpath[] = {
					
					SeatsPage.Airtbus_32_A_Exit_12A, SeatsPage.Airtbus_32_A_Exit_12B,SeatsPage.Airtbus_32_A_Exit_12C,SeatsPage.Airtbus_32_A_Exit_12D,SeatsPage.Airtbus_32_A_Exit_12E,SeatsPage.Airtbus_32_A_Exit_12F,SeatsPage.Airtbus_32_A_Exit_13A,
					SeatsPage.Airtbus_32_A_Exit_13B,SeatsPage.Airtbus_32_A_Exit_13C,SeatsPage.Airtbus_32_A_Exit_13D,SeatsPage.Airtbus_32_A_Exit_13E,SeatsPage.Airtbus_32_A_Exit_13F	
					
			};
			if(driver.findElement(By.xpath(seatxpath[i])).isSelected()) {
				
				driver.findElement(By.xpath(seatxpath[i])).click();
				log.info("user succesfully clicks on the exit row seat button on the airbus");
			}
			else {
				driver.findElement(By.xpath(SeatsPage.Airbus_319_Big_Front_Seat_1F)).click();
				log.info("user succesfully clicks on the big front seat");
				Thread.sleep(2000);
//				driver.switchTo().alert().accept();
			}
			}
				
			}
    	
    	
    @Then("^user naviagte down to seats total content block and validate the price$")
    public void user_naviagte_down_to_seats_total_content_block_and_validate_the_price() throws Throwable {
       
    	
    	Assert.assertTrue("User not validates the cities names ",
    			driver.findElement(By.xpath(SeatsPage.first_cities_names_validation)).getText().equals("Denver (DEN) - Fort Lauderdale (FLL)"));
		log.info("user succesfully validated the city names");
    
    	
    	
    }

    @Then("^user clciks on the itinerary caret and validate the bags amount$")
    public void user_clciks_on_the_itinerary_caret_and_validate_the_bags_amount() throws Throwable {
       
    	driver.findElement(By.xpath(SeatsPage.seats_page_seats_total_caret)).click();
    	Assert.assertTrue("User not validates the  seat total price",
				driver.findElement(By.xpath(SeatsPage.first_city_total_price_validation)).getText().equals("Total: 19"));
		log.info("user succesfully validated the seats total price ");
    
    	
    	
    	
    }
    @Then("^User validates the SEATS TOTAl content block has a Black background$")
    public void user_validates_the_seats_total_content_block_has_a_black_background() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.SEATS_TOATL_Content_Block)));
    	log.info(driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block)).getCssValue("background-color"));
    	Assert.assertTrue("User could not validate the SEATS TOTAL Content Block Background Color",
				driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block)).getCssValue("background-color")
						.equals("rgba(0, 0, 0, 1)"));
		log.info("User succesfully validated the SEATS TOTAL Content Block Background Color");
    }
    
    
    @Then("^User Validates the SEATS TOTAL content block White Font$")
    public void user_validates_the_seats_total_content_block_white_font() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)));
    	log.info(driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)).getCssValue("color"));
    	Assert.assertTrue("User could not validate the SEATS TOTAL Content Block text font Color",
				driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)).getCssValue("color")
						.equals("rgba(255, 255, 255, 1)"));
		log.info("User succesfully validated the SEATS TOTAL Content Text Font Color");
    }
    
    
    @Then("^User Validates the Continue Button has a Blue background$")
    public void user_validates_the_continue_button_has_a_blue_background() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.CONTINUE_CONTENT_BLOCK)));
    	log.info(driver.findElement(By.xpath(SeatsPage.CONTINUE_CONTENT_BLOCK)).getCssValue("background-color"));
    	Assert.assertTrue("User could not validate the CONTINUE Block background Color",
				driver.findElement(By.xpath(SeatsPage.CONTINUE_CONTENT_BLOCK)).getCssValue("background-color")
						.equals("rgba(0, 115, 230, 1)"));
		log.info("User succesfully validated the CONTINUE Block background Color");
    }
    
    
    @Then("^User Validates the Continue Button has white font$")
    public void user_validates_the_continue_button_has_white_font() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)));
    	log.info(driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)).getCssValue("color"));
    	Assert.assertTrue("User could not validate the CONTINUE Content Block text font Color",
				driver.findElement(By.xpath(SeatsPage.SEATS_TOATL_Content_Block_SEATS_TOTAL_TEXT)).getCssValue("color")
						.equals("rgba(255, 255, 255, 1)"));
		log.info("User succesfully validated the CONTINUE Content Text Font Color");
    }
    
    
    @Then("^user validates the blue font on CONTINUE WITHOUT SELECTING SEATS$")
    public void user_validates_the_blue_font_on_continue_without_selecting_seats() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.CONTINUE_WITHOUT_SELECTING_SEATS_FONT_VALIDATION)));
    	log.info(driver.findElement(By.xpath(SeatsPage.CONTINUE_WITHOUT_SELECTING_SEATS_FONT_VALIDATION)).getCssValue("color"));
    	Assert.assertTrue("User could not validate the CONTINUE WITHOUT SELECTING SEATS Text Font Color",
				driver.findElement(By.xpath(SeatsPage.CONTINUE_WITHOUT_SELECTING_SEATS_FONT_VALIDATION)).getCssValue("color")
						.equals("rgba(0, 115, 230, 1)"));
		log.info("User succesfully validated the CONTINUE WITHOUT SELECTING SEATS Text Font Color");
    }

}
