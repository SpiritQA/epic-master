package com.cucumber.stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.SeatsPage;



public class SeatsCommon {

	private static final Logger log = LogManager.getLogger();
	
//	WebDriver driver = MySharedClass.getDriver();
//	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
//			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	public static boolean seatSelection(String flightname, char seatType, int seatCount,WebDriver driver) {
		boolean seatFillFlag = false;
//		int seatrow = 0;
		int seatsPerEachRow = 6;
		int selectSeatCount = 0;
		if(flightname.equalsIgnoreCase("Airbus 32A")) {
			switch(seatType) {
			case 'b' :
				for(int i=0; i<=seatCount-1;i++) {
					String BFS_seatxpath[] = {SeatsPage.Airbus_32_A_BFS_1A,SeatsPage.Airbus_32_A_BFS_1C,SeatsPage.Airbus_32_A_BFS_1D,SeatsPage.Airbus_32_A_BFS_1F,SeatsPage.Airbus_32_A_BFS_2A,SeatsPage.Airbus_32_A_BFS_2C,SeatsPage.Airbus_32_A_BFS_2D,SeatsPage.Airbus_32_A_BFS_2F};
					driver.findElement(By.xpath(BFS_seatxpath[i])).click();
				}
				break;
			case 's' :
//				for(int i=0; i<=seatCount-1;i++) {
//					String seatxpath[] = {SeatsPage.Airbus_32_A_5A,SeatsPage.Airbus_32_A_5E,SeatsPage.Airbus_32_A_6C,SeatsPage.Airbus_32_A_7F,SeatsPage.Airbus_32_A_8D,SeatsPage.Airbus_32_A_10B,SeatsPage.Airbus_32_A_11B,SeatsPage.Airbus_32_A_10E};
					
//					driver.findElement(By.xpath(seatxpath[i])).click();
				for (int seatrow=1; ((seatrow<8 || seatrow<2 || seatrow<25 || seatrow<11 || seatrow<40 || seatrow<25)&& selectSeatCount<=seatCount );seatrow++) {
					for (int eachseat=1; eachseat <=seatsPerEachRow; eachseat++) {
						String common_xpath = "//div[@class='reanimate-seat-rows ng-star-inserted']["+seatrow+"]//app-unit["+eachseat+"]";
						if (!driver.findElement(By.xpath(common_xpath)).isSelected()) {
							driver.findElement(By.xpath(common_xpath)).click();
							selectSeatCount++;
						}
					}
					
				}
					
					
//				}
				break;
			case 'e' :
				for(int i=0; i<=seatCount-1;i++) {
					String seatxpath[] = {SeatsPage.Airtbus_32_A_Exit_12A,SeatsPage.Airtbus_32_A_Exit_12B,SeatsPage.Airtbus_32_A_Exit_12C,SeatsPage.Airtbus_32_A_Exit_12D,SeatsPage.Airtbus_32_A_Exit_12E,SeatsPage.Airtbus_32_A_Exit_12F,SeatsPage.Airtbus_32_A_Exit_13A,SeatsPage.Airtbus_32_A_Exit_13B};
					
					driver.findElement(By.xpath(seatxpath[i])).click();
					
				}
				break;
			}
			seatFillFlag = true;
		} else if(flightname.equalsIgnoreCase("Airbus 32B")) {
			/*switch(seatType) {
			case 'b' :
				driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Big_Front_Seat_1A)).click();
				break;
			case 's' :
				driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Standard_Seat_9C)).click();
				break;
			case 'e' :
				driver.findElement(By.xpath(SeatsPage.Airbus_32_B_Exit_Seat_11E)).click();
				break;
			
			}*/
			switch(seatType) {
			case 'b' :
				for(int i=0; i<=seatCount-1;i++) {
					String BFS_seatxpath[] = {SeatsPage.Airbus_32_B_Big_Front_Seat_1A,SeatsPage.Airbus_32_B_Big_Front_Seat_1B,SeatsPage.Airbus_32_B_Big_Front_Seat_1E,SeatsPage.Airbus_32_B_Big_Front_Seat_1F,SeatsPage.Airbus_32_B_Big_Front_Seat_2A,SeatsPage.Airbus_32_B_Big_Front_Seat_2B,SeatsPage.Airbus_32_B_Big_Front_Seat_2E,SeatsPage.Airbus_32_B_Big_Front_Seat_2F};
					driver.findElement(By.xpath(BFS_seatxpath[i])).click();
				}
				break;
			case 's' :
				for(int i=0; i<=seatCount-1;i++) {
					String seatxpath[] = {SeatsPage.Airbus_32_B_Standard_Seat_3A,SeatsPage.Airbus_32_B_Standard_Seat_3B,SeatsPage.Airbus_32_B_Standard_Seat_3C,SeatsPage.Airbus_32_B_Standard_Seat_3D,SeatsPage.Airbus_32_B_Standard_Seat_3E,SeatsPage.Airbus_32_B_Standard_Seat_3F,SeatsPage.Airbus_32_B_Standard_Seat_4A,SeatsPage.Airbus_32_B_Standard_Seat_4B};
					driver.findElement(By.xpath(seatxpath[i])).click();
				}
				break;
			case 'e' :
				for(int i=0; i<=seatCount-1;i++) {
					String seatxpath[] = {SeatsPage.Airbus_32_B_Exit_Seat_11A,SeatsPage.Airbus_32_B_Exit_Seat_11B,SeatsPage.Airbus_32_B_Exit_Seat_11C,SeatsPage.Airbus_32_B_Exit_Seat_11D,SeatsPage.Airbus_32_B_Exit_Seat_11E,SeatsPage.Airbus_32_B_Exit_Seat_11F,SeatsPage.Airbus_32_B_Exit_Seat_25A};
					driver.findElement(By.xpath(seatxpath[i])).click();
				}
				break;
			}
			seatFillFlag = true;
		} else if (flightname.equalsIgnoreCase("Airbus 319")) {
			
			switch(seatType) {
			case 'b' :
				for(int i=0; i<=seatCount-1;i++) {
					String BFS_seatxpath[] = {SeatsPage.Airbus_319_Big_Front_Seat_1A,SeatsPage.Airbus_319_Big_Front_Seat_1B,SeatsPage.Airbus_319_Big_Front_Seat_1E,SeatsPage.Airbus_319_Big_Front_Seat_1F,SeatsPage.Airbus_319_Big_Front_Seat_2A,SeatsPage.Airbus_319_Big_Front_Seat_2B,SeatsPage.Airbus_319_Big_Front_Seat_2E,SeatsPage.Airbus_319_Big_Front_Seat_2F};
					driver.findElement(By.xpath(BFS_seatxpath[i])).click();
				}
				break;
			case 's' :
				for(int i=0; i<=seatCount-1;i++) {
					String seatxpath[] = {SeatsPage.Airbus_319_Standard_Seat_4A,SeatsPage.Airbus_319_Standard_Seat_4B,SeatsPage.Airbus_319_Standard_Seat_4C,SeatsPage.Airbus_319_Standard_Seat_5A,SeatsPage.Airbus_319_Standard_Seat_5B,SeatsPage.Airbus_319_Standard_Seat_5C,SeatsPage.Airbus_319_Standard_Seat_5D,SeatsPage.Airbus_319_Standard_Seat_5E};
					driver.findElement(By.xpath(seatxpath[i])).click();
				}
				break;
			case 'e' :
				for(int i=0; i<=seatCount-1;i++) {
					Assert.assertTrue("The given number of seats are more than 6, hence cannot fill the given number of seats", seatCount>6);
					String seatxpath[] = {SeatsPage.Airbus_319_Exit_Seat_11A,SeatsPage.Airbus_319_Exit_Seat_11B,SeatsPage.Airbus_319_Exit_Seat_11C,SeatsPage.Airbus_319_Exit_Seat_11D,SeatsPage.Airbus_319_Exit_Seat_11E,SeatsPage.Airbus_319_Exit_Seat_11F};
					driver.findElement(By.xpath(seatxpath[i])).click();
				}
				break;
			}
			seatFillFlag = true;
		} else {
			Assert.assertTrue("The flights displayed are not Airbus32A, Airbus32B or Airbus319 ",false);
			return seatFillFlag;
		}
		return seatFillFlag;
	}
}
