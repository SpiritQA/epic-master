package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage320;


import cucumber.api.java.en.Then;
import managers.FileReaderManager;

public class BagsStep {
	private static final Logger log = LogManager.getLogger();
	static WebDriver driver = MySharedClass.getDriver();
	private FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	private static String carryon_Bag = FileReaderManager.getInstance().getTestDataReader().carryonBagprice();
	private static String Checked_bag_prices = FileReaderManager.getInstance().getTestDataReader().checkedBagprice();
	private static String Checked_bag_prices_Minus_Button = FileReaderManager.getInstance().getTestDataReader().checkedBagpriceMinus();
	
	private static String NineFC_valid_emailaddress = FileReaderManager.getInstance().getTestDataReader().Ninefcemail();
	private static String NineFC_Valid_Password = FileReaderManager.getInstance().getTestDataReader().Ninepassword();
	private static String ninedfc_carryon_Bag = FileReaderManager.getInstance().getTestDataReader().ninedfccarryonBagprice();
	private static String ninedfc_Checked_bag_prices = FileReaderManager.getInstance().getTestDataReader().ninedfccheckedBagprice();
	private static String ninedfc_Checked_bag_prices_Minus_Button = FileReaderManager.getInstance().getTestDataReader().ninedfccheckedBagpriceMinus();
	
	private static String military_ninedfc_email = FileReaderManager.getInstance().getTestDataReader().militaryninedfcemail();
	
	
	
	@Then("^user Click on the plus button next to carryon bag$")
	public void user_click_on_the_plus_button_next_to_carryon_bag() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		
	}

	@Then("^user validates the caryon bag price$")
	public void user_validates_the_caryon_bag_price() throws Throwable {

		String s = driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		log.info("carryon bag price is " + s);
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(s));
		log.info("user succesfully validated the  carryon bag price ");
	}

	@Then("^user Click on the minus button next to carryon bag$")
	public void user_click_on_the_minus_button_next_to_carryon_bag() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_minus_button)).click();
		String carry_on_bag_price_validation=driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the  carryon bag price ");
		
	}

	@Then("^user Click on the plus button next to checked bags$")
	public void user_click_on_the_plus_button_next_to_checked_bags() throws Throwable {
	
		 for (int i = 0; i < 4; i++) {
		 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();


		 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
		 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

		 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
		 log.info("user successfully validate the checked bags prices ");
		 }
		
		
	/*	
		String bagsCost = Checked_bag_prices;
		String[] bagsCostArray = bagsCost.split(",");
		int[] BagsCost = new int[bagsCostArray.length];
		for (int l = 0; l < bagsCostArray.length; l++) {
			BagsCost[l] = Integer.parseInt(bagsCostArray[l]);
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			log.info("Now the bags count is increased to " + (l + 1));

			Assert.assertTrue(
					
					"After increasing the bags count to " + (l + 1) + " the cost is not increased to " + BagsCost[l],
					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
							.equals("$" + BagsCost[l]));
			log.info("The cost increased to " + BagsCost[l] + " for adding the " + (l + 1) + "th bag");
		}

		String bagsCostminus = Checked_bag_prices_Minus_Button;
		String[] PricesArray1 = bagsCostminus.split(",");
		int[] BagsCostminus = new int[PricesArray1.length];
		for(int m=PricesArray1.length-1;m>=0;m--) {
			BagsCostminus[m] = Integer.parseInt(PricesArray1[m]);
		}
		for (int i = PricesArray1.length-1; i>0; i--) {
			//BagsCostminus[i] = Integer.parseInt(PricesArray1[i]);
			driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();
			log.info("Now the bags count is dicreased to " + (i - 1));

			Assert.assertTrue(
					"After decreasing the bags count to " + (i - 1) + " the cost is not decreased to "
							+ BagsCostminus[i - 1],
					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
							.equals("$" + BagsCostminus[i - 1]));
			log.info("The cost decreased to " + BagsCostminus[i - 1] + " for removing the " + (i) + "th bag");
		}
		*/


		 for (int i = 4; i != 0; i--) {
				driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();
				String BagsCost= driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
				log.info("The cost Decrease to " + BagsCost + " for removing the " + (i - 1) + "th bag");
				 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(BagsCost));
				 log.info("user successfully validate the checked bags prices when its dicreasing  ");
			}

}

	@Then("^user clicks on the adding sporting equipment$")
	public void user_clicks_on_the_adding_sporting_equipment() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(BagsPage.add_sporting_equipment)));
		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();
	}

	@Then("^user click on the plus and minus buttons next to the bicycle and validate the prices$")
	public void user_click_on_the_plus_and_minus_button_next_to_the_bicycle_and_validate_the_prices() throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.bicycle_plus_buton)).click();
			log.info("Now the bicycle count is increased to " + (i + 1));
			String bicycle_prices = driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText();
			log.info("The cost increased to " + bicycle_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the bicycle prices",driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText().equals(bicycle_prices));
					
		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.bicycle_minus_buton)).click();
			log.info("Now the bags count is decreased to " + (i - 1));
			String bicycle_prices = driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText();
			log.info("The cost Decrease to " + bicycle_prices + " for removing the " + (i - 1) + "th bag");
			Assert.assertTrue("user not validate the bicycle prices",driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText().equals(bicycle_prices));
		}
	}

	@Then("^user clicks on the plus and minus buttons next to the surfboard and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_surfboard_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			
			driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(Surfboard_prices));
		}
		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.surfboard_minus_button)).click();
			log.info("Now the Surfboard count is decreased to " + (i - 1));
			String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
			log.info("The cost remove to " + Surfboard_prices + " for removing the " + (i - 1) + "th bag");
				Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(Surfboard_prices));
		}
	}

	@Then("^user clicks on the plus and minus buttons next to the forty one to fifty lbs and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_forty_one_to_fifty_lbs_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_plus_button)).click();
			log.info("Now the 41-50lbs count is increased to " + (i + 1));
			int fortyonetofiftyCost[] = new int[] { 30, 60, 90, 120, 150 };
			Assert.assertTrue(
					"After increasing the 41-50lbs count to " + (i + 1) + " the cost is not increased to "
							+ fortyonetofiftyCost[i],
					driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_price_validation)).getText()
							.equals("$" + fortyonetofiftyCost[i]));
			log.info("The cost increased to " + fortyonetofiftyCost[i] + " for adding the " + (i + 1) + "th 41-50lbs");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_minus_button)).click();
			log.info("Now the 41-50lbs count is decreased to " + (i - 1));
			int fortytofiftycost[] = new int[] { 0, 30, 60, 90, 120, 150 };
			Assert.assertTrue(
					"After decreasing the 41-50lbs count to " + (i - 1) + " the cost is not decreased to "
							+ fortytofiftycost[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_price_validation)).getText()
							.equals("$" + fortytofiftycost[i - 1]));
			log.info("The cost decreased to " + fortytofiftycost[i - 1] + " for removing the " + (i) + "th bag");
		}
	}

	@Then("^user clicks on the plus and minus buttons next to the fifty one to seventy lbs and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_fifty_one_to_seventy_lbs_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.weight_51_70_lbs_plus_button)).click();
			log.info("Now the 51-70lbs count is increased to " + (i + 1));
			int fiftyonetoseventy[] = new int[] { 55, 110, 165, 220, 275 };
			Assert.assertTrue(
					"After increasing the 51-70lbs count to " + (i + 1) + " the cost is not increased to "
							+ fiftyonetoseventy[i],
					driver.findElement(By.xpath(BagsPage.weight_51_70_price_validation)).getText()
							.equals("$" + fiftyonetoseventy[i]));
			log.info("The cost increased to " + fiftyonetoseventy[i] + " for adding the " + (i + 1) + "th 51-70lbs");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.weight_51_70_lbs_minus_button)).click();
			log.info("Now the 51-70lbs count is decreased to " + (i - 1));
			int fiftyonetoseventy[] = new int[] { 0, 55, 110, 165, 220, 275 };
			Assert.assertTrue(
					"After decreasing the 51-70lbs count to " + (i - 1) + " the cost is not decreased to "
							+ fiftyonetoseventy[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_51_70_price_validation)).getText()
							.equals("$" + fiftyonetoseventy[i - 1]));
			log.info("The cost decreased to " + fiftyonetoseventy[i - 1] + " for removing the " + (i) + "th 51-70lbs");

		}
	}

	@Then("^user clicks on the plus and minus buttons next to the seventy one to hundred lbs and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_seventy_one_to_hundred_lbs_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.weight_71_100_lbs_plus_button)).click();
			log.info("Now the 71-100lbs count is increased to " + (i + 1));
			int seventyonetohundred[] = new int[] { 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After increasing the 71-100lbs count to " + (i + 1) + " the cost is not increased to "
							+ seventyonetohundred[i],
					driver.findElement(By.xpath(BagsPage.weight_71_100_price_validation)).getText()
							.equals("$" + seventyonetohundred[i]));
			log.info("The cost increased to " + seventyonetohundred[i] + " for adding the " + (i + 1) + "th 71-100lbs");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.weight_71_100_lbs_minus_button)).click();
			log.info("Now the 71-100lbs count is decreased to " + (i - 1));
			int seventyonetohundred[] = new int[] { 0, 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After decreasing the 71-100lbs count to " + (i - 1) + " the cost is not decreased to "
							+ seventyonetohundred[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_71_100_price_validation)).getText()
							.equals("$" + seventyonetohundred[i - 1]));
			log.info(
					"The cost decreased to " + seventyonetohundred[i - 1] + " for removing the " + (i) + "th 71-100lb");

		}
	}

	@Then("^user clicks on the plus and minus buttons next to the sixty eight one to eighty lbs and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_sixty_eight_one_to_eighty_lbs_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.weight_68_80_lbs_plus_button)).click();
			log.info("Now the 68-80inches count is increased to " + (i + 1));
			int sixtyeighttoeighty[] = new int[] { 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After increasing the 68-80inches count to " + (i + 1) + " the cost is not increased to "
							+ sixtyeighttoeighty[i],
					driver.findElement(By.xpath(BagsPage.weight_68_80_price_validation)).getText()
							.equals("$" + sixtyeighttoeighty[i]));
			log.info(
					"The cost increased to " + sixtyeighttoeighty[i] + " for adding the " + (i + 1) + "th 68-80inches");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.weight_68_80_lbs_minus_button)).click();
			log.info("Now the 68-80inches count is decreased to " + (i - 1));
			int sixtyeighttoeighty[] = new int[] { 0, 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After decreasing the 68-80inches count to " + (i - 1) + " the cost is not decreased to "
							+ sixtyeighttoeighty[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_68_80_price_validation)).getText()
							.equals("$" + sixtyeighttoeighty[i - 1]));
			log.info("The cost decreased to " + sixtyeighttoeighty[i - 1] + " for removing the " + (i)
					+ "th 68-80inches");
		}
	}

	@Then("^user clicks on the plus and minus buttons next to the special item and validates the prices$")
	public void user_clicks_on_the_plus_and_minus_buttons_next_to_the_special_item_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.Special_item_plus_button)).click();
			log.info("Now the special item count is increased to " + (i + 1));
			int specialitem[] = new int[] { 150, 300, 450, 600, 750 };
			Assert.assertTrue(
					"After increasing the special item count to " + (i + 1) + " the cost is not increased to "
							+ specialitem[i],
					driver.findElement(By.xpath(BagsPage.special_item_price_validation)).getText()
							.equals("$" + specialitem[i]));
			log.info("The cost increased to " + specialitem[i] + " for adding the " + (i + 1) + "th special item");

		}
		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.Special_item_minus_button)).click();
			log.info("Now the special item count is decreased to " + (i - 1));
			int sixtyeighttoeighty[] = new int[] { 0, 150, 300, 450, 600, 750 };
			Assert.assertTrue(
					"After decreasing the special item count to " + (i - 1) + " the cost is not decreased to "
							+ sixtyeighttoeighty[i - 1],
					driver.findElement(By.xpath(BagsPage.special_item_price_validation)).getText()
							.equals("$" + sixtyeighttoeighty[i - 1]));
			log.info("The cost decreased to " + sixtyeighttoeighty[i - 1] + " for removing the " + (i)
					+ "th special item");

		}
	}

	@Then("^user clicks the text box next to the carryon bags and enter the number of bags and validate the price$")
	public void user_clicks_the_text_box_next_to_the_carryon_bags_and_enter_the_number_of_bags_and_validate_the_price()
			throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).click();
		driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).clear();
		driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).sendKeys("1");
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals("$35"));
		log.info("user succesfully validated the  carryon bag price ");
	}

	@Then("^user clicks on the continue without adding bags$")
	public void user_clicks_on_the_continue_without_adding_bags() throws Throwable {

		FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		String FocusonBagsPage = driver.getWindowHandle(); // get the current window handle
		driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user clicks on the continue wihout adding the bags");
		for (String iNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(iNeedBags); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_i_need_bags)));
		driver.findElement(By.xpath(BagsPage.popup_i_need_bags)).click();

		// close newly opened window when done with it
		driver.switchTo().window(FocusonBagsPage); // switch back to the original window
		log.info("user clicks on the popup window i need bags and navigate back to the parent window");
	}

	@Then("^user add the bags to the carryon and checkin and validates the total price and clicks on continue without adding bags$")
	public void user_add_the_bags_to_the_carryon_and_checkin_and_validates_the_total_price_and_clicks_on_continue_without_adding_bags()
			throws Throwable {

		String FocusonBagsPage = driver.getWindowHandle(); // get the current window handle
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.continue_withou_bags)));
		driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user clicks on the continue wihout adding the bags");
		for (String iNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(iNeedBags); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_i_need_bags)));
		driver.findElement(By.xpath(BagsPage.popup_i_need_bags)).click();

		// close newly opened window when done with it
		driver.switchTo().window(FocusonBagsPage); // switch back to the original window
		log.info("user clicks on the popup window i need bags and navigate back to the parent window");
		// FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5,
		// TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		// driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).clear();
		// driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).sendKeys("1");

		log.info("user succesfully add the carryon bag");
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		// driver.findElement(By.xpath(BagsPage.checked_bag_Text_input)).sendKeys("3");
		log.info("user succesfully add the checked bags ");
		Thread.sleep(3000);
		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		
		String shopping_cart_bags_prices = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		log.info(" bags total pruce is " + shopping_cart_bags_prices );
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(shopping_cart_bags_prices));
		log.info("user successfully validates the total  bags prices");
		
		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_carryon_bags_price=driver.findElement(By.xpath(BagsPage.total_carry_on_bag_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_carry_on_bag_price)).getText().equals(total_carryon_bags_price));
		log.info("user succesfully validated the  total  carry on bags price ");
		
		String total_checked_bags_price=driver.findElement(By.xpath(BagsPage.total_checked_bag_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_checked_bag_price)).getText().equals(total_checked_bags_price));
		log.info("user succesfully validated the  total  checked bags price ");

		String FocusBagspage_1 = driver.getWindowHandle(); // get the current window handle
		driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user clicks on the continue wihout adding the bags");
		for (String iDontNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(iDontNeedBags); // switch focus of WebDriver to the next found window handle
														// (that's your newly opened window)

		}
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_idont_need_bags)));
		// driver.findElement(By.xpath(BagsPage.popup_idont_need_bags)).click();
		// driver.switchTo().window(FocusBagspage_1);

	}

	@Then("^user enter the carryon bag and checked bag validates the bag total bar and itinerary$")
	public void user_enter_the_carryon_bag_and_checked_bag_validates_the_bag_total_bar_and_itinerary()
			throws Throwable {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();;
		
		log.info("user succesfully add the carryon bag");
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		log.info("user succesfully add the checked bags ");

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		String shopping_cart_bags_prices=driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		log.info("user succesfully clicked on the your itinerary caret");
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(shopping_cart_bags_prices));

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_carryon_bags_price=driver.findElement(By.xpath(BagsPage.total_carry_on_bag_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_carry_on_bag_price)).getText().equals(total_carryon_bags_price));
		log.info("user succesfully validated the  total  carry on bags price ");
		
		String total_checked_bags_price=driver.findElement(By.xpath(BagsPage.total_checked_bag_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_checked_bag_price)).getText().equals(total_checked_bags_price));
		log.info("user succesfully validated the  total  checked bags price ");

	}
	/*
	 * 
	 * Text input in the box
	 */

	@Then("^user clicks the text box next to the checked bags and enter the number of bags and validate the prices$")
	public void user_clicks_the_text_box_next_to_the_checked_bags_and_enter_the_number_of_bags_and_validate_the_prices()
			throws Throwable {
		for (int i = 0; i < 5; i++) {
		 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

		 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
		 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

		 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
		 log.info("user successfully validate the checked bags prices ");

		
		}

		for (int i = 5; i != 0; i--) {

			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost dicreased to " + checked_bag_price + " for removing the " + (i - 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

	
	}

	@Then("^user clicks on the text box next to the bicycle and enter the number of bicycles and validate the prices$")
	public void user_clicks_on_the_text_box_next_to_the_bicycle_and_enter_the_number_of_bicycles_and_validate_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {

			driver.findElement(By.xpath(BagsPage.add_bicycle_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.add_bicycle_Text_input)).sendKeys(j);
			log.info("Now the bicycle count is increased to " + (i + 1));

			int BicycleCost[] = new int[] { 75, 150, 225, 300, 375 };
			Assert.assertTrue(
					"After increasing the bicycle count to " + (i + 1) + " the cost is not increased to "
							+ BicycleCost[i],
					driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText()
							.equals("$" + BicycleCost[i]));
			log.info("The cost increased to " + BicycleCost[i] + " for adding the " + (i + 1) + "th bicycle");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.add_bicycle_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.add_bicycle_Text_input)).sendKeys(j);
			log.info("Now the bicycle count is decreased to " + (i - 1));
			int BicycleCost[] = new int[] { 0, 75, 150, 225, 300, 375 };
			Assert.assertTrue(
					"After decreasing the bicycle count to " + (i - 1) + " the cost is not decreased to "
							+ BicycleCost[i - 1],
					driver.findElement(By.xpath(BagsPage.bicycle_price_validation)).getText()
							.equals("$" + BicycleCost[i - 1]));
			log.info("The cost decreased to " + BicycleCost[i - 1] + " for removing the " + (i) + "th bicycle");
		}

	}

	@Then("^user clicks on the text box next to the surfboard and enter the number of surfboards and validate the prices$")
	public void user_clicks_on_the_text_box_next_to_the_surfboard_and_enter_the_number_of_surfboards_and_validate_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {

			driver.findElement(By.xpath(BagsPage.add_surfboard_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.add_surfboard_Text_input)).sendKeys(j);
			log.info("Now the surfboard count is increased to " + (i + 1));

			int surfboadCost[] = new int[] { 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After increasing the surfboard count to " + (i + 1) + " the cost is not increased to "
							+ surfboadCost[i],
					driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText()
							.equals("$" + surfboadCost[i]));
			log.info("The cost increased to " + surfboadCost[i] + " for adding the " + (i + 1) + "th surfboard");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.add_surfboard_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.add_surfboard_Text_input)).sendKeys(j);
			log.info("Now the bicycle count is decreased to " + (i - 1));
			int surfboadCost[] = new int[] { 0, 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After decreasing the bicycle count to " + (i - 1) + " the cost is not decreased to "
							+ surfboadCost[i - 1],
					driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText()
							.equals("$" + surfboadCost[i - 1]));
			log.info("The cost decreased to " + surfboadCost[i - 1] + " for removing the " + (i) + "th surfboard");
		}
	}

	@Then("^user clicks on the text box next to the forty one to fifty lbs and enter the numbe of bags and validate the prices$")
	public void user_clicks_on_the_text_box_next_to_the_forty_one_to_fifty_lbs_and_enter_the_numbe_of_bags_and_validate_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {

			driver.findElement(By.xpath(BagsPage.overweight_41_50lb_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.overweight_41_50lb_Text_input)).sendKeys(j);
			log.info("Now the 41-50lbs count is increased to " + (i + 1));

			int fotytofiftylbs[] = new int[] { 30, 60, 90, 120, 150 };
			Assert.assertTrue(
					"After increasing the 41-50lbs count to " + (i + 1) + " the cost is not increased to "
							+ fotytofiftylbs[i],
					driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_price_validation)).getText()
							.equals("$" + fotytofiftylbs[i]));
			log.info("The cost increased to " + fotytofiftylbs[i] + " for adding the " + (i + 1) + "th 41-50lbs");
		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.overweight_41_50lb_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.overweight_41_50lb_Text_input)).sendKeys(j);
			log.info("Now the 41-50lbs count is decreased to " + (i - 1));
			int fotytofiftylbs[] = new int[] { 0, 30, 60, 90, 120, 150 };
			Assert.assertTrue(
					"After decreasing the 41-50lbs count to " + (i - 1) + " the cost is not decreased to "
							+ fotytofiftylbs[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_41_50_lbs_price_validation)).getText()
							.equals("$" + fotytofiftylbs[i - 1]));
			log.info("The cost decreased to " + fotytofiftylbs[i - 1] + " for removing the " + (i) + "th 41-50lbs");
		}

	}

	@Then("^user clicks on the text box next to the fifty one to seventy lbs enter the numbe of bags and validates the prices$")
	public void user_clicks_on_the_text_box_next_to_the_fifty_one_to_seventy_lbs_enter_the_numbe_of_bags_and_validates_the_prices()
			throws Throwable {
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.overweight_51_70lb_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.overweight_51_70lb_Text_input)).sendKeys(j);
			log.info("Now the 51-70lbs count is increased to " + (i + 1));

			int fiftyonetoseventylbs[] = new int[] { 55, 110, 165, 220, 275 };
			Assert.assertTrue(
					"After increasing the 51-70lbs count to " + (i + 1) + " the cost is not increased to "
							+ fiftyonetoseventylbs[i],
					driver.findElement(By.xpath(BagsPage.weight_51_70_price_validation)).getText()
							.equals("$" + fiftyonetoseventylbs[i]));
			log.info("The cost increased to " + fiftyonetoseventylbs[i] + " for adding the " + (i + 1) + "th 51-70lbs");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.overweight_51_70lb_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.overweight_51_70lb_Text_input)).sendKeys(j);
			log.info("Now the 51-70lbs count is decreased to " + (i - 1));
			int fiftyonetoseventylbs[] = new int[] { 0, 55, 110, 165, 220, 275 };
			Assert.assertTrue(
					"After decreasing the 51-70lbss count to " + (i - 1) + " the cost is not decreased to "
							+ fiftyonetoseventylbs[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_51_70_price_validation)).getText()
							.equals("$" + fiftyonetoseventylbs[i - 1]));
			log.info("The cost decreased to " + fiftyonetoseventylbs[i - 1] + " for removing the " + (i)
					+ "th 51-70lbs");
		}
	}

	@Then("^user clicks on the text box next to the seventy one to hundred lbs enter the numbe of bags and validates the prices$")
	public void user_clicks_on_the_text_box_next_to_the_seventy_one_to_hundred_lbs_enter_the_numbe_of_bags_and_validates_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.overweight_71_100lb_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.overweight_71_100lb_Text_input)).sendKeys(j);
			log.info("Now the 71-100lbs count is increased to " + (i + 1));

			int seventyonetoundredlbs[] = new int[] { 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After increasing the 71-100lbs count to " + (i + 1) + " the cost is not increased to "
							+ seventyonetoundredlbs[i],
					driver.findElement(By.xpath(BagsPage.weight_71_100_price_validation)).getText()
							.equals("$" + seventyonetoundredlbs[i]));
			log.info("The cost increased to " + seventyonetoundredlbs[i] + " for adding the " + (i + 1)
					+ "th 71-100lbs");

		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.overweight_71_100lb_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.overweight_71_100lb_Text_input)).sendKeys(j);
			log.info("Now the 71-100lbs count is decreased to " + (i - 1));
			int seventyonetoundredlbs[] = new int[] { 0, 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After decreasing the 71-100lbs count to " + (i - 1) + " the cost is not decreased to "
							+ seventyonetoundredlbs[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_71_100_price_validation)).getText()
							.equals("$" + seventyonetoundredlbs[i - 1]));
			log.info("The cost decreased to " + seventyonetoundredlbs[i - 1] + " for removing the " + (i)
					+ "th 71-100lbs");
		}
	}

	@Then("^user clicks on the text box next to the sixty eight to eighty lbs enters the numbe of bags and validates the prices$")
	public void user_clicks_on_the_text_box_next_to_the_sixty_eight_to_eighty_lbs_enters_the_numbe_of_bags_and_validates_the_prices()
			throws Throwable {
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.overweight_60_80_linear_iches_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.overweight_60_80_linear_iches_Text_input)).sendKeys(j);
			log.info("Now the 68-80lbs count is increased to " + (i + 1));

			int sixyeighttoeightylbs[] = new int[] { 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After increasing the 68-80lbs count to " + (i + 1) + " the cost is not increased to "
							+ sixyeighttoeightylbs[i],
					driver.findElement(By.xpath(BagsPage.weight_68_80_price_validation)).getText()
							.equals("$" + sixyeighttoeightylbs[i]));
			log.info("The cost increased to " + sixyeighttoeightylbs[i] + " for adding the " + (i + 1) + "th 68-80lbs");
		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.overweight_60_80_linear_iches_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.overweight_60_80_linear_iches_Text_input)).sendKeys(j);
			log.info("Now the 68-80lbs count is decreased to " + (i - 1));
			int sixyeighttoeightylbs[] = new int[] { 0, 100, 200, 300, 400, 500 };
			Assert.assertTrue(
					"After decreasing the 68-80lbs count to " + (i - 1) + " the cost is not decreased to "
							+ sixyeighttoeightylbs[i - 1],
					driver.findElement(By.xpath(BagsPage.weight_68_80_price_validation)).getText()
							.equals("$" + sixyeighttoeightylbs[i - 1]));
			log.info("The cost decreased to " + sixyeighttoeightylbs[i - 1] + " for removing the " + (i)
					+ "th 68-80lbs");
		}
	}

	@Then("^user clicks on the text box next to the special item enters the number of items and validate the prices$")
	public void user_clicks_on_the_text_box_next_to_the_special_item_enters_the_number_of_items_and_validate_the_prices()
			throws Throwable {

		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.special_items_over80_inches_Text_input)).clear();
			String j = Integer.toString(i + 1);
			driver.findElement(By.xpath(BagsPage.special_items_over80_inches_Text_input)).sendKeys(j);
			log.info("Now the special item count is increased to " + (i + 1));

			int specialitem[] = new int[] { 150, 300, 450, 600, 750 };
			Assert.assertTrue(
					"After increasing the special itemcount to " + (i + 1) + " the cost is not increased to "
							+ specialitem[i],
					driver.findElement(By.xpath(BagsPage.special_item_price_validation)).getText()
							.equals("$" + specialitem[i]));
			log.info("The cost increased to " + specialitem[i] + " for adding the " + (i + 1) + "th special item");
		}

		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.special_items_over80_inches_Text_input)).clear();
			String j = Integer.toString(i - 1);
			driver.findElement(By.xpath(BagsPage.special_items_over80_inches_Text_input)).sendKeys(j);
			log.info("Now the special item count is decreased to " + (i - 1));
			int specialitem[] = new int[] { 0, 150, 300, 450, 600, 750 };
			Assert.assertTrue(
					"After decreasing the special item count to " + (i - 1) + " the cost is not decreased to "
							+ specialitem[i - 1],
					driver.findElement(By.xpath(BagsPage.special_item_price_validation)).getText()
							.equals("$" + specialitem[i - 1]));
			log.info("The cost decreased to " + specialitem[i - 1] + " for removing the " + (i) + "th special item");
		}
	}

	@Then("^user clicks on the more information link at the botom of the page and validate the information$")
	public void user_clicks_on_the_more_information_link_at_the_botom_of_the_page_and_validate_the_information()
			throws Throwable {
		// FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5,
		// TimeUnit.SECONDS)
		// .ignoring(NoSuchElementException.class);

		driver.findElement(By.xpath(BagsPage.More_infomation_link)).click();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));

		String url = driver.getCurrentUrl();
		log.info("user succesully get the url" + url);

		driver.close();
		log.info("user succesfully closes the new tab");
		driver.switchTo().window(tabs2.get(0));
		log.info("user succesfully navigate back to the bags page ");

	}

	@Then("^user clicks on the embargo restrictions link at the bottom of the page and valiadte the page and navigate back to bags page$")
	public void user_clicks_on_the_embargo_restrictions_link_at_the_bottom_of_the_page_and_valiadte_the_page_and_navigate_back_to_bags_page()
			throws Throwable {
		FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.embargo_restrictions)));
		driver.findElement(By.xpath(BagsPage.embargo_restrictions)).click();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));

		String url = driver.getCurrentUrl();
		log.info("user succesully get the url" + url);

		driver.close();
		log.info("user succesfully closes the new tab");
		driver.switchTo().window(tabs2.get(0));
		log.info("user succesfully navigate back to the bags page ");

	}

	@Then("^user adds the one carryon and five checked bags$")
	public void user_adds_the_one_carryon_and_five_checked_bags() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();

		log.info("user succesfully added the carryon bag");
		for (int i = 0; i < 5; i++) {
			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}
	}

	@Then("^user validates the bags total with the shopping cart$")
	public void user_validates_the_bags_total_with_the_shopping_cart() throws Throwable {
		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
	String 	your_itinerary_bags_total=driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validate the prices with the drop down pricing$")
	public void user_validate_the_prices_with_the_drop_down_pricing() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_bags_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total  carry on bags price ");
		


	}

	@Then("^user selects the one carryon three checked bags one bike and two surfboards$")
	public void user_selects_the_one_carryon_three_checked_bags_one_bike_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		log.info("user succesfully selected the one carryon bag");
		for (int i = 0; i < 3; i++) {
			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");
			 }

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();
		log.info("user succesfully clicked on the add sporting equipment");

		driver.findElement(By.xpath(BagsPage.bicycle_plus_buton)).click();
		log.info("user succesfully selected the one bicycle");

		for (int i = 0; i < 1; i++) {
			driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
			log.info("Now the surfboard count is increased to " + (i + 1));

		String surf_board_price=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
		 log.info("The cost increased to " + surf_board_price + " for adding the " + (i + 1) + "th bag");
		 Assert.assertTrue("user not validate the cheked bags prices",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(surf_board_price));
		 log.info("user successfully validate the checked bags prices ");
			
		}

	}

	@Then("^user validates the bags total with the above shopping cart$")
	public void user_validates_the_bags_total_with_the_above_shopping_cart() throws Throwable {
		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		String your_itinerary_bags_total = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased", driver
				.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validates the dropdown total prices$")
	public void user_validates_the_dropdown_total_prices() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_bags_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total  carry on bags price ");
		
		
	}

	@Then("^user adds the one carry on four checked bags and two surfboards$")
	public void user_adds_the_one_carry_on_four_checked_bags_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		log.info("user succesfully selected the one carryon bag");
		for (int i = 0; i < 4; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			log.info("Now the bags count is increased to " + (i + 1));

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");
		}

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();
		log.info("user succesfully clicks on the sporting equipment");

		for (int i = 0; i < 1; i++) {
			driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(Surfboard_prices));
		}
		}

	@Then("^user validates the bag total with the above shopping cart$")
	public void user_validates_the_bag_total_with_the_above_shopping_cart() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		String your_itinerary_bags_total=driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validates the dropdown total price$")
	public void user_validates_the_dropdown_total_price() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	String total_bags_price=	driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^primary passenger add the one carryon four checked bags and two surfboards$")
	public void primary_passenger_add_the_one_carryon_four_checked_bags_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		for (int i = 0; i < 4; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");


		}

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();

		log.info("primary passenger succesfully click on the add sporting equipment ");
		for (int i = 0; i < 1; i++) {
			driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(Surfboard_prices));
		}

	}

	@Then("^passenger two add the one carryon four checked bags and two surfboards$")
	public void passenger_two_add_the_one_carryon_four_checked_bags_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.passenger_two_carryonbag_plusbutton)).click();
		log.info("user succesfully clicks on the carryon bag");

		for (int i = 0; i < 4; i++) {
			 driver.findElement(By.xpath(BagsPage.passenger_checkedbag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText();
//			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

		driver.findElement(By.xpath(BagsPage.passneger_two_adding_sport_equipment)).click();
		log.info("passenger two succesfully clicks on the add sporting equipment");
		for (int i = 0; i < 1; i++) {
			driver.findElement(By.xpath(BagsPage.passenger_two_surfboard_plusbutton)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.passebger_two_surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.passebger_two_surfboard_price_validation)).getText().equals(Surfboard_prices));
		}

	}

	@Then("^passengers validates the price in the cart$")
	public void passenger_two_validates_the_price_in_the_cart() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
	String your_itinerary_bags_total=	driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");
	}

	@Then("^passengers validates  the total bags price$")
	public void passenger_two_validates_the_total_bags_price() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	String total_bags_price=	driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^primary passenger add the one carryon three checked bags and two surfboards$")
	public void primary_passenger_add_the_one_carryon_three_checked_bags_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		for (int i = 0; i < 3; i++) {
			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();

		log.info("primary passenger succesfully click on the add sporting equipment ");
		for (int i = 0; i < 2; i++) {
			driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals(Surfboard_prices));
		}

	}

	@Then("^passenger two add the one carryon three checked bags and two surfboards$")
	public void passenger_two_add_the_one_carryon_three_checked_bags_and_two_surfboards() throws Throwable {

		driver.findElement(By.xpath(BagsPage.passenger_two_carryonbag_plusbutton)).click();
		log.info("user succesfully clicks on the carryon bag");

		for (int i = 0; i < 3; i++) {
			 driver.findElement(By.xpath(BagsPage.passenger_checkedbag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");


		}
		driver.findElement(By.xpath(BagsPage.passneger_two_adding_sport_equipment)).click();
		log.info("passenger two succesfully clicks on the adding sporting equipment");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.passenger_two_surfboard_plusbutton)));

		for (int i = 0; i < 2; i++) {
			driver.findElement(By.xpath(BagsPage.passenger_two_surfboard_plusbutton)).click();
			log.info("Now the sufboard count is increased to " + (i + 1));
		String Surfboard_prices=	driver.findElement(By.xpath(BagsPage.passebger_two_surfboard_price_validation)).getText();
		log.info("The cost adding to " + Surfboard_prices + " for adding the " + (i + 1) + "th bag");
			Assert.assertTrue("user not validate the surfboard prices ",driver.findElement(By.xpath(BagsPage.passebger_two_surfboard_price_validation)).getText().equals(Surfboard_prices));
		}

	}

	@Then("^passengers validates the prices in the cart$")
	public void passengers_validates_the_prices_in_the_cart() throws Throwable {
		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		String your_itinerary_bags_total= driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^passengers validates  the total bags prices$")
	public void passengers_validates_the_total_bags_prices() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_bags_price =driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^primary passenger add the one carryon three checked bags$")
	public void primary_passenger_add_the_one_carryon_three_checked_bags() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		log.info("user succesfully clicks on the carryon bag");

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		for (int i = 0; i < 3; i++) {
			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

	}

	@Then("^passenger two add the one carryon three checked bags$")
	public void passenger_two_add_the_one_carryon_three_checked_bags() throws Throwable {
//      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.passenger_two_carryonbag_plusbutton)));
//		driver.findElement(By.xpath(BagsPage.passenger_two_carryonbag_plusbutton)).click();
//		log.info("passeneger two succesfully clicks on the one carryon bag ");
		
		for (int i = 0; i < 3; i++) {
		
			 driver.findElement(By.xpath(BagsPage.passenger_checkedbag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

	}

	@Then("^passengers validate the prices in the cart$")
	public void passengers_validate_the_prices_in_the_cart() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
	String your_itinerary_bags_total =	driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^passengers validate  the total bags prices$")
	public void passengers_validate_the_total_bags_prices() throws Throwable {
		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_bags_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^primary passneger selects one carryon bag$")
	public void primary_passneger_selects_one_carryon_bag() throws Throwable {

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		log.info("user succesfully added the carryon bag ");
		String carry_on_bag_price_validation=driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the  carryon  bags price ");

	}

	@Then("^passenger two selects the one carryon bag$")
	public void passenger_two_selects_the_one_carryon_bag() throws Throwable {

		driver.findElement(By.xpath(BagsPage.passenger_two_carryonbag_plusbutton)).click();
		log.info("passenger two  succesfully added the one carryon bag");
	String passneger_two_carryonbag_price_validation=	driver.findElement(By.xpath(BagsPage.passneger_two_carryonbag_price_validation)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased", driver
				.findElement(By.xpath(BagsPage.passneger_two_carryonbag_price_validation)).getText().equals(passneger_two_carryonbag_price_validation));
		log.info("user succesfully validated the  carryon  bags price ");
	}

	@Then("^user validate the bags prices in the cart$")
	public void user_validate_the_bags_prices_in_the_cart() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		String 	your_itinerary_bags_total=	driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validate the bags prices in the total bags dropdown$")
	public void user_validate_the_bags_prices_in_the_total_bags_dropdown() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	String total_bags_price=	driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}
	

	@Then("^user validates the cities names$")
	public void user_validates_the_cities_names() throws Throwable {

		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.city_names_validation)).getText()
						.equals("Denver (DEN) - Fort Lauderdale (FLL)"));
		log.info("user succesfully validated the cities names ");

	}

	@Then("^primary passneger add the one surfboard$")
	public void primary_passneger_add_the_one_surfboard() throws Throwable {

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();
		log.info("primary passneger succesfully clicks on the add sporting equipment");
		driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
		log.info("user succesfully add the one surfboard");

		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.surfboard_price_validation)).getText().equals("$100"));
		log.info("user succesfully validated the surfboard prices ");
	}

	@Then("^passenger two add the one surfboard$")
	public void passenger_two_add_the_one_surfboard() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.passneger_two_adding_sport_equipment)));
		driver.findElement(By.xpath(BagsPage.passneger_two_adding_sport_equipment)).click();
		log.info("passenger two clicks on the add sporting equipment");
		driver.findElement(By.xpath(BagsPage.passenger_two_surfboard_plusbutton)).click();
		log.info("passenger two succesfully added the surfboard");

	}

	@Then("^user validate the bag price in cart$")
	public void user_validate_the_bag_price_in_cart() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
	String 	your_itinerary_bags_total=driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validate the price in  total bags price$")
	public void user_validate_the_price_in_total_bags_price() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String total_bags_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^user selects the five adult passnegers$")
	public void user_selects_the_five_adult_passnegers() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.select_adults)));
		
		for (int i = 0; i < 4; i++) {
			driver.findElement(By.xpath(PassengerinfoPage.select_adults)).click();
            log.info("Now the passenger count is " + (i + 1));
        }
	
	

	}

	@Then("^user enters the five passnegers information$")
	public void user_enters_the_five_passnegers_information() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flyer");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("12/12/1989");
		log.info("user succesfully enters the primary passneger information ");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
		Select s1 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s1.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Anthony");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("cardona");
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("12/24/1988");
		log.info("user successfully enters the passneger two information");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_three_Title)));
		Select s2 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_three_Title)));
		s2.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_first_name)).sendKeys("stephen");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_last_name)).sendKeys("hawking");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_dateof_birth)).sendKeys("12/14/1988");
		log.info("user successfully enters the passneger three information");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_four_Title)));
		Select s3 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_four_Title)));
		s3.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_first_name)).sendKeys("steve");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_last_name)).sendKeys("jobs");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_dateof_birth)).sendKeys("12/10/1988");
		log.info("user successfully enters the passneger four information");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_five_Title)));
		Select s4 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_five_Title)));
		s4.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_first_name)).sendKeys("alfonso");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_last_name)).sendKeys("canels");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_dateof_birth)).sendKeys("12/10/1988");
		log.info("user successfully enters the passneger five information");

	}

	@Then("^user added one surfboard to the passneger one$")
	public void user_added_one_surfboard_to_the_passneger_one() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.add_sporting_equipment)));

		driver.findElement(By.xpath(BagsPage.add_sporting_equipment)).click();
		driver.findElement(By.xpath(BagsPage.surfboard_plus_button)).click();
		log.info("primary user selected the one surfboard");

	}

	@Then("^user added the one surfboard to the passenger four$")
	public void user_added_the_one_surfboard_to_the_passenger_four() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passneger_four_adding_sport_equipment)));

		driver.findElement(By.xpath(PassengerinfoPage.passneger_four_adding_sport_equipment)).click();
		driver.findElement(By.xpath(PassengerinfoPage.passenger_four_plus_surfboard)).click();
		log.info("passneger four selected the one surfboard");
	}

	@Then("^user validate the shopping cart bags price$")
	public void user_validate_the_shopping_cart_bags_price() throws Throwable {
		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicked on the your itinerary caret");
		
	String your_itinerary_bags_total=	driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validate the bags prices in itinerary cart");

	}

	@Then("^user validate the dropdown total bags price$")
	public void user_validate_the_dropdown_total_bags_price() throws Throwable {

		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		
		String Bags_total_button = driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(Bags_total_button));
		log.info("user succesfully validated the  total bags price ");

	}

	@Then("^user selects the travel date$")
	public void user_selects_the_travel_date() throws Throwable {

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 3); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Date)));
		driver.findElement(By.xpath(BookPage.Date)).click();

		driver.findElement(By.xpath(BookPage.Date)).clear();
		driver.findElement(By.xpath(BookPage.Date)).sendKeys(DepartureDate);

	}

	@Then("^user validate the city pair names$")
	public void user_validate_the_city_pair_names() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.city_names_validation)));
	String city_names_validation=	driver.findElement(By.xpath(BagsPage.city_names_validation)).getText();
		Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
				driver.findElement(By.xpath(BagsPage.city_names_validation)).getText().equals(city_names_validation));
		log.info("user succesfully validated the cities names ");

	}

	@Then("^user validates the bags prices when increased and dicreased$")
	public void user_validates_the_bgas_prices_when_increased_and_dicreased() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		String carry_on_bag_price_validation=driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the  carryon bag price ");
		driver.findElement(By.xpath(BagsPage.carry_on_bag_minus_button)).click();
		log.info("user successfully clicks on the carryon bags minus button");
	String carry_on_bag_price=	driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price));
		log.info("user succesfully validated the  carryon bag price ");

		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		log.info("user succesfully selects the one carryon bag ");

		for (int i = 0; i < 5; i++) {
			 driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

		for (int i = 5; i != 0; i--) {
			 driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();

			 String checked_bag_price =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i - 1) + "th bag");

			 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price));
			 log.info("user successfully validate the checked bags prices ");

		}

	}

	@Then("^user validates the total bags price$")
	public void user_validates_the_total_bags_price() throws Throwable {
		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.total_bags_price)));
	String total_bags_price=	driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
		log.info("user succesfully validated the total carryon bag price ");
	}

	@Then("^user clicks on the caret and validates the bags price$")
	public void user_clicks_on_the_caret_and_validates_the_bags_price() throws Throwable {

		driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
		log.info("user succesfully clicks on the itinerary caret");
		String your_itinerary_bags_total=driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_bags_total)));
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(your_itinerary_bags_total));
		log.info("user succesfully validated the itinerary carryon bag price ");

	}

	@Then("^use clicks on the nine FC continue button and switch to the popup and validate the text on the popup$")
	public void use_clicks_on_the_nine_fc_continue_button_and_switch_to_the_popup_and_validate_the_text_on_the_popup()
			throws Throwable {

		String parentHandle = driver.getWindowHandle(); // get the current window handle
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.NineFC_Continue_button)));
		driver.findElement(By.xpath(BagsPage.NineFC_Continue_button)).click();
		log.info("user succesfully clicks on the nine fc continue button");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); 
		}

		String s = driver.findElement(By.xpath(BagsPage.NineFC_popup_text_validation)).getText();
		log.info(s);

	
	driver.switchTo().window(parentHandle);

	}

	

    @Then("^user selects the one carryon bag$")
    public void user_selects_the_one_carryon_bag() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		String carry_on_bag_price_validation=driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the  carryon bag price ");
    	
    	
    	
    }

    @Then("^user clicks on the sigup button on the popup$")
    public void user_clicks_on_the_sigup_button_on_the_popup() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.NineFC_popup_sigup_button)));
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_sigup_button)).click();
    	log.info("user succesfully clicks on the signup button");
    }

    @Then("^user enters the invalid password$")
    public void user_enters_the_invalid_password() throws Throwable {
        
    	
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_signup_password)).sendKeys("spirit11!");
    	log.info("user succesfully enters the password");
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_signup_confirm_password)).sendKeys("spirit11!");
    	log.info("user succesfully enters the confirm password");
    }

    @Then("^user clicks on the signup with the email$")
    public void user_clicks_on_the_signup_with_the_email() throws Throwable {
    	
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_signup_confirmwith_email)).click();
    	log.info("user succesfully clicks on the signup with the email");
       
    }

    @Then("^user validates the error message on the popup$")
    public void user_validates_the_error_message_on_the_popup() throws Throwable {
    	
    	Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.NineFC_popup_signup_error_text)).getText().equals("Password must be at least 8 characters, no more than 16 characters, must include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user succesfully validated the error text ");
    	
       
    }
    @Then("^user clicks on the continue with the standard fare button$")
    public void user_clicks_on_the_continue_with_the_standard_fare_button() throws Throwable {
       
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_signup_continue_with_statndardfare)).click();
    	log.info("user succesfully clicks on the cintinue with the statndard fare button");
    	
    	
    }

    @Then("^user clicks on the login button on the popup$")
    public void user_clicks_on_the_login_button_on_the_popup() throws Throwable {
        
    	driver.findElement(By.xpath(BagsPage.NineFC_popup_login_button)).click();
    	log.info("user succesfully clicks on the login button");
    	
    	
    }
    
    @Then("^user clicks on continue with bare fare button$")
    public void user_clicks_on_continue_with_bare_fare_button() throws Throwable {
    	String parentHandle = driver.getWindowHandle(); 
    	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Continue_Button)));
    	  WebElement element = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
    	  JavascriptExecutor executor = (JavascriptExecutor)driver;
    	  executor.executeScript("arguments[0].click();", element);
    	  
		  for (String winHandle : driver.getWindowHandles()) {
		      driver.switchTo().window(winHandle); 
		  }
		  Thread.sleep(2000);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Continue_With_Bare_Fare_buttton)));
		driver.findElement(By.xpath(BagsPage.Continue_With_Bare_Fare_buttton)).click();
		log.info("user successfully clicked on CONTINUE WITH BARE FARE BUTTON");
		 driver.switchTo().window(parentHandle);
    }

    @Then("^user clicks on choose this bundle button$")
    public void user_clicks_on_choose_this_bundle_button() throws Throwable {
    	String parentHandle = driver.getWindowHandle(); 
		  for (String winHandle : driver.getWindowHandles()) {
		      driver.switchTo().window(winHandle); 
		  }
		  Thread.sleep(5000);
		
		driver.findElement(By.xpath(BagsPage.Choose_This_Bundle_buttton)).click();
		log.info("user successfully clicked CHOOSE THIS BUNDLE BUTTON");
		driver.switchTo().window(parentHandle);
    }

    @Then("^user clicks on choose thrills combo button$")
    public void user_clicks_on_choose_thrills_combo_button() throws Throwable {
    	String parentHandle = driver.getWindowHandle(); 
		  for (String winHandle : driver.getWindowHandles()) {
		      driver.switchTo().window(winHandle); 
		  }
		  Thread.sleep(5000);
		driver.findElement(By.xpath(BagsPage.Choose_Thrills_Combo_buttton)).click();
		log.info("user successfully clicked CHOSSE THRILLS COMBO BUTTON");
		driver.switchTo().window(parentHandle);
    }
    
    @Then("^user clicks on dynamic shopping cart carrot$")
    public void user_clicks_on_dynamic_shopping_cart_carrot() throws Throwable {
    	driver.findElement(By.xpath(BagsPage.ShoppingCart_carrot)).click();
    }
    
    @Then("^user Click continue underneath the NineDFC Banner$")
    public void user_click_continue_underneath_the_ninedfc_banner() throws Throwable {
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.NineFC_Continue_button)));
    	driver.findElement(By.xpath(BagsPage.NineFC_Continue_button)).click();;
    }
    @Then("^user Verify One carryon and One Checkin is selected$")
    public void user_verify_one_carryon_and_one_checkin_is_selected() throws Throwable {
    	Assert.assertTrue(
				"User does not have one carry on ",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).getAttribute("value").
				equals("1"));
		log.info("Carryon verified");
		
		Assert.assertTrue(
				"User does not have one carry on ",
				driver.findElement(By.xpath(BagsPage.checked_bag_Text_input)).getAttribute("value").
				equals("1"));
		log.info("Check bag verified");
    }

    @Then("^user Verify price increases and decrease for each bag added$")
    public void user_Verify_price_increases_and_decrease_for_each_bag_added() throws Throwable {
    	String BagsCost[] = new String[4];
		for (int i = 0; i < 4; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			BagsCost[i] = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			log.info("The cost increased to " + BagsCost[i] + " for adding the " + (i + 1) + "th bag");
			
			String ShoppingCartBag = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
			String ShoppingCartBagPrice = ShoppingCartBag.replace(".00","");
			Assert.assertTrue(
					"Shopping Cart does not equal Bags total ",
					ShoppingCartBagPrice.equals(driver.findElement(By.xpath(BagsPage.BagsTotal)).getText()));
			log.info("Shopping Cart matching bag price " + ShoppingCartBagPrice);
		}
		
		for (int i = 4; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();
			log.info("Now the bags count is decreased to " + (i - 1));
//			int BagsCost[] = new int[] { 0, 30, 70, 155, 240, 325 };
			String ShoppingCartBag = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
			String ShoppingCartBagPrice = ShoppingCartBag.replace(".00","");
			Assert.assertTrue(
					"Shopping Cart does not equal Bags total ",
					ShoppingCartBagPrice.equals(driver.findElement(By.xpath(BagsPage.BagsTotal)).getText()));
			log.info("Shopping Cart matching bag price " + ShoppingCartBagPrice);
		}
    }
    
    @Then("^user Verify One carryon is selected$")
    public void user_verify_one_carryon_is_selected() throws Throwable {
    	Assert.assertTrue(
				"User does not have one carry on ",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_Text_input)).getAttribute("value").
				equals("1"));
		log.info("Carryon verified");
	}
    
    @Then("^user Click on the plus button next to checked bags and validates he could only get one checkbag$")
    public void user_click_on_the_plus_button_next_to_checked_bags_and_validates_he_could_only_get_one_checkbag() throws Throwable {
    	
    	WebElement CheckBagPlusButton = driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton));
    	CheckBagPlusButton.click();
    	
    	Assert.assertTrue(
				"User does not have one Checkin bag ",
				driver.findElement(By.xpath(BagsPage.checked_bag_Text_input)).getAttribute("value").
				equals("1"));
		log.info("Check bag verified");
		
//		 if(CheckBagPlusButton.isEnabled()){
//		        log.info("Warning!!checked bag plusbutton still Enabled ");
//		  }else{
//			  log.info("checked bag plus button is disabled.");
//		  }
    }
    
    @Then("^user check if element is enabled$")
    public void user_check_if_element_is_enabled() throws Throwable {
    	WebElement CheckBagPlusButton = driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton_disabled));
    	 if(CheckBagPlusButton.isEnabled()){
    		 log.info("Warning!!checked bag plusbutton still Enabled ");
		  }else{
			  log.info("checked bag plus button is disabled.");
		  }
    }
    
    
    @Then("^user clicks on Active Duty Military Personal Checkbox$")
    public void user_clicks_on_active_duty_military_personal_checkbox() throws Throwable {
    	Thread.sleep(3000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)));
    	driver.findElement(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)).click();
    	log.info("User clicked on Active Duty U.S. Military Personnel");
    }
    
    @Then("^user verifies military user get one free carry on$")
    public void user_verifies_military_user_get_one_free_carry_on() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	String carry_on_bag_price_validation=	driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the carryon bag price for military memeber");
		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		String bags_total_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		Assert.assertTrue(
				"Bag Price does not equal Bags total ",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().
				equals(bags_total_price));
		log.info("Bags total matching bag price");
    }

    @Then("^user verifies military user gets two free check bags$")
    public void user_verifies_military_user_gets_two_free_check_bags() throws Throwable {
    	for (int i = 0; i < 2; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			String checked_bag_price_validation =driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			Assert.assertTrue("User not validates the Checkbag price",
					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(checked_bag_price_validation));
			log.info("user succesfully validated the Check in bag price for military member");;
			
		
    	}
    }
    @Then("^user Verify price increases for Check bag bag added$")
    public void user_verify_price_increases_for_check_bag_bag_added() throws Throwable {
    	
		for (int i = 2; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			String BagsCost= driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			log.info("The cost increased to " + BagsCost + " for adding the " + (i + 1) + "th bag");
			
			String ShoppingCartBag = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
			String ShoppingCartBagPrice = ShoppingCartBag.replace(".00","");
			
					ShoppingCartBagPrice.equals(driver.findElement(By.xpath(BagsPage.BagsTotal)).getText());
			log.info("Shopping Cart matching bag price " + ShoppingCartBagPrice);
		}
	
    }
    
	@Then("^user enters the personal information for Second adult military memeber$")
	public void user_enters_the_personal_information_for_Second_adult_military_member() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Jack");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Flyer");
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("01/02/1981");
		
		log.info("user succesfully enters information for second Pax");
	}
	
    @Then("^user clicks on Active Duty Military Personal Checkbox for passenger two$")
    public void user_clicks_on_active_duty_military_personal_checkbox_for_passenger_two() throws Throwable {
    	driver.findElement(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)).click();
    	log.info("User clicked on Active Duty U.S. Military Personnel");
    }
    @Then("^user Verify I dont need bags Pop up$")
    public void user_verify_i_dont_need_bags_pop_up() throws Throwable {
    	String parentHandle = driver.getWindowHandle(); 
  	  for (String winHandle : driver.getWindowHandles()) {
	      driver.switchTo().window(winHandle); 
	  }
    	
//    	Assert.assertTrue(
//				"Bags pop up contain wrong text",
//				driver.findElement(By.xpath(BagsPage.BagsPopupH2)).getText().
//				equals("Are you sure?"));
//		log.info("Bags pop up contain correct text");
//    	String S = driver.findElement(By.xpath(BagsPage.BagsPopupH2)).getText();
//    	log.info("the value of s is "+ S);
  	  
  	  
  	if(driver.getPageSource().contains("Are you sure?"))
  	{
  	   log.info("user validated the text on the popup");
  	}

  	else
  	{
  		 log.info("user doesn't validated the text on the popup");
  	}
		
//		Assert.assertTrue(
//				"Bags pop up body contain wrong text",
//				driver.findElement(By.xpath(BagsPage.BagsPopupBody)).getText().
//				equals(" You really don't need bags? If you change your mind, they'll only be more expensive at the airport. "));
//		log.info("Bags pop up body contain correct text");
  	
  	if(driver.getPageSource().contains("You really don't need bags? If you change your mind, they'll only be more expensive at the airport."))
  	{
  	   log.info("user validated the text on the popup");
  	}

  	else
  	{
  		 log.info("user doesn't validated the text on the popup");
  	}
    	driver.switchTo().window(parentHandle);
    }
    
	@Then("^user login as NineDFC member$")
	public void user_login_as_NineDFC_member() throws Throwable {
			
			Thread.sleep(5000);
			 
			for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
			}
			driver.findElement(By.xpath(BookPage.signIn_Header)).click();
			
			driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(NineFC_valid_emailaddress);

			driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(NineFC_Valid_Password);

			driver.findElement(By.xpath(BookPage.login_button_onpopup)).click();

			log.info("user successfully loged in as a 9DFC spirit member");
	}
    
	@Then("^user validates the caryon bag price for NineDFC$")
	public void user_validates_the_caryon_bag_price_for_NineDFC() throws Throwable {
	String carry_on_bag_price_validation= 	driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
		Assert.assertTrue("User not validates the carryon_bag price",
				driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carry_on_bag_price_validation));
		log.info("user succesfully validated the  carryon bag price ");
	}
	
	@Then("^user Click on the plus button next to checked bags for NineDFC$")
	public void user_click_on_the_plus_button_next_to_checked_bags_for_NineDFC() throws Throwable {
//		String bagsCost = ninedfc_Checked_bag_prices;
//		String[] bagsCostArray = bagsCost.split(",");
//		int[] BagsCost = new int[bagsCostArray.length];
//		for (int l = 0; l < bagsCostArray.length; l++) {
//			BagsCost[l] = Integer.parseInt(bagsCostArray[l]);
//			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
//			log.info("Now the bags count is increased to " + (l + 1));
//
//			Assert.assertTrue(
//					
//					"After increasing the bags count to " + (l + 1) + " the cost is not increased to " + BagsCost[l],
//					driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
//							.equals("$" + BagsCost[l]));
//							
//			log.info("The cost increased to " + BagsCost[l] + " for adding the " + (l + 1) + "th bag");
//		
//			Assert.assertTrue(
//					"Bag Price does not equal Bags total ",
//					driver.findElement(By.xpath(CheckInPage.BagsTotal_Price)).getText().
//					equals(driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()));
//			log.info("Bags total matching bag price");
//			
//			String ShoppingCartBag = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
//			String ShoppingCartBagPrice = ShoppingCartBag.replace(".00","");
//			
//			Assert.assertTrue(
//					"Shopping Cart does not equal Bags total ",
//					
//					ShoppingCartBagPrice.equals(driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()));
//			log.info("Shopping Cart matching bag price " + ShoppingCartBagPrice);
//		}
//			
//		
//		
//		String bagsCostminus = ninedfc_Checked_bag_prices_Minus_Button;
//        String[] PricesArray1 = bagsCostminus.split(",");
//        int[] BagsCostminus = new int[PricesArray1.length];
//        for(int m=PricesArray1.length-1;m>=0;m--) {
//            BagsCostminus[m] = Integer.parseInt(PricesArray1[m]);
//        }
//        for (int i = PricesArray1.length-1; i>0; i--) {
//            //BagsCostminus[i] = Integer.parseInt(PricesArray1[i]);
//            driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();
//            log.info("Now the bags count is dicreased to " + (i - 1));
//
//            Assert.assertTrue(
//                    "After decreasing the bags count to " + (i - 1) + " the cost is not decreased to "
//                            + BagsCostminus[i - 1],
//                    driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()
//                            .equals("$" + BagsCostminus[i - 1]));
//            log.info("The cost decreased to " + BagsCostminus[i - 1] + " for removing the " + (i) + "th bag");
//            
//			Assert.assertTrue(
//					"Bag Price does not equal Bags total ",
//					driver.findElement(By.xpath(CheckInPage.BagsTotal_Price)).getText().equals(driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText()));
//			log.info("Bags total matching bag price");
//        }
		
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
			String BagsCost= driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			log.info("The cost increased to " + BagsCost + " for adding the " + (i + 1) + "th bag");
			
			String ShoppingCartBag = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
			String ShoppingCartBagPrice = ShoppingCartBag.replace(".00","");
			
					ShoppingCartBagPrice.equals(driver.findElement(By.xpath(BagsPage.BagsTotal)).getText());
			log.info("Shopping Cart matching bag price " + ShoppingCartBagPrice);
		}
		for (int i = 5; i != 0; i--) {
			driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();
			String BagsCost= driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
			log.info("The cost Decrease to " + BagsCost + " for removing the " + (i - 1) + "th bag");
        }

}
	
	
	@Then("^user login as Military_NineDFC member$")
	public void user_login_as_Military_NineDFC_member() throws Throwable {
			
			Thread.sleep(5000);
			 
			for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
			}
			driver.findElement(By.xpath(BookPage.signIn_Header)).click();
			
			driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(military_ninedfc_email);

			driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(NineFC_Valid_Password);

			driver.findElement(By.xpath(BookPage.login_button_onpopup)).click();

			log.info("user successfully loged in as a Military 9DFC spirit member");
			
		
	}
	
	
	
	
	  @Then("^User choose the To city LIM$")
	    public void User_choose_the_To_city_LIM() throws Throwable{
	    	
	    	driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	    	
//	    	Random ran = new Random();
//		List<WebElement> cities = driver.findElements(By.xpath(BookPage.TO_city));
//			
//			int city=ran.nextInt(cities.size())+ 1;
//			
//			cities.get(city).click();
//			Thread.sleep(3000);
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LIM)));
	       driver.findElement(By.xpath(SeatsPage320.TO_city_LIM)).click();
	    }
	
	
	
	  @Then("^validate city pair of passenger$")
	    public void validate_city_pair_of_passenger() throws Throwable {
	        Thread.sleep(2000);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.city_names_validation)));
	        String citypair = driver.findElement(By.xpath(BagsPage.city_names_validation)).getText();
	        if (citypair.equals("Los Angeles (LAX) - Fort Lauderdale (FLL)")) {
	            log.info("Selected Departure Cities " + citypair);
	        } else {
	            log.info("Selected Departure Cities not matched" + citypair);
	        }
	        // String city=BookPageStep.depcitypair();
	        // if(citypair.equals(city)){
	        // log.info("Citypair of Itinerary is validated");}
	        // else {
	        // log.info("Citypair of Itinerary is Invalid"); }
	    }
	
	  @Then("^User validates save money of 9DFC member in itinerary$")
	    public void User_validates_save_money_of_9DFC_member_in_itinerary() {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Save_money_9dfc)));
	        WebElement savemoney = driver.findElement(By.xpath(BagsPage.Save_money_9dfc));
	        String savings = savemoney.getText();
	        if (savings.startsWith("Join $9FC")) {
	            log.info("The price in the $9FC block is validated" + savings);
	        } else {
	            log.info("The price in the $9FC block is not discounted");
	        }
	    }
	  @Then("^user validates the caryon bag price when choose thrills combo$")
	    public void user_validates_the_caryon_bag_price_when_choose_thrills_combo() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_price_validation)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
	        log.info("Carry-on bag price=" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price",
	                driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carrybagprice));
	        log.info("user successfully validated the  carryon bag price ");
	    }
	  
	  @Then("^user adds the one carryon and one checked bags$")
	    public void user_adds_the_one_carryon_and_one_checked_bags() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	        log.info("user successfully added the carryon bag");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.checked_bag_plusbutton)));
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        log.info("user successfully added the CHECKED bag");
	    }
	  @Then("^user enter the carryon bag and checked bag validates the bag total bar and itinerary with thrills combo$")
	    public void user_enter_the_carryon_bag_and_checked_bag_validates_the_bag_total_bar_and_itinerary_with_thrills_combo()
	            throws Throwable {
	        FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
	                .ignoring(NoSuchElementException.class);
	        log.info("user successfully add the carryon bag");
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        log.info("user successfully add the checked bags ");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
	        String totalbagsprice = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
	        log.info("Total Bags Price " + totalbagsprice);
	        log.info("user successfully clicked on the your itinerary caret");
	        Thread.sleep(2000);
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        String total_bags_price =driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
	        log.info("user successfully validated the  total bags price ");
	 
	    }
	  
	  @Then("^user clicks on the continue with 9DFC conitnue button and signup$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_signup() throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
	        driver.findElement(By.xpath(BagsPage.popup_signup)).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_choose_pwd)));
	        WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_choose_pwd));
	        choosepwd.click();
	        choosepwd.clear();
	        choosepwd.sendKeys("Spirit123!");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_confirm_pwd)));
	        WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_confirm_pwd));
	        confirmpwd.click();
	        confirmpwd.clear();
	        confirmpwd.sendKeys("Spirit123@");
	        confirmpwd.sendKeys(Keys.TAB);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup_emailbtn)));
	        WebElement signupbtn = driver.findElement(By.xpath(BagsPage.popup_signup_emailbtn));
	        String errormsg = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	        Assert.assertTrue("User unable to get the 9DFC signup error",
	                errormsg.equals("must match"));
	        driver.switchTo().window(FocusonBagsPage);
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
	  
	  
	  
	  
	  @Then("^user clicks on the continue with adding bags$")
	    public void user_clicks_on_the_continue_with_adding_bags() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.continue_with_bags)));
	        driver.findElement(By.xpath(BagsPage.continue_with_bags)).click();
	        log.info("Use clicks Standard Pricing Continue button");
	    }
	  
	  @Then("^user validates the caryon bag price with bundle$")
	    public void user_validates_the_caryon_bag_price_with_bundle() throws Throwable {
		  driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_price_validation)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
	        log.info("Carry-on bag price=" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price",
	                driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carrybagprice));
	        log.info("user successfully validated the  carryon bag price ");
	    }
	  @Then("^user Click on the minus button next to carryon bag with bundle$")
	    public void user_click_on_the_minus_button_next_to_carryon_bag_with_bundle() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_minus_button)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_minus_button)).click();
	        String carbagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText();
	        log.info("Carry on bag price is " + carbagprice);
	        Assert.assertTrue("User not validates the carryon_bag price",
	                driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation)).getText().equals(carbagprice));
	        log.info("user successfully validated the carryon bag price ");
	    }
	  @Then("^user enter the carryon bag and checked bag validates the bag total bar and itinerary with bundle$")
	    public void user_enter_the_carryon_bag_and_checked_bag_validates_the_bag_total_bar_and_itinerary_with_bundle()
	            throws Throwable {
	       
//		  driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	        log.info("user successfully add the carryon bag");
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        log.info("user successfully add the checked bags ");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
	        String totalbagsprice = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
	        log.info("Total Bags Price " + totalbagsprice);
	        log.info("user successfully clicked on the your itinerary caret");
	        Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
	                driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(totalbagsprice));
	 
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        String total_bags_price=driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
	        log.info("user successfully validated the  total bags price ");
	 
	    }
	  
	  @Then("^user continue without adding bags with I do not need bags$")
	    public void user_continue_without_adding_bags_with_I_do_not_need_bags() throws Throwable {
	 
	        FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
	                .ignoring(NoSuchElementException.class);
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.continue_withou_bags))); // window
	                                                                                                            // handle
	        WebElement ele = driver.findElement(By.xpath(BagsPage.continue_withou_bags));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("user clicks on the continue wihout adding the bags");
	        for (String iNeedBags : driver.getWindowHandles()) {
	            driver.switchTo().window(iNeedBags); // switch focus of WebDriver to
	                                                    // the next found window
	                                                    // handle (that's your newly
	                                                    // opened window)
	        }
	 
	        // code to do something on new window
	 
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_idont_need_bags)));
	        driver.findElement(By.xpath(BagsPage.popup_idont_need_bags)).click();
	        // close newly opened window when done with it
	        driver.switchTo().window(FocusonBagsPage); // switch back to the
	                                                    // original window
	      
	    }
	  
	  @Then("^user Click on the plus button next to checked bags of passenger2$")
	    public void user_click_on_the_plus_button_next_to_checked_bags_of_passenger2_for_return_trip() throws Throwable {
		  for (int i = 0; i < 5; i++) {
				 driver.findElement(By.xpath(BagsPage.passenger_checkedbag_plusbutton)).click();

				 String checked_bag_price =driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText();
				 log.info("The cost increased to " + checked_bag_price + " for adding the " + (i + 1) + "th bag");

				 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText().equals(checked_bag_price));
				 log.info("user successfully validate the checked bags prices ");

	        }
	 
	        for (int i = 5; i != 0; i--) {
	        	 driver.findElement(By.xpath(BagsPage.checked_minus_plusbutton)).click();

				 String checked_bag_price =driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText();
				 log.info("The cost decreased to " + checked_bag_price + " for removing the " + (i - 1) + "th bag");

				 Assert.assertTrue("user not validate the cheked bags prices", driver.findElement(By.xpath(BagsPage.passenger_two_checkedbag_price_validation)).getText().equals(checked_bag_price));
				 log.info("user successfully validate the checked bags prices ");
	        }
	 
	    }
	  @Then("^user enter the carryon bag and checked bag validates the bag total bar and itinerary of two passengers$")
	    public void user_enter_the_carryon_bag_and_checked_bag_validates_the_bag_total_bar_and_itinerary_of_two_passengers()
	            throws Throwable {
	        FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
	                .ignoring(NoSuchElementException.class);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.carry_on_bag_plus_button)));
	        // driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
	 
	        // log.info("user successfully add the carryon bag");
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        log.info("user successfully add the checked bags ");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.your_itinerary_caret)).click();
	        String totalbagsprice = driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText();
	        log.info("Total Bags Price " + totalbagsprice);
	        log.info("user successfully clicked on the your itinerary caret");
	        Assert.assertTrue("User after increasing the bags count,the toatal is not increased",
	                driver.findElement(By.xpath(BagsPage.your_itinerary_bags_total)).getText().equals(totalbagsprice));
	 
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        String total_bags_price=  driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(total_bags_price));
	        log.info("user successfully validated the total bags price ");
	 
	    }
	  @Then("^user validates the bags total with the shopping cart for two passengers$")
	    public void user_validates_the_bags_total_with_the_shopping_cart_for_two_passengers() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        log.info("user successfully clicked on Your itinerary caret");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Bags_total_button)));
	        String totalbagscost = driver.findElement(By.xpath(BagsPage.Bags_total_button)).getText();
	        log.info("Total Bags price " + totalbagscost);
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.Bags_total_button)).getText().equals(totalbagscost));
	        log.info("user successfully validated the bags prices in itinerary cart");
	 
	    }
	  
	  
	  @Then("^user validates the bags total with the shopping cart for two passengers with bundle$")
	    public void user_validates_the_bags_total_with_the_shopping_cart_for_two_passengers_with_bundle() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        log.info("user successfully clicked on Your itinerary caret");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Bags_total_button)));
	        String totalbagscost = driver.findElement(By.xpath(BagsPage.Bags_total_button)).getText();
	        log.info("Total Bags price " + totalbagscost);
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.Bags_total_button)).getText().equals(totalbagscost));
	        log.info("user successfully validated the bags prices in itinerary cart");
	 
	    }
	  
	  @Then("^user validates the caryon bag price of passenger2$")
	    public void user_validates_the_caryon_bag_price_of_passenger2() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button)).click();
	        wait.until(
	                ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)).getText();
	        log.info("Carry-on bag price=" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price",
	                driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)).getText().equals(carrybagprice));
	        log.info("user successfully validated the  carryon bag price ");
	    }
	  
	  @Then("^user Click on plus button next to checked bags$")
	    public void user_click_the_plus_button_next_to_checked_bags() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_plusbutton)));
	        WebElement plusbtn = driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton));
	        plusbtn.click();
	        WebElement plusbtndis = driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton_disable));
	        String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
	        log.info("1 Bag Price is " + Bagsprice);
	        if (plusbtndis.isDisplayed()) {
	            log.info("This One Way Trip does require Proof of Return travel at the airport");
	        } else {
	            log.info("This One Way Trip doesn't required Proof of return travel at the airport");
	        }
	        Thread.sleep(6000);
	    }


	  @Then("^user Click on plus button next to checked bags for passenger2$")
	    public void user_click_the_plus_button_next_to_checked_bags_for_passenger2() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)));
	        WebElement plusbtn = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton));
	        plusbtn.click();
	        WebElement plusbtndis = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton_disable));
	        String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("1 Bag Price is " + Bagsprice);
	        if (plusbtndis.isDisplayed()) {
	            log.info("This One Way Trip does require Proof of Return at the airport");
	        } else {
	            log.info("This One Way Trip doesn't required Proof of return travel at the airport");
	        }
	        Thread.sleep(6000);
	 
	    }


	  @Then("^user Click on the plus button next to checked bags for passenger2$")
	    public void user_click_on_the_plus_button_next_to_checked_bags_for_passenger2() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)));
	        driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)).click();
	        String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  1st Checked Bag Price is " + Bagsprice);
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice));
	        driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)).click();
	        String Bagsprice2 = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  2nd Checked Bag Price is " + Bagsprice2);
	        
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice2));
	        driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)).click();
	        String Bagsprice3 = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  3rd Checked Bag Price is " + Bagsprice3);
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice3));
	    }
	
	    @Then("^user validates the bags total with the shopping cart of military multipax$")
	    public void user_validates_the_bgas_total_with_the_shopping_cart_of_military_multipax() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        log.info("user successfully clicked on the your itinerary caret");
	        String totalbagscost = driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        log.info("Total Bags price " + totalbagscost);
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(totalbagscost));
	        log.info("user successfully validated the bags prices in itinerary cart");
	 
	    }
	 

	    @Then("^user validates the caryon bag price of military passenger2$")
	    public void user_validates_the_caryon_bag_price_of_military_passenger2() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button)).click();
	        wait.until(
	                ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)).getText();
	        log.info("Carry-on bag price=" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price",
	                driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation)).getText().equals(carrybagprice));
	        log.info("user successfully validated the  carryon bag price ");
	    }


	    @Then("^user Click on the plus button next to checked bags for military passenger2$")
	    public void user_click_on_the_plus_button_next_to_checked_bags_for_military_passenger2() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)));
	        driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_plusbutton)).click();
	        String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  1st Checked Bag Price is " + Bagsprice);
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice));
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        String Bagsprice2 = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  2nd Checked Bag Price is " + Bagsprice2);
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice2));
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        String Bagsprice3 = driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText();
	        log.info("Validated Passenger 2's  3rd Checked Bag Price is " + Bagsprice3);
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
	                driver.findElement(By.xpath(BagsPage.checked_bag_psgr2_price_validation)).getText().equals(Bagsprice3));
	    }


	    @Then("^user validates the bags total with the shopping cart of multi military members$")
	    public void user_validates_the_bgas_total_with_the_shopping_cart_of_multi_military_members() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_caret)));
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        log.info("user successfully clicked on the your itinerary caret");
	        String totalbagscost = driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        log.info("Total Bags price " + totalbagscost);
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(totalbagscost));
	        log.info("user successfully validated the bags prices in itinerary cart");
	 
	    }
	    @Then("^user validates the bags total with the shopping cart with thrills combo$")
	    public void user_validates_the_bags_total_with_the_shopping_cart_with_thrills_combo() throws Throwable {
	        
	        driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
	        log.info("user successfully clicked on the bags total button");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.total_bags_price)));
	        String Itintotalbagsprice = driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
	        log.info("Itinerary total bags price showing as " + Itintotalbagsprice);
	        Assert.assertTrue("User after increasing the bags count,the total is not increased",
	                driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(Itintotalbagsprice));
	        log.info("user successfully validated the  total bags prices ");
	 
	    }
	    @Then("^user clicks on the continue with 9DFC conitnue button and login$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_login() throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
	        driver.findElement(By.xpath(BagsPage.popup_login)).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
	        WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
	        choosepwd.click();
	        choosepwd.clear();
	        choosepwd.sendKeys("mikesmith@spirit.com");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
	        WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_password));
	        confirmpwd.click();
	        confirmpwd.clear();
	        confirmpwd.sendKeys("Spirit123@");
	        confirmpwd.sendKeys(Keys.TAB);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
	        WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
	        loginbtn.click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_err_invalid_emailorpwd)));
	        String errormsg = driver.findElement(By.xpath(BagsPage.pop_err_invalid_emailorpwd)).getText();
	        log.info("Error message should be " + errormsg);
	        Assert.assertTrue("User unable to get the 9DFC signup error", errormsg
	                .equals("Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
	        log.info("User successfully validated Invalid login enrollment");
	        driver.switchTo().window(FocusonBagsPage);
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
	    
	    @Then("^user clicks on the continue with 9DFC conitnue button and continue with std fare$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_continue_with_std_fare() throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_cont_std_farebtn)));
	        driver.findElement(By.xpath(BagsPage.popup_cont_std_farebtn)).click();
	        log.info("User CONTINUE WITH STANDARD FARES");
	        // close newly opened window when done with it
	        driver.switchTo().window(FocusonBagsPage);
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
	    
	    @Then("^user clicks on the continue with 9DFC conitnue button and signup then continue with std fare$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_signup_then_continue_with_std_fare()
	            throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
	        driver.findElement(By.xpath(BagsPage.popup_signup)).click();
	        log.info("User clicks signup button");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_cont_std_farebtn)));
	        driver.findElement(By.xpath(BagsPage.popup_cont_std_farebtn)).click();
	        log.info("User CONTINUE WITH STANDARD FARES");
	        // close newly opened window when done with it
	        driver.switchTo().window(FocusonBagsPage); // switch back to the
	                                                    // original window
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
	    
	    @Then("^user clicks on the continue with 9DFC conitnue button and login then continue with std fare$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_login_then_continue_with_std_fare()
	            throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
	        driver.findElement(By.xpath(BagsPage.popup_login)).click();
	        log.info("User clicks signup button");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_cont_std_farebtn)));
	        driver.findElement(By.xpath(BagsPage.popup_cont_std_farebtn)).click();
	        log.info("User CONTINUE WITH STANDARD FARES");
	        // close newly opened window when done with it
	        driver.switchTo().window(FocusonBagsPage); // switch back to the
	                                                    // original window
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
	    
	    @Then("^user Click on the plus button next to carryon bag for return trip$")
	    public void user_click_on_the_plus_button_next_to_carryon_bag_for_return_trip() throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.carry_on_bag_plus_button_return)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button_return)).click();
	 
	    }
	 
	    @Then("^user validates the caryon bag price for return trip$")
	    public void user_validates_the_caryon_bag_price_for_return_trip() throws Throwable {
	        wait.until(
	                ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_price_validation_return)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_price_validation_return)).getText();
	        log.info("Carry-on bag price for return trip=" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price for return trip", driver
	                .findElement(By.xpath(BagsPage.carry_on_bag_price_validation_return)).getText().equals(carrybagprice));
	        log.info("user successfully validated the  carryon bag price for return trip ");
	    }
	 
	    @Then("^user validates the caryon bag price of military passenger2 for return trip$")
	    public void user_validates_the_caryon_bag_price_of_military_passenger2_for_return_trip() throws Throwable {
	        wait.until(ExpectedConditions
	                .visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button_return)));
	        driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_plus_button_return)).click();
	        wait.until(ExpectedConditions
	                .visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation_return)));
	        String carrybagprice = driver.findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation_return))
	                .getText();
	        log.info("Carry-on bag price of return trip =" + carrybagprice);
	        Assert.assertTrue("User not validates the carryon_bag price for return trip", driver
	                .findElement(By.xpath(BagsPage.carry_on_bag_psgr2_price_validation_return)).getText().equals(carrybagprice));
	        log.info("user successfully validated the carryon bag price for return trip ");
	    }
	 
	    @Then("^user Click on the plus button next to checked bags for military member for return trip$")
	    public void user_click_on_the_plus_button_next_to_checked_bags_for_military_member_for_return_trip()
	            throws Throwable {
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_plusbutton_return)));
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton_return)).click();
	        String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
	        Assert.assertTrue("After increasing the bags count to 1 the cost is not increased for return trip",
	                driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice));
	        log.info("Validated Passenger 1's 1st Checked Bag Price for return trip is " + Bagsprice);
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        String Bagsprice2 = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
	        Assert.assertTrue("After increasing the bags count to 2 the cost is not increased for return trip",
	                driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice2));
	        log.info("Validated Passenger 1's  2nd Checked Bag Price for return trip is " + Bagsprice2);
	        driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
	        String Bagsprice3 = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
	        Assert.assertTrue("After increasing the bags count to 3 the cost is not increased for return trip",
	                driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice3));
	        log.info("Validated Passenger 1's  3rd Checked Bag Price for return trip is " + Bagsprice3);
	 
	    }
	 

		@Then("^user clicks on the continue with 9DFC conitnue button and login success$")
		public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_login_success() throws Throwable {
			String FocusonBagsPage = driver.getWindowHandle(); // get the current
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																											// handle
			WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			log.info("User clicks on the 9DFC conitnue button");
			for (String nineclub : driver.getWindowHandles()) {
				driver.switchTo().window(nineclub); // switch focus of WebDriver to
													// the next found window
													// handle (that's your newly
													// opened window)
			}

			// code to do something on new window

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
			driver.findElement(By.xpath(BagsPage.popup_login)).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
			WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
			choosepwd.click();
			choosepwd.clear();
			choosepwd.sendKeys("mike.smith1@spirit.com");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
			WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_password));
			confirmpwd.click();
			confirmpwd.clear();
			confirmpwd.sendKeys("Test@123");
			confirmpwd.sendKeys(Keys.TAB);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
			WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
			loginbtn.click();

			driver.switchTo().window(FocusonBagsPage); // switch back to the
														// original window
			String currenturl = driver.getCurrentUrl();
			log.info(currenturl);
			Assert.assertTrue("User not navigated back to Bags page",
					currenturl.equals("http://qaepic01.spirit.com/book/bags"));
			log.info("User successfully navigated back to Bags page");
		}

		@Then("^user clicks on the continue with 9DFC conitnue button and validate verbiages$")
		public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_validate_verbiages() throws Throwable {
			String FocusonBagsPage = driver.getWindowHandle(); // get the current
			WebElement ninedfcmodal = driver.findElement(By.xpath(BagsPage.nineDFC_modal_content));
			String ndfccontent = ninedfcmodal.getText();
			log.info(ndfccontent);
			Assert.assertTrue("9DFC Modal verbiage is mismatch", ninedfcmodal.equals(ndfccontent));
			log.info("9DFC Modal content is verified");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																											// handle
			WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			log.info("User clicks on the 9DFC conitnue button");
			for (String nineclub : driver.getWindowHandles()) {
				driver.switchTo().window(nineclub); // switch focus of WebDriver to
													// the next found window
													// handle (that's your newly
													// opened window)
			}

			// code to do something on new window
			WebElement ninedfcmodalcontent = driver.findElement(By.xpath(BagsPage.popup_modal_content));
			String modalcontent = ninedfcmodal.getText();
			log.info(modalcontent);
			Assert.assertTrue("9DFC Pricing popup modal content is mismatch", ninedfcmodalcontent.equals(modalcontent));
			log.info("9DFC Pricing popup modal content is verified");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
			driver.findElement(By.xpath(BagsPage.popup_login)).click();
			Assert.assertTrue("9DFC Pricing popup modal content is mismatch after selects login",
					ninedfcmodalcontent.equals(modalcontent));
			log.info("9DFC Pricing popup modal content is verified after selects login");
			WebElement outsidepoopup = driver.findElement(By.xpath(BagsPage.popup_modal_fade_show));
			Actions builder = new Actions(driver);
			builder.moveToElement(outsidepoopup, 10, 25).click().build().perform();
			try {
				driver.findElement(By.xpath(BagsPage.popup_ninedfc_modal)).isDisplayed();
				log.info("9DFC Modal popup not closed when you click outside of the modal");
			} catch (NoSuchElementException e) {
				log.info("9DFC Modal shouldn't suppose to be closed when you click outside of the modal");
			}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_closebtn)));
			driver.findElement(By.xpath(BagsPage.popup_closebtn)).click();
			log.info("9DFC Pricing popup modal is successfully closed");
		}

		@Then("^user clicks on the continue with 9DFC conitnue button and FS login success$")
		public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_FS_login_success() throws Throwable {
			String FocusonBagsPage = driver.getWindowHandle(); // get the current
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																											// handle
			WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			log.info("User clicks on the 9DFC conitnue button");
			for (String nineclub : driver.getWindowHandles()) {
				driver.switchTo().window(nineclub); // switch focus of WebDriver to
													// the next found window
													// handle (that's your newly
													// opened window)
			}

			// code to do something on new window

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
			driver.findElement(By.xpath(BagsPage.popup_login)).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
			WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
			choosepwd.click();
			choosepwd.clear();
			choosepwd.sendKeys("john.doe1@spirit.com");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
			WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_password));
			confirmpwd.click();
			confirmpwd.clear();
			confirmpwd.sendKeys("Test@123");
			confirmpwd.sendKeys(Keys.TAB);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
			WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
			loginbtn.click();

			driver.switchTo().window(FocusonBagsPage); // switch back to the
														// original window
			String currenturl = driver.getCurrentUrl();
			log.info(currenturl);
			Assert.assertTrue("User not navigated back to Bags page",
					currenturl.equals("http://qaepic01.spirit.com/book/bags"));
			log.info("User successfully navigated back to Bags page");
		}

		@Then("^user clicks on the continue with 9DFC conitnue button and login failure$")
		public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_login_failure() throws Throwable {
			String FocusonBagsPage = driver.getWindowHandle(); // get the current
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																											// handle
			WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			log.info("User clicks on the 9DFC conitnue button");
			for (String nineclub : driver.getWindowHandles()) {
				driver.switchTo().window(nineclub); // switch focus of WebDriver to
													// the next found window
													// handle (that's your newly
													// opened window)
			}

			// code to do something on new window

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
			driver.findElement(By.xpath(BagsPage.popup_login)).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
			WebElement email = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
			email.click();
			email.clear();
			email.sendKeys(Keys.TAB);
			// choosepwd.sendKeys("mikesmith@spirit.com");

			// confirmpwd.click();
			// confirmpwd.clear();
			// confirmpwd.sendKeys("Spirit123@");
			// confirmpwd.sendKeys(Keys.TAB);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_user_err)));
			String errormsg = driver.findElement(By.xpath(BagsPage.pop_user_err)).getText();
			log.info("Error message should be " + errormsg);
			Assert.assertTrue("Login username error message is Invalid for without entering Email or FREE SPIRIT Number",
					errormsg.equals("Email Address or FREE SPIRIT® Number is required"));
			log.info("Login username error message is validated for without entering Email or FREE SPIRIT Number");

			email.clear();
			email.sendKeys("mike.smith@spirit.com");
			email.sendKeys(Keys.TAB);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
			WebElement pwd = driver.findElement(By.xpath(BagsPage.popup_password));
			pwd.clear();
			pwd.sendKeys("SPIRIT123");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
			WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
			loginbtn.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_alert_err)));
			String errormsg1 = driver.findElement(By.xpath(BagsPage.pop_alert_err)).getText();
			log.info(errormsg1);
			Assert.assertTrue("Login username error message is Invalid when you entering with Invalid Email", errormsg1
					.equals("Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
			log.info("Login username error message is validated for with entering Invalid Email");

			pwd.clear();
			pwd.sendKeys(Keys.TAB);
			loginbtn.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_pwd_err)));
			String errormsg2 = driver.findElement(By.xpath(BagsPage.pop_pwd_err)).getText();
			log.info(errormsg2);
			Assert.assertTrue("Login error message is Invalid when you entering with Invalid Email",
					errormsg2.equals("Password is required"));
			log.info("Login password error message is validated for without entering password");

			email.clear();
			pwd.clear();
			pwd.sendKeys("SPIRIT123");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_pwd_err)));
			String errormsg3 = driver.findElement(By.xpath(BagsPage.pop_pwd_err)).getText();
			log.info(errormsg3);
			Assert.assertTrue("Login error message is Invalid when you entering with Invalid Email",
					errormsg3.equals("Password is required"));
			log.info("Login password error message is validated for without entering password");
		}

		@Then("^user clicks on the continue with 9DFC conitnue button and reset password$")
		public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_rest_password() throws Throwable {
			String FocusonBagsPage = driver.getWindowHandle(); // get the current
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																											// handle
			WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			log.info("User clicks on the 9DFC conitnue button");
			for (String nineclub : driver.getWindowHandles()) {
				driver.switchTo().window(nineclub);
			}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
			WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
			loginbtn.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_reset_pwd)));
			driver.findElement(By.xpath(BagsPage.pop_reset_pwd)).click();
			log.info("User clicks on Reset Password button");
			for (String iNeedBags : driver.getWindowHandles()) {
				driver.switchTo().window(iNeedBags);
			}
			log.info("User switches to Bags popup");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.Popup_dont_purchase_bags)));
			driver.findElement(By.xpath(BagsPage.Popup_dont_purchase_bags)).click();
			log.info("User selects Don't purchase Bags option button");
			driver.switchTo().window(FocusonBagsPage);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.Retrieve_pwd)));
			driver.findElement(By.xpath(BagsPage.Retrieve_pwd)).sendKeys("mike.smith1@spirit.com");
			driver.findElement(By.xpath(BagsPage.send_btn)).click();
			Assert.assertTrue("Success Password reset message is not displayed",
					driver.findElement(By.xpath(BagsPage.Pwd_reset_msg)).isDisplayed());
			log.info(driver.findElement(By.xpath(BagsPage.Pwd_reset_msg)).getText());
		}

		@Then("^user clicks on the continue with 9DFC conitnue button and signup failure$")
	    public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_signup_failure() throws Throwable {
	        String FocusonBagsPage = driver.getWindowHandle(); // get the current
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
	                                                                                                        // handle
	        WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
	        JavascriptExecutor executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", ele);
	        log.info("User clicks on the 9DFC conitnue button");
	        for (String nineclub : driver.getWindowHandles()) {
	            driver.switchTo().window(nineclub); // switch focus of WebDriver to
	                                                // the next found window
	                                                // handle (that's your newly
	                                                // opened window)
	        }
	 
	        // code to do something on new window
	 
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
	        driver.findElement(By.xpath(BagsPage.popup_signup)).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_choose_pwd)));
	        WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_choose_pwd));
	        choosepwd.click();
	        choosepwd.clear();
	        choosepwd.sendKeys("ABCDEFGHIJ");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	        log.info(errormsg);
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS ONLY", errormsg.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	        choosepwd.clear();
	        choosepwd.sendKeys("123456789");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg2 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS ONLY", errormsg2.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("~!@#$%^&*");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg3 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 SPECIAL characters ONLY", errormsg3.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("Spirit1");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg4 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 7 or LESS characters", errormsg4.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("Spirit11Spirit1123");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg5 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 17 or MORE characters", errormsg5.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("SPIRIT123456");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg6 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS & LETTERS ONLY", errormsg6.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("123456!@#$%^");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg7 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS & SPECIAL characters ONLY", errormsg7
	                .equals("Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("1234&@#!$spirit");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg8 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue(
	                "Invalid error not matched when you enter 8-16 numbers, special characters and ALL LOWERCASE letters",
	                errormsg8.equals(
	                        "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("SPIRIT!@#$%^");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg9 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS & SPECIAL characters ONLY", errormsg9
	                .equals("Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("spirit123456");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg10 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS & NUMBERS ONLY", errormsg10.equals(
	                "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("S@P*I#R&I$T%");
	        choosepwd.sendKeys(Keys.TAB);
	        String errormsg11 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	 
	        Assert.assertTrue(
	                "Invalid error not matched when you enter 8-16 numbers, special characters and ALL CAPITALIZED letters",
	                errormsg11.equals(
	                        "Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
	 
	        choosepwd.clear();
	        choosepwd.sendKeys("Test@123");
	        choosepwd.sendKeys(Keys.TAB);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_confirm_pwd)));
	        WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_confirm_pwd));
	        confirmpwd.click();
	        confirmpwd.clear();
	        confirmpwd.sendKeys("Spirit11!");
	        confirmpwd.sendKeys(Keys.TAB);
	        String errormsg13 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
	        log.info(errormsg13);
	        Assert.assertTrue("User unable to get the 9DFC signup error",
	                errormsg.equals("Password and Confirm Password must match"));
	 
	        driver.switchTo().window(FocusonBagsPage);
	        String currenturl = driver.getCurrentUrl();
	        log.info(currenturl);
	        Assert.assertTrue("User not navigated back to Bags page",
	                currenturl.equals("http://qaepic01.spirit.com/book/bags"));
	        log.info("User successfully navigated back to Bags page");
	    }
 
	

	 



	 

	 

}
