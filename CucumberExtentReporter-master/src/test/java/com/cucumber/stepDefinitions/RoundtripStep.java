package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.RoundtripPage;

import cucumber.api.java.en.Then;

public class RoundtripStep {
	
	
	WebDriver driver = MySharedClass.getDriver();
	private  final FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	private static final Logger log = LogManager.getLogger();
	
	
	

    @Then("^User choose the from city FLL$")
    public void user_choose_the_from_city_fll() throws Throwable {
      
    	Thread.sleep(2000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
    	driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RoundtripPage.select_from_city)));

 	driver.findElement(By.xpath(RoundtripPage.select_from_city)).click();
    	
    }

	
	@Then("^user clicks on the Return selected flight$")
    public void user_clicks_on_the_return_selected_flight() throws Throwable {
		
		int a = 0;
		int b = 0;
		boolean flag = false;
		boolean Return_available_status = true;
		try {
			driver.findElement(By.xpath(RoundtripPage.Return_select_day_not_available)).isEnabled();
		} catch (NoSuchElementException e) {
			Return_available_status = false;
		}
		if (!Return_available_status) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(RoundtripPage.Return_flight_select_day_cost)));
		}
		if (!Return_available_status) {
			driver.findElement(By.xpath(RoundtripPage.Return_select_day)).click();
			flag = true;
		} else {

			for (a = 5; a <= 7; a++) {
				Return_available_status = true;
				String Return_otherday_status = null;
				String Return_otherday = null;
				try {
					Return_otherday_status = RoundtripPage.Return_not_available(a, Return_otherday_status);
					driver.findElement(By.xpath(Return_otherday_status)).isEnabled();
				} catch (NoSuchElementException e) {
					Return_available_status = false;
				}
				Return_otherday = RoundtripPage.Return_next_day_Return(a, Return_otherday);
				if (!Return_available_status) {
					driver.findElement(By.xpath(Return_otherday)).click();
					flag = true;
					break;
				}

			}
		}

		if (flag == false) {
			driver.findElement(By.xpath(RoundtripPage.Return_next_week_click)).click();
			Thread.sleep(8000);
			for (b = 1; b <= 7; b++) {
				Return_available_status = true;
				String Return_next_week_day_status = null;
				String Return_next_week_day = null;
				try {
					Return_next_week_day_status = RoundtripPage.Return_not_available(b, Return_next_week_day_status);
					driver.findElement(By.xpath(Return_next_week_day_status)).isEnabled();
				} catch (NoSuchElementException e) {
					Return_available_status = false;
				}

				Return_next_week_day = RoundtripPage.Return_next_day_Return(b, Return_next_week_day);
				if (!Return_available_status) {
					driver.findElement(By.xpath(Return_next_week_day)).click();
					flag = true;
					break;
				}
			}

		}
		log.info(flag);
		Assert.assertTrue("The correct prices are not displayed in the flight page", flag);

       
    }
	 @Then("^user selects the return flight \"([^\"]*)\"$")
	    public void user_selects_the_return_flight_something(String return_flght_stops) throws Throwable {      
		 
		 int Return_flight_count = driver.findElements(By.xpath(RoundtripPage.Return_flight_count)).size();
			boolean Return_flight_selected = false;
			log.info("There are " + Return_flight_count + " available for this selected date");
			for (int a = 1; a <= Return_flight_count; a++) {

				try {
					Thread.sleep(5000);
	// ///section[@class='ng-star-inserted']/div[3]//div[@class='d-flex flex-column']/div[1]/app-journey[1]/div/div[2]/div[2]//button[@type='button']
					if (driver
							.findElement(By.xpath("//section[@class='ng-star-inserted']//div[2]//div[@class='d-flex flex-column']/div[" +a+ "]/app-journey/div[1]//app-input[1]/div/label"))
							.isDisplayed()
							&& driver
									.findElement(By.xpath("//section[@class='ng-star-inserted']//div[2]//div[@class='d-flex flex-column']/div[" + a+ "]/app-journey[1]/div/div[2]/div[2]//button[@type='button']"))
									.getText().equals(return_flght_stops)) {

						log.info("The available flights has the " + return_flght_stops + ", selecting the mentioned flight");
						driver.findElement(By.xpath("//section[@class='ng-star-inserted']//div[2]//div[@class='d-flex flex-column']/div[" +a+"]/app-journey/div[1]//app-input[1]/div/label")).click();
						// wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//div[@class='d-flex
						// flex-column justify-content-between
						// mb-1']/app-journey["+i+"]//input[@name='fareValue']"))));
						// Thread.sleep(5000);
						// driver.findElement(By.xpath("//div[@class='d-flex flex-column
						// justify-content-between
						// mb-1']/app-journey["+i+"]//input[@name='fareValue']")).click();
						//// input[@name='fareValue']
						Return_flight_selected = true;
					}
				} catch (NoSuchElementException e) {
					log.info("There is a sold out flight in the displayed flights");
				}
			}

			for (int b = 1; b <= Return_flight_count; b++) {
				try {

					Thread.sleep(5000);
					if (driver
							.findElement(By.xpath("//section[@class='ng-star-inserted']//div[2]//div[@class='d-flex flex-column']/div[" +b+ "]/app-journey/div[1]//app-input[1]/div/label"))
							.isDisplayed() && !Return_flight_selected) {
						driver.findElement(By.xpath("//section[@class='ng-star-inserted']//div[2]//div[@class='d-flex flex-column']/div[" +b+ "]/app-journey/div[1]//app-input[1]/div/label")).click();
						// wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//div[@class='d-flex
						// flex-column justify-content-between
						// mb-1']/app-journey["+j+"]//input[@name='fareValue']"))));
						// Thread.sleep(5000);
						// driver.findElement(By.xpath("//div[@class='d-flex flex-column
						// justify-content-between
						// mb-1']/app-journey["+j+"]//input[@name='fareValue']")).click();
						// ////input[@name='fareValue']
						Return_flight_selected = true;
					}
				} catch (NoSuchElementException e) {
					log.info("There is a sold out flight in the displayed flights");
				}
			}
			Assert.assertTrue(
					"None of the flights are selected, please modify the cities according to the flight availability",
					Return_flight_selected);

		 
		 
		 
		 
	    }

	
	
}
