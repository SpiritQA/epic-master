package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage320;
import com.cucumber.pages.VacationPage;

import cucumber.api.java.en.Then;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.VacationPage;


public class VacationStep {

	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	
	@Then("^User selects the vacation from city$")
    public void user_selects_the_vacation_from_city() throws Throwable {
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.From_city)));
		driver.findElement(By.xpath(VacationPage.From_city)).click();
		log.info("user successfully clicks on  the vacation trip departure city ");
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.vacation_select_from_city)));
		driver.findElement(By.xpath(VacationPage.vacation_select_from_city)).click();
		log.info("user successfully selects the vacation trip from city ");
    }

    @Then("^User selects the vacation To city$")
    public void user_selects_the_vacation_to_city() throws Throwable {
       
    	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.To_city)));
 		driver.findElement(By.xpath(VacationPage.To_city)).click();
 		log.info("user successfully clicks on  the vacation trip arrival city ");
 		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.vacation_select_to_city)));
 		driver.findElement(By.xpath(VacationPage.vacation_select_to_city)).click();
 		log.info("user successfully selects the vacation trip arrival city ");
    	
    	
    }
    
    
    @Then("^user selects vacation departure date and return date$")
    public void user_selects_vacation_departure_date_and_return_date() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).click();
		driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).clear();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		Date date1 = c.getTime();
		log.info("Departure date: " + dateFormat.format(date1));
		String DepartureDate = dateFormat.format(date1);
		c.setTime(date1);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String ReturnDate = dateFormat.format(currentDatePlusOne);

		log.info("Return date: " + dateFormat.format(currentDatePlusOne));
		driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates))
				.sendKeys(DepartureDate + " - " + ReturnDate);
    }
    
    @Then("^User selects one adult passenger for vacation$")
    public void user_selects_one_adult_passenger_for_vacation() throws Throwable {
       
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.passengers)));
    	driver.findElement(By.xpath(VacationPage.passengers)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Numbe_of_adults)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(VacationPage.Numbe_of_adults)).clear();
		driver.findElement(By.xpath(VacationPage.Numbe_of_adults)).sendKeys("1");
		log.info("user selects the one adult passeneger");
    }
    
    
    @Then("^User selects two adult passenger for vacation$")
    public void user_selects_two_adult_passenger_for_vacation() throws Throwable {
       
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.passengers)));
    	driver.findElement(By.xpath(VacationPage.passengers)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Numbe_of_adults)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(VacationPage.Numbe_of_adults)).clear();
		driver.findElement(By.xpath(VacationPage.Numbe_of_adults)).sendKeys("2");
		log.info("user selects the one adult passeneger");
    	
    }
    
    @Then("^User selects one adult passenger with lap child for vacation$")
    public void user_selects_one_adult_passenger_with_lap_child_for_vacation() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.passengers)));
    	driver.findElement(By.xpath(VacationPage.passengers)).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Numbe_of_child)));
    	driver.findElement(By.xpath(VacationPage.Numbe_of_child)).clear();
    	driver.findElement(By.xpath(VacationPage.Numbe_of_child)).sendKeys("1");
    	log.info("user successfuly selects the one lap child");
    	
    

    	
    }
    
    
    @Then("^User clicks on Search Vacations button and switch to the young travelers popup and enters the child DOB$")
    public void user_clicks_on_search_vacations_button_and_switch_to_the_young_travelers_popup_and_enters_the_child_dob() throws Throwable {
    	String parentHandle = driver.getWindowHandle();
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Search_Vacations_button)));
    	driver.findElement(By.xpath(CheckInPage.Search_Vacations_button)).click();
    	
    	for (String winHandle : driver.getWindowHandles()) {
    	    driver.switchTo().window(winHandle); 
    	}
    	Thread.sleep(2000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
		driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2017");
		driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(BookPage.Child1LapSeat)).click();
		driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
		log.info("user succesfully enter the birthdate and made the first child a lapchild");
    	
    }
    
    @Then("^User selects Flight and Hotel$")
    public void user_selects_Flight_and_hotel() throws Throwable {
	 driver.findElement(By.xpath(VacationPage.Flight_Hotel)).click();
	 log.info("user successfully Clicked on Flight and hotel ");
    }

    @Then("^User Verify One room is selected by default$")
    public void user_verify_1_room_is_selected_by_default() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.Rooms)).getText().equals("1 Room");
    	log.info("user verified 1 room is selected ");
    	
    }

    @Then("^User selects from city for vacation FLL$")
    public void user_selects_from_city_for_vacation_FLL() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_from_airport_or_city)));
		WebElement from = driver.findElement(By.xpath(BookPage.Vacation_from_airport_or_city));
		from.click();
		from.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.From_FLL_City)));
		driver.findElement(By.xpath(SeatsPage.From_FLL_City)).click();
    }

    @Then("^User selects to city for vacation LGA$")
    public void user_selects_to_city_for_vacation_LGA() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_to_airport_or_city)));
		WebElement to = driver.findElement(By.xpath(BookPage.Vacation_to_airport_or_city));
		to.click();
		to.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LGA)));
		driver.findElement(By.xpath(SeatsPage320.TO_city_LGA)).click();
    }
    
    @Then("^User selects to city for vacation LAS$")
    public void user_selects_to_city_for_vacation_LAS() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_to_airport_or_city)));
		WebElement to = driver.findElement(By.xpath(BookPage.Vacation_to_airport_or_city));
		to.click();
		to.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LAS)));
		driver.findElement(By.xpath(SeatsPage320.TO_city_LAS)).click();
    }
    
    @Then("^User selects to city for vacation CUN$")
    public void user_selects_to_city_for_vacation_CUN() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_to_airport_or_city)));
		WebElement to = driver.findElement(By.xpath(BookPage.Vacation_to_airport_or_city));
		to.click();
		to.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_CUN)));
		driver.findElement(By.xpath(SeatsPage320.TO_city_CUN)).click();
    }

    @Then("^User Selects One adult$")
    public void user_selects_1_adult() throws Throwable {
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_clickon_passengers)));
    		driver.findElement(By.xpath(BookPage.Vacation_clickon_passengers)).click();
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
    		driver.findElement(By.xpath(BookPage.Adults_input_box)).clear();
    		driver.findElement(By.xpath(BookPage.Adults_input_box)).sendKeys("1");
    		
    		log.info("user inputs one Adult");
    	}
    
    @Then("^user lands on Select your Hotel page$")
    public void user_lands_on_select_your_hotel_page() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Select_Your_hotel_header)));
		Assert.assertTrue("User is not  navigated to Select your Hotel page",
				driver.findElement(By.xpath(VacationPage.Select_Your_hotel_header)).getText().equals("Select Your Hotel"));
		log.info("User is successfully navigated to Select your Hotel page");
	}
    
    @Then("^User Selects Three adults$")
    public void user_selects_Three_adults() throws Throwable {
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_clickon_passengers)));
    		driver.findElement(By.xpath(BookPage.Vacation_clickon_passengers)).click();
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
    		driver.findElement(By.xpath(BookPage.Adults_input_box)).clear();
    		driver.findElement(By.xpath(BookPage.Adults_input_box)).sendKeys("3");
    		
    		log.info("user inputs three Adults");
    	}

    @Then("^User Selects Two rooms$")
    public void user_selects_two_rooms() throws Throwable {
    	Select dropdown = new Select(driver.findElement(By.xpath(VacationPage.Rooms)));
		dropdown.selectByVisibleText("2 Rooms");
		
		log.info("user Selects 2 rooms");
    }
    
    @Then("^User Enters LapChild Dob$")
    public void user_enters_lapchild_dob() throws Throwable {
    	String parentHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

			driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the lap child and selects lapchild");
		}

    }
    
    @Then("^User Enters Child Dob require seat$")
    public void user_enters_child_dob_require_seat() throws Throwable {
    	String parentHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

			driver.findElement(By.xpath(PassengerinfoPage.Vacations_Child_seat_option)).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the child and selects require seat");
		}

    }
    
    @Then("^user selects the two child and two adult passengers$")
    public void user_selects_the_two_child_and_two_adult_passengers() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BookPage.Adults_input_box)).clear();
		driver.findElement(By.xpath(BookPage.Adults_input_box)).sendKeys("2");
		driver.findElement(By.xpath(BookPage.Children_input_box)).clear();
		driver.findElement(By.xpath(BookPage.Children_input_box)).sendKeys("2");
		log.info("user succesfully inputs two adults and two children");
	}

    @Then("^User Enters Child Dob for two children on popup$")
    public void user_enters_child_dob_for_two_children() throws Throwable {
    	String parentHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("07/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
			driver.findElement(By.xpath(BookPage.Child1LapSeat)).click();
			log.info("user succesfully enter the birthdate and made the first child a lapchild");

			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys("08/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.TAB);
			driver.findElement(By.xpath(BookPage.Child2WillRequireSeat)).click();
			log.info("user succesfully enter the birthdate and the second child will need a seat");
			
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
		}

    }
    @Then("^verifies plus and minus buttons for adult and children$")
    public void verifies_plus_and_minus_buttons_for_adult_and_children() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		Thread.sleep(2000);
    	for (int i = 0; i < 7; i++) {
    		driver.findElement(By.xpath(BookPage.select_adults)).click();
    		String AdultCount = driver.findElement(By.xpath(BookPage.Adults_input_box)).getAttribute("value");
    		log.info("User press the adult plus button " + (i+1) + " time and the Adult count increased to " + AdultCount);
    	}
    	for (int i = 8; i != 0; i--) {
    		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
    		String AdultCount = driver.findElement(By.xpath(BookPage.Adults_input_box)).getAttribute("value");
    		log.info("User press the adult minus button and the Adult count descreased to " + AdultCount);
    	}
    	for (int i = 0; i < 8; i++) {
    		driver.findElement(By.xpath(BookPage.select_child)).click();
    		String ChildCount = driver.findElement(By.xpath(BookPage.Children_input_box)).getAttribute("value");
    		log.info("User press the child plus button " + (i+1) + " time and the child count increased to " + ChildCount);
    	}
    	for (int i = 8; i != 0; i--) {
    		driver.findElement(By.xpath(BookPage.select_child_minus_button)).click();
    		String ChildCount = driver.findElement(By.xpath(BookPage.Children_input_box)).getAttribute("value");
    		log.info("User press the Child minus button and the Child count descreased to " + ChildCount);
    	}
    }

    @Then("^User Verifies passengers required popup and close the popup$")
    public void user_verifies_passengers_required_popup_and_close_the_popup() throws Throwable {
    	Thread.sleep(2000);
    	String parentHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			
			Assert.assertTrue("User is not presented with Passengers required popup",
					driver.findElement(By.xpath(VacationPage.Passengers_Required_header)).getText().equals("Passengers Required"));
			log.info("User is successfully presented with Passengers Required popup");
		}
		
		driver.findElement(By.xpath(VacationPage.Passengers_Required_popup_close_button)).click();
		log.info("User clicked on Passengers required popup close button!");
    }
    
    @Then("^User clicks on Add a Promo Code$")
    public void user_clicks_on_add_a_promo_code() throws Throwable {
		driver.findElement(By.xpath(VacationPage.pomocode)).click();
		log.info("User Clicks on Add a Promo code link");
    }
    
    @Then("^user add a valid promo code for dollars$")
    public void user_add_a_valid_promo_code() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.pomocode_textbox)).sendKeys("10OFF");
		log.info("User adds a valid promo code");
    }
    
    @Then("^user add a valid promo code for percent$")
    public void user_add_a_valid_promo_code_for_percent() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.pomocode_textbox)).sendKeys("50PCT");
		log.info("User adds a valid promo code");
    }
    
    @Then("^user selects vacation dates within fortyEight hour window$")
    public void user_selects_vacation_dates_within_fortyeight_hour_window() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.dates)));
		driver.findElement(By.xpath(BookPage.dates)).clear();
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 0);
		Date date = c.getTime();
		log.info("Departure date: " + dateFormat.format(date));
		String DepartureDate = dateFormat.format(date);
		c.setTime(date);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String ReturnDate = dateFormat.format(currentDatePlusOne);

		log.info("Return date: " + dateFormat.format(currentDatePlusOne));
		driver.findElement(By.xpath(BookPage.dates)).sendKeys(DepartureDate + " - " + ReturnDate);
	}
    
    @Then("^User verifies No Hotel Available popup$")
    public void user_verifies_no_hotel_available_popup() throws Throwable {
    	Thread.sleep(15000);
    	String parentHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			
			Assert.assertTrue("User is not presented with No Hotels Available popup",
					driver.findElement(By.xpath(VacationPage.No_Hotel_content)).getText()
					.equals("Unfortunately, There are no hotels available for the dates selected."));
			log.info("User is successfully presented with No Hotels Available popup and the verbiage is verified");
		}
		
		driver.findElement(By.xpath(VacationPage.No_Hotel_x_button)).click();
		log.info("User clicked on No Hotels Available popup exit button!");
    }
    
    @Then("^user add invalid Promo Code$")
    public void user_add_invalid_promo_code() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.pomocode_textbox)).sendKeys("100OFF");
		log.info("User adds a invalid promo code");
    }

    @Then("^user verifies Promation code popup$")
    public void user_verifies_promation_code_popup() throws Throwable {
    	String parentHandle = driver.getWindowHandle();
    	Thread.sleep(3000);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Assert.assertTrue("User is not presented with Promotion Code popup",
				driver.findElement(By.xpath(VacationPage.Promocode_Popup_Title)).getText().equals("Promotion Code"));
		log.info("User is presented with Promotion Code popup");
			
    }
    
    @Then("^user clicks Edit Coupon$")
    public void user_clicks_edit_coupon() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.Edit_coupon_code_buttton)).click();
    	log.info("User Clicks on Edit Coupon Code Button");
    }

    @Then("^user reenter invalid promo code$")
    public void user_reenter_invalid_promo_code() throws Throwable {
    	driver.findElement(By.xpath(VacationPage.pomocode_textbox)).clear();
    	driver.findElement(By.xpath(VacationPage.pomocode_textbox)).sendKeys("200OFF");
		log.info("User reenters a invalid promo code");
    }
    
    
    
}
