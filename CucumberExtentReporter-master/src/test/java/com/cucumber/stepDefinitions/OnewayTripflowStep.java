package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.FlightAvailability;
import com.cucumber.pages.MultiCityPage;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.PaymentPage;
import com.cucumber.pages.RoundtripPage;
import com.cucumber.pages.SeatsPage319;
import com.cucumber.runner.TestRunner;
import com.mysql.jdbc.Driver;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import managers.FileReaderManager;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class OnewayTripflowStep {
	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	private static String Discover_card_number = FileReaderManager.getInstance().getTestDataReader()
			.discovercardnumber();
	private static String Visa_card_number = FileReaderManager.getInstance().getTestDataReader().visacardnumber();
	private static String Master_card_number = FileReaderManager.getInstance().getTestDataReader().mastercardnumber();
	private static String American_Express_card_number = FileReaderManager.getInstance().getTestDataReader()
			.americanexpresscardnumber();

	private static String free_spirit_email = FileReaderManager.getInstance().getTestDataReader().freeSpiritEmail();
	private static String free_spirit_password = FileReaderManager.getInstance().getTestDataReader()
			.freeSpiritPassword();
	private static String Promo_code = FileReaderManager.getInstance().getTestDataReader().Promocode();
	private static String Invalid_promo_code = FileReaderManager.getInstance().getTestDataReader().InvalidPromocode();
	private static String Promo_code_Percent_off = FileReaderManager.getInstance().getTestDataReader()
			.Promocode_percent_off();

	@Then("^User clicks on onewaytrip$")
	public void user_clicks_on_onewaytrip() throws Throwable {
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
		}
		// Write code here that turns the phrase above into concrete actions
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.oneway_Trip)));
		driver.findElement(By.xpath(OnewaytripPage.oneway_Trip)).click();
	}

	@When("^user lands on flight page$")
	public void user_lands_on_flight_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.New_search_button)));
		Assert.assertTrue("User is not  navigated to Flight page",
				driver.findElement(By.xpath(OnewaytripPage.New_search_button)).getText().equals("NEW SEARCH"));
		log.info("User is successfully navigated to Flight page after clicking on search in Book page");
	}

	@Then("^user clicks on the selected flight$")
	public void user_clicks_on_the_selected_flight() throws Throwable {
		
		/*boolean Flight_selected_status =  false;
		 boolean flight_notavailable_or_soldout  = false;
		try {
			flight_notavailable_or_soldout = (driver.findElement(By.xpath(OnewaytripPage.select_day_not_available)).isEnabled() || driver.findElement(By.xpath(OnewaytripPage.select_day_soldout)).isEnabled());
		} catch (NoSuchElementException e) {
			
		}
		if(flight_notavailable_or_soldout) {log.info("The flight is either soldout or not available");}
		Flight_selected_status = OnewaytripPage.NotAvailableSeatSelection();
		log.info(Flight_selected_status);
		Assert.assertTrue("The correct prices are not displayed in the flight page", Flight_selected_status);*/
		int i = 0;
		int j = 0;
		boolean flag = false;
		boolean available_status = true;
		try {
			driver.findElement(By.xpath(OnewaytripPage.select_day_not_available)).isEnabled();
		} catch (NoSuchElementException e) {
			available_status = false;
		}
		if (!available_status) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.selected_day_cost)));
		}
		if (!available_status) {
			driver.findElement(By.xpath(OnewaytripPage.selected_day)).click();
			flag = true;
		} else {

			for (i = 5; i <= 7; i++) {
				available_status = true;
				String otherday_status = null;
				String otherday = null;
				try {
					otherday_status = OnewaytripPage.not_available(i, otherday_status);
					driver.findElement(By.xpath(otherday_status)).isEnabled();
				} catch (NoSuchElementException e) {
					available_status = false;
				}
				otherday = OnewaytripPage.next_day_oneway(i, otherday);
				if (!available_status) {
					driver.findElement(By.xpath(otherday)).click();
					flag = true;
					break;
				}

			}
		}

		if (flag == false) {
			driver.findElement(By.xpath(OnewaytripPage.next_week_click)).click();
			Thread.sleep(2000);
			for (j = 1; j <= 7; j++) {
				available_status = true;
				String next_week_day_status = null;
				String next_week_day = null;
				try {
					next_week_day_status = OnewaytripPage.not_available(i, next_week_day_status);
					driver.findElement(By.xpath(next_week_day_status)).isEnabled();
				} catch (NoSuchElementException e) {
					available_status = false;
				}

				next_week_day = OnewaytripPage.next_week_day_oneway(j, next_week_day);
				if (!available_status) {
					driver.findElement(By.xpath(next_week_day)).click();
					flag = true;
					break;
				}
			}

		}
		log.info(flag);
		Assert.assertTrue("The correct prices are not displayed in the flight page", flag);

	}

	/*@Then("^user selects the which time to fly$")
	public void user_selects_the_what_time_to_flying() throws Throwable {
		
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
		 * OnewaytripPage.flight_time_selection))); try {Thread.sleep(5000);} catch
		 * (InterruptedException e) {}
		 * driver.findElement(By.xpath(OnewaytripPage.flight_time_selection)).click();
		 
		int flight_count = driver.findElements(By.xpath(OnewaytripPage.flight_count)).size();
		boolean flight_selected = false;
		log.info("There are " + flight_count + " available for this selected date");
		for (int i = 1; i <= flight_count; i++) {

			try {
				Thread.sleep(5000);

				if (driver.findElement(
						By.xpath("//div[@class='d-flex flex-columnjustify-content-between mb-1']/app-journey[" + i
								+ "]//button[@class='btnbtn-secondary']"))
						.isDisplayed()) {
					driver.findElement(
							By.xpath("//div[@class='d-flex flex-columnjustify-content-between mb-1']/app-journey[" + i
									+ "]//button[@class='btnbtn-secondary']"))
							.click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(
							(By.xpath("//div[@class='d-flexflex-column justify-content-betweenmb-1']/app-journey[" + i
									+ "]//input[@name='fareValue']"))));
					Thread.sleep(5000);
					driver.findElement(
							By.xpath("//div[@class='d-flex flex-columnjustify-content-betweenmb-1']/app-journey[" + i
									+ "]//input[@name='fareValue']"))
							.click();
					//// input[@name='fareValue']
					flight_selected = true;
				}
			} catch (NoSuchElementException e) {
				return ;
			}log.info("There is a sold out flight in the displayed flights");

		}
		Assert.assertTrue(
				"None of the flights are selected, please modify the cities according to the flight availability",
				flight_selected);

		// Thread.sleep(4000);
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.flight_availability_button)));
		// driver.findElement(By.xpath(OnewaytripPage.flight_availability_button)).click();

	}*/

	@Then("^user selects the what time to fly \"([^\"]*)\"$")
	public void user_selects_the_what_time_to_fly(String required_stops) throws Throwable {
		int flight_count = driver.findElements(By.xpath(OnewaytripPage.flight_count)).size();
		boolean flight_selected = false;
		log.info("There are " + flight_count + " available for this selected date");
		for (int i = 1; i <= flight_count; i++) {

			try {
				Thread.sleep(2000);
				// ///section[@class='ng-star-inserted']/div[3]//div[@class='d-flex
				// flex-column']/div[1]/app-journey[1]/div/div[2]/div[2]//button[@type='button']
				if (driver
						.findElement(By
								.xpath("//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div["
										+ i + "]/app-journey/div[1]//app-input[1]/div/label"))
						.isDisplayed()
						&& driver.findElement(By
								.xpath("//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div["
										+ i + "]/app-journey[1]/div/div[2]/div[2]//button[@type='button']"))
								.getText().equals(required_stops)) {

					log.info("The available flights has the " + required_stops + ", selecting the mentioned flight");
					driver.findElement(
							By.xpath("//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div["+ i +"]/app-journey/div[1]//app-input[1]/div/label"))
							.click();
					// wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//div[@class='d-flex
					// flex-column justify-content-between
					// mb-1']/app-journey["+i+"]//input[@name='fareValue']"))));
					// Thread.sleep(5000);
					// driver.findElement(By.xpath("//div[@class='d-flex flex-column
					// justify-content-between
					// mb-1']/app-journey["+i+"]//input[@name='fareValue']")).click();
					//// input[@name='fareValue']
					flight_selected = true;
				}
			} catch (NoSuchElementException e) {
				log.info("There is a sold out flight in the displayed flights");
			}
		}

		for (int j = 1; j <= flight_count; j++) {
			try {

				Thread.sleep(2000);
				if (driver
						.findElement(By
								.xpath("//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div["
										+ j + "]/app-journey/div[1]//app-input[1]/div/label"))
						.isDisplayed() && !flight_selected) {
					driver.findElement(
							By.xpath("//section[@class='ng-star-inserted']//div[@class='d-flex flex-column']/div["
									+ j + "]/app-journey/div[1]//app-input[1]/div/label"))
							.click();
					// wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//div[@class='d-flex
					// flex-column justify-content-between
					// mb-1']/app-journey["+j+"]//input[@name='fareValue']"))));
					// Thread.sleep(5000);
					// driver.findElement(By.xpath("//div[@class='d-flex flex-column
					// justify-content-between
					// mb-1']/app-journey["+j+"]//input[@name='fareValue']")).click();
					// ////input[@name='fareValue']
					flight_selected = true;
				}
			} catch (NoSuchElementException e) {
				log.info("There is a sold out flight in the displayed flights");
			}
		}
		Assert.assertTrue(
				"None of the flights are selected, please modify the cities according to the flight availability",
				flight_selected);
	}

	@Then("^user clicks on the continue button$")
	public void user_clicks_on_the_continue_button() throws Throwable {
		Thread.sleep(2000);
		driver.findElement(By.xpath(OnewaytripPage.Continue_Button)).click();

	}

	@Then("^user switch to the popup and clicks on close button$")
	public void user_switch_to_the_popup_and_clicks_on_close_button() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);

		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@When("^user lands on passengers page$")
	public void user_lands_on_passengers_page() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.passengerpage_validation_passneger_information_text)));
		Assert.assertTrue("User is not  navigated to Passengers page",
				driver.findElement(By.xpath(OnewaytripPage.passengerpage_validation_passneger_information_text))
						.getText().equals("Passenger Information"));
		log.info("User is successfully navigated to passengers page after clicking on search in Book page");
	}

	
	private static final DateFormat dateFormat1 = new SimpleDateFormat("MMddyyyyhhss");

	@Then("^user enters the personal info and contact address$")
	public void user_enters_the_personal_info_and_contact_address() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

		Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("anthony");

		log.info("user able to enters his first name");
		driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
		log.info("user able to enters his last name");
		driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
		log.info("user able to enters his date of birth");
		driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
		log.info("user successfully select the primary passenger  contact checkbox ");
		driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
		log.info("user able to enter his address");
		driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
		log.info("user able to enter his city name");
		driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
		log.info("user able to enter his city zipcode");

		driver.findElement(By.xpath(OnewaytripPage.country)).click();
		Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
		Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
		s1.selectByVisibleText("United States of America");
		// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys("U");
		// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys(Keys.ENTER);
		log.info("user able to selects the country name");
		
		Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        Date sysdate = c.getTime();
        String datetime = dateFormat1.format(sysdate);
		driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys(datetime+"@spirit.com");
		log.info("user able to enters his email");
		String email=driver.findElement(By.xpath(OnewaytripPage.passengers_email)).getAttribute("value");
		driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys(email);
		
		driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
		log.info("user able to enters his phone number");
		Thread.sleep(3000);
		Select s2 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
		s2.selectByVisibleText("Florida");
		log.info("user able to select  his state");
	}

	@Then("^user clicks in the continue button$")
	public void user_clicks_in_the_continue_button() throws Throwable {
		Thread.sleep(4000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		WebElement element = driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
         
	}

	@When("^user landing on the bags page$")
	public void user_landing_on_the_bags_page() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.continue_withou_bags)));

		Assert.assertTrue("User is not  navigated to bags page",
				driver.findElement(By.xpath(OnewaytripPage.continue_withou_bags)).getText()
						.equals("CONTINUE WITHOUT ADDING BAGS"));
		log.info("user succesfully navigated to the bags page after clicking on the passengers info page");
	}

	@Then("^user enters the one carry-on Bag$")
	public void user_enters_the_one_carryon_bag() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.carry_on_bag_plus_button)));
		driver.findElement(By.xpath(BagsPage.carry_on_bag_plus_button)).click();
		// driver.findElement(By.xpath(OnewaytripPage.carry_on_bag)).click();
		// driver.findElement(By.xpath(OnewaytripPage.carry_on_bag)).clear();
		// driver.findElement(By.xpath(OnewaytripPage.carry_on_bag)).sendKeys("1");

	}

	@Then("^user enters the number of checked bag$")
	public void user_enters_the_number_of_checked_bag() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.checked_bag_plusbutton)));
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		// driver.findElement(By.xpath(OnewaytripPage.checked_bag)).click();
		// driver.findElement(By.xpath(OnewaytripPage.checked_bag)).clear();
		// driver.findElement(By.xpath(OnewaytripPage.checked_bag)).sendKeys("1");
	}

	@Then("^user validates the bags total amount$")
	public void user_validates_the_bags_total_amount() throws Throwable {

		String s = driver.findElement(By.xpath(OnewaytripPage.bags_total)).getText();
		log.info("user able to validates his bags total price " + s);

		Assert.assertTrue("user not validate the total bags price",
				driver.findElement(By.xpath(OnewaytripPage.bags_total)).getText().equals(s));
		log.info("user succefully validate the total bags price");
	}

	@Then("^user clicks on the standard pricing continue button$")
	public void user_clicks_on_the_standard_pricing_continue_button() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.standard_picing_continue_button)));
		driver.findElement(By.xpath(OnewaytripPage.standard_picing_continue_button)).click();

	}

	@When("^user lands on seats page$")
	public void user_lands_on_seats_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.continue_without_selecting_seats)));

		Assert.assertTrue("User is not  navigated to seats page",
				driver.findElement(By.xpath(OnewaytripPage.continue_without_selecting_seats)).getText()
						.equals("CONTINUE WITHOUT SELECTING SEATS"));

		log.info("user navigated to seats selection page");
		Thread.sleep(2000);
		String flightnumber = driver.findElement(By.xpath(SeatsPage319.Airbus_Name)).getText();
		log.info("airbus number " + flightnumber);

	}

	@Then("^user selects the continue without selecting the seats$")
	public void user_selects_the_continue_without_selecting_the_seats() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(OnewaytripPage.continue_without_selecting_seats)));
		driver.findElement(By.xpath(OnewaytripPage.continue_without_selecting_seats)).click();

	}

	@When("^user lands on options page$")
	public void user_lands_on_options_page() throws Throwable {
		Thread.sleep(3000);
		Assert.assertTrue("User is not  navigated to options page", driver
				.findElement(By.xpath(OnewaytripPage.options_page_Header)).getText().equals("Add On Trip Options"));

		log.info("user navigated to options page");
	}

	@Then("^user clicks on checkin$")
	public void user_clicks_on_checkin() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.checkin_decision)));
		Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.checkin_decision)));
		dropdown.selectByVisibleText("I'm not sure, I'll decide later");
	}

	@Then("^user clicks on the continue button on options page$")
	public void user_clicks_on_the_save_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.continue_button)));
		driver.findElement(By.xpath(OnewaytripPage.continue_button)).click();

	}

	@When("^user landing on payment page$")
	public void user_landing_on_payment_page() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Paymets_page)));
		Assert.assertTrue("User is not  navigated to payments page",
				driver.findElement(By.xpath(OnewaytripPage.Paymets_page)).getText().equals("Payment"));

		log.info("user navigated to payments page");

	}

	@Then("^user enters the Payments information$")
	public void user_enetrs_the_payments_information() throws Throwable {

		driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("mister disc");
		driver.findElement(By.xpath(OnewaytripPage.card_number)).sendKeys("6011212100000004");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
	}

	@Then("^user clicks on the Book Trip button$")
	public void user_clicks_on_the_book_trip_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Book_trip)));
		driver.findElement(By.xpath(OnewaytripPage.Book_trip)).click();
	}

	@Then("^user clicks on the Booktrip and handle the travel more popup$")
	public void user_clicks_on_the_booktrip_and_handle_the_travel_more_popup() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Book_trip)));
		Thread.sleep(3000);
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(OnewaytripPage.Book_trip)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Travel_more_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Travel_more_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@When("^user lands on confirmation page$")
	public void user_lands_on_conformation_page() throws Throwable {
		Thread.sleep(15000);

		driver.switchTo().frame(3);

		log.info("user suceesfully swith to the iframe");
		Thread.sleep(3000);

		// String T =
		// driver.findElement(By.xpath(OnewaytripPage.iframe_text)).getText();
		// log.info("iframe pop text is " + T);
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.iframe_popup)));
		// driver.findElement(By.xpath(OnewaytripPage.iframe_Nothanks_button)).click();
		// driver.findElement(By.xpath(OnewaytripPage.iframe_continue_button)).click();
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.iframe_close_button)));
		// driver.findElement(By.xpath(OnewaytripPage.iframe_close_button)).click();

		driver.findElement(By.xpath(OnewaytripPage.iframe_popup)).click();

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.iframe_continue_button)));
		// driver.findElement(By.xpath(OnewaytripPage.iframe_Nothanks_button)).click();
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.iframe_continue_button)));
		// driver.findElement(By.xpath(OnewaytripPage.iframe_continue_button)).click();
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.iframe_close_button)));
		// driver.findElement(By.xpath(OnewaytripPage.iframe_close_button)).click();
		// log.info("user succesfully close the frame ");

		driver.switchTo().defaultContent();
		log.info("user succesfully navigate to parent conformation page");

	}

	@Then("^user clicks in the confirmation button$")
	public void user_clicks_in_the_conformation_button() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.print_conformation)));
		driver.findElement(By.xpath(OnewaytripPage.print_conformation)).click();

	}

	@Then("^user clicks on the deparure city$")
	public void user_clicks_on_the_deparure_city() throws Throwable {

		driver.findElement(By.xpath(OnewaytripPage.clickon_fromcity)).click();
		log.info("user succesfully clicks on the From city field");

	}

	@Then("^user selects the Domestic city as a departure city$")
	public void user_selects_the_domestic_city_as_a_departure_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Denever_city)));
		driver.findElement(By.xpath(OnewaytripPage.Denever_city)).click();
		log.info("user succesfully selects the domestic city as a departure city ");
	}

	@Then("^user clicks on the To city$")
	public void user_clicks_on_the_to_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.clickon_Tocity)));
		driver.findElement(By.xpath(OnewaytripPage.clickon_Tocity)).click();
		log.info("user succesfully selects the to city ");
	}

	@Then("^user selects international city as a arriaval city$")
	public void user_selects_international_city_as_a_arriaval_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Cancun_Mexico_city)));
		driver.findElement(By.xpath(OnewaytripPage.Cancun_Mexico_city)).click();
		log.info("user succesfully selects the cancun city as a international city");
	}

	@Then("^User clicks on Search button and switch to the popup above the international field and clicks on OK button then switch to the child popup and enters the date of birth$")
	public void user_clicks_on_search_button_and_switch_to_the_popup_above_the_international_field_and_clicks_on_ok_button_then_switch_to_the_child_popup_and_enters_the_date_of_birth()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(OnewaytripPage.International_city_field_popup_ok_button)).click();
		log.info("user successfully clicks on the ok button on the international city field popup");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -4);
		Date date = c.getTime();
		log.info("Date of birth is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)));

		WebElement element = driver
				.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		log.info(
				"user succesfully clicks on the continue button on the young travelers popup and navigates to the flight availability page ");
	}

	@Then("^user clicks on the search flight button and switch to the popup enters the child date of birth less than seven days old$")
	public void user_clicks_on_the_search_flight_button_and_switch_to_the_popup_enters_the_child_date_of_birth_less_than_seven_days_old()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -4);
		Date date = c.getTime();
		log.info("Date of birth is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the popup continue button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Infant_acceptance_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Infant_acceptance_popup)).click();
		log.info("user succesfully handled the infant acceptance popup");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the search flight and switch to the popup enters the child date of birth under two years selects lap child$")
	public void user_clicks_on_the_search_flight_and_switch_to_the_popup_enters_the_child_date_of_birth_under_two_years_selects_lap_child()
			throws Throwable {
		Thread.sleep(3000);
		String parentHandle = driver.getWindowHandle();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -6);
		c.add(Calendar.YEAR, -1);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.lap_child_require_seat)));
		driver.findElement(By.xpath(OnewaytripPage.lap_child_require_seat)).click();
		log.info("user successfully selects the lap child require a car seat ");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the popup continue button");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user selects the child under five years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button$")
	public void user_selects_the_child_under_five_years_old_and_switch_to_the_popup_clicks_on_continue_button_and_then_switch_to_the_umnr_popup_clicks_on_close_button()
			throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.clickon_passengers)));
		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		driver.findElement(By.xpath(BookPage.select_child)).click();
		log.info("user successfully selects one UMNR child ");
		String parentHandle = driver.getWindowHandle();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -6);
		c.add(Calendar.YEAR, -3);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the child DOB popup continue button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.UMNR_popup_close_button)));
		WebElement element = driver.findElement(By.xpath(OnewaytripPage.UMNR_popup_close_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		log.info("user succesfully switch to the UMNR popup and close it");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user selects the child age between five to fourteen years old and switch to the popup clicks on continue button and then switch to the UMNR popup clicks on close button$")
	public void user_selects_the_child_age_between_five_to_fourteen_years_old_and_switch_to_the_popup_clicks_on_continue_button_and_then_switch_to_the_umnr_popup_clicks_on_close_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();

		c.add(Calendar.YEAR, -9);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the child DOB popup continue button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button)));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button)).click();
		log.info("user succesfully switch to the UMNR popup and Accepts it");
		driver.switchTo().window(parentHandle);

	}

	@Then("^user selects two adult and children passenegers$")
	public void user_selects_two_adult_and_children_passenegers() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.clickon_passengers)));
		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		for (int i = 0; i < 2; i++) {
			driver.findElement(By.xpath(BookPage.select_child)).click();
			log.info("Now the child count is " + (i + 1));
		}

	}

	@Then("^user clicks on the searchflight and switch to the popup and enters the DOB of the children and clicks on the continue button$")
	public void user_clicks_on_the_searchflight_and_switch_to_the_popup_and_enters_the_dob_of_the_children_and_clicks_on_the_continue_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();

		c.add(Calendar.YEAR, -9);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");
		Date currentDate2 = new Date();
		Calendar c2 = Calendar.getInstance();

		c.add(Calendar.YEAR, -9);
		Date date2 = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age2 = dateFormat.format(date);
		c.setTime(date2);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_two_date_of_birth)).sendKeys(age2);
		log.info("user succesfully enters the child two date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the young traveler popup continue button");

	}

	@Then("^user clicks on the Miles tab next to the dollar tab and login as free spirit member$")
	public void user_clicks_on_the_miles_tab_next_to_the_dollar_tab_and_login_as_free_spirit_member() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.BookPage_Miles_Tab)));
		driver.findElement(By.xpath(OnewaytripPage.BookPage_Miles_Tab)).click();
		log.info("user succesfully clicks on the Miles tab");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys(free_spirit_email);

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys(free_spirit_password);

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a free spirit member");
	}

	@Then("^user selects the Nine adult passengers$")
	public void user_selects_the_nine_adult_passengers() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		for (int i = 0; i < 8; i++) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
			driver.findElement(By.xpath(BookPage.select_adults)).click();
			log.info("Now the child count is " + (i + 1));
		}

	}

	@Then("^user verifies that round trip is selected by default$")
	public void user_verifies_that_round_trip_is_selected_by_default() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RoundtripPage.Roundtrip_radio_button)));
		driver.findElement(By.xpath(RoundtripPage.Roundtrip_radio_button)).isSelected();
		log.info("user succesfully verifies that roundtrip radio button is selected");

	}

	@Then("^user selects the departure city as a Aruba$")
	public void user_selects_the_departure_city_as_a_canucun_mexico() throws Throwable {

		driver.findElement(By.xpath(OnewaytripPage.clickon_fromcity)).click();
		log.info("user succesfully clicks on the From city field");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RoundtripPage.Aruba_city_From_city_INT)));
		driver.findElement(By.xpath(RoundtripPage.Aruba_city_From_city_INT)).click();
		log.info("user succesfully selects the Aruba as a departure city ");

	}

	@Then("^user select the arrival city as a canucun Mexico$")
	public void user_select_the_arrival_city_as_a_puerto_rico() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.clickon_Tocity)));
		driver.findElement(By.xpath(OnewaytripPage.clickon_Tocity)).click();
		log.info("user succesfully selects the to city ");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RoundtripPage.cancun_city_To_city_INT)));
		driver.findElement(By.xpath(RoundtripPage.cancun_city_To_city_INT)).click();
		log.info("user succesfully selects the cancun city as a arrival city");

	}

	@Then("^user clicks on the searchflight button and switch to the popup and enters the child DOB and clicks on continue button$")
	public void user_clicks_on_the_searchflight_button_and_switch_to_the_popup_and_enters_the_child_dob_and_clicks_on_continue_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();

		c.add(Calendar.YEAR, -9);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user successfully clicks on the young traveler popup continue button");

	}

	@Then("^User Clicks on the x on thrills combo pop up$")
	public void user_clicks_on_the_x_on_thrills_combo_pop_up() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);

		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user selects clicks on continue button and eneter the date of birth for UNMR fifteen and older$")
	public void user_selects_clicks_on_continue_button_and_eneter_the_date_of_birth_for_unmr_fifteen_and_older()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.YEAR, -15);
		Date date = c.getTime();
		log.info("Lap child's DOB is : " + dateFormat.format(date));
		String age = dateFormat.format(date);
		c.setTime(date);

		driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(age);
		log.info("user succesfully enters the child date of birth");

		driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

		driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
		log.info("user successfully clicks on the young traveler popup continue button");

	}

	@Then("^user enters the UMNR passenger information and changes the DOB$")
	public void user_enters_the_umnr_passenger_information_and_changes_the_dob() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("jhonson");

		log.info("user succesfully enter passenger information");

	}

	@Then("^user validates that the Actvie duty Military check box is suppressed$")
	public void user_validates_that_the_actvie_duty_military_check_box_is_suppressed() throws Throwable {

		try {
			if (driver.findElement(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox))
					.isDisplayed()) {
				log.info("Element is Visible");
			}
		} catch (NoSuchElementException e) {
			{
				log.info("Element is InVisible");
			}
		}

		driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
		log.info("user successfully select the primary passenger  contact checkbox ");

	}

	@Then("^user clicks on the promocode link$")
	public void user_clicks_on_the_promocode_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.add_promocode_Homepage)));
		driver.findElement(By.xpath(OnewaytripPage.add_promocode_Homepage)).click();
		log.info("user successfully clicks on the promocode link");
	}

	@Then("^user send the invalid promocode$")
	public void user_send_the_invalid_promocode() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_input_homepage)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_input_homepage)).sendKeys(Invalid_promo_code);

		log.info("user succesfully enters the inavalid promocode");
	}

	@Then("^user clicks on the search flight button and switch to the popup and validate the text then clicks on the edit promocode$")
	public void user_clicks_on_the_search_flight_button_and_switch_to_the_popup_and_validate_the_text_then_clicks_on_the_edit_promocode()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Search_button)));
		driver.findElement(By.xpath(OnewaytripPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_popup_text)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_popup_text)).getText();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_popup_edit_coupon_code)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_popup_edit_coupon_code)).click();

		driver.switchTo().window(parentHandle);
	}

	@Then("^user clicks on the search flight button and switch to the popup and clicks on the close button$")
	public void user_clicks_on_the_search_flight_button_and_switch_to_the_popup_and_clicks_on_the_close_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Search_button)));
		driver.findElement(By.xpath(OnewaytripPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_popup_close_button)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_popup_close_button)).click();

		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the serach flight button and switch to the popup and clicks on the continue without the coupon code$")
	public void user_clicks_on_the_serach_flight_button_and_switch_to_the_popup_and_clicks_on_the_continue_without_the_coupon_code()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Search_button)));
		driver.findElement(By.xpath(OnewaytripPage.Search_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_popup_continue_without_coupon_code)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_popup_continue_without_coupon_code)).click();

		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the promocode link and enters the valid promocode$")
	public void user_clicks_on_the_promocode_link_and_enters_the_valid_promocode() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.add_promocode_Homepage)));
		driver.findElement(By.xpath(OnewaytripPage.add_promocode_Homepage)).click();
		log.info("user successfully clicks on the promocode link");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_input_homepage)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_input_homepage)).sendKeys(Promo_code);

		log.info("user succesfully enters the valid promocode for dollers off");

	}

	@Then("^user clicks on the continue without adding bags and switch to the popup and clicks on the i do not need bags$")
	public void user_clicks_on_the_continue_without_adding_bags_and_switch_to_the_popup_and_clicks_on_the_i_do_not_need_bags()
			throws Throwable {

		FluentWait<WebDriver> wait = new WebDriverWait(driver, 60).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		String FocusonBagsPage = driver.getWindowHandle();
		driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user clicks on the continue wihout adding the bags");
		for (String iNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(iNeedBags);
		}

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_idont_need_bags)));
		driver.findElement(By.xpath(BagsPage.popup_idont_need_bags)).click();

		driver.switchTo().window(FocusonBagsPage);
		log.info("user clicks on the popup window i  don't need bags and navigate back to the parent window");

	}

	@Then("^user clciks on the checkin drop down and selects the i will check in at mobile for free$")
	public void user_clciks_on_the_checkin_drop_down_and_selects_the_i_will_check_in_at_mobile_for_free()
			throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.checkin_decision)));
		Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.checkin_decision)));
		dropdown.selectByVisibleText("I'll check in at Spirit.com/Mobile for free");

	}

	@Then("^user clicks on the Multicity Radio button$")
	public void user_clicks_on_the_multicity_radio_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multi_city_radio_button)));
		driver.findElement(By.xpath(MultiCityPage.Multi_city_radio_button)).click();
		log.info("user successfully selects the multicity radio button ");
	}

	@Then("^user clicks on the from city on first city pairs$")
	public void user_clicks_on_the_from_city_on_first_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_clickson_fromcity_one)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_clickson_fromcity_one)).click();
		log.info("user successfully clicks on the from city on the first city pairs");

	}

	@Then("^user selects the from city on first city pairs$")
	public void user_selects_the_from_city_on_first_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_select_one__from_city)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_select_one__from_city)).click();
		log.info("user successfully selects the from city on the first city pairs");

	}

	@Then("^user clicks on the to city on first city pairs$")
	public void user_clicks_on_the_to_city_on_first_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_clickson_Tocity_one)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_clickson_Tocity_one)).click();
		log.info("user successfully clicks on the TO city on the first city pairs");
	}

	@Then("^user selects the to city on first city pairs$")
	public void user_selects_the_to_city_on_first_city_pairs() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicicty_select_one_to_city)));
		driver.findElement(By.xpath(MultiCityPage.Multicicty_select_one_to_city)).click();
		log.info("user successfully selects the TO city on the first city pairs");

	}

	@Then("^user enters the date of travel for first city pairs$")
	public void user_enters_the_date_of_travel_for_first_city_pairs() throws Throwable {
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_date_one)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_date_one)).click();

		driver.findElement(By.xpath(MultiCityPage.Multicity_date_one)).sendKeys(DepartureDate);

	}

	@Then("^user clicks on the from city on second city pairs$")
	public void user_clicks_on_the_from_city_on_second_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_clickson_fromcity_two)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_clickson_fromcity_two)).click();
		log.info("user succesfully clicks on the ");

	}

	@Then("^user selects the from city on second city pairs$")
	public void user_selects_the_from_city_on_second_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_select_two_fromcity)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_select_two_fromcity)).click();
		log.info("user successfully selects  on the from city on second city pair  ");

	}

	@Then("^user clicks on the to city on second city pairs$")
	public void user_clicks_on_the_to_city_on_second_city_pairs() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_clickson_Tocity_two)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_clickson_Tocity_two)).click();
		log.info("user succesfully clicks on the to city on the decond city pair ");
	}

	@Then("^user selects the to city on second city pairs$")
	public void user_selects_the_to_city_on_second_city_pairs() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_select_two_TOcity)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_select_two_TOcity)).click();
		log.info("user successfully selects  on the from city on second city pair  ");

	}

	@Then("^user enters the date of travel on second city pair$")
	public void user_enters_the_date_of_travel_on_second_city_pair() throws Throwable {

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 2); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_date_two)));
		driver.findElement(By.xpath(MultiCityPage.Multicity_date_two)).click();

		driver.findElement(By.xpath(MultiCityPage.Multicity_date_two)).sendKeys(DepartureDate);

	}

	@Then("^user mouse over on the question mark and switch to the popup and get text$")
	public void user_mouse_over_on_the_question_mark_and_switch_to_the_popup_and_get_text() throws Throwable {

		String expectedTooltip = "You'll find our Promo Codes in our promotional emails.";

		WebElement question_mark = driver.findElement(By.xpath(FlightAvailability.Flight_availability_tip_tool));
		question_mark.click();
		// Actions builder = new Actions(driver);
		// builder.clickAndHold().moveToElement(question_mark);
		// builder.moveToElement(question_mark).build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MultiCityPage.Multicity_mouse_over_text)));
		String s = driver.findElement(By.xpath(MultiCityPage.Multicity_mouse_over_text)).getText();
		log.info(" the mouse over text is" + s);
	}

	@Then("^user clicks on the promocode link and enters the invalid promocode$")
	public void user_clicks_on_the_promocode_link_and_enters_the_invalid_promocode() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FlightAvailability.Flight_availability_promo_code)));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_promo_code)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_input_homepage)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_input_homepage)).sendKeys(Invalid_promo_code);

		log.info("user succesfully enters the inavalid promocode");

	}

	@Then("^user clicks on the promocode link and enters the valid promocode for percent off$")
	public void user_clicks_on_the_promocode_link_and_enters_the_valid_promocode_for_percent_off() throws Throwable {

		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.add_promocode_Homepage)));
		driver.findElement(By.xpath(OnewaytripPage.add_promocode_Homepage)).click();
		log.info("user successfully clicks on the promocode link");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.promo_code_input_homepage)));
		driver.findElement(By.xpath(OnewaytripPage.promo_code_input_homepage)).sendKeys(Promo_code_Percent_off);

		log.info("user succesfully enters the valid promocode for percent off");

	}

	@Then("^user select today's date as a departure date$")
	public void user_select_todays_date_as_a_departure_date() throws Throwable {

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date

		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Date)));
		driver.findElement(By.xpath(BookPage.Date)).click();

		driver.findElement(By.xpath(BookPage.Date)).clear();
		driver.findElement(By.xpath(BookPage.Date)).sendKeys(DepartureDate);

	}

	@Then("^user switch to the popup and clicks on choose thrills combo$")
	public void user_switch_to_the_popup_and_clicks_on_choose_thrills_combo() throws Throwable {
		String parentHandle = driver.getWindowHandle();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Continue_Button)));
		Thread.sleep(3000);
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Choose_thrills_combo)));
		Thread.sleep(2000);

		WebElement ele1 = driver.findElement(By.xpath(OnewaytripPage.Choose_thrills_combo));
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", ele1);
		log.info("User selects Thrills combo");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user switch to military bags popup and continue$")
	public void user_switch_to_military_bags_popup_and_continue() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Three_free_military_bags)));
		driver.findElement(By.xpath(OnewaytripPage.Three_free_military_bags)).click();
		driver.findElement(By.xpath(OnewaytripPage.Continue_military_bags)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user switch to military bags popup and close X$")
	public void user_switch_to_military_bags_popup_and_close_x() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button));
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Military_bags_popup_x)));
		driver.findElement(By.xpath(OnewaytripPage.Military_bags_popup_x)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user switch to military bags popup and choose thirills combo$")
	public void user_switch_to_military_bags_popup_and_choose_thirills_combo() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button));
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Thrills_combo_savings)));
		driver.findElement(By.xpath(OnewaytripPage.Thrills_combo_savings)).click();
		driver.findElement(By.xpath(OnewaytripPage.Continue_military_bags)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user validates verbiage of departure flight informations$")
	public void user_validates_verbiage_of_departure_flight_informations() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Your_selected_departure_txt)));
		WebElement deptxt = driver.findElement(By.xpath(OnewaytripPage.Your_selected_departure_txt));
		String fontWeight = deptxt.getCssValue("font-weight");
		Assert.assertTrue(fontWeight.equals("bold") || fontWeight.equals("900"));
		String textalign = deptxt.getCssValue("text-align");
		Assert.assertTrue(textalign.equals("left"));
		log.info("Verified verbiage (Your Selected Departure) and alignments");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_flight1_editlink)));
		WebElement depflyedit = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_flight1_editlink));
		assertEquals("900", depflyedit.getCssValue("font-weight"));
		assertEquals("center", depflyedit.getCssValue("text-align"));
		assertEquals("#0073e6", depflyedit.getCssValue("color"));
		log.info("Verified EDIT link as center and font color for Depart Flight");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_date)));
		WebElement depflydate = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_date));
		assertEquals("left", depflydate.getCssValue("text-align"));
		assertEquals("#1rem", depflydate.getCssValue("font-size"));
		log.info("Verified Date is left aligned and font size for Depart Flight");

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_Plane_icon)));
		WebElement depflyicon = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_Plane_icon));
		assertEquals("#ccc", depflyicon.getCssValue("color"));
		log.info("Verified departure plane icon color of Depart Flight");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_stops)));
		WebElement depflystop = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_stops));
		assertEquals("#0073e6", depflystop.getCssValue("color"));
		log.info("Verified departure plane stops &  seat availablility link's font color of Depart Flight");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_price)));
		WebElement depflyprice = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_price));
		assertEquals("right", depflyprice.getCssValue("text-align"));
		log.info("Verified departure plane stops &  seat availablility link's font color of Depart Flight");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Selected_departure_flight2_headertxt)));
		WebElement depfly2 = driver.findElement(By.xpath(OnewaytripPage.Selected_departure_flight2_headertxt));
		assertEquals("400", depfly2.getCssValue("font-weight"));
		assertEquals("left", depfly2.getCssValue("text-align"));
		assertEquals("#1px solid #333", depfly2.getCssValue("border"));
		log.info("Verified verbiages alignments as left for Return Flight");

	}

	@Then("^user switch to the popup and clicks on continue with bare fare button$")
	public void user_switch_to_the_popup_and_clicks_on_continue_with_bare_fare__button() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Continue_Button)));
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Continue_with_bare_fare)));
		driver.findElement(By.xpath(OnewaytripPage.Continue_with_bare_fare)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user switch to the popup and clicks on choose this bundle$")
	public void user_switch_to_the_popup_and_clicks_on_choose_this_bundle() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Continue_Button)));
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Choose_this_bundle)));
		driver.findElement(By.xpath(OnewaytripPage.Choose_this_bundle)).click();
		log.info("User selects Choose this bundle");
		driver.switchTo().window(parentHandle);
	}

	@Then("^User navigates to Seats Page$")
	public void user_navigates_to_seats_page() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/book/seats"));

		Assert.assertTrue("User is not  navigated to seats page",
				driver.findElement(By.xpath(OnewaytripPage.Choose_your_seat_txt)).getText().equals("Choose Your Seat"));
		log.info("user navigated to Seats page");
	}

	@Then("^User selects most recent available flights$")
	public void User_selects_most_recent_available_flights() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.first_standard_flight)));
		driver.findElement(By.xpath(OnewaytripPage.first_standard_flight)).click();
		log.info("Use selects first available standard flight");
	}

	@Then("^user clicks in the continue button when log in as military$")
	public void user_clicks_in_the_continue_button_when_log_in_as_military() throws Throwable {
		Thread.sleep(6000);
		WebElement chkbtn = driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk));
		if (chkbtn.isSelected()) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			log.info("User Able to continue with the booking process ");
		} else {
			// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Primary_passenger_chk)));
			driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();
			driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			log.info("User Able to continue with the booking process ");
		}
	}

	@Then("^user Click on the plus button next to checked bags for military member$")
	public void user_click_on_the_plus_button_next_to_checked_bags_for_military_member() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.checked_bag_plusbutton)));
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		String Bagsprice = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
		Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
				driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice));
		log.info("Validated Passenger 1's 1st Checked Bag Price is " + Bagsprice);
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		String Bagsprice2 = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
		Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
				driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice2));
		log.info("Validated Passenger 1's  2nd Checked Bag Price is " + Bagsprice2);
		driver.findElement(By.xpath(BagsPage.checked_bag_plusbutton)).click();
		String Bagsprice3 = driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText();
		Assert.assertTrue("After increasing the bags count to 1 the cost is not increased",
				driver.findElement(By.xpath(BagsPage.checked_bag_price_validation)).getText().equals(Bagsprice3));
		log.info("Validated Passenger 1's  3rd Checked Bag Price is " + Bagsprice3);

	}

	@Then("^User validates save money of 9DFC member in itinerary with bundle$")
	public void User_validates_save_money_of_9DFC_member_in_itinerary_with_bundle() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Save_money_bundle)));
		WebElement savemoney = driver.findElement(By.xpath(BagsPage.Save_money_bundle));
		String savings = savemoney.getText();
		log.info("Save money message should be " + savings);
		if (savings.startsWith("CONGRATS! YOU’RE SAVING $")) {
			log.info("The price in the $9FC block is validated as " + savings);
		} else {
			log.info("The price in the $9FC block is not discounted");
		}
	}

	@Then("^user validates the bags total with the shopping cart of military$")
	public void user_validates_the_bgas_total_with_the_shopping_cart_of_military() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.your_itinerary_caret)));
		driver.findElement(By.xpath(BagsPage.Bags_total_button)).click();
		log.info("user successfully clicked on the your itinerary caret");
		String totalbagscost = driver.findElement(By.xpath(BagsPage.total_bags_price)).getText();
		log.info("Total Bags price " + totalbagscost);
		Assert.assertTrue("User after increasing the bags count,the total is not increased",
				driver.findElement(By.xpath(BagsPage.total_bags_price)).getText().equals(totalbagscost));
		log.info("user successfully validated the bags prices in itinerary cart");

	}

	@Then("^user get pnr and navigate to checkin entry$")
	public void user_get_pnr_and_navigate_to_checkin_entry() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@class='card mb-2 p-2']//div[1]//div[2]//div[3]")));
		String pnr = driver.findElement(By.xpath("//div[@class='card mb-2 p-2']//div[1]//div[2]//div[3]")).getText();
		String confirmation = pnr.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + confirmation);
		Thread.sleep(3000);
		driver.switchTo().frame(3);
		driver.findElement(By.xpath(OnewaytripPage.iframe_popup)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_mainmenu)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_mainmenu)).click();
		log.info("Clicked on the checkin button present on top banner");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
		log.info("Passenger entering the last name as mentioned");
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(confirmation);
		log.info("Entered the confirmation code as : " + confirmation);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.checkin_button)));
		driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
		log.info("Then user clicked on the search button to check the info of the booked ticket");
	}

	@Then("^user clicks on the standard fare continue button$")
	public void user_clicks_on_the_standard_fare_continue_button() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.std_fare_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FlightAvailability.popup_seat_dont_purchase_myseats)));
		driver.findElement(By.xpath(FlightAvailability.popup_seat_dont_purchase_myseats)).click();
		log.info("User clicks 'Don't ourchase my seats' option from Seats pop-up");
		driver.switchTo().window(parentHandle);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button']")));
		driver.findElement(By.xpath("//button[@type='button']")).click();
		log.info("User clicks continue from extras page");

	}

	@Then("^user enters the different credit card numbers and validate the card badge$")
	public void user_enters_the_different_credit_card_numbers_and_validate_the_card_badge() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PaymentPage.credit_card_name_validation_discover)));

		cardnumber.sendKeys(Discover_card_number);
		Assert.assertTrue("user not validate the discover card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_discover)).getAttribute("alt")
						.equals("Discover Card"));
		log.info("user succesfully validates the discover card badge");
		cardnumber.clear();

		cardnumber.sendKeys(Visa_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_visa))));
		Assert.assertTrue("user not validate the visa card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_visa)).getAttribute("alt")
						.equals("Visa Card"));
		log.info("user succesfully validates the Visa card badge");
		cardnumber.clear();

		cardnumber.sendKeys(American_Express_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_American_Express))));
		Assert.assertTrue("user not validate the Amex card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_American_Express))
						.getAttribute("alt").equals("American Express"));
		log.info("user succesfully validates the Amex card badge");

		cardnumber.clear();
		cardnumber.sendKeys(Master_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_Master))));
		Assert.assertTrue("user not validate the Master card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_Master)).getAttribute("alt")
						.equals("Master Card"));
		log.info("user succesfully validates the Master card badge");

		cardnumber.clear();
		log.info("user succesfully clear the card number");
	}

	@Then("^user clicks REDEEM A VOUCHER caret$")
	public void user_clicks_redeem_a_voucher_caret() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PaymentPage.payments_page_redeem_a_voucher)));
		driver.findElement(By.xpath(PaymentPage.payments_page_redeem_a_voucher)).click();
		driver.findElement(By.xpath(PaymentPage.payments_page_confirmation_code)).click();
	}

	@Then("^user enter PNR into Reservation Credit and click go$")
	public void user_enter_pnr_into_reservation_credit_and_click_go() throws Throwable {
		// String pnr=user_get_pnr_and_navigate_to_checkin_entry().confirmation;
		// driver.findElement(By.xpath(PaymentPage.payments_page_confirmation_code)).sendKeys(pnr);
		driver.findElement(By.xpath(PaymentPage.payments_page_con_go)).click();
		log.info("User entered PNR and clicked Go button");
		;
	}

	@Then("^user selects creditcard which are already saved onfile$")
	public void user_selects_creditcard_which_are_already_saved_onfile() throws Throwable {
	}

	@Then("^user verify billing address matches contact address$")
	public void user_verify_billing_address_matches_contact_address() throws Throwable {
	}

	@Then("^user verify booking price$")
	public void user_verify_booking_price() throws Throwable {
	}

	@Then("^user enters the AMEX credit card number and validate the card badge$")
	public void user_enters_the_amex_credit_card_number_and_validate_the_card_badge() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		cardnumber.sendKeys(American_Express_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_American_Express))));
		Assert.assertTrue("user not validate the AMEX card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_American_Express))
						.getAttribute("alt").equals("American Express"));
		log.info("user succesfully validates the AMEX card badge");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
		log.info("User entered AMEX creditcard informations and checked terms and conditions");

	}

	@Then("^user enters the Master credit card number and validate the card badge$")
	public void user_enters_the_master_credit_card_number_and_validate_the_card_badge() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		cardnumber.clear();
		cardnumber.sendKeys(Master_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_Master))));
		Assert.assertTrue("user not validate the Master card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_Master)).getAttribute("alt")
						.equals("Master Card"));
		log.info("user succesfully validates the Master card badge");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
		log.info("User entered MasterCard informations and checked terms and conditions");
	}

	@Then("^user enters the Visa credit card number and validate the card badge$")
	public void user_enters_the_visa_credit_card_number_and_validate_the_card_badge() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		cardnumber.clear();
		cardnumber.sendKeys(Visa_card_number);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_visa))));
		Assert.assertTrue("user not validate the visa card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_visa)).getAttribute("alt")
						.equals("Visa Card"));
		log.info("user succesfully validates the Visa card badge");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
		log.info("User entered Visa creditcard informations and checked terms and conditions");
	}

	@Then("^user enters the Discover credit card number and validate the card badge$")
	public void user_enters_the_discover_credit_card_number_and_validate_the_card_badge() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		cardnumber.clear();
		cardnumber.sendKeys(Discover_card_number);
		Assert.assertTrue("user not validate the discover card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_discover)).getAttribute("alt")
						.equals("Discover Card"));
		log.info("user succesfully validates the discover card badge");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("342");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Book_trip)).click();
		Assert.assertTrue("Terms & Conditions error message is mismatch or Invalid",
				driver.findElement(By.xpath(OnewaytripPage.Term_check_error)).isDisplayed());
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
		log.info("User entered Discover creditcard informations and checked terms and conditions");
	}

	@Then("^user switch to the popup and clicks on close button for FS member$")
	public void user_switch_to_the_popup_and_clicks_on_close_button_for_FS_member() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user enters the personal info of UMNR under age of 14$")
	public void user_enters_the_personal_info_of_UMNR_under_age_of_14() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

		Select title = new Select(driver.findElement(By.xpath(OnewaytripPage.Title)));
		title.selectByIndex(1);
		driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("Unaccompanied");

		log.info("Minor able to enters his first name");
		driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("Minor");
		log.info("user able to enters his last name");
		log.info("DOB of passenger field is autopopulated and it is "
				+ driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).getAttribute("readOnly"));
		driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
		log.info("Minor successfully select the primary passenger  contact checkbox ");
		driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("2800 Executive way");
		log.info("Minor able to enter his address");
		driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("Miramar");
		log.info("Minor able to enter his city name");
		driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33025");
		log.info("Minor able to enter his city zipcode");
		driver.findElement(By.xpath(OnewaytripPage.country)).click();
		Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
		Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
		dropdown.selectByVisibleText("United States of America");
		log.info("Minor able to selects the country name");
		driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("unaccompanied.minor@spirit.com");
		log.info("Minor able to enters his email");
		driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("unaccompanied.minor@spirit.com");
		driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("8014012222");
		log.info("Minor able to enters his phone number");
		Thread.sleep(3000);
		Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
		s1.selectByVisibleText("Florida");
		log.info("Minor able to select  his state");
	}

	@Then("^User clicks continue button and handle UMNR popup of connecting flights$")
	public void User_clicks_continue_and_handle_UMNR_popup_of_connecting_flights() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(4000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			Thread.sleep(4000);
			log.info("UMNR content displays as "
					+ driver.findElement(By.xpath(OnewaytripPage.Umnr_popup_connecting_flight_content)).getText());
			Assert.assertTrue("UMNR is not qualified for connecting flights popup is not valid or mismatch",
					driver.findElement(By.xpath(OnewaytripPage.Umnr_popup_connecting_flight_content)).getText().equals(
							"We're sorry, Unaccompanied Minor Service is not permitted on connecting flights."));
			log.info(
					"Unaccompanied Minor Service is not permitted on connecting flights is validated under UMNR popup");
		}
	}

	@Then("^user clicks reset password link under log in$")
	public void user_clicks_reset_password_link_under_log_in() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Passenger_resetpassword_link)));
		driver.findElement(By.xpath(PassengerinfoPage.Passenger_resetpassword_link)).click();
		log.info("Passenger clicks reset passwordlink from login page");
	}

	@Then("^user enter incorrect FS number and validates retrieve password$")
	public void user_enter_incorrect_fs_number_and_validates_retrieve_password() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/retrieve-password"));
		Assert.assertTrue("User is not navigated to retrieve password page",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/retrieve-password"));
		log.info("User navigated to reset password page");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("D54654DW");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		log.info("User enters invalid FS number and clicks send button");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)));
		log.info(driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)).getText());
		Assert.assertTrue("FS number error message is invalid or mismatch",
				driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)).getText()
						.equals("A valid Free Spirit® Number or associated Email address is required"));
		log.info("User validated invalid FS number and error message");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("1000216091");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		log.debug(driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_success_msg)).getText());
		Assert.assertTrue("FS number entered is invalid or mismatch",
				driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_success_msg)).getText()
						.equals("Success!  Password reset instruction email queued successfully"));
		log.info("User resets password successfully");
	}

	@Then("^user enter incorrect FS email and validates retrieve password$")
	public void user_enter_incorrect_fs_email_and_validates_retrieve_password() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/retrieve-password"));
		Assert.assertTrue("User is not navigated to retrieve password page",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/retrieve-password"));
		log.info("User navigated to reset password page");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("D5465@4DW.zcx");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		log.info("User enters invalid FS email address and clicks send button");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)));
		log.info(driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)).getText());
		Assert.assertTrue("FS address error message is invalid or mismatch",
				driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber_err)).getText()
						.equals("A valid Free Spirit® Number or associated Email address is required"));
		log.info("User validated invalid FS email address and error message");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("spiritfly18@gmail.com");
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Password_Reset_popup_content)));
			Assert.assertTrue("Password reset popup is not valid",
					driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_content)).isDisplayed());
			driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_returnlogin)).click();
		}
		driver.switchTo().window(parentHandle);
		log.info("User resets password successfully");
	}
	// private static final DateFormat dateFormat1 = new
	// SimpleDateFormat("MMddyyyy");
	// public String datetime(){
	// Calendar c = Calendar.getInstance();
	// c.add(Calendar.DATE, 0);
	// Date sysdate = c.getTime();
	// String datetime = dateFormat1.format(sysdate);
	// return datetime;
	// }

	// @Then("^user login to gmail and retrieve password$")
	// public void user_login_to_gmail_and_retrieve_password(String email, String
	// password) throws Throwable {
	// Properties props = System.getProperties();
	// props.setProperty("mail.store.protocol", "imaps");
	//
	// Session session = Session.getDefaultInstance(props, null);
	// Store store = session.getStore("imaps");
	// store.connect("imap.gmail.com", email, password);
	//
	// Folder folder = store.getFolder("INBOX");
	// folder.open(Folder.READ_WRITE);
	//
	// System.out.println("Total Message:" + folder.getMessageCount());
	// System.out.println("Unread Message:" + folder.getUnreadMessageCount());
	//
	// Message[] messages = null;
	// boolean isMailFound = false;
	// Message mailFromProx = null;
	//
	// // Search for mail from Prox with Subject = 'Email verification
	// // Test-case'
	// for (int i = 0; i <= 5; i++) {
	//
	// messages = folder.search(new SubjectTerm("Email varification Testcase"),
	// folder.getMessages());
	// // Wait for 20 seconds
	// if (messages.length == 0) {
	// Thread.sleep(20000);
	// }
	// }
	//
	// // Search for unread mail
	// // This is to avoid using the mail for which
	// // Registration is already done
	// for (Message mail : messages) {
	// if (!mail.isSet(Flags.Flag.SEEN)) {
	// mailFromProx = mail;
	// System.out.println("Message Count is: " + mailFromProx.getMessageNumber());
	// isMailFound = true;
	// }
	// }
	//
	// // Test fails if no unread mail was found
	// if (!isMailFound) {
	// throw new Exception("Could not find new mail from iGotThis :-(");
	//
	// // Read the content of mail and get password
	// } else {
	// String line;
	// StringBuffer buffer = new StringBuffer();
	// BufferedReader reader = new BufferedReader(new
	// InputStreamReader(mailFromProx.getInputStream()));
	// while ((line = reader.readLine()) != null) {
	// buffer.append(line);
	// }
	// System.out.println(buffer);
	// String result = buffer.toString().substring(buffer.toString().indexOf("is:")
	// + 1,
	// buffer.toString().indexOf("3. Start enjoying an easier life!"));
	// String resultxx = result.substring(4, result.length() - 1);
	//
	// // Print passsword
	// System.out.println(resultxx);
	// }

	// }

	@Then("^user enter valid FS email and validates retrieve password$")
	public void user_enter_valid_fs_email_and_validates_retrieve_password() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/retrieve-password"));
		Assert.assertTrue("User is not navigated to retrieve password page",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/retrieve-password"));
		log.info("User navigated to reset password page");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("spiritfly18@gmail.com");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		log.info("User enters valid FS email address and clicks send button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Password_Reset_popup_content)));
			Assert.assertTrue("Password reset popup is not valid",
					driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_content)).isDisplayed());
			driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_returnlogin)).click();
		}
		driver.switchTo().window(parentHandle);
		log.info("User resets password successfully");
	}

	@Then("^user enter valid FS number and validates retrieve password$")
	public void user_enter_valid_fs_number_and_validates_retrieve_password() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/retrieve-password"));
		Assert.assertTrue("User is not navigated to retrieve password page",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/retrieve-password"));
		log.info("User navigated to reset password page");
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).click();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_FSNumber)).sendKeys("1000216091");
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(PassengerinfoPage.RetrievePassword_sendbtn)).click();
		log.info("User enters valid FS email address and clicks send button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Password_Reset_popup_content)));
			driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_outsidefade)).click();
			Assert.assertTrue("Password reset popup is not valid",
					driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_content)).isDisplayed());
			log.info("Password reset popup not closed even after clicks outside popup");
			driver.findElement(By.xpath(PassengerinfoPage.Password_Reset_popup_close)).click();
		}
		driver.switchTo().window(parentHandle);
		log.info("User resets password successfully");
	}

	@Then("^user clicks 9fc continue then switchto popup and continue$")
	public void user_clicks_9fc_continue_then_switchto_popup_and_continue() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);

		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.flights_9dfcContinue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^User Validates the Contact info in the reservation summary page$")
	public void user_validates_the_contact_info_in_the_reservation_summary_page() throws Throwable {
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(OnewaytripPage.Contact_Customer_Name_Reservation_Summary)));
		Assert.assertTrue("User could not validate the Customer Contact Info on the reservation summary page",
				driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Name_Reservation_Summary)).getText()
						.equals("Mr. jack flyer"));
		driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Email_Reservation_Summary)).getText()
				.equals("jack.flyer9dfc@spirit.com");
		driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Phone_Number_Reservation_Summary)).getText()
				.equals("1230000000");
		log.info("User succesfully validated the Customer Info on the reservation summary page");
	}

	@Then("^click the EDIT link next to the contact info$")
	public void click_the_edit_link_next_to_the_contact_info() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Contact_Info_EDIT_Link)).click();
	}

	@When("^User lands on my trips Passenger info Page$")
	public void user_lands_on_my_trips_passenger_info_page() throws Throwable {
		Assert.assertTrue("User could not validate The Reservation Summary Page", driver
				.findElement(By.xpath(OnewaytripPage.Your_Trip_Summary_Header)).getText().equals(" Your Trip Summary"));
		log.info("User succesfully validated The Reservation Summary Page");
	}

	@Then("^User Validates all of the Contact Information Fields to make sure they are all correct$")
	public void user_validates_all_of_the_contact_information_fields_to_make_sure_they_are_all_correct()
			throws Throwable {
		Assert.assertTrue("User could not validate The First Name Field",
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).getAttribute("value").equals("jack"));
		log.info("User succesfully validated The First name Field");

		Assert.assertTrue("User could not validate The Last Name Field",
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).getAttribute("value").equals("flyer"));
		log.info("User succesfully validated The Last name Field");

		Assert.assertTrue("User could not validate The Address Field", driver
				.findElement(By.xpath(OnewaytripPage.Address_field)).getAttribute("value").equals("603 west side"));
		log.info("User succesfully validated The Address Field");

		Assert.assertTrue("User could not validate The City Field",
				driver.findElement(By.xpath(OnewaytripPage.City_field)).getAttribute("value").equals("miami"));
		log.info("User succesfully validated The City Field");

		Assert.assertTrue("User could not validate The State Field",
				driver.findElement(By.xpath(OnewaytripPage.State_field)).getAttribute("value").equals("Florida"));
		log.info("User succesfully validated The State Field");

		Assert.assertTrue("User could not validate The Zip Code Field",
				driver.findElement(By.xpath(OnewaytripPage.ZipCode_field)).getAttribute("value").equals("33456"));
		log.info("User succesfully validated The Zip Code Field");

		Assert.assertTrue("User could not validate The Country Field",
				driver.findElement(By.xpath(OnewaytripPage.Country_field)).getAttribute("value")
						.equals("United States of America"));
		log.info("User succesfully validated The Country Field");

		Assert.assertTrue("User could not validate The Email Field",
				driver.findElement(By.xpath(OnewaytripPage.Email_field)).getAttribute("value")
						.equals("jack.flyer9dfc@spirit.com"));
		log.info("User succesfully validated The Email Field");

		Assert.assertTrue("User could not validate The Confirm Email Field",
				driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).getAttribute("value")
						.equals("jack.flyer9dfc@spirit.com"));
		log.info("User succesfully validated The Confirm Email Field");

		Assert.assertTrue("User could not validate The Phone Country Code Field",
				driver.findElement(By.xpath(OnewaytripPage.PhoneCountryCode_field)).getAttribute("value")
						.equals("United States +1"));
		log.info("User succesfully validated The Phone Country Code Field");

		Assert.assertTrue("User could not validate The Phone Field",
				driver.findElement(By.xpath(OnewaytripPage.Phone_field)).getAttribute("value").equals("123-000-0000"));
		log.info("User succesfully validated The Phone Field");
	}

	@Then("^User Clicks the Cancel Changes button and is redirected back to the Reservation Summary page$")
	public void user_clicks_the_cancel_changes_button_and_is_redirected_back_to_the_reservation_summary_page()
			throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Cancel_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Cancel_Changes_Button)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Cancel Changes Button does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/my-trips/reservation-summary"));
	}

	@Then("^User Changes the contact information fields$")
	public void user_changes_the_contact_information_fields() throws Throwable {

		driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).sendKeys("Alfonso");
		log.info("user able to change the first name");

		driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("Canales");
		log.info("user able to change the last name");

		driver.findElement(By.xpath(OnewaytripPage.Address_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.Address_field)).sendKeys("4520 sw 42 terrace");
		log.info("user able to change the address");

		driver.findElement(By.xpath(OnewaytripPage.City_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.City_field)).sendKeys("Dania Beach");
		log.info("user successfully changed the city");

		Select s2 = new Select(driver.findElement(By.xpath(OnewaytripPage.State_field)));
		s2.selectByVisibleText("Georgia");
		log.info("user able to select state");

		driver.findElement(By.xpath(OnewaytripPage.ZipCode_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.ZipCode_field)).sendKeys("33314");
		log.info("user able to change the zip code");

		Select s3 = new Select(driver.findElement(By.xpath(OnewaytripPage.Country_field)));
		s3.selectByVisibleText("Afghanistan");
		log.info("user able to select the Country");

		driver.findElement(By.xpath(OnewaytripPage.Email_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.Email_field)).sendKeys("canales@spirit.com");
		log.info("user able to change the email");

		driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).sendKeys("canales@spirit.com");
		log.info("user able to change the confirmation email");

		Select s4 = new Select(driver.findElement(By.xpath(OnewaytripPage.PhoneCountryCode_field)));
		s4.selectByVisibleText("Afghanistan +93");
		log.info("user able to select the Phone Country Code");

		driver.findElement(By.xpath(OnewaytripPage.Phone_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.Phone_field)).sendKeys("1231230001");
		log.info("user able to change the phone number");

	}

	@Then("^User Clicks the Save Changes button and is redirected back to the Reservation Summary page$")
	public void user_clicks_the_save_changes_button_and_is_redirected_back_to_the_reservation_summary_page()
			throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Save Changes Button does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/my-trips/reservation-summary"));
	}

	@Then("^User Validates the Contact info was changed in the reservation summary page$")
	public void user_validates_the_contact_info_was_changed_in_the_reservation_summary_page() throws Throwable {
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(OnewaytripPage.Contact_Customer_Name_Reservation_Summary)));
		Assert.assertTrue("User could not validate the Customer Contact Info on the reservation summary page",
				driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Name_Reservation_Summary)).getText()
						.equals("Mr. Alfonso Canales"));
		driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Email_Reservation_Summary)).getText()
				.equals("canales@spirit.com");
		driver.findElement(By.xpath(OnewaytripPage.Contact_Customer_Phone_Number_Reservation_Summary)).getText()
				.equals("1231230001");
		log.info("User succesfully validated the Customer Info on the reservation summary page");
	}

	@Then("^User Erases the first name and attempts to save changes and is promtped by a required field$")
	public void user_erases_the_first_name_and_attempts_to_save_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The First Name is required  Field", driver
				.findElement(By.xpath(OnewaytripPage.First_Name_Required)).getText().equals("First Name is required"));
		log.info("User succesfully validated the First Name is required  Field");
		driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).sendKeys("jack");
		log.info("user able to change the first name");
	}

	@Then("^User Erases the last name and attempts to save the changes and is promtped by a required field$")
	public void user_erases_the_last_name_and_attempts_to_save_the_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The Last Name is required  Field", driver
				.findElement(By.xpath(OnewaytripPage.Last_Name_Required)).getText().equals("Last Name is required"));
		log.info("User succesfully validated the Last Name is required  Field");
		driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("Flyer");
		log.info("user able to change the last name");
	}

	@Then("^User Erases the Address and attempts to save the changes and is promtped by a required field$")
	public void user_erases_the_address_and_attempts_to_save_the_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Address_field)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The Address is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.Address_Required)).getText().equals("Address is required"));
		log.info("User succesfully validated the Address is required  Field");
		driver.findElement(By.xpath(OnewaytripPage.Address_field)).sendKeys("4520 sw 42 terrace");
		log.info("user able to change the address");
	}

	@Then("^User Erases the City and attempts to save the changes and is promtped by a required field$")
	public void user_erases_the_city_and_attempts_to_save_the_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.City_field)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The City is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.City_Required)).getText().equals("City is required"));
		log.info("User succesfully validated the City is required  Field");
		driver.findElement(By.xpath(OnewaytripPage.City_field)).sendKeys("Dania Beach");
		log.info("user successfully changed the city");
	}

	@Then("^User Erases the State and Zip Code and attempts to save the changes and is promtped by a required field$")
	public void user_erases_the_state_and_zip_code_and_attempts_to_save_the_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		Select Country1 = new Select(driver.findElement(By.xpath(OnewaytripPage.Country_field)));
		Country1.selectByVisibleText("Afghanistan");
		log.info("user able to select the Country");
		Select Country2 = new Select(driver.findElement(By.xpath(OnewaytripPage.Country_field)));
		Country2.selectByVisibleText("United States of America");
		log.info("user able to select the Country");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The State is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.State_Required)).getText().equals("State is required"));
		log.info("User succesfully validated the State is required  Field");
		Assert.assertTrue("User could not validate The Zip / Postal Code is required  is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.Zip_Code_Required)).getText()
						.equals("Zip / Postal Code is required"));
		log.info("User succesfully validated the Zip / Postal Code is required  is required  Field");
		driver.findElement(By.xpath(OnewaytripPage.ZipCode_field)).sendKeys("33314");
		Select Country3 = new Select(driver.findElement(By.xpath(OnewaytripPage.State_field)));
		Country3.selectByVisibleText("Florida");
		log.info("user able to select state");
		log.info("user able to change the zip code");
	}

	@Then("^User Erases the Email and Email Confirmation and attempts to save the changes and is promtped by a required field$")
	public void user_erases_the_email_and_email_confirmation_and_attempts_to_save_the_changes_and_is_promtped_by_a_required_field()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Email_field)).clear();
		driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).clear();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The Email is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.Email_Required)).getText().equals("Email is required"));
		log.info("User succesfully validated the Confirm Email is required is required  Field");
		Assert.assertTrue("User could not validate The Email is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.Email_Confirmation_Code_Required)).getText()
						.equals("Confirm Email is required"));
		log.info("User succesfully validated the Confirm Email is required  Field");
	}

	@Then("^User Enters an Invalid Email and invalid Email Confirmation and clicks the save button$")
	public void user_enters_an_invalid_email_and_invalid_email_confirmation_and_clicks_the_save_button()
			throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Email_field)).sendKeys("Spirit");
		driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).sendKeys("Spirit");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Save_Changes_Button)));
		driver.findElement(By.xpath(OnewaytripPage.Save_Changes_Button)).click();
		Assert.assertTrue("User could not validate The Email is required  Field",
				driver.findElement(By.xpath(OnewaytripPage.Email_Required)).getText().equals("Email is required"));
		Assert.assertTrue("User could not validate email address is invalid Field",
				driver.findElement(By.xpath(OnewaytripPage.Confirm_Email_email_address_is_invalid)).getText()
						.equals("email address is invalid "));
	}

	@Then("^User clicks on the New Bag prices and Optional services link$")
	public void user_clicks_on_the_new_bag_prices_and_optional_services_link() throws Throwable {
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.New_Bag_Prices_And_Optional_Services)));
		driver.findElement(By.xpath(OnewaytripPage.New_Bag_Prices_And_Optional_Services)).click();
		Thread.sleep(2000);
		String homeurl = driver.getCurrentUrl();
		log.info("Validated Customer Support Center page url is " + homeurl);
		Assert.assertTrue("New Bag prices and Optional services Link does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/optional-services"));
	}

	@When("^User Lands on the Optional Services Page$")
	public void user_lands_on_the_optional_services_page() throws Throwable {
		log.info(driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Bags_Link)).getText());
		Assert.assertTrue("User could not validate The Bags Link", driver
				.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Bags_Link)).getText().equals("BAGS"));
		log.info("User succesfully validated the Bags link");
		log.info(driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Seats_Link)).getText());
		Assert.assertTrue("User could not validate The Seats Link", driver
				.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Seats_Link)).getText().equals("SEATS"));
		log.info("User succesfully validated the Seats link");
		log.info(driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Memberships_Link)).getText());
		Assert.assertTrue("User could not validate The Memberships Link",
				driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Memberships_Link)).getText()
						.equals("MEMBERSHIPS"));
		log.info("User succesfully validated the Memberships link");
		log.info(driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Other_Link)).getText());
		Assert.assertTrue("User could not validate The Other Link", driver
				.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Other_Link)).getText().equals("OTHER"));
		log.info("User succesfully validated the Other link");
	}

	@Then("^User Clicks on the Seats Link next to bags memberships and other$")
	public void user_clicks_on_the_seats_link_next_to_bags_memberships_and_other() throws Throwable {
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Options_and_Extras_Page_Seats_Link)));
		driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Seats_Link)).click();
	}

	@Then("^User Validates the Verbiage Under the Seats Tab$")
	public void user_validates_the_verbiage_under_the_seats_tab() throws Throwable {
		Assert.assertTrue("User could not validate email address is invalid Field",
				driver.findElement(By.xpath(OnewaytripPage.Verbiage_Under_Seats_Tab)).getText().equals(
						"Every customer has the opportunity to select their favorite spot on the plane. Maybe you want a sweet window seat or an aisle up front - go for it. If you don't select a seat we'll assign random seats at check-in for free, but we can't guarantee that you will get to sit with your friends or family. And for those who want some space to stretch out, grab a Big Front Seat: wider seat, extra legroom, and an even more comfortable flight."));
	}

	@Then("^User Clicks on the Memberships Link next to bags seats and other$")
	public void user_clicks_on_the_memberships_link_next_to_bags_seats_and_other() throws Throwable {
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(OnewaytripPage.Options_and_Extras_Page_Memberships_Link)));
		driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Memberships_Link)).click();
	}

	@Then("^User clicks on the Learn more buttons towards the bottom of the page$")
	public void user_clicks_on_the_learn_more_buttons_towards_the_bottom_of_the_page() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Learn_More_Link)));
		driver.findElement(By.xpath(OnewaytripPage.Learn_More_Link)).click();
	}

	@When("^User lands on the Spirit101 page$")
	public void user_lands_on_the_spirit101_page() throws Throwable {
		Assert.assertTrue("User could not validate The Spirit 101 Page",
				driver.findElement(By.xpath(OnewaytripPage.Spirit101_Page_Header)).getText()
						.equals("Spirit gives you More Go for less money"));
		log.info("User Validated The Spirit 101 Page Sucessfully");
	}

	@Then("^User validates the Spirit Gives You More Go for less money$")
	public void user_validates_the_spirit_gives_you_more_go_for_less_money() throws Throwable {
		Assert.assertTrue("User could not validate The Spirit 101 Page",
				driver.findElement(By.xpath(OnewaytripPage.Spirit101_Page_Header)).getText()
						.equals("Spirit gives you More Go for less money"));
		log.info("User Validated The Spirit 101 Page Sucessfully");
	}

	@Then("^User Validates the See how your bag sizes up link$")
	public void user_validates_the_see_how_your_bag_sizes_up_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SEE_HOW_YOUR_BAG_SIZES_UP)));
		driver.findElement(By.xpath(OnewaytripPage.SEE_HOW_YOUR_BAG_SIZES_UP)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SEE HOW YOUR BAG SIZES UP LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.spirit.com/OptionalServices"));
		driver.navigate().back();
	}

	@Then("^User validates the Check Out These Prices Link$")
	public void user_validates_the_check_out_these_prices_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CHECK_OUT_THESE_PRICES)));
		driver.findElement(By.xpath(OnewaytripPage.CHECK_OUT_THESE_PRICES)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("CHECK OUT THESE PRICES LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.spirit.com/OptionalServices"));
		driver.navigate().back();
	}

	@Then("^User Validates the See printing Details$")
	public void user_validates_the_see_printing_details() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SEE_PRINTING_DETAILS)));
		driver.findElement(By.xpath(OnewaytripPage.SEE_PRINTING_DETAILS)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SEE PRINTING DETAILS LINK does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/articles/202098006-How-can-I-check-in-andget-my-boarding-pass"));
		driver.navigate().back();
	}

	@Then("^User validates the Get Check In APP$")
	public void user_validates_the_get_check_in_app() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.GET_CHECKIN_APP)));
		driver.findElement(By.xpath(OnewaytripPage.GET_CHECKIN_APP)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("GET CHECK IN APP LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("http://spirit-mobile-app-landing-page.40d.io/"));
		driver.navigate().back();
	}

	@Then("^Validate the Get More Go In Your Inbox Header$")
	public void validate_the_get_more_go_in_your_inbox_header() throws Throwable {
		Assert.assertTrue("User could not validate The Get More Go in your inbox",
				driver.findElement(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Header)).getText()
						.equals("Get More Go in your inbox"));
		log.info("User Validated The Get More Go in your inbox Sucessfully");
	}

	@Then("^Validate the Get More Go In Your Inbox Header Icon email and social media Links$")
	public void validate_the_get_more_go_in_your_inbox_header_icon_email_and_social_media_links() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Email)));
		driver.findElement(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Email)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Email LINK does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://www.spirit.com/EMailNotifySignIn.aspx?_ga=2.165552635.53600564.1536240380-987277381.1536240380"));
		driver.navigate().back();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Facebook)));
		driver.findElement(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Facebook)).click();
		String homeurl1 = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("FACEBOOK LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.facebook.com/SpiritAirlines/"));
		driver.navigate().back();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Twitter)));
		driver.findElement(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Twitter)).click();
		String homeurl2 = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("TWITTER LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://twitter.com/spiritairlines"));
		driver.navigate().back();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Instagram)));
		driver.findElement(By.xpath(OnewaytripPage.Get_More_Go_In_Your_Inbox_Instagram)).click();
		String homeurl3 = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("INSTAGRAM LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.instagram.com/spiritairlines/"));
		driver.navigate().back();
	}

	@Then("^Validate the BECOME A FREE SPIRIT MEMBER Link$")
	public void validate_the_become_a_free_spirit_member_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.BECOME_A_FREE_SPIRIT_MEMBER)));
		driver.findElement(By.xpath(OnewaytripPage.BECOME_A_FREE_SPIRIT_MEMBER)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("BECOME A FREE SPIRIT MEMBER LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/FreeSpiritEnrollment.aspx"));
		driver.navigate().back();
	}

	@Then("^Validate The GET MORE GO link$")
	public void validate_the_get_more_go_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.GET_MORE_GO)));
		driver.findElement(By.xpath(OnewaytripPage.GET_MORE_GO)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("GET MORE GO LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/StaticFareClubEnrollment.aspx"));
		driver.navigate().back();
	}

	@Then("^Validate the SEE FREEQUENTLY ASKED QUESTIONS$")
	public void validate_the_see_freequently_asked_questions() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SEE_FREQUENTLY_ASKED_QUESTIONS)));
		driver.findElement(By.xpath(OnewaytripPage.SEE_FREQUENTLY_ASKED_QUESTIONS)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SEE FREQUENTLY ASKED QUESTIONS LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/spirit-101"));
		driver.navigate().back();
	}

	@Then("^Validate the SEE OUR LATEST DEALS$")
	public void validate_the_see_our_latest_deals() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SEE_OUR_LATEST_DEALS)));
		driver.findElement(By.xpath(OnewaytripPage.SEE_OUR_LATEST_DEALS)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SEE OUR LATEST DEALS LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/en/flights"));
		driver.navigate().back();
	}

	@Then("^Validate the SHARE YOUR MORE GO STORY$")
	public void validate_the_share_your_more_go_story() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SHARE_YOUR_MORE_GO_STORY)));
		driver.findElement(By.xpath(OnewaytripPage.SHARE_YOUR_MORE_GO_STORY)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SHARE YOUR MORE GO STORY LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.tumblr.com/login_required/spiritairlines"));
		driver.navigate().back();
	}

	@Then("^validate SEE WHERE WE FLY$")
	public void validate_see_where_we_fly() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.SEE_WHERE_WE_FLY)));
		driver.findElement(By.xpath(OnewaytripPage.SEE_WHERE_WE_FLY)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("SEE WHERE LINK does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/RouteMaps.aspx"));
		driver.navigate().back();
	}

	@Then("^User clicks on the Other Tab Link$")
	public void user_clicks_on_the_other_tab_link() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Options_and_Extras_Page_Other_Link)));
		driver.findElement(By.xpath(OnewaytripPage.Options_and_Extras_Page_Other_Link)).click();
	}

	@Then("^User Validates the Verbiage Under The TAB MENU When The OTHER Tab Link Is Clicked$")
	public void user_validates_the_verbiage_under_the_tab_menu_when_the_other_tab_link_is_clicked() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Verbiage_under_Other_Tab)));
		Assert.assertTrue("User could not validate The Verbiage under the Other Tab",
				driver.findElement(By.xpath(OnewaytripPage.Verbiage_under_Other_Tab)).getText().equals(
						"We have plenty of additional extras and services listed below - snacks and drinks, too. All of it costs something, because the Bare Fare is unbundled. You never pay for someone else’s options. Even if you purchase a few extras, it’s likely you’ll fly with us for less than any other airline."));
		log.info("User Validated The Verbiage under the Other Tab Sucessfully");

		Assert.assertTrue("User could not validate Boarding pass printed at home",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_home)).getText()
						.equals("Boarding pass printed at home"));
		log.info("User Validated The Verbiage under the Other Tab Sucessfully");

		Assert.assertTrue("User could not validate Free",
				driver.findElement(By.xpath(OnewaytripPage.Free)).getText().equals("Free"));
		log.info("User Validated Free");

		Assert.assertTrue("User could not validate Boarding pass printed by Airport Kiosk",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_by_Airport_Kiosk)).getText()
						.equals("Boarding pass printed by Airport Kiosk"));
		log.info("User Validated Boarding pass printed by Airport Kiosk");

		Assert.assertTrue("User could not validate $2 per boarding pass printed",
				driver.findElement(By.xpath(OnewaytripPage.$2_per_boarding_pass_printed)).getText()
						.equals("$2 per boarding pass printed"));
		log.info("User Validated T$2 per boarding pass printed");

		Assert.assertTrue("User could not validate Boarding pass printed at Airport Agent",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_Airport_Agent)).getText()
						.equals("Boarding pass printed at Airport Agent"));
		log.info("User Validated Boarding pass printed at Airport Agent");

		Assert.assertTrue("User could not validate $10 per boarding pass printed",
				driver.findElement(By.xpath(OnewaytripPage.$10_per_boarding_pass_printed)).getText()
						.equals("$10 per boarding pass printed"));
		log.info("User Validated $10 per boarding pass printed");

		Assert.assertTrue("User could not validate Unaccompanied Minors (Price includes snack and beverage)",
				driver.findElement(By.xpath(OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage))
						.getText().equals("Unaccompanied Minors (Price includes snack and beverage)"));
		log.info("User Validated Unaccompanied Minors (Price includes snack and beverage)");

		Assert.assertTrue("User could not validate $100 per customer, each way)",
				driver.findElement(By.xpath(OnewaytripPage.$100_per_customer_each_way)).getText()
						.equals("$100 per customer, each way"));
		log.info("User Validated $100 per customer, each way");

		Assert.assertTrue("User could not validate Infant (Lap Child",
				driver.findElement(By.xpath(OnewaytripPage.Infant_Lap_Child)).getText().equals("Infant (Lap Child)"));
		log.info("User Validated Infant (Lap Child");

		Assert.assertTrue("User could not validate Free (taxes may apply for certain countries)",
				driver.findElement(By.xpath(OnewaytripPage.Free_taxes_may_apply_for_certain_countries)).getText()
						.equals("Free (taxes may apply for certain countries)"));
		log.info("User Validated Free (taxes may apply for certain countries)");

		Assert.assertTrue("User could not validate Pet Transportation (Limit 4 pets total in cabin)",
				driver.findElement(By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin)).getText()
						.equals("Pet Transportation (Limit 4 pets total in cabin)"));
		log.info("User Validated Pet Transportation (Limit 4 pets total in cabin)");

		Assert.assertTrue("User could not validate $110 per pet container, each way",
				driver.findElement(By.xpath(OnewaytripPage.$110_per_pet_container_each_way)).getText()
						.equals("$110 per pet container, each way"));
		log.info("User Validated $110 per pet container, each way");

		Assert.assertTrue("User could not validate Travel Guard Insurance / Domestic Itinerary",
				driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary)).getText()
						.equals("Travel Guard Insurance / Domestic Itinerary"));
		log.info("User Validated Travel Guard Insurance / Domestic Itinerary");

		Assert.assertTrue("User could not validate From $14",
				driver.findElement(By.xpath(OnewaytripPage.From_$14)).getText().equals("From $14"));
		log.info("User Validated From $14");

		Assert.assertTrue("User could not validate Travel Guard Insurance / International Itinerary",
				driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary)).getText()
						.equals("Travel Guard Insurance / International Itinerary"));
		log.info("User Validated Travel Guard Insurance / International Itinerary");

		Assert.assertTrue("User could not validate From $25",
				driver.findElement(By.xpath(OnewaytripPage.From_$25)).getText().equals("From $25"));
		log.info("User Validated From $25");

		Assert.assertTrue("User could not validate Travel Guard Insurance / Vacation Package Itinerary",
				driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary)).getText()
						.equals("Travel Guard Insurance / Vacation Package Itinerary"));
		log.info("User Validated Travel Guard Insurance / Vacation Package Itinerary");

		Assert.assertTrue("User could not validate From $28')]",
				driver.findElement(By.xpath(OnewaytripPage.From_$28)).getText().equals("From $28"));
		log.info("User Validated From $28')]");
	}

	@Then("^User Clicks on the Onboard Snacks and Drinks Carrot and Validates all of the verbiage$")
	public void user_clicks_on_the_onboard_snacks_and_drinks_carrot_and_validates_all_of_the_verbiage()
			throws Throwable {
		Assert.assertTrue("User could not validate Onboard Snacks and Drinks",
				driver.findElement(By.xpath(OnewaytripPage.Onboard_Snacks_and_Drinks)).getText()
						.equals("Onboard Snacks and Drinks"));
		log.info("User Validated Onboard Snacks and Drinks");

		driver.findElement(By.xpath(OnewaytripPage.Onboard_Snacks_and_Drinks_Carrot)).click();

		Assert.assertTrue("User could not validate Snacks",
				driver.findElement(By.xpath(OnewaytripPage.Snacks)).getText().equals("Snacks"));
		log.info("User Validated Snacks");

		Assert.assertTrue("User could not validate $1 to $10",
				driver.findElement(By.xpath(OnewaytripPage.$1_to_$10)).getText().equals("$1 to $10"));
		log.info("User Validated $1 to $10");

		Assert.assertTrue("User could not validate Drinks",
				driver.findElement(By.xpath(OnewaytripPage.Drinks)).getText().equals("Drinks"));
		log.info("User Validated Drinks");

		Assert.assertTrue("User could not validate $1 to $15",
				driver.findElement(By.xpath(OnewaytripPage.$1_to_$15)).getText().equals("$1 to $15"));
		log.info("User Validated $1 to $15");

	}

	@Then("^User Clicks on the Booking Related Carrot and Validates all of the verbiage$")
	public void user_clicks_on_the_booking_related_carrot_and_validates_all_of_the_verbiage() throws Throwable {
		Assert.assertTrue("User could not validate Booking Related",
				driver.findElement(By.xpath(OnewaytripPage.Booking_Related)).getText().equals("Booking Related"));
		log.info("User Validated Booking Related ");

		driver.findElement(By.xpath(OnewaytripPage.Booking_Related_Carrot)).click();

		Assert.assertTrue("User could not validate Per Customer ",
				driver.findElement(By.xpath(OnewaytripPage.Per_Customer)).getText().equals("Per Customer"));
		log.info("User Validated Per Customer ");

		Assert.assertTrue("User could not validate Reservation Center Booking (including packages) ",
				driver.findElement(By.xpath(OnewaytripPage.Reservation_Center_Booking_including_packages)).getText()
						.equals("Reservation Center Booking (including packages)"));
		log.info("User Validated Reservation Center Booking (including packages) ");

		Assert.assertTrue("User could not validate $35 per booking",
				driver.findElement(By.xpath(OnewaytripPage.$35_per_booking)).getText().equals("$35 per booking"));
		log.info("User Validated $35 per booking");

		;
		Assert.assertTrue("User could not validate Group Booking",
				driver.findElement(By.xpath(OnewaytripPage.Group_Booking)).getText().equals("Group Booking"));
		log.info("User Validated Group Booking");

		Assert.assertTrue("User could not validate $5 per booking ",
				driver.findElement(By.xpath(OnewaytripPage.$5_per_booking)).getText().equals("$5 per booking"));
		log.info("User Validated $5 per booking ");

		Assert.assertTrue("User could not validate Colombia Administrative Charge ",
				driver.findElement(By.xpath(OnewaytripPage.Colombia_Administrative_Charge)).getText()
						.equals("Colombia Administrative Charge"));
		log.info("User Validated Colombia Administrative Charge");

		Assert.assertTrue("User could not validate $15 to $95 per booking ",
				driver.findElement(By.xpath(OnewaytripPage.$15_to_$95_per_booking)).getText()
						.equals("$15 to $95 per booking"));
		log.info("User Validated $15 to $95 per booking ");

		Assert.assertTrue("User could not validate Standby for Earlier Flight ",
				driver.findElement(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight)).getText()
						.equals("Standby for Earlier Flight"));
		log.info("User Validated Standby for Earlier Flight ");

		Assert.assertTrue("User could not validate $99 each way",
				driver.findElement(By.xpath(OnewaytripPage.$99_each_way)).getText().equals("$99 each way"));
		log.info("User Validated $99 each way");

		Assert.assertTrue("User could not validate Passenger Usage Charge",
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Usage_Charge)).getText()
						.equals("Passenger Usage Charge"));
		log.info("User Validated Passenger Usage Charge");

		Assert.assertTrue("User could not validate $8.99 to $19.99 per segment",
				driver.findElement(By.xpath(OnewaytripPage.$899_to_$1999_per_segment)).getText()
						.equals("$8.99 to $19.99 per segment"));
		log.info("User Validated $8.99 to $19.99 per segment");

		Assert.assertTrue("User could not validate Regulatory Compliance Charge",
				driver.findElement(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge)).getText()
						.equals("Regulatory Compliance Charge"));
		log.info("User Validated Regulatory Compliance Charge");

		Assert.assertTrue("User could not validate $7 per segment",
				driver.findElement(By.xpath(OnewaytripPage.$7_per_segment)).getText().equals("$7 per segment"));
		log.info("User Validated $7 per segment");

		Assert.assertTrue("User could not validate Fuel Charge",
				driver.findElement(By.xpath(OnewaytripPage.Fuel_Charge)).getText().equals("Fuel Charge"));
		log.info("User Validated Fuel Charge");

		Assert.assertTrue("User could not validate $10 per segment",
				driver.findElement(By.xpath(OnewaytripPage.$10_per_segment)).getText().equals("$10 per segment"));
		log.info("User Validated $10 per segment");
	}

	@Then("^User Clicks on the FREE SPIRIT AWARD BOOKING Carrot and Validates all of the verbiage$")
	public void user_clicks_on_the_free_spirit_award_booking_carrot_and_validates_all_of_the_verbiage()
			throws Throwable {
		Assert.assertTrue("User could not validate FREE SPIRIT® Award Booking",
				driver.findElement(By.xpath(OnewaytripPage.FREE_SPIRIT_Award_Booking)).getText()
						.equals("FREE SPIRIT® Award Booking"));
		log.info("User Validated FREE SPIRIT® Award Booking");

		driver.findElement(By.xpath(OnewaytripPage.FREE_SPIRIT_Award_Booking_Carrot)).click();

		Assert.assertTrue("User could not validate Per Customer, Per Booking",
				driver.findElement(By.xpath(OnewaytripPage.Per_Customer_Per_Booking)).getText()
						.equals("Per Customer, Per Booking"));
		log.info("User Validated Per Customer, Per Booking");

		Assert.assertTrue("User could not validate Agent Transaction - Reservation Center",
				driver.findElement(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center)).getText()
						.equals("Agent Transaction - Reservation Center"));
		log.info("User Validated Agent Transaction - Reservation Center");

		Assert.assertTrue("User could not validate $35",
				driver.findElement(By.xpath(OnewaytripPage.$35)).getText().equals("$35"));
		log.info("User Validated $35");

		;
		Assert.assertTrue("User could not validate Award Redemption",
				driver.findElement(By.xpath(OnewaytripPage.Award_Redemption)).getText().equals("Award Redemption"));
		log.info("User Validated Award Redemption");

		Assert.assertTrue("User could not validate Free to $100",
				driver.findElement(By.xpath(OnewaytripPage.Free_to_$100)).getText().equals("Free to $100"));
		log.info("User Validated Free to $100");

		Assert.assertTrue("User could not validate Award Modification",
				driver.findElement(By.xpath(OnewaytripPage.Award_Modification)).getText().equals("Award Modification"));
		log.info("User Validated Award Modification");

		Assert.assertTrue("User could not validate $15 to $95 per booking ",
				driver.findElement(By.xpath(OnewaytripPage.$15_to_$95_per_booking)).getText()
						.equals("$15 to $95 per booking"));
		log.info("User Validated $15 to $95 per booking ");

		Assert.assertTrue("User could not validate $110",
				driver.findElement(By.xpath(OnewaytripPage.Award_$110)).getText().equals("$110"));
		log.info("User Validated $110");

		Assert.assertTrue("User could not validate Mileage Redeposit",
				driver.findElement(By.xpath(OnewaytripPage.Mileage_Redeposit)).getText().equals("Mileage Redeposit"));
		log.info("User Validated Mileage Redeposit");

		Assert.assertTrue("User could not validate $110",
				driver.findElement(By.xpath(OnewaytripPage.Mileage_$110)).getText().equals("$110"));
		log.info("User Validated $110");
	}

	@Then("^User Clicks on the Modification Or Cancellation Carrot and Validates all of the verbiage$")
	public void user_clicks_on_the_modification_or_cancellation_carrot_and_validates_all_of_the_verbiage()
			throws Throwable {
		Assert.assertTrue("User could not validate Modification Or Cancellation",
				driver.findElement(By.xpath(OnewaytripPage.Modification_Or_Cancellation)).getText()
						.equals("Modification Or Cancellation"));
		log.info("User Validated Modification Or Cancellation");

		driver.findElement(By.xpath(OnewaytripPage.Modification_Or_Cancellation_Carrot)).click();

		Assert.assertTrue("User could not validate Per Customer, Per Booking",
				driver.findElement(By.xpath(OnewaytripPage.Modification_Per_Customer_Per_Booking)).getText()
						.equals("Per Customer, Per Booking"));
		log.info("User Validated Per Customer, Per Booking");

		Assert.assertTrue("User could not validate Web Modification or Cancellation",
				driver.findElement(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation)).getText()
						.equals("Web Modification or Cancellation"));
		log.info("User Validated Web Modification or Cancellation");

		Assert.assertTrue("User could not validate $35",
				driver.findElement(By.xpath(OnewaytripPage.$35)).getText().equals("$35"));
		log.info("User Validated $35");

		;
		Assert.assertTrue("User could not validate $90",
				driver.findElement(By.xpath(OnewaytripPage.$90)).getText().equals("$90"));
		log.info("User Validated $90");

		Assert.assertTrue("User could not validate Reservations / Airport Modification or Cancellation",
				driver.findElement(By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation)).getText()
						.equals("Reservations / Airport Modification or Cancellation"));
		log.info("User Validated Reservations / Airport Modification or Cancellation");

		Assert.assertTrue("User could not validate $100",
				driver.findElement(By.xpath(OnewaytripPage.$100)).getText().equals("$100"));
		log.info("User Validated $100");

		Assert.assertTrue("User could not validate Group Booking Itinerary Modification or Cancellation",
				driver.findElement(By.xpath(OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation))
						.getText().equals("Group Booking Itinerary Modification or Cancellation"));
		log.info("User Validated Group Booking Itinerary Modification or Cancellation");

		Assert.assertTrue("User could not validate $50",
				driver.findElement(By.xpath(OnewaytripPage.$50)).getText().equals("$50"));
		log.info("$50");
	}

	@Then("^User Clicks on the Extras Carrot and Validates all of the verbiage$")
	public void user_clicks_on_the_extras_carrot_and_validates_all_of_the_verbiage() throws Throwable {
		Assert.assertTrue("User could not validate Extras",
				driver.findElement(By.xpath(OnewaytripPage.Extras)).getText().equals("Extras"));
		log.info("User Validated Extras");

		driver.findElement(By.xpath(OnewaytripPage.Extras_Carrot)).click();

		Assert.assertTrue("User could not validate Per Customer",
				driver.findElement(By.xpath(OnewaytripPage.Extras_Per_Customer)).getText().equals("Per Customer"));
		log.info("User Validated Per Customer");

		Assert.assertTrue("User could not validate Flight Flex",
				driver.findElement(By.xpath(OnewaytripPage.Flight_Flex)).getText().equals("Flight Flex"));
		log.info("User Validated Flight Flex");

		Assert.assertTrue("User could not validate $35 to $45",
				driver.findElement(By.xpath(OnewaytripPage.$35_to_$45)).getText().equals("$35 to $45"));
		log.info("User Validated $35 to $45");

		Assert.assertTrue("User could not validate Shortcut Boarding",
				driver.findElement(By.xpath(OnewaytripPage.Shortcut_Boarding)).getText().equals("Shortcut Boarding"));
		log.info("User Validated Shortcut Boarding");

		Assert.assertTrue("User could not validate $5.99+ each way",
				driver.findElement(By.xpath(OnewaytripPage.$599_each_way)).getText().equals("$5.99+ each way"));
		log.info("User Validated $5.99+ each way");

		Assert.assertTrue("User could not validate Shortcut Security",
				driver.findElement(By.xpath(OnewaytripPage.Shortcut_Security)).getText().equals("Shortcut Security"));
		log.info("User Validated Shortcut Security");

		Assert.assertTrue("User could not validate Up to $15",
				driver.findElement(By.xpath(OnewaytripPage.Up_to_$15)).getText().equals("Up to $15"));
		log.info("User Validated Up to $15");

	}

	@Then("^User Validates the Print Your Boarding Pass at Home Tool Tip$")
	public void user_validates_the_print_your_boarding_pass_at_home_tool_tip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_home_tooltip)).click();

		Assert.assertTrue("User could not validate Boarding Pass At Home Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_home_tooltip_Popup)).getText()
						.equals("Print your boarding pass at home to save."));
		log.info("User Validated Boarding Pass At Home Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_home_tooltip_Popup_X)).click();
	}

	@Then("^User Validates the Boarding pass printed by Airport Kiosk tooltip$")
	public void user_validates_the_boarding_pass_printed_by_airport_kiosk_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_by_Airport_Kiosk_tooltip)).click();

		Assert.assertTrue("User could not validate Boarding Pass Printed by Airport Kiosk Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_by_Airport_Kiosk_tooltip_Popup))
						.getText().equals("Print your boarding pass at home to save."));
		log.info("User Validated Boarding Pass Printed by Airport Kiosk Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_by_Airport_Kiosk_tooltip_Popup_X)).click();
	}

	@Then("^User Validates the Boarding pass printed by Airport Agent tooltip$")
	public void user_validates_the_boarding_pass_printed_by_airport_agent_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_Airport_Agent_tooltip)).click();

		Assert.assertTrue("User could not validate Boarding Pass Printed at Airport Agent Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_Airport_Agent_tooltip_Popup))
						.getText().equals("Print your boarding pass at home to save."));
		log.info("User Validated Boarding Pass Printed at Airport Agent Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Boarding_pass_printed_at_Airport_Agenttooltip_Popup_X)).click();

	}

	@Then("^User Validates the Unaccompanied Minors Price includes snack and beverage tooltip$")
	public void user_validates_the_unaccompanied_minors_price_includes_snack_and_beverage_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip))
				.click();

		String Tooltip = driver
				.findElement(
						By.xpath(OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Unaccompanied Minors (Price includes snack and beverage) Tool Tip",
				driver.findElement(
						By.xpath(OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup))
						.getText().equals(Tooltip));
		log.info("User Validated Unaccompanied Minors (Price includes snack and beverage) Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup_more_info_link)));
		driver.findElement(By.xpath(
				OnewaytripPage.Unaccompanied_Minors_Price_includes_snack_and_beverage_tooltip_Popup_more_info_link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info LINK does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202096706-Does-Spirit-allow-children-to-travel-alone"));
		driver.navigate().back();
	}

	@Then("^User Validates Infant Lap Child tooltip$")
	public void user_validates_infant_lap_child_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Infant_Lap_Child_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Infant_Lap_Child_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Infant (Lap Child) Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Infant_Lap_Child_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Infant (Lap Child) Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Infant_Lap_Child_tooltip_Popup_more_info_link)));
		driver.findElement(By.xpath(OnewaytripPage.Infant_Lap_Child_tooltip_Popup_more_info_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info LINK does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202096716-Am-I-allowed-to-carry-my-infant-on-my-lap-during-flight"));
		driver.navigate().back();
	}

	@Then("^User Validates the Pet Transportation Limit 4 pets total in cabin tooltip$")
	public void user_validates_the_pet_transportation_limit_4_pets_total_in_cabin_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Pet Transportation (Limit 4 pets total in cabin) Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup))
				.getText().equals(Tooltip));
		log.info("User Validated Pet Transportation (Limit 4 pets total in cabin) Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup_more_info_link)));
		driver.findElement(
				By.xpath(OnewaytripPage.Pet_Transportation_Limit_4_pets_total_in_cabin_tooltip_Popup_more_info_link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info LINK does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202096926-Does-Spirit-Airlines-allow-pets-on-board"));
		driver.navigate().back();
	}

	@Then("^User Validates Travel Guard Insurance Domestic Itinerary tooltip$")
	public void user_validates_travel_guard_insurance_domestic_itinerary_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Travel Guard Insurance / Domestic Itinerary Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup))
						.getText().equals(Tooltip));
		log.info("User Validated Travel Guard Insurance / Domestic Itinerary Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup_Travel_Gaurd_Link)));
		driver.findElement(
				By.xpath(OnewaytripPage.Travel_Guard_Insurance_Domestic_Itinerary_tooltip_Popup_Travel_Gaurd_Link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Travel Gaurd LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.travelguard.com/spirit/"));
		driver.navigate().back();
	}

	@Then("^User Validates the Travel Guard Insurance International Itinerary tooltip$")
	public void user_validates_the_travel_guard_insurance_international_itinerary_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Travel Guard Insurance / International Itinerary Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary_tooltip_Popup))
				.getText().equals(Tooltip));
		log.info("User Validated Travel Guard Insurance / International Itinerary Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary_tooltip_Popup_Travel_Gaurd_Link)));
		driver.findElement(
				By.xpath(OnewaytripPage.Travel_Guard_Insurance_International_Itinerary_tooltip_Popup_Travel_Gaurd_Link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Travel Gaurd LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.travelguard.com/spirit/"));
		driver.navigate().back();
	}

	@Then("^User Validates Travel Guard Insurance Vacation Package Itinerary tooltip$")
	public void user_validates_travel_guard_insurance_vacation_package_itinerary_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Travel Guard Insurance / Vacation Itinerary Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup))
				.getText().equals(Tooltip));
		log.info("User Validated Travel Guard Insurance / Vacation Itinerary Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup_Travel_Gaurd)));
		driver.findElement(
				By.xpath(OnewaytripPage.Travel_Guard_Insurance_Vacation_Package_Itinerary_tooltip_Popup_Travel_Gaurd))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Travel Gaurd LINK does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.travelguard.com/spirit/"));
		driver.navigate().back();
	}

	@Then("^User Validates the Snacks tooltip$")
	public void user_validates_the_snacks_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Snacks_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Snacks_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Snacks Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Snacks_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Snacks Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Snacks_tooltip_Popup_More_Ifo_link)));
		driver.findElement(By.xpath(OnewaytripPage.Snacks_tooltip_Popup_More_Ifo_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097886-What-food-and-drinks-does-Spirit-offer-on-the-plane"));
		driver.navigate().back();
	}

	@Then("^User Validates the Drinks tooltip$")
	public void user_validates_the_drinks_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Drinks_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Drinks_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Snacks Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Drinks_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Snacks Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Drinks_tooltip_Popup_More_Info_link)));
		driver.findElement(By.xpath(OnewaytripPage.Drinks_tooltip_Popup_More_Info_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097886-What-food-and-drinks-does-Spirit-offer-on-the-plane"));
		driver.navigate().back();
	}

	@Then("^User Validates Reservation Center Booking Including All Packages tooltip$")
	public void user_validates_reservation_center_bookiing_including_all_packages_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Reservation_Center_Booking_including_packages_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Reservation_Center_Booking_including_packages_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("Reservation Center Booking (including packages) Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Reservation_Center_Booking_including_packages_tooltip_Popup))
						.getText().equals(Tooltip));
		log.info("User Validated Reservation Center Booking (including packages) Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Reservation_Center_Booking_including_packages_tooltip_Popup_X))
				.click();
	}

	@Then("^User Vlaidates Group Booking tooltip$")
	public void user_vlaidates_group_booking_tooltip() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.Group_Booking_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Group_Booking_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Group Booking Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Group_Booking_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Group Booking Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Group_Booking_tooltip_Popup_more_info_link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(OnewaytripPage.Group_Booking_tooltip_Popup_more_info_link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Information about the Download Adobe Acrobat Reader for free does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://customersupport.spirit.com/hc/en-us/articles/360002248271"));

		driver.close();
		driver.switchTo().window(parentHandle);

		driver.findElement(By.xpath(OnewaytripPage.Group_Booking_tooltip)).click();
	}

	@Then("^User Validates Colombia Administrative Charge tooltip$")
	public void user_validates_colombia_administrative_charge_tooltip() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Colombia_Administrative_Charge_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Colombia_Administrative_Charge_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Colombia_Administrative_Charge_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate the Colombia Administrative Charge Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Colombia_Administrative_Charge_tooltip_Popup)).getText()
						.equals(Tooltip));
		log.info("User Validated Colombia Administrative Charge Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Colombia_Administrative_Charge_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Stand By for Earlier Flight tooltip$")
	public void user_validates_stand_by_for_earlier_flight_tooltip() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Standby for Earlier Flight Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight_tooltip_Popup)).getText()
						.equals(Tooltip));
		log.info("User Validated Standby for Earlier Flight Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Standby_for_Earlier_Flight_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Passenger Usage Charge tooltip$")
	public void user_validates_passenger_usage_charge_tooltip() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Passenger_Usage_Charge_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Passenger_Usage_Charge_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Passenger_Usage_Charge_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Passenger Usage Charge Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Passenger_Usage_Charge_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Passenger Usage Charge Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Passenger_Usage_Charge_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Regulatory Complaiance Charge tooltip$")
	public void user_validates_regulatory_complaiance_charge_tooltip() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Regulatory Compliance Charge Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge_tooltip_Popup)).getText()
						.equals(Tooltip));
		log.info("User Validated Regulatory Compliance Charge Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Regulatory_Compliance_Charge_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Fuel Charge tooltip$")
	public void user_validates_fuel_charge_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Fuel_Charge_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Fuel_Charge_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Fuel_Charge_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Fuel Charge Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Fuel_Charge_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Fuel Charge Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Fuel_Charge_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Agent Transaction Reservation Center tooltip$")
	public void user_validates_agent_transaction_reservation_center_tooltip() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Agent Transaction - Reservation Center Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center_tooltip_Popup))
						.getText().equals(Tooltip));
		log.info("User Validated Agent Transaction - Reservation Center Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Agent_Transaction_Reservation_Center_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Award Redemption tooltip$")
	public void user_validates_award_redemption_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Award_Redemption_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Award_Redemption_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Award_Redemption_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Award Redemption Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Award_Redemption_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Award Redemption Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Award_Redemption_tooltip_Popup_more_info_link)));
		driver.findElement(By.xpath(OnewaytripPage.Award_Redemption_tooltip_Popup_more_info_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097636-What-are-the-taxes-and-fees-for-booking-award-travel-in-the-FREE-SPIRIT-frequent-flier-program"));
		driver.navigate().back();
	}

	@Then("^User Validates Award Modification tooltip$")
	public void user_validates_award_modification_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Award_Modification_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Award_Modification_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Award_Modification_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Award Modification Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Award_Modification_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Award Modification Tool Tip");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Award_Modification_tooltip_Popup_link)));
		driver.findElement(By.xpath(OnewaytripPage.Award_Modification_tooltip_Popup_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097616-Can-I-cancel-or-make-a-change-on-a-FREE-SPIRIT-award-ticket"));
		driver.navigate().back();
	}

	@Then("^User Validates Mileage Redeposit tooltip$")
	public void user_validates_mileage_redeposit_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Mileage_Redeposit_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Mileage_Redeposit_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Mileage_Redeposit_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Mileage Redeposit Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Mileage_Redeposit_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Mileage Redeposit Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Mileage_Redeposit_tooltip_Popup_X)).click();
	}

	@Then("^User Validates Web Modification or Cancellation tooltip$")
	public void user_validates_web_modification_or_cancellation_tooltip() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Web Modification or Cancellation Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip_Popup)).getText()
						.equals(Tooltip));
		log.info("User Validated Web Modification or Cancellation Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip_Popup_more_info_link)));
		driver.findElement(By.xpath(OnewaytripPage.Web_Modification_or_Cancellation_tooltip_Popup_more_info_link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097076-How-can-I-change-or-cancel-my-reservation"));
		driver.navigate().back();
	}

	@Then("^User Validates Reservations Airport Modification or Cancellation tooltip$")
	public void user_validates_reservations_airport_modification_or_cancellation_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip)).click();

		String Tooltip = driver
				.findElement(By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Reservations / Airport Modification or Cancellation Tool Tip", driver
				.findElement(By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip_Popup))
				.getText().equals(Tooltip));
		log.info("User Validated Reservations / Airport Modification or Cancellation Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip_Popup_more_info_link)));
		driver.findElement(
				By.xpath(OnewaytripPage.Reservations_Airport_Modification_or_Cancellation_tooltip_Popup_more_info_link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl().startsWith(
				"https://customersupport.spirit.com/hc/en-us/articles/202097076-How-can-I-change-or-cancel-my-reservation-"));
		driver.navigate().back();
	}

	@Then("^User Validates Group Booking Itinerary Modification or Cancellation tooltip$")
	public void user_validates_group_booking_itinerary_modification_or_cancellation_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip))
				.click();

		String Tooltip = driver
				.findElement(
						By.xpath(OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup))
				.getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User could not validate Group Booking Itinerary Modification or Cancellation Tool Tip",
				driver.findElement(
						By.xpath(OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup))
						.getText().equals(Tooltip));
		log.info("User Validated Group Booking Itinerary Modification or Cancellation Tool Tip");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup_more_info_link)));
		driver.findElement(By.xpath(
				OnewaytripPage.Group_Booking_Itinerary_Modification_or_Cancellation_tooltip_Popup_more_info_link))
				.click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("More info does not navigate to valid URL", driver.getCurrentUrl()
				.startsWith("https://customersupport.spirit.com/hc/en-us/articles/360002248271-Group-Travel"));
		driver.navigate().back();
	}

	@Then("^User Validates the Flight Flex tooltip$")
	public void user_validates_the_flight_flex_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Flight_Flex_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Flight_Flex_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Flight_Flex_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Flight Flex Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Flight_Flex_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Flight Flex Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Flight_Flex_tooltip_Popup_X)).click();
	}

	@Then("^User Validates the Shortcut Boarding tooltip$")
	public void user_validates_the_shortcut_boarding_tooltip() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Shortcut_Boarding_tooltip)));
		driver.findElement(By.xpath(OnewaytripPage.Shortcut_Boarding_tooltip)).click();

		String Tooltip = driver.findElement(By.xpath(OnewaytripPage.Shortcut_Boarding_tooltip_Popup)).getText();
		log.info("Tool tip pop up text should be " + Tooltip);

		Assert.assertTrue("User Could Not Validate Shortcut Boarding Tool Tip",
				driver.findElement(By.xpath(OnewaytripPage.Shortcut_Boarding_tooltip_Popup)).getText().equals(Tooltip));
		log.info("User Validated Shortcut Boarding Tool Tip");

		driver.findElement(By.xpath(OnewaytripPage.Shortcut_Boarding_tooltip_Popup_X)).click();
	}

	@Then("^User Clicks on the Onboard Snacks and Drinks Carrot$")
	public void user_clicks_on_the_onboard_snacks_and_drinks_carrot() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Onboard_Snacks_and_Drinks_Carrot)));
		driver.findElement(By.xpath(OnewaytripPage.Onboard_Snacks_and_Drinks_Carrot)).click();
	}

	@Then("^User Clicks on the Booking Related Carrot$")
	public void user_clicks_on_the_booking_related_carrot() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Booking_Related_Carrot)));
		driver.findElement(By.xpath(OnewaytripPage.Booking_Related_Carrot)).click();
	}

	@Then("^User Clicks on the FREE SPIRIT AWARD BOOKING Carrot$")
	public void user_clicks_on_the_free_spirit_award_booking_carrot() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.FREE_SPIRIT_Award_Booking_Carrot)));
		driver.findElement(By.xpath(OnewaytripPage.FREE_SPIRIT_Award_Booking_Carrot)).click();
	}

	@Then("^User Clicks on the Modification Or Cancellation Carrot$")
	public void user_clicks_on_the_modification_or_cancellation_carrot() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Modification_Or_Cancellation_Carrot)));
		driver.findElement(By.xpath(OnewaytripPage.Modification_Or_Cancellation_Carrot)).click();
	}

	@Then("^User Clicks on the Extras Carrot$")
	public void user_clicks_on_the_extras_carrot() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Extras_Carrot)));
		driver.findElement(By.xpath(OnewaytripPage.Extras_Carrot)).click();
	}

	@Then("^User clicks on the Thrifty Car Rental Banner and validates the URL$")
	public void user_clicks_on_the_thrifty_car_rental_banner_and_validates_the_url() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(OnewaytripPage.Confirmation_Page_Thrifty_Car_Rental_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(OnewaytripPage.Confirmation_Page_Thrifty_Car_Rental_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Thrifty Car Rental Banner does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.thrifty.com/deals/Regular/101126.aspx?iata=00261811"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Validates The Enter Title Field Is Required And Enters A Title$")
	public void user_validates_the_enter_title_field_is_required_and_enters_a_title() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title_Required)));
		Assert.assertTrue("User Can Not Validate the Title is required field",
				driver.findElement(By.xpath(OnewaytripPage.Title_Required)).getText().equals("Title is required"));
		log.info("User Successfully Validated The Title Is Required Field");

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));
		driver.findElement(By.xpath(OnewaytripPage.Title)).click();
		driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
		driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);
	}
	
	
	@Then("^user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code$")
    public void user_inputs_the_pnr_code_and_clicks_on_the_my_trips_button_and_enters_the_lastname_and_confirmation_code() throws Throwable {
	    
	         
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
	        String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
	        String pnr = tmp.replace("Confirmation Code: ", "");
	        log.info("The PNR obtained after the booking is : " + pnr);
	        Thread.sleep(5000);
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.mytrips_mainmenu)));
	        driver.findElement(By.xpath(PassengerinfoPage.mytrips_mainmenu)).click();
	        log.info("Clicked on the checkin button present on top banner");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MyTripsPage.Passengers_Lastname)));
	        driver.findElement(By.xpath(MyTripsPage.Passengers_Lastname)).sendKeys("flyer");
	        log.info("Passenger entering the last name as mentioned");
	        driver.findElement(By.xpath(MyTripsPage.confromation_code)).sendKeys(pnr);
	        log.info("Entered the confirmation code as : " + pnr);
	        try {
	            Thread.sleep(15000);
	        } catch (InterruptedException e) {
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(MyTripsPage.Search_button)));
	        driver.findElement(By.xpath(MyTripsPage.Search_button)).click();
	        log.info("Then user clicked on the check-in button to check the info of the booked ticket");
	    }
}
