package com.cucumber.stepDefinitions;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.FlightAvailability;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.RoundtripPage;
import com.cucumber.runner.TestRunner;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;

public class BookPageStep {

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 10).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	private static final Logger log = LogManager.getLogger();

	@When("^User Clicks on Book link$")
	public void User_Clcks_on_Book_link() {

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		// Assert.assertTrue("User is not navigated to home page",
		// driver.findElement(By.xpath(OnewaytripPage.Learn_more)).getText().equals("LEARN
		// MORE"));
		// log.info("user able to check the learn more button");

		log.info("!! Trying to click Book link *****");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Book_Tab)));
		// driver.findElement(By.xpath(BookPage.Book_Tab)).click();
		log.info("!! We are in book page *****");

	}

	@Then("^User choose the from city$")
	public void User_choose_the_from_city() throws Throwable {
Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
		driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.from_city)));
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.from_city)));
		driver.findElement(By.xpath(BookPage.from_city)).click();

	}

	@Then("^User choose the To city$")
	public void User_choose_the_To_city() throws Throwable {

		driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();

		// Random ran = new Random();
		// List<WebElement> cities = driver.findElements(By.xpath(BookPage.TO_city));
		//
		// int city=ran.nextInt(cities.size())+ 1;
		//
		// cities.get(city).click();
		// Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.TO_city)));
		driver.findElement(By.xpath(BookPage.TO_city)).click();
	}

	@Then("^user selects departure date and return date$")
	public void User_selects_departure_date_and_return_date() throws Throwable {
		driver.findElement(By.xpath(BookPage.Travel_departuredate_Returndate)).click();
		driver.findElement(By.xpath(BookPage.Travel_departuredate_Returndate)).clear();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		Date date1 = c.getTime();
		log.info("Departure date: " + dateFormat.format(date1));
		String DepartureDate = dateFormat.format(date1);
		c.setTime(date1);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String ReturnDate = dateFormat.format(currentDatePlusOne);

		log.info("Return date: " + dateFormat.format(currentDatePlusOne));
		driver.findElement(By.xpath(BookPage.Travel_departuredate_Returndate))
				.sendKeys(DepartureDate + " - " + ReturnDate);
	}

	 @Then("^user select the today as a departure date$")
	    public void user_select_the_today_as_a_departure_date() throws Throwable {
	       
		 Date currentDate = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			// manipulate date

			c.add(Calendar.YEAR, 0);
			c.add(Calendar.MONTH, 0);
			c.add(Calendar.DATE, 0); // same with c.add(Calendar.DAY_OF_MONTH, 1);
			c.add(Calendar.HOUR, 0);
			c.add(Calendar.MINUTE, 0);
			c.add(Calendar.SECOND, 0);

			// convert calendar to date
			Date currentDatePlusOne = c.getTime();
			String DepartureDate = dateFormat.format(currentDatePlusOne);

			log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Date)));
			driver.findElement(By.xpath(BookPage.Date)).click();

			driver.findElement(By.xpath(BookPage.Date)).clear();
			driver.findElement(By.xpath(BookPage.Date)).sendKeys(DepartureDate);
		 
	    }

	
	@Then("^User selects number of passengers$")
	public void User_selects_number_of_passengers() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		log.info("user selects the one adult passeneger");
		
	}

	@Then("^user selects \"([^\"]*)\" passengers$")
	public void user_selects_passnegers(String passenger_count) throws Throwable {
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.select_adults)));
		// driver.findElement(By.xpath(BookPage.select_adults)).clear();
		// driver.findElement(By.xpath(BookPage.select_adults)).sendKeys(passenger_count);
		for (int selectedPassenger = 2; selectedPassenger <= Integer.parseInt(passenger_count); selectedPassenger++) {
			driver.findElement(By.xpath(BookPage.select_adults)).click();
			log.info("Selecting the adult number " + selectedPassenger);
		}
	}

	@Then("^User enters promo code$")
	public void User_enters_promo_code() throws Throwable {

		driver.findElement(By.xpath(BookPage.promocode)).sendKeys("12345");
	}

	@Then("^User clicks on Search button$")
	public void User_clicks_on_Search_button() {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Search_button)));
		WebElement element =driver.findElement(By.xpath(BookPage.Search_button));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click()", element);
	log.info("user successfully clicks on the search button");
	

	}

	@Then("^user selects departures date$")
	public void User_selects_departure_date() throws Throwable {
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date

		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Date)));
		driver.findElement(By.xpath(BookPage.Date)).click();

		driver.findElement(By.xpath(BookPage.Date)).clear();
		driver.findElement(By.xpath(BookPage.Date)).sendKeys(DepartureDate);

	}

	@When("^User Clicks on checkin link$")
	public void user_clcks_on_checkin_link() throws Throwable {
		driver.findElement(By.xpath(CheckInPage.Checkin_link)).click();
		log.info("user clicks on checkin link");
	}

	@Then("^User enters the passengers last name$")
	public void user_enters_the_passengers_last_name() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.passengers_lastname)));
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("shaik");
		log.info("user enters the passengers last name");
	}

	@Then("^User enters the conformation code$")
	public void user_enters_the_conformation_code() throws Throwable {
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys("L2FG56P");
		log.info("user eners the conformation code");
	}

	@Then("^User clicks on submit button$")
	public void user_clicks_on_submit_button() throws Throwable {
		driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
		log.info("user clicks on search buton");

	}

	@When("^User Clicks on Mytrips link$")
	public void user_clicks_on_Mytrips_link() throws Throwable {
		driver.findElement(By.xpath(MyTripsPage.My_Trips)).click();
		log.info("user clicks on MyTrips link");

	}

	@Then("^User enters the passengers lastname$")
	public void user_enters_the_passengers_lastname() throws Throwable {
		driver.findElement(By.xpath(MyTripsPage.Passengers_Lastname)).sendKeys("james");
		log.info("user enters the passengers lastname");
	}

	@Then("^User enters the passenger PNR$")
	public void user_enters_the_passengers_PNR() throws Throwable {
		driver.findElement(By.xpath(MyTripsPage.confromation_code)).sendKeys("L2FG56P");
		log.info("user enters the passengers PNR");
	}

	@Then("^User clicks on submitbutton$")
	public void User_clicks_on_submitbutton() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(MyTripsPage.Search_button)));
		driver.findElement(By.xpath(MyTripsPage.Search_button)).click();
		log.info("user clicks on search buton");

	}

	@Then("^User clicks on Roundtrip$")
	public void user_clicks_on_roundtrip() throws Throwable {

		driver.findElement(By.xpath(BookPage.Round_Trip)).click();
		log.info("user succesfuly clicks on the Round trip radio button");

	}

	@Then("^User selects multi ADT PAX$")
	public void User_selects_multi_ADT_PAX() throws Throwable {
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		log.info("user selects 2 adult passenegers");
	}

	@When("^User Clicks on Vacation tab$")
	public void User_Clcks_on_Vacation_link() throws InterruptedException {
		log.info("!! Trying to click Vacation link *****");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Vacation_Tab)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(BookPage.Vacation_Tab)).click();
		log.info("!! We are in vacation booking with a car page *****");
		driver.findElement(By.xpath(BookPage.Flight_car_rdbtn));
		log.info("!! User choosen vacation booking with Flight and car");
	}

	@Then("^User enter from airport or city$")
	public void User_enter_from_airport_or_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_from_airport_or_city)));
		WebElement from = driver.findElement(By.xpath(BookPage.Vacation_from_airport_or_city));
		from.click();
		from.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.from_city)));
		driver.findElement(By.xpath(BookPage.from_city)).click();

	}

	@Then("^user selects vacation dates$")
	public void user_selects_vacation_dates() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.dates)));
		driver.findElement(By.xpath(BookPage.dates)).clear();
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		Date date = c.getTime();
		log.info("Departure date: " + dateFormat.format(date));
		String DepartureDate = dateFormat.format(date);
		c.setTime(date);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 3); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String ReturnDate = dateFormat.format(currentDatePlusOne);

		log.info("Return date: " + dateFormat.format(currentDatePlusOne));
		driver.findElement(By.xpath(BookPage.dates)).sendKeys(DepartureDate + " - " + ReturnDate);
	}

	@Then("^User selects under age of driver$")
	public void User_selects_under_age_of_driver() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.driver_age)));
		Select driversage = new Select(driver.findElement(By.xpath(BookPage.driver_age)));
		driversage.selectByIndex(1);
		log.info("user selectss under age of 21 driver");
	}

	@Then("^User clicks on Search vacations button$")
	public void user_clicks_on_search_vacations_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_vacationsbtn)));
		driver.findElement(By.xpath(BookPage.Search_vacationsbtn)).click();
	}

	@Then("^User enter to airport or city$")
	public void User_enter_to_airport_or_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_to_airport_or_city)));
		WebElement to = driver.findElement(By.xpath(BookPage.Vacation_to_airport_or_city));
		to.click();
		to.clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.TO_city)));
		driver.findElement(By.xpath(BookPage.TO_city)).click();
	}

	@Then("^User selects number of adult passengers$")
	public void User_selects_number_of_adult_passengers() throws Throwable {
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_clickon_passengers)));
//		driver.findElement(By.xpath(BookPage.Vacation_clickon_passengers)).click();
		// driver.findElement(By.xpath(BookPage.clickon_customers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		// driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		log.info("user selects the two adult passenger");
	}

	@Then("^user lands on flights cars page$")
	public void user_lands_on_flights_cars_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Book_car)));
		driver.findElement(By.xpath(BookPage.Book_car)).click();
	}

	@Then("^user lands on flights cars webpage with pre-selected flights$")
	public void user_lands_on_flights_cars_webpage_with_preselected_flights() throws Throwable {
		wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/book/flights-cars"));
		WebElement preselect = driver.findElement(By.xpath(BookPage.preselected_flights));
		Assert.assertTrue("There is no preselected flights available for your selection", preselect.isDisplayed());
		log.info("Preselected flight is available at flights-cars page");
	}

	@Then("^user selects the one adult passengers and one child$")
	public void user_selects_the_one_adult_passengers_and_one_child() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Vacation_clickon_passengers)));
		driver.findElement(By.xpath(BookPage.Vacation_clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
		driver.findElement(By.xpath(BookPage.select_child)).click();
	}

	@Then("^User clicks on Search vacations button and enters the birthdate of the Lap child$")
	public void user_clicks_on_search_vacations_button_and_enters_the_birthdate_of_the_Lap_child() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_vacationsbtn)));
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_vacationsbtn)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).clear();
			Date currentDate = new Date();
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, -6);
			Date date = c.getTime();
			log.info("Lap Child age is : " + dateFormat.format(date));
			String age = dateFormat.format(date);
			c.setTime(date);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(age);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

			driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.child_popup_continue_button)));
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the lap child");
		}
	}

	@Then("^User clicks on Search vacations button and enters the birthdate of the INFT child$")
	public void user_clicks_on_search_vacations_button_and_enters_the_birthdate_of_the_INFT_child() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_vacationsbtn)));
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_vacationsbtn)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).clear();
			Date currentDate = new Date();
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, -18);
			Date date = c.getTime();
			log.info("INFT age is : " + dateFormat.format(date));
			String age = dateFormat.format(date);
			c.setTime(date);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(age);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Vacations_Child_seat_option)));
			driver.findElement(By.xpath(PassengerinfoPage.Vacations_Child_seat_option)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.child_popup_continue_button)));
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the infant child");
		}
	}

	@Then("^User clicks on Search vacations button and enters the birthdate of the children$")
	public void user_clicks_on_search_vacations_button_and_enters_the_birthdate_of_the_children() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_vacationsbtn)));
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_vacationsbtn)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).clear();
			Date currentDate = new Date();
			Calendar c = Calendar.getInstance();
			c.add(Calendar.YEAR, -10);
			Date date = c.getTime();
			log.info("Children age is : " + dateFormat.format(date));
			String age = dateFormat.format(date);
			c.setTime(date);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(age);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.child_popup_continue_button)));
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
			log.info("user succesfully enter the birthdate of the children");
		}
	}

	@Then("^user selects vacation dates within 48 hours$")
	public void user_selects_vacation_dates_within_48_hours() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.dates)));
		driver.findElement(By.xpath(BookPage.dates)).clear();
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 0);
		Date date = c.getTime();
		log.info("Departure date: " + dateFormat.format(date));
		String DepartureDate = dateFormat.format(date);
		c.setTime(date);

		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 0);
		c.add(Calendar.DATE, 2); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String ReturnDate = dateFormat.format(currentDatePlusOne);

		log.info("Return date: " + dateFormat.format(currentDatePlusOne));
		driver.findElement(By.xpath(BookPage.dates)).sendKeys(DepartureDate + " - " + ReturnDate);
	}
	
	 @Then("^User choose the To Lima$")
	    public void User_choose_the_To_Lima() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.To_lim)));
	        driver.findElement(By.xpath(BookPage.To_lim)).click();
	    }
	 
	 @Then("^User clicks on Search button when you need POT$")
	    public void User_clicks_on_Search_button_when_you_need_POT() {
		 String parentHandle = driver.getWindowHandle();
	    
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Search_button)));
	            driver.findElement(By.xpath(BookPage.Search_button)).click();
	            for (String winHandle : driver.getWindowHandles()) {
	                driver.switchTo().window(winHandle); 
	            }  
	 
	        WebElement ok = driver.findElement(By.xpath(BookPage.Proof_of_return_okbtn));
	 
	        if (ok.isDisplayed()) {
	            ok.click();
	            log.info("This trips needs 'Proof of Return' on your travel");
	        } else {
	            log.info("This trips doesn't need 'Proof of Return' on your travel");
	        }
	        driver.switchTo().window(parentHandle);
	    }
	 
	    public static String depcitypair() {
	        WebDriver driver = null;
	        WebElement depfly = driver.findElement(By.xpath(BookPage.Selected_departure_citypair));
	        String citipair = depfly.getText();
	 
	        return citipair;
	    }
	    
	    
	    @Then("^User selects maximum number of adult passengers$")
		public void User_selects_maximum_number_of_adult_passengers() throws Throwable {
			driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
			Thread.sleep(2000);
			for (int i = 0; i < 8; i++) {
				driver.findElement(By.xpath(BookPage.select_adults)).click();
				log.info("Now adult passenger count is " + (i + 1));
			}
			Assert.assertTrue("User able to add more than 9 adult passengers",
					driver.findElement(By.xpath(BookPage.select_adults_disable)).isDisplayed());
			Assert.assertTrue("User able to add more than 9 child passengers",
					driver.findElement(By.xpath(BookPage.select_child_disable)).isDisplayed());
			log.info("User able to selects only total 9 number of passengers (Include Children) per trip");
		}

		@Then("^User selects maximum number of child passengers$")
		public void User_selects_maximum_number_of_child_passengers() throws Throwable {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
			for (int i = 9; i > 0; i--) {
				driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
			}
			log.info("Now adult passenger count is 0");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
			Thread.sleep(2000);
			for (int j = 0; j < 9; j++) {
				driver.findElement(By.xpath(BookPage.select_child)).click();
				log.info("Now child passenger count is " + (j + 1));
			}
			Assert.assertTrue("User able to add more than 9 adult passengers",
					driver.findElement(By.xpath(BookPage.select_adults_disable)).isDisplayed());
			Assert.assertTrue("User able to add more than 9 child passengers",
					driver.findElement(By.xpath(BookPage.select_child_disable)).isDisplayed());
			log.info("User able to selects only total 9 number of passengers (Include Adults) per trip");

			for (int i = 9; i > 0; i--) {
				driver.findElement(By.xpath(BookPage.select_child_minus_button)).click();
			}
			log.info("Now child passenger count is 0");

		}

		@Then("^user clicks Search and passengers required popup prompts$")
		public void user_clicks_Search_and_passengers_required_popup_prompts() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Search_button)));
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.Search_button)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				String design = driver.findElement(By.xpath("//div[@class='modal-header']")).getCssValue("display");
				log.info(design);
				Assert.assertTrue(
						"Passengers Required Pop-up is not showing straight yellow line on top of the modal header",
						design.equals("flex"));
				log.info("Verified Password Required pop-up modal header design");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@class='icon-close']")));
				driver.findElement(By.xpath("//i[@class='icon-close']")).click();
			}
			driver.switchTo().window(parentHandle);
			log.info("Modal is closed and back to search widget page");
		}

		@Then("^user verify add a promo code link is present$")
		public void user_verify_add_a__promo_code_link_is_present() throws Throwable {
			String s = driver.findElement(By.xpath(BookPage.add_promo_code_link)).getAttribute("href");
			log.info("promocode link is " + s);
		}

		@Then("^user Mouse over on the promo tool tip and validate the text$")
		public void user_mouse_over_on_the_promo_tool_tip_and_validate_the_text() throws Throwable {
			Thread.sleep(2000);
			String expectedTooltip = "You'll find our Promo Codes in our promotional emails.";
			WebElement question_mark = driver.findElement(By.xpath(BookPage.add_promo_tooltip));
//			Actions builder = new Actions(driver);
//			builder.clickAndHold().moveToElement(question_mark);
//			builder.moveToElement(question_mark).build().perform();
			question_mark.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(OnewaytripPage.mouse_over_popup_text))));
			WebElement toolTipElement = driver.findElement(By.xpath(OnewaytripPage.mouse_over_popup_text));
			String actualTooltip = toolTipElement.getText();
			log.info(actualTooltip);

		}

		@Then("^User clicks on Search vacations button and validate no car popup$")
		public void user_clicks_on_search_vacations_button_and_validate_no_car_popup() throws Throwable {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_vacationsbtn)));
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.Search_vacationsbtn)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
		Thread.sleep(2000);
						String car_popup_text=driver.findElement(By.xpath(BookPage.nocar_popup_text)).getText();
						log.info("popup message is" + car_popup_text );
						driver.findElement(By.xpath(BookPage.nocar_popup_text)).getText().equals(car_popup_text);
//				Assert.assertTrue("CHANGE YOUR SEARCH button not present in 'Car Unavailable' popup", driver
//						.findElement(By.xpath(BookPage.nocar_popup_changedatesbtn)).getText().equals("CHANGE YOUR SEARCH"));
//				Assert.assertTrue("SElect new dates button not present in 'Car Unavailable' popup",
//						driver.findElement(By.xpath(BookPage.nocar_popup_select_new_dates)).isDisplayed());
//				Assert.assertTrue("Close button not present in 'Car Unavailable' popup",
//						driver.findElement(By.xpath(BookPage.nocar_popup_close)).isDisplayed());
				driver.findElement(By.xpath(BookPage.nocar_popup_close)).click();
				log.info("No Car Available popup contents are validated");
			}
			driver.switchTo().window(parentHandle);
			log.info("User lands on search widget page");

		}

		@Then("^User choose the from city of Los Cabos$")
		public void User_choose_the_from_city_of_Los_Cabos() throws Throwable {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
			driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.from_sjd)));
			// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.from_city)));
			driver.findElement(By.xpath(BookPage.from_sjd)).click();

		}

		@Then("^user clicks on search button and validate seasonal service popup$")
		public void user_clicks_on_search_vacations_button_and_validate_seasonal_service_popup() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Search_button)));
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.Search_button)).click();
			driver.findElement(By.xpath(BookPage.Proof_of_return_okbtn)).click();
			log.info("This trips needs 'Proof of Return' on your travel");
			
				
			
		
		}

		@When("^user click miles tab on the right corner and expects login popup$")
		public void user_click_miles_tab_on_the_right_corner() throws Throwable {
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.book_miles)).click();
			log.info("User clicks Miles button on right corner of book page");
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_title)));
				String design = driver.findElement(By.xpath(BookPage.login_popup_title)).getText();
				log.info("Login popup Modal header display is " + design);
				Assert.assertTrue("'Login Account' Pop-up title is mismatch", design.equals("Log In To Your Account"));
				log.info("Verified Login pop-up prompts");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_resetpwd_link)));
				driver.findElement(By.xpath(BookPage.login_popup_resetpwd_link)).click();
				log.info("User clicks reset password link from login popup");
			}
			driver.switchTo().window(parentHandle);
			Thread.sleep(3000);
			Assert.assertTrue("User not navigated to retrieve password page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/retrieve-password"));
			log.info("User navigated to retrieve password page");
		}

		@Then("^user navigates to home page and booking again$")
		public void user_navigates_to_home_page_and_booking_again() throws Throwable {
			String url = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
			driver.get(url);
			Assert.assertTrue("User not navigated back to home page", driver.getCurrentUrl().equals(url));
			log.info("User navigated to home page");
		}

		@Then("^user clicks signup and redirects to account enrollment page$")
		public void user_clicks_signup_and_redirects_to_account_enrollment_page() throws Throwable {
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.book_miles)).click();
			log.info("User clicks Miles button on right corner of book page");
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_title)));
				String design = driver.findElement(By.xpath(BookPage.login_popup_title)).getText();
				log.info("Login popup Modal header display is " + design);
				Assert.assertTrue("'Login Account' Pop-up title is mismatch", design.equals("Log In To Your Account"));
				log.info("Verified Login pop-up prompts");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_signupbtn)));
				driver.findElement(By.xpath(BookPage.login_popup_signupbtn)).click();
				log.info("User clicks SignUp link from login popup");
			}
			driver.switchTo().window(parentHandle);
			wait.until(ExpectedConditions.urlToBe("http://qaepic01.spirit.com/account-enrollment"));
			Assert.assertTrue("User not navigated to account-enrollment page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/account-enrollment"));
			log.info("User navigated to account-enrollment page");
		}

		@Then("^user click miles tab and validate email address and password from login in popup$")
		public void user_click_miles_tab_and_validate_email_address_and_password_from_login_in_popup() throws Throwable {
			String parentHandle = driver.getWindowHandle();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.book_miles)));
			driver.findElement(By.xpath(BookPage.book_miles)).click();
			log.info("User clicks Miles button on right corner of book page");
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_title)));
				String design = driver.findElement(By.xpath(BookPage.login_popup_title)).getText();
				log.info("Login popup Modal header display is " + design);
				Assert.assertTrue("'Login Account' Pop-up title is mismatch", design.equals("Log In To Your Account"));
				log.info("Verified Login pop-up prompts");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_username)));
				driver.findElement(By.xpath(BookPage.login_popup_username)).click();
				driver.findElement(By.xpath(BookPage.login_popup_username)).clear();
				driver.findElement(By.xpath(BookPage.login_popup_username)).sendKeys("john.doe");
				driver.findElement(By.xpath(BookPage.login_popup_loginbtn)).click();
				log.info("Error message displays as "+driver.findElement(By.xpath(BookPage.login_popup_username_err)).getText());
//				Assert.assertTrue("Invalid email address error message is incorrect or missing",
//						driver.findElement(By.xpath(BookPage.login_popup_username_err)).getText()
//								.equals("Email Address or FREE SPIRIT® Number is required"));
//				log.info("Invalid email address error message is validated from login popup");
//				driver.findElement(By.xpath(BookPage.login_popup_username)).sendKeys("john.doe");
//				driver.findElement(By.xpath(BookPage.login_popup_username)).sendKeys(Keys.TAB);
				Assert.assertTrue("Invalid email address message is incorrect",
						driver.findElement(By.xpath(BookPage.login_popup_username_err)).getText().equals("email address is invalid"));
				log.info("Invalid email address error message is validated from login popup");
				driver.findElement(By.xpath(BookPage.login_popup_username)).clear();
				driver.findElement(By.xpath(BookPage.login_popup_username)).sendKeys("john.doe@spirit.com");
				driver.findElement(By.xpath(BookPage.login_popup_password)).clear();
				driver.findElement(By.xpath(BookPage.login_popup_loginbtn)).click();
				log.info("Error message displays as "+driver.findElement(By.xpath(BookPage.login_popup_pwd_err)).getText());
				Assert.assertTrue("Invalid password error message is incorrect",
						driver.findElement(By.xpath(BookPage.login_popup_pwd_err)).getText().equals("Password is required"));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.login_popup_closebtn)));
				driver.findElement(By.xpath(BookPage.login_popup_closebtn)).click();
				log.info("user clicks close button of Login popup");
			}
			driver.switchTo().window(parentHandle);
			String url = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
			wait.until(ExpectedConditions.urlToBe(url));
			Assert.assertTrue("User not navigated back to home page", driver.getCurrentUrl().equals(url));
			log.info("User navigated to home page");
		}

		@Then("^User choose the To city MSY$")
		public void User_choose_the_To_city_MSY() throws Throwable {

			driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.TO_city_MSY)));
			driver.findElement(By.xpath(BookPage.TO_city_MSY)).click();
		}
		
		
		@Then("^User choose the from city of FLL$")
	    public void User_choose_the_from_city_of_FLL() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
	        driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.From_FLL)));
	        // wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.from_city)));
	        driver.findElement(By.xpath(BookPage.From_FLL)).click();
	    }
	    
	    @Then("^User choose the To city of LAS$")
	    public void User_choose_the_To_city_of_LAS() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.To_LAS)));
	        driver.findElement(By.xpath(BookPage.To_LAS)).click();
	    }
	    
	    @Then("^User counts number of origin cities$")
	    public void User_counts_number_of_origin_cities() throws Throwable {
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
	        driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightAvailability.Origin_cities)));
	        java.util.List<WebElement> city=driver.findElements(By.xpath(FlightAvailability.Origin_cities));
	        int cities_count = city.size();
	        log.info("There are " + cities_count + " available cities for Origin");
	        for (int i = 0; i < cities_count; i++) {
	            log.info("City "+i+" = "+city.get(i).getText());
	        }
	        Assert.assertEquals(72, cities_count);
	        log.info("Origin airport cities are listed and validated");
	    }
	    
	    @Then("^User counts number of destination cities$")
	    public void User_counts_number_of_destination_cities() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_Tocity)));
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightAvailability.Destination_cities)));
	        java.util.List<WebElement> city= driver.findElements(By.xpath(FlightAvailability.Destination_cities));
	        int cities_count = city.size();
	        log.info("There are " + cities_count + " available cities for Destination");
	        for (int i = 0; i < cities_count; i++) {
	            log.info("City "+i+" = "+city.get(i).getText());
	        }
	        Assert.assertEquals(72, cities_count);
	        log.info("Destination airport cities are listed and validated");
	    }
	    
	    @Then("^User choose the To MDE$")
	    public void User_choose_the_To_MDE() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.To_MDE)));
	        driver.findElement(By.xpath(BookPage.To_MDE)).click();
	    }
	    
	    @Then("^User choose the To SAP$")
	    public void User_choose_the_To_SAP() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.To_SAP)));
	        driver.findElement(By.xpath(BookPage.To_SAP)).click();
	    }
	    
	    @Then("^User choose the To DTW$")
	    public void User_choose_the_To_DTW() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.To_DTW)));
	        driver.findElement(By.xpath(BookPage.To_DTW)).click();
	    }
	    
	    @Then("^User selects two adult passengers$")
	    public void User_selects_two_adult_passengers() throws Throwable {
	        driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
	        Thread.sleep(2000);
	        driver.findElement(By.xpath(BookPage.select_adults)).click();
	    }
	    
	    @Then("^user selects departures date outside of twenty four hours$")
	    public void User_selects_departure_date_outside_of_twenty_four_hours()throws Throwable{
	    	Date currentDate = new Date();
	        Calendar c = Calendar.getInstance();
	        c.setTime(currentDate);
	        // manipulate date
	    
	        c.add(Calendar.YEAR, 0);
	        c.add(Calendar.MONTH, 0);
	        c.add(Calendar.DATE, 2);  //same with c.add(Calendar.DAY_OF_MONTH, 1);
	        c.add(Calendar.HOUR, 0);
	        c.add(Calendar.MINUTE,0);
	        c.add(Calendar.SECOND, 0);

	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();
	        String DepartureDate = dateFormat.format(currentDatePlusOne);

	        log.info("Departure date is : "+dateFormat.format(currentDatePlusOne));
	        Thread.sleep(2000);
	   	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Date)));
	    	driver.findElement(By.xpath(BookPage.Date)).click();
	    	
	       driver.findElement(By.xpath(BookPage.Date)).clear();
	       driver.findElement(By.xpath(BookPage.Date)).sendKeys(DepartureDate);
	   	}
	    
	    
}