package com.cucumber.stepDefinitions;




import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage321;

import cucumber.api.java.en.Then;

public class SeatsStep321 {

	private static final Logger log = LogManager.getLogger();


	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	@Then("^User choose the To SAN city$")
	public void user_choose_the_to_SAN_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_Tocity)));
		driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage321.TO_SAN_City)));
		driver.findElement(By.xpath(SeatsPage321.TO_SAN_City)).click();
		log.info("user successfully selected the SAN city");
	}

	@Then("^user selects the airbus321 standard seat$")
	public void user_selects_the_standard_seat() throws Throwable {

		Thread.sleep(3000);
		String s = driver.findElement(By.xpath(SeatsPage321.Airbus_Name)).getText();
		log.info("flight name " + s);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage321.Airbus_321_Standard_Seat_12A)));
		driver.findElement(By.xpath(SeatsPage321.Airbus_321_Standard_Seat_12A)).click();

	}

	@Then("^user attempts to select the Exit row and Big front seat for the child but it should be blocked$")
	public void user_trying_to_selects_the_exit_row_and_big_front_seat_for_the_child_but_it_should_be_blocked()
			throws Throwable {
		try {
			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
						SeatsPage321.Airbus_321_Exit_Seat_11C, SeatsPage321.Airbus_321_Exit_Seat_11D,
						SeatsPage321.Airbus_321_Exit_Seat_11E, SeatsPage321.Airbus_321_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for child passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {

		}
		driver.findElement(By.xpath(SeatsPage321.Airbus_321_Standard_Seat_10E)).click();
		log.info("user selects the standard seat for the child");

	}

	@Then("^user attempts to select the exit row even though it should be disabled$")
	public void user_trying_to_selects_the_exit_row_but_it_should_be_disabled() throws Throwable {

		try {
			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
						SeatsPage321.Airbus_321_Exit_Seat_11C, SeatsPage321.Airbus_321_Exit_Seat_11D,
						SeatsPage321.Airbus_321_Exit_Seat_11E, SeatsPage321.Airbus_321_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for child passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {

		}
//		driver.findElement(By.xpath(SeatsPage321.Airbus_321_Standard_Seat_10E)).click();
//		log.info("user selects the standard seat for the child");

	}

	@Then("^user trying to select the Exit row and big front seat but it should be disabled$")
	public void user_trying_to_select_the_exit_row_and_big_front_seat_but_it_should_be_disabled() throws Throwable {

		try {
			for (int i = 0; i <= 11; i++) {
				String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
						SeatsPage321.Airbus_321_Exit_Seat_11C, SeatsPage321.Airbus_321_Exit_Seat_11D,
						SeatsPage321.Airbus_321_Exit_Seat_11E, SeatsPage321.Airbus_321_Exit_Seat_11F,
						SeatsPage321.Airbus_321_Big_Front_Seat_1A, SeatsPage321.Airbus_321_Big_Front_Seat_1B,
						SeatsPage321.Airbus_321_Big_Front_Seat_1E, SeatsPage321.Airbus_321_Big_Front_Seat_1F,
						SeatsPage321.Airbus_321_Standard_Seat_5A, SeatsPage321.Airbus_321_Standard_Seat_5B,
						SeatsPage321.Airbus_321_Standard_Seat_5C, SeatsPage321.Airbus_321_Standard_Seat_5D,
						SeatsPage321.Airbus_321_Standard_Seat_5E, SeatsPage321.Airbus_321_Standard_Seat_5F };
				Assert.assertTrue("The disabled seats are displayed for child passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {

		}
	}
	
	 
	  
//	@Then("^user inputs the pnr code and clicks on the my trips button and enters the lastname and confirmation code$")
//    public void user_inputs_the_pnr_code_and_clicks_on_the_my_trips_button_and_enters_the_lastname_and_confirmation_code() throws Throwable {
//	    
//	         
//	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
//	        String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
//	        String pnr = tmp.replace("Confirmation Code: ", "");
//	        log.info("The PNR obtained after the booking is : " + pnr);
//	        Thread.sleep(5000);
//	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.mytrips_mainmenu)));
//	        driver.findElement(By.xpath(PassengerinfoPage.mytrips_mainmenu)).click();
//	        log.info("Clicked on the checkin button present on top banner");
//	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MyTripsPage.Passengers_Lastname)));
//	        driver.findElement(By.xpath(MyTripsPage.Passengers_Lastname)).sendKeys("flyer");
//	        log.info("Passenger entering the last name as mentioned");
//	        driver.findElement(By.xpath(MyTripsPage.confromation_code)).sendKeys(pnr);
//	        log.info("Entered the confirmation code as : " + pnr);
//	        try {
//	            Thread.sleep(15000);
//	        } catch (InterruptedException e) {
//	        }
//	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(MyTripsPage.Search_button)));
//	        driver.findElement(By.xpath(MyTripsPage.Search_button)).click();
//	        log.info("Then user clicked on the check-in button to check the info of the booked ticket");
//	    }
	 
//	@Then("^user clicks on the Edit seats button$")
//    public void user_clicks_on_the_edit_seats_button() throws Throwable {
//	
//		
//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage321.mytrips_edit_seats)));
//    driver.findElement(By.xpath(SeatsPage321.mytrips_edit_seats)).click();
//	log.info("Clicked edit seats");
//	}
	
	@Then("^user clicks on check in tab and inputs last name and pnr and clicks check in$")
    public void user_clicks_on_check_in_tab_and_inputs_last_name_and_pnr_and_clicks_check_in() throws Throwable {
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
        String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
        String pnr = tmp.replace("Confirmation Code: ", "");
        log.info("The PNR obtained after the booking is : " + pnr);
        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_mainmenu)));
        driver.findElement(By.xpath(PassengerinfoPage.checkin_mainmenu)).click();
        log.info("Clicked on the checkin button present on top banner");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath((CheckInPage.passengers_lastname))));
        driver.findElement(By.xpath((CheckInPage.passengers_lastname))).sendKeys("flyer");
        log.info("Passenger entering the last name as mentioned");
        driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
        log.info("Entered the confirmation code as : " + pnr);
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.checkin_button)));
        driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
        log.info("Then user clicked on the check-in button to check the info of the booked ticket");
	}
     @Then("^User validates city pair and passenger names$")
     public void user_validates_city_pair_and_passenger_names() throws Throwable {
	 String s1=driver.findElement(By.xpath(SeatsPage321.Validate_City_Pairs_Seats_Page)).getText();
    	 Assert.assertTrue("User could not validate the city pair",
 				driver.findElement(By.xpath(SeatsPage321.Validate_City_Pairs_Seats_Page)).getText().equals("Fort Lauderdale (FLL) - San Diego (SAN)"));
 		log.info("user succesfully validated the  City Pairs ");
      String s =driver.findElement(By.xpath(SeatsPage321.Validate_Pax_Names)).getText();
 		Assert.assertTrue("User could not validate the city pair",
 				driver.findElement(By.xpath(SeatsPage321.Validate_Pax_Names)).getText().equals(s));
 		log.info("user succesfully validated the passenger names");
     }
	
     @Then("^user clicks on the exit row seat and switch to the popup and selects the change seat$")
     public void user_clicks_on_the_exit_row_seat_and_switch_to_the_popup_and_selects_the_change_seat() throws Throwable {
        
    	 String parentHandle = driver.getWindowHandle(); 
    	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage321 .Airbus_321_Exit_Seat_11A)));
    	 driver.findElement(By.xpath(SeatsPage321 .Airbus_321_Exit_Seat_11A)).click(); 

    	 for (String winHandle : driver.getWindowHandles()) {
    	     driver.switchTo().window(winHandle); 
    	     log.info("driver switch to the popup");
    	 }
    	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage321.change_seat)));
    	  driver.findElement(By.xpath(SeatsPage321.change_seat)).click();
        	log.info("user switch to the popup and selects the change seat");
    	 driver.switchTo().window(parentHandle);
    	 
     }
     
 	@Then("^User selects an exit row seat on plane321$")
    public void user_selects_an_exit_row_seat_on_plane321() throws Throwable {
	 
   	 try {
            for (int i = 0; i <= 8; i++) {
                String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
                        SeatsPage321.Airbus_321_Exit_Seat_11C, SeatsPage321.Airbus_321_Exit_Seat_11D,
                        SeatsPage321.Airbus_321_Exit_Seat_11E, SeatsPage321.Airbus_321_Exit_Seat_11F, SeatsPage321.Airbus_321_Exit_Seat_25A, SeatsPage321.Airbus_321_Exit_Seat_25B, SeatsPage321.Airbus_321_Exit_Seat_25C, SeatsPage321.Airbus_321_Exit_Seat_25D, SeatsPage321.Airbus_321_Exit_Seat_25E, SeatsPage321.Airbus_321_Exit_Seat_25F };
                Assert.assertTrue("Exit Row Seat is Successfully Selected!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());

            }
        } 
   	 catch (NoSuchElementException e) {
     }
//   	 driver.findElement(By.xpath(SeatsPage321.Airbus_32_A_exitrow_popup)).click(); 
}

	@Then("^User verifies the price in the seats total content block$")
   public void user_verifies_the_price_in_the_seats_total_content_block() throws Throwable {
		driver.findElement(By.xpath(SeatsPage321.first_flight_carrot)).click();
		Assert.assertTrue("User could not validate the city pair",
				driver.findElement(By.xpath(SeatsPage321.first_flight_carrot)).getText()
						.equals("FLL - IAH"));
		log.info("user succesfully validated the  City Pairs ");
		Assert.assertTrue("User could not validate the total price",
				driver.findElement(By.xpath(SeatsPage321.first_city_total_price_validation)).getText().equals(""));
		log.info("user succesfully validated the Total Price");
		
	}
     
	
     @Then("^User clicks on regular seat$")
     public void user_clicks_on_regular_seat() throws Throwable {
 	
 		try {
             for (int i = 0; i <= 5; i++) {
                 String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
                         SeatsPage321.Airbus_321_Standard_Seat_3A, SeatsPage321.Airbus_321_Standard_Seat_3B,
                         SeatsPage321.Airbus_321_Standard_Seat_4F, SeatsPage321.Airbus_321_Standard_Seat_5C, SeatsPage321.Airbus_321_Standard_Seat_6E, SeatsPage321.Airbus_321_Standard_Seat_7A, SeatsPage321.Airbus_321_Standard_Seat_7D, SeatsPage321.Airbus_321_Standard_Seat_8C, SeatsPage321.Airbus_321_Standard_Seat_8D, SeatsPage321.Airbus_321_Standard_Seat_9A, SeatsPage321.Airbus_321_Standard_Seat_12E};
                 Assert.assertTrue("Regular Seat is Successfully Selected!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());

             }
         } 
    	 catch (NoSuchElementException e) {
    }
  }
     
     
 	@Then("^user checks off active duty US military personnel$")
    public void user_checks_off_active_duty_us_military_personnel() throws Throwable {
 		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)).click();
 	}
 	
 	@Then("^user clicks on a 321BFS$")
    public void user_clicks_on_a_321bfs() throws Throwable {
 		
 		try {
            for (int i = 0; i <= 7; i++) {
                String seatxpath[] = { SeatsPage321.Airbus_321_Big_Front_Seat_1A, SeatsPage321.Airbus_321_Big_Front_Seat_1B,
                        SeatsPage321.Airbus_321_Big_Front_Seat_1E, SeatsPage321.Airbus_321_Big_Front_Seat_1F,
                        SeatsPage321.Airbus_321_Big_Front_Seat_2A, SeatsPage321.Airbus_321_Big_Front_Seat_2B, SeatsPage321.Airbus_321_Big_Front_Seat_2E, SeatsPage321.Airbus_321_Big_Front_Seat_2F};
                Assert.assertTrue("Big Front Seat is Successfully Selected!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());

            }
        } 
   	 catch (NoSuchElementException e) {
   
 	}
  }

 	@Then("^user clicks on regular seat for passenger2$")
    public void user_clicks_on_regular_seat_for_passenger2() throws Throwable {
	
		try {
            for (int i = 0; i <= 5; i++) {
                String seatxpath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
                        SeatsPage321.Airbus_321_Standard_Seat_3A, SeatsPage321.Airbus_321_Standard_Seat_3B,
                        SeatsPage321.Airbus_321_Standard_Seat_4F, SeatsPage321.Airbus_321_Standard_Seat_5C, SeatsPage321.Airbus_321_Standard_Seat_6E, SeatsPage321.Airbus_321_Standard_Seat_7A, SeatsPage321.Airbus_321_Standard_Seat_7D, SeatsPage321.Airbus_321_Standard_Seat_8C, SeatsPage321.Airbus_321_Standard_Seat_8D, SeatsPage321.Airbus_321_Standard_Seat_9A, SeatsPage321.Airbus_321_Standard_Seat_12E};
                Assert.assertTrue("Regular Seat is Successfully Selected for passenger 2!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());

            }
        } 
   	 catch (NoSuchElementException e) {
   }
 }
 	
 	@Then("^User verifies the price in the seats total content block for passenger2$")
    public void user_verifies_the_price_in_the_seats_total_content_block_for_passenger2() throws Throwable {
 		driver.findElement(By.xpath(SeatsPage321.first_flight_carrot)).click();
 		Assert.assertTrue("User could not validate the city pair",
 				driver.findElement(By.xpath(SeatsPage321.first_flight_carrot)).getText()
 						.equals("FLL - IAH"));
 		log.info("user succesfully validated the  City Pairs ");
 		Assert.assertTrue("User could not validate the total price",
 				driver.findElement(By.xpath(SeatsPage321.first_city_total_price_validation)).getText().equals(""));
 		log.info("user succesfully validated the Total Price");
 		
 		
 	}
 	
 	@Then("^User validates city pair and both passenger names$")
    public void user_validates_city_pair_and_both_passenger_names() throws Throwable {
 		Assert.assertTrue("User could not validate the city pair",
 				driver.findElement(By.xpath(SeatsPage321.Validate_City_Pairs_Seats_Page)).getText().equals("Fort Lauderdale (FLL) - San Diego (SAN)"));
 		log.info("user succesfully validated the  City Pairs ");
 		Assert.assertTrue("User could not validate the Passenger 1 name",
 				driver.findElement(By.xpath(SeatsPage321.Validate_Pax_Names)).getText().equals("jack flayer"));
 		log.info("user succesfully validated passenger 1 name");
 		Assert.assertTrue("User could not validate the Passenger 2 name",
 				driver.findElement(By.xpath(SeatsPage321.Validate_Pax_Names)).getText().equals("dwayne johnson"));
 		log.info("user succesfully validated passenger 2 name");
 	}
 	
 	@Then("^user selects the blue change seat button$")
    public void user_selects_the_blue_change_seat_button() throws Throwable {
 		driver.findElement(By.xpath(SeatsPage321.change_seat)).click();
 	}
 	
 	@Then("^user selects the blue accept seat button$")
    public void user_selects_the_blue_accept_seat_button() throws Throwable {
 		driver.findElement(By.xpath(SeatsPage321.Airbus_32_A_exitrow_popup)).click();
 	}
 	
 	@Then("^user clicks on choose thrills combo$")
    public void user_clicks_on_choose_thrills_combo() throws Throwable {
 		driver.findElement(By.xpath(SeatsPage321.Choose_Thrills_Combo)).click();
 	}
 	

}
