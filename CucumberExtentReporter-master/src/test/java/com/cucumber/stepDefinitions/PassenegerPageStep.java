package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.PaymentPage;
import com.cucumber.pages.VacationPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;

public class PassenegerPageStep {

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	private static final Logger log = LogManager.getLogger();
	private static String free_spirit_email = FileReaderManager.getInstance().getTestDataReader().freeSpiritEmail();
	private static String free_spirit_password = FileReaderManager.getInstance().getTestDataReader()
			.freeSpiritPassword();
	private static String child_DOB = FileReaderManager.getInstance().getTestDataReader().childDOB();
	
	private static String NineFC_valid_emailaddress = FileReaderManager.getInstance().getTestDataReader().Ninefcemail();
	private static String NineFC_Valid_Password = FileReaderManager.getInstance().getTestDataReader().Ninepassword();
	private static String NineFC_Valid_Fsnumber = FileReaderManager.getInstance().getTestDataReader().NineFSNumber();

	@Then("^user selects the one child and one adult passengers$")
	public void user_selects_the_one_child_and_one_adult_passengers() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("1");
		// driver.findElement(By.xpath(PassengerinfoPage.select_child)).clear();
		// driver.findElement(By.xpath(PassengerinfoPage.select_child)).sendKeys("1");
		driver.findElement(By.xpath(BookPage.select_child)).click();
	}

	@Then("^user clicks on search button and enters the birthdate of the child$")
	public void user_enters_the_birthdate_of_the_child() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		Thread.sleep(4000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			Thread.sleep(2000);
			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_month)));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(child_DOB);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
			log.info("user succesfully enter the birthdate of the child");
//			
//			//CHILD TWO
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child2_date_of_birth)));
//			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(child_DOB);
//			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.ENTER);
//			log.info("user succesfully enter the birthdate of the child");
//			
//			//CHILD THREE
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child3_date_of_birth)));
//			driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys(child_DOB);
//			driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys(Keys.ENTER);
//			log.info("user succesfully enter the birthdate of the child");
	
			log.info("user succesfully enter the birthdate of the child");
			String design = driver.findElement(By.xpath("//div[@class='modal-header']")).getCssValue("display");
			log.info(design);
			;
			Assert.assertTrue("Young Travelers Pop-up is not showing straight yellow line on top of the modal header",
					design.equals("flex"));
			log.info("Young Travelers Pop-up header is showing straight yellow line");
			
			
		}
	}
	
	
	
	  @Then("^user clicks on search button and enters three birthdate of the children$")
	    public void user_clicks_on_search_button_and_enters_three_birthdate_of_the_children() throws Throwable {
	      
		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
			driver.findElement(By.xpath(BookPage.Search_button)).click();
			Thread.sleep(4000);
			String parentHandle = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				Thread.sleep(2000);
				// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_month)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(child_DOB);
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
				log.info("user succesfully enter the birthdate of the child");
				
				//CHILD TWO
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child2_date_of_birth)));
				driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(child_DOB);
				driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.ENTER);
				log.info("user succesfully enter the birthdate of the child");
				
				//CHILD THREE
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child3_date_of_birth)));
				driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys(child_DOB);
				driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys(Keys.ENTER);
				log.info("user succesfully enter the birthdate of the child");
		
			}
		  
	    }


	@Then("^user clicks on the popup continue button$")
	public void user_clicks_on_the_popup_continue_button() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_popup_continue_button)));
		driver.findElement(By.xpath(PassengerinfoPage.child_popup_continue_button)).click();
		log.info("user successfully handle the popup and navigated to the flight availability page");
	}

	@Then("^user enters the primary passenger information$")
	public void user_enters_the_primary_passenger_information() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
//		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).click();
//		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.DOWN);
//		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.ENTER);

		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByIndex(1);
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flyer");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("07/12/1989");
		driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
		log.info("user successfully select the primary passenger  contact checkbox ");
		log.info("user succesfully enter the primary passengers information");

	}

	@Then("^user selects the usa military personnel checkbox$")
	public void user_selects_the_usa_military_personnel_checkbox() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ActiveDutyMilitaryPersonnelCheckBox)).click();

		log.info("user selects the active duty military personnel check box");
	}

	@Then("^user enters the personal information of passenger two$")
	public void user_enters_the_personal_information_of_passenger_two() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
        s.selectByVisibleText("Mr.");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_firstname)));
        driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("dwanye");
        driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("jhonson");
        log.info("The child age displayed "
                + driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getAttribute("value"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
//    Assert.assertTrue("child birth date is not populated",
//    		
//         driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getAttribute("value").equals("03/12/2018"));
         log.info("child date of birth is auto populated and validated");
        
        log.info("user succesfully enters the child information");
	}
	
	
	
	  @Then("^user enters the infant passneger information$")
	    public void user_enters_the_infant_passneger_information() throws Throwable {
	       
		  wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
			driver.findElement(By.xpath(PassengerinfoPage.child_title));
	        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
	        s.selectByVisibleText("Mr.");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.infant_firstname)));
	        driver.findElement(By.xpath(PassengerinfoPage.infant_firstname)).sendKeys("dwanye");
	        driver.findElement(By.xpath(PassengerinfoPage.infant_lastname)).sendKeys("jhonson");
	        log.info("The child age displayed "
	                + driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getAttribute("value"));
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
		  
		  
	    }
	
	
	 @Then("^user enters the personal information of Lap child$")
	    public void user_enters_the_personal_information_of_lap_child() throws Throwable {
	        
		 
		 wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
			driver.findElement(By.xpath(PassengerinfoPage.child_title));
	        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
	        s.selectByVisibleText("Mr.");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.infant_firstname)));
	        driver.findElement(By.xpath(PassengerinfoPage.infant_firstname)).sendKeys("dwanye");
	        driver.findElement(By.xpath(PassengerinfoPage.infant_lastname)).sendKeys("jhonson");
	        log.info("The child age displayed "
	                + driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getAttribute("value"));
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
		 
		 
	    }


	@Then("^user enters the contact information$")
	public void user_enters_the_contact_information() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.passengers_address)).sendKeys("603 west side");

		driver.findElement(By.xpath(PassengerinfoPage.passengers_city)).sendKeys("Miami");

		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.passengers_state)));
		s.selectByVisibleText("Florida");

		driver.findElement(By.xpath(PassengerinfoPage.zip_code)).sendKeys("33456");

		// Select s = new
		// Select(driver.findElement(By.xpath(PassengerinfoPage.country)));
		// s.selectByVisibleText("United States of America");

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengers_state)));
		// driver.findElement(By.xpath(PassengerinfoPage.passengers_state)).click();
		// driver.findElement(By.xpath(PassengerinfoPage.passengers_state)).sendKeys("Fl");
		// driver.findElement(By.xpath(PassengerinfoPage.passengers_state)).sendKeys(Keys.ENTER);

		driver.findElement(By.xpath(PassengerinfoPage.passengers_email)).sendKeys("jack.flyer@gmail.com");
		driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("jack.flyer@gmail.com");
		driver.findElement(By.xpath(PassengerinfoPage.Passengers_phone)).sendKeys("1230000000");
		// driver.findElement(By.xpath(PassengerinfoPage.free_spirit_check_button)).click();
		log.info("user successfully enters the contact information");

	}

	@Then("^user login as FS member$")
	public void user_login_as_fs_member() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys(free_spirit_email);

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys(free_spirit_password);

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a free spirit member");

	}

	@Then("^user clicks on additional services and selects the emotional support animal check box$")
	public void user_clicks_on_additional_services_and_selects_the_emotional_support_animal_check_box()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)).click();
		log.info("user succesfully selects the emotional check box");

	}

	@Then("^user Click the drop down under the contact information of a random PAX and add a service animal$")
	public void user_click_the_drop_down_under_the_contact_information_of_a_random_pax_and_add_a_service_animal()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("user succesfully selected the service animal checkbox");

	}

	@Then("^user add a Wheelchair and click the Completely Immobile I Have My own wheelchair Need Help to From Gate and Need Help to From Seat check boxes$")
	public void user_add_a_wheelchair_and_click_the_completely_immobile_i_have_my_own_wheelchair_need_help_to_from_gate_and_need_help_to_from_seat_check_boxes()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ompletely_immobile)));

		driver.findElement(By.xpath(PassengerinfoPage.adult_ompletely_immobile)).click();
		log.info("user succesfully selected the  completely immobile ");
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user succesfully selected the my own wheel chair");
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("user succesfully selected the service Need help to/from gate");
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		s.selectByVisibleText("Manually Powered");
		log.info("user succesfully selected the manually powered wheel chair from the dropdown");
	}

	@Then("^user clicks on additional services and selects the hearing impaired$")
	public void user_clicks_on_additional_services_and_selects_the_hearing_impaired() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)).click();
		log.info("user succesfully selected the hearing impaired check box");
	}

	@Then("^user clicks on additional services and selects the other disability$")
	public void user_clicks_on_additional_services_and_selects_the_other_disability() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_other_checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_other_checkbox)).click();

		log.info("user succesfully selected the others check box");

	}

	@Then("^user clicks on additional services and selects the vision disability$")
	public void user_clicks_on_additional_services_and_selects_the_vision_disability() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)).click();

		log.info("user succesfully selected the vision disability check box");

	}

	@Then("^user selects the us military personnel checkbox$")
	public void user_selects_the_us_military_personnel_checkbox() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_active_duty_militery)).click();
		log.info("user selects the usa military perssonnel check box for passenger two also");
	}

	@Then("^user selects the one passenger$")
	public void user_selects_the_one_passenger() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("1");
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
	}

	@Then("^user clicks on additional services and selects on the service animal and wheel chair$")
	public void user_clicks_on_additional_services_and_selects_on_the_service_animal_and_wheel_chair()
			throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();

		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();

		log.info("user succesfully selected the service animal and wheel chair");

	}

	@Then("^user clicks on additional services and selects Emotional support animal$")
	public void user_clicks_on_additional_services_and_selects_emotional_support_animal() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)).click();

		log.info("user succesfully selected the pschiatric support animal  check box");

	}

	@Then("^user clicks on additional services and selects Pet in cabin$")
	public void user_clicks_on_additional_services_and_selects_pet_in_cabin() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_pet_in_cabin_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_pet_in_cabin_Checkbox)).click();

		log.info("user succesfully selected the pet in cabin  check box");

	}

	@Then("^user clicks on additional services and selects the portable oxygen$")
	public void user_clicks_on_additional_services_and_selects_the_portable_oxygen() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_portable_oxygen)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_portable_oxygen)).click();

		log.info("user succesfully selected the portable oxygen  check box");

	}

	@Then("^user clicks on additional services add a Wheelchair and click the Completely Immobile I Have My own wheelchair Need Help to From Gate and Need Help to From Seat check boxes$")
	public void user_clicks_on_additional_services_add_a_wheelchair_and_click_the_completely_immobile_i_have_my_own_wheelchair_need_help_to_from_gate_and_need_help_to_from_seat_check_boxes()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ompletely_immobile)));

		driver.findElement(By.xpath(PassengerinfoPage.adult_ompletely_immobile)).click();
		log.info("user succesfully selected the  completely immobile ");
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user succesfully selected the my own wheel chair");
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("user succesfully selected the service Need help to/from gate");
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		s.selectByVisibleText("Manually Powered");
		log.info("user succesfully selected the manually powered wheel chair from the dropdown");

	}

	@Then("^user clicks the Additional Services drop down and click the wheelchair and Service animal check box$")
	public void user_clicks_the_additional_services_drop_down_and_click_the_wheelchair_and_service_animal_check_box()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_own_wheel_chair)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();

		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("user succesfully selected the wheel chair and service animal  check box");

	}

	@Then("^user selects the multi passenger$")
	public void user_selects_the_multi_passenger() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.select_adults)));
		driver.findElement(By.xpath(OnewaytripPage.select_adults)).click();

	}

	@Then("^user takes the new pnr code and clicks on the checkin button and enters the lastname and PNR$")
	public void user_takes_the_new_pnr_code_and_clicks_on_the_checkin_button() throws Throwable {

		// driver.findElement(By.xpath(PassengerinfoPage.conformation_page_popup)).click();
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
		String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
		String pnr = tmp.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + pnr);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_mainmenu)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_mainmenu)).click();
		log.info("Clicked on the checkin button present on top banner");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
		log.info("Passenger entering the last name as mentioned");
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
		log.info("Entered the confirmation code as : " + pnr);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.checkin_button)));
		driver.findElement(By.xpath(CheckInPage.checkin_button)).click();
		log.info("Then user clicked on the search button to check the info of the booked ticket");
	}

	@Then("^user enters the personal information of adult two$")
	public void user_enters_the_personal_information_of_adult_two() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("jason");

		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("12/12/1988");
	}

	@Then("^user clicks on the additional info and selects the hearing disability$")
	public void user_clicks_on_the_additional_info_and_selects_the_hearing_disability() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_Heraing_disability)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_Heraing_disability)).click();

	}

	@Then("^user clicks on the Save button$")
	public void user_clicks_on_the_Save_button() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.chekin_primarypassenger_save)));
		driver.findElement(By.xpath(PassengerinfoPage.chekin_primarypassenger_save)).click();
		log.info("user clicks on save button");
	}

	@Then("^user clicks on the additional info and selects the other disability$")
	public void user_clicks_on_the_additional_info_and_selects_the_other_disability() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_primarypassenger_other)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_other)).click();
		log.info("user succesfully clicks on the other disability");

	}

	@Then("^user clicks on the additional info and selects the vision disability$")
	public void user_clicks_on_the_additional_info_and_selects_the_vision_disability() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_visiondisability)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_visiondisability)).click();
		log.info("user succesfully clicks on the vision disability");
	}

	@Then("^user clicks on the additional info and selects the Wheel chair and service animal$")
	public void user_clicks_on_the_additional_info_and_selects_the_wheel_chair_and_service_animal() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_service_animal)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_service_animal)).click();
		log.info("user succesfully clicks on the service animal");

		driver.findElement(By.xpath(PassengerinfoPage.checkinwheelchair_to_from_gate)).click();
		log.info("user selects the wheel chair to/from gate");

		driver.findElement(By.xpath(PassengerinfoPage.wheelchair_need_help_from_gate)).click();
		log.info("user selects the wheel chair help from gate");
		driver.findElement(By.xpath(PassengerinfoPage.checkin_has_own_wheelchair)).click();
		log.info("user clicks on my own wheel chair");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		s.selectByVisibleText("Battery Powered Dry/Gel Cell Battery");
		log.info("user selects the Battery powered dry/gel cell battery");
	}

	@Then("^user clicks on the additional info and selects the emotional support animal$")
	public void user_clicks_on_the_additional_info_and_selects_the_emotional_support_animal() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_emotional)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_emotional)).click();
		log.info("user succesfully clicks on the emotional support animal");

	}

	@Then("^user clicks on the additional info and selects the wheel chair and POC$")
	public void user_clicks_on_the_additional_info_and_selects_the_wheel_chair_and_poc() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_POC)).click();
		log.info("user able to selects the POC");

		driver.findElement(By.xpath(PassengerinfoPage.checkinwheelchair_to_from_gate)).click();
		log.info("user selects the wheel chair to/from gate");
		driver.findElement(By.xpath(PassengerinfoPage.wheelchair_need_help_from_gate)).click();
		log.info("user selects the wheel chair help from gate");
		driver.findElement(By.xpath(PassengerinfoPage.checkin_has_own_wheelchair)).click();
		log.info("user clicks on my own wheel chair");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		s.selectByVisibleText("Manually Powered");
		log.info("user selects the manually powered wheel chair");

	}

	@Then("^user clicks on the additional info and selects the hearing disability and passenger two selects the vision disability$")
	public void user_clicks_on_the_additional_info_and_selects_the_hearing_disability_and_passenger_two_selects_the_vision_disability()
			throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_Heraing_disability)).click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_additional_info_passenger_two)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info_passenger_two)).click();
		log.info("user two successfully clicks on the additional info");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_visiondisability_passenger_two)).click();

		log.info("user successfuly selects the vision disability for passenger two");

	}

	@Then("^user clicks on the additional info and selects the service animal to a military member$")
	public void user_clicks_on_the_additional_info_and_selects_the_service_animal() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_service_animal)).click();
		log.info("user succesfully selects the service animal");

	}

	@Then("^user clicks on the add button and enter the incorrect known travel number$")
	public void user_clicks_on_the_add_button_and_enter_the_incorrect_known_travel_number() throws Throwable {
		Thread.sleep(10000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_passenger_two_add_button1)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_add_button1)).click();
		log.info("user clicks on the add link next to the known traveler");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_inputbox)));

		driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_inputbox)).sendKeys("&$");
		log.info("user enters the known traveler number incorrectly  ");

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_errormessage)));
		Assert.assertTrue("User not validates the text below the knowntaveler",
				driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_errormessage))
						.getText().equals("A KTN value must be 1-26 characters long and alphanumeric only."));

		log.info("user succesfully validate the error message for the wrong known traveler number");
	}

	@Then("^user clicks on the  known traveler Save button$")
	public void user_clicks_on_the_known_traveler_save_button() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_save_button)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_save_button)).click();

	}

	@Then("^user clicks on the add button and enter the correct known travel number$")
	public void user_clicks_on_the_add_button_and_enter_the_correct_known_travel_number() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_passenger_two_add_button1)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_add_button1)).click();
		log.info("user clicks on the add link next to the known traveler");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_inputbox)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_passenger_two_knowntraveler_inputbox))
				.sendKeys("135704549");
		log.info("user enters the known traveler number correctly  ");
		
	}

	@Then("^user clicks on the additional info and selects the vision disability and service animal$")
	public void user_clicks_on_the_additional_info_and_selects_the_vision_disability_and_service_animal()
			throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_service_animal)).click();
		log.info("user succesfully selects the service animal");

		driver.findElement(By.xpath(PassengerinfoPage.checkin_visiondisability)).click();
		log.info("user succesfully selects the vision disability");

	}

	@Then("^user clicks on the Cancel button$")
	public void user_clicks_on_the_cancel_button() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_cancel)).click();
		log.info("user succesfully clicks on the cancel button");
	}

	@Then("^user selects the voluntry provision of emergency service program$")
	public void user_selects_the_voluntry_provision_of_emergency_service_program() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_primarypassenger_voluntary_provision)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_voluntary_provision)).click();
		log.info("user succesfully clicks on the voluntry provision service program ");

	}

	@Then("^user clicks on the additional information button$")
	public void user_clicks_on_the_additional_information_button() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_additional_info)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_additional_info)).click();
		log.info("user successfully clicks on the additional info");

	}

	@Then("^user selects the diability seating and selects the  I need a seat with a moveable aisle armrest$")
	public void user_selects_the_diability_seating_and_selects_the_i_need_a_seat_with_a_moveable_aisle_armrest()
			throws Throwable {
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)).click();

		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disablity_seating_aisel_arrest)).click();

	}

	@Then("^user selects the diability seating and selects the  I am unable to bend leg fused leg immobile leg$")
	public void user_selects_the_diability_seating_and_selects_the_i_am_unable_to_bend_leg_fused_leg_immobile_leg()
			throws Throwable {

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)).click();

		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disability_seating_bendleg)).click();

	}

	@Then("^user selects the diability seating and selects the I need a seat for someone traveling with me$")
	public void user_selects_the_diability_seating_and_selects_the_i_need_a_seat_for_someone_traveling_with_me()
			throws Throwable {

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disabllity_seating)).click();
		driver.findElement(By.xpath(PassengerinfoPage.checkin_primarypassenger_disability_seating_traveling_withme))
				.click();
	}

	@Then("^user selects the POC$")
	public void user_selects_the_poc() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_POC)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_POC)).click();

	}

	@Then("^user selects the one UMNR passenger$")
	public void user_selects_the_one_umnr_passenger() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.select_adults)));
		driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("0");
		driver.findElement(By.xpath(PassengerinfoPage.select_child)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.select_child)).sendKeys("1");

	}

	@Then("^user selects the service animal$")
	public void user_selects_the_service_animal() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_service_animal)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_service_animal)).click();

	}

	@Then("^user enters the primary passenger child information$")
	public void user_enters_the_primary_passenger_child_information() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.DOWN);
		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)).sendKeys(Keys.ENTER);

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("jhonson");
		log.info("user successfully enters the primary child information");

	}

	@Then("^user selects the hearing disability and vision disability$")
	public void user_selects_the_hearing_disability_and_vision_disability() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_visiondisability)));
		driver.findElement(By.xpath(PassengerinfoPage.checkin_visiondisability)).click();
		driver.findElement(By.xpath(PassengerinfoPage.checkin_Heraing_disability)).click();

	}

	@Then("^user selects the wheel chair and completely immobile$")
	public void user_selects_the_wheel_chair_and_completely_immobile() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.checkinwheelchair_to_from_gate)).click();
		driver.findElement(By.xpath(PassengerinfoPage.checkin_completely_immobile)).click();
		driver.findElement(By.xpath(PassengerinfoPage.checkin_has_own_wheelchair)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.checkin_wheelchair_selection)));
		s.selectByVisibleText("Battery Powered Wet Cell Battery");

	}

	@Then("^user enters the primary passenger information with SR$")
	public void user_enters_the_primary_passenger_information_with_sr() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flayer");
		Select s1 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_passeneger_suffix)));
		s1.selectByVisibleText("Sr.");
		log.info("user succesfully select the  suffix as a SR");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("07/12/1989");

		log.info("user succesfully enter the primary passengers information");

	}

	@Then("^user enters the personal information of adult two with JR$")
	public void user_enters_the_personal_information_of_adult_two_with_jr() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("flayer");

		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("12/12/1994");

		Select s1 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_passenger_suffix)));
		s1.selectByVisibleText("Jr.");
		log.info("user succesfully select the  suffix as a JR");
		log.info("user successfully enters the primary child information");

	}

	@Then("^user enters the primary passenger with known traveler number and service animal$")
	public void user_enters_the_primary_passenger_with_known_traveler_number_and_service_animal() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flayer");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("07/12/1989");
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("135707280");
		log.info("user succesfully enter the free spirit number");
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user succesfully clicks on the additional services");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)));
		log.info("user succesfully selects the service animal check box");
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("user succesfully enters the primary passengers information");

	}

	@Then("^user enters the primary passenger with redress number and wheel chair$")
	public void user_enters_the_primary_passenger_with_redress_number_and_wheel_chair() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title)));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flayer");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("07/12/1989");

		driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_redress_input_box)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1234567");
		log.info("user succesfully enter the redress number");
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_own_wheel_chair)));

		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user succesfully selects the i have my own wheel chair");

	}

	@Then("^user enters the personal information of passenger two with know traveler number$")
	public void user_enters_the_personal_information_of_passenger_two_with_know_traveler_number() throws Throwable {
		Thread.sleep(2000);
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("jhonson");
		driver.findElement(By.xpath(PassengerinfoPage.child_knowntraveler_number)).sendKeys("135707280");
		log.info("user succesfully enters the know traveler number");

	}

	@When("^user clicks on the check in tab$")
	public void user_clicks_on_the_check_in_tab() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.check_in_Tab_BookPage)));
		driver.findElement(By.xpath(CheckInPage.check_in_Tab_BookPage)).click();
		log.info("user succesfully clicks on the check in tab ");
	}

	@Then("^user clicks on the where to find your confirmation code link$")
	public void user_clicks_on_the_where_to_find_your_confirmation_code_link() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(CheckInPage.Where_to_find_your_confirmation_code_link)).click();
		log.info("user succesfully clicks on the confirmation code link ");

	}

	@Then("^user switch to the popup and clicks on the Spirit link$")
	public void user_switch_to_the_popup_and_clicks_on_the_spirit_link() throws Throwable {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_spirit_link)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_spirit_link)).click();
		log.info("user succesfully clicks on the spirit link on the popup");
	}

	@Then("^user validate the text on the spirit link$")
	public void user_validate_the_text_on_teh_spirit_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_spirit_link_text_validation)));
		String s = driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_spirit_link_text_validation))
				.getText();

		log.info("spirit link text is " + s);

	}

	@Then("^user clicks on the Expedia link and validate the text$")
	public void user_clicks_on_the_expedia_link_and_validate_the_text() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_Expedia_link)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Expedia_link)).click();
		log.info("user succesfully clicsk on the expedia link on the popup");
		String s = driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Expedia_link_text_validation))
				.getText();
		log.info("expedia link text is " + s);
	}

	@Then("^user clicks on the priceline link and validate the text$")
	public void user_clicks_on_the_priceline_link_and_validate_the_text() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_Priceline_link)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Priceline_link)).click();
		log.info("user succesfully clicsk on the priceline link on the popup");
		String s = driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Priceline_link_text_validation))
				.getText();
		log.info("priceline link text is " + s);
	}

	@Then("^user clicks on the cheapoair link and validate the text$")
	public void user_clicks_on_the_cheapoair_link_and_validate_the_text() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_Cheapoair_link)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Cheapoair_link)).click();
		log.info("user succesfully clicsk on the cheapoair link on the popup");
		String s = driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_Cheapoair_link_text_validation))
				.getText();
		log.info("cheapoair link text is " + s);
	}

	@Then("^user clicks on the orbitz  link and validate the text$")
	public void user_clicks_on_the_orbitz_link_and_validate_the_text() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_popup_orbitz_link)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_orbitz_link)).click();
		log.info("user succesfully clicsk on the Orbitz link on the popup");
		String s = driver.findElement(By.xpath(CheckInPage.confirmation_code_popup_orbitz_link_text_validation))
				.getText();
		log.info("orbitz link text is " + s);

	}

	@Then("^user clicks on the close button on the popup$")
	public void user_clicks_on_the_close_button_on_the_popup() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.confirmation_code_close_button)));
		driver.findElement(By.xpath(CheckInPage.confirmation_code_close_button)).click();
		log.info("user succesfully clicks on the conformation code close button");
	}

	@Then("^user selects the Three child and one adult passengers$")
	public void user_selects_the_Three_child_and_one_adult_passengers() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("1");
//		driver.findElement(By.xpath(PassengerinfoPage.select_child)).clear();
//		driver.findElement(By.xpath(PassengerinfoPage.select_child)).sendKeys("3");
		for(int i=0; i<3; i++) {
			driver.findElement(By.xpath(PassengerinfoPage.select_child)).click();
		}
		// driver.findElement(By.xpath(BookPage.select_child)).click();
	}

	@Then("^user clicks on search button and enters the birthdate for three children$")
	public void user_clicks_on_search_button_and_enters_the_birthdate_for_three_children() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
		driver.findElement(By.xpath(BookPage.Search_button)).click();
		Thread.sleep(4000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			Thread.sleep(2000);
			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_month)));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("07/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
			driver.findElement(By.xpath(BookPage.Child1LapSeat)).click();
			log.info("user succesfully enter the birthdate and made the first child a lapchild");

			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys("08/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.ENTER);
			driver.findElement(By.xpath(BookPage.Child2WillRequireSeat)).click();
			log.info("user succesfully enter the birthdate and the second child will need a seat");

			driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys("08/12/2005");
			driver.findElement(By.xpath(BookPage.enter_child3_date_of_birth)).sendKeys(Keys.ENTER);
			log.info("user succesfully enter the birthdate for the child");
		}
	}

	@Then("^user enters the personal information for all pax$")
	public void user_enters_the_personal_information_for_all_pax() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.infant_firstname)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.infant_lastname)).sendKeys("Flyer");
		log.info("Personal information inputted for child 1");

		Select m = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_three_Title)));
		m.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_first_name)).sendKeys("Eric");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_last_name)).sendKeys("Flyer");
		log.info("Personal information inputted for child 2");

		Select b = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_four_Title)));
		b.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_first_name)).sendKeys("James");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_last_name)).sendKeys("Flyer");
		log.info("Personal information inputted for child 3");

	}

	@Then("^user login as FS member with same name as active military member$")
	public void user_login_as_FS_member_with_same_name_as_active_military_member() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("john.doe1@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Test@123");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully logged in as a free spirit member with same name as military member");

	}

	@Then("^user verifies FS Member with military is logged in$")
	public void user_verifies_FS_Member_with_military_is_logged_in() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JOHN"));
		log.info("Military FS member Log in verified");
		driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();

	}

	@Then("^verify FS member informations are read only$")
	public void verify_fs_member_informations_are_read_only() throws Throwable {
		WebElement firstname = driver.findElement(By.xpath(OnewaytripPage.passengers_first_name));
		if (firstname.isEnabled() == true) {
			log.info("Firstname information shouldn't be editable");
		}

		WebElement lastname = driver.findElement(By.xpath(OnewaytripPage.passengers_last_name));

		if (lastname.isEnabled() == true) {
			log.info("Lastname information shouldn't be editable");
		}
		WebElement dob = driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth));

		if (dob.isEnabled() == true) {
			log.info("DOB information shouldn't be editable");
		}
//		WebElement fsnumber = driver.findElement(By.xpath(PassengerinfoPage.adult_Free_spirit_memeber));
//		fsnumber.isEnabled();
//
//		if (fsnumber.isEnabled() == true) {
//			log.info("FREE SPIRIT number information shouldn't be editable");
//		}
	}

//	@Then("^User verifies LapChild DOB is autopopulated$")
//	public void User_verifies_LapChild_DOB_is_autopopulated() throws Throwable {
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
//		// Assert.assertTrue("LapChild DOB is not autopopulated",
//		// driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText().equals("12/12/2017"));
//		WebElement dob1 = driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth));
//		Date currentDate = new Date();
//		Calendar c = Calendar.getInstance();
//		c.setTime(currentDate);
//		c.add(Calendar.MONTH, -6);
//		Date lapcdob = c.getTime();
//		String dob = dateFormat.format(lapcdob);
//		String Lapdobauto = dob1.getAttribute("value");
//		if (Lapdobauto.contentEquals(dob)) {
//			log.info("Lapchild DOB is matched with autopopulated value");
//		} else {
//			log.info("Lapchild DOB is doesn't match with autopopulated");
//		}
//	}

//	@Then("^user selects the three child and one adult passengers$")
//	public void user_selects_the_three_child_and_one_adult_passengers() throws Throwable {
//
//		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
//		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
//		// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("1");
//		// driver.findElement(By.xpath(PassengerinfoPage.select_child)).clear();
//		// driver.findElement(By.xpath(PassengerinfoPage.select_child)).sendKeys("1");
//		driver.findElement(By.xpath(BookPage.select_child)).click();
//		
//	}

	@Then("^user enters the personal information of the children$")
	public void user_enters_the_personal_information_of_the_children() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Johnson");
		log.info("The child age displayed "
				+ driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText());
		// Assert.assertTrue("child birth date is not populated",
		// driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText().equals("07/17/2007"));
		// log.info("child date of birth is auto populated");

		/*driver.findElement(By.xpath(PassengerinfoPage.child2_title));
		Select s2 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child2_title)));
		s2.selectByVisibleText("Ms.");

		driver.findElement(By.xpath(PassengerinfoPage.child2_firstname)).sendKeys("Janett");
		driver.findElement(By.xpath(PassengerinfoPage.child2_lastname)).sendKeys("Johnson");
		log.info("The child age displayed "
				+ driver.findElement(By.xpath(PassengerinfoPage.child2_dateofbirth)).getText());

		driver.findElement(By.xpath(PassengerinfoPage.child3_title));
		Select s3 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child3_title)));
		s3.selectByVisibleText("Ms.");

		driver.findElement(By.xpath(PassengerinfoPage.child3_firstname)).sendKeys("Nicole");
		driver.findElement(By.xpath(PassengerinfoPage.child3_lastname)).sendKeys("Johnson");
		log.info("The child age displayed "
				+ driver.findElement(By.xpath(PassengerinfoPage.child3_dateofbirth)).getText());
*/
		log.info("user succesfully enters the child information");
	}

	@Then("^user login as FS military member and known traveler_with same name as active military member$")
	public void user_login_as_FS_military_member_and_known_traveler_with_same_name_as_active_military_member()
			throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("gknown.traveler1@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Test@123");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully logged in as a free spirit member with same name as military member");

	}

	@Then("^user enter a DOB that is over the age of 21$")
	public void user_enter_a_dob_that_is_over_the_age_of_21() throws Throwable {
		WebElement dob = driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth));
		dob.clear();
		dob.click();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -21);
		Date age = c.getTime();
		String driverage = dateFormat.format(age);
		dob.sendKeys(driverage);
		log.info("Driver's age: " + dateFormat.format(age));

	}

	@Then("^user enter a DOB that is under the age of 21$")
	public void user_enter_a_dob_that_is_under_the_age_of_21() throws Throwable {
		WebElement dob = driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth));
		dob.clear();
		dob.click();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -20);
		Date age = c.getTime();
		String driverage = dateFormat.format(age);
		dob.sendKeys(driverage);
		log.info("Driver's age: " + dateFormat.format(age));
	}
	
	 @Then("^user enters the personal information of military passenger two$")
	    public void user_enters_the_personal_information_of_military_passenger_two() throws Throwable {
	 
	        driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title));
	        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title)));
	        s.selectByVisibleText("Mr.");
	 
	        driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_first_name)).sendKeys("Dwanye");
	        driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_last_name)).sendKeys("Johnson");
	        WebElement dob = driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_dateof_birth));
	        dob.clear();
	        dob.click();
	        Calendar c = Calendar.getInstance();
	        c.add(Calendar.YEAR, -33);
	        Date age = c.getTime();
	        String birthday = dateFormat.format(age);
	        dob.sendKeys(birthday);
	        log.info("user succesfully enters the military passenger 2 information");
	        wait.until(ExpectedConditions
	                .elementToBeClickable(By.xpath(PassengerinfoPage.adult2_ActiveDutyMilitaryPersonnelCheckBox)));
	        driver.findElement(By.xpath(PassengerinfoPage.adult2_ActiveDutyMilitaryPersonnelCheckBox)).click();
	        log.info("Passenger 2 selects the active duty military personnel check box");
	 
	    }
	 
	
	 
	 @Then("^user enters the personal information of adult passenger two$")
	    public void user_enters_the_personal_information_of_adult_passenger_two() throws Throwable {
	 
	        driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title));
	        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title)));
	        s.selectByVisibleText("Mr.");
	 
	        driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_first_name)).sendKeys("Jermaine");
	        driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_last_name)).sendKeys("Cole");
	        WebElement dob = driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_dateof_birth));
//	        dob.clear();
        dob.click();
	        Calendar c = Calendar.getInstance();
	        c.add(Calendar.YEAR, -33);
	        Date age = c.getTime();
	        String birthday = dateFormat.format(age);
	        dob.sendKeys(birthday);
	        dob.sendKeys(Keys.TAB);
	        log.info("user succesfully enters the adult passenger 2 information");
	 
	    }
	 
	 
	
	  
	  @Then("^user enters the personal info and contact address for UMNR$")
		public void user_enters_the_personal_info_and_contact_address_for_UMNR() throws Throwable {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

			driver.findElement(By.xpath(OnewaytripPage.Title)).click();
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

			driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("UMNR");

			log.info("user able to enters his first name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
			log.info("user able to enters his last name");
//			driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
//			log.info("user able to enters his date of birth");
			driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
			log.info("user successfully select the primary passenger  contact checkbox ");
			driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("2800 Executive Way");
			log.info("user able to enter his address");
			driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("Mirimar");
			log.info("user able to enter his city name");
			driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33025");
			log.info("user able to enter his city zipcode");

			driver.findElement(By.xpath(OnewaytripPage.country)).click();
			Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			dropdown.selectByVisibleText("United States of America");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys("U");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys(Keys.ENTER);
			log.info("user able to selects the country name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("UMNR.flyer@spirit.com");
			log.info("user able to enters his email");
			driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("UMNR.flyer@spirit.com");
			driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
			log.info("user able to enters his phone number");
			Thread.sleep(3000);
			Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
			s1.selectByVisibleText("Florida");
			log.info("user able to select  his state");
		}

	    @Then("^user verify DOB autopopulated for UMNR$")
	    public void user_verify_dob_autopopulated() throws Throwable {
	    	String ChildDob = driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).getAttribute("value");
	    	ChildDob.equals("12/12/2006");
	    	log.info("DOB is  " + ChildDob + " and autopopulated");
	    }
		
		@Then("^user enters the personal info and contact address for two adults$")
		public void user_enters_the_personal_info_and_contact_address_for_two_adults() throws Throwable {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

			driver.findElement(By.xpath(OnewaytripPage.Title)).click();
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

			driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("joe");

			log.info("user able to enters his first name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
			log.info("user able to enters his last name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
			log.info("user able to enters his date of birth");
			
			Select S = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
			S.selectByVisibleText("Mr.");

			driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Jack");
			log.info("user able to enter first name for adult2");
			driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Flyer");
			log.info("user able to enter last name for adult2");
			driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("01/02/1981");
			log.info("user able to enter DOB for adult2");

			log.info("user succesfully enters information for second Pax");
			
			driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
			log.info("user successfully select the primary passenger  contact checkbox ");
			driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("2800 Executive Way");
			log.info("user able to enter his address");
			driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("Mirimar");
			log.info("user able to enter his city name");
			driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33025");
			log.info("user able to enter his city zipcode");

			driver.findElement(By.xpath(OnewaytripPage.country)).click();
			Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
//			Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			dropdown.selectByVisibleText("United States of America");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys("U");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys(Keys.ENTER);
			log.info("user able to selects the country name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("joe.flyer@spirit.com");
			log.info("user able to enters his email");
			driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("joe.flyer@spirit.com");
			driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
			log.info("user able to enters his phone number");
			Thread.sleep(3000);
			Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
			s1.selectByVisibleText("Florida");
			log.info("user able to select  his state");
		}
		
		@Then("^user enters the personal info that contains apostrophe and contact address$")
		public void user_enters_the_personal_info_that_contains_apostrophe_and_contact_address() throws Throwable {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

			driver.findElement(By.xpath(OnewaytripPage.Title)).click();
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

			driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("joe'll");
			log.info("user able to enters his first name with apostrophe");
			
			driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_middle_name)).sendKeys("O'CASEY");
			log.info("user able to enter middle name with apostrophe");
			
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("o'flyer");
			log.info("user able to enters his last name with apostrophe");
			driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
			log.info("user able to enters his date of birth");
			driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
			log.info("user successfully select the primary passenger  contact checkbox ");
			driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("2800 Executive Way");
			log.info("user able to enter his address");
			driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("Mirimar");
			log.info("user able to enter his city name");
			driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33025");
			log.info("user able to enter his city zipcode");

			driver.findElement(By.xpath(OnewaytripPage.country)).click();
			Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			dropdown.selectByVisibleText("United States of America");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys("U");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys(Keys.ENTER);
			log.info("user able to selects the country name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("joe.flyer@spirit.com");
			log.info("user able to enters his email");
			driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("joe.flyer@spirit.com");
			driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
			log.info("user able to enters his phone number");
			Thread.sleep(3000);
			Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
			s1.selectByVisibleText("Florida");
			log.info("user able to select  his state");
		}
		
	    @Then("^user clicks and Add Redress Number$")
	    public void user_clicks_and_add_redress_number() throws Throwable {
	       driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
	       driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1234567");
	       log.info("User is able to enter redress number");
	       
	    }
		
	    @Then("^user selects carseat$")
	    public void user_selects_carseat() throws Throwable {
	    	driver.findElement(By.xpath(PassengerinfoPage.CarSeat)).click();
	    	log.info("User successfully chosen a carseat");
	    }
		
	    @Then("^user enters the personal info and contact address for passenger who is fifthteen years old$")
	    public void user_enters_the_personal_info_and_contact_address_for_passenger_who_is_fifthteen_years_old() throws Throwable {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

			driver.findElement(By.xpath(OnewaytripPage.Title)).click();
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
			driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

			driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("Child");

			log.info("user able to enters his first name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
			log.info("user able to enters his last name");
                driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("12/12/2006");
			
			driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
			log.info("user successfully select the primary passenger  contact checkbox ");
			driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("2800 Executive Way");
			log.info("user able to enter his address");
			driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("Mirimar");
			log.info("user able to enter his city name");
			driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33025");
			log.info("user able to enter his city zipcode");

			driver.findElement(By.xpath(OnewaytripPage.country)).click();
			Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			dropdown.selectByVisibleText("United States of America");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys("U");
			// driver.findElement(By.xpath(OnewaytripPage.country)).sendKeys(Keys.ENTER);
			log.info("user able to selects the country name");
			driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("Child.flyer@spirit.com");
			log.info("user able to enters his email");
			driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("Child.flyer@spirit.com");
			driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
			log.info("user able to enters his phone number");
			Thread.sleep(3000);
			Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
			s1.selectByVisibleText("Florida");
			log.info("user able to select  his state");
	    }
	    
	    @Then("^user selects Esan for pax two$")
	    public void user_selects_esan_for_pax_two() throws Throwable {
	    	driver.findElement(By.xpath(PassengerinfoPage.child_psychiatric_support_animal)).click();
	    	log.info("user able to select ESAN for PAX 2");
	    }

	    
	    @Then("^user selects the two child and one adult passengers$")
		public void user_selects_the_two_child_and_one_adult_passengers() throws Throwable {

			driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
			for (int i = 0; i < 2; i++) {
				driver.findElement(By.xpath(BookPage.select_child)).click();
				log.info("Now child passenger count is " + (i + 1));
			}

			log.info("User selects one adult and two child passenger for trip");

		}
		
		
	    @Then("^user change flights$")
	    public void user_change_flights() throws Throwable {
		  
		  Thread.sleep(10000);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.Change_flights)));
	        String parentHandle = driver.getWindowHandle();
	        driver.findElement(By.xpath(CheckInPage.Change_flights)).click();
	        for (String winHandle : driver.getWindowHandles()) {
	            driver.switchTo().window(winHandle);
	        }
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.popup_editchkbtn)));
	        driver.findElement(By.xpath(CheckInPage.popup_editchkbtn)).click();
	        Select destination=new Select(driver.findElement(By.xpath(CheckInPage.To_destinationdrop)));
	        destination.selectByVisibleText("New Orleans, LA");
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckInPage.popup_continue)));
	        driver.findElement(By.xpath(CheckInPage.popup_continue)).click();
	        driver.switchTo().window(parentHandle);
	       
	        Thread.sleep(5000);
	        
			Assert.assertTrue("User is not navigated to Flight page",
					driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
			log.info("User is successfully navigated to Flight page after clicking on search in Book page");
	       
	    }		
			
	    @Then("^user clicks add or edit passport informations links and validate$")
		public void user_clicks_add_or_edit_passport_informations_links_and_validate() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			Assert.assertTrue("Passport Information title is not bold",
					driver.findElement(By.xpath(PaymentPage.Passport_information_pagetitle)).getCssValue("font-size")
							.equals("40px"));
			Assert.assertTrue("Country of Residence is not default of Unite States",
					driver.findElement(By.xpath(PaymentPage.Passport_information_countryof_residence)).getAttribute("value")
							.equals("US"));
			Assert.assertTrue("Issuing Country is not matched with Country of Residence",
					driver.findElement(By.xpath(PaymentPage.Passport_information_issuingcountry)).getAttribute("value")
							.equals("US"));
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).click();
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S*B&$^1234");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).click();
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			log.info("Use clicks save changes button");
			Thread.sleep(2000);
			String errormessage = driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber_err))
					.getText();
			log.info("Error message against passport number displayed as " + errormessage);
			Assert.assertTrue("Passport number error message is invalid or mismatch", errormessage
					.equals("Passport Number must contain alpha numerical values with no special characters.."));
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, 1);
			Date sysdate = c.getTime();
			String expirationdate = dateFormat.format(sysdate);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate);
			String errortext = driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate_err)).getText();
			log.info("User validated Passport numner and expiration date under travel document page");
			Calendar c1 = Calendar.getInstance();
			c1.add(Calendar.YEAR, 10);
			Date sysdate1 = c.getTime();
			String expirationdate1 = dateFormat.format(sysdate1);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate1);
			log.info("Expiration Date accepts up to 10 years in the future " + expirationdate1);
			Assert.assertTrue("Firstname is NOT autopopulated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_firstname)).isDisplayed());
			Assert.assertTrue("Lastname is NOT autopopulated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_lastname)).isDisplayed());
			log.info("User validated expiration date, Firstname & Lastname");
		}

		@Then("^user clicks add or edit passport informations links and cancel changes$")
		public void user_clicks_add_or_edit_passport_informations_links_and_cancel_changes() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			Select title = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_title)));
			title.selectByIndex(3);
			// driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).sendKeys("Dwayne");
			Select country = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_countryof_residence)));
			country.selectByIndex(3);
			Select residence = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_issuingcountry)));
			residence.selectByIndex(3);
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S1234567");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys("10/20/2025");
			driver.findElement(By.xpath(PaymentPage.Passport_information_cancelchanges)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			Assert.assertTrue("TITLE is changed", driver.findElement(By.xpath(PaymentPage.Passport_information_title))
					.getAttribute("value").equals("MR"));
			Assert.assertTrue("Country of Residence should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_countryof_residence)).getAttribute("value")
							.equals("US"));
			Assert.assertTrue("Issuing Country should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_issuingcountry)).getAttribute("value")
							.equals("US"));
			Assert.assertTrue("PassportNumber should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).getAttribute("value")
							.equals(""));
			Assert.assertTrue("Expiration Date should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).getAttribute("value")
							.equals(""));
			log.info(
					"User validated Title, MiddleName, Country of Residence, Passport Number, Issuing Country & expiration date");
		}

		@Then("^user clicks add or edit passport informations links and save changes$")
		public void user_clicks_add_or_edit_passport_informations_links_and_save_changes() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			Select title = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_title)));
			title.selectByIndex(2);
			// driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).sendKeys("Dwayne");
			Select country = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_countryof_residence)));
			country.selectByIndex(15);
			Select residence = new Select(driver.findElement(By.xpath(PaymentPage.Passport_information_issuingcountry)));
			residence.selectByIndex(15);
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S1234567");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys("10/20/2025");
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			Assert.assertTrue("TITLE is changed", driver.findElement(By.xpath(PaymentPage.Passport_information_title))
					.getAttribute("value").equals("MRS"));
			Assert.assertTrue("Country of Residence should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_countryof_residence)).getAttribute("value")
							.equals("AU"));
			Assert.assertTrue("Issuing Country should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_issuingcountry)).getAttribute("value")
							.equals("AT"));
			Assert.assertTrue("PassportNumber should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).getAttribute("value")
							.equals("S1234567"));
			Assert.assertTrue("Expiration Date should not be updated",
					driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).getAttribute("value")
							.equals("10/20/2025"));
			log.info(
					"User validated Title, MiddleName, Country of Residence, Passport Number, Issuing Country & expiration date after updates");
		}

		@Then("^user clicks add or edit passport informations links and verify middle name popup$")
		public void user_clicks_add_or_edit_passport_informations_links_and_verify_middle_name_popup() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys("10/23/2025");
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			Thread.sleep(3000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				driver.findElement(By.xpath(PaymentPage.Passport_middlename_remainder_popup_updatebtn)).click();
				driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).sendKeys("Dwayne");
				driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
				log.info("User validated MiddleName and Middlename popup");
			}
		}

		@Then("^user clicks add or edit passport informations links and proceed without middlename$")
		public void user_clicks_add_or_edit_passport_informations_links_and_proceed_without_middlename() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys("10/23/2025");
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			Thread.sleep(3000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				driver.findElement(By.xpath(PaymentPage.Passport_middlename_remainder_popup_proceed_without)).click();
				log.info("User validated Middlename popup and proceeded without middlename");
			}
		}

		@Then("^user clicks add or edit passport informations links and validate passport expiration date one day after return day$")
		public void user_clicks_add_or_edit_passport_informations_links_and_validate_passport_expiration_date_one_day_after_return_day()
				throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S1234567");
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, 2);
			Date sysdate = c.getTime();
			String expirationdate = dateFormat.format(sysdate);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate);
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).click();
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).clear();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Passport_information_expirationdate_err)));
			String errortext = driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate_err)).getText();
			Assert.assertTrue("Invalid error message at expiration date", errortext.equals("Expiration Date is required"));
			Calendar c1 = Calendar.getInstance();
			c1.add(Calendar.DATE, 3);
			Date sysdate1 = c1.getTime();
			String expirationdate1 = dateFormat.format(sysdate1);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate1);
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			log.info("User validated Expiration date of Passport one day after return day");
		}

		@Then("^user clicks add or edit passport informations links and verify middle name with apostrophe$")
		public void user_clicks_add_or_edit_passport_informations_links_and_verify_middle_name_with_apostrophe()
				throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			Assert.assertTrue("Middle Name is not displayed",
					driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).isDisplayed());
			driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_middlename)).sendKeys("Dwa'yne");
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S1234567");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys("10/23/2025");
			log.info("User validated MiddleName with apostrophe");
		}

		@Then("^user clicks add or edit passport informations links and verify timatic passport popup$")
		public void user_clicks_add_or_edit_passport_informations_links_and_verify_timatic_passport_popup()
				throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S123S567");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).clear();
			Calendar c1 = Calendar.getInstance();
			c1.add(Calendar.DATE, 90);
			Date sysdate1 = c1.getTime();
			String expirationdate1 = dateFormat.format(sysdate1);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate1);
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(PaymentPage.Passport_expired_popup_title)));
				log.info("Passport Expired Date popup title is "
						+ driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_title)).getText());
				Assert.assertTrue("User not validated Uh-Oh! popup",
						driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_title)).getText().equals("Uh-oh!"));
				driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_okbtn)).click();
				log.info("User clicks ok button from expired date popup");
			}
			driver.switchTo().window(parentHandle);
			log.info("User validated passport within 90 days from expiring");
		}

		@Then("^user clicks add or edit passport informations links and verify timatic passport popup multi$")
		public void user_clicks_add_or_edit_passport_informations_links_and_verify_timatic_passport_popup_multi()
				throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)));
			driver.findElement(By.xpath(PaymentPage.Trip_summary_Add_Edit_Passport_Information)).click();
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/my-trips/travel-document"));
			Assert.assertTrue("User not navigated to travel document page",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/my-trips/travel-document"));
			log.info("User is navigated to travel document/passport information page");
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).clear();
			driver.findElement(By.xpath(PaymentPage.Passport_information_passportnumber)).sendKeys("S123S567");
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).clear();
			Calendar c1 = Calendar.getInstance();
			c1.add(Calendar.DATE, 50);
			Date sysdate1 = c1.getTime();
			String expirationdate1 = dateFormat.format(sysdate1);
			driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate)).sendKeys(expirationdate1);
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PaymentPage.Passport_information_savechanges)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(PaymentPage.Passport_expired_popup_title)));
				log.info("Passport Expired Date popup title is "
						+ driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_title)).getText());
				Assert.assertTrue("User not validated Uh-Oh! popup",
						driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_title)).getText().equals("Uh-oh!"));
				driver.findElement(By.xpath(PaymentPage.Passport_expired_popup_okbtn)).click();
				log.info("User clicks ok button from expired date popup");
			}
			driver.switchTo().window(parentHandle);
			log.info("Border color returns " + driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate))
					.getCssValue("border-color"));
			Assert.assertTrue("Expiration Date border is not highlighed with red box (should be rgb(220, 53, 69)",
					driver.findElement(By.xpath(PaymentPage.Passport_information_expirationdate))
							.getCssValue("border-color").equals("rgb(220, 53, 69"));
			log.info(
					"User validated Expiration Date border is highlighed with red box around the passports that are within 90 days");
		}

		@Then("^user validated station advisory at checkin page$")
		public void user_validated_station_advisory_at_checkin_page() throws Throwable {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(PaymentPage.Trip_summary_travel_advisory_content)));
			log.info("Travel advisory contents displays as "
					+ driver.findElement(By.xpath(PaymentPage.Trip_summary_travel_advisory_content)).getText());
			Assert.assertTrue("User not validated Uh-Oh! popup",
					driver.findElement(By.xpath(PaymentPage.Trip_summary_travel_advisory_content)).getText()
							.equals("Detroit (DTW) Guests: Arrive early due to long lines, learn more"));
			WebElement talink = driver.findElement(By.xpath(PaymentPage.Trip_summary_travel_advisory_link));
			log.info("Travel advisory actual URL is " + talink.getAttribute("href"));
			Assert.assertTrue("Travel advisory link is Invalid or mismatch",
					talink.getAttribute("href").equals("http://qaepic01.spirit.com/travel-advisory"));
			log.info("Travel advisory link is validated");
			talink.click();
			// ArrayList<String> tabs = new
			// ArrayList<String>(driver.getWindowHandles());
			// driver.switchTo().window(tabs.get(1));
			Thread.sleep(2000);
			String url = driver.getCurrentUrl();
			Assert.assertTrue("User navigated to invalid travel advisory URL",
					url.equals("http://qaepic01.spirit.com/travel-advisory"));
			log.info("user navigated to " + url);
			driver.navigate().back();
			// driver.switchTo().window(tabs.get(0));
			wait.until(ExpectedConditions.urlMatches("http://qaepic01.spirit.com/check-in/reservation-summary"));
			Assert.assertTrue("User navigated to invalid travel advisory URL",
					driver.getCurrentUrl().equals("http://qaepic01.spirit.com/check-in/reservation-summary"));
			log.info("user succesfully navigate back to trip summary page ");

		}

		@Then("^user login as 9DFC member$")
		public void user_login_as_9DFC_member() throws Throwable {

			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys(NineFC_valid_emailaddress);

			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys(NineFC_Valid_Password);

			driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

			log.info("user successfully logged in as a $9 Fare Club Pricing member");

		}

		@Then("^user Click the Why can't you edit your name link and verify modal popup$")
		public void user_click_the_why_cant_you_edit_your_name_link_and_verify_modal_popup() throws Throwable {
			Thread.sleep(2000);
			Assert.assertTrue("Passenger Edit name element is not displayed",
					driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_name_link)).isDisplayed());
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Passenger_edit_name_link)));
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_name_link)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)));
				Assert.assertTrue("Passenger edit name popup header display is not flex style",
						driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)).getCssValue("display")
								.equals("flex"));
				log.info("Passenge Edit name popup header style is validated as flex");
				log.info(driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_content)).getText());
				Assert.assertTrue("Passenger edit name popup is Invalid or Mismatch content",
						driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_content)).getText().equals(
								"You are signed in as a $9 Fare Club member and we have therefore pre-filled the form. If you want to enter another name, please log out and then log in with a different, standard account or register for a new one."));
				log.info("Passenge Edit name popup is validated");
				driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_closebtn)).click();
				;
				log.info("User clicks close X and Passenge Edit name popup is closed");
			}
			driver.switchTo().window(parentHandle);
			String parentHandle1 = driver.getWindowHandle();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Passenger_edit_name_link)));
			Thread.sleep(2000);
			driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_name_link)).click();
			for (String winHandle1 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle1);
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)));
				Assert.assertTrue("Passenger edit name popup header display is not flex style",
						driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)).getCssValue("display")
								.equals("flex"));
				log.info("Passenge Edit name popup header style is validated as flex");
				Assert.assertTrue("Passenger eit name popup is Invalid or Mismatch content",
						driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_content)).getText().equals(
								"You are signed in as a $9 Fare Club member and we have therefore pre-filled the form. If you want to enter another name, please log out and then log in with a different, standard account or register for a new one."));
				log.info("Passenge Edit name popup is validated");
				driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_close_link)).click();
				;
				log.info("User clicks close button and Passenge Edit name popup is closed");
			}
			driver.switchTo().window(parentHandle1);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Passenger_edit_name_link)));
			Thread.sleep(2000);
			String parentHandle2 = driver.getWindowHandle();
			driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_name_link)).click();
			for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle2);
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)));
				Assert.assertTrue("Passenger edit name popup header display is not flex style",
						driver.findElement(By.xpath(PassengerinfoPage.Passenger_edit_popup_header)).getCssValue("display")
								.equals("flex"));
				log.info("Passenge Edit name popup header style is validated as flex");
				Assert.assertTrue("Passenger eit name popup is Invalid or Mismatch content",
						driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_content)).getText().equals(
								"You are signed in as a $9 Fare Club member and we have therefore pre-filled the form. If you want to enter another name, please log out and then log in with a different, standard account or register for a new one."));
				log.info("Passenge Edit name popup is validated");
				driver.findElement(By.xpath(PassengerinfoPage.Passengername_edit_popup_logout)).click();
				log.info("User clicks logout button and Passenge Edit name popup is closed");
			}
			driver.switchTo().window(parentHandle2);

		}

		@Then("^user enters the personal informations of passenger two of FS member$")
		public void user_enters_the_personal_information_of_passenger_two_of_fs_member() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.adullt2_Title)));
			Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title)));
			s.selectByVisibleText("Mr.");
			driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_first_name)).sendKeys("Joe");
			driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_last_name)).sendKeys("Flyer");
			WebElement dob = driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_dateof_birth));
			dob.clear();
			dob.click();
			Calendar c = Calendar.getInstance();
			c.add(Calendar.YEAR, -33);
			Date age = c.getTime();
			String paxtwoage = dateFormat.format(age);
			dob.sendKeys(paxtwoage);
			log.info("User enters PAX 2 informations");
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).sendKeys("1000219610");
			log.info("User enters invalid FS member of PAX 2");
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Assert.assertTrue("Error message of Invalid FS member is Invalid or mismatch",
					driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).getText().equals(
							"The customer name and the Free Spirit® number do not match. Please update the form with the customer's Free Spirit® number, or remove it completely if you don't know it."));
			log.info("Error message for Invalid FS number is validated");
			driver.switchTo().window(parentHandle);
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).click();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).sendKeys(NineFC_Valid_Fsnumber);
			String parentHandle1 = driver.getWindowHandle();
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			Assert.assertTrue("Error message of Invalid FS member is Invalid or mismatch",
					driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).getText().equals(
							"The customer name and the Free Spirit® number do not match. Please update the form with the customer's Free Spirit® number, or remove it completely if you don't know it."));
			log.info("Error message for valid FS number of NineDFC's is validated in PAX2");
			driver.switchTo().window(parentHandle1);
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).click();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).sendKeys("123456789");
			String parentHandle2 = driver.getWindowHandle();
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			Assert.assertTrue("Error message of Invalid FS member is Invalid or mismatch",
					driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).getText().equals(
							"The customer name and the Free Spirit® number do not match. Please update the form with the customer's Free Spirit® number, or remove it completely if you don't know it."));
			log.info("Error message for 9 digit FS number is validated");
			driver.switchTo().window(parentHandle2);
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).click();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.adult2_free_spirit_number)).sendKeys("12345678901");
			String parentHandle3 = driver.getWindowHandle();
			driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengerpage_alert)));
			Assert.assertTrue("Error message of Invalid FS member is Invalid or mismatch",
					driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).getText().equals(
							"The customer name and the Free Spirit® number do not match. Please update the form with the customer's Free Spirit® number, or remove it completely if you don't know it."));
			log.info("Error message for more than 10 digit FS number is validated");
			driver.switchTo().window(parentHandle3);
			log.info("Error messages of Invalid FS number of PAX2 are validated");
		}

		@Then("^user selects the three child and one adult passengers$")
		public void user_selects_the_three_child_and_one_adult_passengers() throws Throwable {

			driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
			// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).clear();
			// driver.findElement(By.xpath(PassengerinfoPage.select_adults)).sendKeys("1");
			// driver.findElement(By.xpath(PassengerinfoPage.select_child)).clear();
			// driver.findElement(By.xpath(PassengerinfoPage.select_child)).sendKeys("1");
			driver.findElement(By.xpath(BookPage.select_child)).click();
			driver.findElement(By.xpath(BookPage.select_child)).click();
			driver.findElement(By.xpath(BookPage.select_child)).click();
			log.info("User selects 1 ADT PAX and 3 Child PAX");
			log.info("ADT CHD Content displays as "
					+ driver.findElement(By.xpath(BookPage.ADT_CHD_popup_Content)).getText());
			Assert.assertTrue("ADT CHD Modal popup content is not valid or mismatch in Book tab",
					driver.findElement(By.xpath(BookPage.ADT_CHD_popup_Content)).getText().equals("Ages 0-17"));
			log.info("ADT CHD modal popup Content is validated");
		}

		
		@Then("^user clicks on search button and enters age 14 then accept UMNR popup$")
		public void user_clicks_on_search_button_and_enters_age_14_then_accept_UMNR_popup() throws Throwable {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
			driver.findElement(By.xpath(BookPage.Search_button)).click();
			Thread.sleep(4000);
			String parentHandle = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);}
				Thread.sleep(2000);
				// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_month)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
				Calendar c = Calendar.getInstance();
				c.add(Calendar.YEAR, -14);
				Date sysdate = c.getTime();
				String age = dateFormat.format(sysdate);
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(age);
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
				driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
				log.info("user succesfully entered age 14");
				for (String winHandle1 : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle1);}
					Thread.sleep(2000);
					driver.findElement(By.xpath(OnewaytripPage.UMNR_Popup_Accept_button)).click();
					log.info("user succesfully accepts UNMR fee");
					driver.switchTo().window(parentHandle);
			}
		
		 @Then("^User enters Info For Second Passenger Adult$")
		    public void user_enters_info_for_second_passenger_adult() throws Throwable {
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
				driver.findElement(By.xpath(PassengerinfoPage.child_title));
				Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
				s.selectByVisibleText("Mr.");

				driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("dwanye");
				driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("jason");

				driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("1/1/2014");
		    }
		    
		    
		    @Then("^User enters Info for Third Passenger Child$")
		    public void user_enters_info_for_third_passenger_child() throws Throwable {
		    	wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_three_Title)));
				driver.findElement(By.xpath(PassengerinfoPage.adullt_three_Title));
		        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_three_Title)));
		        s.selectByVisibleText("Mr.");
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_passengers_three_first_name)));
		        driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_first_name)).sendKeys("Jackson");
		        driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_last_name)).sendKeys("Flyer");
		        log.info("The child age displayed "
		                + driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getAttribute("value"));
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_passengers_three_dateof_birth)));
		         Assert.assertTrue("child birth date is not populated",
		         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_dateof_birth)).getAttribute("value").equals("01/01/2018"));
		         log.info("child date of birth is auto populated and validated");
		        
		        log.info("user succesfully enters the child information");
		    }
			
		    @Then("^User In The Booking Path Erases the First Name And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_first_name_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).clear();
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The First Name is required  Field", driver
						.findElement(By.xpath(OnewaytripPage.First_Name_Required)).getText().equals("First Name is required"));
				log.info("User succesfully validated the First Name is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).sendKeys("jack");
				log.info("user able to change the first name");
			}

			@Then("^User In The Booking Path Erases the Last Name And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_last_name_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Last Name is required  Field", driver
						.findElement(By.xpath(OnewaytripPage.Last_Name_Required)).getText().equals("Last Name is required"));
				log.info("User succesfully validated the Last Name is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("Flyer");
				log.info("user able to change the last name");
			}

			@Then("^User In The Booking Path Erases the Address And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_address_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Address_field)).clear();
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Address is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.Address_Required)).getText()
								.equals("Address (Number and Street, Optional Apartment/Unit/P.O. box/etc.) is required"));
				log.info("User succesfully validated the Address is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.Address_field)).sendKeys("4520 sw 42 terrace");
				log.info("user able to change the address");
			}

			@Then("^User In The Booking Path Erases the City And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_city_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.City_field)).clear();
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The City is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.City_Required)).getText().equals("City is required"));
				log.info("User succesfully validated the City is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.City_field)).sendKeys("Dania Beach");
				log.info("user successfully changed the city");
			}

			@Then("^User In The Booking Path Erases the State and Zip Code And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_state_and_zip_code_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				Select Country1 = new Select(driver.findElement(By.xpath(OnewaytripPage.Country_field)));
				Country1.selectByVisibleText("Afghanistan");
				log.info("User able to select the Country");
				Select Country2 = new Select(driver.findElement(By.xpath(OnewaytripPage.Country_field)));
				Country2.selectByVisibleText("United States of America");
				log.info("User able to select the Country");
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The State is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.State_Required)).getText().equals("State is required"));
				log.info("User succesfully validated the State is required  Field");
				Assert.assertTrue("User could not validate The Zip / Postal Code is required  is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.Zip_Code_Required)).getText()
								.equals("Zip / Postal Code is required"));
				log.info("User succesfully validated the Zip / Postal Code is required  is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.ZipCode_field)).sendKeys("33314");
				Select Country3 = new Select(driver.findElement(By.xpath(OnewaytripPage.State_field)));
				Country3.selectByVisibleText("Florida");
				log.info("user able to select state");
				log.info("user able to change the zip code");
			}

			@Then("^User In The Booking Path Erases the Email and Email Confirmation And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_the_booking_path_erases_the_email_and_email_confirmation_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Email_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.ConfirmEmail_field)).clear();

				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Email is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.Email_Required)).getText().equals("Email is required"));
				log.info("User succesfully validated the Confirm Email is required is required  Field");
				Assert.assertTrue("User could not validate The Email is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.Email_Confirmation_Code_Required)).getText()
								.equals("Confirm Email is required"));
				log.info("User succesfully validated the Confirm Email is required  Field");
			}

			@Then("^User In Booking Path Erases the Date Of Birth And Attempts To Continue And Is Promtped By A Required Field$")
			public void user_in_booking_path_erases_the_date_of_birth_and_attempts_to_continue_and_is_promtped_by_a_required_field()
					throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).clear();
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Date Of Birth is required  Field",
						driver.findElement(By.xpath(OnewaytripPage.Date_Of_Birth_is_Required)).getText()
								.equals("Date of Birth is required"));
				log.info("User succesfully validated the Date Of Birth is required  Field");
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
				log.info("user able to change the Date Of Birth");
			}

			@Then("^User In The Booking Path Enters the Contact First Name$")
			public void user_in_the_booking_path_enters_the_contact_first_name() throws Throwable {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)));
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)).sendKeys("Jack");
				log.info("User is Able To Enter Confirmation First Name");
			}

			@Then("^User In the Booking Path Enters The Contact Last Name$")
			public void user_in_the_booking_path_enters_the_contact_last_name() throws Throwable {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)));
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)).sendKeys("Flyer");
				log.info("User is Able To Enter Confirmation Last Name");
			}

			@Then("^User In The Booking Path Clicks The Primary Passenger Is The Contact Person Button$")
			public void user_in_the_booking_path_clicks_the_primary_passenger_is_the_contact_person_button() throws Throwable {
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)));
				driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
				log.info("user successfully select the primary passenger  contact checkbox ");
			}

			@Then("^User In The Booking Path Erases The Contact Address Field$")
			public void user_in_the_booking_path_erases_the_contact_address_field() throws Throwable {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Address_field)));
				driver.findElement(By.xpath(OnewaytripPage.Address_field)).clear();
				log.info("User is Able To Clear The Address Field");
				Thread.sleep(4000);
			}

			@Then("^User In Booking Path Enters An Invalid First Name$")
			public void user_in_booking_path_enters_an_invalid_first_name() throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).sendKeys("2PAC");
				Assert.assertTrue("User could not validate The Invalid name characters entered",
						driver.findElement(By.xpath(OnewaytripPage.Invalid_Name_Characters_Entered)).getText()
								.equals("Invalid name characters entered."));
				log.info("User succesfully validated The Invalid name characters entered");
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.First_Name_field)).sendKeys("Jack");
				log.info("user able to change the first name");
			}

			@Then("^User In Booking Path Enters An Invalid Last Name$")
			public void user_in_booking_path_enters_an_invalid_last_name() throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("K$SHA");
				Assert.assertTrue("User could not validate The Invalid name characters entered",
						driver.findElement(By.xpath(OnewaytripPage.Invalid_Name_Characters_Entered)).getText()
								.equals("Invalid name characters entered."));
				log.info("User succesfully validated The Invalid name characters entered");
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("jack");
				log.info("user able to change the first name");
			}

			@Then("^User In Booking Path Enters An Invalid Date Of Birth$")
			public void user_in_booking_path_enters_an_invalid_date_of_birth() throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).clear();
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("123456789");
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Incorrect date format",
						driver.findElement(By.xpath(OnewaytripPage.Invalid_Name_Characters_Entered)).getText()
								.equals("Invalid name characters entered."));
				log.info("User succesfully validated The Incorrect date format");
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Last_Name_field)).sendKeys("08/12/1989");
				log.info("user able to change the first name");
			}

			@Then("^User In Booking Path Enters An Invalid Redress Number$")
			public void user_in_booking_path_enters_an_invalid_redress_number() throws Throwable {
				driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
				driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("@Rickyrose");
				Assert.assertTrue("User could not validate The Your Redress Number contains invalid characters",
						driver.findElement(By.xpath(OnewaytripPage.Your_Redress_Number_Contains_Invalid)).getText()
								.equals("Your Redress Number contains invalid characters"));
				log.info("User succesfully validated The Your Redress Number contains invalid characters");
				driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).clear();
				driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
			}

			@Then("^User In Booking Path Enters An Invalid Contact Info First Name$")
			public void user_in_booking_path_enters_an_invalid_contact_info_first_name() throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)).sendKeys("2PAC");
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Invalid name characters entered",
						driver.findElement(By.xpath(OnewaytripPage.Invalid_Name_Characters_Entered)).getText()
								.equals("Invalid name characters entered."));
				log.info("User succesfully validated The Invalid name characters entered");
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_First_Name)).sendKeys("Jack");
				log.info("user able to change the first name");
			}

			@Then("^User In Booking Path Enters An Invalid Contact Info Last Name$")
			public void user_in_booking_path_enters_an_invalid_contact_info_last_name() throws Throwable {
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)).sendKeys("2PAC");
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
				driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button)).click();
				Assert.assertTrue("User could not validate The Invalid name characters entered",
						driver.findElement(By.xpath(OnewaytripPage.Invalid_Name_Characters_Entered)).getText()
								.equals("Invalid name characters entered."));
				log.info("User succesfully validated The Invalid name characters entered");
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)).clear();
				driver.findElement(By.xpath(OnewaytripPage.Passenger_Contact_Last_Name)).sendKeys("Flyer");
				log.info("user able to change the first name");
			}

			
			private static final DateFormat dateFormat1 = new SimpleDateFormat("MMddyyyyHHSS");
			@Then("^User Enters Personal Info and Contact Address with a Dynamic Email to Validate Free Spirit Sign Up$")
			public void user_enters_personal_info_and_contact_address_with_a_dynamic_email_to_validate_free_spirit_sign_up()
					throws Throwable {
				Thread.sleep(5000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

				driver.findElement(By.xpath(OnewaytripPage.Title)).click();
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

				driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("jack");
				log.info("user able to enters his first name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
				log.info("user able to enters his last name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
				log.info("user able to enters his date of birth");
				driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
				log.info("user successfully select the primary passenger  contact checkbox ");
				driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
				log.info("user able to enter his address");
				driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
				log.info("user able to enter his city name");
				driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
				log.info("user able to enter his city zipcode");

				driver.findElement(By.xpath(OnewaytripPage.country)).click();
				Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				dropdown.selectByVisibleText("United States of America");
				log.info("user able to selects the country name");

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, 0);
				Date sysdate = c.getTime();
				String emailid = dateFormat1.format(sysdate);
				driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys(emailid+"@spirit.com");
				log.info("user able to enters his email");
				String emailaddress = driver.findElement(By.xpath(OnewaytripPage.passengers_email)).getAttribute("value");
				driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys(emailaddress);

				driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
				log.info("user able to enters his phone number");
				Thread.sleep(3000);
				Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
				s1.selectByVisibleText("Florida");
				log.info("user able to select  his state");
			}
			
			
			@Then("^User Validates the Join Free Spirit Yelow Banner at the bottom$")
			public void user_validates_the_join_free_spirit_yelow_banner_at_the_bottom() throws Throwable {
				String FreeSpiritYellowBanner = driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Yellow_Banner))
						.getText();
				log.info("Free Spirit Yellow Sign Up Banner text should be " + FreeSpiritYellowBanner);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.Join_Free_Spirit_Yellow_Banner)));
				Assert.assertTrue(
						"User Could Not Validate The Join Free Spirit Yellow Banner at the bottom of the Passenger info page",
						driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Yellow_Banner)).getText()
								.equals(FreeSpiritYellowBanner));
				log.info("User Validated The Join Free Spirit Yellow Banner at the bottom of the Passenger info page");
			}

			@Then("^User Clicks Yes I want become a Free Spirit Member$")
			public void user_clicks_yes_i_want_become_a_free_spirit_member() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Yes_Check_Box)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Yes_Check_Box)).click();
			}
			
			
			@Then("^User Enters a Valid Password and a Confirm Valid Password$")
		    public void user_enters_a_valid_password_and_a_confirm_valid_password() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@Spirit101");
				log.info("User Selects A Valis Password");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@Spirit101");
				log.info("User Confirms Valid Password");
			}
			
			
			@Then("^User Enters Personal Info and Contact Address with an Existing 9DFC Member to trigger Email Already In Use$")
		    public void user_enters_personal_info_and_contact_address_with_an_existing_9dfc_member_to_trigger_email_already_in_use() throws Throwable {
				Thread.sleep(5000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

				driver.findElement(By.xpath(OnewaytripPage.Title)).click();
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

				driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("jack");

				log.info("user able to enters his first name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
				log.info("user able to enters his last name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
				log.info("user able to enters his date of birth");
				driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
				log.info("user successfully select the primary passenger  contact checkbox ");
				driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
				log.info("user able to enter his address");
				driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
				log.info("user able to enter his city name");
				driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
				log.info("user able to enter his city zipcode");

				driver.findElement(By.xpath(OnewaytripPage.country)).click();
				Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				dropdown.selectByVisibleText("United States of America");
				log.info("user able to selects the country name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("jamalflyer9dfc@spirit.com");
				log.info("user able to enters his email");
				driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("jamalflyer9dfc@spirit.com");
				driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
				log.info("user able to enters his phone number");
				Thread.sleep(3000);
				Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
				s1.selectByVisibleText("Florida");
				log.info("user able to select  his state");
			}
			
			
			@Then("^User Enters Personal Info and Contact Address with an Existing Free Spirit Member to trigger Email Already In Use$")
		    public void user_enters_personal_info_and_contact_address_with_an_existing_free_spirit_member_to_trigger_email_already_in_use() throws Throwable {
				Thread.sleep(5000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Title)));

				driver.findElement(By.xpath(OnewaytripPage.Title)).click();
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.DOWN);
				driver.findElement(By.xpath(OnewaytripPage.Title)).sendKeys(Keys.ENTER);

				driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("jack");

				log.info("user able to enters his first name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("flyer");
				log.info("user able to enters his last name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
				log.info("user able to enters his date of birth");
				driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
				log.info("user successfully select the primary passenger  contact checkbox ");
				driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
				log.info("user able to enter his address");
				driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
				log.info("user able to enter his city name");
				driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
				log.info("user able to enter his city zipcode");

				driver.findElement(By.xpath(OnewaytripPage.country)).click();
				Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
				dropdown.selectByVisibleText("United States of America");
				log.info("user able to selects the country name");
				driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("acanales@spirit.com");
				log.info("user able to enters his email");
				driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("acanales@spirit.com");
				driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
				log.info("user able to enters his phone number");
				Thread.sleep(3000);
				Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
				s1.selectByVisibleText("Florida");
				log.info("user able to select  his state");
			}
			
			
			@Then("^User Validates That USA Is Selected By Default In Country Drop Down Menu$")
		    public void user_validates_that_usa_is_selected_by_default_in_country_drop_down_menu() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.country)));
				Assert.assertTrue("Country Field is Not Defaulted to USA", 
				driver.findElement(By.xpath(PassengerinfoPage.country)).getAttribute("value").equals("US"));
				log.info("Country Field Is Defaulted To USA");
			}
			
			
			@Then("^User Validates State and ZipCode Are Required Fields By Checking For The Red Asterisk$")
		    public void user_validates_state_and_zipcode_are_required_fields_by_checking_for_the_red_asterisk() throws Throwable {
				    	WebElement StateLableRedStar = driver.findElement(By.xpath(PassengerinfoPage.Passengers_State_Label_Title));
				    	WebElement ZipLableRedStar = driver.findElement(By.xpath(PassengerinfoPage.Zip_Code_Label_Title));
						
						Assert.assertTrue("State Label Does Not Contain Asterik",
								StateLableRedStar.getAttribute("class").equals("ng-star-inserted asterisk"));
								log.info("User Validates State Label Contains The Red Asterik");
								
						Assert.assertTrue("Country Label Does Not Contain Asterik",
								ZipLableRedStar.getAttribute("class").equals("ng-star-inserted asterisk"));
								log.info("User Successfully Validated Zip/Postal Code Contains Red Asterik");	
				    }
			
			
			@Then("^User Changes The Country To Canada$")
		    public void user_changes_the_country_to_canada() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.country)));
				driver.findElement(By.xpath(PassengerinfoPage.country)).sendKeys("Canada");
				driver.findElement(By.xpath(PassengerinfoPage.country)).sendKeys(Keys.TAB);
				log.info("User Successfully Changed The Country To Canada");
			}
			
			
			@Then("^User Changes The Country To Afghanistan$")
		    public void user_changes_the_country_to_afghanistan() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.country)));
				driver.findElement(By.xpath(PassengerinfoPage.country)).sendKeys("Afghanistan");
				log.info("User Successfully Changed The Country To Afghanistan");
			}
			
			
			@Then("^User Inputs Non Alphabetical Characters Into The State Box$")
		    public void user_inputs_non_alphabetical_characters_into_the_state_box() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Passengers_State_Foreign_Country_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.Passengers_State_Foreign_Country_Field)).sendKeys("@!#");
				driver.findElement(By.xpath(PassengerinfoPage.Passengers_State_Foreign_Country_Field)).sendKeys(Keys.TAB);
			}
			
			
			@Then("^User Validates The Only Letters Are Allowed For The State Field When Using A Foreign Country$")
		    public void user_validates_the_only_letters_are_allowed_for_the_state_field_when_using_a_foreign_country() throws Throwable {
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Passengers_State_Foreign_Country_Only_Letters_Are_Allowed_Text)));
				Assert.assertTrue("User Could Not Validate Only Letters Are Allowed Error Message",
				driver.findElement(By.xpath(PassengerinfoPage.Passengers_State_Foreign_Country_Only_Letters_Are_Allowed_Text)).getText().equals("Only letters are allowed"));
				log.info("User Successfully Validated Only Letters Are Allowed Error Message");
			}
			
			
			@Then("^User Validates State Is Required Field$")
		    public void user_validates_state_is_required_field() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.State_Required)));
				Assert.assertTrue("User Could Not Validate State Is Required Message",
				driver.findElement(By.xpath(OnewaytripPage.State_Required)).getText().equals("State is required"));
				log.info("User Sucessfully Validated State Is Required Message");
			}
			
			
			@Then("^User Validates ZipCode Is Required Field$")
		    public void user_validates_zipcode_is_required_field() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Zip_Code_Required)));
				Assert.assertTrue("User Could Not Validate Zip / Postal Code Is Required Message",
				driver.findElement(By.xpath(OnewaytripPage.Zip_Code_Required)).getText().equals("Zip / Postal Code is required"));
				log.info("User Sucessfully Validated Zip / Postal Code Is Required Message");
			}
			
			
			@Then("^User Selects TWO Adults and ONE Child$")
		    public void user_selects_two_adults_and_one_child() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
				driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
				driver.findElement(By.xpath(PassengerinfoPage.select_adults)).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
				driver.findElement(By.xpath(BookPage.select_child)).click();
			}
			
			
			@Then("^User Clicks on SEARCH FLIGHTS and makes One Child Over Four Years Old but Under Fifteen$")
		    public void user_clicks_on_search_flights_and_makes_one_child_over_four_years_old_but_under_fifteen() throws Throwable {
				String parentHandle = driver.getWindowHandle();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
				driver.findElement(By.xpath(BookPage.Search_button)).click();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
					driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("1/1/2013");
					driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
					driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();


					log.info("User Successfully Entered The Date Of Birth Of The Child");
					Thread.sleep(4000);

					for (String winHandle2 : driver.getWindowHandles()) {
						driver.switchTo().window(winHandle);

					}
				}
			}
			
			
			@Then("^User enters Info For Adult PAX Number Two$")
		    public void user_enters_info_for_adult_pax_number_two() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Two_Adult_Title_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Adult_Title_Field));
				Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Adult_Title_Field)));
				s.selectByVisibleText("Mr.");

				driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Adult_First_Name_Field)).sendKeys("dwanye");
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Adult_Last_Name_Field)).sendKeys("jason");

				driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Adult_Date_Of_Birth_Field)).sendKeys("3/6/1989");
			}
			
			
			@Then("^User Enters Info For Child Over Four Under Fifteen PAX Number Three$")
		    public void user_enters_info_for_child_over_four_under_fifteen_pax_number_three() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Three_Child_Title_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Title_Field));
		        Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Title_Field)));
		        s.selectByVisibleText("Mr.");
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Three_Child_First_Name_Field)));
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_First_Name_Field)).sendKeys("Jackson");
		        log.info("User Successfully Entered The Third PAX Child First Name");
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_First_Last_Field)).sendKeys("Flyer");
		        log.info("User Successfully Entered The Third PAX Child Last Name");
			}
		        
			
			
			@Then("^User Clicks On The Continue Button On Passenger Information Page$")
		    public void user_clicks_on_the_continue_button_on_passenger_information_page() throws Throwable {
		Thread.sleep(4000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passenerspage_Continue_button)));
		WebElement element = driver.findElement(By.xpath(OnewaytripPage.passenerspage_Continue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
			}
			
			
			@Then("^User Enters Less Then Nine Digits for Adult PAX TWO Free Spirit Field$")
		    public void user_enters_less_then_nine_digits_for_adult_pax_two_free_spirit_field() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("12345678");
		        log.info("User Entered More Than Nine Digits Into Free Spirit Field");
			}
			
			
			@Then("^User Validates The Error Message For Inputting An Improper Free Spirit Number$")
		    public void user_validates_the_error_message_for_inputting_an_improper_free_spirit_number() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Free_Spirit_Wring_Number_Error_Message)));
				Assert.assertTrue("User Could Not Validate The Free Spirit Number Error Mesaage",
				driver.findElement(By.xpath(PassengerinfoPage.Free_Spirit_Wring_Number_Error_Message)).getText().equals("The customer name and the Free Spirit® number do not match. Please update the form with the customer's Free Spirit® number, or remove it completely if you don't know it."));
				log.info("User Successfully Validated The Free Spirit Number Error Mesaage");
			}
			
			
			@Then("^User Enters More Than Ten Digits Into The Free Spirit Field$")
		    public void user_enters_more_than_ten_digits_into_the_free_spirit_field() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("1234567899");
		        log.info("User Entered More Than Ten Digits Into Free Spirit Field");
			}
			
			
			@Then("^User Enters an Aplha Numeric Free Spirit Number And Attempts To Continue$")
		    public void user_enters_an_aplha_numeric_free_spirit_number_and_attempts_to_continue() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("1A2B3C4DE");
		        log.info("User Entered More Than Ten Digits Into Free Spirit Field");	
			}
			
			
			@Then("^User Enters A Free Spirit Number Containing Symbols$")
		    public void user_enters_a_free_spirit_number_containing_symbols() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("!@#$%^&**");
		        log.info("User Entered More Than Ten Digits Into Free Spirit Field");	
			}
			
			@Then("^User Validates The Auto Populated Date Of Birth For Child Passenger Number Three$")
		    public void user_validates_the_auto_populated_date_of_birth_for_child_passenger_number_three() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Three_Child_Date_Of_Birth_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Date_Of_Birth_Field)).getAttribute("value").equals("01/01/2013");
				log.info("User Sucessfully Validated The Auto Populated Date Of Birth For Child Passenger Number 3");
			}
			
			
			
			@Then("^User Enters A Valid Free Spirit Number From Another Account$")
		    public void user_enters_a_valid_free_spirit_number_from_another_account() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("1000219485");
		        log.info("User Entered More Than Ten Digits Into Free Spirit Field");	
			}
			
			
			@Then("^User Changes The Date Of Birth To Make The Primary Passenger Under Fifteen and A UNMR$")
		    public void user_changes_the_date_of_birth_to_make_the_primary_passenger_under_fifteen_and_a_unmr() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Date_Of_Birth)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Date_Of_Birth)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Date_Of_Birth)).sendKeys("01/01/2013");
		        log.info("User Changed The Date Of Birth To Make Passenger UNMR");	
			}
			
			
			@Then("^User Validates The Young Traveler Pop Up on Pax Info Page$")
		    public void user_validates_the_young_traveler_pop_up_on_pax_info_page() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Header)));
				Assert.assertTrue("User Could Not Validate The Young Traveler Popup Header",
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Header)).getText().equals("Young Traveler"));
				log.info("User Successfully Validated The Young Traveler Popup Header");
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Message)));
				Assert.assertTrue("User Could Not Validate The Young Traveler Popup Message",
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Message)).getText().equals("It appears there is a child under the age of 15 on this booking that was originally selected as an adult. Please start over and select the correct number of adults and children."));
				log.info("User Successfully Validated The Young Traveler Popup Message");
			}
			
			
			@Then("^User Clicks Close On The Young Traveler Popup$")
		    public void user_clicks_close_on_the_young_traveler_popup() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Close)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Close)).click();
			}
			
			
			@Then("^User Clicks Return To Home Page Button On The Young Traveler Popup$")
		    public void user_clicks_return_to_home_page_button_on_the_young_traveler_popup() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Return_To_Homepage_Button)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_Info_Page_Young_Traveler_Popup_Return_To_Homepage_Button)).click();
			}
			
			
			@Then("^User Enters A Password With Less Than Eight Characters$")
		    public void user_enters_a_password_with_less_than_eight_characters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@Spirit");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@Spirit");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A Password With More Than Sixteen Characters$")
		    public void user_enters_a_password_with_more_than_sixteen_characters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@Spirit12345678");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@Spirit12345678");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Validates The Join Free Spirit Invalid Password Error Message$")
		    public void user_validates_the_join_free_spirit_invalid_password_error_message() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Invalid_Password_Error_Message)));
				Assert.assertTrue("User Could Not Validate Join Free Spirit Invalid Password Error Message",
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Invalid_Password_Error_Message)).getText().equals("Password must be at least 8 characters, no more than 16 characters, must include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
				log.info("User Successfully Validated Join Free Spirit Invalid Password Error Message");
			}
			
			
			@Then("^User Enters A Password With Only Upper and Lower Case Letters$")
		    public void user_enters_a_password_with_only_upper_and_lower_case_letters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("AlfonsoCan");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("AlfonsoCan");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A Password With Only Upper Case Letters and Special Characters$")
		    public void user_enters_a_password_with_only_upper_case_letters_and_special_characters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@ALFONSOC");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@ALFONSOC");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A Passwrod With All Lowercase Letters And Special Characters$")
		    public void user_enters_a_passwrod_with_all_lowercase_letters_and_special_characters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@alfonsoc");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@alfonsoc");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A  Password With Only Lowercase Letters$")
		    public void user_enters_a_password_with_only_lowercase_letters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("alfonsoc");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("alfonsoc");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A  Password With Only Special Characters$")
		    public void user_enters_a_password_with_only_special_characters() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("!@#$%%^&");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("!@#$%%^&");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters Two Different Passwords To Trigger Password Must Match Error$")
		    public void user_enters_two_different_passwords_to_trigger_password_must_match_error() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)));
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Select_A_Valid_Password)).sendKeys("@Matrix101");
				log.info("User Enterd Text into A Valid Password Field");
				driver.findElement(By.xpath(PassengerinfoPage.Join_Free_Spirit_Confirm_A_Valid_Password)).sendKeys("@matrix101");
				log.info("User Entered Text into A Confirms Valid Password Field");
			}
			
			
			@Then("^User Enters A Correct Free Spirit Number From A Different Person For This Booking$")
		    public void user_enters_a_correct_free_spirit_number_from_a_different_person_for_this_booking() throws Throwable {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
		        driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("1000219485");
		        log.info("User Entered A Different Persons Valid Free Spirit Number");
			}
			
			
			 @Then("^User Enters An Incorrect Free Spirit Number To Trigger An Error$")
			    public void user_enters_an_incorrect_free_spirit_number_to_trigger_an_error() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("1234567899");
				 log.info("User Entered An Invalid Free Spirit Number");
			}
			 
			 
			 @Then("^User Enters A Free Spirit Number With Less Than Nine Characters$")
			    public void user_enters_a_free_spirit_number_with_less_than_nine_characters() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("12345678");
				 log.info("User Entered An Invalid Free Spirit Number With Less Than 9 Characters");
			 }
			 
			 
			 @Then("^User Enters A Free Spirit Number With Less Than Ten Characters$")
			    public void user_enters_a_free_spirit_number_with_less_than_ten_characters() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)));
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).clear();
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Free_Spirit_Number_Field)).sendKeys("12345678999");
				 log.info("User Entered An Invalid Free Spirit Number With Less Than 9 Characters");
			 }
			 
			 
			 @Then("^User Enter An Incorrect Known Traveler Number For PAX One$")
			    public void user_enter_an_incorrect_known_traveler_number_for_pax_one() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)));
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)).clear();
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)).sendKeys("123456789");
				 log.info("User Entered An Invalid Known Traveler Number");
			 }
			 
			 
			 @Then("^User Clicks On Redress Number and Enters An Incorrect Redress Number$")
			    public void user_clicks_on_redress_number_and_enters_an_incorrect_redress_number() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_redress_number)));
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_redress_input_box)));
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1234567891234567");
				 log.info("User Entered An Invalid Redress Number");
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
			 }
			 
			 
			 @Then("^User Enters A Redress Number With More Than Twenty Five Digits$")
			    public void user_enters_a_redress_number_with_more_than_twenty_five_digits() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_redress_number)));
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_redress_input_box)));
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1234567891234567891234567");
				 log.info("User Entered An Invalid Redress Number");
				 driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
			 }
			 
			 
			 @Then("^User Enters A Redress Number With More Than Sixteen Digits$")
			    public void user_enters_a_redress_number_with_more_than_sixteen_digits() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)));
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)).clear();
				 driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Field)).sendKeys("1234567891234567891234567");
				 log.info("User Entered An Invalid Known Traveler Number");
			 }
			 
			 
			 @Then("^User Selects A Departure and Return Date Starting outside of 48Hours$")
			    public void user_selects_a_departure_and_return_date_starting_outside_of_48hours() throws Throwable {
				 driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).click();
					driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).clear();
					Calendar c = Calendar.getInstance();
					c.add(Calendar.DATE, 3);
					Date date1 = c.getTime();
					log.info("Departure date: " + dateFormat.format(date1));
					String DepartureDate = dateFormat.format(date1);
					c.setTime(date1);

					// manipulate date
					c.add(Calendar.YEAR, 0);
					c.add(Calendar.MONTH, 0);
					c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
					c.add(Calendar.HOUR, 0);
					c.add(Calendar.MINUTE, 0);
					c.add(Calendar.SECOND, 0);

					// convert calendar to date
					Date currentDatePlusOne = c.getTime();
					String ReturnDate = dateFormat.format(currentDatePlusOne);

					log.info("Return date: " + dateFormat.format(currentDatePlusOne));
					driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates))
							.sendKeys(DepartureDate + " - " + ReturnDate);
			 }
			 
			 
			 @Then("^User Clicks On The Vacation From City$")
			    public void user_clicks_on_the_vacation_from_city() throws Throwable {
				 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.From_city)));
				 	driver.findElement(By.xpath(VacationPage.From_city)).click();
				 	log.info("User Successfully Clicks On The Vacation Trip Departure City ");
				 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.vacation_select_from_city)));
				 	driver.findElement(By.xpath(VacationPage.vacation_select_from_city)).click();
				 	log.info("User Successfully Selects The Vacation Trip From City ");
			 }
			 
			 
			 @Then("^User Clicks On The Vacation To City$")
			    public void user_clicks_on_the_vacation_to_city() throws Throwable {
				 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.To_city)));
				 	driver.findElement(By.xpath(VacationPage.To_city)).click();
				 	log.info("User Successfully Clicks On  The Vacation Trip Arrival City ");
				 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.vacation_select_to_city)));
				 	driver.findElement(By.xpath(VacationPage.vacation_select_to_city)).click();
				 	log.info("User Successfully Selects The Vacation Trip Arrival City ");
			 }
			 
			 
			 @Then("^User Selects Drivers Age Twenty One to Twenty Four Years Old$")
			    public void user_selects_drivers_age_twenty_one_to_twenty_four_years_old() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.driver_age)));
				 Select driversage = new Select(driver.findElement(By.xpath(CheckInPage.driver_age)));
				 driversage.selectByIndex(1);
				 log.info("User Selects The Drivers Age of 21-24");
			 }
			 
			 
			 @Then("^User Clicks Book Car And Continues With Standard Bare Fare$")
			    public void user_clicks_book_car_and_continues_with_standard_bare_fare() throws Throwable {
				 	String parentHandle = driver.getWindowHandle(); 
			    	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Book_Car_Button)));
			    	  driver.findElement(By.xpath(VacationPage.Book_Car_Button)).click();
					 	log.info("User Successfully Clicked The Blue Book Car BUtton");
			    	  WebElement element = driver.findElement(By.xpath(VacationPage.Book_Car_Button));
			    	  JavascriptExecutor executor = (JavascriptExecutor)driver;
			    	  executor.executeScript("arguments[0].click();", element);
			    	  
					  for (String winHandle : driver.getWindowHandles()) {
					      driver.switchTo().window(winHandle); 
					  }
					  Thread.sleep(2000);
					  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.Continue_With_Bare_Fare_buttton)));
					driver.findElement(By.xpath(BagsPage.Continue_With_Bare_Fare_buttton)).click();
					log.info("user successfully clicked on CONTINUE WITH BARE FARE BUTTON");
					 driver.switchTo().window(parentHandle);
			 }
			 
			 
			 @Then("^User Selects A Primary Driver$")
			    public void user_selects_a_primary_driver() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Primary_Driver_Field)));
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).click();
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).sendKeys("jack Jr.");
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).sendKeys(Keys.TAB);
				 log.info("User Was Able To Select A Primary Driver");
			 }
			 
			 
			 @Then("^User Clears The Date Of Birth Of PAX One$")
			    public void user_clears_the_date_of_birth_of_pax_one() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passengers_dateof_birth)));
				 driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).clear();
				 log.info("User Was Able To The Date Of Birth");
			 }
			 
			 
			 @Then("^User Validates Primary Driver Is Required Text Under The Field$")
			    public void user_validates_primary_driver_is_required_text_under_the_field() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Primary_Driver_Is_Required_Error_Message)));
				 String PrimaryDriverIsRequired = driver.findElement(By.xpath(VacationPage.Primary_Driver_Is_Required_Error_Message)).getText();
				 log.info("Primary Driver Error Message Should Read " + PrimaryDriverIsRequired);
				 Assert.assertTrue("User Could Not Validate The Primary Driver Is Required Message",
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Is_Required_Error_Message)).getText().equals("Primary driver is required"));
				 log.info("User Succesfully Validated The Primary Driver Is Required Message");
			 }
			 
			 
			 @Then("^User Attempts To Select A Primary Driver Again$")
			    public void user_attempts_to_select_a_primary_driver_again() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Primary_Driver_Field)));
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).click();
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).sendKeys("jack Jr.");
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).sendKeys(Keys.TAB);
				 log.info("User Was Not Able To Select A Primary Driver");
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.Primary_Driver_Field)));
				 String PrimaryDriverIsRequired = driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).getText();
				 log.info("Primary Driver Field Should Read " + PrimaryDriverIsRequired);
				 Assert.assertTrue("User Could Not Validate The Select Primary Driver Field Reads Select Primary Driver",
				 driver.findElement(By.xpath(VacationPage.Primary_Driver_Field)).getText().equals("Select the primary driver"));
				 log.info("User Succesfully Validated The Select Primary Field Reads Select Primary Driver");
			 }
			 
			 
			 @Then("^User Enters A Date Of Birth Under Twenty One$")
			    public void user_enters_a_date_of_birth_under_twenty_one() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.passengers_dateof_birth)));
				 driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/2000");
				 log.info("User Was Able To Enter The Date Of Birth");
			 }
			 
			 
			 @Then("^User Eneters Info For Primary Passenger Along With A Pre Existing Free Spirit Email$")
			    public void user_eneters_info_for_primary_passenger_along_with_a_pre_existing_free_spirit_email() throws Throwable {
				 Thread.sleep(5000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Title)));
					driver.findElement(By.xpath(OnewaytripPage.Title));
					Select s2 = new Select(driver.findElement(By.xpath(OnewaytripPage.Title)));
					s2.selectByVisibleText("Mr.");


					driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("jack");
					log.info("user able to enters his first name");

					driver.findElement(By.xpath(OnewaytripPage.passengers_middle_name)).sendKeys("Jamal");
					log.info("user able to enter passenger middle name");

					driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).click();
					driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("Jr.");
					driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys(Keys.TAB);
					log.info("user able to enter a suffix");

					driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
					log.info("user able to enters his date of birth");

					driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("123456789");
					driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).clear();
					log.info("user able to enters a Known Traveler Number");

					driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
					log.info("user successfully select the primary passenger  contact checkbox ");

					driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
					log.info("user able to enter his address");

					driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
					log.info("user able to enter his city name");

					driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
					log.info("user able to enter his city zipcode");

					driver.findElement(By.xpath(OnewaytripPage.country)).click();
					Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
					Select s = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
					dropdown.selectByVisibleText("United States of America");
					log.info("user able to selects the country name");

					driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("ACANALES@SPIRIT.COM");
					log.info("user able to enters his email");

					driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("ACANALES@SPIRIT.COM");

					driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
					log.info("user able to enters his phone number");

					Thread.sleep(3000);
					Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
					s1.selectByVisibleText("Florida");
					log.info("user able to select  his state");
			 }
			 
			 
			 @Then("^User Verifies That User Is On Passenger Info Page$")
			    public void user_verifies_that_user_is_on_passenger_info_page() throws Throwable {
				 driver.getCurrentUrl().equals("http://qaepic01.spirit.com/book/passenger");
				 log.info("User Was Able To Verify Passenger Information Page URL ");
			 }
			 
			 
			 @Then("^User Validates Contact Check Box$")
			    public void user_validates_contact_check_box() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Primary_passenger_chk)));
				 Assert.assertTrue("User Could Not Validate The Contact Check Box",
				 driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).getText().equals("The primary passenger is the contact person"));
				 log.info("User Succesfully Validated The Contact Check Box");
			 }
			 
			 
			 @Then("^User Verifies All The Fields In Contact Box Are Auto Populated$")
			    public void user_verifies_all_the_fields_in_contact_box_are_auto_populated() throws Throwable {
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.passengers_address)));
				 Assert.assertTrue("User Could Not Validate The Auto Populated Address",
				 driver.findElement(By.xpath(PassengerinfoPage.passengers_address)).getAttribute("value").equals("603 west side"));
				 log.info("User Succesfully Validated The Auto Populated Address");
				 
				 Assert.assertTrue("User Could Not Validate The Auto Populated City",
				 driver.findElement(By.xpath(PassengerinfoPage.passengers_city)).getAttribute("value").equals("miami"));
				 log.info("User Succesfully Validated The Auto Populated City");
				 
				 Assert.assertTrue("User Could Not Validate The Auto Populated Zip Code",
				 driver.findElement(By.xpath(PassengerinfoPage.zip_code)).getAttribute("value").equals("33456"));
				 log.info("User Succesfully Validated The Auto Populated Zip Code");
				 
				 String Country = driver.findElement(By.xpath(PassengerinfoPage.country)).getAttribute("value");
				 log.info("Country Field Value Should Read " + Country);
				 Assert.assertTrue("User Could Not Validate The Auto Populated Country",
				 driver.findElement(By.xpath(PassengerinfoPage.Country_Code_United_States_of_America)).getAttribute("value").equals("US"));
				 log.info("User Succesfully Validated The Auto Populated Country");
				 
				 Assert.assertTrue("User Could Not Validate The Auto Populated Email",
				 driver.findElement(By.xpath(PassengerinfoPage.passengers_email)).getAttribute("value").equals("jack.flyer9dfc@spirit.com"));
				 log.info("User Succesfully Validated The Auto Populated Email");
				 
				 Assert.assertTrue("User Could Not Validate The Auto Populated Confirm Email",
			     driver.findElement(By.xpath(OnewaytripPage.confirm_email)).getAttribute("value").equals("jack.flyer9dfc@spirit.com"));
				 log.info("User Succesfully Validated The Auto Populated Confirm Email");
				 
				 String PassengerPhone = driver.findElement(By.xpath(PassengerinfoPage.Passengers_phone)).getAttribute("value");
				 log.info("Passenger Phone Number Field Value Should Read " + PassengerPhone);
				 Assert.assertTrue("User Could Not Validate The Auto Populated Passengers Phone Number",
				 driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).getAttribute("value").equals("123-000-0000"));
				 log.info("User Succesfully Validated The Auto Populated Passengers Phone Number");
				 
				 String CountryPhoneCode = driver.findElement(By.xpath(PassengerinfoPage.Phone_Country_Code)).getAttribute("value");
				 log.info("Country Phone Number Code Field Value Should Read " + CountryPhoneCode);
				 Assert.assertTrue("User Could Not Validate The Auto Populated Passengers Phone Country Code",
				 driver.findElement(By.xpath(PassengerinfoPage.Phone_Country_Code)).getAttribute("value").equals("0: Object"));
				 log.info("User Succesfully Validated The Auto Populated Passengers Phone Country Code");
				 
				 String State = driver.findElement(By.xpath(PassengerinfoPage.Phone_Country_Code)).getAttribute("value");
				 log.info("State Field Value Should Read " + State);
				 Assert.assertTrue("User Could Not Validate The Auto Populated Passengers State",
				 driver.findElement(By.xpath(PassengerinfoPage.Phone_Country_Code)).getAttribute("value").equals("0: Object"));
				 log.info("User Succesfully Validated The Auto Populated Passengers State");
			 }
			
			
			
			
			
			
			
			
			
			
			
}