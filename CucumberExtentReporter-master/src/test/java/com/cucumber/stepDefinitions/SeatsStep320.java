package com.cucumber.stepDefinitions;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage319;
import com.cucumber.pages.SeatsPage320;
import com.cucumber.pages.SeatsPage321;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class SeatsStep320 {

	
	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();

	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	
	@Then("^user Clicks continue without bags$") 
	public void Then_user_Clicks_continue_without_bags() throws Throwable {
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.continue_withou_bags)));
//				driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user Clicks continue without bags");
	}
	
	@Then("^user Clicks on I dont need bags on pop up$") 
	public void user_Clicks_on_I_dont_need_bags_on_pop_up() throws Throwable {
		String parentHandle = driver.getWindowHandle(); 
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath(BagsPage.continue_withou_bags));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		
		for (String winHandle : driver.getWindowHandles()) {
		    driver.switchTo().window(winHandle); 
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.popup_idont_need_bags)));
		
		WebElement element1 = driver.findElement(By.xpath(BagsPage.popup_idont_need_bags));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
				
		log.info("user Clicks on I dont need bags on pop up");
		
		driver.switchTo().window(parentHandle);
	}
	
	@Then("^user verifies seat map is Airbus ThirtyTwoB$") 
	public void user_verifies_seat_map_is_Airbus_ThirtyTwoB() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Airbus_Name)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText().equals("Airbus 32B"));
		log.info("Airbus 32B verified");
	}
	@Then("^user verifies seat map is Airbus ThirtyTwoA$") 
	public void user_verifies_seat_map_is_Airbus_ThirtyTwoA() throws Throwable {
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Airbus_Name)));
		String Airbus_Name = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
	
		
	if (Airbus_Name.equals("Airbus 32A")) {	Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText().equals("Airbus 32A"));
		log.info("Airbus 32A verified");
	}
	
	else if(Airbus_Name.equals("Airbus 32B")) {
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText().equals("Airbus 32B"));
		log.info("Airbus 32B verified");
	}
	}
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for BLND ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_BLND_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with BLND SSR!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
////        Thread.sleep(5000);
//        for (int p = 0;p<= 7; p++) {
//            String random_seatxpath[] = { SeatsPage321.Airbus_32_B_Standard_Seat_10B,SeatsPage321.Airbus_32_B_Standard_Seat_10C,SeatsPage321.Airbus_32_B_Standard_Seat_10D,SeatsPage321.Airbus_32_B_Standard_Seat_10E,SeatsPage321.Airbus_32_B_Standard_Seat_10F,SeatsPage321.Airbus_32_B_Standard_Seat_12A,SeatsPage321.Airbus_32_B_Standard_Seat_12B,SeatsPage321.Airbus_32_B_Standard_Seat_12C };
//
//            if (driver.findElement(By.xpath(random_seatxpath[p])).isEnabled() ) {
//                driver.findElement(By.xpath(random_seatxpath[p])).click();
//            }   
        
        }
       
//        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for DEAF ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_DEAF_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with DEAF ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
	}
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for POCS ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_POCS_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with POCS ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
	}
//     
	
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for UNMR ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_UNMR_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with UNMR ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
 
	}
//       
       
        
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCBD ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCBD_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with WCBD ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
       
}
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCBW ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCBW_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with WCBW ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
       
     }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCHC ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCHC_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger wtih WCHC!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
       
    }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCHR ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCHR_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with WCHR ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
      }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCHS ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCHS_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with WCHS ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
       
        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for WCMP ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_WCMP_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=11;i++) {
                String seatxpath[] = {
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F};
                Assert.assertTrue("The disabled seats are displayed for passenger with WCMP ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for CRSR ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_CRSR_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=31;i++) {
                String seatxpath[] = {
                		SeatsPage320.Airbus_32_A_BFS_1A, SeatsPage320.Airbus_32_A_BFS_1C,
                		SeatsPage320.Airbus_32_A_3A,SeatsPage320.Airbus_32_A_3B,SeatsPage320.Airbus_32_A_3C,SeatsPage320.Airbus_32_A_3D,SeatsPage320.Airbus_32_A_3E,SeatsPage320.Airbus_32_A_3F,
                		SeatsPage320.Airbus_32_A_11A,SeatsPage320.Airbus_32_A_11B,SeatsPage320.Airbus_32_A_11C,SeatsPage320.Airbus_32_A_11D,SeatsPage320.Airbus_32_A_11E,SeatsPage320.Airbus_32_A_11F,
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F,
                		SeatsPage320.Airbus_32_A_14A,SeatsPage320.Airbus_32_A_14B,SeatsPage320.Airbus_32_A_14C,SeatsPage320.Airbus_32_A_14D,SeatsPage320.Airbus_32_A_14E,SeatsPage320.Airbus_32_A_14F};
                Assert.assertTrue("The disabled seats are displayed for passenger with CRSR ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
//        driver.findElement(By.xpath(SeatsPage320.Airbus_319_Standard_Seat_10E)).click();
      //  log.info("user selects the standard seat for the child");
       
        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for ESAN ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_ESAN_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=19;i++) {
                String seatxpath[] = {
                		SeatsPage320.Airbus_32_A_BFS_1A, SeatsPage320.Airbus_32_A_BFS_1C,
                		SeatsPage320.Airbus_32_A_3A,SeatsPage320.Airbus_32_A_3B,SeatsPage320.Airbus_32_A_3C,SeatsPage320.Airbus_32_A_3D,SeatsPage320.Airbus_32_A_3E,SeatsPage320.Airbus_32_A_3F,
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F
                		};
                Assert.assertTrue("The disabled seats are displayed for passenger with ESAN ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
//        driver.findElement(By.xpath(SeatsPage320.Airbus_319_Standard_Seat_10E)).click();
       // log.info("user selects the standard seat for the child");
       
        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for INFT ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_INFT_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=19;i++) {
                String seatxpath[] = {		
                		SeatsPage320.Airbus_32_A_BFS_1A, SeatsPage320.Airbus_32_A_BFS_1C,
                		SeatsPage320.Airbus_32_A_3A,SeatsPage320.Airbus_32_A_3B,SeatsPage320.Airbus_32_A_3C,SeatsPage320.Airbus_32_A_3D,SeatsPage320.Airbus_32_A_3E,SeatsPage320.Airbus_32_A_3F,
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F
                		};
                Assert.assertTrue("The disabled seats are displayed for passenger with INFT ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
//        driver.findElement(By.xpath(SeatsPage320.Airbus_319_Standard_Seat_10E)).click();
      //  log.info("user selects the standard seat for the child");
       
        }
	
	@Then("^user verifies Blocked seats on ThirtyTwoA for SRVA ssr$") 
	public void user_verifies_Blocked_seats_on_ThreeTwenty_for_SRVA_ssr() throws Throwable {
		Thread.sleep(3000);
        String s = driver.findElement(By.xpath(SeatsPage320.Airbus_Name)).getText();
        log.info("flight name " + s);
   
        try {
            for(int i=0; i<=19;i++) {
                String seatxpath[] = {		
                		SeatsPage320.Airbus_32_A_BFS_1A, SeatsPage320.Airbus_32_A_BFS_1C,
                		SeatsPage320.Airbus_32_A_3A,SeatsPage320.Airbus_32_A_3B,SeatsPage320.Airbus_32_A_3C,SeatsPage320.Airbus_32_A_3D,SeatsPage320.Airbus_32_A_3E,SeatsPage320.Airbus_32_A_3F,
                		SeatsPage320. Airtbus_32_A_Exit_12A,SeatsPage320.Airtbus_32_A_Exit_12B,SeatsPage320.Airtbus_32_A_Exit_12C,SeatsPage320.Airtbus_32_A_Exit_12D,SeatsPage320.Airtbus_32_A_Exit_12E,SeatsPage320.Airtbus_32_A_Exit_12F,
                		SeatsPage320.Airtbus_32_A_Exit_13A,SeatsPage320.Airtbus_32_A_Exit_13B,SeatsPage320.Airtbus_32_A_Exit_13C,SeatsPage320.Airtbus_32_A_Exit_13D,SeatsPage320.Airtbus_32_A_Exit_13E,SeatsPage320.Airtbus_32_A_Exit_13F
                		};
                Assert.assertTrue("The disabled seats are displayed for passenger with SRVA ssr!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());
               
            }
        } catch (NoSuchElementException e) {
           
        }
        
        }
	
    @Then("^User choose the To city LAS$")
    public void User_choose_the_To_city() throws Throwable{
    	
    	driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
    	
//    	Random ran = new Random();
//	List<WebElement> cities = driver.findElements(By.xpath(BookPage.TO_city));
//		
//		int city=ran.nextInt(cities.size())+ 1;
//		
//		cities.get(city).click();
//		Thread.sleep(3000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_las)));
       driver.findElement(By.xpath(SeatsPage320.TO_city_las)).click();
    }
	
    @Then("^User choose the To city LGA$")
    public void User_choose_the_To_city_LGA() throws Throwable{
    	
    	driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
    	
//    	Random ran = new Random();
//	List<WebElement> cities = driver.findElements(By.xpath(BookPage.TO_city));
//		
//		int city=ran.nextInt(cities.size())+ 1;
//		
//		cities.get(city).click();
//		Thread.sleep(3000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LGA)));
       driver.findElement(By.xpath(SeatsPage320.TO_city_LGA)).click();
    }
    
	@Then("^user takes the new pnr code and clicks on the My trips button and enters the lastname and PNR$")
	public void user_takes_the_new_pnr_code_and_clicks_on_the_My_trips_button() throws Throwable {
	 
        //driver.findElement(By.xpath(PassengerinfoPage.conformation_page_popup)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.PNR_CONFORMATION)));
		String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
		String pnr = tmp.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + pnr);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage319.Mytrips_button_confromationpage)));
		driver.findElement(By.xpath(SeatsPage319.Mytrips_button_confromationpage)).click();
		log.info("Clicked on the My trips present on top banner")
		;
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.ConfirmationCode_carrot)));
		driver.findElement(By.xpath(SeatsPage320.ConfirmationCode_carrot)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
		driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
		log.info("Passenger entering the last name");
		driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
		log.info("Entered the confirmation code as : " + pnr);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.Continue_button)));
		driver.findElement(By.xpath(SeatsPage320.Continue_button)).click();
		log.info("Then user clicked on the Continue button to start Manage travel");
	}
	
	@Then("^user clicks on edit seats$")
	public void user_enters_invalid_known_traveler_number()throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.EditSeats_button)));
		driver.findElement(By.xpath(SeatsPage320.EditSeats_button)).click();
		
		log.info("user succesfully clicks on edit seats");
	}


//	@Then("^user clicks on the continue button on the seats page and switch to the popup and selects the i dont need bags$")
//    public void user_clicks_on_the_cintinue_button_on_the_seats_page_and_switch_to_the_popup_and_selects_the_i_dont_need_bags()
//            throws Throwable {
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.seatspage_continue_button)));
//        String parentHandle = driver.getWindowHandle();
//        driver.findElement(By.xpath(OnewaytripPage.seatspage_continue_button)).click();
// 
//        for (String winHandle : driver.getWindowHandles()) {
//            driver.switchTo().window(winHandle);
//        }
//        Thread.sleep(2000);
//        wait.until(
//                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)));
//        driver.findElement(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)).click();
// 
//        log.info("user succefully handled the popup and select the i dont need bags ");
// 
//    }
// 
//@When("^user lands on Extras page$")
//    public void user_lands_on_extras_page() throws Throwable {
//        wait.until(ExpectedConditions
//                .visibilityOfElementLocated(By.xpath(SeatsPage320.checkin_path_extraspage_text_validation)));
//        driver.findElement(By.xpath(SeatsPage320.checkin_path_extraspage_text_validation)).getText().equals("Extras");
//        log.info("user validates the text on the Extras page");
// 
//    }
// 
//    @Then("^user clicks on the continue button on the extras page$")
//    public void user_clicks_on_the_continue_button_on_the_extras_page() throws Throwable {
//        Thread.sleep(3000);
//        wait.until(ExpectedConditions
//                .visibilityOfElementLocated(By.xpath(SeatsPage320.checkin_path_extrapage_continue_button)));
//        driver.findElement(By.xpath(SeatsPage320.checkin_path_extrapage_continue_button)).click();
//        log.info("user succesfully clicks on the continue button on the extras page");
// 
//    }
// 
	
	
	
	
	
	@Then("^user clicks on the seats link on mytrips page$")
    public void user_clicks_on_the_seats_link_on_mytrips_page() throws Throwable {
       
		 wait.until(ExpectedConditions
              .visibilityOfElementLocated(By.xpath(SeatsPage320.Mytrips_checkin_seats_link)));
		driver.findElement(By.xpath(SeatsPage320.Mytrips_checkin_seats_link)).click();
		log.info("user clicks on the seats link after checkin to mytrips");
    }

	
	
	
	
	
	
	
	
	@Then("^user clicks the i dont need bags$")
    public void user_clicks_the_i_dont_need_bags()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)));
        driver.findElement(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup_dont_purchase_bags)).click();
 
        log.info("user succefully handled the popup and select the i dont need bags ");
 
    }
 
	
	
    @Then("^user clicks Checkin and Print Boarding Pass$")
    public void user_clicks_Checkin_and_Print_Boarding_Pass() throws Throwable {
        Thread.sleep(5000);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)));
        driver.findElement(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)).click();
        log.info("user succesfully clicks on the checkin and print boarding pass on the checkin page");
 
    }
	
	@Then("^user clicks Nope im good to pro tip pop up$")
    public void user_clicks_Nope_im_good_to_pro_tip_pop_up()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)));
        driver.findElement(By.xpath(SeatsPage320.chekin_path_seatspage_bags_popup)).click();
 
        log.info("user succefully handled the popup and select the i dont need bags ");
 
    }
	
	@Then("^user clicks Get Random Seats on choose your seats popup$")
    public void user_clicks_Get_Random_Seats_on_choose_your_seats_popup()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Choose_Your_seats_pop_up_random_button)));
        driver.findElement(By.xpath(SeatsPage320.Choose_Your_seats_pop_up_random_button)).click();
 
        log.info("user succefully selects Random Seats ");
 
    }
	
	@Then("^user clicks Accept and Print Boarding Pass on Hazmat$")
    public void user_clicks_Accept_and_Print_Boarding_Pass_on_Hazmat()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Hazmat_Popup_Accept_and_Print_button)));
        driver.findElement(By.xpath(SeatsPage320.Hazmat_Popup_Accept_and_Print_button)).click();
 
        log.info("user succefully clicks accept and print boarding pass ");
 
    }
	
	@Then("^user clicks Finish Check in$")
    public void user_clicks_Finish_Check_in()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Your_Boarding_Pass_Popup_Checkin_Button)));
        driver.findElement(By.xpath(SeatsPage320.Your_Boarding_Pass_Popup_Checkin_Button)).click();
 
        log.info("user succefully clicks finish check in ");
 
    }
	
	@Then("^user clicks No to Reserve a Car$")
    public void user_clicks_No_to_Reserve_a_Car()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.Reserve_a_Car_No_Button)));
        driver.findElement(By.xpath(SeatsPage320.Reserve_a_Car_No_Button)).click();
 
        log.info("user succefully clicks No to reserve a Car PopUp ");
 
    }
	
	
	@Then("^user clicks no to Travel Guard$")
    public void user_clicks_no_to_Travel_Guard()
            throws Throwable {
 
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Thread.sleep(2000);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TravelGuard_No_Button)));
        driver.findElement(By.xpath(SeatsPage320.TravelGuard_No_Button)).click();
 
        log.info("user succefully clicks No to Travel Guard");
 
	}
	@Then("^User Selects Battery Powered drygel cell Wheelchair$")
    public void User_Selects_Battery_Powered_drygel_cell_Wheelchair() throws Throwable {
    Select drpWheelChair = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
    drpWheelChair.selectByVisibleText("Battery Powered Dry/Gel Cell Battery");
     log.info("Battery Powered Dry/Gel Cell Battery Selected");
    }
	
	  @Then("^User Selects completely immobile$")
      public void User_Selects_completely_immobile() throws Throwable {
       driver.findElement(By.xpath(PassengerinfoPage. adult_ompletely_immobile)).click();
       log.info("user selects all completely immobile");
      }
	
	  
	  @Then("^User Selects manually powered Wheelchair$")
      public void User_Selects_manually_powered_Wheelchair() throws Throwable {
      Select drpWheelChair = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
      drpWheelChair.selectByVisibleText("Manually Powered");
       log.info("manually powered Wheelchair Selected");
      }
	  
//	   @Then("^user clicks on search button and enters the birthdate of the Lap child$")
//       public void user_enters_the_birthdate_of_the_Lap_child() throws Throwable {
//
//           String parentHandle = driver.getWindowHandle(); 
//           driver.findElement(By.xpath(BookPage.Search_button)).click(); 
//
//           for (String winHandle : driver.getWindowHandles()) {
//               driver.switchTo().window(winHandle); 
//
//               wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_month)));
//               Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_month)));
//               s.selectByVisibleText("July");
//
//               Select s1 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_day)));
//               s1.selectByVisibleText("17");
//
//               Select s2 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_year)));
//               s2.selectByVisibleText("2017");
//
//               driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
//               
//               log.info("user succesfully enter the birthdate of the lap child");
//           }
//       }
	   
	   
	   @Then("^user clicks on the CarSeat$")
	    public void user_clicks_on_the_CarSeat() throws Throwable {
	        wait.until(ExpectedConditions
	                .visibilityOfElementLocated(By.xpath(PassengerinfoPage.CarSeat)));
	        driver.findElement(By.xpath(PassengerinfoPage.CarSeat)).click();
	        log.info("user clicks on CarSeat");
	    }
	  
}
