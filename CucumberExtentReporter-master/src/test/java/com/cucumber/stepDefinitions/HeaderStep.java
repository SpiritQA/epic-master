package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.EGiftcardPage;
import com.cucumber.pages.BookPage;

import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.NineFC;
import com.cucumber.pages.PassengerinfoPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HeaderStep {
	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();

	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	@When("^User clicks on sign in and then switch to the popup$")
	public void user_clicks_on_sign_in_and_then_switch_to_the_popup() throws Throwable {
//		String parentHandle = driver.getWindowHandle();
//		Thread.sleep(5000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Homepage_sigin_button)));
//		driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).click();

		log.info("Log in pop up displays");
	}

	@Then("^User logs in with fsnumber \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_logs_in_with_fsnumber_something_something(String FSnumber, String Password) throws Throwable {
		
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EGiftcardPage.spanish_signin_button)));
		driver.findElement(By.xpath(EGiftcardPage.spanish_signin_button)).click();

		log.info("Log in pop up displays");
		
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(FSnumber);
		driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(Password);
		driver.findElement(By.xpath(BookPage.login_button_onpopup)).click();
		String Login_user_name=driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText();
		Assert.assertTrue("Not validated the members name",
				driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText().equals(Login_user_name));
		log.info("user succesfully validated the member name ");
	}

	
	 @Then("^User logs in with fsnumber for english login \"([^\"]*)\" \"([^\"]*)\"$")
	    public void user_logs_in_with_fsnumber_for_english_login_something_something(String fsnumber, String password) throws Throwable {
	        
		 String parentHandle = driver.getWindowHandle();
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Homepage_sigin_button)));
			driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).click();

			log.info("Log in pop up displays");
			
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(fsnumber);
			driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(password);
			driver.findElement(By.xpath(BookPage.login_button_onpopup)).click();
			String Login_user_name=driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText();
			Assert.assertTrue("Not validated the members name",
					driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText().equals(Login_user_name));
			log.info("user succesfully validated the member name ");
		 
	    }
	
	
	
	
	
	
	@Then("^User logs in with email \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_logs_in_with_email_something_something(String FSemail, String Password) throws Throwable {
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Homepage_sigin_button)));
		driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).click();

		log.info("Log in pop up displays");
		
		
		driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(FSemail);
		driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(Password);
		driver.findElement(By.xpath(BookPage.login_button_onpopup)).click();
		Thread.sleep(1000);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* FOR USING DYNAMIC MEMBER */
		String name = driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText();
		if (name != "SIGN-IN") {
			log.info("user succesfully validated the member name ");
		} else {
			assertTrue(driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText().equals(""));
			log.info("member not logged in correctly ");
		}

		
	}



	@Then("^User logs in with Spanish fsnumber \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_logs_in_with_spanish_fsnumber_something_something(String FSnumber, String Password)
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Homepage_sigin_button)));
		driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).click();

		log.info("Log in pop up displays");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* FOR USING DYNAMIC MEMBER */
		// //log in successful and Your FS page is displayed
		// String userName =
		// driver.findElement(By.xpath(BookPage.Homepage_sigin_button));
		//// Assert.assertNotSame("Sign-In", userName);
		//// log.info("user enters the FSnumber and password successfully; logged on ");

		/* FOR USING STATIC MEMBER (JOE FLYER) */
		

		driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(FSnumber);
		driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(Password);
		driver.findElement(By.xpath(BookPage.login_button_onpopupES)).click();
        Thread.sleep(4000);
     String s =   driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText();
		Assert.assertTrue("Not validated the members name",
				driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText().equals(s));
		log.info("user succesfully validated the member name ");

	}

	@Then("^User logs in with Spanish email \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_logs_in_with_spanish_email_something_something(String FSemail, String Password) throws Throwable {
		
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EGiftcardPage.spanish_signin_button)));
		driver.findElement(By.xpath(EGiftcardPage.spanish_signin_button)).click();

		log.info("Log in pop up displays");

	

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(BookPage.login_account_emailadress)).sendKeys(FSemail);
		driver.findElement(By.xpath(BookPage.Login_account_password)).sendKeys(Password);
		driver.findElement(By.xpath(EGiftcardPage.spanish_login_button)).click();
		Thread.sleep(1000);

		/* FOR USING DYNAMIC MEMBER */
		Thread.sleep(3000);
		String name = driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText();
		if (name != "SIGN-IN") {
			log.info("user succesfully validated the member name ");
		} else {
			assertTrue(driver.findElement(By.xpath(BookPage.Homepage_sigin_button)).getText().equals(""));
			log.info("member not logged in correctly ");
		}

	}

	/////////////////////////////////////////////////

	@When("^User clicks on Espanol button$")
	public void User_clicks_on_Espanol_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.languageButtonwhileEnglish)));
		Thread.sleep(5000);
		driver.findElement(By.xpath(BookPage.languageButtonwhileEnglish)).click();
		log.info("Language changed");
		Thread.sleep(900);
		driver.findElement(By.xpath(BookPage.languageButtonwhileSpanish)).getText().equals("English");
		log.info("Page changes to spanish successfully");
	}

	@Then("^Verfiy that the spanish home header does not contain links$")
	public void Verfiy_that_the_spanish_home_header_does_not_contain_links() throws Throwable {
		if (driver.findElements(By.xpath(BookPage.reservas_Header)).size() > 0) {
			assertTrue(true);
			log.info("The Header does not contain Main Links verified");
		} else {
			assertFalse(false);
			log.info("The Header does not contain Main Links verified");
		}
	}

	@Then("^verify the spirit logo is on the left$")
	public void verify_the_spirit_logo_is_on_the_left() throws Throwable {
		Thread.sleep(2000);
		assertTrue(driver.findElement(By.xpath(BookPage.spiritLogo_ayuda_page)).isDisplayed());
		log.info("The spirit logo is on the left of the header");
	}
	
	  @Then("^verify the spirit logo is on the leftside$")
	    public void verify_the_spirit_logo_is_on_the_leftside() throws Throwable {
	       
		  Thread.sleep(2000);
			assertTrue(driver.findElement(By.xpath(BookPage.spiritLogo)).isDisplayed());
			log.info("The spirit logo is on the left of the header");
	    }

	@Then("^verify the correct spanish header links are displayed$")
	public void verify_the_correct_spanish_header_links_are_displayed() throws Throwable {
		assertTrue(driver.findElement(By.xpath(BookPage.registrarme_Header)).isDisplayed());
		log.info("The registrarme link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.ayuda_Header)).isDisplayed());
		log.info("The ayuda link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.contactanos_Header)).isDisplayed());
		log.info("The contactanos link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.ofertas_Header)).isDisplayed());
		log.info("The ofertas link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.nineFareClubES_Header)).isDisplayed());
		log.info("The $9 Fare Club link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.spirit101ES_Header)).isDisplayed());
		log.info("The spirit101 is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.destinos_Header)).isDisplayed());
		log.info("The Destinos link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.languageButtonwhileSpanish)).isDisplayed());
		log.info("The English language button is on the right of the header");
	}

	@Then("^verify the main spanish header links are displayed$")
	public void verify_the_main_spanish_header_links_are_displayed() throws Throwable {
		Thread.sleep(500);
		assertTrue(driver.findElement(By.xpath(BookPage.reservas_Header)).isDisplayed());
		log.info("The 'Reservas' link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.mis_viajes_Header)).isDisplayed());
		log.info("The 'Mis Viajes'link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.registrarse_Header)).isDisplayed());
		log.info("The 'Registrarse' link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.estado_del_Vuelo_Header)).isDisplayed());
		log.info("The 'Estado Del Vuelo' link is displayed on the header");
	}

	@Then("^verify the correct english header links are displayed$")
	public void verify_the_correct_english_header_links_are_displayed() throws Throwable {
		assertTrue(driver.findElement(By.xpath(BookPage.signIn_Header)).isDisplayed());
		log.info("The sign-in link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.help_Header)).isDisplayed());
		log.info("The help link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.contactUs_Header)).isDisplayed());
		log.info("The contact us link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.deals_Header)).isDisplayed());
		log.info("The deals link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.nineFareClubEN_Header)).isDisplayed());
		log.info("The $9 Fare Club link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.spirit101EN_Header)).isDisplayed());
		log.info("The spirit101 is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.destination_Header)).isDisplayed());
		log.info("The destination link is on the right of the header");
		assertTrue(driver.findElement(By.xpath(BookPage.languageButtonwhileEnglish)).isDisplayed());
		log.info("The Espanol language button is on the right of the header");
	}

	@Then("^Verfiy that the english header contain main links")
	public void Verfiy_that_the_english_header_contain_main_links() throws Throwable {
		assertTrue(driver.findElement(By.xpath(BookPage.spiritLogo)).isDisplayed());
		log.info("The logo is displayed on the header");
		// assertTrue(driver.findElement(By.xpath(BookPage.header1DnavigationBar)).isDisplayed());
		// log.info("The bold header content div is displayed");
		assertTrue(driver.findElement(By.xpath(BookPage.book_Header)).isDisplayed());
		log.info("The 'Book' link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.myTrips_Header)).isDisplayed());
		log.info("The 'my trips'link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.checkIn_Header)).isDisplayed());
		log.info("The 'check-in' link is displayed on the header");
		assertTrue(driver.findElement(By.xpath(BookPage.flightStatus_Header)).isDisplayed());
		log.info("The 'flight status' link is displayed on the header");
	}

	@Then("^user clicks on 9DFC enrollment header link$")
	public void user_clicks_on_9DFC_enrollment_header_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.nineFareClubEN_Header)));
		driver.findElement(By.xpath(BookPage.nineFareClubEN_Header)).click();
		log.info("user clicks on the 9DFC link on the header");
	}

	@Then("^User clicks on the Spanish 9DFC Header link$")
	public void User_clicks_on_the_Spanish_9DFC_Header_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.nineFareClubES_Header)));
		driver.findElement(By.xpath(BookPage.nineFareClubES_Header)).click();
		log.info("user clicks on the 9DFC link on the Spanish header");
	}

	@Then("^user clicks on the ayuda link$")
	public void user_clicks_on_ayuda_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.ayuda_Header)));
		driver.findElement(By.xpath(BookPage.ayuda_Header)).click();
		log.info("The user clicks on the ayuda link on the header");
	}

	@Then("^user clicks on the book header link$")
	public void user_clicks_on_the_book_header_link() throws Throwable {
		driver.findElement(By.xpath(BookPage.book_Header)).click();
		log.info("The user clicks on the book link on the header");
	}

	@When("^user lands on the home page$")
	public void user_lands_on_the_home_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.oneway_Trip)));
		log.info("The user lands on the Book option on the home page");
	}

	@Then("^User Clicks on Booking Tab$")
	public void user_clicks_on_booking_tab() throws Throwable {
		driver.findElement(By.xpath(BookPage.bookingtab)).click();
		log.info("The user clicks on the booking tab on the homeppage");
	}

	@Then("^user clicks on the myTrips header link$")
	public void user_clicks_on_the_mytrips_header_link() throws Throwable {
		driver.findElement(By.xpath(BookPage.myTrips_Header)).click();
		log.info("The user clicks on the my trips link on the header");
	}

	@When("^user lands on the myTrips home page$")
	public void user_lands_on_the_mytrips_home_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.manageTravelH3)));
		log.info("The user lands on the Mytrips option on the home page");
	}

	@When("^user lands on the Sapnish Mis viajes home page$")
	public void user_lands_on_the_Sapnish_Mis_viajes_home_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.manageTravelH3ES)));
		log.info("The user lands on the Mis Viajes option on the home page");
	}

	@Then("^user clicks on the check in header link$")
	public void user_clicks_on_the_check_in_header_link() throws Throwable {
		driver.findElement(By.xpath(BookPage.checkIn_Header)).click();
		log.info("The user clicks on the my trips link on the header");
	}

	@When("^user lands on the check in home page$")
	public void user_lands_on_the_check_in_home_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.checkInH3)));
		log.info("The user lands on the Mytrips option on the home page");
	}

	@When("^user lands on the registrarse in home page$")
	public void user_lands_on_the_registrarse_in_home_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.checkInH3ES)));
		log.info("The user lands on the Mytrips option on the home page");
	}

	@Then("^user clicks on the flightStatus in header link$")
	public void user_clicks_on_the_flightstatus_in_header_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.flightStatus_Header)));
		driver.findElement(By.xpath(BookPage.flightStatus_Header)).click();
		log.info("The user clicks on the my flight status link on the header");
	}

	@When("^user lands on the flightstatus in home page$")
	public void user_lands_on_the_flightstatus_in_home_page() throws Throwable {
		Thread.sleep(500);
		Assert.assertTrue("Not taken to the flight status tab",
				driver.findElement(By.xpath(BookPage.flightstatusparagraph)).getText().equals("Check Flight Status"));
		log.info("The user lands on the Flight Status option on the home page");
	}

	@When("^user lands on the Estado del Vuelo in home page$")
	public void user_lands_on_the_Estado_del_Vuelo_in_home_page() throws Throwable {
		Thread.sleep(2000);
		String s =driver.findElement(By.xpath(BookPage.flightstatusparagraphES)).getText();
		Assert.assertTrue("Not taken to the flight status tab",
				driver.findElement(By.xpath(BookPage.flightstatusparagraphES)).getText()
						.equals(s));
		log.info("The user lands on the Spanish Flight Status option on the home page");
	}

	@Then("^user clicks on the contactanos link$")
	public void user_clicks_on_the_contactanos_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.contactanos_Header)));
		driver.findElement(By.xpath(BookPage.contactanos_Header)).click();
		log.info("The user clicks on the contactanos link on the header");
	}

	@Then("^user clicks on the contact us link$")
	public void user_clicks_on_the_contact_us_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.contactUs_Header)));
		driver.findElement(By.xpath(BookPage.contactUs_Header)).click();
		log.info("The user clicks on the contact us link on the header");
	}

	@When("^user lands on contactanos page$")
	public void user_lands_on_contactanos_page() throws Throwable {
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.cantactanosbuttons)));
		log.info("The user lands on the contactanos page");
	}

	@When("^user lands on contact us page$")
	public void user_lands_on_contact_us_page() throws Throwable {
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.cantactussbuttons)));
		log.info("The user lands on the contact us page");
	}

	@When("^user clicks on the deals link in the header$")
	public void user_clicks_on_the_deals_link_in_the_header() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.deals_Header)));
		driver.findElement(By.xpath(BookPage.deals_Header)).click();
		log.info("The user clicks on the deals link on the header");
	}

	@Then("^user lands on the deals page$")
	public void user_lands_on_the_deals_page() throws Throwable {
		String URL = driver.getCurrentUrl();
		
		driver.getCurrentUrl().equals(URL);
		log.info("The user lands on the Deals page");
	}

	@Then("^User clicks on the destinos page$")
	public void user_clicks_on_the_destinos_page() throws Throwable {
		driver.findElement(By.xpath(BookPage.destinos_Header)).click();
		log.info("The user clicks on the destinos link on the header");
	}

	@Then("^user clicks on the header destinations link$")
	public void user_clicks_on_the_header_destinations_link() throws Throwable {

		driver.findElement(By.xpath(BookPage.destination_Header)).click();
		log.info("The user clicks on the destinations link on the header");
	}

	@When("^user lands on the destinos page$")
	public void user_lands_on_the_destinos_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.destinosH1)));
		log.info("The user lands on the Destinos page");
	}

	@When("^user lands on the destinations page$")
	public void user_lands_on_the_destinations_page() throws Throwable {
		// HERE
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.routMap)));
		log.info("The user lands on the Destinos page");
		log.info("The user rout map is on the page");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.upcomingNonstopServices)));
		log.info("The user Upcoming Nonstop Services div is on the page");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.timeTables)));
		log.info("The user Time Tables div is on the page");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.seasonalServices)));
		log.info("The user Seasonal Services is on the page");
	}

	@Then("^verify the Spanish route map$")
	public void verify_the_spanish_route_map() throws Throwable {
	}

	@Then("^Verify the Spanish Upcoming NonStop service$")
	public void verify_the_spanish_upcoming_nonstop_service() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.destinosH1)));
		log.info("Futuros Servicios Directos displays on the page");
	}

	@Then("^verify the Spanish Time Table$")
	public void verify_the_spanish_time_table() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.timeTablesES)));
		log.info("Horario De Vuelos displays on the page");
	}

	@Then("^Verify the Spanish Seasonal Services$")
	public void verify_the_spanish_seasonal_services() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.seasonalServiceES)));
		log.info("Servicio De Temporada displays on the page");
	}

	@Then("^User clicks on Buscar vuelo button$")
	public void user_clicks_on_buscar_vuelo_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.Search_buttonES)));
		driver.findElement(By.xpath(BookPage.Search_buttonES)).click();
		log.info("user clicks on Buscar Vuelo buton");
	}
	
	@When("^user lands on Spanish flight page$")
	public void user_lands_on_Spanish_flight_page() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.flight_availability_page_spanish_validation)));
	String s =driver.findElement(By.xpath(EGiftcardPage.flight_availability_page_spanish_validation)).getText();
	log.info(s);
	}

	@Then("^User Lands on the Spanish Enrollment page$")
	public void user_lands_on_the_spanish_enrollment_page() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.fareClubimg)));
		log.info("User Lands on the Spanish 9DFC enrollment Page");
	}

	@Then("^user Clicks on the Spanish spirit101 header link$")
	public void user_clicks_on_the_spanish_spirit101_header_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.spirit101ES_Header)));
		driver.findElement(By.xpath(BookPage.spirit101ES_Header)).click();
		log.info("user clicks on the Spanish Spirit101 link on the Spanish header");
	}

	@Then("^user verifies the spanish spirit101 page$")
	public void user_verifies_the_spanish_spirit101_page() throws Throwable {
		String URL = driver.getCurrentUrl();
		driver.getCurrentUrl().equals(URL);
		log.info(URL);
		log.info("The user lands on the SPANISH Spirit101 page");
	}

	@Then("^user Clicks on the Spanish Ofertas header link$")
	public void user_clicks_on_the_spanish_ofertas_header_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.ofertas_Header)));
		driver.findElement(By.xpath(BookPage.ofertas_Header)).click();
		log.info("user clicks on the Spanish ofertas link on the Spanish header");
	}

	@Then("^user verifies the spanish Ofertas page$")
	public void user_verifies_the_spanish_ofertas_page() throws Throwable {
		String URL = driver.getCurrentUrl();
		 driver.getCurrentUrl().equals(URL);
	log.info(URL);
		log.info("The user lands on the SPANISH Ofertas page");
	}

	@Then("^User Navigates to the previous page$")
	public void user_navigates_to_the_previous_page() throws Throwable {
		driver.navigate().back();
	}

	@Then("^validate header left aligned$")
	public void validate_header_left_aligned() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.header1)));
		String align = driver.findElement(By.xpath(BookPage.header1)).getCssValue("text-align");
		Assert.assertEquals(align, "left");
		log.info("The header alignment is left validated");
	}

	@Then("^user clicks the FAQ link$")
	public void user_clicks_the_faq_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.FAQ)));
		driver.findElement(By.xpath(BookPage.FAQ)).click();
		log.info("The user Clicks on the FAQ link");
		Thread.sleep(300);

		String tempURL = driver.getCurrentUrl();
		String URL = tempURL.substring(0, 47);

		log.info(URL);
		Assert.assertEquals(URL, "https://customersupport.spirit.com/hc/en-us?_ga");
		log.info("The user lands on the FAQ Ofertas page");
	}

	@Then("^User clicks the spirit101 link$")
	public void user_clicks_the_spirit101_link() throws Throwable {
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.FAQ)));
		driver.findElement(By.xpath(BookPage.spirit101)).click();
		log.info("The user Clicks on the spirit101 link");
		Thread.sleep(300);

		String tempURL = driver.getCurrentUrl();
		System.out.println(tempURL);
//		String URL = tempURL.substring(0, 63);
//		System.out.println(URL);

		log.info(tempURL);
//		Assert.assertEquals(URL, "http://marketing.spirit.com/how-to-fly-spirit-airlines/en/?_ga");
		log.info("The user lands on the FAQ  page");
	}

	@Then("^user clicks on the header help link$")
	public void user_clicks_on_the_header_help_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.contactUs_Header)));
		driver.findElement(By.xpath(BookPage.help_Header)).click();
		log.info("The user clicks on the help link on the header");
	}

	@When("^User lands on the help page$")
	public void user_lands_on_the_help_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.commonQuestions)));
		log.info("The user lands on the help page");
		Thread.sleep(300);

		String tempURL = driver.getCurrentUrl();
		String URL = tempURL.substring(0, 47);

		log.info(URL);
		Assert.assertEquals(URL, "https://customersupport.spirit.com/hc/en-us?_ga");
		log.info("Redirected to the correct URL");
	}

	@Then("^user clicks on the header spirit101 link$")
	public void user_clicks_on_the_header_spirit101_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.spirit101EN_Header)));
		Thread.sleep(300);
		driver.findElement(By.xpath(BookPage.spirit101EN_Header)).click();
		log.info("The user clicks on the spirit101 link on the header");
	}

	@When("^User lands on the spirit101 page$")
	public void user_lands_on_the_spirit101_page() throws Throwable {
		Thread.sleep(300);
		WebElement spirit101Tips = driver.findElement(By.xpath("//p[contains(text(),'Tips for Even More Savings')]"));
		wait.until(ExpectedConditions.visibilityOf(spirit101Tips));

		log.info("The user lands on the spirit101 page");

		String URL = driver.getCurrentUrl();

		log.info(URL);
		Assert.assertEquals(URL, "http://qaepic01.spirit.com/spirit-101");
		log.info("Redirected to the correct URL");
	}
	
	@When("^user lands NineDFC enrollment page$")
	public void user_lands_NineDFC_enrollment_page() throws Throwable {
	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EGiftcardPage.Nindfc_signup_page_validation)));
	 
		String Ninedfc_signup_validation =driver.findElement(By.xpath(EGiftcardPage.Nindfc_signup_page_validation)).getText();
		
		log.info(Ninedfc_signup_validation);
	}
	@When("^user lands on Ayuda page$")
	public void user_lands_on_Ayuda_page() throws Throwable {
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EGiftcardPage.AYUDA_page_validation)));
		String ayuda_page_validation =driver.findElement(By.xpath(EGiftcardPage.AYUDA_page_validation)).getText();
		log.info(ayuda_page_validation);
	}
	
	
	
	@Then("^User clicks on the signed in user located in the header$")
    public void user_clicks_on_the_signed_in_user_located_in_the_header() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_loggedin_Displaying_Name_header_Link)));
        driver.findElement(By.xpath(NineFC.FS_member_loggedin_Displaying_Name_header_Link)).click();
    }
    
    
    @Then("^user clicks on Request Mileage Credit in the Drop Down and Is redirected to the correct URL$")
    public void user_clicks_on_request_mileage_credit_in_the_drop_down_and_is_redirected_to_the_correct_url() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_Header_Menu_Dropdown_Request_Mileage_Credit)));
        driver.findElement(By.xpath(NineFC.FS_Header_Menu_Dropdown_Request_Mileage_Credit)).click();
        String homeurl = driver.getCurrentUrl();
        log.info("Validated Request Mileage Credit url is " + homeurl);
        Assert.assertTrue("Request Mileage Credit does not navigate to valid URL",
                driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/"));
    }
    
    
    @Then("^User Validates the Request Mileage Credit Title$")
    public void user_validates_the_request_mileage_credit_title() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Request_Mileage_Credit)));
        Assert.assertTrue("User Could Not Validate Request Mileage Credit Title",
                driver.findElement(By.xpath(NineFC.Request_Mileage_Credit))
                .getText().equals("Request Mileage Credit"));
        log.info("User Validated Request Mileage Credit Title");
    }
@Then("^user enters the PNR from the original free spirit member generated at the begining of the test$")
    public void user_enters_the_pnr_from_the_original_free_spirit_member_generated_at_the_begining_of_the_test() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Confirmation_Code_Block)));
        String tmp = driver.findElement(By.xpath(NineFC.Confirmation_Code_Block)).getText();
        String pnr = tmp.replace("Confirmation Code: ", "");
        log.info("The PNR obtained after the booking is : " + pnr);
        Thread.sleep(5000);
        driver.findElement(By.xpath(NineFC.Confirmation_Code_Block)).click();
        log.info("Clicked on the Confirmation Code Block");
        driver.findElement(By.xpath(NineFC.Confirmation_Code_Block)).sendKeys(pnr);
        log.info("Entered the confirmation code as : " + pnr);
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
        }
    }
@Then("^User Clicks the Go Button$")
    public void user_clicks_the_go_button() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Confirmation_Code_Block_Go_Button)));
        driver.findElement(By.xpath(NineFC.Confirmation_Code_Block_Go_Button)).click();
    }
    
    
    @Then("^User receives error message from inputting a PNR that was made with a different account other than the one currently logged into$")
    public void user_receives_error_message_from_inputting_a_pnr_that_was_made_with_a_different_account_other_than_the_one_currently_logged_into() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Confirmation_Code_Block_Error_Message)));
        Assert.assertTrue("User Could Not Validate the Confirmation Error Message",
                driver.findElement(By.xpath(NineFC.Confirmation_Code_Block_Error_Message))
                .getText().equals("Your name could not be fond as a customer on this booking"));
        log.info("User Validated the Confirmation Error Message");
    }
    
    @Then("^User Clicks On The Spirit Logo To Return To The Home Page And Signs In As A Free Spirit Member Different Then Original Booking$")
    public void user_clicks_on_the_spirit_logo_to_return_to_the_home_page_and_signs_in_as_a_free_spirit_member_different_then_original_booking() throws Throwable {
    	String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
		String pnr = tmp.replace("Confirmation Code: ", "");
		log.info("The PNR obtained after the booking is : " + pnr);
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.spiritLogo)));
		driver.findElement(By.xpath(BookPage.spiritLogo)).click();
		log.info("User Clicks On Spirit Logo and Is Taken Back To The Home Page");
		
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
		driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).click();
		log.info("Log Into Your Account Succesfully Pops Up");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		driver.findElement(By.xpath(NineFC.login_account_emailadress)).sendKeys("ACANALES@SPIRIT.COM");
		log.info("User Successfully Enters The Email Address");
		driver.findElement(By.xpath(NineFC.Login_account_password)).sendKeys("@Matrix101");
		log.info("User Successfully Enters The Password");

		driver.switchTo().window(parentHandle);
    }
    
}
