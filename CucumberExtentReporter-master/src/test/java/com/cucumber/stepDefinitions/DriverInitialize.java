package com.cucumber.stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import com.cucumber.pages.BookPage;
import com.google.common.io.Files;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import managers.FileReaderManager;
import cucumber.api.java.After;

public class DriverInitialize {

	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);

	@Given("^Spirit airlines application$")
	public void launch_spirit_airlines_application() throws Throwable {
		log.info("Waiting for the page to load completely");
		String url = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
		driver.get(url);
		log.info("Launching the browser with the url : http://qaepic01.spirit.com/");
		if (FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) {
			driver.manage().window().maximize();
			log.info("Maximizing the browser window");
			}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.oneway_Trip)));
		log.info("Launched the browser and opened the spirit airlines site!");
	}

	@Then("^User closes the browser$")
	public void user_closes_the_browser() throws Throwable {
		log.info("The test is completed and the browser is being prepared to close");
		
		
	}
	
	 @After(order=1)
		public void afterScenario(Scenario scenario) {
			if (scenario.isFailed()) {
				String screenshotName = scenario.getName().replaceAll(" ", "_");
				try {
//					driver = MySharedClass.getDriver();
					
					TakesScreenshot ts = (TakesScreenshot) driver;
					//This takes a screenshot from the driver at save it to the specified location
					File sourcePath = ts.getScreenshotAs(OutputType.FILE);
					 DateFormat dateFormat = new SimpleDateFormat("MM_dd_yy_HH_mm_ss");
					  Date date = new Date();
					//Building up the destination path for the screenshot to save
					//Also make sure to create a folder 'screenshots' with in the cucumber-report folder
					File destinationPath = new File(System.getProperty("user.dir") + "/output/cucumber-reports/screenshots/" + screenshotName +"_" +dateFormat.format(date) +".png");
					
					//Copy taken screenshot from source location to destination location
					Files.copy(sourcePath, destinationPath);   
	 
					//This attach the specified screenshot to the test
					Reporter.addScreenCaptureFromPath(destinationPath.toString());
				} catch (IOException e) {
				} 
			}
		}
//@After(order=0)
	  public void cleanUp() {
//	    	driver = MySharedClass.getDriver();
	        driver.close();
	        driver.quit();
	      
	    }

}
