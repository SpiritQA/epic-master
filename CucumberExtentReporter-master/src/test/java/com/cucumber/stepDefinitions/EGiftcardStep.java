package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage319;
import com.cucumber.pages.EGiftcardPage;
import com.cucumber.runner.TestRunner;
import com.mysql.jdbc.Driver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class EGiftcardStep {

	
	
	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
    @When("^user clicks on the EGiftCard link in the footer$")
    public void user_clicks_on_the_egiftcard_link_in_the_footer() throws Throwable {
    	Thread.sleep(500);
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BookPage.eGiftCard)));
        driver.findElement(By.xpath(BookPage.eGiftCard)).click();
        log.info("user clicks on the gift card link on the footer");
    }
	
    @Then("^the user lands on the gift cards page$")
    public void the_user_lands_on_the_gift_cards_page() throws Throwable 
    {
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.moreGoGiftCardHeader)));
        log.info("user lands on the E-Gift Card page");
    }
    
    @Then("^user selects the gift card amount \"([^\"]*)\"$")
    public void user_selects_the_gift_card_amount_something(int cardamount) throws Throwable 
    {
		Thread.sleep(3000);
		
    	if(cardamount==25)
    	{
    		driver.findElement(By.xpath(EGiftcardPage.addamountof25)).click();
    		log.info("User selected the $25 option");
    		
    		String amount = driver.findElement(By.xpath(EGiftcardPage.enterAmount)).getAttribute("value");
    		Assert.assertEquals("$25",amount);
    		log.info("Textbox Shows correct amount: " + amount);
    	}
    
    	else if (cardamount == 50)
    	{
    		driver.findElement(By.xpath(EGiftcardPage.addamountof50)).click();
    		log.info("User selected the $50 option");


    		String amount = driver.findElement(By.xpath(EGiftcardPage.enterAmount)).getAttribute("value");
    		Assert.assertEquals("$50",amount);
        	log.info("Textbox Shows correct amount: " + amount);
    	}
    	else if (cardamount == 100)
    	{
    		driver.findElement(By.xpath(EGiftcardPage.addamountof100)).click();
    		log.info("User selected the $100 option");
    		
    		String value = driver.findElement(By.xpath(EGiftcardPage.enterAmount)).getAttribute("value");
    		Assert.assertEquals("$100",value);
        	log.info("Textbox Shows correct amount: " + value);
    	}
    	else 
    	{
    		driver.findElement(By.xpath(EGiftcardPage.enterAmount)).sendKeys("" + cardamount);
    		log.info("user manually input: $ " + cardamount + "for Gift Card Amount");
    	}    		
    }

    @Then("^user validates the carousel icon$")
    public void user_validates_the_carousel_icon() throws Throwable {
    	
     	String [] options = {EGiftcardPage.happyBirthdayOption, 
			     			EGiftcardPage.congratulationsOption, 
			     			EGiftcardPage. goForItOption , 
			     			EGiftcardPage.justBecauseOption , 
			     			EGiftcardPage.thinkingOfYouOption ,
			     			EGiftcardPage.missYouOption, 
			     			EGiftcardPage.loveYouOption , 
			     			EGiftcardPage.timeForAVacayOption , 
			     			EGiftcardPage.justMarriedOption , 
			     			EGiftcardPage.customMessageOption};
     	
     	//loop through options array- validate option and click on left arrow button
    	for ( int i = 0 ; i < options.length ; i++ )
    	{
    		//validate option from array
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(options[i])));
    		String text = driver.findElement(By.xpath(options[i])).getText();
    		log.info("Option '" + text + "' validated");
    		
    		Thread.sleep(800);
    		//click  left arrow
    		driver.findElement(By.xpath(EGiftcardPage.carauselLeftBttn)).click();
    	}
    }

    @Then("^user selects thinking of you option$")
    public void user_selects_thinking_of_you_option() throws Throwable 
    {
		driver.findElement(By.xpath(EGiftcardPage.carauselRightBttn)).click();
		driver.findElement(By.xpath(EGiftcardPage.carauselRightBttn)).click();
		driver.findElement(By.xpath(EGiftcardPage.thinkingOfYouOption)).click();
    	log.info("Thinking of you Card option selected");
    }
    
    @Then("^user selects just married option$")
    public void user_selects_just_married_option() throws Throwable 
    {
		driver.findElement(By.xpath(EGiftcardPage.carauselLeftBttn)).click();
		driver.findElement(By.xpath(EGiftcardPage.carauselLeftBttn)).click();
		driver.findElement(By.xpath(EGiftcardPage.justMarriedOption)).click();
    	log.info("Just Married Card option selected");
    }
    
    @Then("^user selects happy birthday option$")
    public void user_selects_happy_birthday_option() throws Throwable {
    	driver.findElement(By.xpath(EGiftcardPage.happyBirthdayOption)).click();
    	log.info("Happy Birthday Card option selected");
    }

    
    @Then("^user enters recipient name in TO text box$")
    public void user_enters_recipient_name_in_to_text_bax() throws Throwable 
    {
        driver.findElement(By.xpath(EGiftcardPage.toTextBox)).sendKeys("JoeFlyer");
        log.info("To text box populated");
    }
    
    @Then("^user enters sender name in FROM text box$")
    public void user_enters_sender_name_in_from_text_bo() throws Throwable 
    {
        driver.findElement(By.xpath(EGiftcardPage.fromTextBox)).sendKeys("AutomationTeam");
        log.info("from text box populated");
    }
    
    @Then("^user enters Senders email$")
    public void user_enters_senders_email() throws Throwable {
        driver.findElement(By.xpath(EGiftcardPage.senderEmailTextBox)).sendKeys("emailtesters@spirit.com");
        log.info("Sender Email text box populated");
    }

    @Then("^user enters Recipients email$")
    public void user_enters_recipients_email() throws Throwable {
    	driver.findElement(By.xpath(EGiftcardPage.recipientEmailTextBox)).sendKeys("emailtesters@spirit.com");
        log.info("Recipient Email text box populated");
    }
    
    @Then("^user REenters Recipients email$")
    public void user_reenters_recipients_email() throws Throwable {
    	driver.findElement(By.xpath(EGiftcardPage.reEnterEmail)).sendKeys("emailtesters@spirit.com");
        log.info("Re-Enter recipient Email text box populated");   
    }
    
    @Then("^user enters special message$")
    public void user_enters_special_message() throws Throwable {
    	

        driver.findElement(By.xpath(EGiftcardPage.messageTextBox)).clear();
        
		 // Create object of SimpleDateFormat class and decide the format
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		 Date date = new Date();
		 String date1= dateFormat.format(date);
    	
    	driver.findElement(By.xpath(EGiftcardPage.messageTextBox)).sendKeys("time stamp: " + date1);
        log.info("current date and time (" + date1 + ") entered in the message box");      
    }
    
    @Then("^user clicks on the eGiftCard continue button$")
    public void user_clicks_on_the_egiftcard_continue_button() throws Throwable {
    	try {
    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.continueBttn)));
        	driver.findElement(By.xpath(EGiftcardPage.continueBttn)).click();
    	} catch (Exception e) {
    		log.info("Unable to find the element "+e);
    	}
    }
    
    @Then("^user inputs cc infomration \"([^\"]*)\"$")
    public void user_inputs_cc_infomration_something(String cctype) throws Throwable 
    {
    	driver.findElement(By.xpath(EGiftcardPage.nameOnCard)).sendKeys("Joe Flyer");
		log.info("card holder name input");		
				
		
		//Type of card
    	if(cctype.equals("AMEX"))
    	{
    		driver.findElement(By.xpath(EGiftcardPage.cardNumber)).sendKeys("373235387881007");
    		log.info("User input cc info for AMEX card");
    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.amex))); 
    		log.info("card image validated");
    	}    
    	else if (cctype.equals("Mastercard"))
    	{
    		driver.findElement(By.xpath(EGiftcardPage.cardNumber)).sendKeys("5429775372474030");
    		log.info("User input cc info for Mastercard card");
    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.mastercard)));
    		log.info("card image validated");
    	}
    	else if (cctype.equals("Visa"))
    	{
    		driver.findElement(By.xpath(EGiftcardPage.cardNumber)).sendKeys("4012001021000605");
    		log.info("User input cc info for Visa card");
    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.visa)));    
    		log.info("card image validated");
    	}
    	else if (cctype.equals("Discover"))
    	{
    		driver.findElement(By.xpath(EGiftcardPage.cardNumber)).sendKeys("6011212100000004");
    		log.info("User input cc info for Discover card");
    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(EGiftcardPage.discover)));     
    		log.info("card image validated");   	
    	}    	
    	else {log.info("No cards matched");}

		String exp = "10/24";
    	driver.findElement(By.xpath(EGiftcardPage.cardExpiration)).sendKeys(exp);
		log.info("card expiration date input: " + exp );
    	
    	driver.findElement(By.xpath(EGiftcardPage.securityCode)).sendKeys("1234");
		log.info("card security code input");
		
    	driver.findElement(By.xpath(EGiftcardPage.billingAddress)).sendKeys("2800 Executive Way");
		log.info("card Address input");
		
    	driver.findElement(By.xpath(EGiftcardPage.billingCity)).sendKeys("Miramar");
		log.info("card city input");
		
    	driver.findElement(By.xpath(EGiftcardPage.state)).sendKeys("Florida");
		log.info("card state input");
		
    	driver.findElement(By.xpath(EGiftcardPage.zipCode)).sendKeys("33025");
		log.info("card zip code input");
		
    	driver.findElement(By.xpath(EGiftcardPage.country)).sendKeys("United States of America");
		log.info("card country input");
		
		Select s = new Select(driver.findElement(By.xpath(EGiftcardPage.state)));
		s.selectByVisibleText("Florida");
		
		driver.findElement(By.xpath(EGiftcardPage.iAgreeTC)).click();;
		log.info("Terms and conditions selected");
		
		driver.findElement(By.xpath(EGiftcardPage.continueBttnPurchase)).click();
		log.info("Continue button clicked");	
		Thread.sleep(3000);
    }
    
    

	
	
	
	
	
	
	
	
	
	
	
}
