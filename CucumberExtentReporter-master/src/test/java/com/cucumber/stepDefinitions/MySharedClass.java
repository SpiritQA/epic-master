package com.cucumber.stepDefinitions;


import java.net.HttpURLConnection;
import java.net.URL;
import java.lang.Thread;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import cucumber.api.java.Before;
import managers.FileReaderManager;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.lang.String;

public class MySharedClass {

	private static final Logger log = LogManager.getLogger();
	private static boolean startBrowser = false;
	protected static WebDriver driver;
	protected static ThreadLocal<RemoteWebDriver> threadDriver;
	protected static ThreadLocal<RemoteWebDriver> threadDriver_dummy;
	protected static ThreadLocal<RemoteWebDriver> FFnodeOneThread;
	protected static ThreadLocal<RemoteWebDriver> FFnodeTwoThread;
	protected static ThreadLocal<RemoteWebDriver> FFoneNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> FFtwoNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> FFthreeNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> FFfourNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> FFfiveNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHoneNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHtwoNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHthreeNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHfourNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHfiveNodetwoThread;
	protected static ThreadLocal<RemoteWebDriver> CHoneNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> CHtwoNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> CHthreeNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> CHfourNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> CHfiveNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> FFoneNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> FFtwoNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> FFthreeNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> FFfourNodeoneThread;
	protected static ThreadLocal<RemoteWebDriver> FFfiveNodeoneThread;
	private static String node_url1 = FileReaderManager.getInstance().getConfigReader().getnodeURL1();
	private static String node_url2 = FileReaderManager.getInstance().getConfigReader().getnodeURL2();
	private static boolean Grid_status = FileReaderManager.getInstance().getConfigReader().gridStatus();
	private static boolean Smoke_status = FileReaderManager.getInstance().getConfigReader().Smoke_status();

	@Before()
	public void driverInitialize() throws Exception {
		
		log.info("The current thread name is " + Thread.currentThread().getName());
		if (!startBrowser && !Grid_status && !Smoke_status) {
			localDriver();
		} else if (!startBrowser && Grid_status && !Smoke_status) {
			gridDriver();
		} else if (!startBrowser && !Grid_status && Smoke_status) {
			smokeDriver();
		}
		startBrowser = false;
	}

	public static WebDriver getDriver() {
		if (Grid_status && Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome")) {
			return threadDriver.get();
		} else if (Grid_status && Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome")) {
			return threadDriver_dummy.get();
		} else if (Grid_status && Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox")) {
			return FFnodeOneThread.get();
		} else if (Grid_status && Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox")) {
			return FFnodeTwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 1")) {
			return CHoneNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 2")) {
			return CHtwoNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 3")) {
			return CHthreeNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 4")) {
			return CHfourNodeoneThread.get();
		}else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 5")) {
			return CHfiveNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 1")) {
			return FFoneNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 2")) {
			return FFtwoNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 3")) {
			return FFthreeNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 4")) {
			return FFfourNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 5")) {
			return FFfiveNodeoneThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 1")) {
			return FFoneNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 2")) {
			return FFtwoNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 3")) {
			return FFthreeNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 4")) {
			return FFfourNodetwoThread.get();
		}else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 5")) {
			return FFfiveNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 1")) {
			return CHoneNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 2")) {
			return CHtwoNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 3")) {
			return CHthreeNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 4")) {
			return CHfourNodetwoThread.get();
		} else if (Smoke_status && !Grid_status
				&& Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 5")) {
			return CHfiveNodetwoThread.get();
		} 
		
		else {
			return driver;
		}

	}

	public static void gridDriver() throws Exception {
//		try {
//	        URL url = new URL(node_url1);
//	        URL node2 = new URL(node_url2);
//	        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
//	        urlConn.connect();
//	        HttpURLConnection node2URLconn = (HttpURLConnection) node2.openConnection();
//	        node2URLconn.connect();
//
//	        assertEquals(HttpURLConnection.HTTP_OK, urlConn.getResponseCode());
//	        assertEquals(HttpURLConnection.HTTP_OK, node2URLconn.getResponseCode());
//	    } catch (IOException e) {
//	       log.error("Error creating HTTP connection");
//	        e.printStackTrace();
//	        throw e;
//	    }
		int temp = 0;
		if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome")) {
			temp = 1;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome")) {
			temp = 2;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox")) {
			temp = 3;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox")) {
			temp = 4;
		} else {
			throw new Exception("Didnot configure the four nodes properly in the test runner");
		}
		switch (temp) {
		case 1:
			// Test runner is for the Chrome browser on Node 1
			log.info("Initializing the test runner suite");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			// capability.setVersion("67.0.3396.99");
			capability.setBrowserName("chrome");
			capability.setPlatform(Platform.WINDOWS);
			threadDriver = new ThreadLocal<RemoteWebDriver>();
			threadDriver.set(new RemoteWebDriver(new URL(node_url1), capability));
			break;

		case 2:
			// Test runner Dummy is for the Chrome browser on Node 1
			log.info("Initializing the test runner dummy");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			cap.setBrowserName("chrome");
			// cap.setCapability("marionette", true);
			// cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			cap.setPlatform(Platform.WINDOWS);
			threadDriver_dummy = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			threadDriver_dummy.set(new RemoteWebDriver(new URL(node_url2), cap));
			break;

		case 3:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the firefox node 1  suite");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FFCap_nodeOne = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FFCap_nodeOne.setBrowserName("firefox");
			FFCap_nodeOne.setPlatform(Platform.WINDOWS);
			FFnodeOneThread = new ThreadLocal<RemoteWebDriver>();
			FFnodeOneThread.set(new RemoteWebDriver(new URL(node_url1), FFCap_nodeOne));
			break;

		case 4:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the firefox node 2 suite");
			DesiredCapabilities FFCap_nodeTwo = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFCap_nodeTwo.setBrowserName("firefox");
			FFCap_nodeTwo.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFCap_nodeTwo.setPlatform(Platform.WINDOWS);
			FFnodeTwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFnodeTwoThread.set(new RemoteWebDriver(new URL(node_url2), FFCap_nodeTwo));
			break;
		}
	}

	public static void smokeDriver() throws Exception {
//		try {
//	        URL url = new URL(node_url1);
//	        URL node2 = new URL(node_url2);
//	        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
//	        urlConn.connect();
//	        HttpURLConnection node2URLconn = (HttpURLConnection) node2.openConnection();
//	        node2URLconn.connect();
//
//	        assertEquals(HttpURLConnection.HTTP_OK, urlConn.getResponseCode());
//	        assertEquals(HttpURLConnection.HTTP_OK, node2URLconn.getResponseCode());
//	    } catch (IOException e) {
//	       log.error("Error creating HTTP connection");
//	        e.printStackTrace();
//	        throw e;
//	    }
		int flag = 0;
		if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 1")) {
			flag = 1;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 2")) {
			flag = 2;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 3")) {
			flag = 3;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 4")) {
			flag = 4;
		
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 chrome 5")) {
			flag = 5;
			}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 1")) {
			flag = 6;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 2")) {
			flag = 7;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 3")) {
			flag = 8;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 4")) {
			flag = 9;
		} else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 firefox 5")) {
			flag = 10;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 1")) {
			flag = 11;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 2")) {
			flag = 12;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 3")) {
			flag = 13;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 4")) {
			flag = 14;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 2 chrome 5")) {
			flag = 15;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 1")) {
			flag = 16;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 2")) {
			flag = 17;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 3")) {
			flag = 18;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 4")) {
			flag = 19;
		}else if (Thread.currentThread().getName().equalsIgnoreCase("Node 1 firefox 5")) {
			flag = 20;
		}

		else {
			throw new Exception("Didnot configure the  nodes for smoke test properly in the test runners");
		}
		switch (flag) {
		case 1:
			// Test runner is for the Chrome browser on Node 1
			log.info("Initializing the test runner Node 1 chrome 1");
			log.info("The selenium testing is being done on the node  with the URL : " + node_url1);
			DesiredCapabilities CHonecap = DesiredCapabilities.chrome();
			// capability.setVersion("67.0.3396.99");
			CHonecap.setBrowserName("chrome");
			CHonecap.setPlatform(Platform.WINDOWS);
			CHoneNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			CHoneNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), CHonecap));
			break;

		case 2:
			// Test runner Dummy is for the Chrome browser on Node 1
			log.info("Initializing the test runner Node 1 chrome 2");
			DesiredCapabilities CHtwocap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			CHtwocap.setBrowserName("chrome");
			// cap.setCapability("marionette", true);
			// cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			CHtwocap.setPlatform(Platform.WINDOWS);
			CHtwoNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHtwoNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), CHtwocap));
			break;

		case 3:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 chrome 3");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities CHthreecap = DesiredCapabilities.chrome();
			// capability.setVersion("67.0.3396.99");
			CHthreecap.setBrowserName("chrome");
			CHthreecap.setPlatform(Platform.WINDOWS);
			CHthreeNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			CHthreeNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), CHthreecap));
			break;

		case 4:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 chrome 4");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities CHfourcap = DesiredCapabilities.chrome();
			// capability.setVersion("67.0.3396.99");
			CHfourcap.setBrowserName("chrome");
			CHfourcap.setPlatform(Platform.WINDOWS);
			CHfourNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			CHfourNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), CHfourcap));
			break;
			
		case 5:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 chrome 5");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities CHfivecap = DesiredCapabilities.chrome();
			// capability.setVersion("67.0.3396.99");
			CHfivecap.setBrowserName("chrome");
			CHfivecap.setPlatform(Platform.WINDOWS);
			CHfiveNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			CHfiveNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), CHfivecap));
			break;

		case 6:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 firefox 1");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities FFonecap = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFonecap.setBrowserName("firefox");
			FFonecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFonecap.setPlatform(Platform.WINDOWS);
			FFoneNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFoneNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), FFonecap));
			break;
		case 7:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 firefox 2");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities FFtwocap = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFtwocap.setBrowserName("firefox");
			FFtwocap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFtwocap.setPlatform(Platform.WINDOWS);
			FFtwoNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFtwoNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), FFtwocap));
			break;
		case 8:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 firefox 3");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities FFthreecap = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFthreecap.setBrowserName("firefox");
			FFthreecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFthreecap.setPlatform(Platform.WINDOWS);
			FFthreeNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFthreeNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), FFthreecap));
			break;

		case 9:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 firefox 4");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities FFfourcap = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFfourcap.setBrowserName("firefox");
			FFfourcap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFfourcap.setPlatform(Platform.WINDOWS);
			FFfourNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFfourNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), FFfourcap));
			break;
			
		case 10:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 firefox 5");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities FFfivecap = DesiredCapabilities.firefox();
			// cap.setVersion("67.0.3396.99");
			FFfivecap.setBrowserName("firefox");
			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			FFfivecap.setPlatform(Platform.WINDOWS);
			FFfiveNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			FFfiveNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), FFfivecap));
			break;
			
		case 11:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 chrome 1");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities Chonecap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			Chonecap.setBrowserName("chrome");
//			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			Chonecap.setPlatform(Platform.WINDOWS);
			CHoneNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHoneNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), Chonecap));
			break;
		
		case 12:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 chrome 2");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities Chtwocap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			Chtwocap.setBrowserName("chrome");
//			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			Chtwocap.setPlatform(Platform.WINDOWS);
			CHtwoNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHtwoNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), Chtwocap));
			break;
			
		case 13:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 chrome 3");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities Chthreecap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			Chthreecap.setBrowserName("chrome");
//			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			Chthreecap.setPlatform(Platform.WINDOWS);
			CHthreeNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHthreeNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), Chthreecap));
			break;
		
			
		case 14:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 chrome 4");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities Chfourcap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			Chfourcap.setBrowserName("chrome");
//			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			Chfourcap.setPlatform(Platform.WINDOWS);
			CHfourNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHfourNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), Chfourcap));
			break;
			
		case 15:
			// Firefox node 2 is for the firefox browser on Node 2
			log.info("Initializing the testrunner Node 2 chrome 5");
			log.info("The selenium testing is being done on the node with the URL : " + node_url2);
			DesiredCapabilities Chfivecap = DesiredCapabilities.chrome();
			// cap.setVersion("67.0.3396.99");
			Chfivecap.setBrowserName("chrome");
//			FFfivecap.setCapability("marionette", true);
			// FFCap_nodeTwo.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
			// true);
			Chfivecap.setPlatform(Platform.WINDOWS);
			CHfiveNodetwoThread = new ThreadLocal<RemoteWebDriver>();
			// put the node number on which you
			CHfiveNodetwoThread.set(new RemoteWebDriver(new URL(node_url2), Chfivecap));
			break;
			
		case 16:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 fireox 1");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FF1nodeonecap = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FF1nodeonecap.setBrowserName("firefox");
			FF1nodeonecap.setCapability("marionette", true);
			FF1nodeonecap.setPlatform(Platform.WINDOWS);
			FFoneNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			FFoneNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), FF1nodeonecap));
			break;
			
		case 17:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 fireox 2");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FF2nodeonecap = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FF2nodeonecap.setBrowserName("firefox");
			FF2nodeonecap.setCapability("marionette", true);
			FF2nodeonecap.setPlatform(Platform.WINDOWS);
			FFtwoNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			FFtwoNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), FF2nodeonecap));
			break;
			
		case 18:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 fireox 3");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FF3nodeonecap = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FF3nodeonecap.setBrowserName("firefox");
			FF3nodeonecap.setCapability("marionette", true);
			FF3nodeonecap.setPlatform(Platform.WINDOWS);
			FFthreeNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			FFthreeNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), FF3nodeonecap));
			break;
			
		case 19:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 fireox 4");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FF4nodeonecap = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FF4nodeonecap.setBrowserName("firefox");
			FF4nodeonecap.setCapability("marionette", true);
			FF4nodeonecap.setPlatform(Platform.WINDOWS);
			FFfourNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			FFfourNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), FF4nodeonecap));
			break;
			
			
		case 20:
			// Firefox node 1 is for the firefox browser on Node 1
			log.info("Initializing the test runner Node 1 fireox 5");
			log.info("The selenium testing is being done on the node with the URL : " + node_url1);
			DesiredCapabilities FF5nodeonecap = DesiredCapabilities.firefox();
			// capability.setVersion("67.0.3396.99");
			FF5nodeonecap.setBrowserName("firefox");
			FF5nodeonecap.setCapability("marionette", true);
			FF5nodeonecap.setPlatform(Platform.WINDOWS);
			FFfiveNodeoneThread = new ThreadLocal<RemoteWebDriver>();
			FFfiveNodeoneThread.set(new RemoteWebDriver(new URL(node_url1), FF5nodeonecap));
			break;
		}
	}

	public static void localDriver() throws Exception {
		if (FileReaderManager.getInstance().getConfigReader().getBrowser().equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					FileReaderManager.getInstance().getConfigReader().getChromeDriverPath());
			log.info("The automation test are being executed on the local machine with chrome browser");
			driver = new ChromeDriver();
		} else if (FileReaderManager.getInstance().getConfigReader().getBrowser().equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					FileReaderManager.getInstance().getConfigReader().getFirefoxPath());
			log.info("The automation test are being executed on the local machine with firefox browser");
			driver = new FirefoxDriver();
		} else if (FileReaderManager.getInstance().getConfigReader().getBrowser().equalsIgnoreCase("Edge")) {
			System.setProperty("webdriver.edge.driver",
					FileReaderManager.getInstance().getConfigReader().getEdgePath());
			log.info("The automation test are being executed on the Microsoft Edge browser");
			driver = new EdgeDriver();
		} else {
			log.info("The browser is not specified properly in the configuration.properties");
		}
	}
}
