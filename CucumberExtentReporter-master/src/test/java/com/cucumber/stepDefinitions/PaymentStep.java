package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.PaymentPage;


import cucumber.api.java.en.Then;
import managers.FileReaderManager;

public class PaymentStep {

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 15).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final Logger log = LogManager.getLogger();

	private static String Discover_card_number = FileReaderManager.getInstance().getTestDataReader()
			.discovercardnumber();
	private static String Visa_card_number = FileReaderManager.getInstance().getTestDataReader().visacardnumber();
	private static String Master_card_number = FileReaderManager.getInstance().getTestDataReader().mastercardnumber();
	private static String American_Express_card_number = FileReaderManager.getInstance().getTestDataReader()
			.americanexpresscardnumber();

	private static String NineFC_valid_emailaddress = FileReaderManager.getInstance().getTestDataReader().Ninefcemail();
	private static String NineFC_Valid_Password = FileReaderManager.getInstance().getTestDataReader().Ninepassword();

//	@Then("^user enters the different credit card numbers and validate the card badge$")
//	public void user_enters_the_different_credit_card_numbers_and_validate_the_card_badge() throws Throwable {
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.card_number)));
//		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.xpath(PaymentPage.credit_card_name_validation_discover)));
//
//		cardnumber.sendKeys(Discover_card_number);
//		Assert.assertTrue("user not validate the discover card badge",
//				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_discover)).getAttribute("alt")
//						.equals("Discover Card"));
//		log.info("user succesfully validates the discover card badge");
//		cardnumber.clear();
//
//		cardnumber.sendKeys(Visa_card_number);
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_visa))));
//		Assert.assertTrue("user not validate the visa card badge",
//				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_visa)).getAttribute("alt")
//						.equals("Visa Card"));
//		log.info("user succesfully validates the Visa card badge");
//		cardnumber.clear();
//
//		cardnumber.sendKeys(American_Express_card_number);
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_American_Express))));
//		Assert.assertTrue("user not validate the Amex card badge",
//				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_American_Express))
//						.getAttribute("alt").equals("American Express"));
//		log.info("user succesfully validates the Amex card badge");
//
//		cardnumber.clear();
//		cardnumber.sendKeys(Master_card_number);
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_Master))));
//		Assert.assertTrue("user not validate the Master  card badge",
//				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_Master)).getAttribute("alt")
//						.equals("Master Card"));
//		log.info("user succesfully validates the Master card badge");
//
//		cardnumber.clear();
//		log.info("user succesfully clear the card number");
//	}

	@Then("^user selects the the do not purchase on travel guard content block$")
	public void user_selects_the_the_do_not_purchase_on_travel_guard_content_block() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PaymentPage.No_insurance_radio_button)));
		driver.findElement(By.xpath(PaymentPage.No_insurance_radio_button)).click();
		log.info("user succesfully selects the i dont want insurance radio button");
	}

	@Then("^user selects the saved card from the dropdown$")
	public void user_selects_the_saved_card_from_the_dropdown() throws Throwable {
       Thread.sleep(3000);
		Select s = new Select(driver.findElement(By.xpath(PaymentPage.Saved_card_dropdown)));
		s.selectByVisibleText("XXXXXXXXXXXX0605");
		log.info("user successfully select the one of the saved card from the dropdown ");
	}

	@Then("^user validates the prechecked billing address$")
	public void user_validates_the_prechecked_billing_address() throws Throwable {

	}

	@Then("^user enters the Amex card information$")
	public void user_enters_the_amex_card_information() throws Throwable {

		driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("jack flyer");
		WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
		cardnumber.sendKeys(American_Express_card_number);
		log.info("user succesfully enters the amex card number ");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_American_Express))));
		Assert.assertTrue("user not validate the master card badge",
				driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_American_Express))
						.getAttribute("alt").equals("American Express"));
		log.info("user succesfully validates the AMEX card badge");
		driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
		log.info("user succesfully enters the amex card Experation date ");
		driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("3420");
		log.info("user succesfully enters the amex card security code ");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
		driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
		
		
		
	}
	 @Then("^user enters the Visa card information$")
	    public void user_enters_the_visa_card_information() throws Throwable {
	       
		 
		 driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("jack flyer");
			WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
			cardnumber.sendKeys(Visa_card_number);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_visa))));
			Assert.assertTrue("user not validate the visa card badge",
					driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_visa)).getAttribute("alt")
							.equals("Visa Card"));
			log.info("user succesfully validates the Visa card badge");
			driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
			log.info("user succesfully enters the visa card Experation date ");
			driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("3420");
			log.info("user succesfully enters the visa card security code ");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
			driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
			
	    }
	 @Then("^user enters the Master card information$")
	    public void user_enters_the_master_card_information() throws Throwable {
	        
		 driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("jack flyer");
			WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
			cardnumber.sendKeys(Master_card_number);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_Master))));
			Assert.assertTrue("user not validate the Master card badge",
					driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_Master)).getAttribute("alt")
							.equals("Master Card"));
			log.info("user succesfully validates the Master card badge");
			driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
			log.info("user succesfully enters the master card Experation date ");
			driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("3420");
			log.info("user succesfully enters the master card security code ");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Terms_check)));
			driver.findElement(By.xpath(OnewaytripPage.Terms_check)).click();
			
		 
	    }

	 @Then("^user enters the discover card information$")
	 public void user_enters_the_discover_card_information() throws Throwable {
		 driver.findElement(By.xpath(OnewaytripPage.name_on_card)).sendKeys("jack flyer");
			WebElement cardnumber = driver.findElement(By.xpath(OnewaytripPage.card_number));
			cardnumber.sendKeys(Discover_card_number);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated((By.xpath(PaymentPage.credit_card_name_validation_discover))));
			Assert.assertTrue("user not validate the Master card badge",
					driver.findElement(By.xpath(PaymentPage.credit_card_name_validation_discover)).getAttribute("alt")
							.equals("Discover Card"));
			log.info("user succesfully validates the discover card badge");
			driver.findElement(By.xpath(OnewaytripPage.Experation_date)).sendKeys("0220");
			log.info("user succesfully enters the discover card Experation date ");
			driver.findElement(By.xpath(OnewaytripPage.security_code)).sendKeys("3420");
			log.info("user succesfully enters the discover card security code ");
		
			
		 
	 }

	  @Then("^user do not agree the terms and conditions and validate the error message$")
	    public void user_do_not_agree_the_terms_and_conditions_and_validate_the_error_message() throws Throwable {
	      
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OnewaytripPage.Book_trip)));
			driver.findElement(By.xpath(OnewaytripPage.Book_trip)).click();
		  log.info("user successfully clicks on the Booktrip button");
		String s=  driver.findElement(By.xpath(PaymentPage.payments_page_terms_and_conditions)).getText();
		log.info(s);
		  Assert.assertTrue("user not validate the terms and conditions error message", driver.findElement(By.xpath(PaymentPage.payments_page_terms_and_conditions)).getText().equals(s));
		  log.info("user successfully validate the terms and conditions error message ");
	    }

	 
	 
	 
}
