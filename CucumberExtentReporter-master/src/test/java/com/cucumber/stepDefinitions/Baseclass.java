package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.FooterPage;

import managers.FileReaderManager;

public class Baseclass {
	private static final Logger log = LogManager.getLogger();
	static WebDriver driver = MySharedClass.getDriver();
	private FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	

	public String talk_to_us() {
		
		return driver.findElement(By.xpath(FooterPage.Validate_Talk_To_Us)).getText();
		
	}
	
}
