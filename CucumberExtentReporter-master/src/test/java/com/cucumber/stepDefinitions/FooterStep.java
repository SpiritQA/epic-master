package com.cucumber.stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.junit.rules.ErrorCollector;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage321;

import com.cucumber.pages.FooterPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FooterStep {

	private static final Logger log = LogManager.getLogger();
	SoftAssertions softly = new SoftAssertions();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	@Then("^User validates column header Get to know Us$")
	public void user_validates_column_header_get_to_know_us() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Get_To_Know_Us)));
		Assert.assertTrue("User could not validate the get to know us footer",
				driver.findElement(By.xpath(FooterPage.Validate_Get_To_Know_Us)).getText().equals("Get To Know Us"));
		log.info("user succesfully validated the  Get To Know Us Footer");
		
	}

	@Then("^User validates the column header Talk to us$")
	public void user_validates_the_column_header_talk_to_us() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Talk_To_Us)));
		String Tal_to_us = driver.findElement(By.xpath(FooterPage.Validate_Talk_To_Us)).getText();
		log.info(Tal_to_us);
	
		Assert.assertTrue("User could not validate the Talk To us footer",
				driver.findElement(By.xpath(FooterPage.Validate_Talk_To_Us)).getText().equals(Tal_to_us));
   
		log.info("user succesfully validated the Talk To Us Footer");
	}

	@Then("^User validates the column header Fly with us$")
	public void user_validates_the_column_header_fly_with_us() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Fly_With_Us)));
		driver.findElement(By.xpath(FooterPage.Validate_Fly_With_Us)).getText().equals("Fly with us");
		log.info("user succesfully validated the Fly With Us Footer");
	}

	@Then("^User validates the all the links under Get to know us$")
	public void user_validates_the_all_the_links_under_get_to_know_us() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)).getText().equals("FREE SPIRIT®");
		log.info("user succesfully validated the FREE SPIRIT® link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).getText().equals("$9 Fare Club");
		log.info("user succesfully validated the $9 Fare Club link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Media_Center)).getText().equals("media center");
		log.info("user succesfully validated the media center link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Investor_Relations)).getText()
				.equals("Investor Relations");
		log.info("user succesfully validated the Investor Relations link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Careers)).getText().equals("Careers");
		log.info("user succesfully validated the Careers link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_About_Spirit)).getText().equals("About Spirit");
		log.info("user succesfully validated the About Spirit link");
	}

	@Then("^User validates all the links under talk to us$")
	public void user_validates_all_the_links_under_talk_to_us() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Help_Center)).getText().equals("Help Center");
		log.info("user succesfully validated the Help Center link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contact_Us_Link)).getText().equals("Contact Us");
		log.info("user succesfully validated the Contact Us link");

		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).getText().equals("Contact Us");
		log.info("user succesfully validated the Contact Us link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Legal)).getText().equals("Legal");
		log.info("user succesfully validated the Legal link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contract_Of_Carriage)).getText()
				.equals("Contract of Carriage");
		log.info("user succesfully validated the Contract of Carriage link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Guest_Service_Plan)).getText().equals("Guest Service Plan");
		log.info("user succesfully validated the Guest Service Plan link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Tarmac_Delay_Plan)).getText().equals("Tarmac Delay Plan");
		log.info("user succesfully validated the Tarmac Delay Plan link");

	}

	@Then("^User validates all the links under fly with us$")
	public void user_validates_all_the_links_under_fly_with_us() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_E_gift_Card)).getText().equals("E-Gift Card");
		log.info("user succesfully validated the E-Gift Card link");

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Book)).getText().equals("Book");
		log.info("user succesfully validated the Book link");

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_travel_Agent)).getText().equals("Travel agent");
		log.info("user succesfully validated the Travel agent link");

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Legal)).getText().equals("Legal");
		log.info("user succesfully validated the Legal link");

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Group_Travel)).getText().equals("Group travel");
		log.info("user succesfully validated the Group travel link");

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Where_We_Fly)).getText().equals("Where we fly");
		log.info("user succesfully validated the Where we fly link");

		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Deals)).getText().equals("Deals");
		log.info("user succesfully validated the Deals link");
	}

	@Then("^user clicks on the free spirit link in get to know us footer$")
	public void user_clicks_on_the_free_spirit_link_in_get_to_know_us_footer() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)).click();
	}

	@When("^User lands on the account enrollment page$")
	public void user_lands_on_the_account_enrollment_page() throws Throwable {

		Thread.sleep(3000);

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(FooterPage.Free_Spirit_Account_Enrollment_Page_Header)));
		Assert.assertTrue("User is not  navigated to options page",
				driver.findElement(By.xpath(FooterPage.Free_Spirit_Account_Enrollment_Page_Header)).getText()
						.equals("Free Spirit®"));

		log.info("User Sucessfully Navigated to Account Enrollment Page");
	}

	@Then("^User Clicks on the Nine Dollar Fare Club link$")
	public void user_clicks_on_the_nine_dollar_fare_club_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).click();
	}

	@When("^User land on the club enrollment page$")
	public void user_land_on_the_club_enrollment_page() throws Throwable {

		Thread.sleep(3000);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.$9DFC_Club_Enrollment_Page_Header)));
		Assert.assertTrue("User is not  navigated to options page", driver
				.findElement(By.xpath(FooterPage.$9DFC_Club_Enrollment_Page_Header)).getText().equals("$9 Fare Club"));

		log.info("User Sucessfully Navigated to Club Enrollment Page");
	}

	@Then("^User Clicks on the About Spirit link$")
	public void user_clicks_on_the_about_spirit_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_About_Spirit)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_About_Spirit)).click();
	}

	@When("^User land on the About Us Page$")
	public void user_land_on_the_about_us_page() throws Throwable {

		Thread.sleep(3000);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.About_Spirit_About_Us_Page_Header)));
		Assert.assertTrue("User is not  navigated to options page", driver
				.findElement(By.xpath(FooterPage.About_Spirit_About_Us_Page_Header)).getText().equals("About Spirit"));

		log.info("User Sucessfully Navigated to About Us Page");
	}

	@Then("^Validate About Us header$")
	public void validate_about_us_header() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Validate_About_Us_Header)).getText().equals("About Us");
		log.info("user succesfully validated the About Us Header");
	}

	@Then("^Validate the media note message in about us$")
	public void validate_the_media_note_message_in_about_us() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Media_Note_Aircraft_Photos_and_Broll_Message)).getText()
				.equals("Media note: Aircraft photos and video B-roll are available in the");
		log.info("user succesfully validated the Media Note Disclaimer");
	}

	@Then("^Validate the Spirit Standard Product Features header$")
	public void validate_the_spirit_standard_product_features_header() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Validate_Spirit_Standard_Product_Features)).getText()
				.equals("Spirit Standard Product Features");
		log.info("user succesfully validated the Spirit Standard Product Features");
	}

	@Then("^Validate Spirit Optional Services and Products$")
	public void validate_spirit_optional_services_and_products() throws Throwable {
		driver.findElement(By.xpath(FooterPage.Validate_Spirit_Optional_Services_And_Products)).getText()
				.equals("Spirit Optional Services & Products");
		log.info("user succesfully validated the Spirit Optional Services & Products");
	}

	@Then("^User clicks on the Contact Us Link$")
	public void user_clicks_on_the_contact_us_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Talk_To_Us_Contact_Us_Link)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contact_Us_Link)).click();
	}

	@When("^User lands on the Contact Us Page$")
	public void user_lands_on_the_contact_us_page() throws Throwable {

		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.Contact_Us_Contact_Us_Header)));
		Assert.assertTrue("User is not  navigated to Contact Us page",
				driver.findElement(By.xpath(FooterPage.Contact_Us_Contact_Us_Header)).getText().equals("Contact Us"));

		log.info("User Sucessfully Navigated to Contact Us");
	}

	@Then("^User Validates the contact us header$")
	public void User_Validates_the_contact_us_header() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contact_Us_Link)).getText().equals("Contact Us");
		log.info("user succesfully validated the Contact Us header");

	}

	@Then("^User Validates the frequently asked questions link$")
	public void User_Validates_the_frequently_asked_questions_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Frequently_asked_questions_Link)).getAttribute("href");
		log.info("User Sucessfully validated Frequently Asked Questions " + s);
	}

	@Then("^User Validates the Spirit101 link$")
	public void User_Validates_the_spirit101_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Spirit101_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Spirit 101 Link" + s);
	}

	@Then("^User Validates the Blue Bar Manage A reservation link$")
	public void user_validates_the_blue_bar_manage_a_reservation_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Manage_a_Reservation_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Manage a Reservation Link" + s);
	}

	@Then("^User Validates the Blue Bar Check in Link$")
	public void user_validates_the_blue_bar_check_in_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Check_In_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Manage a Check In Link" + s);
	}

	@Then("^User Validates the Blue Bar Get Flight Status Link$")
	public void user_validates_the_blue_bar_get_flight_status_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Get_Flight_Status_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Get Flight Status Link" + s);
	}

	@Then("^User Validates the Blue Bar Call Link$")
	public void user_validates_the_blue_bar_call_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Call_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Call Link" + s);
	}

	@Then("^User Validates the Blue Bar Email Help Link$")
	public void user_validates_the_blue_bar_email_help_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Email_Help_Link)).getAttribute("href");
		log.info("User Sucessfully validated Email Help Link" + s);
	}

	@Then("^User Validates the international numbers link under the call link$")
	public void user_validates_the_international_numbers_link_under_the_call_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Call_Link)).getAttribute("href");
		log.info("User Sucessfully validated the Call Link" + s);
	}

	@Then("^User Validates the Most Common Questions Header$")
	public void user_validates_the_most_common_questions_header() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Validate_Contact_Us_Footer_Most_Common_Questions_Header)).getText()
				.equals("Most Common Questions");
		log.info("user succesfully validated the Most Common Questions Header");
	}

	@Then("^User Validates What are the size and weight limits for bags$")
	public void user_validates_what_are_the_size_and_weight_limits_for_bags() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.What_are_the_size_and_weight_limits_for_bags_Link))
				.getAttribute("href");
		log.info("User Sucessfully validated What are the size and weight limits for bags? Link" + s);
	}

	@Then("^User Validates How much does Spirit charge for bags$")
	public void user_validates_how_much_does_spirit_charge_for_bags() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.How_much_does_Spirit_charge_for_bags_Link))
				.getAttribute("href");
		log.info("User Sucessfully validated How much does Spirit charge for bags? Link" + s);
	}

	@Then("^User Validates How can I check in and get my boarding pass$")
	public void user_vlidates_how_can_i_check_in_and_get_my_boarding_pass() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.How_can_I_check_in_and_get_my_boarding_pass_Link))
				.getAttribute("href");
		log.info("User Sucessfully validated How can I check in and get my boarding pass? Link" + s);
	}

	@Then("^User Validates Do I have to purchase a seat assignment$")
	public void user_validates_do_i_have_to_purchase_a_seat_assignment() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link))
				.getAttribute("href");
		log.info("User Successfully validated Do I have to purchase a seat assignment? Link" + s);
	}

	@Then("^User Validates General travel information Header$")
	public void user_validates_general_travel_information_header() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Validate_General_Travel_Information_Header)).getText()
				.equals("General travel information");
		log.info("user succesfully validated the General travel information Header");
	}

	@Then("^User Validates Travel Insurance Link$")
	public void user_validates_travel_insurance_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link))
				.getAttribute("href");
		log.info("User Successfully validated Travel Insurance Link" + s);
	}

	@Then("^User Validates Cancelation and refund policies link$")
	public void user_validates_cancelation_and_refund_policies_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Cancelation_and_refund_policies_Link)).getAttribute("href");
		log.info("User Successfully validated Cancelation and refund policies Link" + s);
	}

	@Then("^User Validates Traveling With Pets link$")
	public void user_validates_traveling_with_pets_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Traveling_with_pets_Link)).getAttribute("href");
		log.info("User Successfully validated Traveling With Pets Link" + s);
	}

	@Then("^User Validates TSA and TSA PreCheck_Link$")
	public void user_validates_tsa_and_tsa_prechecklink() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.TSA_and_Tsa_PreCheck_Link)).getAttribute("href");
		log.info("User Successfully validated TSA and TSA PreCheck_Link" + s);
	}

	@Then("^User Validates Traveling with children or unaccompanied minors link$")
	public void user_validates_traveling_with_children_or_unaccompanied_minors_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Traveling_with_children_or_unaccompanied_minors_Link))
				.getAttribute("href");
		log.info("User Successfully validated Traveling with children or unaccompanied minors Link" + s);
	}

	@Then("^User Validates Weather issues link$")
	public void user_validates_weather_issues_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Weather_issues_Link)).getAttribute("href");
		log.info("User Successfully validated Weather issues Link" + s);
	}

	@Then("^User Validates What we do for the military link$")
	public void user_validates_what_we_do_for_the_military_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.What_we_do_for_the_military_Link)).getAttribute("href");
		log.info("User Successfully validated What we do for the military Link" + s);
	}

	@Then("^User Validates the 9dollar Fare Club Header in Contact Us Page$")
	public void user_validates_the_9dollar_fare_club_header_in_contact_us_page() throws Throwable {

		driver.findElement(By.xpath(FooterPage.Validate_General_Travel_Information_Header)).getText()
				.equals("Free Spirit and $9 Fare Club");
		log.info("user succesfully validated the  Free Spirit and $9 Fare Club Header");
	}

	@Then("^User Validates Miles expiration policies Link$")
	public void user_validates_miles_expiration_policies_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Miles_expiration_policies_Link)).getAttribute("href");
		log.info("User Successfully validated Miles expiration policies Link" + s);
	}

	@Then("^User Validates Cancel or renew 9DFC Link$")
	public void user_validates_cancel_or_renew_9dfc_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Cancel_or_renew_$9DFC_Link)).getAttribute("href");
		log.info("User Successfully validated Cancel or renew 9DFC Link" + s);
	}

	@Then("^User Validates Use Miles, Get Miles, Buy or Gift Miles Link$")
	public void user_validates_use_miles_get_miles_buy_or_gift_miles_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Use_Miles_Get_Miles_Buy_or_Gift_Miles_Link))
				.getAttribute("href");
		log.info("User Successfully validated Use Miles, Get Miles, Buy or Gift Miles Link" + s);
	}

	@Then("^User Validates Missing miles from a promotion, certificate or bonus offer Link$")
	public void user_validates_missing_miles_from_a_promotion_certificate_or_bonus_offer_link() throws Throwable {

		String s = driver
				.findElement(By.xpath(FooterPage.Missing_Miles_From_A_Promotion_Certificate_Or_Bonus_Offer_Link))
				.getAttribute("href");
		log.info("User Successfully validated Missing miles from a promotion, certificate or bonus offer Link" + s);
	}

	@Then("^User Validates Forgot your password Link$")
	public void user_validates_forgot_your_password_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Forgot_Your_Password_Link)).getAttribute("href");
		log.info("User Successfully validated Forgot your password Link" + s);
	}

	@Then("^User Validates Learn about FREE SPIRIT and 9dollar Fare Club Link$")
	public void user_validates_learn_about_free_spirit_and_9dollar_fare_club_link() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Learn_about_FREE_SPIRIT_and_9DFC_Link)).getAttribute("href");
		log.info("User Successfully validated Learn about FREE SPIRIT and 9dollar Fare Club Link" + s);
	}

	@Then("^User Validates Information about the SPIRIT MasterCard Credit Card$")
	public void user_validates_information_about_the_spirit_mastercard_credit_card() throws Throwable {

		String s = driver.findElement(By.xpath(FooterPage.Information_about_the_SPIRIT_MasterCard_Credit_Card_Link))
				.getAttribute("href");
		log.info("User Successfully validated Information_about_the_SPIRIT_MasterCard_Credit_Card_Link" + s);
	}

	@Then("^User clicks on Manage A Reservation validates the page and navigates back$")
	public void user_clicks_on_manage_a_reservation_validates_the_page_and_navigates_back() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Check_In_Link)));
		driver.findElement(By.xpath(FooterPage.Check_In_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Manage A Reservation does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://qaepic01.spirit.com/#change-flight"));
		driver.navigate().back();
	}

	@Then("^User clicks on Check In validates the page and navigates back$")
	public void user_clicks_on_check_in_validates_the_page_and_navigates_back() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Check_In_Link)));
		driver.findElement(By.xpath(FooterPage.Check_In_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Check In does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://qaepic01.spirit.com/#change-flight"));
		driver.navigate().back();
	}

	@Then("^User clicks on Check In Get Flight Status page and navigates back$")
	public void user_clicks_on_check_in_get_flight_status_page_and_navigates_back() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_Flight_Status_Link)));
		driver.findElement(By.xpath(FooterPage.Get_Flight_Status_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Get flight status does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://qaepic01.spirit.com/#flight-status"));
		driver.navigate().back();
	}

	@Then("^User Clicks on the Call Link and is taken to customer support$")
	public void user_clicks_on_the_call_link_and_is_taken_to_customer_support() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Call_Link)));
		driver.findElement(By.xpath(FooterPage.Call_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Call Link does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/articles/202097826-How-can-I-contact-Spirit-Airlines-?_ga=2.138190576.1550860159.1525699776-1711497568.1525099831"));
		driver.navigate().back();
	}

	@Then("^User Clicks on the Email Help Link and is takent ot he submit a request page$")
	public void user_clicks_on_the_email_help_link_and_is_takent_ot_he_submit_a_request_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Email_Help_Link)));
		driver.findElement(By.xpath(FooterPage.Email_Help_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Email Help Link status does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/requests/new?_ga=2.71627475.1550860159.1525699776-1711497568.1525099831"));
		driver.navigate().back();
	}

	@Then("^User Clicks What are the size and weight limits for bags and is redirected page$")
	public void user_clicks_what_are_the_size_and_weight_limits_for_bags_and_is_redirected_page() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.What_are_the_size_and_weight_limits_for_bags_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.What_are_the_size_and_weight_limits_for_bags_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("What are the size and weight limits for bags does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202096616-How-much-does-Spirit-charge-for-bags-?_ga=2.231241943.979603933.1525449668-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks How much does Spirit charge for bags and is redirected page$")
	public void user_clicks_how_much_does_spirit_charge_for_bags_and_is_redirected_page() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.How_much_does_Spirit_charge_for_bags_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.How_much_does_Spirit_charge_for_bags_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("How much does Spirit charge for bags does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/articles/202096476-What-are-the-size-and-weight-limits-for-bags-?_ga=2.231241943.979603933.1525449668-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks How can I check in and get my boarding pass and is redirected page$")
	public void user_clicks_how_can_i_check_in_and_get_my_boarding_pass_and_is_redirected_page() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.How_can_I_check_in_and_get_my_boarding_pass_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.How_can_I_check_in_and_get_my_boarding_pass_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("How can I check in and get my boarding pass does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202098006-How-can-I-check-in-and-get-my-boarding-pass-?_ga=2.138736627.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Do I have to purchase a seat assignment and is redirected page$")
	public void user_clicks_do_i_have_to_purchase_a_seat_assignment_and_is_redirected_page() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Do I have to purchase a seat assignment does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202098616-Do-I-have-to-purchase-a-seat-assignment-?_ga=2.138736627.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Travel Insurance Link is redirected to the page$")
	public void user_clicks_travel_insurance_link_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Travel_Insurance_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Travel_Insurance_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Travel Insurance does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us?_ga=2.95362524.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Cancelation and refund policies Link and is redirected to the page$")
	public void user_clicks_cancelation_and_refund_policies_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Cancelation_and_refund_policies_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Cancelation_and_refund_policies_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Cancelation and refund policies does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/sections/200447746-Modify-Cancel-a-Reservation?_ga=2.96875103.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Traveling with pets Link and is redirected to the page$")
	public void user_click_traveling_with_pets_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Traveling_with_pets_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Traveling_with_pets_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Traveling with pets does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/sections/200447736-General-Information?_ga=2.95362524.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks TSA and TSA PreCheck Link and is redirected to the page$")
	public void user_clicks_tsa_and_tsa_precheck_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.TSA_and_Tsa_PreCheck_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.TSA_and_Tsa_PreCheck_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("TSA and TSA PreCheck Link does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/sections/200447736-General-Information?_ga=2.92158298.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Traveling with children or unaccompanied minors Link and is redirected to the page$")
	public void user_clicks_traveling_with_children_or_unaccompanied_minors_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Traveling_with_children_or_unaccompanied_minors_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Traveling_with_children_or_unaccompanied_minors_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Traveling with children or unaccompanied minors does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/sections/200447726-Unaccompanied-Minor-Children?_ga=2.92158298.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Weather issues Link and is redirected to the page$")
	public void user_clicks_weather_issues_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Weather_issues_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Weather_issues_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Weather issues does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/categories/200154156-Travel-Info?_ga=2.61734252.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks What we do for the military Link and is redirected to the page$")
	public void user_clicks_what_we_do_for_the_military_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.What_we_do_for_the_military_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.What_we_do_for_the_military_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("What we do for the military does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/articles/213765027-Does-Spirit-offer-military-discounts-?_ga=2.96875103.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Miles expiration policies Link and is redirected to the page$")
	public void user_clicks_miles_expiration_policies_link_and_is_redirected_to_the_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Miles_expiration_policies_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Miles_expiration_policies_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Miles expiration policies does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.spirit.com/FreeSpiritGetMiles.aspx"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Cancel or renew nine dollar Fare Club Link and is redirected to the page$")
	public void user_clicks_cancel_or_renew_nine_dollar_fare_club_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Cancel_or_renew_$9DFC_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Cancel_or_renew_$9DFC_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Cancel or renew nine dollar Fare Club does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154236--9-Fare-Club?_ga=2.159176058.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Use Miles, Get Miles, Buy or Gift Miles Link and is redirected to the page$")
	public void user_clicks_use_miles_get_miles_buy_or_gift_miles_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Use_Miles_Get_Miles_Buy_or_Gift_Miles_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Use_Miles_Get_Miles_Buy_or_Gift_Miles_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Use Miles, Get Miles, Buy or Gift Miles does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://buy.points.com/marketing/spirit/landing-page/?product=buy"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Use Missing miles from a promotion, certificate or bonus offer Link and is redirected to the page$")
	public void user_clicks_use_missing_miles_from_a_promotion_certificate_or_bonus_offer_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(FooterPage.Missing_Miles_From_A_Promotion_Certificate_Or_Bonus_Offer_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Missing_Miles_From_A_Promotion_Certificate_Or_Bonus_Offer_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue(
				"Use Missing miles from a promotion, certificate or bonus offer does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us?_ga=2.59103853.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Learn about FREE SPIRIT and Nine Dollar Fare Club Link and is redirected to the page$")
	public void user_clicks_learn_about_free_spirit_and_nine_dollar_fare_club_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Learn_about_FREE_SPIRIT_and_9DFC_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Learn_about_FREE_SPIRIT_and_9DFC_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Learn about FREE SPIRIT and Nine Dollar Fare Club does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us?_ga=2.61734252.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks Information about the SPIRIT MasterCard Credit Card Link and is redirected to the page$")
	public void user_clicks_information_about_the_spirit_mastercard_credit_card_link_and_is_redirected_to_the_page()
			throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(FooterPage.Information_about_the_SPIRIT_MasterCard_Credit_Card_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Information_about_the_SPIRIT_MasterCard_Credit_Card_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Information about the SPIRIT MasterCard Credit Card does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us?_ga=2.159176058.1550860159.1525699776-1711497568.1525099831"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Clicks on the Legal Link in the Talk To Us Section of the Footer$")
	public void user_clicks_on_the_legal_link_in_the_talk_to_us_section_of_the_footer() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.Talk_To_Us_Legal)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Legal)).click();
	}

	@Then("^Validate the header of the Legal Page$")
	public void validate_the_header_of_the_legal_page() throws Throwable {

		Assert.assertTrue("User could not validate the city pair",
				driver.findElement(By.xpath(FooterPage.Validate_Legal_Page_Header)).getText().equals("Legal"));
		log.info("user succesfully validated the Legal Page Header");
	}

	@Then("^Validate the Text under the legal Header$")
	public void validate_the_text_under_the_legal_header() throws Throwable {

		Assert.assertTrue("User could not validate the text under the legal Header",
				driver.findElement(By.xpath(FooterPage.Validate_Text_Under_Legal_Page_Header)).getText().equals(
						"For complete information on any of the following items please use the link(s) provided to view the information in Adobe Acrobat PDF format. ( Download Adobe Acrobat Reader for free.)"));
		log.info("user succesfully validated the text under the Legal Page Header");
	}

	@Then("^Validate the Download Adobe Acrobat Reader for free Link$")
	public void validate_the_download_adobe_acrobat_reader_for_free_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Download_Adobe_Acrobat_for_Free_Link)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Validate_Download_Adobe_Acrobat_for_Free_Link)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("Information about the Download Adobe Acrobat Reader for free does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://acrobat.adobe.com/us/en/acrobat/pdf-reader.html"));

		driver.close();
		driver.switchTo().window(parentHandle);
	}

	@Then("^User Validates the Privacy Policy Link$")
	public void user_validates_the_privacy_policy_link() throws Throwable {

		Assert.assertTrue("User could not validate the Privacy Policy Link", driver
				.findElement(By.xpath(FooterPage.Validate_Privacy_Policy_Link)).getText().equals("Privacy Policy"));
		log.info("user succesfully validated the Privacy Policy Link");
	}

	@Then("^User Validates the Contract of Carriage Link$")
	public void user_validates_the_contract_of_carriage_link() throws Throwable {

		Thread.sleep(4000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Contract_of_Carriage_Link)));
		Assert.assertTrue("User could not validate the Contract of Carriage Link",
				driver.findElement(By.xpath(FooterPage.Validate_Contract_of_Carriage_Link)).getText()
						.equals("Contract of Carriage"));
		log.info("user succesfully validated the Contract of Carriage Link");
	}

	@Then("^User Validates th Free Spirit Terms and conditions Link$")
	public void user_validates_th_free_spirit_terms_and_conditions_link() throws Throwable {

		Assert.assertTrue("User could not validate the FREE SPIRIT ™ Terms and Conditions Link",
				driver.findElement(By.xpath(FooterPage.Validate_Free_Spirit_terms_and_Conditions_Link)).getText()
						.equals("FREE SPIRIT ® Terms and Conditions"));
		log.info("user succesfully validated the FREE SPIRIT ™ Terms and Conditions Link");
	}

	@Then("^User validates the nine dollar fare club terms and conditons link$")
	public void user_validates_the_nine_dollar_fare_club_terms_and_conditons_link() throws Throwable {

		Assert.assertTrue("User could not validate the $9 Fare Club Terms and Conditions Link",
				driver.findElement(By.xpath(FooterPage.Validate_Nine_Dollar_Fare_club_Terms_and_Conditions)).getText()
						.equals("$9 Fare Club Terms and Conditions"));
		log.info("user succesfully validated the $9 Fare Club Terms and Conditions Link");
	}

	@Then("^User validates the general terms and conditions link$")
	public void user_validates_the_general_terms_and_conditions_link() throws Throwable {

		Assert.assertTrue("User could not validate the General Terms and Conditions Link",
				driver.findElement(By.xpath(FooterPage.Validate_General_Terms_and_Conditions)).getText()
						.equals("General Terms and Conditions"));
		log.info("user succesfully validated the General Terms and Conditions Link");
	}

	@Then("^User clicks the Privacy Policy link and is taken to the correct page$")
	public void user_clicks_the_privacy_policy_link_and_is_taken_to_the_correct_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Privacy_Policy_Link)));
		driver.findElement(By.xpath(FooterPage.Validate_Privacy_Policy_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		String s = driver.getCurrentUrl();
		Assert.assertTrue("Privacy Policy Link does not navigate to valid URL", driver.getCurrentUrl().equals(s));
		driver.navigate().back();
	}

	@Then("^User clicks the contract of carriage page link and is taken to the correct page$")
	public void user_clicks_the_contract_of_carriage_page_link_and_is_taken_to_the_correct_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Contract_of_Carriage_Link)));
		driver.findElement(By.xpath(FooterPage.Validate_Contract_of_Carriage_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Contract Carriage Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User clicks Free spirit terms and conditions link and is taken to the correct page$")
	public void user_clicks_free_spirit_terms_and_conditions_link_and_is_taken_to_the_correct_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Free_Spirit_terms_and_Conditions_Link)));
		driver.findElement(By.xpath(FooterPage.Validate_Free_Spirit_terms_and_Conditions_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("FREE SPIRIT ™ Terms and Conditions Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User clicks nine dollar fare club terms and conditions link and is taken to the correct page$")
	public void user_clicks_nine_dollar_fare_club_terms_and_conditions_link_and_is_taken_to_the_correct_page()
			throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Validate_Nine_Dollar_Fare_club_Terms_and_Conditions)));
		driver.findElement(By.xpath(FooterPage.Validate_Nine_Dollar_Fare_club_Terms_and_Conditions)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("$9 Fare Club Terms and Conditions Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User clicks the general terms and conditions page and is taken to the correct page$")
	public void user_clicks_the_general_terms_and_conditions_page_and_is_taken_to_the_correct_page() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Validate_General_Terms_and_Conditions)));
		driver.findElement(By.xpath(FooterPage.Validate_General_Terms_and_Conditions)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("General Terms and Conditions Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User goes back to the home page$")
	public void user_goes_back_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Spirit_Home_Page_Link)));
		driver.findElement(By.xpath(FooterPage.Spirit_Home_Page_Link)).click();
	}

	@Then("^User clicks on the Contract of Carriage link under Talk To Us$")
	public void user_clicks_on_the_contract_of_carriage_link_under_talk_to_us() throws Throwable {

		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Talk_To_Us_Contract_Of_Carriage)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contract_Of_Carriage)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Contract Carriage Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User clicks on the Guest Service Plan Link under Talk To Us$")
	public void user_clicks_on_the_guest_service_plan_link_under_talk_to_us() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Talk_To_Us_Guest_Service_Plan)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Guest_Service_Plan)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Guest Service Plan Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User clicks on the Tarmac Dekay Plan Link under Talk To Us$")
	public void user_clicks_on_the_tarmac_dekay_plan_link_under_talk_to_us() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Talk_To_Us_Tarmac_Delay_Plan)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Tarmac_Delay_Plan)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Tarmac Delay Plan Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
	}

	@Then("^User Validates the connect with us header inside the footer$")
	public void user_validates_the_connect_with_us_header_inside_the_footer() throws Throwable {

		Assert.assertTrue("User could not validate the Connect With Us Header",
				driver.findElement(By.xpath(FooterPage.Connect_With_Us_Footer)).getText().equals("Connect With Us"));
		log.info("user succesfully validated the Connect With Us Header");
	}

	@Then("^User clicks on the Facebook Icon Validates the site and returns to the home page$")
	public void user_clicks_on_the_facebook_icon_validates_the_site_and_returns_to_the_home_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Facbook_Icon)));
		driver.findElement(By.xpath(FooterPage.Facbook_Icon)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Facebook Icon does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://www.facebook.com/SpiritAirlines/"));
		driver.navigate().back();
	}

	@Then("^User Clicks on the Twitter Icon Validates the site and returns to the home page$")
	public void user_clicks_on_the_twitter_icon_validates_the_site_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Twitter_Icon)));
		driver.findElement(By.xpath(FooterPage.Twitter_Icon)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Twitter Icon does not navigate to valid URL", driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User Clicks on the Instagram Icon Validates the site and returns to the home page$")
	public void user_clicks_on_the_instagram_icon_validates_the_site_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Instagram_Icon)));
		driver.findElement(By.xpath(FooterPage.Instagram_Icon)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Instagram Icon does not navigate to valid URL", driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User Clicks on the YouTube Icon Validates the site and returns to the home page$")
	public void user_clicks_on_the_youtube_icon_validates_the_site_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Youtube_Icon)));
		driver.findElement(By.xpath(FooterPage.Youtube_Icon)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Youtube Icon does not navigate to valid URL", driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User Click on the Tumblr Icon validates the site and returns to the home page$")
	public void user_click_on_the_tumblr_icon_validates_the_site_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Tumblr_Icon)));
		driver.findElement(By.xpath(FooterPage.Tumblr_Icon)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Tumbr Icon does not navigate to valid URL", driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User Validates the Download Our Mobile App Header$")
	public void user_validates_the_download_our_mobile_app_header() throws Throwable {

		Assert.assertTrue("User could not validate the city pair",
				driver.findElement(By.xpath(FooterPage.Download_Our_Mobile_App_Header)).getText()
						.equals("Download Our Mobile App"));
		log.info("User succesfully validated the Download our Mobile App Header");
	}

	@Then("^User Clicks the App Store Icon and returns to the home page$")
	public void user_clicks_the_app_store_icon_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Apple_App_Store_Footer)));
		driver.findElement(By.xpath(FooterPage.Apple_App_Store_Footer)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Download on the App Store Icon does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User Clicks the Google Play Store App and returns to the home page$")
	public void user_clicks_the_google_play_store_app_and_returns_to_the_home_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Google_Play_Store_Footer)));
		driver.findElement(By.xpath(FooterPage.Google_Play_Store_Footer)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Get it on Google Play Icon does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://play.google.com/store/apps/details?id=com.navitaire.nps.spirit&hl=en"));
		driver.navigate().back();
	}

	@Then("^Validate the Spirit Mastercard$")
	public void validate_the_spirit_mastercard() throws Throwable {
		Thread.sleep(3000);
		Assert.assertTrue("User could not validate The Spirit Mastercard Logo",
				driver.findElement(By.xpath(FooterPage.Spirit_Mastercard_Logo)).getAttribute("src")
						.endsWith("sprit-miles-card.png"));
		log.info("User succesfully validated the Spirit Mastercard Logo");
	}

	@Then("^User Validates the fifteen thousand bonus miles offer text in footer$")
	public void user_validates_the_fifteen_thousand_bonus_miles_offer_text_in_footer() throws Throwable {

		String Bonus = driver.findElement(By.xpath(FooterPage.fifteen_Thousand_Miles_Footer_text)).getText();
		log.info("Bonus offer text should be " + Bonus);
		// Assert.assertTrue("User could not validate the 15,000 Bonus Miles Offer",
		String s = driver.findElement(By.xpath(FooterPage.fifteen_Thousand_Miles_Footer_text)).getText();
		log.info(s);
		log.info("User succesfully validated the 15,000 Bonus Miles Offer");
	}

	@Then("^user validates the APPLY NOW link in the connect with us section of the footer$")
	public void user_validates_the_apply_now_link_in_the_connect_with_us_section_of_the_footer() throws Throwable {

		Assert.assertTrue("User could not validate the Apply Now Link",
				driver.findElement(By.xpath(FooterPage.Apply_Now_Footer)).getText().equals("APPLY NOW"));
		log.info("User succesfully validated the Apply Now Link");
	}

	@Then("^User clicks the Help Center Link and validates the customer support center page$")
	public void user_clicks_the_help_center_link_and_validates_the_customer_support_center_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Talk_To_Us_Help_Center)));
		driver.findElement(By.xpath(FooterPage.Talk_To_Us_Help_Center)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated Customer Support Center page url is " + homeurl);
		Assert.assertTrue("Help Center Link does not navigate to valid URL", driver.getCurrentUrl().equals(homeurl));
	}

	@Then("^User validates the Spirit Airlines Support header on the Customer Support Page$")
	public void user_validates_the_spirit_airlines_support_header_on_the_customer_support_page() throws Throwable {

		Assert.assertTrue("User could not validate the Spirit Airlines Support",
				driver.findElement(By.xpath(FooterPage.Spirit_Airlines_Support_Header)).getText()
						.equals("Spirit Airlines Support"));
		log.info("User succesfully validated the Spirit Airlines Support Header");
	}

	@Then("^User inputs text into the search field and validates that it works$")
	public void user_inputs_text_into_the_search_field_and_validates_that_it_works() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Search_Field)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Search_Field)).click();
		driver.findElement(By.xpath(FooterPage.Customer_Support_Search_Field)).sendKeys("Spirit");
		driver.findElement(By.xpath(FooterPage.Customer_Support_Search_Field)).sendKeys(Keys.ENTER);
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Search field on customer support page works", driver.getCurrentUrl().equals(homeurl));
		driver.navigate().back();
	}

	@Then("^User validates the footer on the customer support page$")
	public void user_validates_the_footer_on_the_customer_support_page() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Get_To_Know_US)).getText().equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		Assert.assertTrue("User could not validate the Spirit Airlines Support",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals("Investor Relations"));
		log.info("User succesfully validated the Investor Relations footer");

		Assert.assertTrue("User could not validate the the Careers footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Careers)).getText().equals("Careers"));
		log.info("User succesfully validated the Careers footer");

		Assert.assertTrue("User could not validate the Where We Fly footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		Assert.assertTrue("User could not validate the Need Help Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Need_Help)).getText().equals("Need Help?"));
		log.info("User succesfully validated the Need Help footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Contact_us)).getText().equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Groups Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Groups)).getText().equals("Groups"));
		log.info("User succesfully validated the Groups footer");

		Assert.assertTrue("User could not validate the Deals Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Deals)).getText().equals("Deals"));
		log.info("User succesfully validated the Deals footer");

		Assert.assertTrue("User could not validate the Partners footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Partners)).getText().equals("Partners"));
		log.info("User succesfully validated the Partners footer");

		Assert.assertTrue("User could not validate the Travel Agents footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Travel_Agents)).getText().equals("Travel Agents"));
		log.info("User succesfully validated the Travel Agents footer");

		Assert.assertTrue("User could not validate the Media Center footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Media_Center)).getText().equals("Media Center"));
		log.info("User succesfully validated the Media Center footer");

		Assert.assertTrue("User could not validate the Advertise With Us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Advertise_With_Us)).getText()
						.equals("Advertise With Us"));
		log.info("User succesfully validated the Advertise With Us footer");

		Assert.assertTrue("User could not validate the Team Travel / OA & Jumpseat footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Team_Travel)).getText()
						.equals("Team Travel / OA & Jumpseat"));
		log.info("User succesfully validated the Team Travel / OA & Jumpseat footer");

		Assert.assertTrue("User could not validate the Policies & Legal footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Policies_and_Legal)).getText()
						.equals("Policies & Legal"));
		log.info("User succesfully validated the Policies & Legal footer");

		Assert.assertTrue("User could not validate the Legal footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Legal)).getText().equals("Legal"));
		log.info("User succesfully validated the Legal footer");

		Assert.assertTrue("User could not validate the Contract of Carriage footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Contract_Of_Carriage)).getText()
						.equals("Contract of Carriage"));
		log.info("User succesfully validated the Contract of Carriage footer");

		Assert.assertTrue("User could not validate the Customer Service Plan footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Customer_Service_Plan)).getText()
						.equals("Customer Service Plan"));
		log.info("User succesfully validated the Customer Service Plan footer");

		Assert.assertTrue("User could not validate the Tarmac Delay Plan footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Tarmac_Delay_Plan)).getText()
						.equals("Tarmac Delay Plan"));
		log.info("User succesfully validated the Tarmac Delay Plan footer");

	}

	@Then("^user Validates the customer support page most common questions Header$")
	public void user_validates_the_customer_support_page_most_common_questions_header() throws Throwable {

		Assert.assertTrue("User could not validate the Most Common Questions Header",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Most_Common_Questions_Header)).getText()
						.equals("Most Common Questions"));
		log.info("User succesfully validated the Most Common Questions Header");
	}

	@Then("^User validates the explore all support topics header$")
	public void user_validates_the_explore_all_support_topics_header() throws Throwable {

		Assert.assertTrue("User could not validate the Explore All Support Topics Header",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Explore_All_Support_Topics)).getText()
						.equals("Explore All Support Topics"));
		log.info("User succesfully validated the Explore All Support Topics Header");
	}

	@Then("^user validates the What are the size and weight limits for the bags link$")
	public void user_validates_the_what_are_the_size_and_weight_limits_for_the_bags_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.What_are_the_size_and_weight_limits_for_bags_Link)));
		driver.findElement(By.xpath(FooterPage.What_are_the_size_and_weight_limits_for_bags_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("What are the size and weight limits Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202096476-What-are-the-size-and-weight-limits-for-bags-"));
		driver.navigate().back();
	}

	@Then("^user validates the how much does spirit charge for bags link$")
	public void user_validates_the_how_much_does_spirit_charge_for_bags_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.How_much_does_Spirit_charge_for_bags_Link)));
		driver.findElement(By.xpath(FooterPage.How_much_does_Spirit_charge_for_bags_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("How much does Spirit charge for bags? Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202096616-How-much-does-Spirit-charge-for-bags-"));
		driver.navigate().back();
	}

	@Then("^user validates the how can i change or cancel my reservation link$")
	public void user_validates_the_how_can_i_change_or_cancel_my_reservation_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.How_Can_I_Change_or_cancel_My_Reservation_link)));
		driver.findElement(By.xpath(FooterPage.How_Can_I_Change_or_cancel_My_Reservation_link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("How can I change or cancel my reservation? Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202097076-How-can-I-change-or-cancel-my-reservation-"));
		driver.navigate().back();
	}

	@Then("^user validates the how can i check in and get my boarding pass link$")
	public void user_validates_the_hwo_can_i_check_in_and_get_my_boarding_pass_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.How_can_I_check_in_and_get_my_boarding_pass_Link)));
		driver.findElement(By.xpath(FooterPage.How_can_I_check_in_and_get_my_boarding_pass_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("How can I check in and get my boarding pass? Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202098006-How-can-I-check-in-and-get-my-boarding-pass-"));
		driver.navigate().back();
	}

	@Then("^user validates the do i have to purchase a seat assignment link$")
	public void user_validates_the_do_i_have_to_purchase_a_seat_assignment_link() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link)));
		driver.findElement(By.xpath(FooterPage.Do_I_have_to_purchase_a_seat_assignment_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Do I have to purchase a seat assignment? Link does not navigate to valid URL",
				driver.getCurrentUrl().equals(
						"https://customersupport.spirit.com/hc/en-us/articles/202098616-Do-I-have-to-purchase-a-seat-assignment-"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support bags link$")
	public void user_validates_the_customer_support_bags_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Bags_Link)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Bags_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Bags Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154136-Baggage"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Seats link$")
	public void user_validates_the_customer_support_seats_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_seats_Link)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_seats_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Seats Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154256-Seats"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Online Check In Link$")
	public void user_validates_the_customer_support_online_check_in_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Online_Checkin)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Online_Checkin)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Online Check In Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154206-Online-Check-in"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Travel info Link$")
	public void user_validates_the_customer_support_travel_info_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Travel_info)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Travel_info)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Travel Info Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154156-Travel-Info"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Packages Link$")
	public void user_validates_the_customer_support_packages_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Packages)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Packages)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Packages Info Link does not navigate to valid URL", driver.getCurrentUrl().equals(
				"https://customersupport.spirit.com/hc/en-us/categories/200154266-Spirit-Airlines-Vacation-Packages"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Special Needs Link$")
	public void user_validates_the_customer_support_special_needs_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Special_Needs)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Special_Needs)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Special Needs Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200150843-Special-Needs"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Member Clubs Link$")
	public void user_validates_the_customer_support_member_clubs_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Member_clubs)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Member_clubs)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Member Clubs Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154236--9-Fare-Club"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support Frequent Flyer Link$")
	public void user_validates_the_customer_support_frequent_flyer_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_Frequent_Flyer)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_Frequent_Flyer)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Frequent Flyer Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154186-FREE-SPIRIT-"));
		driver.navigate().back();
	}

	@Then("^user validates the customer support General Link$")
	public void user_validates_the_customer_support_general_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Customer_Support_General)));
		driver.findElement(By.xpath(FooterPage.Customer_Support_General)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("General Link does not navigate to valid URL", driver.getCurrentUrl()
				.equals("https://customersupport.spirit.com/hc/en-us/categories/200154196-Spirit"));
		driver.navigate().back();
	}

	@Then("^user clicks on the careers link in the get to know us section of the footer$")
	public void user_clicks_on_the_careers_link_in_the_get_to_know_us_section_of_the_footer() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_Careers)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Careers)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Careers Link does not navigate to valid URL",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/careers-home"));
	}

	@Then("^Validate the Join Our Team Header$")
	public void validate_the_join_our_team_header() throws Throwable {

		Assert.assertTrue("User could not validate Join Our Team Header", driver
				.findElement(By.xpath(FooterPage.Careers_Join_Our_Team_Header)).getText().equals("Join Our Team"));
		log.info("User succesfully validated the Join Our Team Header");
	}

	@Then("^Validate The Help Us Change The Way People Fly Header$")
	public void validate_the_help_us_change_the_way_people_fly_header() throws Throwable {

		String thewaypeoplefly = driver
				.findElement(By.xpath(FooterPage.Careers_Help_Us_Change_The_Way_People_Fly_for_good)).getText();
		log.info(" The Way People Fly Header should be " + thewaypeoplefly);
		Assert.assertTrue("User could not validate the Help us change the way people fly for good",
				driver.findElement(By.xpath(FooterPage.Careers_Help_Us_Change_The_Way_People_Fly_for_good)).getText()
						.equals(thewaypeoplefly));
		log.info("User succesfully validated the Help us change the way people fly for good");
	}

	@Then("^user clicks and validates the find a job link$")
	public void user_clicks_and_validates_the_find_a_job_link() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Find_A_Job_Link)));
		driver.findElement(By.xpath(FooterPage.Find_A_Job_Link)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Find A Job Link does not navigate to valid URL",
				driver.getCurrentUrl().equals("https://re12.ultipro.com/SPI1000/JobBoard/listjobs.aspx?Page=Browse"));
		driver.navigate().back();
	}

	@Then("^Validate the history and future of spirit$")
	public void validate_the_history_and_future_of_spirit() throws Throwable {

		Assert.assertTrue("User could not validate The Spirit Mastercard Logo",
				driver.findElement(By.xpath(FooterPage.History_and_Future_of_Spirit)).getAttribute("src")
						.endsWith("https://cms10dss.spirit.com/Shared/Careersbanner.png"));
		log.info("User succesfully validated the Future and History of Spirit Logo");
	}

	@Then("^user clicks on the investors relations link and is taken to the page$")
	public void user_clicks_on_the_investors_relations_link_and_is_taken_to_the_page() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_Investor_Relations)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Investor_Relations)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("User is taken to Investor Relations Page", driver.getCurrentUrl().equals(homeurl));
	}

	@Then("^User validates the footer on the Investor Relations page$")
	public void user_validates_the_footer_on_the_investore_relations_page() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Get_To_Know_US)).getText().equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		Assert.assertTrue("User could not validate the Spirit Airlines Support",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals("Investor Relations"));
		log.info("User succesfully validated the Investor Relations footer");

		String careersfooter = driver.findElement(By.xpath(FooterPage.Investor_Relations_Careers_Footer_Link))
				.getText();
		log.info("The Investor Relations Career Footer should be " + careersfooter);
		Assert.assertTrue("User could not validate the The Investor Relations Career Footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Careers_Footer_Link)).getText()
						.equals(careersfooter));
		log.info("User succesfully validated the The Investor Relations Career Footer");

		Assert.assertTrue("User could not validate the Where We Fly footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		Assert.assertTrue("User could not validate the Need Help Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Need_Help)).getText().equals("Need Help?"));
		log.info("User succesfully validated the Need Help footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contact_Us_footer_Link)).getText()
						.equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Groups Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Groups)).getText().equals("Groups"));
		log.info("User succesfully validated the Groups footer");

		Assert.assertTrue("User could not validate the Deals Footer", driver
				.findElement(By.xpath(FooterPage.Investor_Relations_Deals_footer_Link)).getText().equals("Deals"));
		log.info("User succesfully validated the Deals footer");

		Assert.assertTrue("User could not validate the Partners footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Partners)).getText().equals("Partners"));
		log.info("User succesfully validated the Partners footer");

		Assert.assertTrue("User could not validate the Travel Agents footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Travel_Agents)).getText().equals("Travel Agents"));
		log.info("User succesfully validated the Travel Agents footer");

		Assert.assertTrue("User could not validate the Media Center footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Media_Center)).getText().equals("Media Center"));
		log.info("User succesfully validated the Media Center footer");

		Assert.assertTrue("User could not validate the Team Travel / OA & Jumpseat footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Team_Travel_OA_Jumpseat)).getText()
						.equals("Team Travel / OA & Jumpseat"));
		log.info("User succesfully validated the Team Travel / OA & Jumpseat footer");

		Assert.assertTrue("User could not validate the Policies & Legal footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Policies_and_legal)).getText()
						.equals("Policies & Legal"));
		log.info("User succesfully validated the Policies & Legal footer");

		Assert.assertTrue("User could not validate the Legal footer", driver
				.findElement(By.xpath(FooterPage.Investor_Relations_Legal_footer_Link)).getText().equals("Legal"));
		log.info("User succesfully validated the Legal footer");

		Assert.assertTrue("User could not validate the Contract of Carriage footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contract_of_Carriage_Link)).getText()
						.equals("Contract of Carriage"));
		log.info("User succesfully validated the Contract of Carriage footer");

		Assert.assertTrue("User could not validate the Customer Service Plan footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Customer_Service_Plan_Link)).getText()
						.equals("Customer Service Plan"));
		log.info("User succesfully validated the Customer Service Plan footer");

		Assert.assertTrue("User could not validate the Tarmac Delay Plan footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Tarmac_Delay_Plan_Link)).getText()
						.equals("Tarmac Delay Plan"));
		log.info("User succesfully validated the Tarmac Delay Plan footer");

	}

	@Then("^User clicks on the Where We Fly Link under the the fly with us Title in the footer$")
	public void user_clicks_on_the_where_we_fly_link_under_the_the_fly_with_us_title_in_the_footer() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Fly_With_Us_Where_We_Fly)));
		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Where_We_Fly)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Where We Fly Link does not navigate to valid URL",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/route-maps"));
	}

	@When("^User lands on the route maps page$")
	public void user_lands_on_the_route_maps_page() throws Throwable {

		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.Route_Page_Where_We_Fly)));
		Assert.assertTrue("User is not navigated to Where We Fly Page",
				driver.findElement(By.xpath(FooterPage.Route_Page_Where_We_Fly)).getText().equals("Where We Fly"));
	}

	@Then("^User validates the Upcoming Nonstop Services header$")
	public void user_validates_the_upcoming_nonstop_services_header() throws Throwable {

		Assert.assertTrue("User could not validate the Upcoming Nonstop Services title",
				driver.findElement(By.xpath(FooterPage.Route_Page_Upcoming_Nonstop_Services)).getText()
						.equals("Upcoming Nonstop Services"));
		log.info("User succesfully validated the Upcoming Nonstop Services title");
	}

	@Then("^User validates the Times Table Header$")
	public void user_validates_the_times_table_header() throws Throwable {

		Assert.assertTrue("User could not validate the Time Tables title",
				driver.findElement(By.xpath(FooterPage.Route_Page_Time_Table)).getText().equals("Time Tables"));
		log.info("User succesfully validated the Time Tables title");
	}

	@Then("^User validates the Seasonal Service Header$")
	public void user_validates_the_seasonal_service_header() throws Throwable {

		Assert.assertTrue("User could not validate the Seasonal Service title", driver
				.findElement(By.xpath(FooterPage.Route_Page_Seasonal_Service)).getText().equals("Seasonal Service"));
		log.info("User succesfully validated the Seasonal Service title");
	}

	@Then("^user clicks on Media Center Link in the get to know us footer section$")
	public void user_clicks_on_media_center_link_in_the_get_to_know_us_footer_section() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Get_To_Know_Us_Media_Center)));
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Media_Center)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Media Center Link does not navigate to valid URL",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/press-release"));
	}

	@When("^User lands on the press release page$")
	public void user_lands_on_the_press_release_page() throws Throwable {

		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FooterPage.Media_Center_Page_Header)));
		Assert.assertTrue("User is not navigated to media center Page",
				driver.findElement(By.xpath(FooterPage.Media_Center_Page_Header)).getText().equals("Media Center"));
	}

	@Then("^User validates the media contact header$")
	public void user_validates_the_media_contact_header() throws Throwable {

		Assert.assertTrue("User could not validate the Media Contact title", driver
				.findElement(By.xpath(FooterPage.Media_Center_Page_Media_Contact)).getText().equals("Media Contact"));
		log.info("User succesfully validated the Media Contact title");
	}

	@Then("^User validates the press releases header$")
	public void user_validates_the_press_releases_header() throws Throwable {

		Assert.assertTrue("User could not validate the Press Releases title", driver
				.findElement(By.xpath(FooterPage.Media_Center_Page_Press_Releases)).getText().equals("Press Releases"));
		log.info("User succesfully validated the Press Releases title");
	}

	@Then("^User validates the Media Library header$")
	public void user_validates_the_media_library_header() throws Throwable {

		Assert.assertTrue("User could not validate the Media Library title", driver
				.findElement(By.xpath(FooterPage.Media_Center_Page_Media_Library)).getText().equals("Media Library"));
		log.info("User succesfully validated the Media Library title");
	}

	@Then("^User validates the text under the media contact$")
	public void user_validates_the_text_under_the_media_contact() throws Throwable {

		String mediacontacttext = driver.findElement(By.xpath(FooterPage.Text_Under_the_Media_Contact)).getText();
		log.info("media contact text should be " + mediacontacttext);
		Assert.assertTrue("User could not validate the text under the media contact title", driver
				.findElement(By.xpath(FooterPage.Text_Under_the_Media_Contact)).getText().equals(mediacontacttext));
		log.info("User succesfully validated the text under the media contact title");

		Assert.assertTrue("User could not validate the Phone number under the Media Contact text",
				driver.findElement(By.xpath(FooterPage.Phone_number_under_media_Contact_Text)).getText()
						.equals("Ph: 954-364-0231 "));
		log.info("User succesfully validated the Phone number under the Media Contact text");

	}

	@Then("^User validates the text under the media library$")
	public void user_validates_the_text_under_the_media_library() throws Throwable {

		String medialibrarytext = driver.findElement(By.xpath(FooterPage.Text_under_the_media_library)).getText();
		log.info("media contact text should be " + medialibrarytext);
		Assert.assertTrue("User could not validate the text under the media library title", driver
				.findElement(By.xpath(FooterPage.Text_under_the_media_library)).getText().equals(medialibrarytext));
		log.info("User succesfully validated the text under the media library title");
	}

	@Then("^user validates Aircraft photos title$")
	public void user_validates_aircraft_photos_title() throws Throwable {

		Assert.assertTrue("User could not validate the Aircraft Photos title",
				driver.findElement(By.xpath(FooterPage.aircraft_photos_title)).getText().equals("Aircraft Photos"));
		log.info("User succesfully validated the Aircraft Photos title");
	}

	@Then("^user validates videos title$")
	public void user_validates_videos_title() throws Throwable {

		Assert.assertTrue("User could not validate the videos title",
				driver.findElement(By.xpath(FooterPage.videos_title)).getText().equals("Videos"));
		log.info("User succesfully validated the videos title");
	}

	@Then("^User validates logos title$")
	public void user_validates_logos_title() throws Throwable {

		Assert.assertTrue("User could not validate the Logos title",
				driver.findElement(By.xpath(FooterPage.logos_title)).getText().equals("Logos"));
		log.info("User succesfully validated the Logos title");
	}

	@Then("^User validates route map title$")
	public void user_validates_route_map_title() throws Throwable {

		Assert.assertTrue("User could not validate the Route Map title",
				driver.findElement(By.xpath(FooterPage.Route_map_title)).getText().equals("Route Map"));
		log.info("User succesfully validated the Route Map title");
	}

	@Then("^User validates column header Get to know Us in the press release page$")
	public void user_validates_column_header_get_to_know_us_in_the_press_release_page() throws Throwable {

		Assert.assertTrue("User could not validate the get to know us footer",
				driver.findElement(By.xpath(FooterPage.Get_to_know_us_press_release_page)).getText()
						.equals("Get To Know Us"));
		log.info("user succesfully validated the  Get To Know Us Footer");
	}

	@Then("^User validates the column header Talk to us in the press release page$")
	public void user_validates_the_column_header_talk_to_us_in_the_press_release_page() throws Throwable {

		Assert.assertTrue("User could not validate the Talk To Us footer",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Press_Release_page)).getText().equals("Talk To Us"));
		log.info("user succesfully validated the Talk to Us Footer");
	}

	@Then("^User validates the column header Fly with us in the press relase page$")
	public void user_validates_the_column_header_fly_with_us_in_the_press_relase_page() throws Throwable {

		Assert.assertTrue("User could not validate the Fly With Us Footer", driver
				.findElement(By.xpath(FooterPage.Fly_With_Us_Press_Release_Page)).getText().equals("Fly With Us"));
		log.info("user succesfully validated the Fly With Us Footer");
	}

	@Then("^user validates the spirit logo$")
	public void user_validates_the_spirit_logo() throws Throwable {

		Assert.assertTrue("User could not validate The Spirit Bottom Footer Logo",
				driver.findElement(By.xpath(FooterPage.Spirit_Logo_home_page_footer_logo)).getAttribute("src")
						.endsWith("assets/img/global/spirit-logo-white.png"));
		log.info("User succesfully validated The Spirit Bottom Footer Logo");
	}

	@Then("^user validates the copyright text underneath the logo$")
	public void user_validates_the_copyright_text_underneath_the_logo() throws Throwable {

		String SpiritCopyRightFooter = driver.findElement(By.xpath(FooterPage.Copyright_Bottom_Footer_Text)).getText();
		log.info("media contact text should be " + SpiritCopyRightFooter);
		Assert.assertTrue("User could not validate The Copyright Text in the Footer",
				driver.findElement(By.xpath(FooterPage.Copyright_Bottom_Footer_Text)).getText()
						.equals(SpiritCopyRightFooter));
	}

	@Then("^user validates the spirit master card logo in thr pencil banner$")
	public void user_validates_the_spirit_master_card_logo_in_thr_pencil_banner() throws Throwable {

		String PencilBannerMastercardLogo = driver.findElement(By.xpath(FooterPage.Pencil_Banner_Mastercard_logo))
				.getText();
		log.info("media contact text should be " + PencilBannerMastercardLogo);
		Assert.assertTrue("User could not validate The Pencil Banner Mastercard Logo Footer",
				driver.findElement(By.xpath(FooterPage.Pencil_Banner_Mastercard_logo)).getText()
						.equals(PencilBannerMastercardLogo));
	}

	@Then("^User validates the apply now button in the pencil banner$")
	public void user_validates_the_apply_now_button_in_the_pencil_banner() throws Throwable {

		Assert.assertTrue("User could not validate the Apply Now button in the pencil banner located in the footer",
				driver.findElement(By.xpath(FooterPage.Pencil_Banner_Apply_Now_button)).getText().equals("APPLY NOW"));
		log.info("user succesfully validated the Apply Now Button in the pencil banner located in the Footer");
	}

	@Then("^User clicks the apply now button and is taken to the correct page$")
	public void user_clicks_the_apply_now_button_and_is_taken_to_the_correct_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Pencil_Banner_Apply_Now_button)));

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FooterPage.Pencil_Banner_Apply_Now_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);

		Assert.assertTrue("User clicks on the Apply Now Button and does not navigate to valid URL",
				driver.getCurrentUrl().equals(homeurl));

		driver.switchTo().window(parentHandle);
	}

	@Then("^user Validates the e gift card link$")
	public void user_validates_the_e_gift_card_link() throws Throwable {

		Assert.assertTrue("User could not validate the E-Gift Card Link in the Footer",
				driver.findElement(By.xpath(FooterPage.Fly_With_Us_E_gift_Card)).getText().equals("E-Gift Card"));
		log.info("user succesfully validated the E-Gift Card Link in the Footer");
	}

	@Then("^User clicks on the Egift card clink and is taken to the right page$")
	public void user_clicks_on_the_egift_card_clink_and_is_taken_to_the_right_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Fly_With_Us_E_gift_Card)));
		driver.findElement(By.xpath(FooterPage.Fly_With_Us_E_gift_Card)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("E-Gift Card Link does not navigate to valid URL",
				driver.getCurrentUrl().equals("http://qaepic01.spirit.com/gift-card"));
	}

	@Then("^User clicks on the travel agent link in the footer and validates the URL$")
	public void user_clicks_on_the_travel_agent_link_in_the_footer_and_validates_the_url() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Fly_With_Us_travel_Agent)));
		driver.findElement(By.xpath(FooterPage.Fly_With_Us_travel_Agent)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Travel Agent Link does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/travel_portal_home-initial.aspx"));
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------------

	@Then("^User validates the footer on the Travel Agent Portal$")
	public void user_validates_the_footer_on_the_travel_agent_portal() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Get_To_Know_US)).getText().equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		String InvestorRelationsTravelAgentPage = driver
				.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText();
		log.info("media contact text should be " + InvestorRelationsTravelAgentPage);
		Assert.assertTrue("User could not validate the Investor Relations footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals(InvestorRelationsTravelAgentPage));
		log.info("User succesfully validated the Investor Relations footer");

		String CareersTravelAgentPage = driver.findElement(By.xpath(FooterPage.Customer_Support_Careers)).getText();
		log.info("media contact text should be " + CareersTravelAgentPage);
		Assert.assertTrue("User could not validate the the Careers footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Careers)).getText().equals(CareersTravelAgentPage));
		log.info("User succesfully validated the Careers footer");

		Assert.assertTrue("User could not validate the Where We Fly footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		Assert.assertTrue("User could not validate the Need Help? in the footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Need_Help)).getText().equals("Need Help?"));
		log.info("User succesfully validated the Need Help? Header in the footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contact_Us_footer_Link)).getText()
						.equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Groups Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Groups)).getText().equals("Groups"));
		log.info("User succesfully validated the Groups footer");

		String DealsTravelAgentPage = driver.findElement(By.xpath(FooterPage.Deals_Travel_Agent_Link)).getText();
		log.info("media contact text should be " + DealsTravelAgentPage);
		Assert.assertTrue("User could not validate the Deals travel agent page Footer link", driver
				.findElement(By.xpath(FooterPage.Deals_Travel_Agent_Link)).getText().equals(DealsTravelAgentPage));
		log.info("User succesfully validated the Deals travel aganet page footer link");

		Assert.assertTrue("User could not validate the Partners footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Partners)).getText().equals("Partners"));
		log.info("User succesfully validated the Partners footer");

		Assert.assertTrue("User could not validate the Travel Agents footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Travel_Agents)).getText().equals("Travel Agents"));
		log.info("User succesfully validated the Travel Agents footer");

		Assert.assertTrue("User could not validate the Media Center footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Media_Center)).getText().equals("Media Center"));
		log.info("User succesfully validated the Media Center footer");

		Assert.assertTrue("User could not validate the Team Travel / OA & Jumpseat footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Team_Travel_OA_Jumpseat)).getText()
						.equals("Team Travel / OA & Jumpseat"));
		log.info("User succesfully validated the Team Travel / OA & Jumpseat footer");

		Assert.assertTrue("User could not validate the Policies & Legal footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Policies_and_legal)).getText()
						.equals("Policies & Legal"));
		log.info("User succesfully validated the Policies & Legal footer");

		Assert.assertTrue("User could not validate the Legal footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Legal_footer_Link)).getText()
						.equals("Legal / Policy"));
		log.info("User succesfully validated the Legal footer");

		Assert.assertTrue("User could not validate the Contract of Carriage footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contract_of_Carriage_Link)).getText()
						.equals("Contract of Carriage"));
		log.info("User succesfully validated the Contract of Carriage footer");

		Assert.assertTrue("User could not validate the Guest Service Plan link in the travel agent page footer",
				driver.findElement(By.xpath(FooterPage.Travel_Agent_Guest_Service_Plan_Link)).getText()
						.equals("Guest Service Plan"));
		log.info("User succesfully validated the Guest Service Plan link in the travel agent page footer");

		Assert.assertTrue("User could not validate the Tarmac Delay Plan footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Tarmac_Delay_Plan_Link)).getText()
						.equals("Tarmac Delay Plan"));
		log.info("User succesfully validated the Tarmac Delay Plan footer");

	}

	@Then("^User clicks on the Deals link in the footer and validates the URL$")
	public void user_clicks_on_the_deals_link_in_the_footer_and_validates_the_url() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FooterPage.Fly_With_Us_Deals)));
		driver.findElement(By.xpath(FooterPage.Fly_With_Us_Deals)).click();
		String homeurl = driver.getCurrentUrl();
		log.info("Validated home page url is " + homeurl);
		Assert.assertTrue("Deals Link does not navigate to valid URL",
				driver.getCurrentUrl().startsWith("https://www.spirit.com/en/flights"));
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------

	@Then("^User validates the footer on the deals page$")
	public void user_validates_the_footer_on_the_deals_page() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Get_To_Know_Us)).getText().equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		String InvestorRelationsTravelAgentPage = driver
				.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText();
		log.info("media contact text should be " + InvestorRelationsTravelAgentPage);
		Assert.assertTrue("User could not validate the Investor Relations footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals(InvestorRelationsTravelAgentPage));
		log.info("User succesfully validated the Investor Relations footer");

		String CareersDealsPage = driver.findElement(By.xpath(FooterPage.Deals_Page_Career_Link)).getText();
		log.info("media contact text should be " + CareersDealsPage);
		Assert.assertTrue("User could not validate the the Careers footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Career_Link)).getText().equals(CareersDealsPage));
		log.info("User succesfully validated the Careers footer");

		Assert.assertTrue("User could not validate the Where We Fly footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		Assert.assertTrue("User could not validate the Need Help? in the footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Need_Help_Link)).getText().equals("Need Help?"));
		log.info("User succesfully validated the Need Help? Header in the footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contact_Us_footer_Link)).getText()
						.equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Groups Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Groups)).getText().equals("Groups"));
		log.info("User succesfully validated the Groups footer");

		String DealslinkDealsPage = driver.findElement(By.xpath(FooterPage.Deals_page_Deal_Link)).getText();
		log.info("media contact text should be " + DealslinkDealsPage);
		Assert.assertTrue("User could not validate the Deals travel agent page Footer link",
				driver.findElement(By.xpath(FooterPage.Deals_page_Deal_Link)).getText().equals(DealslinkDealsPage));
		log.info("User succesfully validated the Deals travel aganet page footer link");

		Assert.assertTrue("User could not validate the Partners footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Partners)).getText().equals("Partners"));
		log.info("User succesfully validated the Partners footer");

		Assert.assertTrue("User could not validate the Travel Agents footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Travel_Agents)).getText().equals("Travel Agents"));
		log.info("User succesfully validated the Travel Agents footer");

		Assert.assertTrue("User could not validate the Media Center footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Media_Center)).getText().equals("Media Center"));
		log.info("User succesfully validated the Media Center footer");

		Assert.assertTrue("User could not validate the Advertise With Us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Advertise_With_Us)).getText()
						.equals("Advertise With Us"));
		log.info("User succesfully validated the Advertise With Us footer");

		Assert.assertTrue("User could not validate the Team Login footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Team_Login)).getText().equals("Team Login"));
		log.info("User succesfully validated the Team Login footer");

		Assert.assertTrue("User could not validate the Team Travel / OA & Jumpseat footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Team_Travel_and_Jumpseat)).getText()
						.equals("Team Travel/ OA & Jumpseat"));
		log.info("User succesfully validated the Team Travel / OA & Jumpseat footer");

		Assert.assertTrue("User could not validate the Policies & Legal footer", driver
				.findElement(By.xpath(FooterPage.Deals_Page_Policies_and_legal)).getText().equals("Policies & Legal"));
		log.info("User succesfully validated the Policies & Legal footer");

		Assert.assertTrue("User could not validate the Legal footer", driver
				.findElement(By.xpath(FooterPage.Investor_Relations_Legal_footer_Link)).getText().equals("Legal"));
		log.info("User succesfully validated the Legal footer");

		Assert.assertTrue("User could not validate the Contract of Carriage footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contract_of_Carriage_Link)).getText()
						.equals("Contract of Carriage"));
		log.info("User succesfully validated the Contract of Carriage footer");

		Assert.assertTrue("User could not validate the Customer Service Plan footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Customer_Service_Plan_Link)).getText()
						.equals("Customer Service Plan"));
		log.info("User succesfully validated the Customer Service Plan footer");

		Assert.assertTrue("User could not validate the Tarmac Delay Plan footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Tarmac_Delay_Plan_Link)).getText()
						.equals("Tarmac Delay Plan"));
		log.info("User succesfully validated the Tarmac Delay Plan footer");
	}

	// -------------------------------------------------------------------------------------------------------------------------------------------------------

	@Then("^User Validates Reservation Page Summary Footer$")
	public void user_validates_reservation_page_summary_footer() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us",
				driver.findElement(By.xpath(FooterPage.Get_to_know_us_press_release_page)).getText()
						.equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");
		String Get_to_know_us = driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)).getText();
		Assert.assertTrue("User could not validate the the FREE SPIRIT® link",
				driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)).getText().equals(Get_to_know_us));
		log.info("user succesfully validated the FREE SPIRIT® link");

		String nine_fare_club_link = driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).getText();
		Assert.assertTrue("User could not validate the $9 Fare Club link", driver
				.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).getText().equals(nine_fare_club_link));
		log.info("user succesfully validated the $9 Fare Club link");

		Assert.assertTrue("User could not validate the Media Center footer",
				driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_Media_Center)).getText().equals("Media Center"));
		log.info("User succesfully validated the Media Center footer");

		String InvestorRelationsTravelAgentPage = driver
				.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText();
		log.info("media contact text should be " + InvestorRelationsTravelAgentPage);
		Assert.assertTrue("User could not validate the Investor Relations footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals(InvestorRelationsTravelAgentPage));
		log.info("User succesfully validated the Investor Relations footer");

		String CareersDealsPage = driver.findElement(By.xpath(FooterPage.Deals_Page_Career_Link)).getText();
		log.info("media contact text should be " + CareersDealsPage);
		Assert.assertTrue("User could not validate the the Careers footer",
				driver.findElement(By.xpath(FooterPage.Deals_Page_Career_Link)).getText().equals(CareersDealsPage));
		log.info("User succesfully validated the Careers footer");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Checkin_Page_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Checkin_Page_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Talk To Us footer",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Press_Release_page)).getText().equals("Talk To Us"));
		log.info("user succesfully validated the Talk to Us Footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Checkin_Page_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contact_Us_Link)).getText().equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Legal footer",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Legal)).getText().equals("Legal"));
		log.info("User succesfully validated the Legal footer");

		String ContractofCarriageCheckInPage = driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contract_Of_Carriage))
				.getText();
		log.info("contract of carriage text should be " + ContractofCarriageCheckInPage);
		Assert.assertTrue("User could not validate the Contract of Carriage footer",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Contract_Of_Carriage)).getText()
						.equals(ContractofCarriageCheckInPage));
		log.info("User succesfully validated the Contract of Carriage footer");

		Assert.assertTrue("User could not validate the Guest Service Plan link",
				driver.findElement(By.xpath(FooterPage.Talk_To_Us_Guest_Service_Plan)).getText()
						.equals("Guest Service Plan"));
		log.info("user succesfully validated the Guest Service Plan link");

		Assert.assertTrue("User could not validate the Tarmac Delay Plan footer", driver
				.findElement(By.xpath(FooterPage.Talk_To_Us_Tarmac_Delay_Plan)).getText().equals("Tarmac Delay Plan"));
		log.info("User succesfully validated the Tarmac Delay Plan footer");

		Assert.assertTrue("User could not validate the Fly With Us Footer", driver
				.findElement(By.xpath(FooterPage.Fly_With_Us_Press_Release_Page)).getText().equals("Fly With Us"));
		log.info("user succesfully validated the Fly With Us Footer");

		Assert.assertTrue("User could not validate the E-Gift Card Link in the Footer",
				driver.findElement(By.xpath(FooterPage.Fly_With_Us_E_gift_Card)).getText().equals("E-Gift Card"));
		log.info("user succesfully validated the E-Gift Card Link in the Footer");

		Assert.assertTrue("User could not validate the Book link in the Footer",
				driver.findElement(By.xpath(FooterPage.Fly_With_Us_Book)).getText().equals("Book"));
		log.info("user succesfully validated the Book link");

		String TravelAgentCheckinPage = driver.findElement(By.xpath(FooterPage.Fly_With_Us_travel_Agent)).getText();
		log.info("media contact text should be " + TravelAgentCheckinPage);
		Assert.assertTrue("User could not validate the Travel Agents footer", driver
				.findElement(By.xpath(FooterPage.Fly_With_Us_travel_Agent)).getText().equals(TravelAgentCheckinPage));
		log.info("User succesfully validated the Travel Agents footer");

		String GroupTravelCheckinPage = driver.findElement(By.xpath(FooterPage.Fly_With_Us_Group_Travel)).getText();
		log.info("media contact text should be " + GroupTravelCheckinPage);
		Assert.assertTrue("User could not validate the Group travel link", driver
				.findElement(By.xpath(FooterPage.Fly_With_Us_Group_Travel)).getText().equals(GroupTravelCheckinPage));
		log.info("user succesfully validated the Group travel link");

		Assert.assertTrue("User could not validate the Where We Fly footer",
				driver.findElement(By.xpath(FooterPage.Fly_With_Us_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		String DealslinkCheckinPage = driver.findElement(By.xpath(FooterPage.Fly_With_Us_Deals)).getText();
		log.info("media contact text should be " + DealslinkCheckinPage);
		Assert.assertTrue("User could not validate the Deals travel agent page Footer link",
				driver.findElement(By.xpath(FooterPage.Fly_With_Us_Deals)).getText().equals(DealslinkCheckinPage));
		log.info("User succesfully validated the Deals travel aganet page footer link");

	}

	@Then("^user clicks on the free spirits link$")
	public void user_clicks_on_the_free_spirits_link() throws Throwable {
		Thread.sleep(2000);
		driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_FREE_SPIRIT)).click();

	}

	@Then("^User validates the footer on the Investore Relations page$")
	public void user_validates_the_footer_on_the_Investore_Relations_page() throws Throwable {

		Assert.assertTrue("User could not validate the Customer support Get To Know Us", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Get_To_Know_US)).getText().equals("Get To Know Us"));
		log.info("User succesfully validated the Customer support Get To Know Us");

		Assert.assertTrue("User could not validate the Customer support About us footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_About_Us)).getText().equals("About Us"));
		log.info("User succesfully validated the Customer support About us footer");

		String InvestorRelationsTravelAgentPage = driver
				.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText();
		log.info("media contact text should be " + InvestorRelationsTravelAgentPage);
		Assert.assertTrue("User could not validate the Investor Relations footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Investor_Relations)).getText()
						.equals(InvestorRelationsTravelAgentPage));
		log.info("User succesfully validated the Investor Relations footer");

		String CareersTravelAgentPage = driver.findElement(By.xpath(FooterPage.Customer_Support_Careers)).getText();
		log.info("media contact text should be " + CareersTravelAgentPage);
		Assert.assertTrue("User could not validate the the Careers footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Careers)).getText().equals(CareersTravelAgentPage));
		log.info("User succesfully validated the Careers footer");

		Assert.assertTrue("User could not validate the Where We Fly footer", driver
				.findElement(By.xpath(FooterPage.Customer_Support_Where_We_Fly)).getText().equals("Where We Fly"));
		log.info("User succesfully validated the Where We Fly footer");

		Assert.assertTrue("User could not validate the Need Help? in the footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Need_Help)).getText().equals("Need Help?"));
		log.info("User succesfully validated the Need Help? Header in the footer");

		Assert.assertTrue("User could not validate the Help Center footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Help_Center)).getText().equals("Help Center"));
		log.info("User succesfully validated the Help Center footer");

		Assert.assertTrue("User could not validate the Contact Us footer",
				driver.findElement(By.xpath(FooterPage.Investor_Relations_Contact_Us_footer_Link)).getText()
						.equals("Contact Us"));
		log.info("User succesfully validated the Contact Us footer");

		Assert.assertTrue("User could not validate the Groups Footer",
				driver.findElement(By.xpath(FooterPage.Customer_Support_Groups)).getText().equals("Groups"));
		log.info("User succesfully validated the Groups footer");

	}

}
