package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.io.IOException;
import java.math.RoundingMode;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;

import com.cucumber.pages.BookPage;

import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.OptionsPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.RoundtripPage;
import com.cucumber.pages.SeatsPage320;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OptionsStep {	
		
	
	float scbdprice;
	float scpsprice;
	
		WebDriver driver = MySharedClass.getDriver();
		private final Logger log = LogManager.getLogger();
		
		private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, WebDriverException.class);
		
	    @Then("^user selects prepaid check in$")
	    public float user_selects_prepaid_check_in() throws Throwable 
	    {
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptionDD)));
	    	String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
	    	float indexTotal = Float.parseFloat(startTotalAmount); 
			driver.findElement(By.xpath(OptionsPage.checkinptionDD)).click();
			log.info("user clicks on the Check in option drop down");
			
			driver.findElement(By.xpath(OptionsPage.checkinPrepaid)).click();
			
			String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
	    	float finalTotal = Float.parseFloat(finalTotalAmount); 

	        DecimalFormat df = new DecimalFormat("##.##");
	        df.setRoundingMode(RoundingMode.CEILING);
	        
	        // difference to 2 deimal places
	    	Float difference =(finalTotal - indexTotal);
	    	String optTotal = df.format(difference);

	    	float totalFloat = Float.valueOf(optTotal);
			boolean total = (totalFloat) == 10.00;
    		if (total) 
    		{
    			log.info("Expected amount is correct: " + finalTotalAmount);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect");
    			assertTrue(total);
    		} 
    		return finalTotal;
	    }
	    
	 /*   @Then("^user validates the options total is correct \\\"([^\\\"]*)\\\"$")
	    public void user_validates_the_options_total_is_correct(String total) throws Throwable 
	    {
    		String amount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText();
			
    		boolean correctamount =  (amount.equals(total));
    		if (correctamount) 
    		{
    			log.info("Expected amount is correct: " + total);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect: " + amount);
    			assertTrue(correctamount);
    		}    			
	    }	*/    

	    @Then("^user validates that they are not charged incorrectly \\\"([^\\\"]*)\\\"$")
	    public void user_validates_that_they_are_not_charged_incorrectly(String correct) throws Throwable 
	    {
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.purchaseTotal)));
	    	driver.findElement(By.xpath(OptionsPage.purchaseTotal)).click();
	    	driver.findElement(By.xpath(OptionsPage.optionsDD)).click();
	    	log.info("user opens the options price breakdown");

	    	String amount = driver.findElement(By.xpath(OptionsPage.checkinoptionPrice)).getText();
	    		System.out.println(amount + " " + correct);
	    	boolean correctamount = amount.equals(correct);
    		if (correctamount) 
    		{
    			log.info("Expected amount is correct: " + correct);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect: " + amount);
    			assertTrue(correctamount);
    		}
	    }

	    @Then("^Validate that the check in option is disabled$")
	    public void validate_that_the_check_in_option_is_disabled() throws Throwable {
	    	//actual color of div
	    	String backgroundColor = driver.findElement(By.xpath(OptionsPage.checkinOptionDiv)).getCssValue("background-color");
	    	System.out.println(backgroundColor);
	    	
	    	//Disabled color of div
	    	String expectedColor = "rgba(239, 239, 239, 1)";
	    	
	    	boolean validateColor =  (backgroundColor.equals(expectedColor));
    		if (validateColor) 
    		{
    			log.info("Check in option disabled; PASS");
    		}
    		else
    		{	
    			log.info("Check in option IS NOT disabled; FAIL");
    			assertTrue(validateColor);
    		}  
	    }

	    @Then("^user selects Free check in$")
	    public void user_selects_free_check_in() throws Throwable 
	    {

	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptionDD)));
	    	//index total
			String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
			
			
			driver.findElement(By.xpath(OptionsPage.checkinptionDD)).click();	
			log.info("user clicks on the Check in option drop down");			

			driver.findElement(By.xpath(OptionsPage.checkinFree)).click();	

			log.info("user selects the free online check in option");
			
			String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
	    	
			boolean total = finalTotalAmount.equals(startTotalAmount);
    		if (total) 
    		{
    			log.info("Expected amount is correct: " + finalTotalAmount);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect: " + finalTotalAmount);
    			assertTrue(total);
    		}
    		
	    }
	    
	    @Then("^user selects not sure option$")
	    public void user_selects_not_sure_option() throws Throwable 
	    {
	    	
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptionDD)));
	    	
	    	//index total
			String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
			
			driver.findElement(By.xpath(OptionsPage.checkinptionDD)).click();	
			log.info("user clicks on the check in option drop down");			

			driver.findElement(By.xpath(OptionsPage.checkinNotSure)).click();	

			log.info("user clicks on the Not sure option on the drop down");
			
			String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
	    	
			boolean total = finalTotalAmount.equals(startTotalAmount);
    		if (total) 
    		{
    			log.info("Expected amount is correct: " + finalTotalAmount);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect: " + finalTotalAmount);
    			assertTrue(total);
    		}
			
	    }
	    
	    @Then("^User selects to add Flight Flex$")
	    public void user_selects_to_add_flight_flex() throws Throwable 
	    {
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.flightFlexAddBttn)));
	    	String addBttnText = driver.findElement(By.xpath(OptionsPage.flightFlexAddBttn)).getText();
			
	    	String expectedbttnText = "ADD";
	    	
	    //Validate button is enabled
	    	boolean validateColor =  (expectedbttnText.equals(addBttnText));
	    		if (validateColor) 
	    		{
	    			log.info("Flight Flex button says " + addBttnText +": PASS");
	    		}
	    		else
	    		{	
	    			log.info("Flight Flex button says " + addBttnText +": FAIL");
	    			assertTrue(validateColor);
	    		}  
	    		
	    		
		    		//price from cart
			    	String shoppingCartTtl = driver.findElement(By.xpath(OptionsPage.dynamicShoppingCart)).getText();
		    		//parse price to float
			    	shoppingCartTtl = shoppingCartTtl.replace("$","");
			    	float dynamicCart = Float.parseFloat(shoppingCartTtl);   	
	    		
    		driver.findElement(By.xpath(OptionsPage.flightFlexAddBttn)).click();	
    		log.info("Flight Flex button selected");
    		
	    			//get Flight Flex price
	    			String ffTotal = driver.findElement(By.xpath(OptionsPage.flightFlexPrice)).getText();
	    			//parse Flight Flex Price to float
	    			ffTotal = ffTotal.replace("$","");
	    			float fFlexTotal = Float.parseFloat(ffTotal);
		    	
	    			//add original cart total and Flight Flex price
	    			float newTotal = fFlexTotal +  dynamicCart; 
    		
	    			//get new price on shopping cart
	    			String shoppingCartTtl1 = driver.findElement(By.xpath(OptionsPage.dynamicShoppingCart)).getText();
	    			shoppingCartTtl1 = shoppingCartTtl1.replace("$","");
	    			float shopCartTotal = Float.parseFloat(shoppingCartTtl1);
	    			
	    			//if else true
	    		    boolean validateAddedShopCart = (shopCartTotal == newTotal);
		    		if (validateAddedShopCart) 
		    		{
		    			log.info("Dynamic shopping cart is correctly added");
		    		}
		    		else
		    		{	
		    			log.info("Dynamic shopping cart is in - correctly added");
		    			assertTrue(validateColor);
		    		} 
	    		    
	    		    
	    	 //Validate button now says remove
    		addBttnText = driver.findElement(By.xpath(OptionsPage.flightFlexAddBttn)).getText();
			
	    	expectedbttnText = "REMOVE";
	    	
	    //Validate button is enabled
	    	validateColor =  (expectedbttnText.equals(addBttnText));
	    		if (validateColor) 
	    		{
	    			log.info("Flight Flex button says" + addBttnText + ": PASS");
	    		}
	    		else
	    		{	
	    			log.info("Flight Flex button says" + addBttnText + " FAIL");
	    			assertTrue(validateColor);
	    		} 	 
	    }
	    
	    @Then("^User selects to add shortcut boarding$")
	    public float user_selects_to_add_shortcut_boarding() throws Throwable
	    {
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutBoardingAddBttn)));
	    	String addBttnText = driver.findElement(By.xpath(OptionsPage.shortCutBoardingAddBttn)).getText();
			
	    	String expectedbttnText = "ADD";
	    	
	    //Validate button is enabled
	    	boolean validateColor =  (expectedbttnText.equals(addBttnText));
	    		if (validateColor) 
	    		{
	    			log.info("shortcut boarding button says " + addBttnText +": PASS");
	    		}
	    		else
	    		{	
	    			log.info("shortcut boarding button says " + addBttnText +": FAIL");
	    			assertTrue(validateColor);
	    		}  
	    	
				//index total
				String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
		    	//convert to float
				float indexTotal = Float.parseFloat(startTotalAmount); 
				
				//get options price
				String scbdFee = driver.findElement(By.xpath(OptionsPage.shortCutBoardingPrice)).getText().replace("$","");
				//convert to float
				float scbdprice = Float.parseFloat(scbdFee);
								
    		driver.findElement(By.xpath(OptionsPage.shortCutBoardingAddBttn)).click();	
    		log.info("ShortCut Boarding button selected");
	    		    	
	    //Validate button now says remove
    		addBttnText = driver.findElement(By.xpath(OptionsPage.shortCutBoardingAddBttn)).getText();
			
	    	expectedbttnText = "REMOVE";
	    	
	    //Validate button is enabled
	    	validateColor =  (expectedbttnText.equals(addBttnText));
	    		if (validateColor) 
	    		{
	    			log.info("shortcut boarding button says" + addBttnText + ": PASS");
	    		}
	    		else
	    		{	
	    			log.info("shortcut boarding button says" + addBttnText + " FAIL");
	    			assertTrue(validateColor);
	    		} 	 
	    	
	    		//get new amount
				String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
		    	float finalTotal = Float.parseFloat(finalTotalAmount); 
		    	
		        DecimalFormat df = new DecimalFormat("##.##");
		        df.setRoundingMode(RoundingMode.CEILING);
		        
		    	String total1 = df.format(indexTotal + scbdprice);
		    	float totalFloat = Float.valueOf(total1);
		    	    	
		    	
				boolean total = (totalFloat) == finalTotal;
	    		if (total) 
	    		{
	    			log.info("Expected amount is correct: " + finalTotalAmount);
	    		}
	    		else
	    		{	
	    			log.info("Expected amount is incorrect: " + total1);
	    			assertTrue(total);
	    		}

				return scbdprice;
	    }
	    

		@Then("^user clicks first available flight$")
	    public void user_clicks_first_available_flight() throws Throwable
	    {
			//user clicks month view
			driver.findElement(By.xpath("//label[contains(text(),'Month')]")).click();
			
			//loop for week
			for  (int i = 1 ; i <= 6 ; i++)
			{

				//loop for days
				for  (int j = 1 ; j <= 7 ; j++)
				{
					String week = Integer.toString(i);
					String day = Integer.toString(j);
					String dateBox = "//div[@class='d-flex justify-content-center ng-star-inserted'][" + week +"]/app-low-fare-day["+ day + "]";
					
					Thread.sleep(500);
					
					//try to click on date catch if not clickable
					 try 
					    {
							driver.findElement(By.xpath(dateBox)).click(); //click on each date box
					    } 
					 catch (NoSuchElementException e) {}	
					
					//search for flight available text box catch if no element displayed
				    try 
				    {
				    	if (driver.findElement(By.xpath("//div[@class='d-flex flex-column justify-content-center align-items-center ng-star-inserted']")).isDisplayed())
				    		{
				    			driver.findElement(By.xpath("//div[@class='d-flex flex-column justify-content-center align-items-center ng-star-inserted']")).click();
								//end loop
				    			i = 7;
								j = 8;
				    		}
				    	else {}
				    	
				    } 
				    catch (NoSuchElementException e) {}			
				}	
			}
	    }
		

	 /*	@Then("^user validates that they are charged correctly for prepaid check in$")
	    public void user_validates_that_they_are_charged_correctly_for_prepaid_check_in() throws Throwable
	    {
	 		System.out.println("test");
	 		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PurchasePage.purchaseTotal)));
	    	driver.findElement(By.xpath(PurchasePage.purchaseTotal)).click();
	    	driver.findElement(By.xpath(PurchasePage.optionsDD)).click();
	    	log.info("user opens the options price breakdown");

	    	String amount = driver.findElement(By.xpath(PurchasePage.checkinoptionPrice)).getText().replace("$","");
	    	float checkInTotal = Float.parseFloat(amount); 
	    	System.out.println(checkInTotal);
			
	    	float totalold = finalTotal;
	    	System.out.println(totalold);
	    	
	    	boolean correctamount = checkInTotal == totalold;
	   		if (correctamount) 
    		{
    			log.info("Expected amount is correct: " + checkInTotal);
    		}
    		else
    		{	
    			log.info("Expected amount is incorrect: " + checkInTotal);
    			assertTrue(correctamount);
    		}
	    }*/

			@Then("^user clicks first available departing flight$")
			public void user_clicks_first_available_departing_flight() throws Throwable 
	        {
	            //user clicks month view
//	            driver.findElement(By.xpath("//section/div[4]/app-fare-picker/div/div/div[2]/app-low-fare/div/div[1]/div[2]/div/div[1]/label[2]")).click();
				
				driver.findElement(By.xpath("//section[@class='ng-star-inserted']//div[@class='mb-2 ng-star-inserted'][1]//div[@class='col-xl-9 btn-group btn-group-toggle view-toggle justify-content-lg-end']//label[@class='btn btn-secondary-pil']")).click();
	            
	            //loop for week
	            for  (int i = 1 ; i <= 6 ; i++)
	            {

	                //loop for days
	                for  (int j = 1 ; j <= 7 ; j++)
	                {
	                    String week = Integer.toString(i);
	                    String day = Integer.toString(j);
//	                    String dateBox = "//section/div[4]/app-fare-picker/div/div/div[2]/app-low-fare/div/div[2]/div/div/div/div/div[" + week +"]/app-low-fare-day["+ day + "]";
	                    String dateBox ="//section[@class='ng-star-inserted']//div[@class='mb-2 ng-star-inserted']/app-fare-picker//div[@class='d-flex justify-content-center ng-star-inserted']["+ week +"]/app-low-fare-day["+ day +"]";
	                    
	                    Thread.sleep(500);
	                    
	                    //try to click on date catch if not clickable
	                     try
	                        {
	                    	 Thread.sleep(3000);
	                            driver.findElement(By.xpath(dateBox)).click(); //click on each date box
	                        }
	                     catch (NoSuchElementException e) {}    
	                    
	                    //search for flight available text box catch if no element displayed
	                    
	                    try
	                    {
	                    	
	                        if (driver.findElement(By.xpath("//div[@class='d-flex flex-column']//div[@class='d-flex justify-content-center ng-star-inserted']//button[@class='day d-flex flex-column justify-content-between align-items-stretch p-0']")).isDisplayed())
	                            {	
	                        	
	                        	
	                        	
	                        		Thread.sleep(4000);

	                        		
	                                driver.findElement(By.xpath("//div[@class='flight-card-container']//div[@class='d-flex flex-column justify-content-center align-items-center ng-star-inserted']")).click();
	                               
	                                
	                                //end loop
	                                i = 7;
	                                j = 7;
	                                
	                                
	                            }
	                        
	                    
	                     
	                        else {
	                        	
	                        	 
	                        	
	                        	
	                        }
	                        
	                    }
	                    catch (NoSuchElementException e) {}            
	               
	                     }
	                     }    
	            }
	        
			
	        @Then("^User clicks on first available return flight$")
	        public void user_clicks_on_first_available_return_flight() throws Throwable 
	        {
	        	Thread.sleep(3000);
	        	//user clicks month view
	            driver.findElement(By.xpath("//section/div[5]//app-low-fare/div/div[1]/div[2]/div/div[1]/label[2]")).click();
	            
	            //loop for week
	            for  (int i = 1 ; i <= 6 ; i++)
	            {

	                //loop for days
	                for  (int j = 1 ; j <= 8 ; j++)
	                {
	                    String week = Integer.toString(i);
	                    String day = Integer.toString(j);
//	                    String dateBox = "//section/div[5]/app-fare-picker/div/div/div[3]/div[2]/app-low-fare/div/div[2]/div/div/div/div/div[" + week +"]/app-low-fare-day["+ day + "]";
	                    
	                    String dateBox="//section/div[5]//div[@class='d-flex justify-content-center ng-star-inserted']["+week+"]/app-low-fare-day["+ day +"]";
	                    
	                    Thread.sleep(3000);
	                    
	                    //try to click on date catch if not clickable
	                     try
	                        {
	                            driver.findElement(By.xpath(dateBox)).click(); 
	                        }
	                     catch (NoSuchElementException e) {}    
	                    
	                    //search for flight available text box catch if no element displayed
	                    try
	                    {
	                        if (driver.findElement(By.xpath("//section/div[5]//div[@class='flight-card-container']")).isDisplayed())
	                            {	
	                        	System.out.println("test");
	                        		Thread.sleep(2000);
	                                driver.findElement(By.xpath("//section/div[5]//div[@class='d-flex flex-column justify-content-center align-items-center ng-star-inserted']//label")).click();
	                                //end loop
	                                i = 7;
	                                j = 8;
	                            }
	                        else {}
	                        
	                    }
	                    catch (NoSuchElementException e) {}            
	                }    
	            }
	        }
	        
	        @Then("^user selects prepaid check in RT to LIM$")
	        public void user_selects_prepaid_check_in_rt_to_lim() throws Throwable
	        {

		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptionDD)));
		    	String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
		    	float indexTotal = Float.parseFloat(startTotalAmount); 
				driver.findElement(By.xpath(OptionsPage.checkinptionDD)).click();	
				log.info("user clicks on the Check in option drop down");
				
				driver.findElement(By.xpath(OptionsPage.checkinPrepaid)).click();
				String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
		    	float finalTotal = Float.parseFloat(finalTotalAmount); 
		    	
				boolean total = (indexTotal + 10.00) == finalTotal;
	    		if (total) 
	    		{
	    			log.info("Expected amount is correct: " + finalTotalAmount);
	    		}
	    		else
	    		{	
	    			log.info("Expected amount is incorrect: " + finalTotalAmount);
	    			assertTrue(total);
	    		} 
	        }
	        
	        @Then("^User selects to add shortcut security$")
	        public float user_selects_to_add_shortcut_security() throws Throwable
	        {
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutSecurityAddBttn)));
		    	String addBttnText = driver.findElement(By.xpath(OptionsPage.shortCutSecurityAddBttn)).getText();
				
		    	String expectedbttnText = "ADD";
		    	
		    //Validate button is enabled
		    	boolean validateText =  (expectedbttnText.equals(addBttnText));
		    		if (validateText) 
		    		{
		    			log.info("shortcut security button says " + addBttnText +": PASS");
		    		}
		    		else
		    		{	
		    			log.info("shortcut security button says " + addBttnText +": FAIL");
		    			assertTrue(validateText);
		    		}  
		    	
					//index total
					String startTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
			    	//convert to float
					float indexTotal = Float.parseFloat(startTotalAmount); 
					
					//get options price
					String scpsFee = driver.findElement(By.xpath(OptionsPage.shortCutSecurityPrice)).getText().replace("$","");
					//convert to float
					float scpsprice = Float.parseFloat(scpsFee);
					
					Thread.sleep(2000);
	    		driver.findElement(By.xpath(OptionsPage.shortCutSecurityAddBttn)).click();	
	    		log.info("ShortCut security button selected");
	    		
	    		try 
	    		{
	    			driver.findElement(By.xpath(OptionsPage.shortCutSecuritychckBox)).click();
	    			driver.findElement(By.xpath(OptionsPage.shortCutSecurityPopUpBttn)).click();	    			
	    		}

                catch (NoSuchElementException e) {}       
		    		    	
	    		Thread.sleep(5000);
		    //Validate button now says remove
	    		addBttnText = driver.findElement(By.xpath(OptionsPage.shortCutSecurityAddBttn)).getText();
				
		    	expectedbttnText = "CHANGE";
		    	
		    //Validate button is enabled
		    	validateText =  (expectedbttnText.equals(addBttnText));
		    		if (validateText) 
		    		{
		    			log.info("shortcut security button says " + addBttnText + ": PASS");
		    		}
		    		else
		    		{	
		    			log.info("shortcut security button says " + addBttnText + " FAIL");
		    			assertTrue(validateText);
		    		} 	 
		    	
		    		Thread.sleep(1000);
		    		//get new amount
					String finalTotalAmount = driver.findElement(By.xpath(OptionsPage.optionsTotal)).getText().replace("$","");
			    	
					float finalTotal = Float.parseFloat(finalTotalAmount); 
			    	
			        DecimalFormat df = new DecimalFormat("##.##");
			        df.setRoundingMode(RoundingMode.CEILING);
			        
			    	String total1 = df.format(indexTotal + scpsprice);
			    	float totalFloat = Float.valueOf(total1);
			    	    	
			    	
					boolean total = (totalFloat) == finalTotal;
		    		if (total) 
		    		{
		    			log.info("Expected amount is correct: " + finalTotalAmount);
		    		}
		    		else
		    		{	
		    			log.info("Expected amount is incorrect: " + total1);
		    			assertTrue(total);
		    		}
		    		Thread.sleep(2000);
		    		return scpsprice;
	        	}

	        @Then("^user validates that the Shortcut boarding outline is green$")
	        public void user_validates_that_the_shortcut_boarding_outline_is_green() throws Throwable 
	        {
	        	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutBoardingBorder)));
	        	 Thread.sleep(5000);
	        
	        	 String borderColor = driver.findElement(By.xpath(OptionsPage.shortCutBoardingBorder)).getCssValue("border-bottom-color");
	 	    		 	    	
	 	    	//preselected option border color
	 	    	String expectedColor = "rgba(47, 127, 41, 1)";
	 	    	
	 	    	boolean validateColor =  (borderColor.equals(expectedColor));
	     		if (validateColor) 
	     		{
	     			log.info("Shortcut boarding boarder green; PASS");
	     		}
	     		else
	     		{	
	     			log.info("Shortcut boarding boarder is NOT green; FAIL");
	     			assertTrue(validateColor);
	     		}
	        }
	        
	        @Then("^user validates that the Shortcut security outline is green$")
	        public void user_validates_that_the_shortcut_security_outline_is_green() throws Throwable 
	        {
	        	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutSecurityborder)));
	        	 Thread.sleep(5000);
	        
	        	 String borderColor = driver.findElement(By.xpath(OptionsPage.shortCutSecurityborder)).getCssValue("border-bottom-color");
	 	    		 	    	
	 	    	//preselected option border color
	 	    	String expectedColor = "rgba(47, 127, 41, 1)";
	 	    	
	 	    	boolean validateColor =  (borderColor.equals(expectedColor));
	     		if (validateColor) 
	     		{
	     			log.info("Shortcut security boarder is green; PASS");
	     		}
	     		else
	     		{	
	     			log.info("Shortcut security boarder is NOT green; FAIL");
	     			assertTrue(validateColor);
	     		}
	        }
	     		
	     		 @Then("^user validates that the Shortcut boarding outline is green when carry on added$")
	     	    public void user_validates_that_the_shortcut_boarding_outline_is_green_when_carry_on_added() throws Throwable 
	     		{
		        	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutBoardingBorderWithCarryOn)));
		        	 Thread.sleep(5000);
		        
		        	 String borderColor = driver.findElement(By.xpath(OptionsPage.shortCutBoardingBorderWithCarryOn)).getCssValue("border-bottom-color");
		 	    		 	    	
		 	    	//preselected option border color
		 	    	String expectedColor = "rgba(47, 127, 41, 1)";
		 	    	
		 	    	boolean validateColor =  (borderColor.equals(expectedColor));
		     		if (validateColor) 
		     		{
		     			log.info("Check in option disabled; PASS");
		     		}
		     		else
		     		{	
		     			log.info("Check in option IS NOT disabled; FAIL");
		     			assertTrue(validateColor);
		     		}
	     	    }	       
	     		 
	     	    @Then("^user validates that Car Rentals, Hotels, and Activities is not displayed$")
	     	    public void user_validates_that_car_rentals_hotels_and_activities_is_not_displayed() throws Throwable 
	     	    {
	     	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptionDD)));
	     	    	
	     	    	try 
	     	    	{
	     	    		driver.findElement(By.xpath(OptionsPage.viewAllCarOptions)).click();
		     			log.info("Car Upsell is displayed; Fail");
		     			assertTrue(false);
	     	    	}
	     	    	catch(NoSuchElementException e)
	     	    	{ log.info("Car Upsell is not displayed; Pass"); }
	     	    
	     	    	try 
	     	    	{
	     	    		driver.findElement(By.xpath(OptionsPage.viewAllHotelsOption)).click();
		     			log.info("hotel Upsell is displayed; Fail");
		     			assertTrue(false);
	     	    	}
	     	    	catch(NoSuchElementException e)
	     	    	{ log.info("Hotel Upsell is not displayed; Pass"); }
	     	    	
	     	    	try 
	     	    	{
	     	    		driver.findElement(By.xpath(OptionsPage.dealsCarausellViewAll)).click();
		     			log.info("Activity Upsell is displayed; Fail");
		     			assertTrue(false);
	     	    	}
	     	    	catch(NoSuchElementException e)
	     	    	{ log.info("Activity Upsell is not displayed; Pass"); }
	     	    }
	     	    
     	    @Then("^user verifies all Extras are displayed$")
     	    public void user_verifies_all_extras_are_displayed() throws Throwable 
     	    {
     	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.flightFlexAddBttn)));
     	    	log.info("Flight Flex is displayed successfully");
     	    	
     	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutSecurityAddBttn)));
     	    	log.info("shortCut Security is displayed successfully");
     	    	
     	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.shortCutBoardingAddBttn)));
     	    	log.info("ShortCut Boarding is displayed successfully");
     	    }
     	    
     	    @Then("^user validates that the check in div border is green$")
     	    public void user_validates_that_the_check_in_div_border_is_green() throws Throwable 
     	    {

	        	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.checkinptiondiv)));
	        	 Thread.sleep(5000);
	        
	        	 String borderColor = driver.findElement(By.xpath(OptionsPage.checkinptiondiv)).getCssValue("border-bottom-color");
	 	    	
	 	    	//preselected option border color
	 	    	String expectedColor = "rgba(47, 127, 41, 1)";
	 	    	
	 	    	boolean validateColor =  (borderColor.equals(expectedColor));
	     		if (validateColor) 
	     		{
	     			log.info("check in border is green; PASS");
	     		}
	     		else
	     		{	
	     			log.info("Shortcut security border is NOT green; FAIL");
	     			assertTrue(validateColor);
	     		}
     	    }
 	    @Then("^user validates the Activities on page$")
 	    public void user_validates_the_activities_on_page() throws Throwable 
 	    {
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.dealsHeader)));
 	    	log.info("User lands on the options page");
 	    	
 	    	//validate text is bold
 	    	
 	    	 Assert.assertTrue("The Deals header is the correct font",
 	    		driver.findElement(By.xpath(OptionsPage.dealsHeader)).getCssValue("font-size").equals("32px"));
 			
 	    	 //validate there are 4  rectangular tiles
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activity1)));
 			log.info("Activity 1 is displayed");
 	    	
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activity2)));
 	    	log.info("Activity 2 is displayed");
 	    	
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activity3)));
 	    	log.info("Activity 3 is displayed");
 	    	
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activity4)));
 	    	log.info("Activity 4 is displayed");
 	    	
 	    	boolean act5 = driver.findElement(By.xpath(OptionsPage.activity5)).isDisplayed(); 	    	
 	    	assertFalse(act5);
 	    	log.info("Activity 5 is not visible: Pass");
 	    	
 	    	//validate left and right carot on carousell is blue
 	    	String blueColor = "rgba(0, 115, 230, 1)";
 	    	
 	    	 Assert.assertTrue("The left Activity chevron is not blue",
 	 	    		driver.findElement(By.xpath(OptionsPage.dealsCarausellLeft)).getCssValue("color").equals(blueColor));
 	 			
 	    	 Assert.assertTrue("The right Activity chevron is not blue",
  	 	    		driver.findElement(By.xpath(OptionsPage.dealsCarausellRight)).getCssValue("color").equals(blueColor));
  	 			
 	    	 //Validate view all ativities 	    	
 	    	 Assert.assertTrue("view all activities verbiage on button is correct",
   	 	    		driver.findElement(By.xpath(OptionsPage.dealsCarausellViewAll)).getText().equals("VIEW ALL ACTIVITIES"));
   	    }
 	    
 	    @Then("^user clicks on view all activities$")
 	    public void user_clicks_on_view_all_activities() throws Throwable 
 	    {
 	    	driver.findElement(By.xpath(OptionsPage.dealsCarausellViewAll)).click();
 	    }
 	    
 	    @When("^user lands on activities page$")
 	    public void user_lands_on_activities_page() throws Throwable 
 	    {
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activitiesHeading)));
 	    	log.info("activities page is successfully reached");
 	    }
 	    
 	    @Then("^user validates the activities heading$")
 	    public void user_validates_the_activities_heading() throws Throwable 
 	    {
 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.activitiesHeadingdiv)));
 	    	String headingAlign = driver.findElement(By.xpath(OptionsPage.activitiesHeadingdiv)).getCssValue("text-align");
 	    	Assert.assertTrue("The Heading is not left aligned",
 	    			headingAlign.equals("left"));
 	    	 	    	
 	    	Assert.assertTrue("The Heading is not displaying correct verbiage",
 	    			driver.findElement(By.xpath(OptionsPage.activitiesHeading)).getText().equals("Let's Have Some Fun"));
 	    }

	       @Then("^user clicks on view all cars$")
	      public void user_clicks_on_view_all_cars() throws Throwable 
	      {
	    	   driver.findElement(By.xpath(OptionsPage.viewAllCarOptions)).click();
	      }
 	    
	       @When("^user lands on cars page$")
	       public void user_lands_on_cars_page() throws Throwable {
	    	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.carsHeading)));
	 	    	log.info("cars page is successfully reached");
	       }
	       
	 	    @Then("^user validates the cars heading$")
	 	    public void user_validates_the_cars_heading() throws Throwable 
	 	    {
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.carsHeadingdiv)));
	 	    	String headingAlign = driver.findElement(By.xpath(OptionsPage.carsHeadingdiv)).getCssValue("text-align");
	 	    	Assert.assertTrue("The Heading is not left aligned",
	 	    			headingAlign.equals("left"));
	 	    	
	 	    	Assert.assertTrue("The Heading is not displaying correct verbiage",
	 	    			driver.findElement(By.xpath(OptionsPage.carsHeading)).getText().equals("Choose Your Car"));
	 	    
	 	    	log.info("cars heading is correctly displayed");
	 	    }
	 	    
	 	    @Then("^user validates the search boxes$")
	 	    public void user_validates_the_search_boxes() throws Throwable 
	 	    {
	 	       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.carsCarNameSearch)));
	 	    	log.info("Car name text box is displayed");

	 	       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.carsSortBy)));
	 	    	log.info("Car Sort By drop down is displayed");

	 	       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.carsDisplaynum)));
	 	    	log.info("Car Display number drop down is displayed");
	 	    }
	 	    
	 	    @Then("^user validates the first car box$")
	 	    public void user_validates_the_first_car_box() throws Throwable 
	 	    {
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstAvailableCar)));
	 	    	log.info("First car is displayed");
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarCompanyIMG)));
	 	    	log.info("First car company image is displayed");
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarPictureIMG)));
	 	    	log.info("First car picture is displayed");
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarCompanyNameandcarSize)));
	 	    	log.info("Car company and car size is displayed");
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarModel)));
	 	    	log.info("Car model is displayed");
	 	    	
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarMoreInfoLink)));
	 	    	log.info("Car more info link is displayed");
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarPrice)));
	 	    	log.info("Car price is displayed");
	 	    	
	 	    	Assert.assertTrue("The excludes taxes virbage is not correct",
	 	    			driver.findElement(By.xpath(OptionsPage.firstCarExludeTaxes)).getText().equals("excludes taxes"));
	 	    	log.info("excludes taxes verbiage is correct");
	 	   		 	   		
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarPrice)));
	 	    	log.info("Car price is displayed");
	 	    	
	 	    	String blueColor = "rgba(0, 115, 230, 1)";
	 	    	
	 	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OptionsPage.firstCarBookBttn)));
	 	   	
	 	    	 Assert.assertTrue("The BOOK CAR button is not blue",
	 	 	    		driver.findElement(By.xpath(OptionsPage.firstCarBookBttn)).getCssValue("background-color").equals(blueColor));
	 	 	 }
	 	   @Then("^User choose from city FLL$")
	 	   public void user_choose_from_city_fll() throws Throwable {
	 	       
	 	       Thread.sleep(2000);
	 	       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
	 	       driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
	 	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RoundtripPage.select_from_city)));

	 	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(RoundtripPage.select_from_city)));
	 	    driver.findElement(By.xpath(RoundtripPage.select_from_city)).click();

	 	   }
	 	   
	 	   
	 	   
	 	  @Then("^User choose To city LAS$")
	 	   public void user_choose_to_city_las() throws Throwable {
	 	             
	 	       driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();

	 	       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LAS)));
	 	      driver.findElement(By.xpath(SeatsPage320.TO_city_LAS)).click();

	 	   }
	 	  
	 	 @Then("^user enters the nine passnegers information$")
	     public void user_enters_the_nine_passnegers_information() throws Throwable {
	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_Title1)));
	         Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title1)));
	         s.selectByVisibleText("Mr.");

	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("jack");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("flyer");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).sendKeys("12/12/1989");
	         log.info("user succesfully enters the primary passneger information ");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_title)));
	         Select s1 = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
	         s1.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Anthony");
	         driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("cardona");
	         driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("12/24/1988");
	         log.info("user successfully enters the passneger two information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_three_Title)));
	         Select s2 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_three_Title)));
	         s2.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_first_name)).sendKeys("stephen");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_last_name)).sendKeys("hawking");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_three_dateof_birth)).sendKeys("12/14/1988");
	         log.info("user successfully enters the passneger three information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_four_Title)));
	         Select s3 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_four_Title)));
	         s3.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_first_name)).sendKeys("steve");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_last_name)).sendKeys("jobs");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_four_dateof_birth)).sendKeys("12/10/1988");
	         log.info("user successfully enters the passneger four information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_five_Title)));
	         Select s4 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_five_Title)));
	         s4.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_first_name)).sendKeys("alfonso");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_last_name)).sendKeys("canels");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_five_dateof_birth)).sendKeys("12/10/1988");
	         log.info("user successfully enters the passneger five information");
	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_six_Title)));
	         Select s5 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_six_Title)));
	         s5.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_six_first_name)).sendKeys("John");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_six_last_name)).sendKeys("Snow");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_six_dateof_birth)).sendKeys("12/01/1980");
	         log.info("user successfully enters the passenger six information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_seven_Title)));
	         Select s6 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_seven_Title)));
	         s6.selectByVisibleText("Mr.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_seven_first_name)).sendKeys("Tom");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_seven_last_name)).sendKeys("Brady");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_seven_dateof_birth)).sendKeys("01/01/1980");
	         log.info("user successfully enters the passenger seven information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_eight_Title)));
	         Select s7 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_eight_Title)));
	         s7.selectByVisibleText("Ms.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_eight_first_name)).sendKeys("Adriana");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_eight_last_name)).sendKeys("Lima");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_eight_dateof_birth)).sendKeys("03/12/1990");
	         log.info("user successfully enters the passenger 8 information");

	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adullt_nine_Title)));
	         Select s8 = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_nine_Title)));
	         s8.selectByVisibleText("Ms.");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_nine_first_name)).sendKeys("Mila");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_nine_last_name)).sendKeys("Kunis");
	         driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_nine_dateof_birth)).sendKeys("03/12/1990");
	         log.info("user successfully enters the passenger 9 information");

	     }
	 	 
	 	 
	 	 
	 	
	 	    
	}	

