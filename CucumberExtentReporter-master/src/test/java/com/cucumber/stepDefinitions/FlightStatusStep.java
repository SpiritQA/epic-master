package com.cucumber.stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.FlightStatusPage;
import com.cucumber.pages.MultiCityPage;
import com.cucumber.runner.TestRunner;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FlightStatusStep {
	private static final Logger log = LogManager.getLogger();
	WebDriver driver = MySharedClass.getDriver();
	
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	
	@When("^User Clicks on Flight Status link$")
	public void user_Clicks_on_Flight_Status_link() throws Throwable {
	    Thread.sleep(6000);
		driver.findElement(By.xpath(FlightStatusPage.Flight_status)).click();
//		driver.findElement(By.xpath("//span[text()='Flight Status']")).click();
		
		log.info("user clicks on flight status link");
		
	}

	@Then("^User selects the check by destination$")
	public void user_selects_the_check_by_destination() throws Throwable {
		
		driver.findElement(By.xpath(FlightStatusPage.checkby_destination)).click();
		
		log.info("user check by the destination");
	}

	@Then("^User enters the departure city$")
	public void user_enters_the_departure_city() throws Throwable {
		driver.findElement(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)).click();
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)).sendKeys("Fort Lauderdale, FL");
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)).sendKeys(Keys.DOWN);
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)).sendKeys(Keys.ENTER);
    	
    	log.info("user enters the departure city");
	}

	@Then("^user enters the arival city$")
	public void user_enters_the_arival_city() throws Throwable {
		
		driver.findElement(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)).click();
    	driver.findElement(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)).sendKeys("Houston, TX - Intercontinental");
    	driver.findElement(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)).sendKeys(Keys.DOWN);
    	driver.findElement(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)).sendKeys(Keys.ENTER);
	  log.info("user enters the arival city");
	}

	@Then("^User selects the day of journey$")
	public void user_selects_the_day_of_journey() throws Throwable {
		driver.findElement(By.xpath(FlightStatusPage.Flight_status_Select_day)).click();
    	Select s= new Select(driver.findElement(By.xpath(FlightStatusPage.Flight_status_Select_day)));
    	s.selectByIndex(4);
    	log.info("user selects the day of journey");
	}

	@Then("^User clicks on check status$")
	public void user_clicks_on_check_status() throws Throwable {
		driver.findElement(By.xpath(FlightStatusPage.check_status)).click();
		
		log.info("user click on check status");
	}

	@Then("^User selects the check by flight number$")
	public void user_selects_the_check_by_flight_number() throws Throwable {
	   
		driver.findElement(By.xpath(FlightStatusPage.checkby_Flight_number)).click();
		log.info("user selects the check by the flight number");
	}

	@Then("^User enters the flight number$")
	public void user_enters_the_flight_number() throws Throwable {
	    driver.findElement(By.xpath(FlightStatusPage.Flight_number)).sendKeys("1234");
	    
	    log.info("user enters the flight number");
	    
	}


    @Then("^user validates the text on the search widget$")
    public void user_validates_the_text_on_the_search_widget() throws Throwable {
        
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_text_validation)));
    	String s =  driver.findElement(By.xpath(FlightStatusPage.flight_status_text_validation)).getText();
    	
    	log.info("search widget text is " + s);
    	
    	
    	
    }

    @Then("^user validates the default date$")
    public void user_validates_the_default_date() throws Throwable {
       
    	String s= driver.findElement(By.xpath(FlightStatusPage.Flight_status_Select_day)).getAttribute("value");
    	log.info("default date is "+ s);
    	
    }

    @Then("^user clicks on the checkstatus and validate the error icon at fromcity$")
    public void user_clicks_on_the_checkstatus_and_validate_the_error_icon_at_fromcity() throws Throwable {
        
    	driver.findElement(By.xpath(FlightStatusPage.check_status)).click();
    	Assert.assertTrue("User not validates the error icon at from city search widget",
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_from_city_text_validation)).getText().equals("From is required"));
    	log.info("user succesfully validates the error icon at the from city");
    	

    }

//    @Then("^user clicks on the checkstatus and validate the error icon at Tocity$")
//    public void user_clicks_on_the_checkstatus_and_validate_the_error_icon_at_tocity() throws Throwable {
//    	driver.findElement(By.xpath(FlightStatusPage.check_status)).click();
//    	Assert.assertTrue("User not validates the error icon at from city search widget",
//    	driver.findElement(By.xpath(FlightStatusPage.flight_status_to_city_text_validation)).getText().equals("To is required"));
//    	log.info("user succesfully validates the error icon at the To city");
//    
//    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_from_cities_drop_down_close)));
//    	driver.findElement(By.xpath(FlightStatusPage.flight_status_from_cities_drop_down_close)).click();
//    	log.info("user succesfuly clicks on the cities drop down close button");
//    }
    
    @Then("^user clicks on the from city and select the city$")
    public void user_clicks_on_the_from_city_and_select_the_city() throws Throwable {
    	
 	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)));
 	driver.findElement(By.xpath(FlightStatusPage.flight_status_clicks_Departure_city)).click();
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.select_departure_city)));
       driver.findElement(By.xpath(FlightStatusPage.select_departure_city)).click();
    	log.info("user succesfully select the departure city ");
    	
    	
    }

    @Then("^user clicks on the to city and select the city$")
    public void user_clicks_on_the_to_city_and_select_the_city() throws Throwable {
       

    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)));
       driver.findElement(By.xpath(FlightStatusPage.flight_statusclicks_Arrival_city)).click();
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.select_arrival_city)));
       driver.findElement(By.xpath(FlightStatusPage.select_arrival_city)).click();
    	log.info("user succesfully select the arrival city ");
    	
    	
    	
    }

    @Then("^user clicks on the checkstatus button$")
    public void user_clicks_on_the_checkstatus_button() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.check_status)));
       driver.findElement(By.xpath(FlightStatusPage.check_status)).click();
    	log.info("user successfully clicks on the checkstatus button");
    	
    	
    }

    @Then("^user clicks on the get updates button$")
    public void user_clicks_on_the_get_updates_button() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_get_updates)));
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_get_updates)).click();
    	log.info("user successfully clicks on the get updates");
    }

    @Then("^user clicks on the cancel button on flight status page$")
    public void user_clicks_on_the_cancel_button_on_flight_status_page() throws Throwable {
        
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_cancel_button)));
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_cancel_button)).click();
    	log.info("user successfully clicks on the cancel button");
    }
    	
    

    @Then("^user enters the email and clicks on the get updates button and switch to the popup and close the popup$")
    public void user_enters_the_email_and_clicks_on_the_get_updates_button_and_switch_to_the_popup_and_close_the_popup() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_get_updates)));
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_get_updates)).click();
    	log.info("user successfully clicks on the get updates");
    	
      	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_input_email)));
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_input_email)).sendKeys("jack.flyer@spirit.com");
    	log.info("user succesfully enters the email address");
    	
    	String parentHandle = driver.getWindowHandle();
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_getupdates_email)).click();
    	for (String winHandle : driver.getWindowHandles()) {
    	    driver.switchTo().window(winHandle); 
    	}
    	Thread.sleep(3000);
    	driver.findElement(By.xpath(FlightStatusPage.flight_status_popup_close_button)).click();
    	log.info("user succesfully clicks on the popup close button");
    	
    	driver.switchTo().window(parentHandle);
    }
    
    @Then("^user selects the new destination and date and check the flight status$")
    public void user_selects_the_new_destination_and_date_and_check_the_flight_status() throws Throwable {
       Thread.sleep(2000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_new_destination_departure_city_click)));
     	driver.findElement(By.xpath(FlightStatusPage.flight_status_new_destination_departure_city_click)).click();
           wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_new_destination_from_city)));
           driver.findElement(By.xpath(FlightStatusPage.flight_status_new_destination_from_city)).click();
        	log.info("user succesfully select the departure city ");
    	
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_new_destination_arrival_city_click)));
            driver.findElement(By.xpath(FlightStatusPage.flight_status_new_destination_arrival_city_click)).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FlightStatusPage.flight_status_new_destination_to_city)));
            driver.findElement(By.xpath(FlightStatusPage.flight_status_new_destination_to_city)).click();
         	log.info("user succesfully select the arrival city ");
    	
         	driver.findElement(By.xpath(FlightStatusPage.check_status)).click();
         	log.info("user sucesfully clicks on the check status button then new flight status should be updated");
    }

}
