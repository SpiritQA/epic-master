
package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;

import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;

public class CustomerLogInStep {
	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();

	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	private static String NineFC_valid_emailaddress = FileReaderManager.getInstance().getTestDataReader().Ninefcemail();
	private static String NineFC_Valid_Password = FileReaderManager.getInstance().getTestDataReader().Ninepassword();
	private static String UMNR_ninedfc_email = FileReaderManager.getInstance().getTestDataReader().UMNRninedfcemail();

	@Then("^user verifies Member is logged in$")
	public void user_verifies_Member_is_logged_in() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JOE"));
		log.info("User Logged in verified");
	}

	@Then("^user login as FS member who has a known traveler number$")
	public void user_login_as_fs_member_who_has_a_known_traveler_number() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a free spirit member who has a known traveler number");

	}

	@Then("^user verifies Member with known traveler is logged in$")
	public void user_verifies_Member_with_known_traveler_is_logged_in() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JACK"));
		log.info("User Logged in verified");
	}

	@Then("^user enters known traveler number$")
	public void user_enters_known_traveler_number() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("13570454nine ");
		log.info("user succesfully entered ktn");
	}

	@Then("^user enters invalid known traveler number$")
	public void user_enters_invalid_known_traveler_number() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("13570454nine ");
		log.info("user succesfully entered ktn");
	}

	// user clicks sign out
	@Then("^user log out$")
	public void user_log_out() throws Throwable {
		driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.fs_member_logout)));
		driver.findElement(By.xpath(OnewaytripPage.fs_member_logout)).click();
		log.info("User Logged out");
	}

	@Then("^User Selects Need Help ToFrom gate$")
	public void User_Selects_Need_Help_ToFrom_gate() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("Need Help ToFrom gate selected");
	}

	@Then("^User verfies Child DOB is autopopulated$")
	public void User_verfies_Child_DOB_is_autopopulated() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
		Assert.assertTrue("Child DOB is not autopopulated",
				driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText().equals("07/17/2007"));
		log.info("Child DOB verified");
	}

	@Then("^user selects I have my own wheelchair$")
	public void user_selects_I_have_my_own_wheelchair() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user selects I have my own wheelchair");
	}

	@Then("^User Selects Manual Wheelchair$")
	public void User_Selects_Manual_Wheelchair() throws Throwable {
		Select drpWheelChair = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		drpWheelChair.selectByVisibleText("Manually Powered");
		log.info("Manual WheelChair Selected");
	}

	@Then("^User selects One child$")
	public void User_selects_One_child() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		driver.findElement(By.xpath(BookPage.select_child)).click();
	}

	@Then("^user clicks on search button and enters the birthdate for UMNR$")
	public void user_enters_the_birthdate_of_the_child() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2006");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the child");
			
			for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Umnr_popup)));
			driver.findElement(By.xpath(BookPage.Umnr_popup)).click();
			
			driver.switchTo().window(parentHandle);
		}
		
	}

	@Then("^user login as UMNR FS member$")
	public void user_login_as_UMNR_FS_member() throws Throwable {
		// String parentHandle = driver.getWindowHandle(); // get the current window
		// handle
		// driver.findElement(By.xpath(PassengerinfoPage.free_spirit_login)).click(); //
		// click some link that opens a new
		// // window
		// for (String winHandle : driver.getWindowHandles()) {
		// driver.switchTo().window(winHandle);

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("umnrflyer3@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a free spirit member who is a child");
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.passenerspage_Continue_button)));
		// dob: 2/1/2008
	}

	@Then("^user verifies UMNR Member is logged in$")
	public void user_verifies_UMNR_Member_is_logged_in() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("UMNR"));
		log.info("free spirit member who is a child Logged in verified");
	}

	@Then("^user Clicks Additional Services$")
	public void user_Click_Additional_Services() throws Throwable {
		Thread.sleep(6000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.adult_Additional_services)));
		WebElement element = 	driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		log.info("User clicks on additional services");
	}

	@Then("^user selects voluntary provision of emergency services program$")
	public void user_selects_voluntary_provision_of_emergency_services_program() throws Throwable {
		Thread.sleep(2000);
//		driver.findElement(By.xpath(PassengerinfoPage.adult_Voluntry_provision_caret)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_voluntry_provision_checkbox)).click();
		log.info("User clicks on voluntary provision of emergency services program");
	}

	@Then("^user Clicks Pet In Cabin$")
	public void user_Clicks_Pet_In_Cabin() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_pet_in_cabin_Checkbox)).click();
		log.info("User clicks Pet In Cabin");
	}

	@Then("^user Clicks ESAN$")
	public void user_Clicks_ESAN() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)).click();
		log.info("User clicks ESAN");
	}

	@Then("^user login as nine DFC member$")
	public void user_login_as_nine_DFC_member() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.free_spirit_email)));

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("justinflyer@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a nine_DFC member");

	}

	@Then("^user verifies Member with nine DFC is logged in$")
	public void user_verifies_Member_with_nine_DFC_is_logged_in() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
	String login_username=	driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText();
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JUSTIN"));
		log.info("nine_DFC User Logged in verified");
	}

	@Then("^user login as nine DFC member who has a known traveler number$")
	public void user_login_as_nine_DFC_member_who_has_a_known_traveler_number() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("justinflyer@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a nine DFC member who has a known traveler number");
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.passenerspage_Continue_button)));
	}

	@Then("^user verifies nine DFC Member with known traveler is logged in$")
	public void user_verifies_nine_DFC_Member_with_known_traveler_is_logged_in() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JUSTIN"));
		log.info("User Logged in verified");
	}

	@Then("^user enters known traveler number for nine DFC Member$")
	public void user_enters_known_traveler_number_for_nine_DFC_Member() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("135701701");
		log.info("user succesfully entered ktn");
	}

	// @Then("^user clicks on login$")
	// public void user_clicks_on_login() throws Throwable {
	// driver.findElement(By.xpath(PassengerinfoPage.free_spirit_login)).click();
	// log.info("user clicks on LOGIN succesfully");
	// }

	@Then("^user enters a valid email address$")
	public void user_enters_a_valid_email_address() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("joe.flyer9dfc@spirit.com");
		log.info("user enters a valid email succesfully");
	}

	@Then("^user enters a invalid password$")
	public void user_enters_a_invalid_password() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("spirit11");
		log.info("user enters a invalid password ");
	}

	@Then("^user selects the standard barefare$")
	public void user_selects_the_standard_barefare() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Barefare_standard)));
		driver.findElement(By.xpath(OnewaytripPage.Barefare_standard)).click();
		log.info("user sucessfully clicks on the Barefare standard");

	}

	@Then("^user validates login error message$")
	public void user_validates_login_error_message() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Invalid_Email_or_Password_alert)));

		Assert.assertTrue("INVALID ERROR MESSAGE!!!",
				driver.findElement(By.xpath(PassengerinfoPage.Invalid_Email_or_Password_alert)).getText().equals(
						"Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
		log.info("error message validated");
	}

	@Then("^user enters invalid email address$")
	public void user_enters_invalid_email_address() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("joe@spirit.com");
		log.info("user enters a invalid email");
	}

	@Then("^user enters a valid password$")
	public void user_enters_a_valid_password() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");
		log.info("user enters a valid password ");
	}

	@Then("^user enters a invalid FS number$")
	public void user_enters_a_invalid_FS_number() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("461861433");
			log.info("user enters a invalid email ");
		}
	}

	@Then("^user enters valid FS number$")
	public void user_enters_valid_FS_number() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("461861422");
		log.info("user enters a valid FS number");
	}

	//
	@Then("^user enters a valid password without email$")
	public void user_enters_a_valid_password_without_email() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11");
		log.info("user enters a valid password with out email");
	}

	@Then("^user enters a valid email without password$")
	public void user_enters_a_valid_email_without_password() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("joe.flyer@spirit.com");
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).clear();
			log.info("user enters a valid email without password");
		}
	}

	@Then("^user enters a valid FS number without password$")
	public void user_eneters_a_valid_FS_number_without_password() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).clear();
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("461861422");
			driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).clear();
			log.info("user enters a valid FS Number without password ");
		}
	}

	@Then("^user validates required Email error message$")
	public void user_validates_required_Email_error_message() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Email_required_alert)));
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			Assert.assertTrue("INVALID ERROR MESSAGE!!!",
					driver.findElement(By.xpath(PassengerinfoPage.Email_required_alert)).getText()
							.equals("this is required"));
			log.info("Email or FS # required error message validated");
		}
	}

	@Then("^user validates required Password error message$")
	public void user_validates_required_Password_error_message() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Password_required_alert)));
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			Assert.assertTrue("INVALID ERROR MESSAGE!!!",
					driver.findElement(By.xpath(PassengerinfoPage.Password_required_alert)).getText()
							.equals("this is required"));
			log.info("PASSWORD required error message validated");
		}
	}

	@Then("^user selects two adult passengers$")
	public void user_selects_two_adult_passengers() throws Throwable {
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adults)).click();

		log.info("user 2 adults");
	}

	@Then("^user enters the personal information for Second adult$")
	public void user_enters_the_personal_information_for_Second_adult() throws Throwable {
		
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");
       Thread.sleep(2000);
		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Jack");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Flyer");
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("03/05/1983");

		log.info("user succesfully enters information for second Pax");
	}

	//
	// @Then("^user validates FS error message$")
	// public void user_validates_FS_error_message() throws Throwable {
	// // Write code here that turns the phrase above into concrete actions
	// throw new PendingException();
	// }
	//
	@Then("^user enters letters for the FS numbers for Second adult$")
	public void user_enters_letters_for_the_FS_numbers_for_Second_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).sendKeys("LETTERS");
		log.info("user enters letters as fs number");
	}

	@Then("^user enters symbols for the FS numbers for Second adult$")
	public void user_enters_symbols_for_the_FS_numbers_for_Second_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).sendKeys("$#&*%$$$$^#");
		log.info("user enters Symbols as fs number");
	}

	@Then("^user enters eight digits for the FS numbers for Second adult$")
	public void user_enters_eight_digits_for_the_FS_numbers_for_Second_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).sendKeys("12345678");
		log.info("user enters 8 digits as fs number");
	}

	@Then("^user enters ten digits for the FS numbers for Second adult$")
	public void user_enters_ten_digits_for_the_FS_numbers_for_Second_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).sendKeys("12345678nine 1");
		log.info("user enters 10 digits as fs number");
	}

	@Then("^user enters nine digits for the FS numbers for Second adult$")
	public void user_enters_nine_digits_for_the_FS_numbers_for_Second_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).clear();
		driver.findElement(By.xpath(PassengerinfoPage.child_free_spirit)).sendKeys("12345678nine ");
		log.info("use enters invalid nine  digit fs number");
	}

	@Then("^user selects Hearing disability$")
	public void user_selects_Hearing_disability() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)).click();
		log.info("user selects Hearing disability");
	}

	@Then("^user selects other disability$")
	public void user_selects_other_disability() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_other_checkbox)).click();
		log.info("user selects other disability");
	}

	@Then("^user selects Vision disability$")
	public void user_selects_Vision_disability() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)).click();
		log.info("user selects vision disability");
	}

	@Then("^user Clicks POC$")
	public void user_Clicks_POC() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_portable_oxygen)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_portable_oxygen)).click();
		log.info("user selects POC");
	}

	@Then("^user Selects Service Animal$")
	public void user_Selects_Service_Animal() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("user selects Service animal");
	}

	@Then("^user clicks on search button and enters the birthdate of the Lap child$")
	public void user_clicks_on_search_button_and_enters_the_birthdate_of_the_Lap_child() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.LapChild_Radio_Button)));
			driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the lap child");
			
			driver.switchTo().window(parentHandle);
		}
	}

	@Then("^user enters the same name for passenger two$")
	public void user_enters_the_personal_information_of_passenger_two() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("JOE");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("FLYER");
		log.info("The child age displayed "
				+ driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText());
		// Assert.assertTrue("child birth date is not populated",
		// driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText().equals("07/17/2007"));
		// log.info("child date of birth is auto populated");
		//
		// log.info("user successfully enter the passenger-child information");

		log.info("user enters the same name for pax 2");
	}

	@Then("^user selects SR suffix for passenger one$")
	public void user_selects_SR_suffix_for_passenger_one() throws Throwable {
		Select drpAdult1Suffix = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_passeneger_suffix)));
		drpAdult1Suffix.selectByVisibleText("Sr.");
		log.info("Sr suffix selected");
	}

	@Then("^user selects JR suffix for passenger two$")
	public void user_selects_SR_suffix_for_passenger_two() throws Throwable {
		Select drpAdult2Suffix = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_passeneger2_suffix)));
		drpAdult2Suffix.selectByVisibleText("Jr.");
		log.info("Jr suffix selected");
	}

	@Then("^User Selects all Wheelchair checkboxes$")
	public void User_Selects_all_Wheelchair_checkboxes() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_ompletely_immobile)).click();
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();

		log.info("user selects all wheelchair checkboxes");
	}

	@Then("^User Selects Battery Powered wetcell Wheelchair$")
	public void User_Selects_Battery_Powered_wetcell_Wheelchair() throws Throwable {
		Select drpWheelChair = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		drpWheelChair.selectByVisibleText("Battery Powered Wet Cell Battery");
		log.info("Battery Powered wetcell Wheelchair Selected");
	}

	@Then("^user Clicks on additional services for passenger two$")
	public void user_Clicks_on_additional_services_for_passenger_two() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_additional_services)).click();
		log.info("user Clicks on additional services for passenger two");
	}

	@Then("^user Clicks on PETC for passenger two$")
	public void user_Clicks_on_PETC_for_passenger_two() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_petin_cabin)).click();
		log.info("user Clicks on PETC for passenger two");
	}

	@Then("^user selects A random disability$")
	public void user_selects_a_random_disability() throws Throwable {
		Random ran = new Random();
		List<WebElement> Disabilities = driver
				.findElements(By.xpath("//label[@class='custom-control-label ng-star-inserted']"));

		int Disability = ran.nextInt(Disabilities.size()) + 1;

		Disabilities.get(Disability).click();
		String value = Disabilities.get(Disability).getText();
		log.info("User randomly selects " + value);

	}

	@Then("^User selects TWO child$")
	public void User_selects_TWO_child() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		driver.findElement(By.xpath(BookPage.select_child)).click();
		driver.findElement(By.xpath(BookPage.select_child)).click();

	}

	@Then("^user clicks on search button and enters the birthdate for TWO UMNR$")
	public void user_enters_the_birthdate_for_TW0_child() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("12/12/2006");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys("12/12/2008");
			driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.TAB);
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdates of the two children");
			Thread.sleep(4000);

			for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

				driver.findElement(By.xpath(OnewaytripPage.UMNR_Popup_Accept_button)).click();

				log.info("user succesfully accepts UNMR fee");

			}
		}
	}

	@Then("^user login as UMNR NineDFC member$")
	public void user_login_as_UMNR_NineDFC_member() throws Throwable {
		// String parentHandle = driver.getWindowHandle(); // get the current window
		// handle
		// driver.findElement(By.xpath(PassengerinfoPage.free_spirit_login)).click(); //
		// click some link that opens a new
		// // window
		// for (String winHandle : driver.getWindowHandles()) {
		// driver.switchTo().window(winHandle);

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys(UMNR_ninedfc_email);

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a NINEDFC who is a child");
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.passenerspage_Continue_button)));
		// dob: 2/1/2008
	}

	@Then("^User verifies LapChild DOB is autopopulated$")
	public void User_verifies_LapChild_DOB_is_autopopulated() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.child_dateofbirth)));
		// Assert.assertTrue("LapChild DOB is not autopopulated",
		// driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).getText().equals("12/12/2017"));
		WebElement dob1 = driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth));
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.MONTH, -6);
		Date lapcdob = c.getTime();
		String dob = dateFormat.format(lapcdob);
		String Lapdobauto = dob1.getAttribute("value");
		if (Lapdobauto.contentEquals(dob)) {
			log.info("Lapchild DOB is matched with autopopulated value");
		} else {
			log.info("Lapchild DOB is doesn't match with autopopulated");
		}
	}

	@Then("^User verifies military PAX DOB is not populated$")
	public void User_verifies_military_PAX_DOB_is_populated() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)));
		WebElement dob = driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth));
		String fieldNameVal = dob.getAttribute("value");
		if (fieldNameVal.contentEquals("01/01/1990")) {
			log.info("Military PAX DOB is populated " + fieldNameVal);
		} else {
			log.info("Military PAX DOB is not populated");
		}
	}

	@Then("^user verifies no Active duty military or Traveling with car seat check box next to lap child$")
	public void user_verifies_no_active_duty_military_or_traveling_with_car_seat_check_box_next_to_lap_child()
			throws Throwable {
		WebElement carseat = driver.findElement(By.xpath(PassengerinfoPage.CarSeat));
		carseat.click();
		if (carseat.isDisplayed() == true) {
			log.info("Traveling with car seat Checkbox is displayed next to lap child");
		} else {
			log.info("Traveling with car seat Checkbox is not displayed next to lap child");
		}

	}

	@Then("^user login as military nine DFC member$")
	public void user_login_as_military_nine_DFC_member() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.free_spirit_email)));

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("jamalflyer9dfc@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully logged in as a military nine_DFC member");

	}

	@Then("^user verifies Member with military nine DFC is logged in$")
	public void user_verifies_Member_with_military_nine_DFC_is_logged_in() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JAMAL"));
		log.info("Military nineDFC User Log in verified");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.Primary_passenger_chk)));
		driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();

	}

	@Then("^user Clicks vision disability$")
	public void user_Clicks_vision_disability() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)).click();
		log.info("User clicks vision disability");
	}

	@Then("^user Clicks service animal$")
	public void user_Clicks_service_animal() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("User clicks service animal");
	}

	@Then("^user Clicks Own wheelChair$")
	public void user_Clicks_Own_wheelChair() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("User checks Need help To/From gate");
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("User checks Own Wheel Chair");
		Select owdrop = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		owdrop.selectByIndex(1);
	}

	@Then("^user verifies FS Member of military with known traveler is logged in$")
	public void user_verifies_FS_Member_of_military_with_known_traveler_is_logged_in() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("GKNOWN"));
		log.info("Military FS member Log in verified");
		driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();

	}

	@Then("^FS military member enters known traveler number$")
	public void FS_military_member_enters_known_traveler_number() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.adult_known_traveler_number)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys(Keys.RETURN);
		driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("135705881");
		log.info("user succesfully entered ktn");
	}

	@Then("^use enters informations of second adt military pax$")
	public void use_enters_informations_of_second_adt_military_pax() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_first_name)).sendKeys("Ashley");
		driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_last_name)).sendKeys("Johnson");
		WebElement passengertwo = driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_dateof_birth));
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -21);
		Date age = c.getTime();
		String driverage = dateFormat.format(age);
		passengertwo.sendKeys(driverage);
		driver.findElement(By.xpath(PassengerinfoPage.adult2_ActiveDutyMilitaryPersonnelCheckBox)).click();
		log.info("user succesfully enters information for second Pax and checked Active Duty U.S. Military Personnel");
	}

	@Then("^user able to select a primary driver$")
	public void user_able_to_select_a_primary_driver() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.Primary_driver)).click();
		log.info("user selects Primary driver");
	}

	@Then("^use enters informations of second adt military pax under the age of 21$")
	public void use_enters_informations_of_second_adt_military_pax_under_the_age_of_21() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt2_Title)));
		s.selectByVisibleText("Mr.");
		driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_first_name)).sendKeys("Ashley");
		driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_last_name)).sendKeys("Johnson");
		WebElement passengertwo = driver.findElement(By.xpath(PassengerinfoPage.adult2_passengers_dateof_birth));
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -20);
		Date age = c.getTime();
		String driverage = dateFormat.format(age);
		passengertwo.sendKeys(driverage);
		driver.findElement(By.xpath(PassengerinfoPage.adult2_ActiveDutyMilitaryPersonnelCheckBox)).click();
		log.info("user succesfully enters information for second Pax and checked Active Duty U.S. Military Personnel");
	}

	@Then("^user unable to select a primary driver$")
	public void user_unable_to_select_a_primary_driver() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.Primary_driver)).click();
		log.info("user unable to selects Primary driver");
	}

	@Then("^user Clicks Wheelchair with all three check box checked and a manual wheelchair$")
	public void user_Clicks_wheelChairwith_all_three_check_box_checked_and_a_manual_wheelchair() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("User checks Need help To/From gate");
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_or_from_gate)).click();
		log.info("User checks Need help To/From Seat");
		driver.findElement(By.xpath(PassengerinfoPage.Completely_immobile)).click();
		log.info("User checks Completely Immobile");
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("User checks Own Wheel Chair");
		Select owdrop = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		owdrop.selectByIndex(2);
	}

	@Then("^user Clicks hearing disablity$")
	public void user_Clicks_hearing_disablity() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)).click();
		log.info("User checked hearing disablity ");
	}

	@Then("^user Clicks Portal Oxyzen Concentrators$")
	public void user_Clicks_Portal_Oxyzen_Concentrators() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_portable_oxygen)).click();
		log.info("User clicks Portal Oxyzen Concentrators");
	}

	@Then("^user enters the birthdate of the Lap child$")
	public void user_enters_the_birthdate_of_the_Lap_child() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_button)).click();

//		WebElement ok = driver.findElement(By.xpath(BookPage.Proof_of_return_okbtn));
//		if (ok.isDisplayed()) {
//			ok.click();
//			log.info("This trips needs 'Proof of Return' on your travel");
//		}

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			Date currentDate = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate); // manipulate date
			c.add(Calendar.MONTH, -6);
			// convert calendar to date
			Date lapcdob = c.getTime();
			String dob = dateFormat.format(lapcdob);
			WebElement dob1 = driver.findElement(By.xpath(BookPage.enter_child_date_of_birth));
			dob1.click();
			dob1.clear();
			dob1.sendKeys(dob);
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			wait.until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.LapChild_Radio_Button)));
			driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the lap child");
		}
	}

	@Then("^user Clicks on My Own WheelChair$")
	public void user_Clicks_on_My_Own_wheelChair() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		Select owdrop = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		owdrop.selectByIndex(3);
		log.info("User checks on 'I Have My Own Wheel Chair' and selects 'Manually Powered' option");
	}

	@Then("^user verifies 9 DFC Member with known traveler is logged in$")
	public void user_verifies_9_DFC_Member_with_known_traveler_is_logged_in() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		Assert.assertTrue("User is not logged in",
				driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JUSTIN"));
		log.info("User Logged in verified");
		driver.findElement(By.xpath(PassengerinfoPage.Primary_passenger_chk)).click();
	}
	
	 @Then("^user clicks on search button and enters the birthdate of one child$")
	    public void user_clicks_on_search_button_and_enters_the_birthdate_of_one_child() throws Throwable {
	        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
	        driver.findElement(By.xpath(BookPage.Search_button)).click();
	        String parentHandle = driver.getWindowHandle();
	        for (String winHandle : driver.getWindowHandles()) {
	            driver.switchTo().window(winHandle);
	            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
	            driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).click();
	            Date currentDate = new Date();
	            Calendar c = Calendar.getInstance();
	            c.setTime(currentDate);
	            c.add(Calendar.MONTH, -3);
	            Date childonedob = c.getTime();
	            String dob = dateFormat.format(childonedob);
	            log.info("LapChild DOB is : " + dateFormat.format(childonedob));
	            driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(dob);
	            driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
	            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.LapChild_Radio_Button)));
	            driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
	            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.child_popup_continue_button)));
	            driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
	            log.info("user succesfully entered the birthdate of one child");
	 
	        }
	    }
	 
	 
	 @Then("^User selects ONE ADULT and TWO children$")
		public void user_selects_one_adult_and_two_children() throws Throwable {
			driver.findElement(By.xpath(PassengerinfoPage.clickon_passengers)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
			driver.findElement(By.xpath(BookPage.select_child)).click();
			driver.findElement(By.xpath(BookPage.select_child)).click();
		}

		@Then("^User Clicks on SEARCH FLIGHTS and makes one child UNDER four years old and one child OVER four years old$")
		public void user_clicks_on_search_flights_and_makes_one_child_under_four_years_old_and_one_child_over_four_years_old()
				throws Throwable {
			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.Search_button)).click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("1/1/20014");
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);

				driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys("1/1/2018");
				driver.findElement(By.xpath(BookPage.enter_child2_date_of_birth)).sendKeys(Keys.TAB);
				driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

				log.info("user succesfully enter the birthdates of the two children");
				Thread.sleep(4000);

				for (String winHandle2 : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}
			}
		}

		@Then("^User enters Info for Primary Passenger Adult$")
		public void user_enters_info_for_primary_passenger_adult() throws Throwable {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Title)));

			Select  s = new  Select(driver.findElement(By.xpath(OnewaytripPage.Title)));
			s.selectByIndex(1);
			

			driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).sendKeys("jack");
			log.info("user able to enters his first name");

			driver.findElement(By.xpath(OnewaytripPage.passengers_middle_name)).sendKeys("Jamal");
			log.info("user able to enter passenger middle name");

			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).click();
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys("Jr.");
			driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).sendKeys(Keys.TAB);
			log.info("user able to enter a suffix");

			driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).sendKeys("08/12/1989");
			log.info("user able to enters his date of birth");

			driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("123456789");
			driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).clear();
			log.info("user able to enters a Known Traveler Number");

			driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
			log.info("user successfully select the primary passenger  contact checkbox ");

			driver.findElement(By.xpath(OnewaytripPage.passengers_address)).sendKeys("603 west side");
			log.info("user able to enter his address");

			driver.findElement(By.xpath(OnewaytripPage.passengers_city)).sendKeys("miami");
			log.info("user able to enter his city name");

			driver.findElement(By.xpath(OnewaytripPage.zip_code)).sendKeys("33456");
			log.info("user able to enter his city zipcode");

			driver.findElement(By.xpath(OnewaytripPage.country)).click();
			Select dropdown = new Select(driver.findElement(By.xpath(OnewaytripPage.country)));
			
			dropdown.selectByVisibleText("United States of America");
			log.info("user able to selects the country name");

			driver.findElement(By.xpath(OnewaytripPage.passengers_email)).sendKeys("jack.flyer9dfc@spirit.com");
			log.info("user able to enters his email");

			driver.findElement(By.xpath(OnewaytripPage.confirm_email)).sendKeys("jack.flyer9dfc@spirit.com");

			driver.findElement(By.xpath(OnewaytripPage.Passengers_phone)).sendKeys("1230000000");
			log.info("user able to enters his phone number");

			Thread.sleep(3000);
			Select s1 = new Select(driver.findElement(By.xpath(OnewaytripPage.passengers_state)));
			s1.selectByVisibleText("Florida");
			log.info("user able to select  his state");
		}

		@Then("^User Validates Primary Passenger KTN Tooltip$")
		public void user_validates_primary_passenger_ktn_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip)).click();
			log.info("PAX 1 KTN Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip_Popup)).getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger KTN Tool Tip", driver
					.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip_Popup)).getText().equals(Tooltip));
			log.info("User Validated Primary Passenger KTN Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip_Popup_More_Info_Link)).click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 1 KTN Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("https://www.tsa.gov/"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_KTN_Tooltip)).click();
		}

		@Then("^User Validates Primary Passenger Free Spirit Tooltip$")
		public void user_validates_primary_passenger_free_spirit_tooltip() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip)).click();
			log.info("PAX 1 Free Spirit Number Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger Free Spirit Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup)).getText()
							.equals(Tooltip));
			log.info("User Validated Primary Passenger Free Spirit Tool Tip");

			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link))
					.click();
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);
			Assert.assertTrue("Sign Up Link Does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/account-enrollment"));
			driver.navigate().back();
		}

		@Then("^User Validates Primary Passenger Active Duty Tooltip$")
		public void user_validates_primary_passenger_active_duty_tooltip() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_ActiveDutyMP_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_ActiveDutyMP_Tooltip)).click();
			log.info("PAX 1 Active Duty U.S Military Personnel Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_ActiveDutyMP_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger Free Spirit Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_FreeSpiritNumber_Tooltip_Popup)).getText()
							.equals(Tooltip));
			log.info("User Validated Active Duty U.S Military Personnel Tool Tip");

			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(PassengerinfoPage.PAX_One_Adult_ActiveDutyMP_Tooltip_Popup_Sign_Up_Now_Link)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_ActiveDutyMP_Tooltip_Popup_Sign_Up_Now_Link))
					.click();
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);
			Assert.assertTrue("Sign Up Now Link Does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/account-enrollment"));
			driver.navigate().back();
		}

		@Then("^User Clicks on Primary Passenger Additional Services Caret$")
		public void user_clicks_on_primary_passenger_additional_services_caret() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Caret)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Caret)).click();
			log.info("PAX 1 Additional Services Caret can be clicked");
		}

		@Then("^User Validates Primary Passenger Emotional Support Animal Tooltip$")
		public void user_validates_primary_passenger_emotional_support_animal_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip)));
			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip)).click();
			log.info("PAX 1 Emotional/Psychiatric Support Animal Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(
							PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger Emotional/Psychiatric Support Animal Tool Tip",
					driver.findElement(By.xpath(
							PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated Primary Passenger Emotional/Psychiatric Support Animal Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(
					PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue(
					"Pax 1 Passenger Emotional/Psychiatric Support Animal Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096816-Can-I-bring-my-service-emotional-support-or-psychiatric-service-animal-on-my-flight"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Emotional_Support_Animal_Tooltip)).click();
		}

		@Then("^User Validates Primary Passenger Other CPAP nebulizer ventilator respirator cane walker etc$")
		public void user_validates_primary_passenger_other_cpap_nebulizer_ventilator_respirator_cane_walker_etc()
				throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip)).click();
			log.info("PAX 1 Other Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger Other Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated Primary Passenger Other Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip_Popup_More_Information_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 1 Passenger Other Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096766-Can-I-bring-my-Portable-Oxygen-Concentrator-on-board"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Other_Tooltip)).click();
		}

		@Then("^User Validates Primary Passenger Voluntary Provision Tooltip$")
		public void user_validates_primary_passenger_voluntary_provision_tooltip() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip)).click();
			log.info("PAX 1 Voluntary Provision Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate Primary Passenger Voluntary Provision Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated Primary Passenger Voluntary Provision Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(
					PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip_Popup_More_Information_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 1 Passenger Voluntary Provision Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202097026-Voluntary-Provision-of-Emergency-Services-Program"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_One_Adult_Additonal_Services_Voluntary_Tooltip)).click();
		}

		@Then("^User Validates Passenger Two KTN Tooltip$")
		public void user_validates_passenger_two_ktn_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip)).click();
			log.info("PAX 2 KTN Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip_Popup)).getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 2 KTN Tool Tip", driver
					.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip_Popup)).getText().equals(Tooltip));
			log.info("User Validated PAX 2 KTN Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip_Popup_More_Info_Link)).click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 2 KTN Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("https://www.tsa.gov/"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_KTN_Tooltip)).click();
		}

		@Then("^User Validates Passenger Two Free Spirit Tooltip$")
		public void user_validates_passenger_two_free_spirit_tooltip() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip)).click();
			log.info("PAX 2 Free Spirit Number Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 2 Free Spirit Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup)).getText()
							.equals(Tooltip));
			log.info("User Validated PAX 2 Free Spirit Tool Tip");

			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link))
					.click();
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);
			Assert.assertTrue("Sign Up Link Does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/account-enrollment"));
			driver.navigate().back();
		}

		@Then("^User Clicks on Passenger Two Additional Services Caret$")
		public void user_clicks_on_passenger_two_additional_services_caret() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.Pax_Two_Child_Additional_Services_Caret)));
			driver.findElement(By.xpath(PassengerinfoPage.Pax_Two_Child_Additional_Services_Caret)).click();
			log.info("PAX 2 Additional Services Caret can be clicked");
		}

		@Then("^User Validates Passenger Two Emotional Support Animal Tooltip$")
		public void user_validates_passenger_two_emotional_support_animal_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip)));
			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip)).click();
			log.info("PAX 2 Emotional/Psychiatric Support Animal Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(
							PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 2 Emotional/Psychiatric Support Animal Tool Tip",
					driver.findElement(By.xpath(
							PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated PAX 2 Emotional/Psychiatric Support Animal Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(
					PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue(
					"Pax 2 Passenger Emotional/Psychiatric Support Animal Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096816-Can-I-bring-my-service-emotional-support-or-psychiatric-service-animal-on-my-flight"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Emotional_Support_Animal_Tooltip)).click();
		}

		@Then("^User Validates Passenger Two Other CPAP nebulizer ventilator respirator cane walker etc$")
		public void user_validates_passenger_two_other_cpap_nebulizer_ventilator_respirator_cane_walker_etc()
				throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip)).click();
			log.info("PAX 2 Other Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 2 Other Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated PAX 2 Other Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip_Popup_More_Information_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 2 Passenger Other Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096766-Can-I-bring-my-Portable-Oxygen-Concentrator-on-board"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_Two_Child_Additonal_Services_Other_Tooltip)).click();
		}

		@Then("^User Validates Passenger Three KTN Tooltip$")
		public void user_validates_passenger_three_ktn_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip)).click();
			log.info("PAX 3 KTN Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip_Popup)).getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 3 KTN Tool Tip", driver
					.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip_Popup)).getText().equals(Tooltip));
			log.info("User Validated PAX 3 KTN Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip_Popup_More_Info_Link)).click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 3 KTN Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("https://www.tsa.gov/"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_KTN_Tooltip_Popup_More_Info_Link)).click();
		}

		@Then("^User Validates Passenger Three Free Spirit Tooltip$")
		public void user_validates_passenger_three_free_spirit_tooltip() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip)).click();
			log.info("PAX 3 Free Spirit Number Tool Tip can be clicked");

			String Tooltip = driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 3 Free Spirit Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup)).getText()
							.equals(Tooltip));
			log.info("User Validated PAX 3 Free Spirit Tool Tip");

			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_FreeSpiritNumber_Tooltip_Popup_Sign_Up_Link))
					.click();
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);
			Assert.assertTrue("Sign Up Link Does not navigate to valid URL",
					driver.getCurrentUrl().startsWith("http://qaepic01.spirit.com/account-enrollment"));
			driver.navigate().back();
		}

		@Then("^User Clicks on Passenger Three Additional Services Caret$")
		public void user_clicks_on_passenger_three_additional_services_caret() throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.Pax_Three_Child_Additional_Services_Caret)));
			driver.findElement(By.xpath(PassengerinfoPage.Pax_Three_Child_Additional_Services_Caret)).click();
			log.info("PAX 3 Additional Services Caret can be clicked");
		}

		@Then("^User Validates Passenger Three Emotional Support Animal Tooltip$")
		public void user_validates_passenger_three_emotional_support_animal_tooltip() throws Throwable {
			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip)));
			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip))
					.click();
			log.info("PAX 3 Emotional/Psychiatric Support Animal Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(
							PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 3 Emotional/Psychiatric Support Animal Tool Tip",
					driver.findElement(By.xpath(
							PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated PAX 3 Emotional/Psychiatric Support Animal Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(
					PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip_Popup_More_Info_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue(
					"Pax 3 Passenger Emotional/Psychiatric Support Animal Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096816-Can-I-bring-my-service-emotional-support-or-psychiatric-service-animal-on-my-flight"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(
					By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Emotional_Support_Animal_Tooltip))
					.click();
		}

		@Then("^User Validates Passenger Three Other CPAP nebulizer ventilator respirator cane walker etc$")
		public void user_validates_passenger_three_other_cpap_nebulizer_ventilator_respirator_cane_walker_etc()
				throws Throwable {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip)));
			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip)).click();
			log.info("PAX 3 Other Tool Tip can be clicked");

			String Tooltip = driver
					.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip_Popup))
					.getText();
			log.info("Tool tip pop up text should be " + Tooltip);

			Assert.assertTrue("User could not validate PAX 3 Other Tool Tip",
					driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip_Popup))
							.getText().equals(Tooltip));
			log.info("User Validated PAX 3 Other Tool Tip");

			String parentHandle = driver.getWindowHandle();
			driver.findElement(By
					.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip_Popup_More_Information_Link))
					.click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			String homeurl = driver.getCurrentUrl();
			log.info("Validated home page url is " + homeurl);

			Assert.assertTrue("Pax 3 Passenger Other Tool tip More Info Link does not navigate to valid URL",
					driver.getCurrentUrl().startsWith(
							"https://customersupport.spirit.com/hc/en-us/articles/202096766-Can-I-bring-my-Portable-Oxygen-Concentrator-on-board"));

			driver.close();
			driver.switchTo().window(parentHandle);

			driver.findElement(By.xpath(PassengerinfoPage.PAX_Three_Child_Additonal_Services_Other_Tooltip)).click();
		}

	 
	 
	 @Then("^user switch clicks on the search button and switch to the popup at lima city and switch to the child popup and enters the DOB$")
	    public void user_switch_clicks_on_the_search_button_and_switch_to_the_popup_at_lima_city_and_switch_to_the_child_popup_and_enters_the_dob() throws Throwable {
	      
		 
		 String parentHandle = driver.getWindowHandle();
			driver.findElement(By.xpath(BookPage.Search_button)).click();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Proof_of_return_okbtn)));
			 driver.findElement(By.xpath(BookPage.Proof_of_return_okbtn)).click();
    

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
				Date currentDate = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(currentDate); // manipulate date
				c.add(Calendar.MONTH, -6);
				// convert calendar to date
				Date lapcdob = c.getTime();
				String dob = dateFormat.format(lapcdob);
				WebElement dob1 = driver.findElement(By.xpath(BookPage.enter_child_date_of_birth));
				dob1.click();
				dob1.clear();
				dob1.sendKeys(dob);
				driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
				wait.until(
						ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.LapChild_Radio_Button)));
				driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
				driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

				log.info("user succesfully enter the birthdate of the lap child");
			}
	    }
		

		@Then("^user verifies whether Free Spirit member is logged in$")
		    public void user_verifies_whether_Free_Spirit_member_is_logged_in() throws Throwable {
		        Thread.sleep(3000);
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.CustomerLoggedIn)));
		        log.info("User logged as " + driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText());
		        Assert.assertTrue("User is not logged in",
		                driver.findElement(By.xpath(OnewaytripPage.CustomerLoggedIn)).getText().equals("JOE"));
		        log.info("Free Spirit Member Log in is verified");
		    }

		    @Then("^User validates FS number and known traveler number then redress number$")
		    public void user_validates_fs_number_and_known_traveler_number_then_redress_number() throws Throwable {
		        wait.until(
		                ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.adult_Free_spirit_memeber)));
		        Assert.assertTrue("User's FS number is Invalid or Mismatch",
		                driver.findElement(By.xpath(PassengerinfoPage.adult_Free_spirit_memeber)).getAttribute("value")
		                        .equals("1000219856"));
		        log.info("User's FS number is Validated");        
		        wait.until(
		                ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.adult_known_traveler_number)));
		        driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("135705880");
		         String parentHandle = driver.getWindowHandle();
		        driver.findElement(By.xpath(PassengerinfoPage.passenerspage_Continue_button)).click();
		         for (String winHandle : driver.getWindowHandles()) {
		                driver.switchTo().window(winHandle);}
		        Assert.assertTrue("Invalid FS number entry Error message is not displayed", driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).isDisplayed());
		        driver.switchTo().window(parentHandle);
		        driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
		        driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1357");
		         String parentHandle1 = driver.getWindowHandle();
		        driver.findElement(By.xpath(PassengerinfoPage.passenerspage_Continue_button)).click();
		         for (String winHandle : driver.getWindowHandles()) {
		                driver.switchTo().window(winHandle);}
		        Assert.assertTrue("Invalid Redress number message is not displayed", driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).isDisplayed());
		        driver.switchTo().window(parentHandle1);
		        wait.until(
		                ExpectedConditions.elementToBeClickable(By.xpath(PassengerinfoPage.adult_known_traveler_number)));
		        driver.findElement(By.xpath(PassengerinfoPage.adult_known_traveler_number)).sendKeys("1234567890123456789012345");
		         String parentHandle2 = driver.getWindowHandle();
		        driver.findElement(By.xpath(PassengerinfoPage.passenerspage_Continue_button)).click();
		         for (String winHandle : driver.getWindowHandles()) {
		                driver.switchTo().window(winHandle);}
		        Assert.assertTrue("Invalid 25 digit FS number entry Error message is not displayed", driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).isDisplayed());
		        driver.switchTo().window(parentHandle2);
		        driver.findElement(By.xpath(PassengerinfoPage.adult_redress_number)).click();
		        driver.findElement(By.xpath(PassengerinfoPage.adult_redress_input_box)).sendKeys("1357");
		         String parentHandle3 = driver.getWindowHandle();
		        driver.findElement(By.xpath(PassengerinfoPage.passenerspage_Continue_button)).click();
		         for (String winHandle : driver.getWindowHandles()) {
		                driver.switchTo().window(winHandle);}
		        Assert.assertTrue("Invalid Redress number message is not displayed", driver.findElement(By.xpath(PassengerinfoPage.passengerpage_alert)).isDisplayed());
		        driver.switchTo().window(parentHandle3);
		        log.info("9DFC member Entering incorrect information for KTN and redress number are validated");    
		        
		    }

}
