package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.Boardingpass;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.FlightAvailability;
import com.cucumber.pages.MyTripsPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage319;
import com.cucumber.pages.SeatsPage320;
import com.cucumber.pages.SeatsPage321;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SeatsStep319 {

	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	@Then("^User choose the from FLL City$")
	public void user_choose_the_from_fll_city() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_fromcity)));
		driver.findElement(By.xpath(BookPage.clickon_fromcity)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.From_FLL_City)));
		driver.findElement(By.xpath(SeatsPage.From_FLL_City)).click();
		log.info("user successfully selected the FLL city");
	}

	@Then("^User choose the To ATL city$")
	public void user_choose_the_to_atl_city() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_Tocity)));
		driver.findElement(By.xpath(BookPage.clickon_Tocity)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.TO_ATL_City)));
		driver.findElement(By.xpath(SeatsPage.TO_ATL_City)).click();
		log.info("user successfully selected the ATL city");
	}

	@Then("^user selects the has car seat checkbox$")
	public void user_selects_the_has_car_seat_checkbox() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.child_car_seat)));
		driver.findElement(By.xpath(SeatsPage.child_car_seat)).click();
		log.info("user successfully selected the child car seat");

	}

	@Then("^user clicks on search button and enters the birth date of the child$")
	public void user_clicks_on_search_button_and_enters_the_birth_date_of_the_child() throws Throwable {

		driver.findElement(By.xpath(BookPage.Search_button)).click();
		Thread.sleep(4000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("07/12/2017");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.ENTER);
			log.info("user succesfully enter the birthdate of the child");
			
			
		}
	}

	@Then("^user clicks on the continue without adding bags button$")
	public void user_clicks_on_the_continue_without_adding_bags_button() throws Throwable {

		String FocusonBagsPage = driver.getWindowHandle();
		driver.findElement(By.xpath(BagsPage.continue_withou_bags)).click();
		log.info("user clicks on the continue wihout adding the bags");
		for (String idontNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(idontNeedBags);
		}

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_idont_need_bags)));
		driver.findElement(By.xpath(BagsPage.popup_idont_need_bags)).click();

	}

	@Then("^user selects the standard seat$")
	public void user_selects_the_standard_seat() throws Throwable {

		Thread.sleep(3000);
		String s = driver.findElement(By.xpath(SeatsPage319.Airbus_Name)).getText();
		log.info("flight name " + s);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage319.Airbus_319_Standard_Seat_14A)));
				driver.findElement(By.xpath(SeatsPage319.Airbus_319_Standard_Seat_14A)).click();

	}

	@Then("^user trying to selects the Exit row and Big front seat for the child but it should be blocked$")
	public void user_trying_to_selects_the_exit_row_and_big_front_seat_for_the_child_but_it_should_be_blocked()
			throws Throwable {
		try {
			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage319.Airbus_319_Exit_Seat_11A, SeatsPage319.Airbus_319_Exit_Seat_11B,
						SeatsPage319.Airbus_319_Exit_Seat_11C, SeatsPage319.Airbus_319_Exit_Seat_11D,
						SeatsPage319.Airbus_319_Exit_Seat_11E, SeatsPage319.Airbus_319_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for child passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {

		}
//		driver.findElement(By.xpath(SeatsPage319.Airbus_319_Standard_Seat_13D)).click();
//		log.info("user selects the standard seat for the child");

	}

	@Then("^user clicks on the additional info button and selects the vision disability$")
	public void user_clicks_on_the_additional_info_button_and_selects_the_vision_disability() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_vision_disability_Checkbox)).click();
		log.info("user successfully clicked on the vision disability check box");
	}

	@Then("^user trying to selects the Exit row but it should be disabled$")
	public void user_trying_to_selects_the_exit_row_but_it_should_be_disabled() throws Throwable {

		try {
			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage319.Airbus_319_Exit_Seat_11A, SeatsPage319.Airbus_319_Exit_Seat_11B,
						SeatsPage319.Airbus_319_Exit_Seat_11C, SeatsPage319.Airbus_319_Exit_Seat_11D,
						SeatsPage319.Airbus_319_Exit_Seat_11E, SeatsPage319.Airbus_319_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for the passenger!",driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {
		}

	}

	@Then("^user clicks on additional services and selects the Hearing disability$")
	public void user_clicks_on_additional_services_and_selects_the_hearing_disability() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Hearing_impaired_Checkbox)).click();
		log.info("user successfully clicked on the Hearing impaired checkbox");

	}

	@Then("^user clicks on the additional services and selects the emotional psychiatric support animal$")
	public void user_clicks_on_the_additional_services_and_selects_the_support_animal() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Emotional_checkbox)).click();
		log.info("user successfully clicked on the emotional psychiatric support animal ");
	}

	@Then("^user trying to selects the Exit row and big front seat but it should be disabled$")
	public void user_trying_to_selects_the_exit_row_and_big_front_seat_but_it_should_be_disabled() throws Throwable {

		try {
			for (int i = 0; i <= 15; i++) {
				String seatxpath[] = { SeatsPage319.Airbus_319_Exit_Seat_11A, SeatsPage319.Airbus_319_Exit_Seat_11B,
						SeatsPage319.Airbus_319_Exit_Seat_11C, SeatsPage319.Airbus_319_Exit_Seat_11D,
						SeatsPage319.Airbus_319_Exit_Seat_11E, SeatsPage319.Airbus_319_Exit_Seat_11F,
						SeatsPage319.Airbus_319_Big_Front_Seat_1A, SeatsPage319.Airbus_319_Big_Front_Seat_1B,
						SeatsPage319.Airbus_319_Big_Front_Seat_1E, SeatsPage319.Airbus_319_Big_Front_Seat_1F,
						SeatsPage319.Airbus_319_Standard_Seat_5A, SeatsPage319.Airbus_319_Standard_Seat_5B,
						SeatsPage319.Airbus_319_Standard_Seat_5C, SeatsPage319.Airbus_319_Standard_Seat_5D,
						SeatsPage319.Airbus_319_Standard_Seat_5E, SeatsPage319.Airbus_319_Standard_Seat_5F };
				Assert.assertTrue("The exit row seats are displayed for disabled passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {

		}

	}

	@Then("^user clicks on search button and enters the birth date of the lap child$")
	public void user_clicks_on_search_button_and_enters_the_birth_date_of_the_lap_child() throws Throwable {

		driver.findElement(By.xpath(BookPage.Search_button)).click();
		Thread.sleep(4000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("01/02/2018");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			driver.findElement(By.xpath(PassengerinfoPage.LapChild_Radio_Button)).click();
            driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();
			log.info("user succesfully enter the birthdate of the child");
			
			
		}
		driver.switchTo().window(parentHandle);
	}

	@Then("^user selects clicks on the additional services and select the portable oxygen concentrators$")
	public void user_selects_clicks_on_the_additional_services_and_select_the_portable_oxygen_concentrators()
			throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_portable_oxygen)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_portable_oxygen)).click();
		log.info("user successfully clicked on the portable oxygen concentratrs");

	}

	@Then("^user clicks on the additional services and selects the service animal check box$")
	public void user_clicks_on_the_additional_services_and_selects_the_service_animal_check_box() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ServiceAnimal_CheckBox)).click();
		log.info("user successfully selected the service animal check box");

	}

	@Then("^user selects the Child passenger UNMR$")
	public void user_selects_the_child_passenger_unmr() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		// driver.findElement(By.xpath(BookPage.select_adults)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
		driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_child)));
		driver.findElement(By.xpath(BookPage.select_child)).click();
        log.info("user succesfully selected the UMNR passenger ");
	}

	@Then("^user selects clicks on continue button and eneter the date of birth$")
	public void user_selects_clicks_on_continue_button_and_eneter_the_date_of_birth() throws Throwable {
		String parentHandle = driver.getWindowHandle();
        Thread.sleep(2000);
         
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.Search_button)));
        driver.findElement(By.xpath(BookPage.Search_button)).click();
        for (String winHandle : driver.getWindowHandles()) {
             driver.switchTo().window(winHandle);
         }
        Date currentDate = new Date();
       Calendar c = Calendar.getInstance();
     
       c.add(Calendar.YEAR, -9);
       Date date = c.getTime();
       log.info("Lap child's DOB is : " + dateFormat.format(date));
       String age = dateFormat.format(date);
       c.setTime(date);
       
       driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(age);
       log.info("user succesfully enters the child date of birth");
   
       driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
       
       driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
       log.info("user successfully clicks on the young traveler popup continue button");
			log.info("user succesfully enter the birthdate of the child");

		}

	

	@Then("^user enters the UMNR passenger information$")
	public void user_enters_the_umnr_passenger_information() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.adullt_Title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adullt_Title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).sendKeys("jhonson");

		log.info("user succesfully enter passenger information");
	}

	@Then("^user clicks on the additional services and selects the battery powered dry$")
	public void user_clicks_on_the_additional_services_and_selects_the_battery_powered_dry() throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_own_wheel_chair)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user succesfully clicked on the i have my own wheel chair");
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		s.selectByVisibleText("Battery Powered Dry/Gel Cell Battery");
		log.info("user succesfully selected the Battrey powered dry/ gel cell battery");

	}

	@Then("^user selects the two adult passengers$")
	public void user_selects_the_two_adult_passengers() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.clickon_passengers)));
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adults)));
	    driver.findElement(By.xpath(BookPage.select_adults)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.select_adult_minus_button)));
	    driver.findElement(By.xpath(BookPage.select_adult_minus_button)).click();
		log.info("user succesfully selected the passengers");

	}

	@Then("^primary passneger clicks on the additional services and selects the i have my own wheel chair$")
	public void primary_passneger_clicks_on_the_additional_services_and_selects_the_i_have_my_own_wheel_chair()
			throws Throwable {

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_own_wheel_chair)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
		log.info("user succesfully clicked on the i have my own wheel chair");
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		s.selectByVisibleText("Battery Powered Wet Cell Battery");
		log.info("user succesfully selected the Battery Powered Wet Cell Battery");

	}

	@Then("^user enters the secondary passenger information$")
	public void user_enters_the_secondary_passenger_information() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("dwanye");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("jhonson");
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("12/12/1990");
		log.info("user succesfully enter passenger information");

	}

	@Then("^user clicks on the Edit seats button$")
	public void user_clicks_on_the_edit_seats_button() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(SeatsPage319.checkin_path_Edit_seats_passeneger_one)));
		driver.findElement(By.xpath(SeatsPage319.checkin_path_Edit_seats_passeneger_one)).click();
		log.info("user succesfully clicks on the edit seats button");
	}

	@Then("^user selects the standard seat for the passneger two$")
	public void user_selects_the_standard_seat_for_the_passneger_two() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage319.Airbus_319_Standard_Seat_15A)));
		driver.findElement(By.xpath(SeatsPage319.Airbus_319_Standard_Seat_15A)).click();
		log.info("user succesfully select the seat for the passneger two ");

	}

	@Then("^user clicks on the continue button on the seats page and switch to the popup and selects the i dont need bags$")
	public void user_clicks_on_the_cintinue_button_on_the_seats_page_and_switch_to_the_popup_and_selects_the_i_dont_need_bags()
			throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.seatspage_continue_button)));
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(OnewaytripPage.seatspage_continue_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(2000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage319.chekin_path_seatspage_bags_popup)));
		driver.findElement(By.xpath(SeatsPage319.chekin_path_seatspage_bags_popup)).click();

		log.info("user succefully handled the popup and select the i dont need bags ");

	}

	@When("^user lands on Extras page$")
	public void user_lands_on_extras_page() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(SeatsPage319.checkin_path_extraspage_text_validation)));
		driver.findElement(By.xpath(SeatsPage319.checkin_path_extraspage_text_validation)).getText().equals("Extras");
		log.info("user validates the text on the Extras page");

	}

	@Then("^user clicks on the continue button on the extras page$")
	public void user_clicks_on_the_continue_button_on_the_extras_page() throws Throwable {
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(SeatsPage319.checkin_path_extrapage_continue_button)));
		driver.findElement(By.xpath(SeatsPage319.checkin_path_extrapage_continue_button)).click();
		log.info("user succesfully clicks on the continue button on the extras page");

	}

	@Then("^user trying to selects the Exit row but it should be disabled and select any random seat$")
	public void user_trying_to_selects_the_exit_row_but_it_should_be_disabled_and_select_any_random_seat(int seatCount)
			throws Throwable {

		try {
			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage319.Airbus_319_Exit_Seat_11A, SeatsPage319.Airbus_319_Exit_Seat_11B,
						SeatsPage319.Airbus_319_Exit_Seat_11C, SeatsPage319.Airbus_319_Exit_Seat_11D,
						SeatsPage319.Airbus_319_Exit_Seat_11E, SeatsPage319.Airbus_319_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for the passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());

			}
		} catch (NoSuchElementException e) {
		}

		Thread.sleep(4000);
		for (int l = 0; l <= 7; l++) {
			String random_seatpath[] = { SeatsPage319.Airbus_319_Standard_Seat_12A,
					SeatsPage319.Airbus_319_Standard_Seat_12B, SeatsPage319.Airbus_319_Standard_Seat_12C,
					SeatsPage319.Airbus_319_Standard_Seat_12D, SeatsPage319.Airbus_319_Standard_Seat_12E,
					SeatsPage319.Airbus_319_Standard_Seat_12F, SeatsPage319.Airbus_319_Standard_Seat_13A,
					SeatsPage319.Airbus_319_Standard_Seat_13B };

			if (driver.findElement(By.xpath(random_seatpath[l])).isEnabled()) {
				driver.findElement(By.xpath(random_seatpath[l])).click();

			}
		}

	}

	@Then("^user trying to selects the Exit row but it should be disabled and select any random seat\"([^\"]*)\"$")
	public void user_trying_to_selects_the_Exit_row_but_it_should_be_disabled_and_select_any_random_seat(int seatCount)
			throws Throwable {
		String s = driver.findElement(By.xpath(SeatsPage319.Airbus_Name)).getText();
		log.info("Air bus name" + s);
		if (s.equalsIgnoreCase("Airbus 32A")) {
			int flag = 0;

			for (int i = 0; i <= 5; i++) {
				String seatxpath[] = { SeatsPage320.Airtbus_32_A_Exit_12A, SeatsPage320.Airtbus_32_A_Exit_12B,
						SeatsPage320.Airtbus_32_A_Exit_12C, SeatsPage320.Airtbus_32_A_Exit_12D,
						SeatsPage320.Airtbus_32_A_Exit_12E, SeatsPage320.Airtbus_32_A_Exit_12F,
						SeatsPage320.Airtbus_32_A_Exit_13A, SeatsPage320.Airtbus_32_A_Exit_13B,
						SeatsPage320.Airtbus_32_A_Exit_13C, SeatsPage320.Airtbus_32_A_Exit_13D,
						SeatsPage320.Airtbus_32_A_Exit_13E, SeatsPage320.Airtbus_32_A_Exit_13F };
				Assert.assertTrue("The disabled seats are displayed for the passenger!",
						driver.findElement(By.xpath(seatxpath[i])).isEnabled());
			}
			log.info("test log **************************");
			Thread.sleep(5000);
			for (int j = 0; j <= 7; j++) {
				String random_seatxpath[] = { SeatsPage320.Airbus_32_A_10A, SeatsPage320.Airbus_32_A_Standard_Seat_21A,
						SeatsPage320.Airbus_32_A_Standard_Seat_20C, SeatsPage320.Airbus_32_A_Standard_Seat_22F,
						SeatsPage320.Airbus_32_A_Standard_Seat_23B, SeatsPage320.Airbus_32_A_Standard_Seat_24D,
						SeatsPage320.Airbus_32_A_Standard_Seat_29B, SeatsPage320.Airbus_32_A_Standard_Seat_24A };

				if (driver.findElement(By.xpath(random_seatxpath[j])).isEnabled() && flag < seatCount) {
					driver.findElement(By.xpath(random_seatxpath[j])).click();
					flag++;
				}

			}

		} else if (s.equalsIgnoreCase("Airbus 319")) {
			int flag = 0;
			for (int k = 0; k <= 5; k++) {
				String Seatpath[] = { SeatsPage319.Airbus_319_Exit_Seat_11A, SeatsPage319.Airbus_319_Exit_Seat_11B,
						SeatsPage319.Airbus_319_Exit_Seat_11C, SeatsPage319.Airbus_319_Exit_Seat_11D,
						SeatsPage319.Airbus_319_Exit_Seat_11E, SeatsPage319.Airbus_319_Exit_Seat_11F };
				Assert.assertTrue("The disabled seats are displayed for the passenger!",
						driver.findElement(By.xpath(Seatpath[k])).isEnabled());
			}

			Thread.sleep(5000);
			for (int l = 0; l <= 7; l++) {
				String random_seatpath[] = { SeatsPage319.Airbus_319_Standard_Seat_12A,
						SeatsPage319.Airbus_319_Standard_Seat_12B, SeatsPage319.Airbus_319_Standard_Seat_12C,
						SeatsPage319.Airbus_319_Standard_Seat_12D, SeatsPage319.Airbus_319_Standard_Seat_12E,
						SeatsPage319.Airbus_319_Standard_Seat_12F, SeatsPage319.Airbus_319_Standard_Seat_13A,
						SeatsPage319.Airbus_319_Standard_Seat_13B };

				if (driver.findElement(By.xpath(random_seatpath[l])).isEnabled() && flag < seatCount) {
					driver.findElement(By.xpath(random_seatpath[l])).click();
					flag++;
				}
			}

		}

		else {
			int flag = 0;
			for (int g = 0; g <= 5; g++) {
				String Seatspath[] = { SeatsPage321.Airbus_321_Exit_Seat_11A, SeatsPage321.Airbus_321_Exit_Seat_11B,
						SeatsPage321.Airbus_321_Exit_Seat_11C, SeatsPage321.Airbus_321_Exit_Seat_11D,
						SeatsPage321.Airbus_321_Exit_Seat_11E, SeatsPage321.Airbus_321_Exit_Seat_11F,
						SeatsPage321.Airbus_321_Exit_Seat_25A, SeatsPage321.Airbus_321_Exit_Seat_25B,
						SeatsPage321.Airbus_321_Exit_Seat_25C, SeatsPage321.Airbus_321_Exit_Seat_25D,
						SeatsPage321.Airbus_321_Exit_Seat_25E, SeatsPage321.Airbus_321_Exit_Seat_25F };
				Assert.assertTrue("The disabled seats are displayed for the passenger!",
						driver.findElement(By.xpath(Seatspath[g])).isEnabled());
			}

			Thread.sleep(5000);
			for (int p = 0; p <= 7; p++) {
				String random_seatxpath[] = { SeatsPage321.Airbus_321_Standard_Seat_10B,
						SeatsPage321.Airbus_321_Standard_Seat_10C, SeatsPage321.Airbus_321_Standard_Seat_10D,
						SeatsPage321.Airbus_321_Standard_Seat_10E, SeatsPage321.Airbus_321_Standard_Seat_10F,
						SeatsPage321.Airbus_321_Standard_Seat_12A, SeatsPage321.Airbus_321_Standard_Seat_12B,
						SeatsPage321.Airbus_321_Standard_Seat_12C };

				if (driver.findElement(By.xpath(random_seatxpath[p])).isEnabled() && flag < seatCount) {
					driver.findElement(By.xpath(random_seatxpath[p])).click();
					flag++;

				}

			}
		}
	}

	@When("^user clicks on the additional services and select need help from seat$")
	public void user_clicks_on_the_additional_services_and_select_need_help_from_seat() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_gate)).click();
		log.info("user succesfully selected the Need help to from gate");
	}
	
	
	@Then("^user clicks on the additional services button and selects the completely immobile$")
	public void user_clicks_on_the_additional_services_button_and_selects_the_completely_immobile() throws Throwable {
	    
		
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
		log.info("user successfully clicked on the additional services");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_ompletely_immobile)));
		driver.findElement(By.xpath(PassengerinfoPage.adult_ompletely_immobile)).click();
		log.info("user succesfully selected the Need help to from gate");
		
		
		
		
	}
	
	 @Then("^user clicks on the additional services button and selects wheel chair manually powered$")
	    public void user_clicks_on_the_additional_services_button_and_selects_wheel_chair_manually_powered() throws Throwable {
	        
		 
			wait.until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_Additional_services)));
			driver.findElement(By.xpath(PassengerinfoPage.adult_Additional_services)).click();
			log.info("user successfully clicked on the additional services");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_own_wheel_chair)));
			driver.findElement(By.xpath(PassengerinfoPage.adult_own_wheel_chair)).click();
			log.info("user succesfully clicked on the i have my own wheel chair");
			Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
			s.selectByVisibleText("Manually Powered");
			log.info("user succesfully selected the Manually Powered");
			
		 
		 
	    }

	 @Then("^user takes the pnr code and clicks on the mytrips button and enter the lastname and confromation code and clicks on continue button$")
	    public void user_takes_the_pnr_code_and_clicks_on_the_mytrips_button_and_enter_the_lastname_and_confromation_code_and_clicks_on_continue_button() throws Throwable {
	       
		 String tmp = driver.findElement(By.xpath(PassengerinfoPage.PNR_CONFORMATION)).getText();
			String pnr = tmp.replace("Confirmation Code: ", "");
			log.info("The PNR obtained after the booking is : " + pnr);
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage319.Mytrips_button_confromationpage)));
			driver.findElement(By.xpath(SeatsPage319.Mytrips_button_confromationpage)).click();
			log.info("Clicked on the Mytrips button present on top banner");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckInPage.passengers_lastname)));
			driver.findElement(By.xpath(CheckInPage.passengers_lastname)).sendKeys("flyer");
			log.info("Passenger entering the last name as mentioned");
			driver.findElement(By.xpath(CheckInPage.confirmation_code)).sendKeys(pnr);
			log.info("Entered the confirmation code as : " + pnr);
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
			}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(MyTripsPage.Search_button)));
			driver.findElement(By.xpath(MyTripsPage.Search_button)).click();
			log.info("Then user clicked on the search button to check the info of the booked ticket");
		 
		 
	    }


	    @Then("^user clicks on the change seats button$")
	    public void user_clicks_on_the_change_seats_button() throws Throwable {
	    	String parentHandle = driver.getWindowHandle();
			wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)));
			driver.findElement(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)).click();
			log.info("user succesfully clicks on the checkin and print boarding pass on the checkin page");

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
			Thread.sleep(2000);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_borading_pass_nope_im_good)));
			driver.findElement(By.xpath(Boardingpass.checkin_borading_pass_nope_im_good)).click();
			log.info("user succesfully clicks on the i don't need bags on the popup");

			for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle2);
			
			Thread.sleep(3000);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(SeatsPage319.mytrips_select_seats_popup)));
			driver.findElement(By.xpath(SeatsPage319.mytrips_select_seats_popup)).click();
			log.info("user succesfully clicks on the get random seats");

			driver.switchTo().window(parentHandle);
		}
	    	
	    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
