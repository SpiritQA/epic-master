package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gjt.mm.mysql.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BagsPage;
import com.cucumber.pages.BookPage;
import com.cucumber.pages.FooterPage;
import com.cucumber.pages.MyAccountPage;
import com.cucumber.pages.NineFC;
import com.cucumber.pages.NineFCSignUpPage;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;

public class NINEFCStep {

	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);

	private static String NineFC_valid_emailaddress = FileReaderManager.getInstance().getTestDataReader().Ninefcemail();
	private static String NineFC_Valid_Password = FileReaderManager.getInstance().getTestDataReader().Ninepassword();

	private static String NineFc_Name_on_card = FileReaderManager.getInstance().getTestDataReader().NineFCnameoncard();

	private static String NineFC_card_Number = FileReaderManager.getInstance().getTestDataReader().NineFCcardNumber();

	private static String NineFC_Expiration_date = FileReaderManager.getInstance().getTestDataReader()
			.NineFCExpirationdate();

	@When("^user clicks on the sigin button and  switch to the popup and eneter the email address and password$")
	public void user_clicks_on_the_sigin_button_and_switch_to_the_popup_and_eneter_the_email_address_and_password()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
		driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		driver.findElement(By.xpath(NineFC.login_account_emailadress)).sendKeys(NineFC_valid_emailaddress);
		log.info("user successfully enters the email address");
		driver.findElement(By.xpath(NineFC.Login_account_password)).sendKeys(NineFC_Valid_Password);
		log.info("user successfully enters the password");

		driver.switchTo().window(parentHandle);

	}

	@When("^user clicks on the members name$")
	public void user_clicks_on_the_members_name() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_dfc_member_button)));
		driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).click();
		log.info("user successfully clicks on the member login button ");

	}

	@Then("^user clicks on the  login button on the popup$")
	public void user_clicks_on_the_login_button_on_the_popup() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.login_button_onpopup)));
		driver.findElement(By.xpath(NineFC.login_button_onpopup)).click();
		log.info("user succesfully clicks on the continue button");

	}

	@Then("^user clicks on the manage subscription link form the list$")
	public void user_clicks_on_the_manage_subscription_link_form_the_list() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions)));
		driver.findElement(By.xpath(NineFC.manage_subscriptions)).click();
		log.info("user succesfully clicks on the manage subscriptions button");
	}

	@Then("^user enetres the customers email address$")
	public void user_enetres_the_customers_email_address() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.subscribe_email_address)));
		driver.findElement(By.xpath(NineFC.subscribe_email_address)).sendKeys(NineFC_valid_emailaddress);
		log.info("user succesfully enters the subscriber email address");
	}

	@Then("^user clicks on the continue button below the customers email address$")
	public void user_clicks_on_the_continue_button_below_the_customers_email_address() throws Throwable {
		driver.findElement(By.xpath(NineFC.subscribe_email_continue_button)).click();
	}

	@Then("^user complete the form and click on the subscribe button$")
	public void user_complete_the_form_and_click_on_the_subscribe_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.validate_new_email_address)));
		driver.findElement(By.xpath(NineFC.validate_new_email_address)).sendKeys("jamalflyer@spirit.com");
		driver.findElement(By.xpath(NineFC.validate_confirm_email_address)).sendKeys("jamalflyer@spirit.com");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.subscribe_button)));
		driver.findElement(By.xpath(NineFC.subscribe_button)).click();

	}

	@Then("^user can see that subscription is created$")
	public void user_can_see_that_subscription_is_created() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.validate_the_subscription)));
		Assert.assertTrue("User is not able to subscribed",
				driver.findElement(By.xpath(NineFC.validate_the_subscription)).getText()
						.equals("Subscribe to our email deals and other great spirit.com offers!"));
		log.info("user successfully validate the subscription");

	}

	@Then("^user clicks on the free spirit link$")
	public void user_clicks_on_the_free_spirit_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.free_spirit_link)));
		driver.findElement(By.xpath(NineFC.free_spirit_link)).click();
		log.info("user successfully clicks on the free spirit link");

	}

	@Then("^user clicks on the email subscription link on the left side of the list of links$")
	public void user_clicks_on_the_email_subscription_link_on_the_left_side_of_the_list_of_links() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.email_subscription_link)));
		driver.findElement(By.xpath(NineFC.email_subscription_link)).click();
	}

	@When("^user clicks on the nine dollar fair club$")
	public void user_clicks_on_the_nine_dollar_fair_club() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_link)));
		driver.findElement(By.xpath(NineFC.nine_fc_link)).click();
		log.info("user succesfully clicks on the nine dollar fair club link ");

	}

	@Then("^user scrolldown to account section and enters the required information$")
	public void user_scrolldown_to_account_section_and_enters_the_required_information() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");
		log.info("user succesfully select the title");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");
		log.info("user succesfully enters the firstname");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");
		log.info("user succesfully enters the lastname");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");
		log.info("user succesfully enters the date of birth");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");
		log.info("user succesfully enters the email address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");
		log.info("user succesfully enters the confirm email address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
		log.info("user succesfully enters the password");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
		log.info("user succesfully enters the confirm password");

	}

	@Then("^user clicks on the continue to step two button$")
	public void user_clicks_on_the_continue_to_step_two_button() throws Throwable {
		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();
		log.info("user succesfully clicks on the continue to step two button");

	}

	@Then("^user navigates to the contact section and enters the required information$")
	public void user_navigates_to_the_contact_section_and_enters_the_required_information() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_adress)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("603 WEST SIDE");
		log.info("user successfully enters the address ");
		driver.findElement(By.xpath(NineFC.nine_fc_sigup_address2)).sendKeys("603 WEST SIDE");
		log.info("user successfully enters the address two ");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miami");
		log.info("user successfully enters the city name");
		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");
		log.info("user successfully enters the zipcode");
		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");
		log.info("user successfully enters the country name");
		log.info("user successfully enters the state name");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");
		log.info("user successfullyt enters the primary phone number");
		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundaryphone)).sendKeys("1234000000");
		log.info("user successfully enters the secoundary phone number");
		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");
		log.info("user successfully select the primary airport");
		Select s4 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundary_airport)));
		s4.selectByVisibleText("Denver, CO (DEN)");
		log.info("user successfully select the secoundary airport ");

	}

	@Then("^user click on the continue to step three button$")
	public void user_click_on_the_continue_to_step_three_button() throws Throwable {
		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();
		log.info("user successfully clicks on the continue to step three button ");
	}

	@Then("^user navigates to the billing secetion and enters the all required information$")
	public void user_navigates_to_the_billing_secetion_and_enters_the_all_required_information() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_nameoncard)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("jack flyer");
		log.info("user succesfully enters the name on the card");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");
		log.info("user succesfully enters the card number");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("0220");
		log.info("user successfully enters the expiration date ");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");
		log.info("user succesfullu enters the security code");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");
		log.info("user succesfully enetrs the billing address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");
		log.info("user succesfully enetrs the billing city");
		Select drpBillingState= new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		drpBillingState.selectByVisibleText("Florida");
		log.info("user succesfully enetrs the billing state");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("3456");
		log.info("user successfully enters the zipcode");
		Select drpCountry = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		drpCountry.selectByVisibleText("United States of America");
		log.info("user succesfully enters the billing cuntry name");
	}

	@Then("^user clicks on the signup button$")
	public void user_clicks_on_the_signup_button() throws Throwable {
		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();
		log.info("user succesfully clicks on the signup button");
	}

	@Then("^user validates the nine dollar fair club signup$")
	public void user_validates_the_nine_dollar_fair_club_signup() throws Throwable {

	}

	@Then("^user selects the one  adult passenger$")
	public void user_selects_the_one_adult_passenger() throws Throwable {
		driver.findElement(By.xpath(BookPage.clickon_passengers)).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_adult_passeneger_plusbutton)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(NineFC.nine_fc_adult_passeneger_plusbutton)).click();

		driver.findElement(By.xpath(NineFC.nine_fc_adult_minusbutton)).click();
		log.info("user selects the one adult passeneger");

	}

	@Then("^user clicks on the  nine dollar fair club and switch to the popup and clicks on close button$")
	public void user_clicks_on_the_nine_dollar_fair_club_and_switch_to_the_popup_and_clicks_on_close_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_flight_availability_contine_button)));

		WebElement ele = driver.findElement(By.xpath(NineFC.nine_fc_flight_availability_contine_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("user successfully clicks on the nine dollar fair club cotinue button");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the account link$")
	public void user_clicks_on_the_account_link() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.account_link)));
		driver.findElement(By.xpath(NineFC.account_link)).click();
		log.info("user succesfully clicks on the account link");

	}

	@Then("^user clicks on the edit billing information link$")
	public void user_clicks_on_the_edit_billing_information_link() throws Throwable {
         Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Billing_information)));
		driver.findElement(By.xpath(NineFC.Billing_information)).click();
		log.info("user succesfully clicks on the Billing link");

	}

	@Then("^user clicks on the add another button$")
	public void user_clicks_on_the_add_another_button() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.add_another_card)));
		driver.findElement(By.xpath(NineFC.add_another_card)).click();
		log.info("user succesfully clicks on the add another card link");

	}

	@Then("^user validates the navigates to add card page$")
	public void user_validates_the_navigates_to_add_card_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.add_card_validate_text)));
		driver.findElement(By.xpath(NineFC.add_card_validate_text)).getText().equals("Add Card");
		log.info("user succesfully validated the add card text ");

	}

	@Then("^user clicks on the edit link on the primary card table$")
	public void user_clicks_on_the_edit_link_on_the_primary_card_table() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_primary_card_edit_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_primary_card_edit_button)).click();
		log.info("user successfully clciks on the edit link on the primary card table");
	}

	@Then("^user verify the all billing information$")
	public void user_verify_the_all_billing_information() throws Throwable {

		String s = driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).getAttribute("Value");
		log.info("user card expiration date is" + s);

		String s2 = driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).getAttribute("value");
		log.info("user billing address is " + s2);

		String s3 = driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).getAttribute("value");
		log.info("user biiling city is " + s3);

		String s4 = driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)).getAttribute("value");
		log.info("user biiling state is " + s4);

		String s5 = driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).getAttribute("value");
		log.info("user biiling zip code is " + s5);

	}

	@Then("^user uncheck the primary card check box$")
	public void user_uncheck_the_primary_card_check_box() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)));
		driver.findElement(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)).click();
		log.info("user successfully un checked  the primary card check box ");
	}

	@Then("^user check the use same address check box$")
	public void user_check_the_use_same_address_check_box() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_use_same_address_checkbox)));
		driver.findElement(By.xpath(NineFC.Nine_fc_use_same_address_checkbox)).click();
		log.info("user succesfully check the use my address check box ");
	}

	@Then("^user clicks on the save my changes button$")
	public void user_clicks_on_the_save_my_changes_button() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_save_changes_button)));

		WebElement ele = driver.findElement(By.xpath(NineFC.Nine_fc_save_changes_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("user successfully clicks on the save my changes button ");

	}

	@Then("^user clicks on the cancel button on the primary card$")
	public void user_clicks_on_the_cancel_button_on_the_primary_card() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_button_primary_card)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_button_primary_card)).click();
		log.info("user successfully clicks on the cancel button ");

	}

	@Then("^user navigate back to the add  a card page and validate the text$")
	public void user_navigate_back_to_the_add_a_card_page_and_validate_the_text() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.add_another_card)));
		String s = driver.findElement(By.xpath(NineFC.add_another_card)).getText();
		log.info("user validates the  Text is  " + s);

	}

	@Then("^user changes the all information for the primarycard$")
	public void user_changes_the_all_information_for_the_primarycard() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_billing_address)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");
		log.info("user succesfully enetrs the billing address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");
		log.info("user succesfully enetrs the billing city");
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s.selectByVisibleText("Florida");
		log.info("user succesfully enetrs the billing state");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("3456");
		log.info("user successfully Changes the all primary card information");

	}

	@Then("^user clicks on the editbilling infromation link and validates the Billing address$")
	public void user_clicks_on_the_editbilling_infromation_link_and_validates_the_billing_address() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Billing_information)));
		driver.findElement(By.xpath(NineFC.Billing_information)).click();
		log.info("user succesfully clicks on the Billing information  link");

		String s = driver.findElement(By.xpath(NineFC.Nine_fc_billing_address_validation)).getText();
		log.info("new billing address is" + s);

	}

	@Then("^user enters the invalid characters for expiration date and validate the error messge$")
	public void user_enters_the_invalid_characters_for_expiration_date_and_validate_the_error_messge()
			throws Throwable {

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("12/00");
		log.info("user enters the inavlid date");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys(Keys.TAB);

		Assert.assertTrue("user not validated the  Expiration error message",
				driver.findElement(By.xpath(NineFC.Nine_fc_expiration_date_invalid_text_validation)).getText()
						.equals("Expiration Date is invalid"));
		log.info("User suucesfully validates the Exipartion date inavalid text ");

	}

	@Then("^user enters the invalid character for the address$")
	public void user_enters_the_invalid_character_for_the_address() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_expiration_date)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("12/20");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_billing_address)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 west #$%");
		log.info("user enters the invalid charcters in the address field ");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys(Keys.TAB);
		Assert.assertTrue("user not validated the  address error message",
				driver.findElement(By.xpath(NineFC.Nine_fc_invalid_address_text_validation)).getText()
						.equals("Only letters and numbers are allowed"));
		log.info("User suucesfully validates the inavalid  address text ");

	}

	@Then("^user enters the invalid characters in city field$")
	public void user_enters_the_invalid_characters_in_city_field() throws Throwable {
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 west side");
		log.info("user enters the invalid charcters in the address field ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_billingcity)));
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).clear();
		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("mia#$%");

		if (driver.findElement(By.xpath(NineFC.Nine_fc_save_changes_button)).isEnabled()) {

			log.info(" user able to click on savee button even entering the invalid charcters in the city filed");
		}

		else {

			log.info("save changes button is in disable state needs to fill the fiedls correctly ");
		}
	}

	@Then("^user enters the valid card information to all fileds$")
	public void user_enters_the_valid_card_information_to_all_fileds() throws Throwable {

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys(NineFc_Name_on_card);
		log.info("user succesfully enters the name on the card");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys(NineFC_card_Number);
		log.info("user succesfully enters the card number");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys(NineFC_Expiration_date);
		log.info("user succesfully enters the exiparation date on the card");

		driver.findElement(By.xpath(NineFC.Nine_fc_save_changes_button)).click();
		log.info("user succesfully clicks on the save changes button ");
	}

	@Then("^user go back to the account and validate the new card details$")
	public void user_go_back_to_the_account_and_validate_the_new_card_details() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Billing_information)));

		WebElement ele = driver.findElement(By.xpath(NineFC.Billing_information));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("user successfully clicks on the billing information ");

	}

	@Then("^user selects the primary passenger contact checkbox$")
	public void user_selects_the_primary_passenger_contact_checkbox() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)));
		driver.findElement(By.xpath(PassengerinfoPage.primary_passenger_contact_checkbox)).click();
		log.info("user successfully select the primary passenger  contact checkbox ");

	}

	@Then("^user clicks on the delete button on additional card then switch to the popup and close the popup$")
	public void user_clicks_on_the_delete_button_on_additional_card_then_switch_to_the_popup_and_close_the_popup()
			throws Throwable {

		Thread.sleep(6000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Billing_information)));
		driver.findElement(By.xpath(NineFC.Billing_information)).click();
		log.info("user succesfully clicks on the Billing link");

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_delete_button_billing_information)));
		driver.findElement(By.xpath(NineFC.Nine_fc_delete_button_billing_information)).click();
		log.info("user succesfully clicks on the delete button on the additional card ");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)).click();
		log.info("user succesfully switch to the popup and clicks on the close button");

		driver.switchTo().window(parentHandle);
	}

	@Then("^user clicsk on the delete button on additional card then switch to the popup and  clicks on delete the card$")
	public void user_clicsk_on_the_delete_button_on_additional_card_then_switch_to_the_popup_and_clicks_on_delete_the_card()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.Nine_fc_delete_button_billing_information)));
		Thread.sleep(2000);
		driver.findElement(By.xpath(NineFC.Nine_fc_delete_button_billing_information)).click();
		log.info("user succesfully clicks on the delete button on the additional card ");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)).click();
		log.info("user succesfully switch to the popup and clicks on the delete card button");

		driver.switchTo().window(parentHandle);
	}

	@Then("^use selects the check box make this is my primary card$")
	public void use_selects_the_check_box_make_this_is_my_primary_card() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)));
		driver.findElement(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)).click();
		log.info("user succesfully clicks on the primary card check box");
	}

	@Then("^user clisk on the cancel membership caret$")
	public void user_clisk_on_the_cancel_membership_caret() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_caret)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_caret)).click();
		log.info("user succesfully clicks on the cance membership caret");

	}

	@Then("^user clicks on the cancel membership button and switch to the popup and clicks on keep membership$")
	public void user_clicks_on_the_cancel_membership_button_and_switch_to_the_popup_and_clicks_on_keep_membership()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_popup_keep_membership)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_popup_keep_membership)).click();
		log.info("user successfully clicks on the keep membership button");
		driver.switchTo().window(parentHandle);

	}

	@Then("^user selects the reason from the dropdown and clicks on the cancel membership button and switch to the popup close the popup button$")
	public void user_selects_the_reason_from_the_dropdown_and_clicks_on_the_cancel_membership_button_and_switch_to_the_popup_close_the_popup_button()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_delete_card_popup_close_button)).click();
		log.info("user succesfully clicks on the popup close button");
		driver.switchTo().window(parentHandle);

	}

	@Then("^user selects the reason from the dropdown and clicks on the cancel membership button and switch to the popup Clicks on the cancel mebership button$")
	public void user_selects_the_reason_from_the_dropdown_and_clicks_on_the_cancel_membership_button_and_switch_to_the_popup_clicks_on_the_cancel_mebership_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_button)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_cancel_membership_popup_cancel_membership)));
		driver.findElement(By.xpath(NineFC.Nine_fc_cancel_membership_popup_cancel_membership)).click();
		log.info("user succesfully clicks on the cancel membership button");
		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the edit button on the primary card section$")
	public void user_clicks_on_the_edit_button_on_the_primary_card_section() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_primary_card_edit_button)));
		driver.findElement(By.xpath(NineFC.Nine_fc_primary_card_edit_button)).click();

		log.info("user succesfully clicks on the edit button on primary card section");
	}

	@Then("^user verify that prmary card check box is selected or not in order to check the purcahse of NineFC$")
	public void user_verify_that_prmary_card_check_box_is_selected_or_not_in_order_to_check_the_purcahse_of_ninefc()
			throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)));
		driver.findElement(By.xpath(NineFC.Nine_fc_make_this_primary_card_checkbox)).isSelected();
		log.info(
				"user succesfully validates the primary card check box is selected and 9FC membership has been purchased by this card");

	}

	@Then("^user validates the primary card last four digits$")
	public void user_validates_the_primary_card_last_four_digits() throws Throwable {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_primary_card_number_validation)));

		driver.findElement(By.xpath(NineFC.Nine_fc_primary_card_number_validation)).getText()
				.equals("XXXXXXXXXXXX4030");
		log.info("user succesfully validated the primary card number");
	}

	@Then("^user validates the additional cards title$")
	public void user_validates_the_additional_cards_title() throws Throwable {

		Assert.assertTrue("user not validated the additional card text",
				driver.findElement(By.xpath(NineFC.Nine_fc_additional_card_text_validation)).getText()
						.equals("Additional Cards"));

		log.info("user succesfully validated the additional card header text");
	}

	@Then("^user validates the additional card last four digits$")
	public void user_validates_the_additional_card_last_four_digits() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(NineFC.Nine_fc_additional_card_text_validation)));

		driver.findElement(By.xpath(NineFC.Nine_fc_additional_card_text_validation)).getText()
				.equals("XXXXXXXXXXXX0004");
		log.info("user succesfully validated the additional card number");

	}

	@Then("^user validate that still user able to add another card$")
	public void user_validate_that_still_user_able_to_add_another_card() throws Throwable {

		driver.findElement(By.xpath(NineFC.add_another_card)).isEnabled();
		log.info("user succesfully validates the add another card button is enabled or not");
	}

	@Then("^user clicks on the continue with 9DFC conitnue button and signup success$")
	public void user_clicks_on_the_continue_with_9DFC_conitnue_button_and_signup_success() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); // switch focus of WebDriver to
												// the next found window
												// handle (that's your newly
												// opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
		driver.findElement(By.xpath(BagsPage.popup_signup)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_choose_pwd)));
		WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_choose_pwd));
		choosepwd.click();
		choosepwd.clear();
		choosepwd.sendKeys("Spirit11!");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_confirm_pwd)));
		WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_confirm_pwd));
		confirmpwd.click();
		confirmpwd.clear();
		confirmpwd.sendKeys("Spirit11!");
		confirmpwd.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup_emailbtn)));
		WebElement signupbtn = driver.findElement(By.xpath(BagsPage.popup_signup_emailbtn));
		signupbtn.click();
		// close newly opened window when done with it
		driver.switchTo().window(FocusonBagsPage); // switch back to the
													// original window
		String currenturl = driver.getCurrentUrl();
		log.info(currenturl);
		Assert.assertTrue("User not navigated back to Bags page",
				currenturl.equals("http://qaepic01.spirit.com/book/bags"));
		log.info("User successfully navigated back to Bags page");
	}

	@Then("^user click on tool tip next to Get discounted fares and cheaper bags$")
	public void user_click_on_tool_tip_next_to_Get_discounted_fares_and_cheaper_bags() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.DiscountedFare_CheaperBags)));
		WebElement toolTipDiscountFare = driver.findElement(By.xpath(NineFC.DiscountedFare_CheaperBags));

		toolTipDiscountFare.click();

		log.info("User click on tool tip next to Get discounted fares and cheaper bags");
	}

	@Then("^user verify popup verbiage appear after clicking tool tip next to Get discounted fares and cheaper bags$")
	public void user_verify_popup_verbiage_appear_after_clicking_tool_tip_next_to_Get_discounted_fares_and_cheaper_bags()
			throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.ToolTip_Popup_Header)));

		String PopUpHeaderText = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Header)).getText();
		Assert.assertTrue("Tool Tip header text for  Get discounted fares and cheaper bags is not correct",
				PopUpHeaderText.trim().equals("50% DISCOUNT"));
		log.info("User verify Tool Tip header text for  Get discounted fares and cheaper bags");

		String PopUpHeaderClass = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Header)).getAttribute("class");
		Assert.assertTrue("Tool Tip header for  Get discounted fares and cheaper bags is not correct centre alligned",
				PopUpHeaderClass.trim().contains("text-center"));
		log.info("User verify Tool Tip header is centre alligned for  Get discounted fares and cheaper bags");

		String PopupBodyText = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Body)).getText();
		Assert.assertTrue("Tool Tip body text for  Get discounted fares and cheaper bags is not correct",
				PopupBodyText.trim().equals(
						"based on first checked bag at $9 Fare Club rate bought online before check-in compared with airport pricing."));
		log.info("User verify Tool Tip body text for  Get discounted fares and cheaper bags");
	}

	@Then("^user click on tool tip next to Covers everyone on your booking$")
	public void user_click_on_tool_tip_next_to_Covers_everyone_on_your_booking() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.CoverEveryone_OnBooking)));
		WebElement toolTipBookingCoverage = driver.findElement(By.xpath(NineFC.CoverEveryone_OnBooking));

		toolTipBookingCoverage.click();

		log.info("User click on tool tip next to Covers everyone on your booking");
	}

	@Then("^user verify popup verbiage appear after clicking tool tip next to Covers everyone on your booking$")
	public void user_verify_popup_verbiage_appear_after_clicking_tool_tip_next_to_Covers_everyone_on_your_booking()
			throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.ToolTip_Popup_Header)));

		String PopUpHeaderText = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Header)).getText();
		Assert.assertTrue("Tool Tip header text for Covers everyone on your booking is not correct",
				PopUpHeaderText.trim().equals("APPLY TO YOU AND UP TO 8 ADDITIONAL PASSENGERS"));
		log.info("User verify Tool Tip header text for  Covers everyone on your booking");

		String PopUpHeaderClass = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Header)).getAttribute("class");
		Assert.assertTrue("Tool Tip header for Covers everyone on your booking is not correct centre alligned",
				PopUpHeaderClass.trim().contains("text-center"));
		log.info("User verify Tool Tip header is centre alligned for Covers everyone on your booking");

		String PopupBodyText = driver.findElement(By.xpath(NineFC.ToolTip_Popup_Body)).getText();
		Assert.assertTrue("Tool Tip body text for Covers everyone on your booking is not correct", PopupBodyText.trim()
				.equals("on your itinerary. Passengers must be reserved on the same booking confirmation number as the $9 fare club member in order to receive membership benefits."));
		log.info("User verify Tool Tip body text for Covers everyone on your booking");

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.DiscountedFare_CheaperBags)));
		WebElement toolTipDiscountFare = driver.findElement(By.xpath(NineFC.DiscountedFare_CheaperBags));

		toolTipDiscountFare.click();
	}

	@Then("^user click on SIGN-UP NOW button$")
	public void user_click_on_SIGN_UP_NOW_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.SignUpNow_Button)));
		WebElement SignUpNowButton = driver.findElement(By.xpath(NineFC.SignUpNow_Button));

		SignUpNowButton.click();
		log.info("User click on Sign Up Button");
	}

	@Then("^user verify page is scroll down bottom of page$")
	public void user_verify_page_is_scroll_down_bottom_of_page() throws Throwable {
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Long PageYCordinate = (Long) js.executeScript("return window.pageYOffset");

		Assert.assertTrue("User is not  scroll to page bottom after clicking SIGN-UP NOW button",
				PageYCordinate > 1000);
		log.info("User scroll to page bottom after clicking SIGN-UP NOW button");
	}

	@Then("^user click on Terms and Conditions link$")
	public void user_click_on_Terms_and_Conditions_link() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.Terms_And_Conditions)));
		WebElement TermsConditionsLink = driver.findElement(By.xpath(NineFC.Terms_And_Conditions));

		TermsConditionsLink.click();
		log.info("User click on Terms and Condition Link");
	}

	@Then("^user verify navigated URL after clicking Terms and Conditions link$")
	public void user_verify_navigated_URL_after_clicking_Terms_and_Conditions_link() throws Throwable {

		Thread.sleep(4000);
		String currenturl = driver.getCurrentUrl();
		Assert.assertTrue("User not navigated Terms and Condition Link",
				currenturl.trim().contains("Documents/9FC_Terms_and_Conditions.pdf"));
		log.info("User successfully navigated Terms and Condition Link");

		driver.navigate().back();
	}

	@Then("^user click on F\\.A\\.Q link$")
	public void user_click_on_F_A_Q_link() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.FAQ)));
		WebElement FAQLink = driver.findElement(By.xpath(NineFC.FAQ));

		FAQLink.click();
		log.info("User click on F.A.Q Link");
	}

	@Then("^user verify navigated URL after clicking F\\.A\\.Q link$")
	public void user_verify_navigated_URL_after_clicking_F_A_Q_link() throws Throwable {
		Thread.sleep(4000);
		String currenturl = driver.getCurrentUrl();
		Assert.assertTrue("User not navigated FAQ Link",
				currenturl.trim().contains("https://customersupport.spirit.com/hc/en-us/categories/200154236"));
		log.info("User successfully navigated FAQ Link");

		driver.navigate().back();
	}

	@Then("^user click on  Sign-up faster and easier link$")
	public void user_click_on_Sign_up_faster_and_easier_link() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.SignUp_Faster_Easier)));
		WebElement SignUpFasterLink = driver.findElement(By.xpath(NineFC.SignUp_Faster_Easier));

		SignUpFasterLink.click();
		log.info("User click on Sign Up Faster and Eaiser Link");
	}

	@Then("^user verify popup details and close popup$")
	public void user_verify_popup_details_and_close_popup() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.login_Popup)));

		Assert.assertTrue("user successfully not verify Login popup Header Text on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_Popup_Header)).isDisplayed());
		log.info("user successfully verify Login popup Header Text on login page");

		Assert.assertTrue("user successfully not verify Email label on login page",
				driver.findElements(By.xpath(NineFC.login_Popup + NineFC.login_Popup_Label)).get(0).getText().trim()
						.equals("Email Address or Free Spirit® Number"));
		log.info("user successfully verify Email label on login page");

		Assert.assertTrue("user successfully not verify Email box on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_account_emailadress)).isDisplayed());
		log.info("user successfully verify Email box on login page");

		Assert.assertTrue("user successfully not verify Password label on login page",
				driver.findElements(By.xpath(NineFC.login_Popup + NineFC.login_Popup_Label)).get(1).getText().trim()
						.equals("Password"));
		log.info("user successfully verify Password label on login page");

		Assert.assertTrue("user successfully not verify Password box on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.Login_account_password)).isDisplayed());
		log.info("user successfully verify Password box on login page");

		Assert.assertTrue("user successfully not verify login button on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_button_onpopup)).isDisplayed());
		log.info("user successfully verify login button on login page");

		Assert.assertTrue("user successfully not verify reset password link on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_Reset_Password)).isDisplayed());
		log.info("user successfully verify reset password link on login page");

		Assert.assertTrue("user successfully not verify Sign up link on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_SignUp_link)).isDisplayed());
		log.info("user successfully verify Sign up link on login page");

		Assert.assertTrue("user successfully not verify close button on login page",
				driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_button_Close)).isDisplayed());
		log.info("user successfully verify close button on login page");

		driver.findElement(By.xpath(NineFC.login_Popup + NineFC.login_button_Close)).click();
		log.info("user successfully click close button on login page");
	}

	@Then("^user verify Title error message$")
	public void user_verify_Title_error_message() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify reuire titile error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Title is required"));
		log.info("user verify reuire titile error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require First Name error message$")
	public void user_verify_require_First_Name_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify reuire First Name error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("First Name is required"));
		log.info("user verify reuire First Name error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify invalid First Name error message$")
	public void user_verify_invalid_First_Name_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("123");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid First Name error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("First Name has an invalid format."));
		log.info("user verify invalid First Name error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Last Name error message$")
	public void user_verify_require_Last_Name_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Last Name error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Last Name is required"));
		log.info("user verify require Last Name error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify invalid Last Name error message$")
	public void user_verify_invalid_Last_Name_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("123");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Last Name error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Last Name has an invalid format."));
		log.info("user verify invalid Last Name error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Date of Birth error message$")
	public void user_verify_require_Date_of_Birth_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Date of Birth error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Date of Birth is required"));
		log.info("user verify require Date of Birth error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();

	}

	@Then("^user verify invalid Date of Birth error message$")
	public void user_verify_invalid_Date_of_Birth_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("09/05/2030");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Date of Birth error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Incorrect date format."));
		log.info("user verify invalid Date of Birth error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Email error message$")
	public void user_verify_require_Email_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Email error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Email is required"));
		log.info("user verify require Email error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify invalid Email error message$")
	public void user_verify_invalid_Email_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Email error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("email address is invalid"));
		log.info("user verify invalid Email error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Confirm Email error message$")
	public void user_verify_require_Confirm_Email_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Confirm Email error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Confirm Email is required"));
		log.info("user verify require Confirm Email error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify invalid Confirm Email error message$")
	public void user_verify_invalid_Confirm_Email_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Confirm Email error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Email and Confirm Email must match."));
		log.info("user verify invalid Confirm Email error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Password error message$")
	public void user_verify_require_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Password is required"));
		log.info("user verify require Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	//
	@Then("^user verify less than eight characters invalid Password error message$")
	public void user_verify_less_than_eight_characters_invalid_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfgh");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify more than sixteen characters invalid Password error message$")
	public void user_verify_more_than_sixteen_characters_invalid_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfgh123456789012345678901234567890");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify lower case eight characters invalid Password error message$")
	public void user_verify_lower_case_eight_characters_invalid_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfghjk");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify lower case eight characters and upper case character invalid Password error message$")
	public void user_verify_lower_case_eight_characters_and_upper_case_character_invalid_Password_error_message()
			throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfghjkA");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify lower case eight characters, upper case character and numbercial value invalid Password error message$")
	public void user_verify_lower_case_eight_characters_upper_case_character_and_numbercial_value_invalid_Password_error_message()
			throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfghjkA1");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify lower case eight characters, upper case character and special character invalid Password error message$")
	public void user_verify_lower_case_eight_characters_upper_case_character_and_special_character_invalid_Password_error_message()
			throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("asdfghjkA@");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid  Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		log.info("user verify invalid Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Confirm Password error message$")
	public void user_verify_require_Confirm_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Confirm Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Confirm Password is required"));
		log.info("user verify require Confirm Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();

	}

	@Then("^user verify invalid Confirm Password error message$")
	public void user_verify_invalid_Confirm_Password_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("asd");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Confirm Password error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Password and Confirm Password must match.."));
		log.info("user verify invalid Confirm Password error on sign up page");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).clear();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).clear();
	}

	@Then("^user verify require Address error message$")
	public void user_verify_require_Address_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miami");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Address error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Address is required"));
		log.info("user verify require Address error on sign up page");
	}

	@Then("^user verify Symbol invalid Address error message$")
	public void user_verify_Symbol_invalid_Address_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("@");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miami");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Address error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Only letters and numbers are allowed"));
		log.info("user verify invalid Address error on sign up page");
	}

	@Then("^user verify require City error message$")
	public void user_verify_require_City_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require City error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("City is required"));
		log.info("user verify require City error on sign up page");
	}

	@Then("^user verify Symbol invalid City error message$")
	public void user_verify_Symbol_invalid_City_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("@");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Symbol City error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Only letters are allowed"));
		log.info("user verify invalid Symbol City error on sign up page");
	}

	@Then("^user verify Number invalid City error message$")
	public void user_verify_Number_invalid_City_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("123");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Number City error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Only letters are allowed"));
		log.info("user verify invalid Number City error on sign up page");
	}

	@Then("^user verify require State error message$")
	public void user_verify_require_State_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33456");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("Aland Islands");

		Thread.sleep(1000);
		// Select s1 = new
		// Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		// Select s2 = new
		// Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		// s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require State error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("State/Province is required"));
		log.info("user verify require State error on sign up page");
	}

	@Then("^user verify require Zip Code error message$")
	public void user_verify_require_Zip_Code_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Zip/Postal Code error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Zip/Postal Code is required"));
		log.info("user verify require Zip/Postal Code error on sign up page");
	}

	@Then("^user verify less than four character Zip Code error message$")
	public void user_verify_less_than_four_character_Zip_Code_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("12");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid length less then 4 Zip/Postal Code error on sign up page", driver
				.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("min length 4"));
		log.info("user verify invalid length less then 4 Zip/Postal Code error on sign up page");
	}

	@Then("^user verify Symbol invalid Zip Code error message$")
	public void user_verify_Symbol_invalid_Zip_Code_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("@@@@@@");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Symbol Zip/Postal Code error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Invalid Zip/Postal Code"));
		log.info("user verify invalid Symbol Zip/Postal Code error on sign up page");
	}

	@Then("^user verify require Primary Mobile Number error message$")
	public void user_verify_require_Primary_Mobile_Number_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Primary Mobile Number error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Primary Phone is required"));
		log.info("user verify require Primary Mobile Number Code error on sign up page");
	}

	@Then("^user verify invalid Primary Mobile Number error message$")
	public void user_verify_invalid_Primary_Mobile_Number_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("@@@@@@@1234");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		// Assert.assertTrue("user not verify require Primary Mobile Number error on
		// sign up page",
		// driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("Primary
		// Phone is required"));
		log.info("user verify require Primary Mobile Number Code error on sign up page");
	}

	@Then("^user verify require Home Airport error message$")
	public void user_verify_require_Home_Airport_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		// Select s3 = new
		// Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		// s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Home Airport error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Home Airport is required"));
		log.info("user verify require Home Airport Code error on sign up page");
	}

	@Then("^user verify require Name on Card error message$")
	public void user_verify_require_Name_on_Card_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("0220");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(2000);

		log.info(driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).size());
		log.info(driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText());

		log.info("user verify require Name on Card error on sign up page");

		Assert.assertTrue("user not verify require Name on Card error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Name (As it appears on your card) is required"));
		log.info("user verify require Name on Card error on sign up page");

	}

	@Then("^user verify invalid Name on Card error message$")
	public void user_verify_invalid_Name_on_Card_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("were@@@@@");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("0220");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Name on Card error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Invalid characters entered"));
		log.info("user verify invalid Name on Card error on sign up page");
	}

	@Then("^user verify require Card Number error message$")
	public void user_verify_require_Card_Number_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		// 6011212100000004
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("0220");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Card Number error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Card Number is required"));
		log.info("user verify require Card Number error on sign up page");

	}

	@Then("^user verify invalid less than (\\d+) numbers Card Number error message$")
	public void user_verify_invalid_less_than_numbers_Card_Number_error_message(int arg1) throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		// 6011212100000004
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("1111111111111111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("0220");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Card Number error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Invalid card number."));
		log.info("user verify invalid Card Number error on sign up page");

	}

	@Then("^user verify require Expiration Date error message$")
	public void user_verify_require_Expiration_Date_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Expiration Date error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Expiration Date is required"));
		log.info("user verify require Expiration Date error on sign up page");
	}

	@Then("^user verify invalid Expiration date error message$")
	public void user_verify_invalid_Expiration_date_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/11");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("342");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid Expiration Date error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Invalid expiration date"));
		log.info("user verify invalid Expiration Date error on sign up page");
	}

	@Then("^user verify require CVV error message$")
	public void user_verify_require_CVV_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require CVV Number error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Security Code is required"));
		log.info("user verify require CVV Number error on sign up page");
	}

	@Then("^user verify invalid CVV error message$")
	public void user_verify_invalid_CVV_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("1");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("603 WEST SIDE");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid CVV Number error on sign up page", driver
				.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("min length 3"));
		log.info("user verify invalid CVV Number error on sign up page");
	}

	@Then("^user verify require Billing Address error message$")
	public void user_verify_require_Billing_Address_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Billing Address error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Address is required"));
		log.info("user verify  error on sign up page");
	}

	@Then("^user verify invalid Billing Address error message$")
	public void user_verify_invalid_Billing_Address_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("@345$%^&*");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miami");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		// Assert.assertTrue("user not verify invalid Billing Address error on sign up
		// page",
		// driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("Address
		// is required"));
		log.info("user verify invalid Billing Address error on sign up page");
	}

	@Then("^user verify require Billing City error message$")
	public void user_verify_require_Billing_City_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Billing City error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("City is required"));
		log.info("user verify require Billing City error on sign up page");
	}

	@Then("^user verify invalid Billing City error message$")
	public void user_verify_invalid_Billing_City_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("@#");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Billing City error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("City has invalid format"));
		log.info("user verify require Billing City error on sign up page");
	}

	@Then("^user verify require Billing State error message$")
	public void user_verify_require_Billing_State_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miramar");

		// Select s5 = new
		// Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		// s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("34562");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("Andorra, Principality of");

		Thread.sleep(1000);

		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Billing State error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("State is required"));
		log.info("user verify require Billing State error on sign up page");
	}

	@Then("^user verify require Billing Zip/Postal Code error message$")
	public void user_verify_require_Billing_Zip_Postal_Code_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miramar");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Zip/Postal Code error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("Zip/Postal Code is required"));
		log.info("user verify require Zip/Postal Code error on sign up page");
	}

	@Then("^user verify invalid Billing Zip/Postal Code error message$")
	public void user_verify_invalid_Billing_Zip_Postal_Code_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miramar");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("@#");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify invalid ZIP/Postal Code error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("The Zip/Postal Code must be at least 4 characters"));
		log.info("user verify invalid ZIP/Postal Code error on sign up page");
	}

	@Then("^user verify require Terms and Condition check box error message$")
	public void user_verify_require_Terms_and_Condition_check_box_error_message() throws Throwable {
		driver.navigate().refresh();
		for (int counter = 0; counter < 30; counter++) {
			String pageStyle = driver.findElement(By.xpath(NineFC.Page_Load_Complete)).getAttribute("Style");
			if (pageStyle.contains("none")) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");

		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();

		Thread.sleep(1000);

		driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).sendKeys("john flyer");
		//
		driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).sendKeys("6011212100000004");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).sendKeys("11/23");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).sendKeys("111");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).sendKeys("Miramar");

		Select s5 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)));
		s5.selectByVisibleText("Florida");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).sendKeys("33333");

		Select s6 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)));
		s6.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_signup_button)).click();

		Thread.sleep(1000);

		Assert.assertTrue("user not verify require Terms and Condition error on sign up page",
				driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim()
						.equals("You must agree to the Terms and Conditions to continue"));
		log.info("user verify require Terms and Condition  error on sign up page");

	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and login success$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_login_success() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); 
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); 
		}

		

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
		driver.findElement(By.xpath(BagsPage.popup_login)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
		WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
		choosepwd.click();
		choosepwd.clear();
		choosepwd.sendKeys(NineFC_valid_emailaddress);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
		WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_password));
		confirmpwd.click();
		confirmpwd.clear();
		confirmpwd.sendKeys(NineFC_Valid_Password);
		confirmpwd.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
		WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
		loginbtn.click();

		driver.switchTo().window(FocusonBagsPage); // switch back to the
													// original window
		String currenturl = driver.getCurrentUrl();
		log.info(currenturl);

		Thread.sleep(10000);
		Assert.assertTrue("User not navigated back to flight Availablity page",
				driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
		log.info("User successfully navigated back to flight Availablity page");
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and FS login success$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_fs_login_success()
			throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); // switch focus of WebDriver to
												// the next found window
												// handle (that's your newly
												// opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
		driver.findElement(By.xpath(BagsPage.popup_login)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
		WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
		choosepwd.click();
		choosepwd.clear();
		choosepwd.sendKeys(NineFC_valid_emailaddress);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));
		WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_password));
		confirmpwd.click();
		confirmpwd.clear();
		confirmpwd.sendKeys(NineFC_Valid_Password);
		confirmpwd.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
		WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
		loginbtn.click();

		driver.switchTo().window(FocusonBagsPage); // switch back to the
		Thread.sleep(5000); // original window
		String currenturl = driver.getCurrentUrl();
		log.info(currenturl);
		Assert.assertTrue("User not navigated back to flight Availablity page",
				driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
		log.info("User successfully navigated back to flight Availablity page");
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and continue with std fare$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_continue_with_std_fare()
			throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub);
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_cont_std_farebtn)));
		driver.findElement(By.xpath(BagsPage.popup_cont_std_farebtn)).click();
		log.info("User CONTINUE WITH STANDARD FARES");
		
		for (String window : driver.getWindowHandles()) {
			driver.switchTo().window(window); 
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_seats_i_dont_need_seats)));
		driver.findElement(By.xpath(BagsPage.popup_seats_i_dont_need_seats)).click();
		log.info("user succefully handle the seats popup");
		
		driver.switchTo().window(FocusonBagsPage); 
		
		
	
//		Thread.sleep(5000);
//		Assert.assertTrue("User not navigated back to flight Availablity page",
//				driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
//		log.info("User successfully navigated back to flight Availablity page");
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and signup failure$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_signup_failure() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue)));

		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub);
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
		driver.findElement(By.xpath(BagsPage.popup_signup)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_choose_pwd)));
		WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_choose_pwd));
		choosepwd.click();
		choosepwd.clear();
		choosepwd.sendKeys("ABCDEFGHIJ");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
		log.info(errormsg);
		Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS ONLY", errormsg.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));
		choosepwd.clear();
		choosepwd.sendKeys("123456789");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg2 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS ONLY", errormsg2.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("~!@#$%^&*");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg3 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 SPECIAL characters ONLY", errormsg3.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("Spirit1");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg4 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 7 or LESS characters", errormsg4.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("Spirit11Spirit1123");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg5 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 17 or MORE characters", errormsg5.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("SPIRIT123456");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg6 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS & LETTERS ONLY", errormsg6.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("123456!@#$%^");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg7 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 NUMBERS & SPECIAL characters ONLY", errormsg7
				.equals("Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("1234&@#!$spirit");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg8 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue(
				"Invalid error not matched when you enter 8-16 numbers, special characters and ALL LOWERCASE letters",
				errormsg8.equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("SPIRIT!@#$%^");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg9 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS & SPECIAL characters ONLY", errormsg9
				.equals("Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("spirit123456");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg10 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue("Invalid error not matched when you enter 8-16 LETTERS & NUMBERS ONLY", errormsg10.equals(
				"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("S@P*I#R&I$T%");
		choosepwd.sendKeys(Keys.TAB);
		String errormsg11 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();

		Assert.assertTrue(
				"Invalid error not matched when you enter 8-16 numbers, special characters and ALL CAPITALIZED letters",
				errormsg11.equals(
						"Password must be at least 8 characters, no more than 16 characters, must, include minimum of 1 upper and lower case letter, a numeric digit, and a special character."));

		choosepwd.clear();
		choosepwd.sendKeys("Test@123");
		choosepwd.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_confirm_pwd)));
		WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_confirm_pwd));
		confirmpwd.click();
		confirmpwd.clear();
		confirmpwd.sendKeys("Spirit11!");
		confirmpwd.sendKeys(Keys.TAB);
		String errormsg13 = driver.findElement(By.xpath(BagsPage.pop_err_must_match)).getText();
		log.info(errormsg13);
		Assert.assertTrue("User unable to get the 9DFC signup error",
				errormsg13.equals("must match"));

		driver.switchTo().window(FocusonBagsPage);
//		String currenturl = driver.getCurrentUrl();
//		log.info(currenturl);
//		Thread.sleep(10000);
//		Assert.assertTrue("User not navigated back to flight Availablity page",
//				driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
//		log.info("User successfully navigated back to flight Availablity page");
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and login failure$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_login_failure() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); // switch focus of WebDriver to
												// the next found window
												// handle (that's your newly
												// opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_login)));
		driver.findElement(By.xpath(BagsPage.popup_login)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_emailaddress)));
		WebElement email = driver.findElement(By.xpath(BagsPage.popup_emailaddress));
		email.click();
		email.clear();
		email.sendKeys(Keys.TAB);
		WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
		WebElement pwd = driver.findElement(By.xpath(BagsPage.popup_password));

		pwd.sendKeys("123654");
		loginbtn.click();

		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_user_err)));
		String errormsg = driver.findElement(By.xpath(BagsPage.pop_user_err)).getText();
		log.info("Error message should be " + errormsg);

		Assert.assertTrue("Login username error message is Invalid for without entering Email or FREE SPIRIT Number",
				errormsg.equals("Email Address or Free Spirit® Number is required"));
		log.info("Login username error message is validated for without entering Email or FREE SPIRIT Number");

		email.clear();
		email.sendKeys("mike.smith@spirit.com");
		email.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_password)));

		pwd.clear();
		pwd.sendKeys("SPIRIT123");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));

		loginbtn.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_alert_err)));
		String errormsg1 = driver.findElement(By.xpath(BagsPage.pop_alert_err)).getText();
		log.info(errormsg1);
		Assert.assertTrue("Login username error message is Invalid when you entering with Invalid Email", errormsg1
				.equals("Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
		log.info("Login username error message is validated for with entering Invalid Email");

		pwd.clear();
		pwd.sendKeys(Keys.TAB);
		loginbtn.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_pwd_err)));
		String errormsg2 = driver.findElement(By.xpath(BagsPage.pop_pwd_err)).getText();
		log.info(errormsg2);
		Assert.assertTrue("Login error message is Invalid when you entering with Invalid Email",
				errormsg2.equals("Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
		log.info("Login password error message is validated for without entering password");

		email.clear();
		pwd.clear();
		pwd.sendKeys("SPIRIT123");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_pwd_err)));
		String errormsg3 = driver.findElement(By.xpath(BagsPage.pop_pwd_err)).getText();
		log.info(errormsg3);
		Assert.assertTrue("Login error message is Invalid when you entering with Invalid Email",
				errormsg3.equals("Invalid email address or incorrect password. Please correct and re-try or select Sign Up."));
		log.info("Login password error message is validated for without entering password");
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and signup success$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_signup_success() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); // switch focus of WebDriver to
												// the next found window
												// handle (that's your newly
												// opened window)
		}

		// code to do something on new window

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup)));
		driver.findElement(By.xpath(BagsPage.popup_signup)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_choose_pwd)));
		WebElement choosepwd = driver.findElement(By.xpath(BagsPage.popup_choose_pwd));
		choosepwd.click();
		choosepwd.clear();
		choosepwd.sendKeys("Spirit11!");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_confirm_pwd)));
		WebElement confirmpwd = driver.findElement(By.xpath(BagsPage.popup_confirm_pwd));
		confirmpwd.click();
		confirmpwd.clear();
		confirmpwd.sendKeys("Spirit11!");
		confirmpwd.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_signup_emailbtn)));
		WebElement signupbtn = driver.findElement(By.xpath(BagsPage.popup_signup_emailbtn));
		signupbtn.click();
		
		for (String window : driver.getWindowHandles()) {
			driver.switchTo().window(window); 
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_seats_i_dont_need_seats)));
		driver.findElement(By.xpath(BagsPage.popup_seats_i_dont_need_seats)).click();
		log.info("user succefully handle the seats popup");
		
		driver.switchTo().window(FocusonBagsPage); 
//		Thread.sleep(5000);
//		Assert.assertTrue("User not navigated back to flight Availablity page",
//				driver.findElement(By.xpath(OnewaytripPage.FlightPage_header)).getText().equals("Choose Your Flight"));
//		log.info("User successfully navigated back to flight Availablity page");
	}

	@Then("^user clicks on terms and Conditions checkbox$")
	public void user_clicks_on_terms_and_conditions_checkbox() throws Throwable {
		driver.findElement(By.xpath(NineFC.FS_member_termsand_conditions)).click();
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and reset password$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_reset_password() throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");
		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub);
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_loginbtn)));
		WebElement loginbtn = driver.findElement(By.xpath(BagsPage.popup_loginbtn));
		loginbtn.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.pop_reset_pwd)));
		driver.findElement(By.xpath(BagsPage.pop_reset_pwd)).click();
		log.info("User clicks on Reset Password button");
		for (String iNeedBags : driver.getWindowHandles()) {
			driver.switchTo().window(iNeedBags);
		}
		log.info("User switches to Bags popup");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.Popup_dont_purchase_bags)));
		driver.findElement(By.xpath(BagsPage.Popup_dont_purchase_bags)).click();
		log.info("User selects Don't purchase Bags option button");
		driver.switchTo().window(FocusonBagsPage);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.Retrieve_pwd)));
		driver.findElement(By.xpath(BagsPage.Retrieve_pwd)).sendKeys("mike.smith1@spirit.com");
		driver.findElement(By.xpath(BagsPage.send_btn)).click();
		Assert.assertTrue("Success Password reset message is not displayed",
				driver.findElement(By.xpath(BagsPage.Pwd_reset_msg)).isDisplayed());
		log.info(driver.findElement(By.xpath(BagsPage.Pwd_reset_msg)).getText());
	}

	@Then("^user clicks on the continue with 9DFC conitnue on FA page button and validate verbiages$")
	public void user_clicks_on_the_continue_with_9dfc_conitnue_on_fa_page_button_and_validate_verbiages()
			throws Throwable {
		String FocusonBagsPage = driver.getWindowHandle(); // get the current
		String ninedfcmodal = driver.findElement(By.xpath(BagsPage.nineDFC_modal_content)).getText();
		log.info(ninedfcmodal);

		Assert.assertTrue("9DFC Modal verbiage is mismatch", ninedfcmodal.equals(
				"By choosing $9 Fare Club® Pricing fares, you are already a member or will become a club member for only $59 per year."));
		log.info("9DFC Modal content is verified");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BagsPage.nineDFC_continue))); // window
																										// handle
		WebElement ele = driver.findElement(By.xpath(BagsPage.nineDFC_continue));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("User clicks on the 9DFC conitnue button");

		Thread.sleep(5000);

		for (String nineclub : driver.getWindowHandles()) {
			driver.switchTo().window(nineclub); // switch focus of WebDriver to
												// the next found window
												// handle (that's your newly
												// opened window)
		}

		// code to do something on new window
		String ninedfcmodalcontent1 = driver.findElement(By.xpath(BagsPage.popup_modal_content_1)).getText();
		String ninedfcmodalcontent2 = driver.findElement(By.xpath(BagsPage.popup_modal_content_2)).getText();
		log.info(ninedfcmodalcontent1 + ninedfcmodalcontent2);

		Assert.assertTrue("9DFC Pricing popup modal content is mismatch",
				ninedfcmodalcontent1.equals("Join and save on flights and bags for everyone on your bookings."));
		log.info("9DFC Pricing popup modal content is verified");

		Assert.assertTrue("9DFC Pricing popup modal content is mismatch", ninedfcmodalcontent2.equals(
				"Membership charges of $59.95 are non-refundable and program renews automatically each year. Terms and Conditions apply."));
		log.info("9DFC Pricing popup modal content is verified");

		driver.findElement(By.xpath(BagsPage.popup_login)).click();

		Assert.assertTrue("9DFC Pricing popup modal content is mismatch",
				ninedfcmodalcontent1.equals("Join and save on flights and bags for everyone on your bookings."));
		log.info("9DFC Pricing popup modal content is verified");

		Assert.assertTrue("9DFC Pricing popup modal content is mismatch", ninedfcmodalcontent2.equals(
				"Membership charges of $59.95 are non-refundable and program renews automatically each year. Terms and Conditions apply."));
		log.info("9DFC Pricing popup modal content is verified");

		WebElement outsidepopup = driver.findElement(By.xpath(BagsPage.popup_modal_fade_show));
		Actions builder = new Actions(driver);
		builder.moveToElement(outsidepopup, 10, 25).click().build().perform();
		try {
			driver.findElement(By.xpath(BagsPage.popup_ninedfc_modal)).isDisplayed();
			log.info("9DFC Modal popup not closed when you click outside of the modal");
		} catch (NoSuchElementException e) {
			log.info("9DFC Modal shouldn't suppose to be closed when you click outside of the modal");
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BagsPage.popup_closebtn)));
		driver.findElement(By.xpath(BagsPage.popup_closebtn)).click();
		log.info("9DFC Pricing popup modal is successfully closed");
	}

	@Then("^user switch to the popup and clicks on close button for ninedfc$")
	public void user_switch_to_the_popup_and_clicks_on_close_button_for_nindfc() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);

		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.standard_picing_continue_button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.Treat_yourself_popup)));
		driver.findElement(By.xpath(OnewaytripPage.Treat_yourself_popup)).click();
		driver.switchTo().window(parentHandle);

	}

	   @Then("^user verify shopping cart is displayed$")
	    public void user_verify_shopping_cart_is_displayed() throws Throwable {
			Assert.assertTrue("Shoppin cart is not displayed",
					driver.findElement(By.xpath(NineFC.ShoppingCart_Itinerary)).isDisplayed());
			log.info("Shopping Cart is Displayed");
	    }
	    

	    @Then("^user Click the caret next to the Join NineFc and Save$")
	    public void user_click_the_caret_next_to_the_join_NineFc_and_save() throws Throwable {
	    	driver.findElement(By.xpath(NineFC.ShoppingCart_JoinNineFC_Carrot)).click();
	    	log.info("User Clicks on ShoppingCart Join NineFC Carrot ");
	    }

	    @Then("^user Verify verbiage on the Join and Save drop down$")
	    public void user_verify_verbiage_on_the_join_and_save_drop_down() throws Throwable {
	    	Thread.sleep(2000);
	    	
	       String ShopCartNFCVerb1 = driver.findElement(By.xpath(NineFC.ShoppingCart_Ninefc_Content_Line_One)).getText();
	       String ShopCartNFCVerbSub = ShopCartNFCVerb1.substring(0,27);
	       log.info(ShopCartNFCVerbSub);
	       Assert.assertTrue("$9FC Verbiage is incorrect",
	    		   ShopCartNFCVerbSub.equals("JOIN $9FC® PRICING AND SAVE"));
	       log.info("User Verify verbiage on the Join and Save drop down, DrpDown contains " +ShopCartNFCVerbSub);
	       
	       String ShopCartNFCVerb2 = driver.findElement(By.xpath(NineFC.ShoppingCart_Ninefc_Content_Line_Two)).getText();
	       Assert.assertTrue("$9FC Verbiage is incorrect",
	       ShopCartNFCVerb2.equals("Yes, I want to save by joining the $9 Fare Club® Pricing"));
	       log.info("User Verify verbiage on the Join and Save drop down, DrpDown contains " +ShopCartNFCVerb2);
	       
	        
	       String ShopCartNFCVerb3 = driver.findElement(By.xpath(NineFC.ShoppingCart_Ninefc_Content_Line_Three)).getText();
	       Assert.assertTrue("$9FC Verbiage is incorrect",
	       ShopCartNFCVerb3.equals("Membership charges of $59.95 are non-refundable and program renews automatically each year"));
	       log.info("User Verify verbiage on the Join and Save drop down, DrpDown contains " +ShopCartNFCVerb3);
	        
	       String ShopCartNFCVerb4 =driver.findElement(By.xpath(NineFC.ShoppingCart_Ninefc_Content_Line_Four)).getText();
	       Assert.assertTrue("$9FC Verbiage is incorrect",
	       ShopCartNFCVerb4 .equals("Terms and Conditions apply"));
	       log.info("User Verify verbiage on the Join and Save drop down, DrpDown contains " +ShopCartNFCVerb4);
	    }
	    
	    @Then("^user click the Join Now and Save button$")
	    public void user_click_the_join_now_and_save_button() throws Throwable {
	    	driver.findElement(By.xpath(NineFC.ShoppingCart_Join_Now_And_Save_button)).click();
	    }

	    @Then("^user Verify the NineFCmessage updates and instead of saying JOIN NineFC AND SAVE it now says CONGRATS$")
	    public void user_verify_the_9fc_message_updates_and_instead_of_saying_join_NineFC_and_save_it_now_says_congrats() throws Throwable {
	        Thread.sleep(3000);
			
				String ShopCartCongratsVerb = driver.findElement(By.xpath(NineFC.ShoppingCart_Ninefc_Congrats)).getText();
				String ShopCartVerbSub = ShopCartCongratsVerb.substring(0,9);
				log.info(ShopCartVerbSub);
				Assert.assertTrue("$9FC Message not updated",
						ShopCartVerbSub.equals("CONGRATS!"));
			log.info("Shopping Cart is updated with " + ShopCartVerbSub);
	    }

	    @Then("^user Verify the NineFC membership due is added to the cart$")
	    public void user_verify_the_9fc_membership_due_is_added_to_the_cart() throws Throwable {
	    	Thread.sleep(3000);
	    	driver.findElement(By.xpath(BagsPage.ShoppingCart_carrot)).click();
	    	Thread.sleep(1000);
	    	driver.findElement(By.xpath(NineFC.ShoppingCart_Option_carrot)).click();
	    	log.info("User clicks on Shopping Cart Option Carrot");
	    	Thread.sleep(1000);
	    	
	    	driver.findElement(By.xpath(NineFC.ShoppingCart_NineFc_Price_title)).getText()
	    	.equals("$9 Fare Club® Pricing Membership");
	    	
	    	driver.findElement(By.xpath(NineFC.ShoppingCart_NineFc_Price)).getText()
	    	.equals("$59.95");
	    	
	    	log.info("User verifys NINEFC membership has been updated to shopping cart");
	    }
	    
	    @When("^user lands on NineFC signup page$")
	    public void user_lands_on_ninefc_signup_page() throws Throwable {
	    	Thread.sleep(5000);
	    	driver.findElement(By.xpath(NineFCSignUpPage.NineFc_header)).getText()
	    	.equals("$9 Fare Club");
	    	log.info("User Has landed on 9fc Page");
	    }

	    @Then("^user Click on NineFC link under get to know us on home page$")
	    public void user_click_on_ninefc_link_under_get_to_know_us_on_home_page() throws Throwable {
	    	Thread.sleep(5000);
	    	driver.findElement(By.xpath(FooterPage.Get_To_Know_Us_$9_Fare_Club)).click();
	    	log.info("User Clicks on 9fc Link under GET TO KNOW US");
	    }

	    @Then("^user verifys the title of the page NineFC and NineFC fare club logo is displayed$")
	    public void user_verifys_the_title_of_the_page_ninefc_and_ninefc_fare_club_logo_is_displayed() throws Throwable {
	    	
			Assert.assertTrue("NinceFc Title is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_header)).isDisplayed());
			log.info("user verifys title displays");
			
			Assert.assertTrue("NinceFc LOGO is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Logo)).isDisplayed());
			log.info("user verifys LOGO displays");
	    }

	    @Then("^user verifys Join the club Verbiage$")
	    public void user_verifys_join_the_club_verbiage() throws Throwable {
	        String JoinClubline1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Join_The_Club_Content1)).getText();
	        String JoinClubline2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Join_The_Club_Content2)).getText();
	        String JoinClubline3 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Join_The_Club_Content3)).getText();
	        
			Assert.assertTrue("Join the club Verbiage is incorrect",
					JoinClubline1.equals("Join the club and experience the ultimate in cost savings."));
	        log.info("User verifys join the club verbiage: " +JoinClubline1);
	        
			Assert.assertTrue("Join the club Verbiage is incorrect",
					JoinClubline2.equals("Get discounted fares and cheaper bags."));
	        log.info("User verifys join the club verbiage: " +JoinClubline2);
	        
			Assert.assertTrue("Join the club Verbiage is incorrect",
					JoinClubline3.equals("Covers everyone on your booking."));
	        log.info("User verifys join the club verbiage: " +JoinClubline3);
	        
	    }

	    @Then("^user verifys SignUp Now button displayed$")
	    public void user_verifys_signup_now_button_displayed() throws Throwable {
			Assert.assertTrue("Sign Up now Button is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_header)).isDisplayed());
			log.info("Sign Up button is displayed");
	    }

	    @Then("^user verifys Images displayed and Verbiage is correct$")
	    public void user_verifys_images_displayed() throws Throwable {
	    	
			Assert.assertTrue("Fly More Image is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Fly_More_Img)).isDisplayed());
			log.info("User verifys Fly More Image is displayed");	
			
			String FlyMoreLine1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Fly_More_Content1)).getText();
			String FlyMoreLine2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Fly_More_Content2)).getText();
			
			Assert.assertTrue("Fly More Verbiage is incorrect",
					FlyMoreLine1.equals("FLY MORE"));
			Assert.assertTrue("Fly More Verbiage is incorrect",
					FlyMoreLine2.equals("Exclusive access to discounted fares and vacation packages"));
			
			log.info("User verifys Verbiage under Image FLY MORE image: " + FlyMoreLine1 + " " + FlyMoreLine2 );

			Assert.assertTrue("Bring More Image is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Bring_More_Img)).isDisplayed());
			log.info("User verifys Bring More Image is displayed");
			
			String BringMoreLine1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Bring_More_Content1)).getText();
			String BringMoreLine2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Bring_More_Content2)).getText();

			Assert.assertTrue("Bring More Verbiage is incorrect",
					BringMoreLine1.equals("BRING MORE"));
			Assert.assertTrue("Fly More Verbiage is incorrect",
					BringMoreLine2.equals("Up to 50% off bags compared to paying at the airport"));
			
			log.info("User verifys Verbiage under Bring MORE image: " + BringMoreLine1 + " " + BringMoreLine2 );
			
			Assert.assertTrue("Save More Image is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Save_More_Img)).isDisplayed());
			log.info("User verifys Save More Image is displayed");
			String SaveMoreLine1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Save_More_Content1)).getText();
			String SaveMoreLine2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Save_More_Content2)).getText();

			Assert.assertTrue("SAVE More Verbiage is incorrect",
					SaveMoreLine1.equals("SAVE MORE"));
			Assert.assertTrue("Fly More Verbiage is incorrect",
					SaveMoreLine2.equals("Enjoy the same savings for everyone in your travel itinerary"));
			
			log.info("User verifys Verbiage under SAVE MORE image: " + SaveMoreLine1 + " " + SaveMoreLine2 );
	    }

	    @Then("^user Verifys the Find More Information verbiage$")
	    public void user_verifys_the_find_more_information_verbiage() throws Throwable {
	    	
	        String FindMoreInfoLine1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line1)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine1.equals("Find more information in the program  Terms and Conditions and our  F.A.Q."));
	        log.info("Find More info line1: " + FindMoreInfoLine1);
	        
	        String FindMoreInfoLine2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line2)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine2.equals("$9 Fare Club membership enrollment is only $59.95"));
	        log.info("Find More info line2: " + FindMoreInfoLine2);
	        
	        String FindMoreInfoLine3 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line3)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine3.equals("(You could offset the enrollment cost in as little as one booking!!)"));
	        log.info("Find More info line3: " + FindMoreInfoLine3);
	        
	        String FindMoreInfoLine4 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line4)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine4.equals("Sign-up below and start enjoying all the benefits today!"));
	        log.info("Find More info line4: " + FindMoreInfoLine4);
	        
	        String FindMoreInfoLine5 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line5)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine5.equals("(Membership renews automatically each year at $69.95 unless cancelled)"));
	        log.info("Find More info line5: " + FindMoreInfoLine5);
	        
	        String FindMoreInfoLine6 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_Find_Information_Line6)).getText();
	        Assert.assertTrue("Find More information is incorrect",
	        		FindMoreInfoLine6.equals("Already a Free Spirit® Member? Sign-up faster and easier."));
	        log.info("Find More info line6: " + FindMoreInfoLine6);
	    }

	    @Then("^user clicks on Signup faster and easier link$")
	    public void user_clicks_on_signup_faster_and_easier_link() throws Throwable {
	    	driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_fasterAndEasier_Link)).click();
	    	log.info("user clicks on Signup faster and easier link");
	    }

	    @Then("^user verifys popup displays and validates verbiage$")
	    public void user_verifys_popup_displays_and_validates_verbiage() throws Throwable {
			String parentHandle = driver.getWindowHandle();
			Thread.sleep(5000);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Assert.assertTrue("Log in popup is not displayed",
				driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_fasterAndEasier_PopUp_header)).getText()
					.equals("log in to your account"));
			log.info("User verifys Log in popup is displayed");
				
	    }
	    
	    @Then("^user Verify the header of information table$")
	    public void user_verify_the_header_of_information_table() throws Throwable {
	    	
			Assert.assertTrue("Account Tab is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Account_Tab)).getText()
						.equals("Account"));
				log.info("User verifys Account Tab is displayed");
				
				Assert.assertTrue("Contact Tab is not displayed",
						driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Contact_Tab)).getText()
							.equals("Contact"));
					log.info("User verifys Contact Tab is displayed");
					
					Assert.assertTrue("Billing Tab is not displayed",
							driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_Tab)).getText()
								.equals("Billing"));
						log.info("User verifys Billing Tab is displayed");
	    }

	    @Then("^user Verify Title dropdown$")
	    public void user_verify_title_dropdown() throws Throwable {
	    	
	    	WebElement LabelTitle = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Title_label));
			Assert.assertTrue("Title Label is not displayed",
					LabelTitle.isDisplayed());
					log.info("User verifys Title Label is displayed");
				
			Assert.assertTrue("Label Display Does not say title",
					LabelTitle.getText().equals("Title"));
					log.info("User verifys Label is displayed as Title ");
				
			Assert.assertTrue("Title Label Does not contain asterik",
					LabelTitle.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Title Label contains asterik");
				
			Assert.assertTrue("Title dropdown is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_title)).isDisplayed());
					log.info("User verifys Title dropdown is displayed");
					
					driver.findElement(By.xpath(NineFC.nine_fc_signup_title)).click();
					log.info("User Clicks on Dropdown");
					
			Assert.assertTrue("Title Mr is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Title_Drp_Mr)).isDisplayed());
					log.info("User verifys Title Mr is displayed");
						
			 Assert.assertTrue("Title Mrs is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Title_Drp_Mrs)).isDisplayed());
					log.info("User verifys Title Mrs is displayed");
					
			 Assert.assertTrue("Title Ms is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Title_Drp_Ms)).isDisplayed());
					log.info("User verifys Title Ms is displayed");		
				
	    }

	    @Then("^user Verify textbox Labeled First Name is displayed$")
	    public void user_verify_textbox_labeled_first_name_is_displayed() throws Throwable {
	    	
	    	WebElement LabelFirstName = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_FirstName_label));
			Assert.assertTrue("First Name Label is not displayed",
					LabelFirstName.isDisplayed());
					log.info("User verifys First Name Label is displayed");
				
			Assert.assertTrue("Label Display Does not say First Name",
					LabelFirstName.getText().equals("First Name"));
					log.info("User verifys Label is displayed as First Name ");
				
			Assert.assertTrue("First Name Label Does not contain asterik",
					LabelFirstName.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys First Name Label contains asterik");
					
			Assert.assertTrue("First Name Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).isDisplayed());
					log.info("User verifys First Name Textbox is displayed");
	    }

	    @Then("^user Verify textbox Labeled Middle Name is displayed$")
	    public void user_verify_textbox_labeled_middle_name_is_displayed() throws Throwable {
	    	
	    	WebElement LabelMiddleName = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_MiddleName_label));
			Assert.assertTrue("Middle Name Label is not displayed",
					LabelMiddleName.isDisplayed());
					log.info("User verifys Middle Name Label is displayed");
				
			Assert.assertTrue("Label Display Does not say First Name",
					LabelMiddleName.getText().equals("Middle Name"));
					log.info("User verifys Label is displayed as Middle Name ");
					
			Assert.assertTrue("Middle Name Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_middle_name)).isDisplayed());
					log.info("User verifys Middle Name Textbox is displayed");
	    }

	    @Then("^user Verify textbox Last Name is displayed$")
	    public void user_verify_textbox_last_name_is_displayed() throws Throwable {
	    	
	    	WebElement LabelLastName = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_LastName_label));
	    	
				Assert.assertTrue("Last Name Label is not displayed",
						LabelLastName.isDisplayed());
						log.info("User verifys Last Name Label is displayed");
					
				Assert.assertTrue("Label Display Does not say Last Name",
						LabelLastName.getText().equals("Last Name"));
						log.info("User verifys Label is displayed as Last Name ");
					
				Assert.assertTrue("Last Name Label Does not contain asterik",
						LabelLastName.getAttribute("class").equals("ng-star-inserted asterisk"));
						log.info("User verifys Last Name Label contains asterik");
						
				Assert.assertTrue("Last Name Textbox is not displayed",
						driver.findElement(By.xpath(NineFC.ninedfc_member_lastname)).isDisplayed());
						log.info("User verifys Last Name Textbox is displayed");
	    }

	    @Then("^user Verify Suffix dropdown$")
	    public void user_verify_suffix_dropdown() throws Throwable {
	    	
	    	WebElement LabelSuffix = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Suffix_label));
			Assert.assertTrue("Suffix Label is not displayed",
					LabelSuffix.isDisplayed());
					log.info("User verifys Suffix Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Suffix",
					LabelSuffix.getText().equals("Suffix"));
					log.info("User verifys Label is displayed as Suffix ");
				
			Assert.assertTrue("Suffix dropdown is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Suffix_DrpDown)).isDisplayed());
					log.info("User verifys Suffix dropdown is displayed");
					
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Suffix_DrpDown)).click();
					log.info("User Clicks on Dropdown");
					
			Assert.assertTrue("Suffix Jr is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Suffix_Drp_Jr)).isDisplayed());
					log.info("User verifys Suffix Jr is displayed");
						
			 Assert.assertTrue("Suffix Sr is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Suffix_Drp_Sr)).isDisplayed());
					log.info("User verifys uffix Sr is displayed");	
	    }
	    
	    @Then("^user Verify textbox Labeled Date of Birth is displayed$")
	    public void user_verify_textbox_labeled_date_of_birth_is_displayed() throws Throwable {
	    	WebElement LabelDateOfBirth = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_DateOfBirth_label));
	    	
				Assert.assertTrue("Date of Birth Label is not displayed",
						LabelDateOfBirth.isDisplayed());
						log.info("User verifys Date of Birth Label is displayed");
					
				Assert.assertTrue("Label Display Does not say Date of Birth",
						LabelDateOfBirth.getText().equals("Date of Birth"));
						log.info("User verifys Label is displayed as Date of Birth");
					
				Assert.assertTrue("Date of Birth Label Does not contain asterik",
						LabelDateOfBirth.getAttribute("class").equals("ng-star-inserted asterisk"));
						log.info("User verifys Date of Birth Label contains asterik");
						
				Assert.assertTrue("Date of Birth Textbox is not displayed",
						driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).isDisplayed());
						log.info("User verifys Date of Birth Textbox is displayed");
	    }

	    @Then("^user Verify the Important Note is displayed$")
	    public void user_verify_the_important_note_is_displayed() throws Throwable {
	        WebElement ImportantNoteLine1 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Important_line1));
	        WebElement ImportantNoteLine2 = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Important_line2));
	        
			Assert.assertTrue("Important Content is not displayed on 9fc sign up page",
					ImportantNoteLine1.isDisplayed());
			
			Assert.assertTrue("Important Content is not displayed on 9fc sign up page",
					ImportantNoteLine1.getText().equals("IMPORTANT"));
			
			Assert.assertTrue("Important Content is not displayed on 9fc sign up page",
					ImportantNoteLine2.isDisplayed());
			
			Assert.assertTrue("Important Content is not displayed on 9fc sign up page",
					ImportantNoteLine2.getText()
					.equals("Enter your first, middle, last name and any suffix, exactly as it appears on your "
							+ "government ID that you plan to use at the airport."));
			
			log.info("User verifys Important Content on 9fc page: " + ImportantNoteLine1.getText());
			log.info("User verifys Important Content on 9fc page: " + ImportantNoteLine2.getText());
	        
	    }

	    @Then("^user Verify textbox labeled Email is displayed$")
	    public void user_verify_textbox_labeled_email_is_displayed() throws Throwable {
			
					
			WebElement LabelEmail = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Email_label));
					
			Assert.assertTrue("Email Label is not displayed",
					LabelEmail.isDisplayed());
					log.info("User verifys Email Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Email",
					LabelEmail.getText().equals("Email"));
					log.info("User verifys Label is displayed as Email");
				
			Assert.assertTrue("Email Label Does not contain asterik",
					LabelEmail.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Date of Birth Label contains asterik");
					
			Assert.assertTrue("Email Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).isDisplayed());
					log.info("User verifys Email Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Confirm Email is displayed$")
	    public void user_verify_textbox_labeled_confirm_email_is_displayed() throws Throwable {
	    	
	    	WebElement LabelConfirmEmail = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Confirm_Email_label));
			
			Assert.assertTrue("Confirm Email Label is not displayed",
					LabelConfirmEmail.isDisplayed());
					log.info("User verifys Confirm Email Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Confirm Email",
					LabelConfirmEmail.getText().equals("Confirm Email"));
					log.info("User verifys Label is displayed as Date of Birth");
				
			Assert.assertTrue("Confirm Email Label Does not contain asterik",
					LabelConfirmEmail.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Confirm Email Label contains asterik");
					
			Assert.assertTrue("Confirm Email Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).isDisplayed());
					log.info("User verifys Confirm Email Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Password is displayed$")
	    public void user_verify_textbox_labeled_password_is_displayed() throws Throwable {
	    	
	    	WebElement LabelPassword = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Password_label));
			
			Assert.assertTrue("Password Label is not displayed",
					LabelPassword.isDisplayed());
					log.info("User verifys Password Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Password",
					LabelPassword.getText().equals("Select a Password (8-16 characters include 1 upper, 1 lower, 1 special)"));
					log.info("User verifys Label is displayed as Password");
				
			Assert.assertTrue("Confirm Email Label Does not contain asterik",
					LabelPassword.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Password Label contains asterik");
					
			Assert.assertTrue("Password Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).isDisplayed());
					log.info("User verifys Password Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Confirm Password is displayed$")
	    public void user_verify_textbox_labeled_confirm_password_is_displayed() throws Throwable {
	    	
	    	WebElement LabelConfirmPassword = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Confirm_Password_label));
			
			Assert.assertTrue("Confirm Password Label is not displayed",
					LabelConfirmPassword.isDisplayed());
					log.info("User verifys Confirm Password Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Confirm Password",
					LabelConfirmPassword .getText().equals("Confirm Password (8-16 characters include 1 upper, 1 lower, 1 special)"));
					log.info("User verifys Label is displayed as Confirm Password");
				
			Assert.assertTrue("Confirm Email Label Does not contain asterik",
					LabelConfirmPassword .getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Confirm Password Label contains asterik");
					
			Assert.assertTrue("Confirm Email Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).isDisplayed());
					log.info("User verifys Confirm Password Textbox is displayed");
	    }

	    @Then("^user inputs all account information$")
	    public void user_inputs_all_account_information() throws Throwable {
	    	
			Select Title = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
			Title.selectByVisibleText("Mr.");
			String TitleVal = Title.getFirstSelectedOption().getText();
			log.info("user succesfully select the title: " + TitleVal);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("JOE");
			String firstName = driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).getAttribute("value");
			log.info("user succesfully enters the firstname: " + firstName );
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("FLYER");
			String LastName = driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).getAttribute("value");
			log.info("user succesfully enters the lastname: " + LastName);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("01/01/1980");
			String Dob = driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).getAttribute("value");
			log.info("user succesfully enters the date of birth: " + Dob);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("joe.flyer123@spirit.com");
			String Email = driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).getAttribute("value");
			log.info("user succesfully enters the email address: " + Email);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("joe.flyer123@spirit.com");
			String ConfirmEmail = driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).getAttribute("value");
			log.info("user succesfully enters the confirm email address: " + ConfirmEmail );
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
			String Password = driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).getAttribute("value");
			log.info("user succesfully enters the password: "+ Password);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
			String ConfirmPassword = driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).getAttribute("value");
			log.info("user succesfully enters the confirm password: " + ConfirmPassword );
	    }

	    @Then("^user Verify textbox labeled Address is displayed$")
	    public void user_verify_textbox_labeled_address_is_displayed() throws Throwable {
	    	
	    	Thread.sleep(3000);
	    	
	    	WebElement LabelAddress = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Address_label));
			
			Assert.assertTrue("Address Label is not displayed",
					LabelAddress.isDisplayed());
					log.info("User verifys Address Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Address",
					LabelAddress.getText().equals("Address"));
					log.info("User verifys Label is displayed as Address");
				
			Assert.assertTrue("Address Label Does not contain asterik",
					LabelAddress.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Address Label contains asterik");
					
			Assert.assertTrue("Address Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).isDisplayed());
					log.info("User verifys Address Textbox Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Address Continued is displayed$")
	    public void user_verify_textbox_labeled_address_continued_is_displayed() throws Throwable {
	    	
	    	WebElement LabelAddressContinued = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Address_Continued_label));
			
			Assert.assertTrue("Address Continued Label is not displayed",
					LabelAddressContinued.isDisplayed());
					log.info("User verifys Address Continued Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Address Continued",
					LabelAddressContinued.getText().equals("Address (Continued)"));
					log.info("User verifys Label is displayed as Address Continued");
					
			Assert.assertTrue("Address Continued Textbox is not displayed",
					driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Address_Continued_Textbox)).isDisplayed());
					log.info("User verifys Address Continued Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled City is displayed$")
	    public void user_verify_textbox_labeled_city_is_displayed() throws Throwable {
	    	
	    	WebElement LabelCity = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_City_label));
			
			Assert.assertTrue("City Label is not displayed",
					LabelCity.isDisplayed());
					log.info("User verifys Address Label is displayed");
				
			Assert.assertTrue("Label Display Does not say City",
					LabelCity.getText().equals("City"));
					log.info("User verifys Label is displayed as City");
				
			Assert.assertTrue("Address Label Does not contain asterik",
					LabelCity.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Address Label contains asterik");
					
			Assert.assertTrue("City Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).isDisplayed());
					log.info("User verifys City Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled ZipCode is displayed$")
	    public void user_verify_textbox_labeled_zipcode_is_displayed() throws Throwable {
	    	
	    	WebElement LabelZipCode = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Zip_PostalCode_label));
			
			Assert.assertTrue("Zip/Postal Code Label is not displayed",
					LabelZipCode.isDisplayed());
					log.info("User verifys Zip/Postal Code Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Zip/Postal Code",
					LabelZipCode.getText().equals("Zip/Postal Code"));
					log.info("User verifys Label is displayed as Zip/Postal Code");
				
//			Assert.assertTrue("Zip/Postal Code Does not contain asterik",
//					LabelZipCode.getAttribute("class").equals("ng-star-inserted asterisk"));
//					log.info("User verifys Zip/Postal Code contains asterik");
					
			Assert.assertTrue("Zip/Postal Code Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).isDisplayed());
					log.info("User verifys Zip/Postal Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Country is displayed$")
	    public void user_verify_textbox_labeled_country_is_displayed() throws Throwable {
	    	
	    	WebElement LabelCountry = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Country_label));
			
			Assert.assertTrue("Country Label is not displayed",
					LabelCountry.isDisplayed());
					log.info("User verifys Country Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Country",
					LabelCountry.getText().equals("Country"));
					log.info("User verifys Label is displayed as Country");
				
			Assert.assertTrue("Country Does not contain asterik",
					LabelCountry.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Country contains asterik");
					
			Assert.assertTrue("Country Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_country)).isDisplayed());
					log.info("User verifys Country Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Primary Phone Number is displayed$")
	    public void user_verify_textbox_labeled_primary_phone_number_is_displayed() throws Throwable {
	    	
	    	WebElement LabelPrimaryPhone = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_PrimaryPhone_label));
			
			Assert.assertTrue("Primary Phone Label is not displayed",
					LabelPrimaryPhone.isDisplayed());
					log.info("User verifys Primary Phone Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Primary Phone",
					LabelPrimaryPhone.getText().equals("Primary Phone"));
					log.info("User verifys Label is displayed as Primary Phone");
				
			Assert.assertTrue("Primary Phone label Does not contain asterik",
					LabelPrimaryPhone.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Country contains asterik");
					
			Assert.assertTrue("Primary Phone Textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).isDisplayed());
					log.info("User verifys Primary Phone Textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Secondary Phone Number is displayed$")
	    public void user_verify_textbox_labeledsecondary_phone_number_is_displayed() throws Throwable {
	    	
	    	WebElement LabelSecondaryPhone = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_SecondaryPhone_label));
			
				Assert.assertTrue("Secondary Phone Label is not displayed",
						LabelSecondaryPhone.isDisplayed());
						log.info("User verifys Primary Phone Label is displayed");
					
				Assert.assertTrue("Label Display Does not say Secondary Phone",
						LabelSecondaryPhone.getText().equals("Secondary Phone"));
						log.info("User verifys Label is displayed as Secondary Phone");
					
						
				Assert.assertTrue("Secondary Phone Textbox is not displayed",
						driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundaryphone)).isDisplayed());
						log.info("User verifys Secondary Phone Textbox is displayed");
	    }

	    @Then("^user Verify Dropdown labeled Home Airport is displayed$")
	    public void user_verify_dropdown_labeled_home_airport_is_displayed() throws Throwable {
	    	
	    	WebElement LabelHomeAirport = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_HomeAirport_label));
			
			Assert.assertTrue("Home Airport Label is not displayed",
					LabelHomeAirport.isDisplayed());
					log.info("User verifys Home Airport Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Home Airport",
					LabelHomeAirport.getText().equals("Home Airport"));
					log.info("User verifys Label is displayed as Home Airport");
				
			Assert.assertTrue("Home Airport label Does not contain asterik",
					LabelHomeAirport.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Home Airport contains asterik");
					
			Assert.assertTrue("Home Airport Dropdown is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)).isDisplayed());
					log.info("User verifys Home Airport Dropdown is displayed");
	    }

	    @Then("^user Verify Dropdown labeled Secondary Airport is displayed$")
	    public void user_verify_dropdown_labeled_secondary_airport_is_displayed() throws Throwable {
	    	
	    	WebElement LabelSecondaryAirport = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_SecondaryAirport_label));
			
			Assert.assertTrue("Secondary Airport Label is not displayed",
					LabelSecondaryAirport.isDisplayed());
					log.info("User verifys Secondary Airport Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Secondary Airport",
					LabelSecondaryAirport.getText().equals("Secondary Airport"));
					log.info("User verifys Label is displayed as Secondary Airport");
					
			Assert.assertTrue("Secondary Airport Dropdown is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundary_airport)).isDisplayed());
					log.info("User verifys Secondary Airport Dropdown is displayed");
	    }

	    @Then("^user inputs all Contact information$")
	    public void user_inputs_all_contact_information() throws Throwable {
	    	
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_adress)));
	    	
			driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 EXECUTIVE WAY");
			String Address1 = driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).getAttribute("value");
			log.info("user successfully enters the address: " + Address1);
			
			driver.findElement(By.xpath(NineFC.nine_fc_sigup_address2)).sendKeys(" Spirit Airlines");
			String Address2 = driver.findElement(By.xpath(NineFC.nine_fc_sigup_address2)).getAttribute("value");
			log.info("user successfully enters the address two: " + Address2);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("MIRIMAR");
			String City = driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).getAttribute("value");
			log.info("user successfully enters the city name: " + City);
			
			driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");
			String ZipCode = driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).getAttribute("value");
			log.info("user successfully enters the zipcode: " + ZipCode);
			
			Select Country = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
			Country.selectByVisibleText("United States of America");
			String CountryVal = Country.getFirstSelectedOption().getText();
			log.info("user successfully selects the country name: " + CountryVal);
		
			Select State = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
			State.selectByVisibleText("Florida");
			String StateVal = State.getFirstSelectedOption().getText();
			log.info("user successfully selects the State name: " + StateVal);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("1230000000");
			String PrimaryPhone = driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).getAttribute("value");
			log.info("user successfullyt enters the primary phone number: " + PrimaryPhone);
			
			driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundaryphone)).sendKeys("1234000000");
			String SecondaryPhone = driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundaryphone)).getAttribute("value");
			log.info("user successfully enters the secoundary phone number: " + SecondaryPhone);
			
			Select PrimaryAirport = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
			PrimaryAirport.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");
			String PrimaryAirportVal = PrimaryAirport.getFirstSelectedOption().getText();
			log.info("user successfully select the primary airport: " + PrimaryAirportVal);
			
			Select SecondaryAirport = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_secoundary_airport)));
			SecondaryAirport.selectByVisibleText("New York, NY - LaGuardia (LGA)");
			String SecondaryAirportVal = SecondaryAirport.getFirstSelectedOption().getText();
			log.info("user successfully select the secoundary airport: " + SecondaryAirportVal);

	    }


	    @Then("^user Verify textbox labeled Name on Card is displayed$")
	    public void user_verify_textbox_labeled_name_on_card_is_displayed() throws Throwable {
	    	
	    	WebElement LabelNameOnCard = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_NameOnCard_label));
			
			Assert.assertTrue("Name on Card Label is not displayed",
					LabelNameOnCard.isDisplayed());
					log.info("User verifys Name on Card Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Name on Card",
					LabelNameOnCard.getText().equals("Name on Card"));
					log.info("User verifys Label is displayed as Name on Card");
				
			Assert.assertTrue("Name on Card label Does not contain asterik",
					LabelNameOnCard.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Name on Card  contains asterik");
					
			Assert.assertTrue("Name on Card is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_nameoncard)).isDisplayed());
					log.info("User verifys Name on Card is displayed");
	    }

	    @Then("^user Verify textbox labeled Card Number is displayed$")
	    public void user_verify_textbox_labeled_card_number_is_displayed() throws Throwable {
	    	
	    	WebElement LabelCardNumber = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_CardNumber_label));
	    	
			Assert.assertTrue("Card Number Label is not displayed",
					LabelCardNumber.isDisplayed());
					log.info("User verifys Card Number Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Card Number",
					LabelCardNumber.getText().equals("Card Number"));
					log.info("User verifys Label is displayed as Card Number");
				
			Assert.assertTrue("Card Number label Does not contain asterik",
					LabelCardNumber.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Card Number contains asterik");
					
			Assert.assertTrue("Card Number is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_cardnumber)).isDisplayed());
					log.info("User verifys Card Number is displayed");
	    }

	    @Then("^user Verify textbox labeled Expiration Date is displayed$")
	    public void user_verify_textbox_labeled_expiration_date_is_displayed() throws Throwable {
	    	
	    	WebElement LabelExpirationDate = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_ExpirationDate_label));
	    	
			Assert.assertTrue("Expiration Date Label is not displayed",
					LabelExpirationDate.isDisplayed());
					log.info("User verifys Expiration Date Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Expiration Date",
					LabelExpirationDate.getText().equals("Expiration Date"));
					log.info("User verifys Label is displayed as Expiration Date");
				
			Assert.assertTrue("Expiration Date label Does not contain asterik",
					LabelExpirationDate.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Expiration Date contains asterik");
					
			Assert.assertTrue("Expiration Date textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_expiration_date)).isDisplayed());
					log.info("User verifys Expiration Date textbox is displayed");
	    }

	    @Then("^user Verify textbox labeled Security Code is displayed$")
	    public void user_verify_textbox_labeled_security_code_is_displayed() throws Throwable {
	    	
	    	WebElement LabelSecurityCode = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_SecurityCode_label));
	    	
			Assert.assertTrue("Security Code Label is not displayed",
					LabelSecurityCode.isDisplayed());
					log.info("User verifys Security Code Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Security Code",
					LabelSecurityCode.getText().equals("Security Code"));
					log.info("User verifys Label is displayed as Security Code");
				
			Assert.assertTrue("Security Code label Does not contain asterik",
					LabelSecurityCode.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Security Code contains asterik");
					
			Assert.assertTrue("Security Code textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_securitycode)).isDisplayed());
					log.info("User verifys Security Code textbox is displayed");
	    }


	    @Then("^user Verify checkbox labeled Billing Address is displayed$")
	    public void user_verify_checkbox_labeled_billing_address_is_displayed() throws Throwable {
	    	
	    	WebElement LabelBillingAddress = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_Address_label));
	    	WebElement CheckBoxBillingAddress = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_BillingAddress_Checkbox));
	    	
			Assert.assertTrue("Billing Address Label is not displayed",
					LabelBillingAddress.isDisplayed());
					log.info("User verifys Billing Address Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Billing Address",
					LabelBillingAddress.getText().equals("BILLING ADDRESS"));
					log.info("User verifys Label is displayed as Billing Address");
					
			Assert.assertTrue("Billing Address Check Box is not displayed",
					CheckBoxBillingAddress .isDisplayed());
					log.info("User verifys Billing Address Checkbox is displayed");		
				
	    }

	    @Then("^user Verify textbox labeled Address for billing is displayed$")
	    public void user_verify_textbox_labeled_address_for_billing_is_displayed() throws Throwable {
	    	
	    	WebElement LabelAddress = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_CC_Address_label));
	    	
			Assert.assertTrue("Address Label is not displayed",
					LabelAddress.isDisplayed());
					log.info("User verifys Address Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Address",
					LabelAddress.getText().equals("Address"));
					log.info("User verifys Label is displayed as Address");
				
			Assert.assertTrue("Address Label Does not contain asterik",
					LabelAddress.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys  Address  contains asterik");
					
			Assert.assertTrue("Address textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_address)).isDisplayed());
					log.info("User verifys Address textbox is displayed");
	    }
	    
	    @Then("^user Verify textbox labeled City for billing is displayed$")
	    public void user_verify_textbox_labeled_city_for_billing_is_displayed() throws Throwable {
	    	
	    		WebElement LabelBillingCity = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_City_label));
	    	
				Assert.assertTrue("City Label is not displayed",
						LabelBillingCity.isDisplayed());
						log.info("User verifys City Label is displayed");
					
				Assert.assertTrue("Label Display Does not say City",
						LabelBillingCity.getText().equals("City"));
						log.info("User verifys Label is displayed as City");
					
				Assert.assertTrue("City Label Does not contain asterik",
						LabelBillingCity.getAttribute("class").equals("ng-star-inserted asterisk"));
						log.info("User verifys City contains asterik");
						
				Assert.assertTrue("City textbox is not displayed",
						driver.findElement(By.xpath(NineFC.nine_fc_signup_billingcity)).isDisplayed());
						log.info("User verifys City textbox is displayed");
	    }

	    @Then("^user Verify Dropdown labeled State for billing is displayed$")
	    public void user_verify_dropdown_labeled_state_is_displayed() throws Throwable {
	    	
	    	WebElement LabelBillingSate = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_State_label));
	    	
			Assert.assertTrue("State Label is not displayed",
					LabelBillingSate.isDisplayed());
					log.info("User verifys State Label is displayed");
				
			Assert.assertTrue("Label Display Does not say State",
					LabelBillingSate.getText().equals("State"));
					log.info("User verifys Label is displayed as State");
				
			Assert.assertTrue("State Label Does not contain asterik",
					LabelBillingSate.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys State contains asterik");
					
			Assert.assertTrue("State Dropdoown is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_billing_state)).isDisplayed());
					log.info("User verifys State Dropdoown is displayed");
	    }

	    @Then("^user Verify textbox Zip Postal Code for billing is displayed$")
	    public void user_verify_textbox_zip_postal_code_is_displayed() throws Throwable {
	    	
	    	WebElement LabelBillingZip = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_Zip_label));
	    	
			Assert.assertTrue("Zip Postal Code label is not displayed",
					LabelBillingZip.isDisplayed());
					log.info("User verifys Address Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Zip Postal Code",
					LabelBillingZip.getText().equals("Zip/Postal Code"));
					log.info("User verifys Label is displayed as Zip Postal Code");
				
			Assert.assertTrue("Zip Postal Code Label Does not contain asterik",
					LabelBillingZip.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Zip Postal Code contains asterik");
					
			Assert.assertTrue("Zip Postal Code textbox is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_signup_billingzipcode)).isDisplayed());
					log.info("User verifys Zip Postal Code textbox is displayed");
	    }

	    @Then("^user Verify Dropdown labeled Country for billing is displayed$")
	    public void user_verify_dropdown_labeled_country_is_displayed() throws Throwable {
	    	
	    	WebElement LabelBillingCountry = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_Billing_Country_label));
	    	
			Assert.assertTrue("Country label is not displayed",
					LabelBillingCountry.isDisplayed());
					log.info("User verifys Country Label is displayed");
				
			Assert.assertTrue("Label Display Does not say Country",
					LabelBillingCountry.getText().equals("Country"));
					log.info("User verifys Label is displayed as Country");
				
			Assert.assertTrue("Country Label Does not contain asterik",
					LabelBillingCountry.getAttribute("class").equals("ng-star-inserted asterisk"));
					log.info("User verifys Country contains asterik");
					
			Assert.assertTrue("Country dropdown is not displayed",
					driver.findElement(By.xpath(NineFC.nine_fc_sigup_billingcountry)).isDisplayed());
					log.info("User verifys Country dropdown is displayed");
	    }

	    @Then("^user Verify the terms and conditions$")
	    public void user_verify_the_terms_and_conditions() throws Throwable {
	    	
	    	WebElement LabelTermsAndConditions = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_TermsAndConditions_label));
	    	WebElement TermsAndConditionsContent = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_TermsAndConditions_Content));
	    	
			Assert.assertTrue("terms and conditions label is not displayed",
					LabelTermsAndConditions.isDisplayed());
					log.info("User verifys terms and conditions label is displayed");
				
			Assert.assertTrue("Label Display Does not say LabelTermsAndConditions",
					LabelTermsAndConditions.getText().equals("$9 Fare Club® Terms and Conditions"));
					log.info("User verifys Label is displayed as Terms And Conditions");
					
			Assert.assertTrue("terms and conditions content is not displayed",
					TermsAndConditionsContent.isDisplayed());
					log.info("User verifys terms and conditions content is displayed");
						
			Assert.assertTrue("Label Display Does not say LabelTermsAndConditions",
					TermsAndConditionsContent.getText().equals("I have read and agree to the $9 Fare Club® Pricing terms and conditions."
							+ " I understand that all charges are non-refundable and that membership renews automatically each year unless cancelled."));
					log.info("User verifys Content is displayed as Terms And Conditions");
				
	    }

	    @Then("^user Verify Button labeled SignUP and Start Saving Today is displayed$")
	    public void user_verify_button_labeled_signup_and_start_saving_today_is_displayed() throws Throwable {
	    	
	    	WebElement SignUpbutton = driver.findElement(By.xpath(NineFCSignUpPage.NineFc_SignUp_and_Start_Saving_btn));
	    	
			Assert.assertTrue("Sign-up and start saving today! button is not displayed",
					SignUpbutton.isDisplayed());
					log.info("User verifys Sign-up and start saving today! button is displayed");
	    }
	    
	    @Then("^user clicks on the sigin button and switch to the popup enters the NineFC memeber and password$")
		public void user_clicks_on_the_sigin_button_and_switch_to_the_popup_enters_the_NineFC_memeber_and_password()
				throws Throwable {

			String parentHandle = driver.getWindowHandle();
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
			driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).click();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}

			driver.findElement(By.xpath(NineFC.login_account_emailadress)).sendKeys(NineFC_valid_emailaddress);
			log.info("user successfully enters the email address for 9FC");
			driver.findElement(By.xpath(NineFC.Login_account_password)).sendKeys(NineFC_Valid_Password);
			log.info("user successfully enters the password");

			driver.switchTo().window(parentHandle);

		}

	    @Then("^user Clicks 9dfc menu from header$")
	        public void user_clicks_9dfc_menu_from_header() throws Throwable {
	        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='nav2D']//a[@aria-label='$9 Fare Club® Pricing'][contains(text(),'$9 Fare Club®')]")));
	        	driver.findElement(By.xpath("//li[@class='nav2D']//a[@aria-label='$9 Fare Club® Pricing'][contains(text(),'$9 Fare Club®')]")).click();
	        	
	        }

	        @Then("^clicks signup now$")
	        public void clicks_signup_now() throws Throwable {
	        	Thread.sleep(4000);
	        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//main//div[@class='container']//a[@class='btn btn-primary']")));
	        	driver.findElement(By.xpath("//main//div[@class='container']//a[@class='btn btn-primary']")).click();
	        }

	        @Then("^enters member informations$")
	        public void enters_member_informations() throws Throwable {
	        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='nav-link active']")));
	        	driver.findElement(By.xpath("//a[@class='nav-link active']")).click();
	        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='title']")));
	    		Select s = new Select(driver.findElement(By.xpath("//select[@id='title']")));
	    		s.selectByVisibleText("Mr.");
	    		log.info("user succesfully select the title");
	    		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("GKNOWN");
	    		log.info("user succesfully enters the firstname");
//	    		driver.findElement(By.xpath(NineFC.nine_fc_signup_middle_name)).sendKeys("x'");
	    		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("TRAVELER");
	    		log.info("user succesfully enters the lastname");
	    		driver.findElement(By.xpath("//input[@id='dateOfBirth']")).sendKeys("12/12/1989");
	    		log.info("user succesfully enters the date of birth");
	    		driver.findElement(By.xpath("//input[@id='emailAddress']")).sendKeys("gknown.traveler@spirit.com");
	    		String fsemailaddress=driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).getAttribute("value");
	    		log.info("user succesfully enters the email address");
	    		driver.findElement(By.xpath("//input[@id='confirmEmail']")).sendKeys("gknown.traveler@spirit.com");
	    		String fsemail=driver.findElement(By.xpath("//input[@id='confirmEmail']")).getAttribute("value");
	    		log.info("user succesfully enters the confirm email address "+fsemail);
	    		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Spirit11!");
	    		log.info("user succesfully enters the password");
	    		driver.findElement(By.xpath("//input[@id='confirmPassword']")).sendKeys("Spirit11!");
	    		log.info("user succesfully enters the confirm password");
	    		driver.findElement(By.xpath("//button[contains(text(),'continue to step 2')]")).click();
	    		Thread.sleep(3000);
	    		driver.findElement(By.xpath("//input[@id='address1']")).sendKeys("2800 Exective Way");
	    		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Miramar");		
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='state']")));
	    		driver.findElement(By.xpath("//input[@id='state']")).sendKeys("Florida");
	    		driver.findElement(By.xpath("//input[@id='postalCode']")).sendKeys("33025");
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='country']")));
	    		Select s2 = new Select(driver.findElement(By.xpath("//select[@id='country']")));
	    		s2.selectByValue("US");
	    		driver.findElement(By.xpath("//input[@id='homePhone']")).sendKeys("8014012222");
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='primaryAirport']")));
	    		Select s3 = new Select(driver.findElement(By.xpath("//select[@id='primaryAirport']")));
	    		s3.selectByValue("FLL");
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='secondaryAirport']")));
	    		Select s4 = new Select(driver.findElement(By.xpath("//select[@id='secondaryAirport']")));
	    		s4.selectByValue("MCO");
	    		Select s1= new Select (driver.findElement(By.xpath("//select[@name='state']")));
	    		s1.selectByValue("FL");
	    		driver.findElement(By.xpath("//button[contains(text(),'continue to step 3')]")).click();
	    		Thread.sleep(3000);
	    		driver.findElement(By.xpath("//input[@id='accountHolderName']")).sendKeys("MISTER VISA");
	    		driver.findElement(By.xpath("//input[@id='cardNumber']")).sendKeys("4012001021000605");
	    		driver.findElement(By.xpath("//input[@id='expMonthYear']")).sendKeys("02/22");
	    		driver.findElement(By.xpath("//input[@id='securityCode']")).sendKeys("123");
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Please use')]")));
	    		driver.findElement(By.xpath("//label[contains(text(),'Please use')]")).click();
	    		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'I have read and agree to the $9 Fare Club® Pricing')]")));
	    		driver.findElement(By.xpath("//label[contains(text(),'I have read and agree to the $9 Fare Club® Pricing')]")).click();			
	        }

	        @Then("^clicks signup button$")
	        public void clicks_signup_button() throws Throwable {
	        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'sign-up and start saving today!')]")));
	    		driver.findElement(By.xpath("//button[contains(text(),'sign-up and start saving today!')]")).click();
	    		log.info("9DFC Sign-Up is successful");
	        }
	
	
	      
	
	        @Then("^user Verify that the title is Your FREE SPIRIT Account$")
		    public void user_verify_that_the_title_is_your_free_spirit_account() throws Throwable {
		    	
		    	Thread.sleep(5000);
		    	
		    	WebElement FreeSpiritHeader = driver.findElement(By.xpath(MyAccountPage.MyAccount_header));
		    	String FreeSpiritHeaderText = driver.findElement(By.xpath(MyAccountPage.MyAccount_header)).getText();
		    	
				Assert.assertTrue("Title is not displayed",
						FreeSpiritHeader.isDisplayed());
						log.info("User verifys Title is Displayed");
						
				Assert.assertTrue("Title does not say Your FREE SPIRIT Account",
						FreeSpiritHeaderText.equals("Your Free Spirit® Account"));
						log.info("User verifys Title says Your Free Spirit® Account");
						log.info("Title says: " + FreeSpiritHeaderText);
						
		    }

		    @Then("^user Verify the links on the left hand side of the Account page$")
		    public void user_verify_the_links_on_the_left_hand_side_of_the_account_page() throws Throwable {
		    	
		        WebElement MyAccountLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_My_Account_link));
		        WebElement PersonalInfoLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Personal_Information_link));
		        WebElement BillingInfoLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Billing_Information_link));
		        WebElement EmailSubscriptiosnsLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Email_Subscriptions_link));
		        WebElement StatementsLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Statements_link));
		        WebElement RedeemMilesLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Redeem_miles_link));
		        WebElement BuyOrGiftMilesLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Buy_or_gift_miles_link));
		        WebElement RequestMileageCreditLink = driver.findElement(By.xpath(MyAccountPage.MyAccount_Request_Mileage_Credit_link));
		        
				Assert.assertTrue("My Account link is not displayed",
						MyAccountLink.isDisplayed());
						log.info("User verifys My Account link is Displayed");
		        
				Assert.assertTrue("Personal Info link is not displayed",
						PersonalInfoLink.isDisplayed());
						log.info("User verifys Personal Info link is Displayed");
						
				Assert.assertTrue("Billing Info link is not displayed",
						BillingInfoLink.isDisplayed());
						log.info("User verifys Billing Info link is Displayed");
						
				Assert.assertTrue("Email subscriptions link is not displayed",
						EmailSubscriptiosnsLink.isDisplayed());
						log.info("User verifys Email subscriptions link is Displayed");
						
				Assert.assertTrue("Statements link is not displayed",
						StatementsLink.isDisplayed());
						log.info("User verifys Statements link is Displayed");
				        
				Assert.assertTrue("Redeem Miles link is not displayed",
						RedeemMilesLink.isDisplayed());
						log.info("User verifys Redeem Miles link is Displayed");
								
				Assert.assertTrue("Buy or Gift Miles link is not displayed",
						BuyOrGiftMilesLink.isDisplayed());
						log.info("User verifys Buy or Gift Miles link is Displayed");
								
				Assert.assertTrue("Request Mileage Credit link is not displayed",
						RequestMileageCreditLink.isDisplayed());
						log.info("User verifys Request Mileage Credit link is Displayed");
		    }

		    @Then("^user Verify the welcome message$")
		    public void user_verify_the_welcome_message() throws Throwable {
		        String WelcomeMessage= driver.findElement(By.xpath(MyAccountPage.MyAccount_Welcome_message)).getText();	        
		        String WelcomeMessageSub =WelcomeMessage.substring(0,14);
		        String WelcomeMessageName =WelcomeMessage.replace("Welcome back, ", "");
		        String MemberName = driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).getText();
		        
				Assert.assertTrue("Welcome Back message name is not matching member logged in",
						MemberName.equals(WelcomeMessageName));
						log.info("User verify Welcome Back message name is matching member logged in");
		        
				Assert.assertTrue("Welcome Back message is not displayed",
						WelcomeMessageSub.equals("Welcome back, "));
						log.info("User verifys Welcome Back is Displayed");
						
				Assert.assertTrue("Welcome message is not displayed with member name",
						WelcomeMessage.equals(WelcomeMessageSub + "" + MemberName));
				log.info("User verifys Welcome Message Display: " + WelcomeMessageSub + "" + MemberName);
		    }

		    @Then("^user Verify that your current miles for the member are shown$")
		    public void user_verify_that_your_current_miles_for_the_member_are_shown() throws Throwable {
		    	
		         String YourCurrentMiles= driver.findElement(By.xpath(MyAccountPage.MyAccount_Your_Current_miles)).getText();	
		         String YourCurrentMilesSub = YourCurrentMiles.substring(0,18);
		         String YourCurrentMilesSub2 = YourCurrentMiles.replace("Your Current Miles  ", "");
		         String YourCurrentMilesSub3 = YourCurrentMilesSub2.replace(" miles", "");
		         int YourCurrnetMilesVal = Integer.parseInt(YourCurrentMilesSub3);
		         
					Assert.assertTrue("Your current miles text does is incorrect",
							YourCurrentMilesSub.equals("Your Current Miles"));
					log.info("User verifys Your current miles text is correct");
					
					Assert.assertTrue("Your current miles is incorrect",
							YourCurrnetMilesVal >= 0);
					log.info("User verifys Your current miles text is greater than or equal to 0");
					log.info("User Verifys Current miles display as: " + YourCurrentMiles);
		         
		    }

		    @Then("^user Verify that your account number is correct$")
		    public void user_verify_that_your_account_number_is_correct() throws Throwable {
		    	driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).click();
		    	Thread.sleep(1000);
		    	String fsDrpAccountNumber = driver.findElement(By.xpath(MyAccountPage.FSDrpDwn_Free_Spirit_Account_Number)).getText();
		    	String fsDrpAccountNumberVal = fsDrpAccountNumber.replace("Free Spirit® #: ", "");
		    	String MyAccountFsNum = driver.findElement(By.xpath(MyAccountPage.MyAccount_Free_Spirit_Account_Number)).getText(); 
		    	String MyAccountFsLabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Free_Spirit_Account_label)).getText(); 
		    	
		    	
				Assert.assertTrue("Free Spirit Account Label is incorrect",
						MyAccountFsLabel.equals("Free Spirit® Account Number"));
						log.info("User verifys Free Spirit Account Label is correct");
						log.info("Free Spirit Account Label displays as: " + MyAccountFsLabel);
		    	
				Assert.assertTrue("Free Spirit Account numbers are not matching",
						fsDrpAccountNumberVal.equals(MyAccountFsNum));
						log.info("User verifys Free Spirit Account numbers are matching");
						log.info("Freespirit number displays as: " + fsDrpAccountNumberVal);
		    	
		    }

		    @Then("^user Verify that your milleage earning tier is the correct tier$")
		    public void user_verify_that_your_millage_earning_tier_is_the_correct_tier() throws Throwable {
		    	
		       String MileageEarningTierLabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Mileage_Earning_Tier_label)).getText();
		       String MileageEarningTier = driver.findElement(By.xpath(MyAccountPage.MyAccount_Mileage_Earning_Tier)).getText();
		        
				Assert.assertTrue("milleage earning tier label is incorrect",
						MileageEarningTierLabel.equals("Mileage Earning Tier"));
						log.info("User verify millage earning tier label is correct");
						log.info(" millage earning tier label is Displayed as: " + MileageEarningTierLabel);
						
						Assert.assertTrue("user millage earning tier is incorrect",
								MileageEarningTier.equals("Somebody") ||
								MileageEarningTier.equals("Elite") ||
								MileageEarningTier.equals("VIP"));
								log.info("User verify your millage earning tier is correct");
								log.info(" Users millage earning tier is Displayed as: " + MileageEarningTier);
		    }

		    @Then("^user Verify that there is a section with a marketing tile$")
		    public void user_verify_that_there_is_a_section_with_a_marketing_tile() throws Throwable {
		    	
		    	  WebElement MarketingTile = driver.findElement(By.xpath(MyAccountPage.MyAccount_Marketing_tile));
			        
					Assert.assertTrue("Marketing tile is not displayed",
							MarketingTile.isDisplayed());
							log.info("User verify Marketing tile is displayed");
		    }

		    @Then("^user Verify that there is a label that says Your Membership$")
		    public void user_verify_that_there_is_a_label_that_says_your_membership() throws Throwable {
		    	
		    	  WebElement YourMembershipHeader = driver.findElement(By.xpath(MyAccountPage.MyAccount_Your_membership_header));
			        
					Assert.assertTrue("label that says Your Membership is not displayed",
							YourMembershipHeader.isDisplayed());
							log.info("User verify label that says Your Membership is displayed");
							
					Assert.assertTrue("text that says Your Membership is not correct",
							YourMembershipHeader.getText().equals("Your Membership"));
							log.info("User verify Labbel text that says Your Membership is correct");
							log.info("Your membership is displayed as: " + YourMembershipHeader.getText());
		    }

		    @Then("^user Verify that there is a section that says Member Name$")
		    public void user_verify_that_there_is_a_seaction_that_says_member_name() throws Throwable {
		    	 String MemberNameLabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Membership_Name_Label)).getText();
		    	 String MemberName = driver.findElement(By.xpath(MyAccountPage.MyAccount_Membership_Name)).getText();
		    	 String MemberNameLink = driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).getText();
		    	 
					Assert.assertTrue("MemberName label text is not correct",
							MemberNameLabel.equals("Membership Name"));
							log.info("User verify Label text that says Your Membership Name is correct");
							log.info("membership Name Label is displayed as: " + MemberNameLabel);
							
					Assert.assertTrue("Membership Name does not contain name from memberlink",
							MemberName.contains(MemberNameLink));
							log.info("User verify Membership Name contains name from memberlink");
							log.info(MemberNameLabel + " contains: " + MemberName);
							

		    }

		    @Then("^user Verify the section that says Date Joined$")
		    public void user_verify_the_section_that_says_date_joined() throws Throwable {
		       String DateJoinedLablel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Date_Joined_Label)).getText();
		       WebElement DateJoined = driver.findElement(By.xpath(MyAccountPage.MyAccount_Date_Joined));
		       
				Assert.assertTrue("Date Joined label text is not correct",
						DateJoinedLablel.equals("Date Joined"));
						log.info("User verify Label text that says Date joined is correct");
						log.info("Date Joined Label is displayed as: " + DateJoinedLablel);
						
					Assert.assertTrue("Date Joined label text is not correct",
							DateJoined.isDisplayed());
						log.info("User verify date joined is displayed");
						log.info("Date Joined is displayed as: " + DateJoined.getText());
		    }

		    @Then("^user Verify the section that says Paid Membership type$")
		    public void user_verify_the_section_that_says_paid_membership_type() throws Throwable {
		    	
		        String PaidMemTypelabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Paid_Membership_Type_Label)).getText();
		        WebElement PaidMemType = driver.findElement(By.xpath(MyAccountPage.MyAccount_Paid_Membership_Type));
		        
				Assert.assertTrue("Paid Membership Type label text is not correct",
						PaidMemTypelabel.equals("Paid Membership Type"));
						log.info("User verify Label text that says Paid Membership Type is correct");
						log.info("Paid Membership Type Label is displayed as: " + PaidMemTypelabel);
						
				Assert.assertTrue("Paid Membership Type is displayed",
						PaidMemType.isDisplayed());
						log.info("User verify Paid Membership Type is displayed");
						
				Assert.assertTrue("Paid Membership Type is displayed",
						PaidMemType.getText().equals("$9 Fare Club®"));
						log.info("User verify Paid Membership is equal to $9 Fare Club®");
						log.info("Paid Membership is displayed as: " + PaidMemType.getText());
		    }

		    @Then("^user Verify the section that says Days Left in membership$")
		    public void user_verify_the_section_that_says_days_left_in_membership() throws Throwable {
		    	
		        String DaysLeftInMemLabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_days_left_in_Membership_label)).getText();
		        WebElement DaysLeftInMem = driver.findElement(By.xpath(MyAccountPage.MyAccount_days_left_in_Membership));
		        
		        Assert.assertTrue("Days left in Membership label text is not correct",
		        		DaysLeftInMemLabel.equals("Days Left in Membership"));
						log.info("User verify Label text that says Days left in Membership is correct");
						log.info("Days left in Membership Label is displayed as: " + DaysLeftInMemLabel);
						
			    Assert.assertTrue("Days left in Membership is not displayed",
			    		DaysLeftInMem.isDisplayed());
						log.info("User verify Days left in Membership is displayed");
						log.info("Days left in Membership is displayed as: " + DaysLeftInMem.getText());
		        
		    }

		    @Then("^user Verify the section that says Cancel membership$")
		    public void user_verify_the_section_that_says_cancel_membership() throws Throwable {
		        WebElement CancelMembershipbtn = driver.findElement(By.xpath(MyAccountPage.MyAccount_Cancel_Membership_btn));
			    Assert.assertTrue("Cancel membership button is not displayed",
			    		CancelMembershipbtn.isDisplayed());
						log.info("User verify Cancel membership button is displayed");
						log.info("Cancel membership button is displayed as: " +  CancelMembershipbtn.getText());
		        
		    }

		    @Then("^user Verify the section that says Your Reservations$")
		    public void user_verify_the_section_that_says_your_reservations() throws Throwable {
		       WebElement YourReservationHeader = driver.findElement(By.xpath(MyAccountPage.MyAccount_Your_Reservation_Header));
		       
			    Assert.assertTrue("Your Reservations header not displayed",
			    		YourReservationHeader.isDisplayed());
						log.info("User verify Your Reservations header is displayed");
						
				Assert.assertTrue("Your Reservations header text is incorrect ",
					    YourReservationHeader.getText().equals("Your Reservations"));
						log.info("User verify Your Reservations header text is correct");
						log.info("Reservations header is displayed as: " + YourReservationHeader.getText());   
		    }

		    @Then("^user Verify the section that says Locate a reservation$")
		    public void user_verify_the_section_that_says_locate_a_reservation() throws Throwable {
		    	
		    	String LocateReservationContent = driver.findElement(By.xpath(MyAccountPage.MyAccount_Locate_Reservation_content)).getText();
		    	
		    	 Assert.assertTrue("Locate a reservation not displayed",
		    			 LocateReservationContent.equals("Locate A Reservation: Enter your six character Confirmation Code to locate and add your"
		    			 		+ " Free Spirit Number to the reservation. Mileage for previously flown flights will be credited instantly, "
		    			 		+ "future flights will be credited within 24 hours of flight departure."));
							log.info("User verifys Locate a reservation Section");
							log.info("Locate a reservation context is displayed as: " + LocateReservationContent);
		    	
		    }

		    @Then("^user Verify the section with the confirmation code$")
		    public void user_verify_the_section_with_the_confirmation_code() throws Throwable {
		      WebElement ConfirmCodeLabel = driver.findElement(By.xpath(MyAccountPage.MyAccount_Confirmation_Code_Label));
		      WebElement ConfirmCodeTxtBox = driver.findElement(By.xpath(MyAccountPage.MyAccount_Confirmation_Code_TextBox));
		      WebElement ConfirmCodeGoBtn = driver.findElement(By.xpath(MyAccountPage.MyAccount_Confirmation_Code_Go_btn));
		      
		      Assert.assertTrue("Confirmation Code label text is incorrect",
		    		  ConfirmCodeLabel.getText().equals("Confirmation Code (6 characters)"));
						log.info("User verify Confirmation Code label text is correct");
						log.info("Confirmation Code is displayed as: " + ConfirmCodeLabel.getText());
						
		      Assert.assertTrue("Confirmation Code textbox is not displayed",
		    		  ConfirmCodeTxtBox.isDisplayed());
						log.info("User verify Confirmation Code textbox is not displayed");
								
			  Assert.assertTrue("Confirmation Code Go button is not displayed",
					  ConfirmCodeGoBtn.isDisplayed());
						log.info("User verify Confirmation Code Go button is displayed");
		    }

		    @Then("^user Verify the section with Current Reservations$")
		    
		    public void user_verify_the_section_with_current_reservations() throws Throwable {
		    	
		     WebElement CurrentResHeader = driver.findElement(By.xpath(MyAccountPage.MyAccount_Current_Reservation_Header));
		     
		     Assert.assertTrue("Current Reservations header not displayed",
		    		    CurrentResHeader.isDisplayed());
						log.info("User verify Current Reservations header is displayed");
						
			 Assert.assertTrue("Your Reservations header text is incorrect ",
						CurrentResHeader.getText().equals("Current Reservations"));
						log.info("User verify Current Reservations header text is correct");
						log.info("Current Reservations header is displayed as: " +  CurrentResHeader.getText());   
		    }

		    @Then("^user Verify the section with Past Reservations$")
		    public void user_verify_the_section_with_past_reservations() throws Throwable {
		        
			     WebElement PastResHeader = driver.findElement(By.xpath(MyAccountPage.MyAccount_Past_Reservations_Header));
			     
			     Assert.assertTrue("Current Reservations header not displayed",
			    		 PastResHeader.isDisplayed());
							log.info("User verify Current Reservations header is displayed");
							
					Assert.assertTrue("Your Reservations header text is incorrect ",
							PastResHeader.getText().equals("Past Reservations"));
							log.info("User verify Past Reservations header text is correct");
							log.info("Past Reservations header is displayed as: " +  PastResHeader.getText());   
		    }
		    
		    @Then("^user Verify the headline of the page on add card page$")
		    public void user_verify_the_headline_of_the_page_on_add_card_page() throws Throwable {
		    	
		    	String AddCardHeader = driver.findElement(By.xpath(NineFC.add_card_validate_text)).getText();
		    	
				Assert.assertTrue("headline of the page is incorrect ",
						AddCardHeader.equals("Add Card"));
						log.info("User verify headline of the page text is correct");
						log.info("headline of the page is displayed as: " +  AddCardHeader); 
		    }

		    
		    @Then("^user Verify there is a name card text box on add card page$")
		    public void user_verify_there_is_a_name_card_text_box_on_add_card_page() throws Throwable {
		  
		        WebElement NameOnCardLabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_Name_label));
//		        String NameOnCardTxt = NameOnCardLabel.getText();
		        WebElement NameOnCardTxtBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Name_TextBox));
		        
				Assert.assertTrue("Label for Name on Card is incorrect",
						NameOnCardLabel.getText().equals("Name On Card"));
						log.info("User verify Label for Name on Card is correct");
						log.info("Label for Name on Card is displayed as: " +  NameOnCardLabel.getText()); 
						
						Assert.assertTrue("Label for Name on Card Does not contain asterik",
								NameOnCardLabel.getAttribute("class").equals("ng-star-inserted asterisk"));
								log.info("User verifys Label for Name on Card contains asterik");
								
								Assert.assertTrue("Name on Card textbox does not display",
										NameOnCardTxtBox.isDisplayed());
										log.info("User verifys Name on Card textbox displays");
		        
		    }		

				
			@Then("^user Verify there is a Card Number text box on add card page$")
		    public void user_verify_there_is_a_card_number_text_box_on_add_card_page() throws Throwable {
		    	
		        WebElement CardNumberLabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_Card_number_label));
		        WebElement CardNumberTxtBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Card_Number_TextBox));
		        
				Assert.assertTrue("Label for Card Number is incorrect",
						CardNumberLabel.getText().equals("Card Number"));
						log.info("User verify Label for Card Number is correct");
						log.info("Label for Card Number is displayed as: " +  CardNumberLabel.getText()); 
						
						Assert.assertTrue("Label for Card Number Does not contain asterik",
								CardNumberLabel.getAttribute("class").equals("ng-star-inserted asterisk"));
								log.info("User verifys Label for Card Number contains asterik");
								
								Assert.assertTrue("Card Number textbox does not display",
										CardNumberTxtBox.isDisplayed());
										log.info("User verifys Card Number textbox displays");
		    }

		    @Then("^user Verify there is a Expiration Date text box on add card page$")
		    public void user_verify_there_is_a_expiration_date_text_box_on_add_card_page() throws Throwable {
		        WebElement ExpirationDatelabel = driver.findElement(By.xpath(MyAccountPage.Add_card_Expiration_Date_label));
		        WebElement ExpirationDateTextBox = driver.findElement(By.xpath(MyAccountPage.Add_card_Expiration_Date_TextBox));
		        
				Assert.assertTrue("Label for Expiration Date is incorrect",
						ExpirationDatelabel.getText().equals("Expiration Date"));
						log.info("User verify Label for Expiration Date is correct");
						log.info("Label for Expiration Date is displayed as: " +  ExpirationDatelabel.getText()); 
						
						Assert.assertTrue("Label for Expiration Date Does not contain asterik",
								ExpirationDatelabel.getAttribute("class").equals("ng-star-inserted asterisk"));
								log.info("User verifys Label for Expiration Date contains asterik");
								
								Assert.assertTrue("Card Expiration Date textbox does not display",
										ExpirationDateTextBox.isDisplayed());
										log.info("User verifys Expiration Date textbox displays");
		    }

		    @Then("^user Verify there is no section for Security code on add card page$")
		    public void user_verify_there_is_no_section_for_security_code_on_add_card_page() throws Throwable {
					        
				        try {
				           driver.findElement(By.xpath(OnewaytripPage.security_code)).isDisplayed();
				        } catch (Exception e) {
				            log.info("user Verify there is no section for Security code " + e);
				        }
		    }

		    @Then("^user Verify the Please use for my billing information check box on add card page$")
		    public void user_verify_the_please_use_for_my_billing_information_check_box_on_add_card_page() throws Throwable {
		        
		    	WebElement BillingInfoCheckBox = driver.findElement(By.xpath(MyAccountPage.Add_card_Use_My_Billing_info_CheckBox));
		    	
		    	Assert.assertTrue("billing information check box does not display",
		    			BillingInfoCheckBox.isDisplayed());
						log.info("User verifys billing information check box displays");
						
		    }

		    @Then("^user Click the Please use your address for my billing information check box on add card page$")
		    public void user_click_the_please_use_your_address_for_my_billing_information_check_box_on_add_card_page() throws Throwable {
		      
		    	WebElement BillingInfoCheckBox = driver.findElement(By.xpath(MyAccountPage.Add_card_Use_My_Billing_info_CheckBox));
		    	
				BillingInfoCheckBox.click();
				
				log.info("User clicks on billing information check box");
		    }

		    @Then("^user Verify the Address text box on add card page$")
		    public void user_verify_the_address_text_box_on_add_card_page() throws Throwable {
		    	
		        WebElement AddressTextBoxlabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_Address_label));
		        WebElement AddressTextBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Adress_TextBox));
		        
				Assert.assertTrue("Label for Address text box is incorrect",
						AddressTextBoxlabel.getText().equals("Address"));
						log.info("User verify Label for Address text box is correct");
						log.info("Label for Address text box is displayed as: " +  AddressTextBoxlabel.getText());  
						
						Assert.assertTrue("Label for Address text box Does not contain asterik",
								AddressTextBoxlabel.getAttribute("class").contains("asterisk"));
								log.info("User verifys Label for Expiration Date contains asterik");
								
								Assert.assertTrue("Address text box does not display",
										AddressTextBox.isDisplayed());
										log.info("User verifys Address text box displays");
		    }

		    @Then("^user Verify the City text box on add card page$")
		    public void user_verify_the_city_text_box_on_add_card_page() throws Throwable {
		    	
		        WebElement Citylabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_City_label));
		        WebElement CityTextBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_City_TextBox));
		        
				Assert.assertTrue("Label for City text box is incorrect",
						Citylabel.getText().equals("City"));
						log.info("User verify Label for City text box is correct");
						log.info("Label for City text box is displayed as: " +  Citylabel.getText()); 
						
						Assert.assertTrue("Label for City text box Does not contain asterik",
								Citylabel.getAttribute("class").contains("asterisk"));
								log.info("User verifys Label for City contains asterik");
								
								Assert.assertTrue("City text box does not display",
										CityTextBox.isDisplayed());
										log.info("User verifys City text box displays");
		    }

		    @Then("^user Verify the Zip Postal Code text box on add card page$")
		    public void user_verify_the_zippostal_code_text_box_on_add_card_page() throws Throwable {
		    	
		        WebElement ZipPostalCodelabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_label));
		        WebElement ZipPostalCodeTextBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_TextBox));
		        
				Assert.assertTrue("Label for ZipCode is incorrect",
						ZipPostalCodelabel.getText().equals("Zip/Postal Code"));
						log.info("User verify Label for ZipCode is correct");
						log.info("Label for ZipCode is displayed as: " +  ZipPostalCodelabel.getText()); 
						
								Assert.assertTrue("ZipCode text box does not display",
										ZipPostalCodeTextBox.isDisplayed());
										log.info("User verifys ZipCode text box displays");
		    }

		    @Then("^user Verify the State drop box on add card page$")
		    public void user_verify_the_state_drop_box_on_add_card_page() throws Throwable {
		        WebElement Statelabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_State_label));
		        WebElement StateTextBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_State_TextBox));
		        
				Assert.assertTrue("Label for State text box is incorrect",
						Statelabel.getText().equals("State"));
						log.info("User verify Label for State text box is correct");
						log.info("Label for State text box is displayed as: " +  Statelabel.getText()); 
								
								Assert.assertTrue("State text box does not display",
										StateTextBox.isDisplayed());
										log.info("User verifys State text box displays");
		    }

		    @Then("^user Verify the country drop box on add card page$")
		    public void user_verify_the_country_drop_box_on_add_card_page() throws Throwable {
		    	
		        WebElement Countrylabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_Country_label));
		        WebElement CountryDrpDwn = driver.findElement(By.xpath(MyAccountPage.Add_Card_Country_Drpdwn));
		        WebElement ZipPostalCodelabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_label));
		        WebElement Statelabel = driver.findElement(By.xpath(MyAccountPage.Add_Card_State_label));
		        
				Assert.assertTrue("Label for Country is incorrect",
						Countrylabel.getText().equals("Country"));
						log.info("User verify Label for Country is correct");
						log.info("Label for City text box is displayed as: " +  Countrylabel.getText()); 
						
						Assert.assertTrue("Label for Country text box Does not contain asterik",
								Countrylabel.getAttribute("class").contains("asterisk"));
								log.info("User verifys Label for Country contains asterik");
								
								Assert.assertTrue("Country dropdown does not display",
										CountryDrpDwn.isDisplayed());
										log.info("User verifys Country dropdown displays");
										
										Select CountryDrpDwnSel = new Select(CountryDrpDwn);
										
										CountryDrpDwnSel.selectByVisibleText("United States of America");
										
										Assert.assertTrue("Label for ZipCode Does not contain asterik",
												ZipPostalCodelabel.getAttribute("class").contains("asterisk"));
												log.info("User verifys Label for ZipCode contains asterik");
												
												Assert.assertTrue("Label for State Does not contain asterik",
														Statelabel.getAttribute("class").contains("asterisk"));
														log.info("User verifys Label for State contains asterik");
														
		    }

		    @Then("^user Verify there is a Save and Cancel button on add card page$")
		    public void user_verify_there_is_a_save_and_cancel_button_on_add_card_page() throws Throwable {
		    WebElement SaveChangesBtn = driver.findElement(By.xpath(MyAccountPage.Add_Card_Save_changes_Btn));
		    WebElement CancelBtn = driver.findElement(By.xpath(MyAccountPage.Add_Card_Cancel_Btn));
		    
				Assert.assertTrue("Save Changes Button does not display",
						SaveChangesBtn.isDisplayed());
							log.info("User verifys Save Changes Button displays");
					
					Assert.assertTrue("Cancel Button does not display",
							CancelBtn.isDisplayed());
								log.info("User verifys Cancel Button displays");
		    }
		    
		    @Then("^user clicks on save button on add card page$")
		    public void user_clicks_on_save_button_on_add_card_page() throws Throwable {
		    	WebElement SaveChangesBtn = driver.findElement(By.xpath(MyAccountPage.Add_Card_Save_changes_Btn));
		    	SaveChangesBtn.click();
		    	log.info("User Clicks on Save Changes Button");
		    }
		    
		    @Then("^user Enter invalid characters for name and enter valid information for all other fields$")
		    public void user_enter_invalid_characters_for_name_and_enter_valid_information_for_all_other_fields() throws Throwable {
		    	WebElement NameOnCardTxtBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Name_TextBox));
		    	WebElement CardNumberTxtBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Card_Number_TextBox));
		    	WebElement ExpirationDateTextBox = driver.findElement(By.xpath(MyAccountPage.Add_card_Expiration_Date_TextBox));
		    	
		    	NameOnCardTxtBox.sendKeys("$!%&*?");
		    	CardNumberTxtBox.sendKeys("4012001021000605");
		    	ExpirationDateTextBox.sendKeys("01/28");
		    	
		    	String NameOnCardError = driver.findElement(By.xpath(MyAccountPage.Add_Card_Name_Error_Message)).getText();
		    	
				Assert.assertTrue("Name Error Message is incorrect",
						NameOnCardError.equals("Invalid characters entered"));
							log.info("User verifys Name Error Message is correct");
							log.info("Name error message: " + NameOnCardError);
							
					    	NameOnCardTxtBox.clear();
					    	NameOnCardTxtBox.sendKeys("Joe");
		    }

		    @Then("^user Enter invalid characters for card numberand enter valid information for all other fields$")
		    public void user_enter_invalid_characters_for_card_numberand_enter_valid_information_for_all_other_fields() throws Throwable {
		    
		    	WebElement CardNumberTxtBox = driver.findElement(By.xpath(MyAccountPage.Add_Card_Card_Number_TextBox));
		   	
		    	CardNumberTxtBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		    	CardNumberTxtBox.sendKeys(Keys.BACK_SPACE);

		    	CardNumberTxtBox.sendKeys("INVALIDKEYS");
		    	
		    	String CreditCardError = driver.findElement(By.xpath(MyAccountPage.Add_Card_Card_Number_Error_message)).getText();
		    	
				Assert.assertTrue("Credit Card Error Message is incorrect",
						CreditCardError.equals("Card Number is required"));
							log.info("User verifys Credit Card Error Message is correct");
							log.info("Credit card error message: " + CreditCardError);
				
							CardNumberTxtBox.sendKeys("4012001021000605");
		    }

		    @Then("^user Enter invalid charaters for expiration date and enter valid information for all other fields$")
		    public void user_enter_invalid_charaters_for_expiration_date_and_enter_valid_information_for_all_other_fields() throws Throwable {
		    	
		    
		    	WebElement ExpirationDateTextBox = driver.findElement(By.xpath(MyAccountPage.Add_card_Expiration_Date_TextBox));
		    	
		    	ExpirationDateTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		    	ExpirationDateTextBox.sendKeys(Keys.BACK_SPACE);
		    	ExpirationDateTextBox.sendKeys("INVALIDKEYS");
		    	
		    	String ExpirationDateError= driver.findElement(By.xpath(MyAccountPage.Add_card_Expiration_Error_Message)).getText();
		    	
				Assert.assertTrue("Expiration Error Message is incorrect",
						ExpirationDateError.equals("Expiration Date is required"));
							log.info("User verifys Expiration Error Message is correct");
							log.info("Expiration error message: " + ExpirationDateError);
							
							ExpirationDateTextBox.sendKeys("01/28");
		    }

		    @Then("^user Enter invalid characters for Address and enter valid information for all other Fields Click the save button$")
		    public void user_enter_invalid_characters_for_address_and_enter_valid_information_for_all_other_fields_click_the_save_button() throws Throwable {
		       WebElement Address = driver.findElement(By.xpath(MyAccountPage.Add_Card_Adress_TextBox));
		       WebElement City = driver.findElement(By.xpath(MyAccountPage.Add_Card_City_TextBox));
		       Select Country = new Select(driver.findElement(By.xpath(MyAccountPage.Add_Card_Country_Drpdwn)));
		       WebElement ZipCode = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_TextBox));
		       
		       Address.sendKeys("$$!#%");
		       City.sendKeys("Mirmiar");
		       Country.selectByVisibleText("United States of America");
		       
		       Select State = new Select(driver.findElement(By.xpath(MyAccountPage.Add_Card_State_DropDown)));
		       State.selectByVisibleText("Florida");
		       
		       ZipCode.sendKeys("33025");
		       
		       String AddressErrorMessage = driver.findElement(By.xpath(MyAccountPage.Add_Card_Adress_Error_message)).getText();
		       
		    	WebElement SaveChangesBtn = driver.findElement(By.xpath(MyAccountPage.Add_Card_Save_changes_Btn));
		    	SaveChangesBtn.click();
		    	log.info("User Clicks on Save Changes Button");
		    	
				Assert.assertTrue("Address Error Message is incorrect",
						AddressErrorMessage.equals("Address has an invalid format"));
							log.info("User verifys Address Error Message is correct");
							log.info("Address error message: " + AddressErrorMessage);
		    	
							Address.clear();
							Address.sendKeys("2800 executive way");
							log.info("User Enters a valid Address");
							
		    }

		    @Then("^user Enter invalid characters for City and enter valid information for all other fields$")
		    public void user_enter_invalid_characters_for_city_and_enter_valid_information_for_all_other_fields() throws Throwable {
		    	
		    	WebElement City = driver.findElement(By.xpath(MyAccountPage.Add_Card_City_TextBox));
		    	City.clear();
		    	City.sendKeys("@@Miri@r");
		    	
		    	String CityErrorMessage = driver.findElement(By.xpath(MyAccountPage.Add_Card_City_ErrorMessage)).getText();
		    	
				Assert.assertTrue("City Error Message is incorrect",
						CityErrorMessage.equals("City has invalid format"));
							log.info("User verifys City Error Message is correct");
							log.info("City error message: " + CityErrorMessage);
		    	
							City.clear();
							City.sendKeys("Mirimar");
							log.info("User Enters a valid City");
		    }

		    @Then("^user Enter invalid characters for ZipPostal code and enter valid information for all other fields$")
		    public void user_enter_invalid_characters_for_zippostal_code_and_enter_valid_information_for_all_other_fields() throws Throwable {
		    	WebElement ZipCode = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_TextBox));
		
		    	ZipCode.clear();
		    	ZipCode.sendKeys("!03ll4!");
		    	
		    	String ZipCodeErrorMessage = driver.findElement(By.xpath(MyAccountPage.Add_Card_ZipPostalCode_ErrorMessage)).getText();
		    	
				Assert.assertTrue("ZipCode Error Message is incorrect",
						ZipCodeErrorMessage.equals("Zip/Postal Code should contain only numbers and be between 5 and 10 characters long"));
							log.info("User verifys ZipCode Error Message is correct");
							log.info("ZipCode error message: " + ZipCodeErrorMessage);
		    	
							ZipCode.clear();
							ZipCode.sendKeys("33025");
							log.info("User Enters a valid ZipCode");
		    }

		    @Then("^user Do not enter any information for State and country and enter valid information for all other fields$")
		    public void user_do_not_enter_any_information_for_state_and_country_and_enter_valid_information_for_all_other_fields() throws Throwable {
		    	Select Country = new Select(driver.findElement(By.xpath(MyAccountPage.Add_Card_Country_Drpdwn)));
		    	
			       Select State = new Select(driver.findElement(By.xpath(MyAccountPage.Add_Card_State_DropDown)));
			       State.selectByVisibleText("State");
			       
			       String StateErrorMessage = driver.findElement(By.xpath(MyAccountPage.Add_Card_State_ErrorMessage)).getText();
			       
					Assert.assertTrue("State Error Message is incorrect",
							StateErrorMessage.equals("State is required"));
								log.info("User verifys State Error Message is correct");
								log.info("State error message: " + StateErrorMessage);
								
								Country.selectByVisibleText("Country");
								
								String CountryErrorMessage = driver.findElement(By.xpath(MyAccountPage.Add_Card_Country_Drpdwn_error_message)).getText();
								
								Assert.assertTrue("Country Error Message is incorrect",
										CountryErrorMessage.equals("Country is required"));
											log.info("User verifys Country Error Message is correct");
											log.info("Country error message: " + CountryErrorMessage);
			       
		    }
		    
		    @Then("^user click the Email Subscriptions link$")
		    public void user_click_the_email_subscriptions_link() throws Throwable {
		        Thread.sleep(2000);
		    	WebElement EmailSubscriptionBtn = driver.findElement(By.xpath(MyAccountPage.MyAccount_Email_Subscriptions_link));
		    	EmailSubscriptionBtn.click();
		    	log.info("user clicks the Email Subscriptions link");
		    }

		    @Then("^user enters Email on Your Fs Account page$")
		    public void user_enters_email_on_your_fs_account_page() throws Throwable {
		    	WebElement EmailAddressTxtBox = driver.findElement(By.xpath(MyAccountPage.Email_Deals_EmailAdress_txtbox));
		    	EmailAddressTxtBox.sendKeys(NineFC_valid_emailaddress);
		    	
		    	log.info("user enters Email");
		    }

		    @Then("^user clicks continue on email notification signin page$")
		    public void user_clicks_continue_on_email_notification_signin_page() throws Throwable {
		    	WebElement ContinueBtn = driver.findElement(By.xpath(MyAccountPage.Email_Deals_Continue_btn));
		    	ContinueBtn.click();
		    	log.info("user clicks continue");
		    }

		    @Then("^user clicks the checkbox Unsubscribe from Email Deals$")
		    public void user_clicks_the_checkbox_unsubscribe_from_email_deals() throws Throwable {
		    	Thread.sleep(2000);
		    	WebElement  UnsubscribeCheckbox = driver.findElement(By.xpath(MyAccountPage. Unsubscribe_Checkbox));
		    	UnsubscribeCheckbox.click();
		    	log.info("user clicks on unsubscribe checkbox");
		    }

		    @Then("^user clikcs Submit$")
		    public void user_clikcs_submit() throws Throwable {
		     	WebElement  SubmitButton = driver.findElement(By.xpath(MyAccountPage. SubmitBtn));
		     	SubmitButton.click();
		    	log.info("user clicks on Submit button");
		    }
		    
		    @Then("^user verify Email deals popup$")
		    public void user_verify_email_deals_popup() throws Throwable {
		    	
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				
				Thread.sleep(2000);
		        String EmailDealsPopUP = driver.findElement(By.xpath(MyAccountPage.Email_Deals_Popup_header)).getText();
		        
				Assert.assertTrue("Email Popup not appeared",
						EmailDealsPopUP.equals("EMAIL DEALS"));
						log.info("User verify Email Popup appeared");
				
		    }
		    
		    @Then("^user verify member is already subsribed$")
		    public void user_verify_member_is_already_subsribed() throws Throwable {
		    	
		    	Thread.sleep(2000);
		    	
		    	String MemberName = driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).getText();
		    	WebElement  FirstNameTextBox = driver.findElement(By.xpath(MyAccountPage. Email_Deals_FirstName_txtbox));
		    	WebElement  SubmitButton = driver.findElement(By.xpath(MyAccountPage. SubmitBtn));
		    	
				Assert.assertTrue("First Name did not autopopulate",
						FirstNameTextBox.getAttribute("value").equals(MemberName));
						log.info("User verify name autopopulated");
						
						Assert.assertTrue("Display button is not displayed",
								SubmitButton.isDisplayed());
								log.info("User verify Display button is displayed");
		    }

		    @Then("^user verify Enrollment at the bottom$")
		    public void user_verify_enrollment_at_the_bottom() throws Throwable {
		    	
		     WebElement EnrollmentSection = driver.findElement(By.xpath(MyAccountPage.EnrollmentSection));
		     
				Assert.assertTrue("Enrollment section is missing",
						EnrollmentSection.isDisplayed());
						log.info("User verify Enrollment section");
		     
		    }

		    @Then("^user verify Enrollment at the bottom is suppressed$")
		    public void user_verify_enrollment_at_the_bottom_is_suppressed() throws Throwable {
		        try {
		        	driver.findElement(By.xpath(MyAccountPage.EnrollmentSection)).isDisplayed();
			        } catch (Exception e) {
			            log.info("user Verify EnrollmentSection is suprresed " + e);
			        }
		    }
		    	@Then("^user login as nine DFC member using enter key$")
		    	public void user_login_as_nine_DFC_member_using_enter_key() throws Throwable {

		    		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.free_spirit_email)));

		    		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys(NineFC_valid_emailaddress);

		    		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		    		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).sendKeys(Keys.RETURN);

		    		log.info("user successfully loged in as a nine_DFC member");

		    	}

	
	
	
	
	
	
	
	
}
