package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookPage;
import com.cucumber.pages.CheckInPage;
import com.cucumber.pages.FlightAvailability;
import com.cucumber.pages.NineFC;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.PassengerinfoPage;
import com.cucumber.pages.SeatsPage;
import com.cucumber.pages.SeatsPage320;
import com.cucumber.pages.VacationPage;

import cucumber.api.java.en.Then;

public class FlightAvailabilityStep {

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final Logger log = LogManager.getLogger();

	@Then("^user clicks on the Nonstop button and switch to the popup validate the flight details$")
	public void user_clicks_on_the_nonstop_button_and_switch_to_the_popup_validate_the_flight_details()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(FlightAvailability.Nonstop_button)));
		driver.findElement(By.xpath(FlightAvailability.Nonstop_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(FlightAvailability.Flight_information_popup)).getText();
		log.info("user succesfully validate the text on the popup");
		driver.findElement(By.xpath(FlightAvailability.Flight_information_popup)).getText();
		log.info("user succesfully validate the text on the popup");
		String s = driver.findElement(By.xpath(FlightAvailability.Flight_Number_popup)).getText();
		log.info("flight number is " + s);
		String s5 = driver.findElement(By.xpath(FlightAvailability.Airbus_number_flight_one)).getText();
		log.info("Air bus number is " + s5);
		String s2 = driver.findElement(By.xpath(FlightAvailability.Flight_actual_time)).getText();
		log.info("Actual time is  " + s2);
		String s3 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time)).getText();
		log.info("Flight ON_Time is " + s3);
		String s4 = driver.findElement(By.xpath(FlightAvailability.Flight_late)).getText();
		log.info("Flight Late time is " + s4);
		driver.findElement(By.xpath(FlightAvailability.flight_info_close_button)).click();
		log.info("user succesfully clicks on the close button on the popup");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user clicks on the onestop button and switch to the popup validate the flight details$")
	public void user_clicks_on_the_onestop_button_and_switch_to_the_popup_validate_the_flight_details()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(FlightAvailability.One_stop))));
		driver.findElement(By.xpath(FlightAvailability.One_stop)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(FlightAvailability.Flight_information_popup)).getText();
		log.info("user succesfully validate the text on the popup");
		String s = driver.findElement(By.xpath(FlightAvailability.Flight_Number_popup)).getText();
		log.info("flight number is " + s);
		String s5 = driver.findElement(By.xpath(FlightAvailability.Airbus_number_flight_one)).getText();
		log.info("Air bus number is " + s5);
		String s2 = driver.findElement(By.xpath(FlightAvailability.Flight_actual_time)).getText();
		log.info("Actual time is  " + s2);
		String s3 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time)).getText();
		log.info("Flight ON_Time is " + s3);
		String s4 = driver.findElement(By.xpath(FlightAvailability.Flight_late)).getText();
		log.info("Flight Late time is " + s4);
		String t = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_onestop_flight_two)).getText();
		log.info("flight number for flight two is" + t);
		String t4 = driver.findElement(By.xpath(FlightAvailability.Airbus_Number_one_stop_flight_two)).getText();
		log.info("Air bus number is " + t4);
		String t1 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_one_stop_flight_two)).getText();
		log.info("flight two actual time is " + t1);
		String t2 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time_one_stop_flight_two)).getText();
		log.info("Flight On time is" + t2);
		String t3 = driver.findElement(By.xpath(FlightAvailability.flight_late_one_stop_flight_two)).getText();
		log.info("Flight late time is " + t3);
		driver.findElement(By.xpath(FlightAvailability.flight_info_close_button)).click();
		log.info("user succesfully clicks on the close button on the popup");
		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the Twostop button and switch to the popup validate the flight details$")
	public void user_clicks_on_the_twostop_button_and_switch_to_the_popup_validate_the_flight_details()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(FlightAvailability.two_stops))));
		driver.findElement(By.xpath(FlightAvailability.two_stops)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(FlightAvailability.Flight_information_popup)).getText();
		log.info("user succesfully validate the text on the popup");
		String s = driver.findElement(By.xpath(FlightAvailability.Flight_Number_popup)).getText();
		log.info("flight number is " + s);
		String s5 = driver.findElement(By.xpath(FlightAvailability.Airbus_number_flight_one)).getText();
		log.info("Air bus number is " + s5);
		String s2 = driver.findElement(By.xpath(FlightAvailability.Flight_actual_time)).getText();
		log.info("Actual time is  " + s2);
		String s3 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time)).getText();
		log.info("Flight ON_Time is " + s3);
		String s4 = driver.findElement(By.xpath(FlightAvailability.Flight_late)).getText();
		log.info("Flight Late time is " + s4);
		String t = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_onestop_flight_two)).getText();
		log.info("flight number for flight two is" + t);
		String t4 = driver.findElement(By.xpath(FlightAvailability.Airbus_Number_one_stop_flight_two)).getText();
		log.info("Air bus number is " + t4);
		String t1 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_one_stop_flight_two)).getText();
		log.info("flight two actual time is " + t1);
		String t2 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time_one_stop_flight_two)).getText();
		log.info("Flight On time is" + t2);
		String t3 = driver.findElement(By.xpath(FlightAvailability.flight_late_one_stop_flight_two)).getText();
		log.info("Flight late time is " + t3);

		String p = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_two_stop_flight_three)).getText();
		log.info("flight number for flight two is" + p);
		String p1 = driver.findElement(By.xpath(FlightAvailability.Airbus_Numner_two_stop_flight_three)).getText();
		log.info("Air bus number is " + p1);
		String p2 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_two_stop_flight_three)).getText();
		log.info("flight two actual time is " + p2);
		String p3 = driver.findElement(By.xpath(FlightAvailability.flight_on_time_two_stop_flight_three)).getText();
		log.info("Flight On time is" + p3);
		String p4 = driver.findElement(By.xpath(FlightAvailability.flight_late_two_stop_flight_three)).getText();
		log.info("Flight late time is " + p4);
		driver.findElement(By.xpath(FlightAvailability.flight_info_close_button)).click();
		log.info("user succesfully clicks on the close button on the popup");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the Threestop button and switch to the popup validate the flight details$")
	public void user_clicks_on_the_threestop_button_and_switch_to_the_popup_validate_the_flight_details()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(FlightAvailability.Three_stops))));
		driver.findElement(By.xpath(FlightAvailability.Three_stops)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.xpath(FlightAvailability.Flight_information_popup)).getText();
		log.info("user succesfully validate the text on the popup");
		String s = driver.findElement(By.xpath(FlightAvailability.Flight_Number_popup)).getText();
		log.info("flight number is " + s);
		String s5 = driver.findElement(By.xpath(FlightAvailability.Airbus_number_flight_one)).getText();
		log.info("Air bus number is " + s5);
		String s2 = driver.findElement(By.xpath(FlightAvailability.Flight_actual_time)).getText();
		log.info("Actual time is  " + s2);
		String s3 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time)).getText();
		log.info("Flight ON_Time is " + s3);
		String s4 = driver.findElement(By.xpath(FlightAvailability.Flight_late)).getText();
		log.info("Flight Late time is " + s4);
		String t = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_onestop_flight_two)).getText();
		log.info("flight number for flight two is" + t);
		String t4 = driver.findElement(By.xpath(FlightAvailability.Airbus_Number_one_stop_flight_two)).getText();
		log.info("Air bus number is " + t4);
		String t1 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_one_stop_flight_two)).getText();
		log.info("flight two actual time is " + t1);
		String t2 = driver.findElement(By.xpath(FlightAvailability.flight_ON_Time_one_stop_flight_two)).getText();
		log.info("Flight On time is" + t2);
		String t3 = driver.findElement(By.xpath(FlightAvailability.flight_late_one_stop_flight_two)).getText();
		log.info("Flight late time is " + t3);

		String p = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_two_stop_flight_three)).getText();
		log.info("flight number for flight two is" + p);
		String p1 = driver.findElement(By.xpath(FlightAvailability.Airbus_Numner_two_stop_flight_three)).getText();
		log.info("Air bus number is " + p1);
		String p2 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_two_stop_flight_three)).getText();
		log.info("flight two actual time is " + p2);
		String p3 = driver.findElement(By.xpath(FlightAvailability.flight_on_time_two_stop_flight_three)).getText();
		log.info("Flight On time is" + p3);
		String p4 = driver.findElement(By.xpath(FlightAvailability.flight_late_two_stop_flight_three)).getText();
		log.info("Flight late time is " + p4);

		String q = driver.findElement(By.xpath(FlightAvailability.Flight_number_popup_three_stop_flight_four))
				.getText();
		log.info("flight number for flight two is" + q);
		String q1 = driver.findElement(By.xpath(FlightAvailability.Airbus_Numner_three_stop_flight_four)).getText();
		log.info("Air bus number is " + q1);
		String q2 = driver.findElement(By.xpath(FlightAvailability.flight_actual_time_three_stop_flight_four))
				.getText();
		log.info("flight two actual time is " + q2);
		String q3 = driver.findElement(By.xpath(FlightAvailability.flight_on_time_three_stop_flight_four)).getText();
		log.info("Flight On time is" + q3);
		String q4 = driver.findElement(By.xpath(FlightAvailability.flight_late_three_stop_flight_four)).getText();
		log.info("Flight late time is " + q4);

		driver.findElement(By.xpath(FlightAvailability.flight_info_close_button)).click();
		log.info("user succesfully clicks on the close button on the popup");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user clicks on the New Search button$")
	public void user_clicks_on_the_new_search_button() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availanbility_New_search_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availanbility_New_search_button)).click();
		log.info("user succesfully clicks on the New search button");
	}

	@Then("^user clicks on the advanced search button below the search button$")
	public void user_clicks_on_the_advanced_search_button_below_the_search_button() throws Throwable {

	}

	@Then("^user verifies the Radio button options for the booking$")
	public void user_verifies_the_radio_button_options_for_the_booking() throws Throwable {

		driver.findElement(By.xpath(FlightAvailability.flight_availability_Roundtrip_radio_button)).isDisplayed();
		log.info("user succesfully validates the Roundtrip radio button");
		driver.findElement(By.xpath(FlightAvailability.flight_availability_onewaytrip_radio_button)).isDisplayed();
		log.info("user succesfully validates the onewaytrip radio button");
		driver.findElement(By.xpath(FlightAvailability.flight_availability_Multicity_radio_button)).isDisplayed();
		log.info("user succesfully validates the Multicity radio button");
	}

	@Then("^user selects the one way type of the booking$")
	public void user_selects_the_one_way_type_of_the_booking() throws Throwable {

		driver.findElement(By.xpath(FlightAvailability.flight_availability_onewaytrip_radio_button)).click();
		log.info("user succesfully clciks on the onewaytrip radio button");

	}

	@Then("^user validate the dollar and miles are present$")
	public void user_validate_the_dollar_and_miles_are_present() throws Throwable {

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_dollars)).isDisplayed();
		log.info("user succesfully validates the dollars tab on the flight avilability page ");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_Miles)).isDisplayed();
		log.info("user succesfully validates the Miles tab on the flight avilability page ");

	}

	@Then("^user clicks on the from down arrow and select the city$")
	public void user_clicks_on_the_from_down_arrow_and_select_the_city() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_select_from_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_from_city)).click();
		log.info("user succesfully selects the  from city ");
	}

	@Then("^user clicks on the To down arrow and select the city$")
	public void user_clicks_on_the_to_down_arrow_and_select_the_city() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_select_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_To_city)).click();
		log.info("user succesfully selects the  To city ");

	}

	@Then("^user select the date by clicking the down arrow$")
	public void user_select_the_date_by_clicking_the_down_arrow() throws Throwable {

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date
		c.add(Calendar.YEAR, 0);
		c.add(Calendar.MONTH, 4);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.HOUR, 0);
		c.add(Calendar.MINUTE, 0);
		c.add(Calendar.SECOND, 0);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		String DepartureDate = dateFormat.format(currentDatePlusOne);

		log.info("Departure date is : " + dateFormat.format(currentDatePlusOne));
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(FlightAvailability.Flight_availability_select_date)));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_date)).click();

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_date)).clear();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_date)).sendKeys(DepartureDate);

	}

	@Then("^user selects the number of passengers By clikcing on the drop down$")
	public void user_selects_the_number_of_passengers_by_clikcing_on_the_drop_down() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();

	}

	@Then("^user verify the promo code link is present$")
	public void user_verify_the_promo_code_link_is_present() throws Throwable {

		String s = driver.findElement(By.xpath(FlightAvailability.Flight_availability_promo_code)).getAttribute("href");
		log.info("promocode link is" + s);
	}

	@Then("^user Mouse over on the tool tip and validate the text$")
	public void user_mouse_over_on_the_tool_tip_and_validate_the_text() throws Throwable {

		String expectedTooltip = "You'll find our Promo Codes in our promotional emails.";

		WebElement question_mark = driver.findElement(By.xpath(FlightAvailability.Flight_availability_tip_tool));
//		Actions builder = new Actions(driver);
//		builder.clickAndHold().moveToElement(question_mark);
//		builder.moveToElement(question_mark).build().perform();
		question_mark.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(OnewaytripPage.mouse_over_popup_text))));
		WebElement toolTipElement = driver.findElement(By.xpath(OnewaytripPage.mouse_over_popup_text));
		String actualTooltip = toolTipElement.getText();
		log.info(actualTooltip);

	}

	@Then("^user clicks on the search flight button$")
	public void user_clicks_on_the_search_flight_button() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();
		log.info("user succesfully clicks on the search flight button");
	}

	@Then("^user selects the one adult and seven child passengers$")
	public void user_selects_the_one_adult_and_seven_child_passengers() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_child_input_text))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).click();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).clear();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).sendKeys("7");

	}

	@Then("^user clicks on the serach flight switch to the popup and enters the children date of birth$")
	public void user_clicks_on_the_serach_flight_switch_to_the_popup_and_enters_the_children_date_of_birth()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  first child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_two_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  2nd child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_two_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_three_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  3rd child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_three_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_four_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  4th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_four_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_five_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  5th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_five_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_six_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  6th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_six_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_seven_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  7th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_seven_date_of_birth))
				.sendKeys(Keys.TAB);

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user succesfully clicks on the child popup continue button");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user selects the one adlut and one child passenger$")
	public void user_selects_the_one_adlut_and_one_child_passenger() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_clicks_child_Plus_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_clicks_child_Plus_button)).click();

	}

	@Then("^user clicks on the serach flight switch to the popup and enters the child date of birth and click on continue button$")
	public void user_clicks_on_the_serach_flight_switch_to_the_popup_and_enters_the_child_date_of_birth_and_clicks_on_continue_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {

		}

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth))
				.sendKeys("12/12/2017");
		log.info("user succesfully enetrs the  first child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user succesfully clicks on the child popup continue button");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user selects the one UMNR passneger$")
	public void user_selects_the_one_umnr_passneger() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button)).click();
		log.info("user succesfully clicks on the adult passenger minus button");
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_clicks_child_Plus_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_clicks_child_Plus_button)).click();
		log.info("user succesfully clicks on the child passenger plus button");

	}

	@Then("^user switch to the popup and enters the date of birth of the child and clicks on the continue button and again switch to the UMNR popup and clicks on accept$")
	public void user_switch_to_the_popup_and_enters_the_date_of_birth_of_the_child_and_clicks_on_the_continue_button_and_again_switch_to_the_umnr_popup_and_clicks_on_accept()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {

		}
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  first child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		for (String winHandle2 : driver.getWindowHandles()) {

		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button)).click();
		log.info("user succesfully clicks on the accept buuton on unaccompained minor ");

		driver.switchTo().window(parentHandle);
	}

	@Then("^user selects the seven child passengers$")
	public void user_selects_the_seven_child_passengers() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_child_input_text))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).click();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).clear();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_input_text)).sendKeys("7");
		log.info("user succesfully selects the seven child passengers ");

	}

	@Then("^user clicks on the serach flight switch to the popup and enters the children date of birth and agian switch to the unaccompained minor popup and clicks on accept button$")
	public void user_clicks_on_the_serach_flight_switch_to_the_popup_and_enters_the_children_date_of_birth_and_agian_switch_to_the_unaccompained_minor_popup_and_clicks_on_accept_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  first child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_one_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_two_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  2nd child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_two_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_three_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  3rd child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_three_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_four_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  4th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_four_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_five_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  5th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_five_date_of_birth))
				.sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_six_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  6th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_six_date_of_birth)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_seven_date_of_birth))
				.sendKeys("12/12/2006");
		log.info("user succesfully enetrs the  7th child date of birth");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_seven_date_of_birth))
				.sendKeys(Keys.TAB);

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_child_popup_continue_button)).click();
		log.info("user succesfully clicks on the child popup continue button");

		for (String winHandle2 : driver.getWindowHandles()) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button))));
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_unaccompanied_minor_accept_button)).click();
		log.info("user succesfully clicks on the accept buuton on unaccompained minor ");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user Mouse over on the tool tip and clicks on the signup now button and navigate to the and navigate to the account enrolment window and switch back to the flight availability page$")
	public void user_mouse_over_on_the_tool_tip_and_clicks_on_the_signup_now_button_and_navigate_to_the_and_navigate_to_the_account_enrolment_window_and_switch_back_to_the_flight_availability_page()
			throws Throwable {

		String expectedTooltip = "You'll find our Promo Codes in our promotional emails.";

		WebElement question_mark = driver.findElement(By.xpath(FlightAvailability.Flight_availability_tip_tool));
//		Actions builder = new Actions(driver);
//		builder.clickAndHold().moveToElement(question_mark);
//		builder.moveToElement(question_mark).build().perform();
		question_mark.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_Tool_tip_signup_now_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_Tool_tip_signup_now_button)).click();
		log.info("user succesfully clicks on the signup now button on tool tip");
		String s = driver.getCurrentUrl();
		log.info("The current url is " + s);
		driver.navigate().back();
		log.info("user succesfully navigate back to the flight availability page");
	}

	@Then("^user clicks on the add promocode link in flight availability page$")
	public void user_clicks_on_the_add_promocode_link_in_flight_availability_page() throws Throwable {

	}

	@Then("^user clicks on the passengers and selects zero passenger$")
	public void user_clicks_on_the_passengers_and_selects_zero_passenger() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_click_passengers))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_click_passengers)).click();
		log.info("user succesfully clicks on the passenger button ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_clicks_adlult_minus_button)).click();
		log.info("user succesfully clicks on the adult minus button");
	}

	@Then("^user clicks on the search flight and switch to the popup and close the popup window$")
	public void user_clicks_on_the_search_flight_and_switch_to_the_popup_and_close_the_popup_window() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_search_flight_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_passenger_popup_button))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_passenger_popup_button)).click();
		log.info("user succesfully switch to the popup and clicks on the close window button");
		driver.switchTo().window(parentHandle);
	}

	@Then("^user clicks on the From city and choose North America region and select the Departure city$")
	public void user_clicks_on_the_from_city_and_choose_north_america_region_and_select_the_departure_city()
			throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_North_America_tab))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_North_America_tab)).click();
		log.info("user succesfully clicks on the North America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_from_city)).click();
		log.info("user succesfully selects the departure city ");
	}

	@Then("^user clicks on the To city and chosse North America region and select the arrival city$")
	public void user_clicks_on_the_to_city_and_chosse_north_america_region_and_select_the_arrival_city()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_North_America_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_North_America_To_city)).click();
		log.info("user succesfully clicks on the North America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_To_city)).click();
		log.info("user succesfully selects the arrival city ");
	}

	@Then("^user clicks on the From city and choose Central America region and select the Departure city$")
	public void user_clicks_on_the_from_city_and_choose_central_america_region_and_select_the_departure_city()
			throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_central_America_From_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_central_America_From_city)).click();
		log.info("user succesfully clicks on the central America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_cental_America_select_From_city))
				.click();
		log.info("user succesfully clicks on central America tab and selects the departure city ");
	}

	@Then("^user clicks on the To city and chosse Central America region and select the arrival city$")
	public void user_clicks_on_the_to_city_and_chosse_central_america_region_and_select_the_arrival_city()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_central_America_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_central_America_To_city)).click();
		log.info("user succesfully clicks on the central America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_central_America_select_To_city))
				.click();
		log.info("user succesfully selects the arrival city ");
	}

	@Then("^user clicks on the From city and choose South America region and select the Departure city$")
	public void user_clicks_on_the_from_city_and_choose_south_america_region_and_select_the_departure_city()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_south_America_From_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_south_America_From_city)).click();
		log.info("user succesfully clicks on the North America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_south_America_select_From_city))
				.click();
		log.info("user succesfully clicks on central America tab and selects the departure city ");

	}

	@Then("^user clicks on the To city and chosse South America region and select the arrival city$")
	public void user_clicks_on_the_to_city_and_chosse_south_america_region_and_select_the_arrival_city()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_south_America_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_south_America_To_city)).click();
		log.info("user succesfully clicks on the central America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_south_America_select_To_city))
				.click();
		log.info("user succesfully selects the arrival city ");

	}

	@Then("^user clicks on the From city and choose caribbean region and select the Departure city$")
	public void user_clicks_on_the_from_city_and_choose_caribbean_region_and_select_the_departure_city()
			throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_caribbean_From_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_caribbean_From_city)).click();
		log.info("user succesfully clicks on the North America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_caribbean_select_From_city)).click();
		log.info("user succesfully clicks on central America tab and selects the departure city ");

	}

	@Then("^user clicks on the To city and chosse caribbean region and select the arrival city$")
	public void user_clicks_on_the_to_city_and_chosse_caribbean_region_and_select_the_arrival_city() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_caribbean_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_caribbean_To_city)).click();
		log.info("user succesfully clicks on the central America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_caribbean_select_To_city)).click();
		log.info("user succesfully selects the arrival city ");

	}

	@Then("^user clicks on the From city text field$")
	public void user_clicks_on_the_from_city_text_field() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		log.info("user succesfully clicks on the from city text field");
	}

	@Then("^user send the keys to the from city input box and select the departure city$")
	public void user_send_the_keys_to_the_from_city_input_box_and_select_the_departure_city() throws Throwable {
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).clear();
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).sendKeys("LGA");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_from_city)).sendKeys(Keys.TAB);
		log.info("user succesfully send the text and select the departure city");

	}

	@Then("^user clicks on the TO city text field$")
	public void user_clicks_on_the_to_city_text_field() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		log.info("user succesfully clicks on the To city text field");

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_select_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_To_city)).sendKeys("san");
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_To_city)).sendKeys(Keys.TAB);
		log.info("user succesfully send the text and select the arrival city");
	}

	@Then("^user selects the Domestic city as a from city$")
	public void user_selects_the_domestic_city_as_a_from_city() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_select_from_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_from_city)).click();
		log.info("user succesfully selects the domestic city ");
	}

	@Then("^user selects international city as a To city$")
	public void user_selects_international_city_as_a_to_city() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_central_America_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_central_America_To_city)).click();
		log.info("user succesfully clicks on the central America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_central_America_select_To_city))
				.click();
		log.info("user succesfully selects the  international arrival city ");
	}

	@Then("^user clicks on the search flight button and switch to the popup and get the text on that popup$")
	public void user_clicks_on_the_search_flight_button_and_switch_to_the_popup_and_get_the_text_on_that_popup()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_search_flight_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			String s = driver
					.findElement(By.xpath(FlightAvailability.Flight_availability_travel_message_over_the_field))
					.getText();
			log.info("travel message is " + s);

			driver.findElement(By.xpath(FlightAvailability.Flight_availability_travel_message_over_the_field_Ok_button))
					.click();
			log.info("user succesfully clicks on the ok button on the travel message popup");
			driver.switchTo().window(parentHandle);

		}

	}

	@Then("^user selects the international city as a from city$")
	public void user_selects_the_international_city_as_a_from_city() throws Throwable {

		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_From_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_availability_region_south_America_From_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_region_south_America_From_city)).click();
		log.info("user succesfully clicks on the North America Region");

		driver.findElement(By.xpath(FlightAvailability.Flight_Availability_region_south_America_select_From_city))
				.click();
		log.info("user succesfully clicks on central America tab and selects the departure city ");

	}

	@Then("^user selects the domestic city as a To city$")
	public void user_selects_the_domestic_city_as_a_to_city() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.flight_availability_TO_city_click))));
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.xpath(FlightAvailability.Flight_availability_select_To_city))));
		driver.findElement(By.xpath(FlightAvailability.Flight_availability_select_To_city)).click();
		log.info("user succesfully selects the  To city ");
	}

	@Then("^user clciks on the continue button and switch to the popup and selects the standard barefare$")
	public void user_clciks_on_the_continue_button_and_switch_to_the_popup_and_selects_the_standard_barefare()
			throws Throwable {
		String parentHandle = driver.getWindowHandle();
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_continue_bare_fare))));
		driver.findElement(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_continue_bare_fare)).click();
		log.info("user succesfully selects the continue with the Bare fare on the treat your self popup");

		driver.switchTo().window(parentHandle);
	}

	@Then("^user clciks on the continue button and switch to the popup and selects the choose bundle$")
	public void user_clciks_on_the_continue_button_and_switch_to_the_popup_and_selects_the_choose_bundle()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_choose_bundle))));
		driver.findElement(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_choose_bundle)).click();
		log.info("user succesfully selects the continue with the choose bundle on the treat your self popup");

		driver.switchTo().window(parentHandle);
	}

	@Then("^user clciks on the continue button and switch to the popup and selects the Thrills combo$")
	public void user_clciks_on_the_continue_button_and_switch_to_the_popup_and_selects_the_thrills_combo()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		WebElement ele = driver.findElement(By.xpath(OnewaytripPage.Continue_Button));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_Thrills_combo))));
		driver.findElement(By.xpath(FlightAvailability.Flight_avialbility_treat_yourself_Thrills_combo)).click();
		log.info("user succesfully selects the continue with the Thrills combo on the treat your self popup");

		driver.switchTo().window(parentHandle);
	}

	@Then("^user validates the New Search button is displayed$")
	public void user_validates_the_new_search_button_is_displayed() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OnewaytripPage.New_search_button)));
		Assert.assertTrue("User is not  navigated to Flight page",
				driver.findElement(By.xpath(OnewaytripPage.New_search_button)).isDisplayed());
		log.info("user succesfully valiadte the New Serach button is displayed ");
	}

	@Then("^user validate the search widget fields on flight availability page$")
	public void user_validate_the_search_widget_fields_on_flight_availability_page() throws Throwable {

		driver.findElement(By.xpath(FlightAvailability.flight_availability_From_city_click)).isDisplayed();
		driver.findElement(By.xpath(FlightAvailability.flight_availability_TO_city_click)).isDisplayed();
		driver.findElement(By.xpath(FlightAvailability.flight_availability_onewaytrip_radio_button)).isDisplayed();
		driver.findElement(By.xpath(FlightAvailability.flight_availability_Roundtrip_radio_button)).isDisplayed();
		log.info(
				"user succesfully validates the flight avilability search widget is expand after clicking on the New Search button ");
	}

	
	@Then("^user enters the personal information for Second adult same name as first pax$")
	public void user_enters_the_personal_information_for_second_adult_same_name_as_first_pax() throws Throwable {
		Thread.sleep(5000);
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		String Pax1Fname = driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).getAttribute("value");
		String Pax1Lname = driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).getAttribute("value");
		String Pax1DOB = driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).getAttribute("value");
		log.info(" first Name pax " + Pax1Fname);
		log.info("Last name pax " +  Pax1Lname );
		log.info("pax 1 DOB" +  Pax1DOB);
		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("justin");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys(Pax1Lname);
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys(Pax1DOB);

		log.info("user succesfully enters information for second Pax same name as first pax " + Pax1Fname, Pax1Lname );
	}
	
	
	@Then("^user inputs different DOB for second Pax$")
	public void user_inputs_different_DOB_for_second_Pax() throws Throwable {
	   
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("02/03/1982");
		log.info("user succesfully enters the new date of birth for the secound passenger ");
		
	}
	@Then("^User Selects Battery Powered Drycell Wheelchair$")
	public void User_Selects_Battery_Powered_Drycell_Wheelchair() throws Throwable {
		Select drpWheelChair = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_wheel_chir_dropdown)));
		drpWheelChair.selectByVisibleText("Battery Powered Dry/Gel Cell Battery");
		log.info("Battery Powered Drycell Wheelchair Selected");
	}
	
    @Then("^User accepts UMNR FEE$")
    public void user_accepts_umnr_fee() throws Throwable {
        Thread.sleep(2000);
	driver.findElement(By.xpath(OnewaytripPage.UMNR_Popup_Accept_button)).click();

	log.info("user succesfully accepts UNMR fee");
    }
    
    @Then("^user adds SR and Jr suffix$")
    public void user_adds_sr_and_jr_suffix() throws Throwable {
//    	Select drpAdult1Suffix = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_passeneger_suffix)));
//		drpAdult1Suffix.selectByVisibleText("Sr.");
//		log.info("Sr suffix selected for pax 1");
		
		Select drpAdult2Suffix = new Select(driver.findElement(By.xpath(PassengerinfoPage.adult_passeneger2_suffix)));
		drpAdult2Suffix.selectByVisibleText("Jr.");
		log.info("Jr suffix selected for pax 2");
    }

    @Then("^User Verify that the inline login block is suppressed$")
    public void user_verify_that_the_inline_login_block_is_suppressed() throws Throwable {
    	{
		    try
		    {
		    	driver.findElement(By.xpath(PassengerinfoPage.inline_login_block));
		    	log.info("WARNING!!!!!! inLine Login block is not surpressed");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Inline login block surppresed verified");
		    }
		}
    }

    @Then("^User Verify Inline enrollment is not present$")
    public void user_verify_inline_enrollment_is_not_present() throws Throwable {
    	{
		    try
		    {
		    	driver.findElement(By.xpath(PassengerinfoPage.inline_enrollment_block));
		    	log.info("WARNING!!!!!! inLine enrollment block is not surpressed");
		    }
		    catch (NoSuchElementException e)
		    {
		    	log.info("Inline login block surppresed verified");
		    }
		}
    }
    
	@Then("^user login as nine DFC member with aspostrophe in there name$")
	public void user_login_as_nine_dfc_member_with_aspostrophe_in_there_name() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.free_spirit_email)));

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("OBRIANTEST9dfc@SPIRIT.COM");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a nine_DFC member with aspostrophe in there name");

	}

	
	
    @Then("^user verify First middle and last name autopopulated with apostrophe$")
    public void user_verify_first_middle_and_last_name_autopopulated_with_apostrophe() throws Throwable {
		Thread.sleep(5000);
		Assert.assertTrue("User First name did not auto populate",
				driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_first_name)).getAttribute("value").equals("O'BRIAN"));
		log.info("First Name autopopulated verified with apostrophe");
		
		Assert.assertTrue("User Middle name did not auto populate",
				driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_middle_name)).getAttribute("value").equals("O'CASEY"));
		log.info("Middle Name autopopulated verified with apostrophe");
		
		Assert.assertTrue("User Last name did not auto populate",
				driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_last_name)).getAttribute("value").equals("O'SULLIVAN"));
		log.info("Last Name autopopulated verified with apostrophe");
	}
    
    @Then("^user verify Umnr dob autopopulated$")
    public void user_verify_umnr_dob_autopopulated() throws Throwable {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)));
    	String child_date_of_brth= driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).getAttribute("value");
		Assert.assertTrue("Childs DOB did not auto populate",
				driver.findElement(By.xpath(PassengerinfoPage.adult_passengers_dateof_birth)).getAttribute("value").equals(child_date_of_brth));
		log.info("Childs DOB autopopulated verified");
    }
    
    @Then("^user clicks on search button and enters the birthdate for UMNR who is fifthteen years old$")
    public void user_clicks_on_search_button_and_enters_the_birthdate_for_umnr_who_is_fifthteen_years_old() throws Throwable {

		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.xpath(BookPage.Search_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BookPage.enter_child_date_of_birth)));
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys("08/31/2003");
			driver.findElement(By.xpath(BookPage.enter_child_date_of_birth)).sendKeys(Keys.TAB);
			driver.findElement(By.xpath(BookPage.child_popup_continue_button)).click();

			log.info("user succesfully enter the birthdate of the child");
		}
	}
    
    @Then("^user login as UMNR NineDFC member who is 15 years old$")
    public void user_login_as_umnr_ninedfc_member_who_is_15_years_old() throws Throwable {

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("eric159dfc@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a NINEDFC who is a child and 15 yrs old");
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.passenerspage_Continue_button)));
		// dob: 2/1/2008
	}
    
    @Then("^user login as nine DFC member who has a suffix attached$")
    public void user_login_as_nine_dfc_member_who_has_a_suffix_attached() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(PassengerinfoPage.free_spirit_email)));

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("danflyer9dfc@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a nine_DFC member");

	}
    
	@Then("^User Selects Need Help ToFrom Seat$")
	public void User_Selects_Need_Help_ToFrom_Seat() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.adult_Need_help_to_from_Seat)).click();
		log.info("Need Help ToFrom Seat selected");
	}
	
	@Then("^user enters the adult name same as first adult$")
	public void user_enters_the_adult_name_same_as_first_adult() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.child_title));
		Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
		s.selectByVisibleText("Mr.");

		driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Joe");
		driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Flyer");
		driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("01/01/1981");
		
		log.info("user succesfully enters same name for second Pax");
	}
	
    @Then("^user selects suffix for passenger two$")
    public void user_selects_suffix_for_passenger_two() throws Throwable {
    	Select Suffix = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_passenger_suffix)));
    	Suffix.selectByVisibleText("Jr.");
		 log.info("Jr. Suffix Selected");
    }
    
    @Then("^user verfies all information is correct$")
	public void user_verfies_all_information_is_correct() throws Throwable {
		Thread.sleep(5000);

		Assert.assertTrue("User First Name did not autopopoulate",
				driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).getAttribute("value").equals("Joe"));
		log.info("User First Name autopopulated verified");
		
		Assert.assertTrue("User Last Name did not autopopoulate",
				driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).getAttribute("value").equals("Flyer"));
		log.info("User Last Name autopopulated verified");
		
		Assert.assertTrue("User DOB did not autopopoulate",
				driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).getAttribute("value").equals("01/01/1980"));
		log.info("User DOB autopopulated verified");
    }
    
    @Then("^user goes to Personal Information$")
    public void user_goes_to_myaccount() throws Throwable {
    	
    	Thread.sleep(4000);
    	 driver.findElement(By.xpath(NineFC.nine_dfc_member_button)).click();   
    	 Thread.sleep(2000);
    	 driver.findElement(By.xpath(NineFC.MyAccount_link)).click();   
    	 Thread.sleep(2000);
    	 driver.findElement(By.xpath(NineFC.PersonalInformation_link)).click();  
    }
	
    @Then("^user verfies Name and DOB$")
    public void user_verfies_name_and_dob() throws Throwable {
    	Assert.assertTrue("User Name has changed",
				driver.findElement(By.xpath(NineFC.MemberName_Account_Page)).getText().equals("JUSTIN FLYER"));
		log.info("User Name Verifed");
		
		Assert.assertTrue("DOB date has changed",
				driver.findElement(By.xpath(NineFC.MemberDOB_Account_Page)).getAttribute("value").equals("01/01/1980"));
		log.info("DOB date Verifed");
    }
    
	@Then("^Vacation User choose the from FLL City$")
	public void user_choose_the_from_fll_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.From_city)));
		driver.findElement(By.xpath(VacationPage.From_city)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage.From_FLL_City)));
		driver.findElement(By.xpath(SeatsPage.From_FLL_City)).click();
		log.info("user successfully selected the FLL city");
	}
	
	@Then("^Vacation User choose the To LGA city$")
	public void user_choose_the_to_LGA_city() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VacationPage.To_city)));
		driver.findElement(By.xpath(VacationPage.To_city)).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SeatsPage320.TO_city_LGA)));
        driver.findElement(By.xpath(SeatsPage320.TO_city_LGA)).click();
		log.info("user successfully selected the LGA city");
	}
	
	 @Then("^user selects departure date and return date for Vacation$")
	   public void User_selects_departure_date_and_return_date() throws Throwable{
		   driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).click();
	    	driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).clear();
	    	Calendar c = Calendar.getInstance(); 
	        c.add(Calendar.DATE, 1);
	        Date date1=c.getTime();
	        log.info("Departure date: "+dateFormat.format(date1));
	        String DepartureDate = dateFormat.format(date1);
	        c.setTime(date1);

	        // manipulate date
	        c.add(Calendar.YEAR, 0);
	        c.add(Calendar.MONTH, 0);
	        c.add(Calendar.DATE, 1);  //same with c.add(Calendar.DAY_OF_MONTH, 1);
	        c.add(Calendar.HOUR, 0);
	        c.add(Calendar.MINUTE,0);
	        c.add(Calendar.SECOND, 0);

	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();
	        String ReturnDate = dateFormat.format(currentDatePlusOne);

	        log.info("Return date: "+dateFormat.format(currentDatePlusOne));
	    	driver.findElement(By.xpath(VacationPage.Departure_and_Return_dates)).sendKeys(DepartureDate+" - "+ReturnDate);
	    }
	 
	  @Then("^User verifys Primary driver dropdown$")
	    public void user_verifys_primary_driver_dropdown() throws Throwable {
	    	Thread.sleep(3000);
	
	    if (driver.findElements(By.xpath(CheckInPage.Primary_driver_dropdown)).size() != 0) {
	    	   Assert.fail("Error..Primary driver dropdown missing");    
	    	}
	    else{
	    	log.info("Primay driver dropdown verified");
	    	}
	    }
	  
		@Then("^user enters the adult name and DOB same as first adult$")
		public void user_enters_the_adult_name_and_dob_same_as_first_adult() throws Throwable {
			driver.findElement(By.xpath(PassengerinfoPage.child_title));
			Select s = new Select(driver.findElement(By.xpath(PassengerinfoPage.child_title)));
			s.selectByVisibleText("Mr.");

			driver.findElement(By.xpath(PassengerinfoPage.child_firstname)).sendKeys("Joe");
			driver.findElement(By.xpath(PassengerinfoPage.child_lastname)).sendKeys("Flyer");
			driver.findElement(By.xpath(PassengerinfoPage.child_dateofbirth)).sendKeys("01/01/1981");
			
			log.info("user succesfully enters same name for second Pax");
		}
		
	    @Then("^User verifys Primary driver dropdown does not appear$")
	    public void user_verifys_primary_driver_dropdown_does_not_appear() throws Throwable {
	    	  try
			    {
			    	driver.findElement(By.xpath(CheckInPage.Primary_driver_dropdown));
			    	log.info("Primary driver dropdown appears!!!");
			    }
			    catch (NoSuchElementException e)
			    {
			    	log.info("Primary driver dropdown Doesnt not appear verified");
			    }
	    }
	    
	    @Then("^user verfies all information is correct for 9dfc$")
		public void user_verfies_all_information_is_correct_for_9dfc() throws Throwable {
			Thread.sleep(5000);
//
//			Assert.assertTrue("User First Name did not autopopoulate",
//					driver.findElement(By.xpath(OnewaytripPage.passengers_first_name)).getAttribute("value").equals("justin"));
//			log.info("User First Name autopopulated verified");
//			
//			Assert.assertTrue("User Last Name did not autopopoulate",
//					driver.findElement(By.xpath(OnewaytripPage.passengers_last_name)).getAttribute("value").equals("flyer"));
//			log.info("User Last Name autopopulated verified");
			
			Assert.assertTrue("User DOB did not autopopoulate",
					driver.findElement(By.xpath(OnewaytripPage.passengers_dateof_birth)).getAttribute("value").equals("01/01/1980"));
			log.info("User DOB autopopulated verified");
	    }
}

   
    	
    
	

