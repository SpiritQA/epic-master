package com.cucumber.stepDefinitions;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNoException;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.model.Test;
import com.cucumber.pages.Boardingpass;
import com.cucumber.pages.OnewaytripPage;
import com.cucumber.pages.SeatsPage320;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.en.Then;
import cucumber.deps.com.thoughtworks.xstream.core.util.ArrayIterator;

public class BoardingpassStep {

	
	private static final Logger log = LogManager.getLogger();

	WebDriver driver = MySharedClass.getDriver();
	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 20).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	@Then("^user click on the checkin boarding pass switch to the pro trip popup and clicks on i do not need bags and clicks on get random seats popup$")
    public void user_click_on_the_checkin_boarding_pass_switch_to_the_pro_trip_popup_and_clicks_on_i_do_not_need_bags_and_clicks_on_get_random_seats_popup() throws Throwable {
		String parentHandle = driver.getWindowHandle();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)));
		driver.findElement(By.xpath(SeatsPage320.Checkin_and_Print_Boarding_Pass_Button)).click();
		log.info("user succesfully clicks on the checkin and print boarding pass on the checkin page");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_borading_pass_nope_im_good)));
		driver.findElement(By.xpath(Boardingpass.checkin_borading_pass_nope_im_good)).click();
		log.info("user succesfully clicks on the i don't need bags on the popup");

		for (String winHandle2 : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle2);
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_get_random_seats)));
		driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_get_random_seats)).click();
		log.info("user succesfully clicks on the get random seats");

		driver.switchTo().window(parentHandle);
		
		
	}
	}
	
	    @Then("^user lands on the Extras page$")
	    public void user_lands_on_the_extras_page() throws Throwable {
	   wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_extras)));
	  WebElement s=  driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_extras));	
	  log.info("text is " + s);
	    log.info("user succesfully lands on the extras page");
	    	
	    }

	    @Then("^user clicks on the continue button on the extras page and switch to the popup and select the i dont want insurance and accept and print the boarding pass popup and switch to the boarding pass popup$")
	    public void user_clicks_on_the_continue_button_on_the_extras_page_and_switch_to_the_popup_and_select_the_i_dont_want_insurance_and_accept_and_print_the_boarding_pass_popup_and_switch_to_the_boarding_pass_popup() throws Throwable {
	      
	    	String parentHandle = driver.getWindowHandle(); 
	    	Thread.sleep(3000);
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_Extras_continue_button)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_Extras_continue_button)).click();
	    	log.info("user succesfully clicks on the continue button on the extras page");
	    	for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_travel_gurad_insuarance)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_travel_gurad_insuarance)).click();
	    	log.info("user succesfully clicks on the i don't want insurance");
	    	
	    	for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle2);

			}
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_accept_and_print)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_accept_and_print)).click();
	    	log.info("user succesfully clicks on the accept and print the boarding pass ");
	    	
	    	for (String winHandle3 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle3);

			}
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boardingpass_popup_close)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boardingpass_popup_close)).click();
	    	log.info("user succesfully clicks on the close button on the your boarding pass popup");
	    	
	    	driver.switchTo().window(parentHandle);
	    }
	
	    @Then("^user selects the print and email the boarding pass$")
	    public void user_selects_the_print_and_email_the_boarding_pass() throws Throwable {
	    	  
	    	String parentHandle = driver.getWindowHandle(); 
	    	Thread.sleep(3000);
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_Extras_continue_button)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_Extras_continue_button)).click();
	    	log.info("user succesfully clicks on the continue button on the extras page");
	    	for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_travel_gurad_insuarance)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_travel_gurad_insuarance)).click();
	    	log.info("user succesfully clicks on the i don't want insurance");
	    	
	    	for (String winHandle2 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle2);

			}
	    	 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_accept_and_print)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_accept_and_print)).click();
	    	log.info("user succesfully clicks on the accept and print the boarding pass ");
	    	
	    	for (String winHandle3 : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle3);

			}
	    	wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_popup_print_checkbox)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_popup_print_checkbox)).click();
	    	log.info("user succesfully selects the print checkbox on the boarding pass popup");
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_popup_email_checkbox)).click();
	    	log.info("user succesfully selects the email checkbox on the boarding pass popup");
	    	
	    	wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Boardingpass.checkin_boarding_pass_finish_checkin)));
	    	driver.findElement(By.xpath(Boardingpass.checkin_boarding_pass_finish_checkin)).click();
	    	log.info("user succesfully clicks on the finish checkin button ");
	    	
	    	driver.switchTo().window(parentHandle);
	    }
	    
	    @Then("^user save the generated pdf$")
	    public void user_save_the_generated_pdf() throws Throwable {
	 
	    	  ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    	    driver.switchTo().window(tabs2.get(1));
//	    	    driver.close();
//	    	    driver.switchTo().window(tabs2.get(0));
	    	
	    	
	Thread.sleep(4000);
	    	
	   driver.findElement(By.xpath("//button[@class='destination-settings-change-button']")).click();
	   log.info("user succesfully clicks on the change button");
	   
	   Thread.sleep(2000);
	   driver.findElement(By.xpath("//span[contains(text(),'Save as PDF')]")).click();
	   log.info("user succesfully clicks on the save as a pdf button");
	   
	  
	  	Thread.sleep(2000);
		   driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
		   log.info("user succesfully clicks on the save button");
	      log.debug("needs to be debug this step");
  }
	
	    

	       }
	      
	    
	    
	    

