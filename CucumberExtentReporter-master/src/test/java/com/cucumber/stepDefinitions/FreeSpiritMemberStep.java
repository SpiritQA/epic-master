package com.cucumber.stepDefinitions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.pages.BookingSummaryPage;
import com.cucumber.pages.NineFC;
import com.cucumber.pages.PassengerinfoPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FreeSpiritMemberStep {

	WebDriver driver = MySharedClass.getDriver();
	private static final Logger log = LogManager.getLogger();

	private final FluentWait<WebDriver> wait = new WebDriverWait(driver, 90).pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class, WebDriverException.class);
	
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	

	@When("^user clicks on the sigin button and switch to the popup enters the free spirit memeber and password$")
	public void user_clicks_on_the_sigin_button_and_switch_to_the_popup_enters_the_free_spirit_memeber_and_password()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
		driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		driver.findElement(By.xpath(NineFC.login_account_emailadress)).sendKeys("joe.flyer3@spirit.com");
		log.info("user successfully enters the email address");
		driver.findElement(By.xpath(NineFC.Login_account_password)).sendKeys("Spirit11!");
		log.info("user successfully enters the password");

		driver.switchTo().window(parentHandle);

	}
	
	

	@Then("^user enetres the free spirit member customers email address$")
	public void user_enetres_the_free_spirit_member_customers_email_address() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.subscribe_email_address)));
		driver.findElement(By.xpath(NineFC.subscribe_email_address)).sendKeys("joe.flyer@spirit.com");
		log.info("user succesfully enters the subscriber email address");

	}

	@Then("^user lands on the passengers page and clicks on the reset the password$")
	public void user_lands_on_the_passengers_page_and_clicks_on_the_reset_the_password() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_Reset_Password)));
		driver.findElement(By.xpath(NineFC.FS_member_Reset_Password)).click();
		log.info("user succesfully clics on the reset the password");
	}

	@Then("^user logged in as free spirit member$")
	public void user_logged_in_as_free_spirit_member() throws Throwable {
		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_email)).sendKeys("joe.flyer@spirit.com");

		driver.findElement(By.xpath(PassengerinfoPage.free_spirit_password)).sendKeys("Spirit11!");

		driver.findElement(By.xpath(PassengerinfoPage.login_account_button)).click();

		log.info("user successfully loged in as a free spirit member");

	}

	@When("^user lands on the passenger page and validate the terms and conditions text$")
	public void user_lands_on_the_passenger_page_and_validate_the_passenger_information_text() throws Throwable {

		Assert.assertTrue("User is not  navigated to bags page",
				driver.findElement(By.xpath(NineFC.Terms_condition_text_validation_passenger_page)).getText()
						.equals("Terms & Conditions"));

		log.info("user succesfully validated the text on the passenger information page ");

	}

	@When("^user clicks on the signin button and switch to the popup and clicks on the signup button$")
	public void user_clicks_on_the_signin_button_and_switch_to_the_popup_and_clicks_on_the_signup_button()
			throws Throwable {

		String parentHandle = driver.getWindowHandle();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
		driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).click();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Fs_member_popup_signup)));
		driver.findElement(By.xpath(NineFC.Fs_member_popup_signup)).click();
		log.info("user successfully clicks on the free spirit members signup button");

		driver.switchTo().window(parentHandle);

	}

	@Then("^user scroll down to Account section and enetrs the information and member must have the apostrophein there first middle and last name$")
	public void user_scroll_down_to_account_section_and_enetrs_the_information_and_member_must_have_the_apostrophein_there_first_middle_and_last_name()
			throws Throwable {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");
		log.info("user succesfully select the title");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("Dwanye'");
		log.info("user succesfully enters the firstname");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_middle_name)).sendKeys("x'");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("Jhonson'");
		log.info("user succesfully enters the lastname");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");
		log.info("user succesfully enters the date of birth");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("dwanye.jhonson123@spirit.com");
		log.info("user succesfully enters the email address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address))
				.sendKeys("dwanye.jhonson123@spirit.com");
		log.info("user succesfully enters the confirm email address");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
		log.info("user succesfully enters the password");
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
		log.info("user succesfully enters the confirm password");

	}

	@Then("^user clicks on the Term and conditions check box$")
	public void user_clicks_on_the_term_and_conditions_check_box() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_termsand_conditions)));
		driver.findElement(By.xpath(NineFC.FS_member_termsand_conditions)).click();
		log.info("user succesfully clicks on the terms and conditions check box");

	}

	@Then("^user clicks on the  Free signup button$")
	public void user_clicks_on_the_free_signup_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_termsand_conditions)));
		driver.findElement(By.xpath(NineFC.FS_Member_free_signup_button)).click();
		log.info("user succesfully clicks on the free signup button");
	}

	@Then("^user clicks on the Book now button$")
	public void user_clicks_on_the_book_now_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_BookNow_button)));
		driver.findElement(By.xpath(NineFC.FS_member_BookNow_button)).click();
		log.info("user succesfully clicks on the Book now button");

	}

	@Then("^user clicks on the Request Mileage credit$")
	public void user_clicks_on_the_request_mileage_credit() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Request_Mileage_credit)));
		driver.findElement(By.xpath(NineFC.Request_Mileage_credit)).click();
		log.info("user succesfully clicks on the Request mileage credit link ");
	}

	@Then("^user enters the invalid conformation code \"([^\"]*)\"$")
    public void user_enters_the_invalid_conformation_code_something(String inavalidconformationcode) throws Throwable {
       
    
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.conformation_code_request_mileage_credit)));
		driver.findElement(By.xpath(NineFC.conformation_code_request_mileage_credit)).sendKeys(inavalidconformationcode);
	}

	@Then("^user clicks on the GO button and switch to the error messge and validate it$")
	public void user_clicks_on_the_go_button_and_switch_to_the_error_messge_and_validate_it() throws Throwable {

		driver.findElement(By.xpath(NineFC.Request_mileage_credit_GO_button)).click();
		
	}
	
	private static final DateFormat dateFormat1 = new SimpleDateFormat("MMddyyyyHHSS");
    @Then("^user Creates FS Account$")
    public void user_creates_fs_account() throws Throwable {
      
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.nine_fc_signup_title)));
        Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
        s.selectByVisibleText("Mr.");
        log.info("user succesfully select the title");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("Joe");
        log.info("user succesfully enters the firstname");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_middle_name)).sendKeys("");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("Flyer");
        log.info("user succesfully enters the lastname");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("01/01/1980");
        log.info("user succesfully enters the date of birth");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 0);
        java.util.Date sysdate = c.getTime();
        String emailid = dateFormat1.format(sysdate);
        driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys(emailid+"@spirit.com");
        log.info("user succesfully enters the email address");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys(emailid+"@spirit.com");
        String fsemail=driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).getAttribute("value");
        log.info("user succesfully enters the confirm email address "+fsemail);
        driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
        log.info("user succesfully enters the password");
        driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
        log.info("user succesfully enters the confirm password");

        
    }

    
    @Then("^user clicks close signup close button$")
    public void user_clicks_close_signup_close_button() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.FS_member_termsand_conditions)));
         String parentHandle = driver.getWindowHandle();
        driver.findElement(By.xpath(NineFC.FS_Member_free_signup_button)).click();
        Thread.sleep(3000);
        log.info("user succesfully clicks on the free signup button");

    }
    
    @When("^User click on footer Free Spirit link$")
	public void user_click_on_footer_Free_Spirit_link() throws Throwable {
		driver.findElement(By.xpath(NineFC.FS_member_SignUp_button)).click();
	}

//	@Then("^User verify header verbiage on FS sign up page$")
//	public void user_verify_header_verbiage_on_FS_sign_up_page() throws Throwable {
//		Thread.sleep(5000);
//		int listCounter = 0;
//		ArrayList<String> headerValues = new ArrayList<>(Arrays.asList(	"joinnow_r2.png",      "Join Now",         "Be the first to hear about member-only fares and hot deals.",
//																		"earnmiles_r2.png",    "Earn Miles",       "Rack up miles every time you fly or when using our partners.",
//																		"buygiftmiles_r2.png", "Buy & Gift Miles", "Buy or gift miles for as little as $27.",
//																		"usemiles_r2.png",     "Use Miles",        "Plan ahead to get the most out of your miles.",
//																		"partners_r2.png",     "Partners",         "Be the first to hear about member-only fares and hot deals.",
//																		"creditcards_r2.png",  "Credit Cards",     "Apply today and start planning your trip tomorrow."));
//		
//		
//		List<WebElement> headerSections = driver.findElements(By.xpath(NineFC.FS_member_singup_verbiage));
//		
//		for (WebElement signupHeaders : headerSections) {
//			
//	        Assert.assertTrue("user not verify header image " + headerValues.get(listCounter)+ " on sign up page",
//	        		signupHeaders.findElements(By.tagName("img")).get(0).getAttribute("src").trim().contains(headerValues.get(listCounter)));
//			log.info("user verify header image " + headerValues.get(listCounter) + " on sign up page");
//			listCounter = listCounter + 1;
//			
//	        Assert.assertTrue("user not verify header button " + headerValues.get(listCounter)+ " on sign up page",
//	        		signupHeaders.findElements(By.tagName("a")).get(0).getText().trim().equalsIgnoreCase(headerValues.get(listCounter)));
//	        log.info("user verify header button " + headerValues.get(listCounter) + " on sign up page");
//	        listCounter = listCounter + 1;
//	        
//	        Assert.assertTrue("user not verify header verbiage " + headerValues.get(listCounter)+ " on sign up page",
//	        		signupHeaders.findElements(By.tagName("p")).get(0).getText().trim().equalsIgnoreCase(headerValues.get(listCounter)));
//	        log.info("user verify header verbiage " + headerValues.get(listCounter) + " on sign up page");
//	        listCounter = listCounter + 1;
//		}
//	}
	
	@Then("^user verify Terms and Condition error for FS SignUp$")
	public void user_verify_Terms_and_Condition_error_for_FS_SignUp() throws Throwable {
	 	driver.navigate().refresh();
	 	Thread.sleep(5000);
	 	
	  	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();
		
		Thread.sleep(1000);
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");
		
		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();
		
		Thread.sleep(1000);
		
        Assert.assertTrue("user not verify require Terms and Condition error on sign up page",
        		driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("You must agree to the Terms and Conditions to continue"));
		log.info("user verify require Terms and Condition error on sign up page");
	}
	
	@Then("^user verify FS signup successfully$")
	public void user_verify_FS_signup_successfully() throws Throwable {
	 	driver.navigate().refresh();
	 	Thread.sleep(5000);
	 	
	  	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jack.flyer123@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jack.flyer123@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();
		
		Thread.sleep(1000);
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_adress)).sendKeys("2800 Executive Way");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_city)).sendKeys("Miramar");

		driver.findElement(By.xpath(NineFC.nine_fc_sigup_zipcode)).sendKeys("33025");
		
		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");

		driver.findElement(By.xpath(NineFC.nine_fc_signup_primaryphone)).sendKeys("9544471111");

		Select s2 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_state)));
		s2.selectByVisibleText("Florida");

		Select s3 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_primary_airport)));
		s3.selectByVisibleText("Fort Lauderdale, FL / Miami, FL AREA (FLL)");
		
		driver.findElement(By.xpath(NineFC.FS_member_termsand_conditions)).click();

		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();
		
		Thread.sleep(5000);

		
/*        Assert.assertTrue("user not verify require Terms and Condition error on sign up page",
        		driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("You must agree to the Terms and Conditions to continue"));
		log.info("user verify require Terms and Condition error on sign up page");*/
	}
//  
/*//	@Then("^user clicks on the Your Miles link in new browser and verify all fields$")
//	public void user_clicks_on_the_Your_Miles_link_in_new_browser_and_verify_all_fields() throws Throwable {
//		
//		Actions manualClick = new Actions(driver);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.your_miles)));
//		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.your_miles))).keyUp(Keys.CONTROL).build().perform();
//		
//		String mainWindow=driver.getWindowHandle();
//		
//		 Set<String> set =driver.getWindowHandles();
//		 // Using Iterator to iterate with in windows
//		 Iterator<String> itr= set.iterator();
//		 while(itr.hasNext()){
//			 String childWindow=itr.next();
//		
//			 if(!mainWindow.equals(childWindow)){
//				 driver.switchTo().window(childWindow);
//				 
//				 break;
//			 }
//		 }
//		 
//		Thread.sleep(10000);	
//		
//		//verify user profile header verbiage
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_Header)));
//        Assert.assertTrue("user not verify header verbiage on user profile page",
//        		driver.findElement(By.xpath(NineFC.member_profile_Header)).isDisplayed());
//		log.info("user verify header verbiage on user profile page");
//		
//		//verify user profile sub header verbiage
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_SubHeader)));
//        Assert.assertTrue("user not verify sub header verbiage on user profile page",
//        		driver.findElement(By.xpath(NineFC.member_profile_SubHeader)).isDisplayed());
//		log.info("user verify sub header verbiage on user profile page");
//		
//		int listCounter = 0;
//		ArrayList<String> userProfileLinks = new ArrayList<>(Arrays.asList(	"My Account",				"Personal Information",		"Billing Information",
//																			"Email Subscriptions",    	"Statements",				"Redeem Miles",
//																			"Buy or Gift Miles", 		"Request Mileage Credit", 	""));
//		
//		List<WebElement> allProfileLinks = driver.findElements(By.xpath(NineFC.member_profile_LeftPanelinks));
//		
//		//verify left pane link on user profile page
//		for (WebElement profileLink : allProfileLinks) {
//	        Assert.assertTrue("user not verify left pane link " + userProfileLinks.get(listCounter) + " on user profile page",
//	        		profileLink.getText().trim().equals(userProfileLinks.get(listCounter)));
//			log.info("user verify left pane link \" + userProfileLinks.get(listCounter) + \" on user profile page");
//			
//			listCounter = listCounter + 1;
//		}
//		
		//verify user profile current miles verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_CurrentMiles)));
        Assert.assertTrue("user not verify Current Miles verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_CurrentMiles)).isDisplayed());
		log.info("user verify Current Miles verbiage on user profile page");
		
		//verify user profile Free Spirit Number verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_fsNumber)));
        Assert.assertTrue("user not verify Free Spirit Number verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_fsNumber)).isDisplayed());
		log.info("user verify Free Spirit Number verbiage on user profile page");
		
		
		//verify user profile Mileage Earning Tier verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_MileagEarning)));
        Assert.assertTrue("user not verify Mileage Earning Tier verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_MileagEarning)).isDisplayed());
		log.info("user verify Mileage Earning Tier verbiage on user profile page");
		
		
		//verify user profile Bank Of America Banner Image 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_BannerImage)));
        Assert.assertTrue("user not verify Bank Of America Banner Image on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_BannerImage)).getAttribute("src").contains("https://cms10dss.spirit.com/Shared/en-us/Account/fsmc-a_en_full.jpg"));
		log.info("user verify Bank Of America Banner Image on user profile page");
		
		//verify user profile Your Membership verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_YourMembership)));
        Assert.assertTrue("user not verify Your Membership verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_YourMembership)).isDisplayed());
		log.info("user verify Your Membership verbiage on user profile page");
		
		//verify user profile Membership Name verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_MembershipName)));
        Assert.assertTrue("user not verify Membership Name verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_MembershipName)).isDisplayed());
		log.info("user verify Membership Name verbiage on user profile page");
		
		//verify user profile Date Joined verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_DateJoined)));
        Assert.assertTrue("user not verify Date Joined verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_DateJoined)).isDisplayed());
		log.info("user verify Date Joined verbiage on user profile page");
		
		final String membershipVerbiage = "Not a member yet of Spirit's $9 Fare Club®? Click here to learn more about gaining exclusive access to some of Spirit's lowest fares!";
		//verify user profile Membership verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_MembershipVerbiage)));
        Assert.assertTrue("user not verify Membership verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_MembershipVerbiage)).getText().trim().equals(membershipVerbiage));
		log.info("user verify Membership verbiage on user profile page");
		
		
		//verify user profile Your Reservations verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_YourReservation)));
        Assert.assertTrue("user not verify Your Reservations verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_YourReservation)).isDisplayed());
		log.info("user verify Your Reservations verbiage on user profile page");
		
		//verify user profile Your Reservations verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_YourReservation)));
        Assert.assertTrue("user not verify Your Reservations verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_YourReservation)).isDisplayed());
		log.info("user verify Your Reservations verbiage on user profile page");
		
		final String locateReservation = "Locate A Reservation: Enter your six character Confirmation Code to locate and add your Free Spirit Number to the reservation. Mileage for previously flown flights will be credited instantly, future flights will be credited within 24 hours of flight departure.";
		//verify user Locate Reservation verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_LocateReservation)));
        Assert.assertTrue("user not verify Locate Reservation verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_LocateReservation)).getText().trim().equals(locateReservation));
		log.info("user verify Locate Reservation verbiage on user profile page");
		
		//verify user profile Confirmation Code (6 characters) verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_ConfirmationVerbiage)));
        Assert.assertTrue("user not verify Confirmation Code (6 characters) verbiage on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_ConfirmationVerbiage)).isDisplayed());
		log.info("user verify Confirmation Code (6 characters) verbiage on user profile page");
		
		//verify user profile Confirmation Code Box verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_ConfirmationBox)));
        Assert.assertTrue("user not verify Confirmation Code Box  on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_ConfirmationBox)).isDisplayed());
		log.info("user verify Confirmation Code Box  on user profile page");
		
		//verify user profile Confirmation Code Button verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_ConfirmationButton)));
        Assert.assertTrue("user not verify Confirmation Code Button  on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_ConfirmationButton)).isDisplayed());
		log.info("user verify Confirmation Code Button on user profile page");
		
		//verify user profile Current Reservations verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_CurrentReservation)));
        Assert.assertTrue("user not verify Current Reservations on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_CurrentReservation)).isDisplayed());
		log.info("user verify Current Reservations on user profile page");
		
		//verify user profile Past Reservations verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.member_profile_PastReservation)));
        Assert.assertTrue("user not verify Past Reservations on user profile page",
        		driver.findElement(By.xpath(NineFC.member_profile_PastReservation)).isDisplayed());
		log.info("user verify Past Reservations on user profile page");
		
		driver.close();
		 
		driver.switchTo().window(mainWindow);
		 
	}*/

	@Then("^user clicks on the Free Spirit Number in new browser and verify all fields$")
	public void user_clicks_on_the_Free_Spirit_Number_in_new_browser_and_verify_all_fields() throws Throwable {
		
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.free_spirit)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.free_spirit))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		 Thread.sleep(5000);
	
        Assert.assertTrue("user not verify Free Spirit Number link on Home page",
        		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/profile"));
		log.info("user verify Free Spirit Number link on Home page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the My Reservations in new browser and verify all fields$")
	public void user_clicks_on_the_My_Reservations_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.my_reservation)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.my_reservation))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		 Thread.sleep(5000);
			
        Assert.assertTrue("user not verify My Reservations link on Home page",
        		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/profile"));
		log.info("user verify My Reservations link on Home page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the My Account in new browser and verify all fields$")
	public void user_clicks_on_the_My_Account_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.account_link)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.account_link))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		 Thread.sleep(5000);
			
        Assert.assertTrue("user not verify My Account link on Home page",
        		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/profile"));
		log.info("user verify My Account link on Home page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the Manage Subscription in new browser and verify all fields$")
	public void user_clicks_on_the_Manage_Subscription_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.manage_subscriptions))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		Thread.sleep(10000);
		
	    Assert.assertTrue("user not verify Manage Subscription link on Home page",
	    		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/email-notify-sign-in"));
		log.info("user verify Manage Subscription link on Home page");
		
		//verify Manage Subscription Email Deals verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_Header)));
        Assert.assertTrue("user not verify Email Deals verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_Header)).isDisplayed());
		log.info("user verify Email Deals verbiage on Manage Subscription page");
		
		//verify Manage Subscription Sub Header1 verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_SubHeader1)));
        Assert.assertTrue("user not verify Sub Header1 verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_SubHeader1)).isDisplayed());
		log.info("user verify Sub Header1 verbiage on Manage Subscription page");
		
		//verify Manage Subscription Sub Header2 verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_SubHeader2)));
        Assert.assertTrue("user not verify Sub Header2 verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_SubHeader2)).isDisplayed());
		log.info("user verify Sub Header2 verbiage on Manage Subscription page");
		
		//verify Manage Subscription Email Title verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_EmailTitle)));
        Assert.assertTrue("user not verify Email Title verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_EmailTitle)).isDisplayed());
		log.info("user verify Email Title verbiage on Manage Subscription page");
		
		//verify Manage Subscription Email Box verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_EmailBox)));
        Assert.assertTrue("user not verify Email Box verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_EmailBox)).isDisplayed());
		log.info("user verify Email Box verbiage on Manage Subscription page");
		
		//verify Manage Subscription Email Button verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_EmailButton)));
        Assert.assertTrue("user not verify Email Button verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_EmailButton)).isDisplayed());
		log.info("user verify Email Button verbiage on Manage Subscription page");
		
		//verify Manage Subscription Footer verbiage
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.manage_subscriptions_FooterText)));
        Assert.assertTrue("user not verify Footer  verbiage on Manage Subscription page",
        		driver.findElement(By.xpath(NineFC.manage_subscriptions_FooterText)).isDisplayed());
		log.info("user verify Footer verbiage on Manage Subscription page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the Statements in new browser and verify all fields$")
	public void user_clicks_on_the_Statements_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.statements))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		Thread.sleep(5000);
		
	    Assert.assertTrue("user not verify Statements link on Home page",
	    		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/statement"));
		log.info("user verify Statements link on Home page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the Request Mileage Credit in new browser and verify all fields$")
	public void user_clicks_on_the_Request_Mileage_Credit_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.request_mileage_credit)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.request_mileage_credit))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		Thread.sleep(5000);
		
	    Assert.assertTrue("user not verify Request Mileage Credit link on Home page",
	    		driver.getCurrentUrl().equalsIgnoreCase("http://qaepic01.spirit.com/account/retro-credit-request"));
		log.info("user verify Request Mileage Credit link on Home page");
		  	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the Buy Miles in new browser and verify all fields$")
	public void user_clicks_on_the_Buy_Miles_in_new_browser_and_verify_all_fields() throws Throwable {
		Actions manualClick = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.buy_miles)));
		manualClick.keyDown(Keys.CONTROL).click(driver.findElement(By.xpath(NineFC.buy_miles))).keyUp(Keys.CONTROL).build().perform();
		
		String mainWindow=driver.getWindowHandle();
		
		 Set<String> set =driver.getWindowHandles();
		 // Using Iterator to iterate with in windows
		 Iterator<String> itr= set.iterator();
		 while(itr.hasNext()){
			 String childWindow=itr.next();
		
			 if(!mainWindow.equals(childWindow)){
				 driver.switchTo().window(childWindow);
				 
				 break;
			 }
		 }
		 
		Thread.sleep(5000);
		
	    Assert.assertTrue("user not verify Request Mileage Credit link on Home page",
	    		driver.getCurrentUrl().equalsIgnoreCase("https://buy.points.com/marketing/spirit/landing-page/?product=buy"));
		log.info("user verify Request Mileage Credit link on Home page");
	
		driver.close();
		 
		driver.switchTo().window(mainWindow);
	}

	@Then("^user clicks on the Sign Out and verify user is loggod out from application$")
	public void user_clicks_on_the_Sign_Out_and_verify_user_is_loggod_out_from_application() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.sign_out)));
		driver.findElement(By.xpath(NineFC.sign_out)).click();
		
		Thread.sleep(5000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.Homepage_sigin_button)));
	    Assert.assertTrue("user not verify Sign out link on Home page",
	    		driver.findElement(By.xpath(NineFC.Homepage_sigin_button)).getText().trim().equals("SIGN-IN"));
		log.info("user verify Sign Out link on Home page");
	}
	
	@Then("^user clicks on the My Account link$")
	public void user_clicks_on_the_My_Account_link() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.account_link)));
		driver.findElement(By.xpath(NineFC.account_link)).click();
	}

	/*@Then("^user verify all header fields under Current Reservation$")
	public void user_verify_all_header_fields_under_Current_Reservation() throws Throwable {
		Thread.sleep(10000);
		ArrayList<String> currentReservationsHeaders = new ArrayList<>(Arrays.asList(	"Date",					"Origin",		"Destination",
																						"Confirmation Code",	"Miles",		""));
		
		List<WebElement> allHeaderCount = driver.findElements(By.xpath(NineFC.member_profile_CurrentReservationHeader));
		for(int headerCounter = 0; headerCounter<currentReservationsHeaders.size();headerCounter++) {
		    Assert.assertTrue("user not verify header "+ currentReservationsHeaders.get(headerCounter) + " on Manage Profile Page",
		    		allHeaderCount.get(headerCounter).getText().trim().equals(currentReservationsHeaders.get(headerCounter)));
			log.info("user verify header "+ currentReservationsHeaders.get(headerCounter) + " on Manage Profile Page");
		}
	}*/

	@Then("^user verify reservation details under Current Reservation$")
	public void user_verify_reservation_details_under_Current_Reservation() throws Throwable {
		HashMap<String, String> flightDetails = new HashMap<String, String>();
		
		List<WebElement> allFlightDetails = driver.findElements(By.xpath(NineFC.member_profile_CurrentReservationFlights));
		
		flightDetails.put("Date", allFlightDetails.get(0).getText());
		flightDetails.put("Origin", allFlightDetails.get(1).getText());
		flightDetails.put("Destination", allFlightDetails.get(2).getText());
		flightDetails.put("ConfirmationCode", allFlightDetails.get(3).getText());
		flightDetails.put("Miles", allFlightDetails.get(4).getText());
		
		//to handle when dates throw parse exception
		try {
			//parse date string from user current reservation 
			Date date1=new SimpleDateFormat("MMM dd, yyyy", Locale.US).parse(flightDetails.get("Date")); 
			
			log.info("user verify dates from booking summary and user profile on Booking Summary Page");
		}
		catch (ParseException e) {
            e.printStackTrace();
			//compare both dates are equal
		    Assert.assertTrue("user not verify dates from booking summary and user profile on Booking Summary Page",false);
        }

	

	}

	@Then("^user click on end of reservation under Current Reservation link and verify all fields$")
	public void user_click_on_end_of_reservation_under_Current_Reservation_link_and_verify_all_fields() throws Throwable {
		HashMap<String, String> flightDetails = new HashMap<String, String>();
		
		List<WebElement> allFlightDetails = driver.findElements(By.xpath(NineFC.member_profile_CurrentReservationFlights));
		
		flightDetails.put("Date", allFlightDetails.get(0).getText());
		flightDetails.put("Origin", allFlightDetails.get(1).getText());
		flightDetails.put("Destination", allFlightDetails.get(2).getText());
		flightDetails.put("ConfirmationCode", allFlightDetails.get(3).getText());
		flightDetails.put("Miles", allFlightDetails.get(4).getText());
		
		allFlightDetails.get(5).click();
		
		Thread.sleep(20000);
		
		//to handle when dates throw parse exception
		try {
			//parse date string from user current reservation 
			Date date1=new SimpleDateFormat("MMM dd, yyyy", Locale.US).parse(flightDetails.get("Date")); 
			//parse date string from booking summary page
			Date date2=new SimpleDateFormat("MMMMM dd, yyyy", Locale.US).parse(driver.findElement(By.xpath(BookingSummaryPage.departureDate)).getText().trim());
			
			//compare both dates are equal
		    Assert.assertTrue("user not verify dates from booking summary and user profile on Booking Summary Page",
		    		date2.compareTo(date1) ==0);
			log.info("user verify dates from booking summary and user profile on Booking Summary Page");
		}
		catch (ParseException e) {
            e.printStackTrace();
        }

		//compare both origin airports are equal
	    Assert.assertTrue("user not verify origin airports from booking summary and user profile on Booking Summary Page",
	    		driver.findElement(By.xpath(BookingSummaryPage.originAirport)).getText().trim().contains(flightDetails.get("Origin")));
		log.info("user verify origin airports from booking summary and user profile on Booking Summary Page");

		//compare both destination airports are equal
	    Assert.assertTrue("user not verify destination airports from booking summary and user profile on Booking Summary Page",
	    		driver.findElement(By.xpath(BookingSummaryPage.destinationAirport)).getText().trim().contains(flightDetails.get("Destination")));
		log.info("user verify destination airports from booking summary and user profile on Booking Summary Page");
		
		//compare both Confirmation Code are equal
	    Assert.assertTrue("user not verify Confirmation Code from booking summary and user profile on Booking Summary Page",
	    		driver.findElement(By.xpath(BookingSummaryPage.confirmationCode)).getText().trim().contains(flightDetails.get("ConfirmationCode")));
		log.info("user verify Confirmation Code from booking summary and user profile on Booking Summary Page");
		
		driver.navigate().back();
		
		Thread.sleep(5000);
		
	}

/*	@Then("^user verify all header fields under Past Resevations$")
	public void user_verify_all_header_fields_under_Past_Resevations() throws Throwable {
		ArrayList<String> currentReservationsHeaders = new ArrayList<>(Arrays.asList("Date",				"Origin",		"Destination",
																					"Confirmation Code",	"Miles",		""));

		List<WebElement> allHeaderCount = driver.findElements(By.xpath(NineFC.member_profile_PastReservationHeader));
		
		for(int headerCounter = 0; headerCounter<currentReservationsHeaders.size();headerCounter++) {
			Assert.assertTrue("user not verify header "+ currentReservationsHeaders.get(headerCounter) + " on Manage Profile Page",
			allHeaderCount.get(headerCounter).getText().trim().equals(currentReservationsHeaders.get(headerCounter)));
			log.info("user verify header "+ currentReservationsHeaders.get(headerCounter) + " on Manage Profile Page");
		}
	}*/

	@Then("^user verify reservation details under Past Resevations$")
	public void user_verify_reservation_details_under_Past_Resevations() throws Throwable {

	}

	@Then("^user click on end of reservation under Past Resevations link and verify all fields$")
	public void user_click_on_end_of_reservation_under_Past_Resevations_link_and_verify_all_fields() throws Throwable {

	}
	
	@Then("^User click on CONTINUE TO STEP (\\d+) button$")
	public void user_click_on_CONTINUE_TO_STEP_button(int arg1) throws Throwable {

		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();
		
		Thread.sleep(1000);
		
        Assert.assertTrue("user not click Continue to Step 2 on sign up page",
        		driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("Title is required"));
		log.info("user verify click Continue to Step 2 on sign up page");
	}

	/*@Then("^User verify all mandatory fields error message of Account Tab$")
	public void user_verify_all_mandatory_fields_error_message_of_Account_Tab() throws Throwable {
		int counter = 0;
		ArrayList<String> accountTabErrors = new ArrayList<>(Arrays.asList(	"Title is required",			"First Name is required",		"Last Name is required",
																			"Date of Birth is required",	"Email is required",			"Confirm Email is required",
																			"Password is required",			"Confirm Password is required"));
		
		List<WebElement> allAccoutTabErrors = driver.findElements(By.xpath(NineFC.SignUp_Error_Message));
		
		for(WebElement errorMessage : allAccoutTabErrors) {
			if(errorMessage.getText().trim().length() > 0) {
		        Assert.assertTrue("user not verify error message " + accountTabErrors.get(counter) + " Account Tab on sign up page",
		        		errorMessage.getText().trim().equals(accountTabErrors.get(counter)));
				log.info("user verify error message " + accountTabErrors.get(counter) + " Account Tab on sign up page");
				
				counter = counter + 1;
			}	
		}
		
	}*/

	@Then("^User fill all mandatory fields of Account Tab and click CONTINUE TO STEP (\\d+)$")
	public void user_fill_all_mandatory_fields_of_Account_Tab_and_click_CONTINUE_TO_STEP(int arg1) throws Throwable {

	  	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(NineFC.nine_fc_signup_title)));
		Select s = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_title)));
		s.selectByVisibleText("Mr.");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_firstname)).sendKeys("jack");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_lastname)).sendKeys("flyer");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_dateofbirth)).sendKeys("12/12/1989");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_email_address)).sendKeys("jackflyer@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_email_address)).sendKeys("jackflyer@spirit.com");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_confirm_password)).sendKeys("Spirit11!");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_continue_step_button)).click();
		
		Thread.sleep(1000);
	}

	@Then("^User click on FREE SIGN UP button on Contact Tab$")
	public void user_click_on_FREE_SIGN_UP_button_on_Contact_Tab() throws Throwable {
		Select s1 = new Select(driver.findElement(By.xpath(NineFC.nine_fc_signup_country)));
		s1.selectByVisibleText("United States of America");
		
		driver.findElement(By.xpath(NineFC.nine_fc_signup_contine3_button)).click();
		
		Thread.sleep(1000);
	}

	/*@Then("^User verify all mandatory fields error message of Contact Tab$")
	public void user_verify_all_mandatory_fields_error_message_of_Contact_Tab() throws Throwable {
		int counter = 0;
		ArrayList<String> contactTabErrors = new ArrayList<>(Arrays.asList(	"Address is required",			"City is required",					"State/Province is required",
																			"Zip/Postal Code is required",	"Primary Phone is required",		"Home Airport is required",
																			"You must agree to the Terms and Conditions to continue"));
		
		List<WebElement> allContactTabErrors = driver.findElements(By.xpath(NineFC.SignUp_Error_Message));
		
		for(WebElement errorMessage : allContactTabErrors) {
			if(errorMessage.getText().trim().length() > 0) {
		        Assert.assertTrue("user not verify error message " + contactTabErrors.get(counter) + " Contact Tab on sign up page",
		        		errorMessage.getText().trim().equals(contactTabErrors.get(counter)));
				log.info("user verify error message " + contactTabErrors.get(counter) + " Contact Tab on sign up page");
				
				counter = counter + 1;
			}
		}
	}*/

	@Then("^User verify Terms & Conditions message is in red colors$")
	public void user_verify_Terms_Conditions_message_is_in_red_colors() throws Throwable {

		List<WebElement> allContactTabErrors = driver.findElements(By.xpath(NineFC.SignUp_Error_Message));
		
		int errorcount = allContactTabErrors.size();
		
		String errormessage = allContactTabErrors.get(errorcount-1).getText().trim();
		
		if(errormessage.equals("You must agree to the Terms and Conditions to continue")) {
			String errormessagecolor = allContactTabErrors.get(errorcount-1).getCssValue("color");
			
			if(errormessagecolor.equals("rgba(220, 0, 0, 1)")) {

				log.info("user verify color of terms and condition error message Contact Tab on sign up page");		
			}else{
		        Assert.assertTrue("user not verify color of terms and condition error message Contact Tab on sign up page",false);
			}
		}else {
			Assert.assertTrue("user not verify color of terms and condition error message Contact Tab on sign up page",false);
		}
	}

	@Then("^user click on Unsubscribe from Email Deals$")
	public void user_click_on_Unsubscribe_from_Email_Deals() throws Throwable {
			Thread.sleep(7000);
			
			WebElement element = driver.findElement(By.xpath(NineFC.subscribe_email_checkbox));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			
			driver.findElement(By.xpath(NineFC.subscribe_button)).click();
	}

	@Then("^user verify Unsubscribe from Email Deals Popup$")
	public void user_verify_Unsubscribe_from_Email_Deals_Popup() throws Throwable {
		
		Thread.sleep(7000);
		
		//subscibe_Popup_Header
        Assert.assertTrue("user not verify header verbiage Email Subscribe popup on sign up page",
        		driver.findElement(By.xpath(NineFC.subscibe_Popup_Header)).isDisplayed());
		log.info("user verify header verbiage Email Subscribe popup on sign up page");
		
		final String subheaderfirst = "Your subscription has been successfully created.";
		
        Assert.assertTrue("user not verify sub header first  verbiage Email Subscribe popup on sign up page",
        		driver.findElement(By.xpath(NineFC.subscibe_Popup_SubHeader1)).getText().trim().equals(subheaderfirst));
		log.info("user verify sub header first verbiage Email Subscribe popup on sign up page");
		
		final String subheadersecond = "To ensure delivery of our e-mails, please remember to add deals@p.spiritairlines.com or the domain spiritairlines.com to your address book or appoved senders list.";
		
        Assert.assertTrue("user not verify sub header second  verbiage Email Subscribe popup on sign up page",
        		driver.findElement(By.xpath(NineFC.subscibe_Popup_SubHeader2)).getText().trim().equals(subheadersecond));
		log.info("user verify sub header second verbiage Email Subscribe popup on sign up page");
		
		//subscibe_Popup_Header
        Assert.assertTrue("user not verify Home Button Email Subscribe popup on sign up page",
        		driver.findElement(By.xpath(NineFC.subscribe_popup_HomePageButton)).isDisplayed());
		log.info("user verify Home Button Email Subscribe popup on sign up page");
		
		//subscibe_Popup_Header
        Assert.assertTrue("user not verify Close Button Email Subscribe popup on sign up page",
        		driver.findElement(By.xpath(NineFC.subscribe_popup_CloseButton)).isDisplayed());
		log.info("user verify Close Button Email Subscribe popup on sign up page");
		
		driver.findElement(By.xpath(NineFC.subscribe_popup_CloseButton)).click();
	}
	
	@Then("^user clicks on the Free Spirit Number link form the list$")
	public void user_clicks_on_the_Free_Spirit_Number_link_form_the_list() throws Throwable {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.free_spirit)));
		driver.findElement(By.xpath(NineFC.free_spirit)).click();
	}

	@Then("^user get booking Confirmation Code of last month rom past reservation$")
	public void user_get_booking_Confirmation_Code_of_last_month_rom_past_reservation() throws Throwable {
		//use below variable to store confirmation code
		//confirmationCode
	}
	
	@Then("^user get booking Confirmation Code of last month rom current reservation$")
	public void user_get_booking_Confirmation_Code_of_last_month_rom_current_reservation() throws Throwable {
		Thread.sleep(7000);
		List<WebElement> allFlightDetails = driver.findElements(By.xpath(NineFC.member_profile_CurrentReservationFlights));
		 String confirmationCode = allFlightDetails.get(3).getText().trim();
	}

	@Then("^user click on Request Mileage Credit link$")
	public void user_click_on_Request_Mileage_Credit_link() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.request_mileage_creditlink)));
		driver.findElement(By.xpath(NineFC.request_mileage_creditlink)).click();
	}

	@Then("^user enter Confirmation Code into for Request Mileage Credit$")
	public void user_enter_Confirmation_Code_into_for_Request_Mileage_Credit() throws Throwable {
		
		Thread.sleep(3000);
		
	   List<WebElement> allFlightDetails = driver.findElements(By.xpath(NineFC.member_profile_CurrentReservationFlights));
	   String	confirmationCode = allFlightDetails.get(3).getText().trim();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.conformation_code_request_mileage_credit)));
		driver.findElement(By.xpath(NineFC.conformation_code_request_mileage_credit)).sendKeys(confirmationCode);
		
		
		driver.findElement(By.xpath(NineFC.Request_mileage_credit_GO_button)).click();
		
	}

	@Then("^user verify error message for past flown flight$")
	public void user_verify_error_message_for_past_flown_flight() throws Throwable {

	}

	@Then("^user verify error message for future flight$")
	public void user_verify_error_message_for_future_flight() throws Throwable {
		final String errorText = "Sorry, please wait until your flight has arrived before requesting point credit.";
		
        Assert.assertTrue("user not verify Request Mileage Credit error message on sign up page",
        		driver.findElement(By.xpath(NineFC.request_mileage_ErrorMessage)).getText().trim().equals(errorText));
		log.info("user verify Request Mileage Credit error message on sign up page");
	}
	

	@Then("^user clicks on the Statement link form the list$")
	public void user_clicks_on_the_Statement_link_form_the_list() throws Throwable {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements)));
		driver.findElement(By.xpath(NineFC.statements)).click();
	}
	
	
	@Then("^user verify Statement header text$")
	public void user_verify_Statement_header_text() throws Throwable {
		Thread.sleep(5000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements_HeaderText)));	
        Assert.assertTrue("user not verify Statement header text on sign up page",
        		driver.findElement(By.xpath(NineFC.statements_HeaderText)).isDisplayed());
		log.info("user verify Statement header text on sign up page");
	}

	@Then("^user verify Activity Period header text$")
	public void user_verify_Activity_Period_header_text() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements_ActivityPeriod)));	
        Assert.assertTrue("user not verify Activity Period header text on sign up page",
        		driver.findElement(By.xpath(NineFC.statements_ActivityPeriod)).isDisplayed());
		log.info("user verify Activity Period header text on sign up page");
	}

	@Then("^user verify Activity Period drop down$")
	public void user_verify_Activity_Period_drop_down() throws Throwable {
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements_ActivityPeriod_DropDown)));		
		
		Select drpdwnActivityPeriod = new Select(driver.findElement(By.xpath(NineFC.statements_ActivityPeriod_DropDown)));
		
		List<WebElement> allActivtyPeriod = drpdwnActivityPeriod.getOptions();
		
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		
		//create dates instance of cuurent date and before one year date
		Calendar cal = Calendar.getInstance();
		Date endDate =  cal.getTime();
	    cal.add(Calendar.YEAR, -1);
	    Date startDate =  cal.getTime();
	    
		
		for(int count=0; count < allActivtyPeriod.size();count++) {
			if(count==0) {
				allActivtyPeriod.get(count).getText().trim().equals("Current Period");
			}else {
				String dtValue = "01-" + allActivtyPeriod.get(count).getText();
		
				Date drpdnDate = df.parse(dtValue);
			    	
		        Assert.assertTrue("user not verify Activity Period dates on sign up page",
		        		startDate.before(drpdnDate) && endDate.after(drpdnDate));
				log.info("user verify Activity Period dates on sign up page");	
			}
		}
	}

	@Then("^user verify Transaction Types header text$")
	public void user_verify_Transaction_Types_header_text() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements_TransactionTypes)));	
        Assert.assertTrue("user not verify Transaction Types header text on sign up page",
        		driver.findElement(By.xpath(NineFC.statements_TransactionTypes)).isDisplayed());
		log.info("user verify Transaction Types header text on sign up page");
	}

	@Then("^user verify Transaction Types drop down$")
	public void user_verify_Transaction_Types_drop_down() throws Throwable {
		
		List<String> expectedTransactionTypes = Arrays.asList("All Transactions", "Bank of America","Promerica","Spirit Airlines Miles");
		Select drpdwnTransactionTypes = new Select(driver.findElement(By.xpath(NineFC.statements_TransactionTypes_DropDown)));
		
		List<WebElement> allTransactionTypes = drpdwnTransactionTypes.getOptions();
		
		for(int counter=0;counter<allTransactionTypes.size();counter++) {
			
	        Assert.assertTrue("user not verify Transaction Types drop down value on sign up page",
	        		allTransactionTypes.get(counter).getText().trim().equals(expectedTransactionTypes.get(counter)));
			log.info("user verify Transaction Types drop down value on sign up page");
		}
	}

	@Then("^user verify Post Transactions header text$")
	public void user_verify_Post_Transactions_header_text() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NineFC.statements_PostedTransactions)));	
        Assert.assertTrue("user not verify Post Transaction header text on sign up page",
        		driver.findElement(By.xpath(NineFC.statements_PostedTransactions)).isDisplayed());
		log.info("user verify Post Transaction header text on sign up page");
	}

	@Then("^user verify Post Transactions items$")
	public void user_verify_Post_Transactions_items() throws Throwable {
		List<String> expectedPostTransactionsFields = Arrays.asList("Date","Transaction","Debit(-)","Credit(+)","Balance*");
		List<WebElement> postTransactionsFields = driver.findElements(By.xpath(NineFC.statements_PostedTransactions_Fields));
		
		for(int counter=0;counter<postTransactionsFields.size();counter++) {
			
			
	        Assert.assertTrue("user not verify postTransaction Fields value on sign up page",
	        		postTransactionsFields.get(counter).getText().trim().equals(expectedPostTransactionsFields.get(counter)));
			log.info("user verify postTransaction Fields value on sign up page");
		}
	}

	@Then("^user verify Post Transaction details$")
	public void user_verify_Post_Transaction_details() throws Throwable {

	}

	@Then("^user verify Statement footer verbiage$")
	public void user_verify_Statement_footer_verbiage() throws Throwable {
		final String expectedFooterVerbiage = "*Beginning Balance as of";
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy",Locale.US);
		
		String footerVerbiage = driver.findElement(By.xpath(NineFC.statements_FooterVerbiage)).getText();
		
        Assert.assertTrue("user not verify Footer Verbiage value on sign up page",
        		footerVerbiage.contains(expectedFooterVerbiage));
		log.info("user verify Footer Verbiage value on sign up page");
		
		String[] allfooterText = footerVerbiage.split(" ");
		
		//get date from footer verbiage
		String dateVerbiage = allfooterText[allfooterText.length-1];
		
		try {
			sdf.parse(dateVerbiage);
			log.info("user verify Footer Verbiage date value on sign up page");
		}catch(ParseException e) {
			 Assert.assertTrue("user not verify Footer Verbiage date value on sign up page",
		        		false);
		}
	
	}

	@Then("^user click on Personal Information link$")
	public void user_click_on_Personal_Information_link() throws Throwable {
		Thread.sleep(2000);
		
		driver.findElement(By.xpath(NineFC.PersonalInformation_link)).click();
	}

	@Then("^user verify filled details of address section in Your Information$")
	public void user_verify_filled_details_of_address_section_in_Your_Information() throws Throwable {
		int counter = 0;
		List<String> filledInformation = Arrays.asList("2800 Executive Way","Miramar", "Florida","33025","United States of America");
		
		String txtAddress = driver.findElement(By.xpath(NineFC.personal_information_Address)).getText().trim();
		
        Assert.assertTrue("user not verify Address of Your Information value on Personal Information page",
        		txtAddress.equals(filledInformation.get(counter)));
		log.info("user verify Address of Your Information value on Personal Information page");
		counter += 1;
		
		String txtCity = driver.findElement(By.xpath(NineFC.personal_information_City)).getText().trim();
        Assert.assertTrue("user not verify City of Your Information value on Personal Information page",
        		txtCity.equals(filledInformation.get(counter)));
		log.info("user verify City of Your Information value on Personal Information page");
		counter += 1;
		
		Select drpdnState = new Select(driver.findElement(By.xpath(NineFC.personal_information_State)));
		String txtState = drpdnState.getFirstSelectedOption().getText().trim();
        Assert.assertTrue("user not verify State of Your Information value on Personal Information page",
        		txtState.equals(filledInformation.get(counter)));
		log.info("user verify State of Your Information value on Personal Information page");
		counter += 1;
		
		String txtZipCode = driver.findElement(By.xpath(NineFC.personal_information_ZIPCode)).getText().trim();
        Assert.assertTrue("user not verify Zip/Postal Code of Your Information value on Personal Information page",
        		txtZipCode.equals(filledInformation.get(counter)));
		log.info("user verify Zip/Postal Code of Your Information value on Personal Information page");
		counter += 1;
		
		Select drpdnCountry = new Select(driver.findElement(By.xpath(NineFC.personal_information_Country)));
		String txtCountry = drpdnCountry.getFirstSelectedOption().getText().trim();
        Assert.assertTrue("user not verify Country Code of Your Information value on Personal Information page",
        		txtCountry.equals(filledInformation.get(counter)));
		log.info("user verify Country Code of Your Information value on Personal Information page");
	}

	@Then("^user verify require Your Information Address error message$")
	public void user_verify_require_Your_Information_Address_error_message() throws Throwable {
		driver.findElement(By.xpath(NineFC.personal_information_Address)).clear();
		
		driver.findElement(By.xpath(NineFC.personal_information_SaveButton)).click();
		
		Thread.sleep(1000);
		
        Assert.assertTrue("user not verify require Your Information Address error on Personal Information page",
        		driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("Address is required"));
		log.info("user verify require Your Information Address error on Personal Information page");
	}

	@Then("^user verify Symbol invalid Your Information Address error message$")
	public void user_verify_Symbol_invalid_Your_Information_Address_error_message() throws Throwable {
		driver.findElement(By.xpath(NineFC.personal_information_Address)).sendKeys("##########");
		
		driver.findElement(By.xpath(NineFC.personal_information_SaveButton)).click();
		
		Thread.sleep(1000);
		
        Assert.assertTrue("user not verify invalid Your Information Address error on Personal Information page",
        		driver.findElements(By.xpath(NineFC.SignUp_Error_Message)).get(0).getText().trim().equals("At least one letter is required"));
		log.info("user verify invalid Your Information Address error on Personal Information page");
	}

	@Then("^user verify require Your Information City error message$")
	public void user_verify_require_Your_Information_City_error_message() throws Throwable {

	}

	@Then("^user verify Symbol invalid Your Information City error message$")
	public void user_verify_Symbol_invalid_Your_Information_City_error_message() throws Throwable {

	}

	@Then("^user verify Number invalid Your Information City error message$")
	public void user_verify_Number_invalid_Your_Information_City_error_message() throws Throwable {

	}

	@Then("^user verify require Your Information State error message$")
	public void user_verify_require_Your_Information_State_error_message() throws Throwable {

	}

	@Then("^user verify require Your Information Zip Code error message$")
	public void user_verify_require_Your_Information_Zip_Code_error_message() throws Throwable {

	}

	@Then("^user verify less than four character Your Information Zip Code error message$")
	public void user_verify_less_than_four_character_Your_Information_Zip_Code_error_message() throws Throwable {

	}

	@Then("^user verify Symbol invalid Your Information Zip Code error message$")
	public void user_verify_Symbol_invalid_Your_Information_Zip_Code_error_message() throws Throwable {

	}

	@Then("^user verify require Your Information Country error message$")
	public void user_verify_require_Your_Information_Country_error_message() throws Throwable {

	}

	@Then("^user update Address Section and click on Save button$")
	public void user_update_Address_Section_and_click_on_Save_button() throws Throwable {

	}

	@Then("^user verify updated Address section of Your Information$")
	public void user_verify_updated_Address_section_of_Your_Information() throws Throwable {

	}


    
    
}
