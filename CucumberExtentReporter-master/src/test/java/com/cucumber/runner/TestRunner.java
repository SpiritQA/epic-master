package com.cucumber.runner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import managers.FileReaderManager;


@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/features/" }, glue = { "" }, tags = {
" @EPIC27520"}, dryRun = false, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:" })

public class TestRunner {

	private static final Logger log = LogManager.getLogger();

	@BeforeClass
	public static void initializeExtentReports() {
		// CurrentClassHolder.setClass(TestRunner.class);
		Thread t = Thread.currentThread();
		t.setName("Node 1 chrome");
		// Calendar cal = Calendar.getInstance();
		// SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_YY_HH_mm_ss");
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		extentProperties.setReportPath("output/cucumber-reports/extent-reports/test/reports.html");
		extentProperties.setProjectName("Spirit_EPIC");
		
	
	}

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
		 Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		    Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		    Reporter.setSystemInfo("Machine", 	"Windows 10" + "  64 Bit");
		    Reporter.setSystemInfo("Selenium", "3.7.0");
		    Reporter.setSystemInfo("Maven", "3.5.2");
		    Reporter.setSystemInfo("Java Version", "1.8.0_171");
		    Reporter.assignAuthor("Jamal Sheik");
		  
		   
	}
	

}
