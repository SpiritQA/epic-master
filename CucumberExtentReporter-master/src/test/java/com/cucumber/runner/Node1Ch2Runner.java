package com.cucumber.runner;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import managers.FileReaderManager;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features/Book.feature","src/test/resources/features/Checkin.feature","src/test/resources/features/CustomerLogin.feature"}, glue = {""},  dryRun = false,
plugin = {"com.cucumber.listener.ExtentCucumberFormatter:"})
public class Node1Ch2Runner {

	
	
private static final Logger log = LogManager.getLogger();
	
	@BeforeClass
	public static  void initializeExtentReports() {
		Thread t = Thread.currentThread();
		t.setName("Node 1 chrome 2");
//		Calendar cal = Calendar.getInstance();
//      SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_YY_HH_mm_ss");
      ExtentProperties extentProperties = ExtentProperties.INSTANCE;
      extentProperties.setReportPath("output/cucumber-reports/extent-reports/test/reports.html");

	}
	
	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
	}
}
