package com.cucumber.runner;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//@RunWith(Suite.class)
//@SuiteClasses({ TestRunner.class, TestRunnerdummy.class })
public class Junitrunner {
	@Test
    public void test() {
		 Class[] cls = { TestRunner.class, TestRunnerdummy.class,FirefoxnodeONERunner.class,FirefoxNodeTWORunner.class };

        // Parallel among classes
        JUnitCore.runClasses(ParallelComputer.classes(), cls);

//        System.out.println("----------------------------");
//        
//        // Parallel among methods in a class
//        JUnitCore.runClasses(ParallelComputer.methods(), cls);
//
//        System.out.println("----------------------------");
//        
//        // Parallel all methods in all classes
//        JUnitCore.runClasses(new ParallelComputer(true, true), cls);
    }
	
	public static  String suiteName() {
//		String suitename = this.getName
		Class thisClass = new Object(){}.getClass();
		String className = thisClass.getEnclosingClass().getSimpleName();
//				String currentsuitename = this.getClass().getSimpleName();
				return className;
	}
}
