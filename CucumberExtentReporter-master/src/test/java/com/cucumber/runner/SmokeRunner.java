package com.cucumber.runner;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;

public class SmokeRunner {

	@Test
	public void SmokeTest() {
		Class[] cls = { Node1Ch1Runner.class, Node1Ch2Runner.class, Node1Ch3Runner.class,Node2Ch1Runner.class,Node2Ch2Runner.class,Node2Ch3Runner.class, };

		// Parallel among classes
		JUnitCore.runClasses(ParallelComputer.classes(), cls);

		// System.out.println("----------------------------");
		//
		// // Parallel among methods in a class
		// JUnitCore.runClasses(ParallelComputer.methods(), cls);
		//
		// System.out.println("----------------------------");
		//
		// // Parallel all methods in all classes
		// JUnitCore.runClasses(new ParallelComputer(true, true), cls);
	}

	public static String suiteName() {
		// String suitename = this.getName
		Class thisClass = new Object() {
		}.getClass();
		String className = thisClass.getEnclosingClass().getSimpleName();
		// String currentsuitename = this.getClass().getSimpleName();
		return className;
	}

}
